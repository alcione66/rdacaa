package ec.gob.msp.alfresco.operations;

public class AlfrescoFileNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 8174527959927167688L;

	public AlfrescoFileNotFoundException() {
		super();
	}

	public AlfrescoFileNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public AlfrescoFileNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public AlfrescoFileNotFoundException(String message) {
		super(message);
	}

	public AlfrescoFileNotFoundException(Throwable cause) {
		super(cause);
	}

}
