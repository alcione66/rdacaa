CREATE VIEW VARIABLES_LUGAR_TIPO_ATENCION AS
SELECT
    ATENCIONMEDICA.ID                  AS ID_ATENCION,
    ATENCIONMEDICA.UUID                AS UUID_ATENCION,
    INTRAEXTRAMURAL.CTLUGARATENCION_ID AS COD_EST_LUG_ATENCION,
    DC_LUGAR_AT.DESCRIPCION            AS COD_EST_LUG_ATENCION_DESC,
    INTRAEXTRAMURAL.CTTIPOATENCION_ID  AS COD_EST_TIP_ATENCION,
    DC_TIPO_ATEN.DESCRIPCION           AS COD_EST_TIP_ATENCION_DESC
FROM
    ATENCIONMEDICA
LEFT OUTER JOIN
    INTRAEXTRAMURAL
ON
    (
        ATENCIONMEDICA.ID = INTRAEXTRAMURAL.ATENCIONMEDICA_ID)
LEFT OUTER JOIN
    DETALLECATALOGO DC_TIPO_ATEN
ON
    (
        INTRAEXTRAMURAL.CTTIPOATENCION_ID = DC_TIPO_ATEN.ID)
LEFT OUTER JOIN
    DETALLECATALOGO DC_LUGAR_AT
ON
    (
        INTRAEXTRAMURAL.CTLUGARATENCION_ID = DC_LUGAR_AT.ID) ;