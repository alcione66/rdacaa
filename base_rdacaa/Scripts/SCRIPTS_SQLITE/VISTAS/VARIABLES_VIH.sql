CREATE VIEW VARIABLES_VIH AS 
SELECT
    ATENCIONMEDICA.ID         AS ID_ATENCION,
    ATENCIONMEDICA.UUID       AS UUID_ATENCION,
    VIHSIDA.CTMOTIVOPRUEBA_ID AS COD_VIH_MOT_PRUEBA,
    CT_MOTIVO.DESCRIPCION     AS COD_VIH_MOT_PRUEBA_DESC,
    VIHSIDA.CTPRUEBAUNO_ID    AS COD_VIH_PRI_PRUEBA,
    CT_PRUEBA_UNO.DESCRIPCION AS COD_VIH_PRI_PRUEBA_DESC,
    VIHSIDA.REACTIVAONOUNO    AS COD_VIH_PRI_PRUEBA_REACTIVA,
    CASE
        WHEN VIHSIDA.REACTIVAONOUNO IS NULL
        THEN NULL
        WHEN VIHSIDA.REACTIVAONOUNO = 0
        THEN 'No'
        WHEN VIHSIDA.REACTIVAONOUNO = 1
        THEN 'Si'
    END                       AS COD_VIH_PRI_PRUEBA_REACTIVA_DESC,
    VIHSIDA.CTPRUEBADOS_ID    AS COD_VIH_SEG_PRUEBA,
    CT_PRUEBA_DOS.DESCRIPCION AS COD_VIH_SEG_PRUEBA_DESC,
    VIHSIDA.REACTIVAONODOS    AS COD_VIH_SEG_PRUEBA_REACTIVA,
    CASE
        WHEN VIHSIDA.REACTIVAONODOS IS NULL
        THEN NULL
        WHEN VIHSIDA.REACTIVAONODOS = 0
        THEN 'No'
        WHEN VIHSIDA.REACTIVAONODOS = 1
        THEN 'Si'
    END                          AS COD_VIH_SEG_PRUEBA_REACTIVA_DESC,
    VIHSIDA.CTVIASTRANSMISION_ID AS COD_VIH_VIA_TRANSMISION,
    CT_VIA_TRANS.DESCRIPCION     AS COD_VIH_VIA_TRANSMISION_DESC,
    VIHSIDA.CARGAVIRAL           AS VIH_CARGA_VIRAL,
    VIHSIDA.CD4                  AS VIH_CD4
FROM
    ATENCIONMEDICA
LEFT OUTER JOIN
    VIHSIDA
ON
    (
        ATENCIONMEDICA.ID = VIHSIDA.ATENCIONMEDICA_ID)
LEFT OUTER JOIN
    DETALLECATALOGO CT_PRUEBA_UNO
ON
    (
        VIHSIDA.CTPRUEBAUNO_ID = CT_PRUEBA_UNO.ID)
LEFT OUTER JOIN
    DETALLECATALOGO CT_PRUEBA_DOS
ON
    (
        VIHSIDA.CTPRUEBADOS_ID = CT_PRUEBA_DOS.ID)
LEFT OUTER JOIN
    DETALLECATALOGO CT_MOTIVO
ON
    (
        VIHSIDA.CTMOTIVOPRUEBA_ID = CT_MOTIVO.ID)
LEFT OUTER JOIN
    DETALLECATALOGO CT_VIA_TRANS
ON
    (
        VIHSIDA.CTVIASTRANSMISION_ID = CT_VIA_TRANS.ID) ;