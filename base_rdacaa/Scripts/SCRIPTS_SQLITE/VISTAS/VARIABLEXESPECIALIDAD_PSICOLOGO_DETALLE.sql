CREATE VIEW VARIABLEXESPECIALIDAD_PSICOLOGO_DETALLE AS
SELECT
    VARIABLEXESPECIALIDAD_PSICOLOGO.ID,
    VARIABLEXESPECIALIDAD_PSICOLOGO.VARIABLE_ID,
    VARIABLE.CODIGO,
    VARIABLE.NOMBRE,
    VARIABLEXESPECIALIDAD_PSICOLOGO.CTESPECIALIDAD_ID,
    DETALLECATALOGO.DESCRIPCION,
    VARIABLEXESPECIALIDAD_PSICOLOGO.ISMANDATORIO,
    VARIABLEXESPECIALIDAD_PSICOLOGO.ESTADO
FROM
    VARIABLEXESPECIALIDAD_PSICOLOGO
INNER JOIN
    VARIABLE
ON
    (
        VARIABLEXESPECIALIDAD_PSICOLOGO.VARIABLE_ID = VARIABLE.ID)
INNER JOIN
    DETALLECATALOGO
ON
    (
        VARIABLEXESPECIALIDAD_PSICOLOGO.CTESPECIALIDAD_ID = DETALLECATALOGO.ID);
