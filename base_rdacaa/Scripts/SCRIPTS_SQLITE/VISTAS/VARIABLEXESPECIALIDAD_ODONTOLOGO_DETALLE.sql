CREATE VIEW VARIABLEXESPECIALIDAD_ODONTOLOGO_DETALLE AS
SELECT
VARIABLEXESPECIALIDAD_ODONTOLOGO.ID,
VARIABLEXESPECIALIDAD_ODONTOLOGO.VARIABLE_ID,
VARIABLE.CODIGO,
VARIABLE.NOMBRE,
VARIABLEXESPECIALIDAD_ODONTOLOGO.CTESPECIALIDAD_ID,
DETALLECATALOGO.DESCRIPCION,
VARIABLEXESPECIALIDAD_ODONTOLOGO.ISMANDATORIO,
VARIABLEXESPECIALIDAD_ODONTOLOGO.ESTADO
FROM
VARIABLEXESPECIALIDAD_ODONTOLOGO
INNER JOIN
VARIABLE
ON
(
VARIABLEXESPECIALIDAD_ODONTOLOGO.VARIABLE_ID = VARIABLE.ID)
INNER JOIN
DETALLECATALOGO
ON
(
VARIABLEXESPECIALIDAD_ODONTOLOGO.CTESPECIALIDAD_ID = DETALLECATALOGO.ID);
