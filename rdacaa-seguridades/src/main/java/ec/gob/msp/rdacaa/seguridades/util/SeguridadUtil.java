package ec.gob.msp.rdacaa.seguridades.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.attribute.PosixFileAttributeView;
import java.nio.file.attribute.PosixFileAttributes;
import java.nio.file.attribute.PosixFilePermission;
import java.security.GeneralSecurityException;
import java.util.Arrays;

import com.amazonaws.util.Base64;
import com.google.crypto.tink.Aead;
import com.google.crypto.tink.BinaryKeysetReader;
import com.google.crypto.tink.BinaryKeysetWriter;
import com.google.crypto.tink.CleartextKeysetHandle;
import com.google.crypto.tink.HybridDecrypt;
import com.google.crypto.tink.HybridEncrypt;
import com.google.crypto.tink.JsonKeysetReader;
import com.google.crypto.tink.KeysetHandle;
import com.google.crypto.tink.NoSecretKeysetHandle;
import com.google.crypto.tink.aead.AeadFactory;
import com.google.crypto.tink.aead.AeadKeyTemplates;
import com.google.crypto.tink.config.TinkConfig;
import com.google.crypto.tink.hybrid.HybridDecryptFactory;
import com.google.crypto.tink.hybrid.HybridEncryptFactory;
import com.google.crypto.tink.subtle.ChaCha20Poly1305;
import com.google.crypto.tink.subtle.Random;

import de.mkammerer.argon2.Argon2Advanced;
import de.mkammerer.argon2.Argon2Constants;
import de.mkammerer.argon2.Argon2Factory;

public class SeguridadUtil {

	private static Argon2Advanced argon2 = Argon2Factory.createAdvanced();

	private static int memory = 250000;
	private static int parallelism = 2;
	private static int iterations = 4;

	private SeguridadUtil() {
	}

	public static void init() throws GeneralSecurityException {
		TinkConfig.register();
	}

	/**
	 * 
	 * Crea una llave ChaCha20_Poly1305 que esta cifrada con otra llave generada
	 * con Argon2 a partir del password del usuario y una sal aleatoria.
	 * 
	 * @param usuario
	 * @param password
	 * @return
	 * @throws GeneralSecurityException
	 * @throws IOException
	 */

	public static DatosLlave crearLlaveUnicaUsuario(String usuario, String password)
			throws GeneralSecurityException, IOException {

		KeysetHandle keysetHandle = KeysetHandle.generateNew(AeadKeyTemplates.CHACHA20_POLY1305);

		byte[] salt = Random.randBytes(Argon2Constants.DEFAULT_SALT_LENGTH);
		byte[] cipherPass = null;

		try (ByteArrayOutputStream stream = new ByteArrayOutputStream()) {
			CleartextKeysetHandle.write(keysetHandle, BinaryKeysetWriter.withOutputStream(stream));
			cipherPass = cifrarConPasswordYSalt(password, salt, stream.toByteArray(), usuario);
		}

		return new DatosLlave(cipherPass, salt);
	}

	/***
	 * Crea una llave a partir de un password conocido por el usuario y una sal
	 * aleatoria previamente generada con crearLlaveUnicaUsuario, utilizando
	 * Argon2i.
	 * 
	 * @param password
	 * @param salt
	 * @return
	 */
	private static byte[] crearLlaveConPasswordYSalt(String password, byte[] salt) {

		return argon2.rawHash(iterations, memory, parallelism, password, salt);
	}

	/***
	 * Decifra un texto cifrado con una llave Argon2 creada a partir del
	 * password del usuario, * y una sal aleatoria previamente generada con
	 * crearLlaveUnicaUsuario.
	 * 
	 * @param password
	 * @param salt
	 * @param ciphertext
	 * @param usuario
	 * @return
	 * @throws GeneralSecurityException
	 */
	public static byte[] descifrarConPasswordYSalt(String password, byte[] salt, byte[] ciphertext, String usuario)
			throws GeneralSecurityException {

		byte[] keyPass = crearLlaveConPasswordYSalt(password, salt);

		ChaCha20Poly1305 aead = new ChaCha20Poly1305(keyPass);

		return aead.decrypt(ciphertext, usuario.getBytes(StandardCharsets.UTF_8));
	}

	/***
	 * Cifra una cadena de bytes utilizando una llave Argon2 creada a partir de
	 * una contrasena conocida por el usuario y una sal aleatoria previamente
	 * generada con crearLlaveUnicaUsuario.
	 * 
	 * @param password
	 * @param salt
	 * @param plaintext
	 * @param usuario
	 * @return
	 * @throws GeneralSecurityException
	 */
	public static byte[] cifrarConPasswordYSalt(String password, byte[] salt, byte[] plaintext, String usuario)
			throws GeneralSecurityException {

		byte[] keyPass = crearLlaveConPasswordYSalt(password, salt);

		ChaCha20Poly1305 aead = new ChaCha20Poly1305(keyPass);

		return aead.encrypt(plaintext, usuario.getBytes(StandardCharsets.UTF_8));
	}

	/***
	 * Cifra una cadena de bytes utilizando una llave previamente generada por
	 * crearLlaveUnicaUsuario. Como la llave generada esta cifrada con la
	 * contrasena del usuario, se requiere antes haber decifrado esa llave con
	 * decifrarConPasswordYSalt para poder utilizarla en esta funcion.
	 * 
	 * @param plaintext
	 * @param key
	 * @param usuario
	 * @return
	 * @throws GeneralSecurityException
	 * @throws IOException
	 */
	public static byte[] cifrar(byte[] plaintext, byte[] key, String usuario)
			throws GeneralSecurityException, IOException {

		KeysetHandle keysetHandle = CleartextKeysetHandle.read(BinaryKeysetReader.withBytes(key));

		Aead aead = AeadFactory.getPrimitive(keysetHandle);

		return aead.encrypt(plaintext, usuario.getBytes(StandardCharsets.UTF_8));
	}
	
	public static String cifrarString(String plaintext, byte[] key, String usuario)
			throws GeneralSecurityException, IOException {

		return Base64.encodeAsString(cifrar(plaintext.getBytes(StandardCharsets.UTF_8), key, usuario));
	}

	/**
	 * Cifra una cadena de bytes con un keyset que contiene una llave publica.
	 * 
	 * @param plaintext
	 * @param keysetHandle
	 * @return
	 * @throws GeneralSecurityException
	 */
	public static byte[] cifrarPublico(byte[] plaintext, KeysetHandle publicKeysetHandle) throws GeneralSecurityException {

		HybridEncrypt hybridEncrypt= HybridEncryptFactory.getPrimitive(publicKeysetHandle);

		return hybridEncrypt.encrypt(plaintext, null);
	}
	
	public static String cifrarPublicoString(String plaintext, KeysetHandle keysetHandle)
			throws GeneralSecurityException {

		return Base64.encodeAsString(cifrarPublico(plaintext.getBytes(StandardCharsets.UTF_8), keysetHandle));
	}

	/***
	 * Decifra una cadena de bytes utilizando una llave previamente generada por
	 * crearLlaveUnicaUsuario. Como la llave generada esta cifrada con la
	 * contrasena del usuario, se requiere antes haber decifrado esa llave con
	 * decifrarConPasswordYSalt para poder utilizarla en esta funcion.
	 * 
	 * @param ciphertext
	 * @param key
	 * @param usuario
	 * @return
	 * @throws GeneralSecurityException
	 * @throws IOException
	 */
	public static byte[] descifrar(byte[] ciphertext, byte[] key, String usuario)
			throws GeneralSecurityException, IOException {
		KeysetHandle keysetHandle = CleartextKeysetHandle.read(BinaryKeysetReader.withBytes(key));

		Aead aead = AeadFactory.getPrimitive(keysetHandle);

		return aead.decrypt(ciphertext, usuario.getBytes(StandardCharsets.UTF_8));
	}
	
	public static String descifrarString(String ciphertext, byte[] key, String usuario)
			throws GeneralSecurityException, IOException {
		return new String(descifrar(Base64.decode(ciphertext), key, usuario),StandardCharsets.UTF_8);
	}

	/**
	 * Decifra una cadena de bytes que fue cifrada con una llave publica,
	 * utilizando el keyset que corresponde a su llave privada.
	 * 
	 * @param ciphertext
	 * @param keysetHandle
	 * @return
	 * @throws GeneralSecurityException
	 */
	public static byte[] descifrarPrivado(byte[] ciphertext, KeysetHandle privateKeysetHandle)
			throws GeneralSecurityException {

		HybridDecrypt hybridDecrypt = HybridDecryptFactory.getPrimitive(privateKeysetHandle);

		return hybridDecrypt.decrypt(ciphertext, null);
	}
	
	public static String descifrarPrivadoString(String ciphertext, KeysetHandle keysetHandle)
			throws GeneralSecurityException {
		return new String(descifrarPrivado(Base64.decode(ciphertext), keysetHandle),StandardCharsets.UTF_8);
	}

	/***
	 * Crea un hash de la contrasena utilizando Argon2i
	 * 
	 * @param password
	 * @return
	 */
	public static String hashContrasena(String password) {

		return argon2.hash(iterations, memory, parallelism, password);
	}

	/***
	 * Verifica una contrasena contra un hash previamente generado con Argon2i
	 * 
	 * @param hash
	 * @param password
	 * @return
	 */
	public static boolean verificarContrasena(String hash, String password) {
		return argon2.verify(hash, password);
	}

	/***
	 * Limpia un arreglo de bytes una vez se han utilizado los datos.
	 * 
	 * @param secreto
	 */
	public static void limpiar(byte[] secreto) {
		assert secreto != null;
		Arrays.fill(secreto, (byte) 0);

		// TODO SeguridadUtil.limpiar(utilitario); //limpiar la variable de la
		// llave privada antes de salir.
	}

	/***
	 * Lee la llave publica desde un archivo ubicado en el path ingresado
	 * 
	 * @param path
	 * @return
	 * @throws GeneralSecurityException
	 * @throws IOException
	 */
	public static KeysetHandle leerLlavePublica(String path) throws GeneralSecurityException, IOException {

		return NoSecretKeysetHandle.read(JsonKeysetReader.withPath(path));
	}

	/***
	 * Lee la llave privada desde un archivo ubicado en el path ingresado.
	 * 
	 * IMPORTANTE: NO ES SEGURO guardar el archivo en disco con la llave privada
	 * en texto claro, se debe guardar el archivo cifrado o al menos asegurado a
	 * nivel de permisos en el S.O. Esta funcion no se encarga de asegurar el
	 * archivo en texto claro.
	 * 
	 * @param path
	 * @return
	 * @throws GeneralSecurityException
	 * @throws IOException
	 */
	public static KeysetHandle leerLlavePrivada(String path) throws GeneralSecurityException, IOException {

		String permisos = leerPermisosArchivo(path);
		if(permisos.equals("400") || permisos.equals("600")){
			return CleartextKeysetHandle.read(JsonKeysetReader.withPath(path));
		}
		
		StringBuilder sb = new StringBuilder();
		sb.append("Los permisos del archivo de llave privada son muy abiertos: ");
		sb.append(permisos);
		sb.append(". Por favor cambielos a 400");
		
		throw new GeneralSecurityException(sb.toString()); 
	}

	public static String leerPermisosArchivo(String path) throws IOException {

		int owner = 0;
		int group = 0;
		int others = 0;

		PosixFileAttributes attrs = Files.getFileAttributeView(new File(path).toPath(), PosixFileAttributeView.class)
				.readAttributes();

		for (PosixFilePermission c : attrs.permissions()) {

			switch (c) {
			case OWNER_READ:
				owner += 4;
				break;
			case OWNER_WRITE:
				owner += 2;
				break;
			case OWNER_EXECUTE:
				owner += 1;
				break;
			case GROUP_READ:
				group += 4;
				break;
			case GROUP_WRITE:
				group += 2;
				break;
			case GROUP_EXECUTE:
				group += 1;
				break;
			case OTHERS_READ:
				others += 4;
				break;
			case OTHERS_WRITE:
				others += 2;
				break;
			case OTHERS_EXECUTE:
				others += 1;
				break;
			}
		}
		return String.valueOf(owner)+String.valueOf(group)+String.valueOf(others);
	}
	
}
