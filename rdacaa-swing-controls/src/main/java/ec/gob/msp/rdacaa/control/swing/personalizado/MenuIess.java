/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.control.swing.personalizado;

import java.awt.Dimension;
import javax.swing.JMenu;

/**
 *
 * @author jtarapuez
 */
public class MenuIess extends JMenu {

    private static final long serialVersionUID = -4508643064095712469L;

    public MenuIess(String texto) {
        setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
//        setPreferredSize(new Dimension(texto.length(), 30));
        setBackground(new java.awt.Color(255, 255, 255));
        setBorder(null);
        setForeground(new java.awt.Color(108, 119, 132));
        setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/hijo.png"))); // NOI18N
        setIconTextGap(20);
        setOpaque(true);
        setText(texto);
    }

}
