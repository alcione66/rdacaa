/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.control.swing.personalizado;

import ec.gob.msp.rdacaa.control.swing.utilitario.color.UtilitarioColor;
import ec.gob.msp.rdacaa.control.swing.utilitario.combo.ComboUtilitario;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Insets;
import java.awt.RenderingHints;
import java.awt.geom.Area;
import java.awt.geom.Rectangle2D;
import java.awt.geom.RoundRectangle2D;
import javax.swing.JComboBox;
import javax.swing.border.AbstractBorder;

/**
 *
 * @author christian
 */
public class ComboMspAdmision extends JComboBox {

    private static final long serialVersionUID = -313915287612354739L;

    public ComboMspAdmision() {
        Dimension dimension = new Dimension(250, 32);
        setPreferredSize(dimension);
        setSize(dimension);
        setForeground(new java.awt.Color(125, 125, 125));
        setBackground(new Color(247, 247, 247));
        setUI(ComboUtilitario.createUI(this));
        setBorder(new RedondoCombo());
    }

    public void setearBordeComboNormal() {
        setBorder(new RedondoCombo());
    }

    public void setearBordeComboRequerido() {
        setBorder(new RedondoComboRojo());
    }

    class RedondoCombo extends AbstractBorder {

        private static final long serialVersionUID = 8652111766127941679L;

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            Graphics2D g2 = (Graphics2D) g.create();
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            int w = getWidth();
            int h = getHeight();
            RoundRectangle2D round = new RoundRectangle2D.Float(0, 0, w - 1, h - 1, 10, 10);
            Container parent = c.getParent();
            if (parent != null) {
                g2.setColor(parent.getBackground());
                Area corner = new Area(new Rectangle2D.Float(x, y, width, height));
                corner.subtract(new Area(round));
                g2.fill(corner);
            }
            g2.setColor(Color.LIGHT_GRAY);
            g2.draw(round);
            g2.dispose();
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return new Insets(0, 5, 0, 0);
        }

        @Override
        public Insets getBorderInsets(Component c, Insets insets) {
            insets.left = insets.right = 0;
            insets.top = insets.bottom = 0;
            return insets;
        }
    }

    class RedondoComboRojo extends AbstractBorder {

        private static final long serialVersionUID = 8652111766127941679L;

        @Override
        public void paintBorder(Component c, Graphics g, int x, int y, int width, int height) {
            Graphics2D g2 = (Graphics2D) g.create();
            g2.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            int w = getWidth();
            int h = getHeight();
            RoundRectangle2D round = new RoundRectangle2D.Float(0, 0, w - 1, h - 1, 10, 10);
            Container parent = c.getParent();
            if (parent != null) {
                g2.setColor(parent.getBackground());
                Area corner = new Area(new Rectangle2D.Float(x, y, width, height));
                corner.subtract(new Area(round));
                g2.fill(corner);
            }
            g2.setColor(UtilitarioColor.CAMPO_REQUERIDO);
            g2.draw(round);
            g2.dispose();
        }

        @Override
        public Insets getBorderInsets(Component c) {
            return new Insets(0, 5, 0, 0);
        }

        @Override
        public Insets getBorderInsets(Component c, Insets insets) {
            insets.left = insets.right = 0;
            insets.top = insets.bottom = 0;
            return insets;
        }
    }

}
