/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.control.swing.personalizado;

import javax.swing.JLabel;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author christian
 */
public class TituloEtiquetaIess extends JLabel {

    private static final long serialVersionUID = 5396381973169678844L;

    public TituloEtiquetaIess() {
        setFont(new java.awt.Font("Open Sans", 1, 13)); // NOI18N
        setForeground(new java.awt.Color(86, 86, 86));
        setOpaque(false);
        setBorder(new EmptyBorder(3, 3, 3, 3));
    }
}
