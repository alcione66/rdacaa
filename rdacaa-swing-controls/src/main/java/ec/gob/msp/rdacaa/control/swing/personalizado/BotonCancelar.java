package ec.gob.msp.rdacaa.control.swing.personalizado;

import javax.swing.JButton;

/**
 *
 * @author jtarapuez
 */
public class BotonCancelar extends JButton {

	private static final long serialVersionUID = -5001920718850782647L;

	public BotonCancelar() {

		setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/bot_cancelar1_off.png"))); // NOI18N
		setBorder(null);
		setBorderPainted(false);
		setContentAreaFilled(false);
		setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		setPreferredSize(new java.awt.Dimension(94, 29));
		setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/bot_cancelar1_on.png"))); // NOI18N

	}

}
