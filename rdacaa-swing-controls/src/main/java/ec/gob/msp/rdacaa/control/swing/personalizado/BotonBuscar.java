package ec.gob.msp.rdacaa.control.swing.personalizado;

import javax.swing.JButton;

/**
 *
 * @author jtarapuez
 */
public class BotonBuscar extends JButton {

	private static final long serialVersionUID = -7676509046674261969L;

	public BotonBuscar() {
    
    setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/bot_buscar_off.png"))); // NOI18N
      setBorder(null);
        setBorderPainted(false);
        setContentAreaFilled(false);
       setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
     setPreferredSize(new java.awt.Dimension(94, 29));
        setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/bot_buscar_on.png"))); // NOI18N
        
    
    }
    
 


}
