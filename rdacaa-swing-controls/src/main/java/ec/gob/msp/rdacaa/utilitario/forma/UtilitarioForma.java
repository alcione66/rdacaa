/*
 * Copyright 2018 MINISTERIO DE SALUD PUBLICA - ECUADOR
 * Todos los derechos reservados
 */
package ec.gob.msp.rdacaa.utilitario.forma;

import java.awt.Color;
import java.awt.Component;
import java.awt.Container;
import java.awt.Event;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.FocusEvent;
import java.awt.event.KeyEvent;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Enumeration;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.swing.AbstractButton;
import javax.swing.BorderFactory;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.InputMap;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JComponent;
import javax.swing.JInternalFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JPasswordField;
import javax.swing.JRadioButton;
import javax.swing.JSpinner;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.KeyStroke;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import javax.swing.text.JTextComponent;

import org.codehaus.plexus.util.StringUtils;

import com.github.lgooddatepicker.components.DatePicker;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.control.swing.notifications.Notifications;
import ec.gob.msp.rdacaa.control.swing.personalizado.CajaTextoAreaIess;
import ec.gob.msp.rdacaa.control.swing.personalizado.CajaTextoIess;
import ec.gob.msp.rdacaa.control.swing.personalizado.ComboIess;
import ec.gob.msp.rdacaa.control.swing.personalizado.ComboMspAdmision;
import ec.gob.msp.rdacaa.utilitario.enumeracion.CodigoRetornoEnum;
import ec.gob.msp.rdacaa.utilitario.enumeracion.ComunEnum;
import ec.gob.msp.rdacaa.utilitario.fecha.FechasUtil;
import ec.gob.msp.rdacaa.utilitario.sesion.TipoMensaje;

/**
 * <b>
 * Incluir aqui la descripcion de la clase.
 * </b>
 *
 * @author David Murillo
 * <p>
 * [$Author: David Murillo $, $Date: 31/05/2016]</p>
 */
public class UtilitarioForma {
	
	private UtilitarioForma() {}

    private static final String CAMPO = "Campo ";
    private static final String REQUERIDO = " Requerido";
    private static final String SALTO_LINEA = "\n";
    private static final String SIN_ESPACIO = "";
    private static final String ESPACIO_BLANCO = " ";
    public static final String SELECCIONE = "Seleccione";
    private static final String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public static boolean validarEmail(String email) {
        Pattern pattern = Pattern.compile(PATTERN_EMAIL);
        Matcher matcher = pattern.matcher(email);
        return matcher.matches();
    }

    public static boolean validarEmailSwing(Component ventana, String email) {
        boolean retorno = true;
        Pattern pattern = Pattern.compile(PATTERN_EMAIL);
        Matcher matcher = pattern.matcher(email);
        retorno = matcher.matches();
        if (!retorno) {
            UtilitarioForma.muestraMensaje(null, ventana, TipoMensaje.ERROR.getNemonico(), "Formato de email incorrecto.");
        }
        return retorno;
    }

    private static void desabilitarPegado(Object[] components) {
        for (Object campo : components) {
            if (campo instanceof JComponent) {
                JComponent campoAux = (JComponent) campo;
                InputMap map = campoAux.getInputMap();
                map.put(KeyStroke.getKeyStroke(KeyEvent.VK_V, Event.CTRL_MASK), "null");
                map.put(KeyStroke.getKeyStroke(KeyEvent.VK_INSERT, Event.SHIFT_MASK), "null");
            }
        }

    }

    private static void desabilitarCopiado(Object[] components) {

        for (Object campo : components) {

            if (campo instanceof JComponent) {
                InputMap map = ((JComponent) campo).getInputMap();
                map.put(KeyStroke.getKeyStroke(KeyEvent.VK_C, Event.CTRL_MASK), "null");
            }
        }
    }

    public static final void ganarFoco(KeyEvent evt, Component j) {
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            j.requestFocus();
        }
    }

    public static final void aparecerControles(boolean aparece, Component... j) {
        for (Component c : j) {
            c.setVisible(aparece);
        }
    }

    public static ImageIcon formatoImagenBoton(ImageIcon image, int tamanioAncho, int tamanioLargo) {
        ImageIcon imageIcon = image;
        Image image1 = imageIcon.getImage();
        return new ImageIcon(image1.getScaledInstance(tamanioAncho, tamanioLargo, Image.SCALE_SMOOTH));
    }

    /**
     *
     * <b>
     * Incluir aqui­ la descripcion del metodo.
     * </b>
     * <p>
     * [$Author: David Murillo $, $Date: 01/07/2015]</p>
     *
     * @param j
     * @param codigoRespuesta
     * @return
     */
    public static boolean validarRespuestaCrud(Component j, String codigoRespuesta) {
        if (codigoRespuesta.equals(CodigoRetornoEnum.CORRECTO.getCodigo())) {
            muestraMensaje(null, j, TipoMensaje.INFO.getNemonico(), "Su registro ha sido guardado con éxito");
            return true;
        } else {
            muestraMensaje(null, j, TipoMensaje.ERROR.getNemonico(), ComunEnum.ERROR_REGISTRO_ACTUALIZADO.getDescripcion());
            return false;
        }
    }

    public static boolean validarRespuestaCrudMensaje(Component j, String codigoRespuesta) {
        if (codigoRespuesta.equals(CodigoRetornoEnum.CORRECTO.getCodigo())) {
            muestraMensaje(null, j, TipoMensaje.INFO.getNemonico(), "Su registro ha sido guardado con éxito");
            return true;
        } else {
            muestraMensaje(null, j, TipoMensaje.ERROR.getNemonico(), codigoRespuesta.length() > 2 ? codigoRespuesta.substring(2) : ComunEnum.ERROR_REGISTRO_ACTUALIZADO.getDescripcion());
            return false;
        }
    }

    /**
     *
     * <b>
     * Mensaje de eliminacion, validacion
     * </b>
     * <p>
     * [$Author: jtufinol $, $Date: 20/10/2016]</p>
     *
     * @param j
     * @param codigoRespuesta
     * @return
     */
    public static boolean validarRespuestaCrudMensajeEliminar(Component j, String codigoRespuesta) {
        if (codigoRespuesta.equals(CodigoRetornoEnum.CORRECTO.getCodigo())) {
            muestraMensaje(null, j, TipoMensaje.INFO.getNemonico(), "Su registro ha sido eliminado con éxito");
            return true;
        } else {
            muestraMensaje(null, j, TipoMensaje.ERROR.getNemonico(), codigoRespuesta.length() > 2 ? codigoRespuesta.substring(2) : ComunEnum.ERROR_REGISTRO_ACTUALIZADO.getDescripcion());
            return false;
        }
    }

    public static boolean validarRespuestaToken(Component j, String codigoRespuesta) {
        if (codigoRespuesta.equals(CodigoRetornoEnum.ERROR.getCodigo())) {
            muestraMensaje(null, j, TipoMensaje.ERROR.getNemonico(), ComunEnum.ERROR_REGISTRO_ACTUALIZADO.getDescripcion());
            return false;
        } else {
            return true;
        }
    }

    /**
     *
     * <b>
     * Incluir aqui­ la descripcion del metodo.
     * </b>
     * <p>
     * [$Author: David Murillo $, $Date: 01/07/2015]</p>
     *
     * @param j
     * @param codigoRepuesta
     * @param mensajeRespuesta
     * @return
     */
    public static boolean validarRespuestaServicioConsulta(Component j, String codigoRepuesta, String mensajeRespuesta) {
        if (codigoRepuesta.equals(CodigoRetornoEnum.CORRECTO.getCodigo())) {
            return true;
        } else {
            muestraMensaje(null, j, TipoMensaje.ERROR.getNemonico(), ComunEnum.ERROR_RECUPERAR_REGISTRO.getDescripcion() + ESPACIO_BLANCO + mensajeRespuesta);
            return false;
        }
    }

    /**
     *
     * <b>
     * Incluir aqui­ la descripcion del metodo.
     * </b>
     * <p>
     * [$Author: David Murillo $, $Date: 01/07/2015]</p>
     *
     * @param ventana
     * @param controles
     * @return
     */
    public static boolean validarCamposRequeridos(String tituloVentana, Component ventana, Object... controles) {
        StringBuilder mensajes = new StringBuilder(SIN_ESPACIO);
        for (Object obj : controles) {
            StringBuilder cajas = validarTextos(obj);
            StringBuilder combos = validarCombos(obj);
            if (cajas != null) {
                mensajes.append(cajas);
            }
            if (combos != null) {
                mensajes.append(combos);
            }
        }
        return validarCadenaMensajes(tituloVentana, ventana, mensajes);
    }
    
    /**
     *
     * <b>
     * Incluir aqui­ la descripcion del metodo.
     * </b>
     * <p>
     * [$Author: David Murillo $, $Date: 01/07/2015]</p>
     *
     * @param ventana
     * @param controles
     * @return
     */
    public static boolean validarCamposRequeridos(Object... controles) {
        List<ValidationResult> listaErrores = new ArrayList<>();
        for (Object obj : controles) {
           
            if(obj != null){
                StringBuilder cajas = validarTextos(obj);
                StringBuilder combos = validarCombos(obj);
                StringBuilder radios = validarGroupButtonRadio(obj);
                StringBuilder listas = validarList(obj);

                if (cajas != null) {
                    ValidationResult validationError = new ValidationResult("", cajas.toString());
                    listaErrores.add(validationError);
                }
                if (combos != null) {
                    ValidationResult validationError = new ValidationResult("", combos.toString());
                    listaErrores.add(validationError);
                }
                if(radios != null){
                    ValidationResult validationError = new ValidationResult("", radios.toString());
                    listaErrores.add(validationError);
                }
                if (listas != null){
                    ValidationResult validationError = new ValidationResult("", listas.toString());
                    listaErrores.add(validationError);
                }
            }
        }
        if (!listaErrores.isEmpty()) {
            Notifications.showFormValidationAlert(listaErrores, "Validación Forma");
            return false;
        }else{
          return true;  
        }
    }

    public static StringBuilder validarList(Object obj) {
        StringBuilder mensajes = new StringBuilder(SIN_ESPACIO);
        if (obj instanceof JList){
            JList jList = (JList) obj;
            if (jList.getModel().getSize() == 0){
                mensajes.append(CAMPO).append(jList.getToolTipText()).append(REQUERIDO).append(SALTO_LINEA);
            }
        }
        if (!mensajes.toString().isEmpty()) {
            return mensajes;
        } else {
            return null;
        }
    }
            
    public static boolean doesObjectContainField(Object object, String fieldName) {
		Class<?> objectClass = object.getClass();
        for (Field field : objectClass.getDeclaredFields()) {
            if (field.getName().equals(fieldName)) {
                return true;
            }
        }
        return false;
    }
    
    /**
     *
     * <b>
     * Incluir aqui­ la descripcion del metodo.
     * </b>
     * <p>
     * [$Author: David Murillo $, $Date: 01/07/2015]</p>
     *
     * @param ventana
     * @return
     */
    public static boolean validarCamposRequeridos(JPanel ventana) {
        StringBuilder cajas = null;
        List<ValidationResult> listaErrores = new ArrayList<>();
        for (Component panel : ventana.getComponents()) {
            if (panel instanceof JPanel) {
                for (Object obj : ((JPanel) panel).getComponents()) {
                    cajas = validarTextos(obj);
                    if (cajas != null) {
                        ValidationResult validationError = new ValidationResult("", cajas.toString());
                        listaErrores.add(validationError);
                    }
                }
            }
        }
        if (!listaErrores.isEmpty()) {
            Notifications.showFormValidationAlert(listaErrores, "Validación Forma");
            return false;
        }else{
          return true;  
        }
    }
    

    /**
     *
     * <b>
     * Incluir aqui­ la descripcion del metodo.
     * </b>
     * <p>
     * [$Author: David Murillo $, $Date: 01/07/2015]</p>
     *
     * @param ventana
     * @param controles
     * @return
     */
    public static void limpiarCampos(Component ventana, Object... controles) {
        desabilitarCopiado(controles);
        desabilitarPegado(controles);

        for (Object obj : controles) {
            if (obj instanceof JLabel) {
                JLabel t = (JLabel) obj;
                t.setText("-");
                t.setIcon(new ImageIcon());
            }
            if (obj instanceof JRadioButton) {
                JRadioButton t = (JRadioButton) obj;
                t.setSelected(false);
            }
            if (obj instanceof ButtonGroup) {
                ButtonGroup t = (ButtonGroup) obj;
                t.clearSelection();
            }
            if (obj instanceof JCheckBox) {
                JCheckBox t = (JCheckBox) obj;
                t.setSelected(false);
            }
            if (obj instanceof JList) {
                JList t = (JList) obj;
                t.clearSelection();
            }
            if (obj instanceof JTable) {
                ((JTable) obj).clearSelection();
            }
            if (obj instanceof JTextField) {
                JTextField t = (JTextField) obj;
                t.setText("");
            }
            if (obj instanceof JComboBox) {
                ((JComboBox) obj).setSelectedItem(null);
            }
        }

    }

    /**
     *
     * <b>
     * Incluir aqui­ la descripcion del metodo.
     * </b>
     * <p>
     * [$Author: David Murillo $, $Date: 01/07/2015]</p>
     *
     * @param ventana
     * @param habilitar
     * @param controles
     */
    public static void habilitarControles(boolean habilitar, Object... controles) {
        for (Object obj : controles) {
            habilitarCajas(obj, habilitar);
            if (obj instanceof JComboBox) {
                JComboBox t = (JComboBox) obj;
                t.setEnabled(habilitar);
                if(!habilitar){
                    t.getEditor().getEditorComponent().setBackground(Color.LIGHT_GRAY);
                }else{
                    t.getEditor().getEditorComponent().setBackground(Color.WHITE);
                }
            }
            if (obj instanceof JTable) {
                JTable t = (JTable) obj;
                t.setEnabled(habilitar);
            }
            if (obj instanceof JRadioButton) {
                JRadioButton t = (JRadioButton) obj;
                t.setEnabled(habilitar);
            }

        }
    }

    /**
     *
     * <b>
     * Incluir aqui­ la descripcion del metodo.
     * </b>
     * <p>
     * [$Author: David Murillo $, $Date: 01/07/2015]</p>
     */
    private static void habilitarCajas(Object obj, boolean habilitar) {
        if (obj instanceof JTextField) {
            JTextField t = (JTextField) obj;
            t.setEditable(habilitar);
            if(!habilitar){
                t.setBackground(Color.LIGHT_GRAY);
            }else{
                t.setBackground(Color.WHITE);
            }
        }
        if (obj instanceof JTextArea) {
            JTextArea t = (JTextArea) obj;
            t.setEditable(habilitar);
            if(!habilitar){
                t.setBackground(Color.LIGHT_GRAY);
            }else{
                t.setBackground(Color.WHITE);
            }
        }
        if (obj instanceof JPasswordField) {
            JPasswordField t = (JPasswordField) obj;
            t.setEditable(habilitar);
        }
    }
    
    /**
     *
     * <b>
     * Incluir aqui­ la descripcion del metodo.
     * </b>
     * <p>
     * [$Author: David Murillo $, $Date: 01/07/2015]</p>
     *
     * @param ventana
     * @param controles
     */
    public static void reSetearControles(Component ventana,Object... controles) {
        for (Object obj : controles) {
            if (obj instanceof JComboBox) {
                ComboIess t = (ComboIess) obj;
                t.setearBordeComboNormal();
            }
            if (obj instanceof JTextField) {
                CajaTextoIess t = (CajaTextoIess) obj;
                t.setearBordeCajaNormal();
            }
        }
    }
    

    /**
     *
     * <b>
     * Incluir aqui­ la descripcion del metodo.
     * </b>
     * <p>
     * [$Author: David Murillo $, $Date: 01/07/2015]</p>
     *
     * @param ventana
     * @param habilitar
     * @param controles
     */
    public static void activarControles(Component ventana, boolean habilitar, Object... controles) {
        for (Object obj : controles) {
            if (obj instanceof JComboBox) {
                JComboBox t = (JComboBox) obj;
                t.setEnabled(habilitar);
                if(!habilitar){
                    t.getEditor().getEditorComponent().setBackground(Color.LIGHT_GRAY);
                }else{
                    t.getEditor().getEditorComponent().setBackground(Color.WHITE);
                }
            }
            if (obj instanceof JTextField) {
                JTextField t = (JTextField) obj;
                t.setEnabled(habilitar);
                if(!habilitar){
                    t.setBackground(Color.LIGHT_GRAY);
                }else{
                    t.setBackground(Color.WHITE);
                }
            }
            if (obj instanceof JTextArea) {
                JTextArea t = (JTextArea) obj;
                t.setEnabled(habilitar);
                if(!habilitar){
                    t.setBackground(Color.LIGHT_GRAY);
                }else{
                    t.setBackground(Color.WHITE);
                }
            }
            if (obj instanceof JPasswordField) {
                JPasswordField t = (JPasswordField) obj;
                t.setEnabled(habilitar);
            }
            if (obj instanceof JCheckBox) {
                JCheckBox t = (JCheckBox) obj;
                t.setEnabled(habilitar);
            }
            if (obj instanceof JTable) {
                JTable t = (JTable) obj;
                t.setEnabled(habilitar);
            }
            if (obj instanceof JButton) {
                JButton t = (JButton) obj;
                t.setEnabled(habilitar);
            }
        }
    }

    /**
     *
     * <b>
     * Incluir aqui­ la descripcion del metodo.
     * </b>
     * <p>
     * [$Author: David Murillo $, $Date: 01/07/2015]</p>
     */
    private static boolean validarCadenaMensajes(String tituloVentana, Component j, StringBuilder mensajes) {
        if (!mensajes.toString().isEmpty()) {
            muestraMensaje(tituloVentana, j, TipoMensaje.ERROR.getNemonico(), mensajes.toString());
        }
        return mensajes.toString().isEmpty();
    }

    /**
     *
     * <b>
     * Incluir aqui­ la descripcion del metodo.
     * </b>
     * <p>
     * [$Author: David Murillo $, $Date: 01/07/2015]</p>
     */
    private static StringBuilder validarTextos(Object obj) {
        StringBuilder mensajes = new StringBuilder(SIN_ESPACIO);
        if (obj instanceof JTextField) {
                CajaTextoIess t = (CajaTextoIess) obj;
                if (t.getText() == null || t.getText().trim().isEmpty()) {
                    mensajes.append(CAMPO).append(t.getToolTipText()).append(REQUERIDO).append(SALTO_LINEA);
                    t.setearBordeCajaRequerida();
                } else {
                    t.setearBordeCajaNormal();
                }
        }
        if (obj instanceof JPasswordField) {
            JPasswordField t = (JPasswordField) obj;
            if (t.getPassword().length == 0) {
                mensajes.append(CAMPO).append(t.getToolTipText()).append(REQUERIDO).append(SALTO_LINEA);
                t.setBorder(BorderFactory.createLineBorder(Color.RED, 1));
            } else {
                t.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1));
            }
        }
        if (obj instanceof DatePicker) {
            DatePicker dt = (DatePicker) obj;
            if (dt.getDate() == null) {
                mensajes.append(CAMPO).append(dt.getToolTipText()).append(REQUERIDO).append(SALTO_LINEA);
                dt.setBorder(BorderFactory.createLineBorder(Color.RED, 1));
            } else {
                dt.setBorder(BorderFactory.createLineBorder(Color.LIGHT_GRAY, 1));
            }
        }
        if (obj instanceof JLabel) {
            JLabel t = (JLabel) obj;
            if (t.getText() == null || t.getText().trim().isEmpty() || t.getText().trim().equals("-")) {
                mensajes.append(CAMPO).append(t.getToolTipText()).append(REQUERIDO).append(SALTO_LINEA);
            }
        }
        StringBuilder mensajes1 = validarTextosAreas(obj);
        mensajes.append(mensajes1 == null ? "" : mensajes1);
        if (!mensajes.toString().isEmpty()) {
            return mensajes;
        } else {
            return null;
        }
    }

    /**
     *
     * <b>
     * Incluir aqui­ la descripcion del metodo.
     * </b>
     * <p>
     * [$Author: David Murillo $, $Date: 01/07/2015]</p>
     */
    private static StringBuilder validarTextosAreas(Object obj) {
        StringBuilder mensajes = new StringBuilder(SIN_ESPACIO);
        if (obj instanceof JTextArea) {
            CajaTextoAreaIess t = (CajaTextoAreaIess) obj;
            if (t.getText() == null || t.getText().trim().isEmpty()) {
                mensajes.append(CAMPO).append(t.getToolTipText()).append(REQUERIDO).append(SALTO_LINEA);
                t.setearBordeCajaRequerida();
            } else {
                t.setearBordeCajaNormal();
            }
        }
        if (!mensajes.toString().isEmpty()) {
            return mensajes;
        } else {
            return null;
        }
    }

    /**
     *
     * <b>
     * Incluir aqui­ la descripcion del metodo.
     * </b>
     * <p>
     * [$Author: David Murillo $, $Date: 01/07/2015]</p>
     */
    private static StringBuilder validarCombos(Object obj) {
        StringBuilder mensajes = new StringBuilder(SIN_ESPACIO);
        if (obj instanceof JComboBox) {
            try {
                ComboIess t = (ComboIess) obj;
                if(t.getSelectedItem() == null || 
                   doesObjectContainField(t.getSelectedItem(), "isNoSelectable")){
                    mensajes.append(CAMPO).append(t.getToolTipText()).append(REQUERIDO).append(SALTO_LINEA);
                    t.setearBordeComboRequerido();
                }  else {
                    t.setearBordeComboNormal();
                }
            } catch (Exception e) {
                ComboMspAdmision t = (ComboMspAdmision) obj;
                if(t.getSelectedItem() == null || 
                   doesObjectContainField(t.getSelectedItem(), "isNoSelectable")){
                    mensajes.append(CAMPO).append(t.getToolTipText()).append(REQUERIDO).append(SALTO_LINEA);
                    t.setearBordeComboRequerido();
                }  else {
                    t.setearBordeComboNormal();
                }
            }
        }
        if (!mensajes.toString().isEmpty()) {
            return mensajes;
        } else {
            return null;
        }
    }
    
    /**
     *
     * <b>
     * Incluir aqui­ la descripcion del metodo.
     * </b>
     * <p>
     * [$Author: David Murillo $, $Date: 01/07/2015]</p>
     */
    private static StringBuilder validarGroupButtonRadio(Object obj) {
        StringBuilder mensajes = new StringBuilder(SIN_ESPACIO);
        if (obj instanceof ButtonGroup) {
            boolean validar = false;
            for (Enumeration<AbstractButton> buttons = ((ButtonGroup) obj).getElements(); buttons.hasMoreElements();) {
               AbstractButton button = buttons.nextElement();
               if(button.isSelected()){
                  validar = true;
               }
            }
            if(!validar) {
               mensajes.append(CAMPO).append(((ButtonGroup) obj).getElements().nextElement().getToolTipText() != null ? ((ButtonGroup) obj).getElements().nextElement().getToolTipText() : "vacio").append(REQUERIDO).append(SALTO_LINEA);
            }
        }
        if (!mensajes.toString().isEmpty()) {
            return mensajes;
        } else {
            return null;
        }
    }
    /**
     *
     * <b>
     * Incluir aqui­ la descripcion del metodo.
     * </b>
     * <p>
     * [$Author: David Murillo $, $Date: 01/07/2015]</p>
     *
     * @param ventana
     * @param tipo
     * @param mensaje
     */
    public static void muestraMensaje(String tituloVentana, Component ventana, String tipo, String mensaje) {
        if ((tituloVentana == null || tituloVentana.isEmpty()) && tipo.toUpperCase().equals(TipoMensaje.ERROR.getNemonico())) {
            tituloVentana = "Error";
        } else if ((tituloVentana == null || tituloVentana.isEmpty()) && tipo.toUpperCase().equals(TipoMensaje.INFO.getNemonico())) {
            tituloVentana = "Información";
        }
        JOptionPane.showMessageDialog(ventana, mensaje, tituloVentana, JOptionPane.INFORMATION_MESSAGE );
    }

    /**
     *
     * <b>
     * Incluir aqui­ la descripcion del metodo.
     * </b>
     * <p>
     * [$Author: David Murillo $, $Date: 01/07/2015]</p>
     *
     * @param fechaSistema
     * @return
     */
    public static String devuelveFechaLetras(Date fechaSistema) {
        Calendar fecha = Calendar.getInstance();
        fecha.setTime(fechaSistema);
        return FechasUtil.devuelveFechaEnLetrasSinHora(fechaSistema);
    }

    /**
     *
     * <b>
     * Incluir aqui­ la descripcion del metodo.
     * </b>
     * <p>
     * [$Author: David Murillo $, $Date: 01/07/2015]</p>
     */
    private static String devuelveMes(int mes) {
        switch (mes) {
            case 0:
                return "Enero";
            case 1:
                return "Febrero";
            case 2:
                return "Marzo";
            case 3:
                return "Abril";
            case 4:
                return "Mayo";
            case 5:
                return "Junio";
            case 6:
                return "Julio";
            case 7:
                return "Agosto";
            case 8:
                return "Septiembre";
            case 9:
                return "Octubre";
            case 10:
                return "Noviembre";
            case 11:
                return "Diciembre";
            default:
                return "";
        }
    }

    /**
     *
     * <b>
     * Incluir aqui­ la descripcion del metodo.
     * </b>
     * <p>
     * [$Author: David Murillo $, $Date: 01/07/2015]</p>
     *
     * @param urlImagen
     * @param tamanio
     * @return
     */
    public static ImageIcon ajustarImagen(URL urlImagen, int tamanio) {
        ImageIcon imageIcon = new ImageIcon(urlImagen);
        Image image = imageIcon.getImage();
        ImageIcon iconoAjustado = new ImageIcon(image.getScaledInstance(tamanio, tamanio, Image.SCALE_SMOOTH));
        return iconoAjustado;
    }

    public static ImageIcon ajustarImagen(URL urlImagen, int ancho, int largo) {
        ImageIcon imageIcon = new ImageIcon(urlImagen);
        Image image = imageIcon.getImage();
        ImageIcon iconoAjustado = new ImageIcon(image.getScaledInstance(ancho, largo, Image.SCALE_SMOOTH));
        return iconoAjustado;
    }

    /**
     *
     * <b>
     * Incluir aqui­ la descripcion del metodo.
     * </b>
     * <p>
     * [$Author: David Murillo $, $Date: 01/07/2015]</p>
     *
     * @param image
     * @param tamanio
     * @return
     */
    public static ImageIcon ajustarImagen(ImageIcon image, int tamanio) {
        ImageIcon imageIcon = image;
        Image image1 = imageIcon.getImage();
        ImageIcon iconoAjustado = new ImageIcon(image1.getScaledInstance(tamanio, tamanio, Image.SCALE_SMOOTH));
        return iconoAjustado;
    }

    /**
     *
     * <b>
     * Incluir aqui­ la descripcion del metodo.
     * </b>
     * <p>
     * [$Author: David Murillo $, $Date: 01/07/2015]</p>
     *
     * @param evt
     */
    public static void soloNumeros(KeyEvent evt) {
        char codigo = evt.getKeyChar();
        if ((codigo < '0' || codigo > '9') && (codigo != '\b')) {
            evt.consume();
        }
    }

    /**
     *
     * <b>
     * Incluir aqui­ la descripcion del metodo.
     * </b>
     * <p>
     * [$Author: David Murillo $, $Date: 01/07/2015]</p>
     *
     * @param evt
     * @param lugaresDecimales cantidad de decimales luego del punto
     */
    public static void soloNumerosDecimales(KeyEvent evt, int lugaresDecimales) {
        JTextComponent textComponent = (JTextComponent) evt.getComponent();
        int integerPlaces = textComponent.getText().indexOf('.');
        int decimalPlaces = integerPlaces == -1 ? 0 : textComponent.getText().length() - integerPlaces - 1;
        char codigo = evt.getKeyChar();
        if (textComponent.getText().isEmpty()) {
            if ((codigo < '0' || codigo > '9') && (codigo != '\b')) {
                evt.consume();
            }
        } else {
            if (codigo == '.' && integerPlaces != -1) {
                evt.consume();
            } else if ((codigo < '0' || codigo > '9') && (codigo != '\b') && (codigo != '.')
                    || (decimalPlaces >= lugaresDecimales)) {
                evt.consume();
            }
        }
    }

    /**
     *
     * <b>
     * Incluir aqui­ la descripcion del metodo.
     * </b>
     * <p>
     * [$Author: David Murillo $, $Date: 01/07/2015]</p>
     *
     * @param evt
     */
    public static void aMayusculas(KeyEvent evt) {
        Character c = evt.getKeyChar();
        if(Character.isLetter(c)) {
            evt.setKeyChar(Character.toUpperCase(c));
        }
    }
    /**
     *
     * <b>
     * Incluir aqui­ la descripcion del metodo.
     * </b>
     * <p>
     * [$Author: David Murillo $, $Date: 01/07/2015]</p>
     *
     * @param evt
     */
    public static void soloLetras(KeyEvent evt) {
        char codigo = evt.getKeyChar();
        if (!StringUtils.isAlphaSpace(String.valueOf(codigo))) {
            evt.consume();
        }
    }
    
    /**
     *
     * <b>
     * Incluir aqui­ la descripcion del metodo.
     * </b>
     * <p>
     * [$Author: David Murillo $, $Date: 01/07/2015]</p>
     *
     * @param evt
     */
    public static void textoSinCaracteresEspeciales(KeyEvent evt) {
        char codigo = evt.getKeyChar();
        if(Character.isLetter(codigo)) {
            evt.setKeyChar(Character.toUpperCase(codigo));
        }
        if (!StringUtils.isAlphanumericSpace(String.valueOf(codigo))) {
            evt.consume();
        }
    }

    public static void validarLongitudCampo(Object obj, KeyEvent evt, int longitudCampo) {
        int tamanio = 0;
        JTextField caja = null;
        JTextArea area = null;
        if (obj instanceof JTextField) {
            caja = (JTextField) obj;
            tamanio = caja.getText().length();
        }
        if (obj instanceof JTextArea) {
            area = (JTextArea) obj;
            tamanio = area.getText().length();
        }
        if (tamanio == longitudCampo) {
            evt.consume();
        }
        if (tamanio > longitudCampo) {
            if (caja != null) {
                caja.setText("");
            }
            if (area != null) {
                area.setText("");
            }
        }
    }

    public static void validarLongitudCampo(KeyEvent evt, int longitudCampo) {
        int tamanio = 0;
        Object obj = evt.getComponent();
        JTextField caja = null;
        JTextArea area = null;
        if (obj instanceof JTextField) {
            caja = (JTextField) obj;
            tamanio = caja.getText().length();
        }
        if (obj instanceof JTextArea) {
            area = (JTextArea) obj;
            tamanio = area.getText().length();
        }
        if (tamanio == longitudCampo) {
            evt.consume();
        }
        if (tamanio > longitudCampo) {
            if (caja != null) {
                caja.setText("");
            }
            if (area != null) {
                area.setText("");
            }
        }
    }
    
    public static boolean validarValorMinimoMaximo(FocusEvent evt, double valorMinimo, double valorMaximo) {
        double valor = 0;
        Object obj = evt.getComponent();
        if (obj instanceof JTextComponent) {
            JTextComponent cajaInst = (JTextComponent) obj;
            try {
                valor = Double.parseDouble(cajaInst.getText());
            } catch (NumberFormatException e) {
                valor = 0d;
            }
        }
    	if (obj instanceof CajaTextoIess) {
    		((CajaTextoIess)obj).setearBordeCajaNormal();
    	}
    	if (obj instanceof CajaTextoAreaIess) {
    		((CajaTextoAreaIess)obj).setearBordeCajaNormal();
    	}
        if (valor < valorMinimo) {
            setValueOnError(obj);
            return false;
        }
        if (valor > valorMaximo) {
            setValueOnError(obj);
            return false;
        }
        return true;
    }

    private static void setValueOnError(Object obj) {
        if (obj instanceof CajaTextoIess) {
            ((CajaTextoIess)obj).setText("");
            ((CajaTextoIess)obj).setearBordeCajaRequerida();
        }
        if (obj instanceof CajaTextoAreaIess) {
            ((CajaTextoAreaIess)obj).setText("");
            ((CajaTextoAreaIess)obj).setearBordeCajaRequerida();
        }
    }

    /**
     *
     * <b>
     * Incluir aqui­ la descripcion del metodo.
     * </b>
     * <p>
     * [$Author: David Murillo $, $Date: 01/07/2015]</p>
     *
     * @param lista
     * @return
     */
    public static DefaultComboBoxModel cargarCombo(List lista) {
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        modelo.addElement(SELECCIONE);
        for (Object o : lista) {
            modelo.addElement(o);

        }
        return modelo;
    }

    public static DefaultListModel cargarJlista(List lista) {
        DefaultListModel modelo = new DefaultListModel();
        for (Object o : lista) {
            modelo.addElement(o);
        }
        return modelo;
    }

    /**
     *
     * <b>
     * Incluir aqui­ la descripcion del metodo.
     * </b>
     * <p>
     * [$Author: fquisiguina $, $Date: 01/07/2015]</p>
     *
     * @param lista
     * @return
     */
    public static DefaultComboBoxModel cargarComboSinSeleccione(List lista) {
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        for (Object o : lista) {
            modelo.addElement(o);

        }
        return modelo;
    }

    /**
     *
     * <b>
     * Incluir aqui­ la descripcion del metodo.
     * </b>
     * <p>
     * [$Author: David Murillo $, $Date: 01/07/2015]</p>
     *
     * @param lista
     * @return
     */
    public static DefaultComboBoxModel cargaLista(List lista) {
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        for (Object o : lista) {
            modelo.addElement(o);

        }
        return modelo;
    }

    public static DefaultComboBoxModel cargarComboVacio() {
        DefaultComboBoxModel modelo = new DefaultComboBoxModel();
        modelo.addElement(SELECCIONE);
        return modelo;
    }

   

    public static boolean confirmacion(Component ventana, String mensaje) {
        int opcion = JOptionPane.showConfirmDialog(ventana, mensaje, "Confirmación", JOptionPane.YES_NO_OPTION);
        return opcion == 0;
    }

    public static String devolverDescripcionMes(int mes) {
        String retorno = "";
        switch (mes) {
            case 1:
                retorno = "ENERO";
                break;
            case 2:
                retorno = "FEBRERO";
                break;
            case 3:
                retorno = "MARZO";
                break;
            case 4:
                retorno = "ABRIL";
                break;
            case 5:
                retorno = "MAYO";
                break;
            case 6:
                retorno = "JUNIO";
                break;
            case 7:
                retorno = "JULIO";
                break;
            case 8:
                retorno = "AGOSTO";
                break;
            case 9:
                retorno = "SEPTIEMBRE";
                break;
            case 10:
                retorno = "OCTUBRE";
                break;
            case 11:
                retorno = "NOVIEMBRE";
                break;
            case 12:
                retorno = "DICIEMBRE";
                break;
        }
        return retorno;
    }
    
    public static String devolverDescripcionEstado(int estado) {
        String retorno = "";
        switch (estado) {
            case 0:
                retorno = "CERRADO";
                break;
            case 1:
                retorno = "ABIERTO";
                break;
        }
        return retorno;
    }

    public static void quitarBarraTitulo(JInternalFrame internalFrame) {
        ((BasicInternalFrameUI) internalFrame.getUI()).setNorthPane(null);
        internalFrame.setBorder(null);

    }

    public static String devolverSistemaOperativo() {
        return System.getProperty("os.name").toUpperCase();
    }

    public static void transformarSpinnerHora(JSpinner control) {
        JSpinner.DateEditor timeEditor = new JSpinner.DateEditor(control, "HH:mm");
        control.setEditor(timeEditor);
        ((JSpinner.DateEditor) control.getEditor()).getTextField().setEditable(false);
    }

    public static final byte[] convertirFileaBytes(File file) {
        try (FileInputStream fis = new FileInputStream(file);
        	ByteArrayOutputStream bos = new ByteArrayOutputStream();) {
            byte[] buf = new byte[1024];
            for (int readNum; (readNum = fis.read(buf)) != -1;) {
                bos.write(buf, 0, readNum); //no doubt here is 0
            }
            return bos.toByteArray();
        } catch (IOException e) {
            return null;
        }
    }

    public static void cambiarColorSeleccionado(JCheckBox checkBox) {
        checkBox.setFont(new Font("Open Sans", Font.BOLD, 12));
        checkBox.setForeground(new Color(0, 153, 255));
    }

    public static void cambiarColorDesSeleccionado(JCheckBox checkBox) {
        checkBox.setFont(new Font("Open Sans", Font.PLAIN, 12));
        checkBox.setForeground(new Color(100, 100, 100));
    }
    
    public static void setEnableContainer(Container c, boolean band) {    
        Component[] components = c.getComponents();
        c.setEnabled(band);
        for (Component component : components) {
            component.setEnabled(band);
            if (component instanceof Container) {
                setEnableContainer((Container) component, band);
            }
        }
    }
    
    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();
        return BigDecimal.valueOf(value).setScale(places, RoundingMode.HALF_UP).doubleValue();
    }
    
    public static void stateControlGroupButton(Boolean state, ButtonGroup groupButton) {
        try {
            Enumeration<AbstractButton> enumeration = groupButton.getElements();
            while (enumeration.hasMoreElements()) {
                enumeration.nextElement().setEnabled(state);
            }
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }
    }

}
