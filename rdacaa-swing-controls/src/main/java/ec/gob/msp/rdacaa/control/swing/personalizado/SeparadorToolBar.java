/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.control.swing.personalizado;

import javax.swing.JLabel;

/**
 *
 * @author jtarapuez
 */
public class SeparadorToolBar extends JLabel {

    private static final long serialVersionUID = -3663420351251053604L;

    public SeparadorToolBar() {

        setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/separador_toolbar.png"))); // NOI18N
        setPreferredSize(new java.awt.Dimension(41, 3));
        setHorizontalAlignment(javax.swing.SwingConstants.CENTER);

    }

}
