package ec.gob.msp.rdacaa.utilitario.forma;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JFormattedTextField;
import javax.swing.JTable;
import javax.swing.table.TableCellRenderer;

public class FormatoTabla implements TableCellRenderer {

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        JFormattedTextField campoTexto = new JFormattedTextField();
        for (int i = 0; i < table.getColumnCount(); i++) {
            table.getColumnModel().getColumn(i).setHeaderRenderer(new PintarTabla());
        }
        campoTexto.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(238, 238, 238), 1, true));
        campoTexto.setBorder(null);
        campoTexto.setFont(new java.awt.Font("Open Sans", 0, 12));
        campoTexto.setForeground(new java.awt.Color(125, 125, 125));
        campoTexto.setText("  " + value);
        if (row % 2 == 0) {
            campoTexto.setBackground(Color.WHITE);
        } else {
            campoTexto.setBackground(new Color(250, 250, 250));
        }

        if (isSelected) {
            campoTexto.setBackground(new Color(3,169,244));
            campoTexto.setForeground(Color.WHITE);
        }
        return campoTexto;
    }
}
