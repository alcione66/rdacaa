/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.control.swing.personalizado;

import javax.swing.JLabel;

/**
 *
 * @author jtarapuez
 */
public class LabelMensaje extends JLabel {

    private static final long serialVersionUID = -8264954046309807366L;

    public LabelMensaje() {
        setFont(new java.awt.Font("Open Sans", 1, 14));
        setText("");
    }
}
