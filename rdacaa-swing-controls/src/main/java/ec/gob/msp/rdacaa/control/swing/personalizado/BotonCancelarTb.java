package ec.gob.msp.rdacaa.control.swing.personalizado;

import javax.swing.JButton;

/**
 *
 * @author jtarapuez
 */
public class BotonCancelarTb extends JButton {

	private static final long serialVersionUID = -6315538026427339072L;

	public BotonCancelarTb() {
		setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/bot_cancelar_off.png"))); // NOI18N
		setBorder(null);
		setBorderPainted(false);
		setContentAreaFilled(false);
		setToolTipText("Cancelar");
		setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
		setPreferredSize(new java.awt.Dimension(148, 43));
		setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/bot_cancelar_on.png"))); // NOI18N
	}

}
