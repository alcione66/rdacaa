/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.control.swing.personalizado;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Insets;
import javax.swing.JPasswordField;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;

/**
 *
 * @author jtarapuez
 */
public class CajaPasswordLoginIess extends JPasswordField {

    private static final long serialVersionUID = -1434511835262265466L;

    private Dimension d = new Dimension(270, 50);
    private String placeholder = "";
    private Color phColor = new java.awt.Color(255, 255, 255);
    private boolean band = true;

    public CajaPasswordLoginIess() {
        super();
        setSize(d);
        setPreferredSize(d);
        setVisible(true);
        setMargin(new Insets(3, 6, 3, 6));
        setFont(new java.awt.Font("Open Sans", 0, 14));
        setForeground(new java.awt.Color(255, 255, 255));
        setOpaque(false);
        getDocument().addDocumentListener(new DocumentListener() {
            @Override
            public void removeUpdate(DocumentEvent e) {
                band = (getPassword().length > 0) ? false : true;
            }

            @Override
            public void insertUpdate(DocumentEvent e) {
                band = false;
            }

            @Override
            public void changedUpdate(DocumentEvent de) {
            }
        });
    }

    public void setPlaceholder(String placeholder) {
        this.placeholder = placeholder;
    }

    public String getPlaceholder() {
        return placeholder;
    }

    public Color getPhColor() {
        return phColor;
    }

    public void setPhColor(Color phColor) {
        this.phColor = phColor;
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        g.drawString((band) ? placeholder : "",
                getMargin().left,
                (getSize().height) / 2 + getFont().getSize() / 2);
    }
}
