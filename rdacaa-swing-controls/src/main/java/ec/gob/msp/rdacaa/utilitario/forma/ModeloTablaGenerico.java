package ec.gob.msp.rdacaa.utilitario.forma;

import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.util.ArrayList;
import java.util.List;

import javax.swing.DefaultCellEditor;
import javax.swing.JComboBox;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.table.AbstractTableModel;

import lombok.Getter;

public abstract class ModeloTablaGenerico<T> extends AbstractTableModel {

	private static final long serialVersionUID = 6140701330257202761L;

	@Getter
	private String[] columnas;

	@Getter
	private List<T> listaDatos;

	public ModeloTablaGenerico(String[] columnas) {
		this.columnas = columnas;
		this.listaDatos = new ArrayList<>();
	}

	public void addFila(List<T> listaDatos) {
		for (T element : listaDatos) {
			this.listaDatos.add(element);
		}
		fireTableDataChanged();
	}

	@Override
	public int getRowCount() {
		return this.getListaDatos().size();
	}

	@Override
	public int getColumnCount() {
		return getColumnas().length;
	}

	public void ocultaColumnaCodigo(JTable tblDatos) {
		tblDatos.getColumnModel().getColumn(0).setWidth(0);
		tblDatos.getColumnModel().getColumn(0).setMaxWidth(0);
		tblDatos.getColumnModel().getColumn(0).setMinWidth(0);
		tblDatos.getColumnModel().getColumn(0).setPreferredWidth(0);
		tblDatos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	}

	public void ocultaColumnaCodigoCheck(JTable tblDatos) {
		tblDatos.getColumnModel().getColumn(0).setWidth(0);
		tblDatos.getColumnModel().getColumn(0).setMaxWidth(0);
		tblDatos.getColumnModel().getColumn(0).setMinWidth(0);
		tblDatos.getColumnModel().getColumn(0).setPreferredWidth(0);
		tblDatos.getColumnModel().getColumn(2).setCellEditor(new DefaultCellEditor(new JComboBox()));
		tblDatos.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
	}

	@Override
	public String getColumnName(int col) {
		return getColumnas()[col];
	}

	private void resizeColumns(JTable tblDatos, int[] percentages) {
		for (int i = 1; i < tblDatos.getColumnModel().getColumnCount(); i++) {
			tblDatos.getColumnModel().getColumn(i).setPreferredWidth(percentages[i - 1]);
		}
	}

	public void definirAnchoColumnas(final JTable tblDatos, final int[] percentages) {
                resizeColumns(tblDatos, percentages);
		tblDatos.addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				resizeColumns(tblDatos, percentages);
			}
		});
	}

	public void clear() {
		listaDatos.clear();
		fireTableDataChanged();
	}
	
}
