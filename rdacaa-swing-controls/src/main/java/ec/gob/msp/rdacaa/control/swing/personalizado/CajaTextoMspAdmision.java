/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.control.swing.personalizado;

import java.awt.Dimension;
import java.awt.Insets;

import javax.swing.JTextField;

/**
 *
 * @author jtarapuez
 */
public class CajaTextoMspAdmision extends JTextField {

    private static final long serialVersionUID = 4232356196634495614L;

    private Dimension d = new Dimension(250, 30);

    public CajaTextoMspAdmision() {
        super();
        setSize(d);
        setPreferredSize(d);
        setVisible(true);
        setMargin(new Insets(3, 6, 3, 6));
        setFont(new java.awt.Font("Open Sans", 0, 12));
        setOpaque(false);
        setBorder(new RedondoCaja());

    }
    
    public void setearBordeCajaNormal() {
        setBorder(new RedondoCaja());
    }

    public void setearBordeCajaRequerida() {
        setBorder(new RedondoCajaRoja());
    }

}
