package ec.gob.msp.rdacaa.control.swing.personalizado;

import javax.swing.JButton;

/**
 *
 * @author jtarapuez
 */
public class BotonAceptar extends JButton {

	private static final long serialVersionUID = 5551078721003915646L;

	public BotonAceptar() {
    
    setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/bot_aceptar1_off.png"))); // NOI18N
      setBorder(null);
        setBorderPainted(false);
        setContentAreaFilled(false);
       setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
     setPreferredSize(new java.awt.Dimension(94, 29));
        setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/bot_aceptar1_on.png"))); // NOI18N
        
    
    }
    
 


}
