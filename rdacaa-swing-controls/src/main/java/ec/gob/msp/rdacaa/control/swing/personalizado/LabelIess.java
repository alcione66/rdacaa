/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.control.swing.personalizado;

import javax.swing.JLabel;
import javax.swing.border.EmptyBorder;

/**
 *
 * @author jtarapuez
 */
public class LabelIess extends JLabel {

    private static final long serialVersionUID = -3007167790737156611L;

    public LabelIess() {
        setFont(new java.awt.Font("Open Sans", 0, 12)); // NOI18N
        setForeground(new java.awt.Color(100, 100, 100));
        setBorder(new EmptyBorder(1, 1, 1, 1));
    }

    public LabelIess(String nombre) {
        setIcon(new javax.swing.ImageIcon(getClass().getResource(nombre)));

    }
    
    public LabelIess(int size) {
        setFont(new java.awt.Font("Open Sans", 0, size)); // NOI18N
        setForeground(new java.awt.Color(100, 100, 100));
        setBorder(new EmptyBorder(1, 1, 1, 1));
    }
}
