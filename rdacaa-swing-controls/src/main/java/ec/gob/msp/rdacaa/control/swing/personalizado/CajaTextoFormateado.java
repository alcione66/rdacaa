/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.control.swing.personalizado;

import java.awt.Color;
import java.awt.Insets;
import java.text.Format;

import javax.swing.JFormattedTextField;

/**
 *
 * @author msp
 */
public class CajaTextoFormateado extends JFormattedTextField{

	private static final long serialVersionUID = -8722685716719405579L;

	public CajaTextoFormateado() {
         super();
         lafInit();
    }

    public CajaTextoFormateado(Object value) {
        super(value);
        lafInit();
    }

    public CajaTextoFormateado(Format format) {
        super(format);
        lafInit();
    }

    public CajaTextoFormateado(AbstractFormatter formatter) {
        super(formatter);
        lafInit();
    }

    public CajaTextoFormateado(AbstractFormatterFactory factory) {
        super(factory);
        lafInit();
    }

    public CajaTextoFormateado(AbstractFormatterFactory factory, Object currentValue) {
        super(factory, currentValue);
        lafInit();
    }

    private void lafInit(){
        setVisible(true);
        setMargin(new Insets(3, 6, 3, 6));
        setFont(new java.awt.Font("Open Sans", 0, 12));
        setOpaque(true);
        setBorder(new RedondoCaja());
        setBackground(Color.WHITE);
    }
    
}
