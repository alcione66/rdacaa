/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.control.swing.personalizado;

import javax.swing.JButton;

/**
 *
 * @author jtarapuez
 */
public class BotonVisualizarDerivaciones extends JButton {

    private static final long serialVersionUID = 2540207739547869947L;

    public BotonVisualizarDerivaciones() {

        setIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/bot_derivaciones_off.png"))); // NOI18N
        setBorder(null);
        setBorderPainted(false);
        setContentAreaFilled(false);
        //setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        setPreferredSize(new java.awt.Dimension(100, 29));
        setRolloverIcon(new javax.swing.ImageIcon(getClass().getResource("/imagenes/botones/bot_derivaciones_on.png"))); // NOI18N

    }

}
