/*
* Copyright 2016 INSTITUTO ECUATORIANO DE SEGURIDAD SOCIAL - ECUADOR
* Todos los derechos reservados
 */
package ec.gob.msp.rdacaa.control.swing.personalizado;

import javax.swing.JFileChooser;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * <b>
 * Descripción de clase.
 * </b>
 *
 * @author christian
 * <p>
 * [$Author: christian $, $Date: 28/10/2016</p>
 */
public class SelectorMultipleArchivo extends JFileChooser {

    private static final long serialVersionUID = 2384488319713402491L;

    public SelectorMultipleArchivo() {
    }

    public SelectorMultipleArchivo(String titulo, String... extensiones) {
        FileNameExtensionFilter filtro = new FileNameExtensionFilter(titulo, extensiones);
        setFileFilter(filtro);
        setMultiSelectionEnabled(true);
    }

}
