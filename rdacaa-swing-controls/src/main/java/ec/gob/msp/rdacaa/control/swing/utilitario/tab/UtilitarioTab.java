/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.control.swing.utilitario.tab;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.GradientPaint;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Polygon;
import java.awt.Rectangle;
import javax.swing.JComponent;
import static javax.swing.SwingConstants.BOTTOM;
import static javax.swing.SwingConstants.LEFT;
import static javax.swing.SwingConstants.RIGHT;
import static javax.swing.SwingConstants.TOP;
import javax.swing.UIManager;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.basic.BasicTabbedPaneUI;
import javax.swing.text.View;

/**
 *
 * @author christian
 */
public class UtilitarioTab extends BasicTabbedPaneUI {

    private Color selectColor;
    private Color deSelectColor;
    private final int inclTab = 4;
    private final int anchoFocoH = 4;
    private Polygon shape;

    public static ComponentUI createUI(JComponent c) {
        return new UtilitarioTab();
    }

    @Override
    protected void installDefaults() {
        super.installDefaults();
        selectColor = new Color(250, 192, 192);
        deSelectColor = new Color(197, 193, 168);
        //tabAreaInsets.right = anchoCarpetas;
    }

    @Override
    protected void paintTabBackground(Graphics g, int tabPlacement, int tabIndex, int x, int y, int w, int h, boolean isSelected) {
        Graphics2D g2D = (Graphics2D) g;
        GradientPaint gradientShadow;
        int xp[] = null; // Para la forma
        int yp[] = null;
        switch (tabPlacement) {
            case LEFT:
                xp = new int[]{x, x, x + w, x + w, x};
                yp = new int[]{y, y + h - 3, y + h - 3, y, y};
                gradientShadow = new GradientPaint(x, y, new Color(100, 100, 255), x, y + h, Color.ORANGE);
                break;
            case RIGHT:
                xp = new int[]{x, x, x + w - 2, x + w - 2, x};
                yp = new int[]{y, y + h - 3, y + h - 3, y, y};
                gradientShadow = new GradientPaint(x, y, new Color(100, 100, 255), x, y + h, new Color(153, 186, 243));
                break;
            case BOTTOM:
                xp = new int[]{x, x, x + 3, x + w - inclTab - 6, x + w - inclTab - 2, x + w - inclTab, x + w - 3, x};
                yp = new int[]{y, y + h - 3, y + h, y + h, y + h - 1, y + h - 3, y, y};
                gradientShadow = new GradientPaint(x, y, new Color(100, 100, 255), x, y + h, Color.BLUE);
                break;
            case TOP:
            default:
                xp = new int[]{x, x, x + 3, x + w - inclTab - 3, x + w - inclTab - 2, x + w - inclTab, x + w - inclTab, x};
                yp = new int[]{y + h, y + 3, y, y, y + 1, y + 3, y + h, y + h};
                gradientShadow = new GradientPaint(0, 0, new Color(214, 215, 216), 0, y + h / 2, new Color(214, 215, 216));
                break;
        }
        shape = new Polygon(xp, yp, xp.length);
        if (isSelected) {
            g2D.setColor(selectColor);
            g2D.setPaint(gradientShadow);
        } else if (tabPane.isEnabled() && tabPane.isEnabledAt(tabIndex)) {
            g2D.setColor(deSelectColor);
            GradientPaint gradientShadowTmp = new GradientPaint(0, 0, new Color(246, 247, 249), 0, y + h / 2, new Color(246, 247, 249));
            g2D.setPaint(gradientShadowTmp);
        } else {
            GradientPaint gradientShadowTmp = new GradientPaint(0, 0, new Color(246, 247, 249), 0, y + 15 + h / 2, new Color(246, 247, 249));
            g2D.setPaint(gradientShadowTmp);
        }
        g2D.fill(shape);
        if (runCount > 1) {
            g2D.setColor(hazAlfa(getRunForTab(tabPane.getTabCount(), tabIndex) - 1));
            g2D.fill(shape);
        }
        g2D.fill(shape);
    }

    @Override
    protected void paintText(Graphics g, int tabPlacement, Font font, FontMetrics metrics, int tabIndex, String title, Rectangle textRect, boolean isSelected) {
        super.paintText(g, tabPlacement, font, metrics, tabIndex, title, textRect, isSelected);
        g.setFont(font);
        View v = getTextViewForTab(tabIndex);
        if (v != null) {
            v.paint(g, textRect);
        } else if (tabPane.isEnabled() && tabPane.isEnabledAt(tabIndex)) {
            g.setColor(tabPane.getForegroundAt(tabIndex));
        } else {
            g.setColor(Color.BLACK);
        }
    }

    @Override
    protected int calculateTabWidth(int tabPlacement, int tabIndex, FontMetrics metrics) {
        return 20 + inclTab + super.calculateTabWidth(tabPlacement, tabIndex, metrics);
    }

    @Override
    protected int calculateTabHeight(int tabPlacement, int tabIndex, int fontHeight) {
        if (tabPlacement == LEFT || tabPlacement == RIGHT) {
            return super.calculateTabHeight(tabPlacement, tabIndex, fontHeight);
        } else {
            return anchoFocoH + super.calculateTabHeight(tabPlacement, tabIndex, fontHeight);
        }
    }

    @Override
    protected void paintTabBorder(Graphics g, int tabPlacement, int tabIndex, int x, int y, int w, int h, boolean isSelected) {

    }

    @Override
    protected void paintFocusIndicator(Graphics g, int tabPlacement, Rectangle[] rects, int tabIndex, Rectangle iconRect, Rectangle textRect, boolean isSelected) {
        if (tabPane.hasFocus() && isSelected) {
            g.setColor(UIManager.getColor("ScrollBar.thumbShadow"));
            g.drawPolygon(shape);
        }
    }

    protected Color hazAlfa(int fila) {
        int alfa = 0;
        if (fila >= 0) {
            alfa = 50 + (fila > 7 ? 70 : 10 * fila);
        }
        return new Color(0, 0, 0, alfa);
    }

    protected void paintContentBorderTopEdge(Graphics g, int tabPlacement,
            int selectedIndex, int x, int y, int w, int h) {

        g.setColor(Color.WHITE);
        if (tabPlacement != TOP
                || selectedIndex < 0
                || (rects[selectedIndex].y + rects[selectedIndex].height + 1 < y)) {
            g.drawLine(x, y, x + w - 2, y);
        } else {
            Rectangle selRect = rects[selectedIndex];
            g.drawLine(x, y, selRect.x + 1, y);
            if (selRect.x + selRect.width < x + w - 2) {
                g.drawLine(selRect.x + selRect.width, y, x + w - 2, y);
            } else {
                g.setColor(shadow);
                g.drawLine(x + w - 2, y, x + w - 2, y);
            }
        }
    }

    protected void paintContentBorderBottomEdge(Graphics g, int tabPlacement,
            int selectedIndex, int x, int y, int w, int h) {
        g.setColor(darkShadow);
        if (tabPlacement != BOTTOM || selectedIndex < 0
                || (rects[selectedIndex].y - 1 > h)) {
            g.drawLine(x, y + h - 1, x + w - 1, y + h - 1);
        } else {
            Rectangle selRect = rects[selectedIndex];
            g.drawLine(x, y + h - 1, selRect.x, y + h - 1);
            if (selRect.x + selRect.width < x + w - 2) {
                g.drawLine(selRect.x + selRect.width, y + h - 1, x + w - 1, y
                        + h - 1);
            }
        }
    }

    protected void paintContentBorderRightEdge(Graphics g, int tabPlacement,
            int selectedIndex, int x, int y, int w, int h) {
        g.setColor(darkShadow);
        if (tabPlacement != RIGHT || selectedIndex < 0
                || (rects[selectedIndex].y - 1 > h)) {
            g.drawLine(x, y + h - 1, x + w - 1, y + h - 1);
        } else {
            Rectangle selRect = rects[selectedIndex];
            g.drawLine(x, y + h - 1, selRect.x, y + h - 1);
            if (selRect.x + selRect.width < x + w - 2) {
                g.drawLine(selRect.x + selRect.width, y + h - 1, x + w - 1, y
                        + h - 1);
            }
        }

    }
}
