/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.utilitario.forma;

import java.awt.Color;
import java.awt.Component;
import javax.swing.JTable;
import javax.swing.table.DefaultTableCellRenderer;

/**
 *
 * @author dmurillo
 */
public class PintarTabla extends DefaultTableCellRenderer {

    private static final long serialVersionUID = -2646812848873671495L;

    private Color backgroundColor;
    private Color foregroundColor;

    public PintarTabla() {
        super();
        this.backgroundColor = new Color(230, 233, 238);
        this.foregroundColor = new Color(101, 99, 99);
    }

    @Override
    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Component comp = super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        comp.setBackground(backgroundColor);
        comp.setForeground(foregroundColor);
        comp.setFont(new java.awt.Font("Open Sans", 1, 12));
        return comp;
    }
}
