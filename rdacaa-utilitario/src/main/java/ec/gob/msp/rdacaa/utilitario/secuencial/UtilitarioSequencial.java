/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.utilitario.secuencial;

import ec.gob.msp.rdacaa.utilitario.enumeracion.ComunEnum;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author miguel.faubla
 */
public class UtilitarioSequencial {
    
    public static String obtenerSequencialNoidentificado(String primerNombre, String segundoNombre, String primerApellido, String segundoApellido, Date fechaNacimiento, Integer paisId, String provinciaCod) throws Exception {
        
        try {
            String primernombre = primerNombre.substring(0, 2);
            String segundonombre = (segundoNombre != null) ? segundoNombre.trim().substring(0, 1) : "0";
            String primerapellido = primerApellido.substring(0, 2);
            String segundoapellido = (segundoApellido != null ) ? segundoApellido.trim().substring(0, 1) : "0";
            SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
            String fechanacimiento = format.format(fechaNacimiento);
            String codigoprovincia = "";

            String[] arrFecha = fechanacimiento.split("-");
            String anio = arrFecha[0];
            String mes = arrFecha[1];
            String dia = arrFecha[2];

            String control = anio.substring(2, 3);

            if(paisId == Integer.parseInt(ComunEnum.NACIONALIDAD_ECUATORIANA.getDescripcion())) {
                codigoprovincia = provinciaCod;
            } else {
                codigoprovincia = "99";
            } 

            String sequencialdatopaciente = primernombre + segundonombre + primerapellido + segundoapellido;
            String sequencialFecha = anio + mes + dia;

            String secuencialgenerado = sequencialdatopaciente + codigoprovincia + sequencialFecha + control;
            
            if(secuencialgenerado.length() != Integer.parseInt(ComunEnum.CANTIDAD_SEQUENCIAL_NOIDENTIFICADO.getDescripcion())) {
                throw new Exception("Se ha producido un error, no es posible generar los 17 dígitos del sequencial");
            }
            
            return secuencialgenerado;
            
        } catch (NullPointerException e) {
            throw new RuntimeException("Error al generar el sequencial de 17 dígitos. \\ \n" +e.getMessage());
        }
        
    }
    
}
