package ec.gob.msp.rdacaa.utilitario.sesion;



/**
 *
 * @author christian
 */
public enum TipoMensaje {

    ERROR("ERROR"), INFO("INFO");

    private String nemonico;

    private TipoMensaje(String nemonico) {
        this.nemonico = nemonico;
    }

	public String getNemonico() {
		return nemonico;
	}

}
