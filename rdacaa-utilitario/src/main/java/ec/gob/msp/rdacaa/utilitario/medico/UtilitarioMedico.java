/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.utilitario.medico;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.util.Date;
import java.util.Optional;
import java.util.stream.Stream;

import ec.gob.msp.rdacaa.utilitario.fecha.FechasUtil;

/**
 *
 * @author jazael.faubla
 */
public class UtilitarioMedico {

	private static final Object[][] FACTOR_CORRECION_ALTURA = { { 0.0d, 0, 999 }, { 0.2d, 1000, 1499 },
			{ 0.5d, 1500, 1999 }, { 0.8d, 2000, 2499 }, { 1.3d, 2500, 2999 }, { 1.9d, 3000, 3499 },
			{ 2.7d, 3500, 3999 }, { 3.5d, 4000, 4499 }, { 4.5d, 4500, 4999 }

	};

	public static Double calcularSemanasEmbarazo(Date fum, Date fechaAtencion) {

		Double numerodedias = FechasUtil.obtenerDiferenciaDias(fum, fechaAtencion);
		Double numerosemanasdecimal = (numerodedias / 7.1);
		Integer numerosemanas = (int) (numerodedias / 7.1);
		Double fraccion = (numerosemanasdecimal - numerosemanas);
		// Double dias = (fraccion > 0.6) ? 0.6 : fraccion;
		Double dias = (fraccion > 0.6) ? 1 : fraccion;
		Double semanas = (numerosemanas + dias);

		return semanas;
	}

	public static Double calcularIndiceMasaCorporal(Double pesokg, Double tallaMetros) {

		BigDecimal imc = BigDecimal.valueOf(pesokg / Math.pow(tallaMetros, 2));
		imc = imc.setScale(2, RoundingMode.HALF_UP);

		return imc.doubleValue();
	}

	public static Double[] calcularValorHBRiesgoCorregido(Double valorHBRiesgo, Integer z17s) {

		Optional<Object[]> factorCorreccionOptional = Stream.of(FACTOR_CORRECION_ALTURA).parallel()
				.filter((Object[] factorCorrecion) -> {
					int alturaMinima = (int) factorCorrecion[1];
					int alturaMaxima = (int) factorCorrecion[2];
					return z17s >= alturaMinima && z17s <= alturaMaxima;

				}).findFirst();

		if (factorCorreccionOptional.isPresent()) {
			BigDecimal valorHBRiesgoDecimal = BigDecimal.valueOf(valorHBRiesgo);
			BigDecimal factorCorrecion = BigDecimal.valueOf((Double) factorCorreccionOptional.get()[0]);
			BigDecimal valorHBRiesgoCorregido = valorHBRiesgoDecimal.subtract(factorCorrecion).setScale(2,
					RoundingMode.HALF_UP);
			return new Double[] { valorHBRiesgoCorregido.doubleValue(), factorCorrecion.doubleValue() };
		} else {
			return new Double[] {};
		}
	}

	public static Double[] calcularPesoRecomendadoDadoIMCTalla(Double imcMin, Double imcMax, Double tallaCmts) {

		BigDecimal pesokgMin = BigDecimal.valueOf(imcMin * Math.pow(tallaCmts / 100, 2));
		pesokgMin = pesokgMin.setScale(2, RoundingMode.HALF_UP);

		BigDecimal pesokgMax = BigDecimal.valueOf(imcMax * Math.pow(tallaCmts / 100, 2));
		pesokgMax = pesokgMax.setScale(2, RoundingMode.HALF_UP);

		return new Double[] { pesokgMin.doubleValue(), pesokgMax.doubleValue() };
	}

	public static Double calcularScoreZGivenValues(Double y, Double l, Double m, Double s) {

		BigDecimal z = BigDecimal.valueOf((Math.pow((y / m), l) - 1d) / (s * l)).setScale(2, RoundingMode.HALF_UP);

		if (z.doubleValue() > 3.0d) {
			BigDecimal SD3pos = BigDecimal.valueOf(m * Math.pow((1 + l * s * 3d), (1d / l)));
			BigDecimal SD23pos = BigDecimal.valueOf(SD3pos.doubleValue() - m * Math.pow((1 + l * s * 2d), (1d / l)));
			z = BigDecimal.valueOf(3d + (y - SD3pos.doubleValue()) / SD23pos.doubleValue()).setScale(2,
					RoundingMode.HALF_UP);
		} else if (z.doubleValue() < (-3d)) {
			BigDecimal SD3neg = BigDecimal.valueOf(m * Math.pow((1d + l * s * (-3d)), (1d / l)));
			BigDecimal SD23neg = BigDecimal.valueOf(m * Math.pow((1 + l * s * (-2d)), (1d / l)) - SD3neg.doubleValue());
			z = BigDecimal.valueOf((-3d) + (y - SD3neg.doubleValue()) / SD23neg.doubleValue()).setScale(2,
					RoundingMode.HALF_UP);
		}

		return z.doubleValue();
	}
}
