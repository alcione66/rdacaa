/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.utilitario.animacion;

import javax.swing.JProgressBar;

/**
 *
 * @author jazael.faubla
 */
public class Loading extends Thread {
    
    JProgressBar progreso;
    
    public Loading(JProgressBar progreso){
        super();
        this.progreso=progreso;
    }
    
    @Override
    public void run(){
        for(int i = 1; i <= 100; i++) {
            progreso.setValue(i);
            pausa(90);
        }
    }
    
    public void pausa(int mlSeg){
        try{
            Thread.sleep(mlSeg);
        } catch(InterruptedException e){
            System.err.println("Ah ocurrido un error ====>>> " + e.getMessage());
            Thread.currentThread().interrupt();
        }
    }
}
