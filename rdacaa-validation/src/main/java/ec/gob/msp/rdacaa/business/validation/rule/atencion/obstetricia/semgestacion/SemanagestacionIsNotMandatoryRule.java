package ec.gob.msp.rdacaa.business.validation.rule.atencion.obstetricia.semgestacion;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 5)
public class SemanagestacionIsNotMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		String semGestacion = getVariableFromMap(RdacaaVariableKeyCatalog.OBSTETRICIA_VALOR_SEMANA_GESTACION);
		return (!isMandatory(RdacaaVariableKeyCatalog.OBSTETRICIA_VALOR_SEMANA_GESTACION)
				&& ValidationSupport.isNotDefined(semGestacion));
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}
