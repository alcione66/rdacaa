package ec.gob.msp.rdacaa.business.validation.rule.persona.nacionalidadetnica;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.deliveredtechnologies.rulebook.model.RuleBook;

import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.RdacaaRuleExecutor;

/**
*
* @author dmurillo
*/
@Component
public class NacionalidadEtnicaRuleGroupExecutor extends RdacaaRuleExecutor {

	@Autowired
    public  NacionalidadEtnicaRuleGroupExecutor(@Qualifier("ReglasNacionalidadEtnica") RuleBook<RdacaaRawRowResult> ruleBook){
        super();
        this.ruleBook = ruleBook;
        this.variableKey = RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD_ETNICA;
    }
}
