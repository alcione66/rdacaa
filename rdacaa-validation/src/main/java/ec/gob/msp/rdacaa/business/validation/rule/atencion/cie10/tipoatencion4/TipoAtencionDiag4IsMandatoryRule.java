package ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.tipoatencion4;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 4)
public class TipoAtencionDiag4IsMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		return true;

	}

	@Then
	public RuleState then() {

		String tipoatenciondiagnostico4 = getVariableFromMap(
				RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_ATENCION_4);
		if (isMandatory(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_ATENCION_4)
				&& ValidationSupport.isNotDefined(tipoatenciondiagnostico4)) {
			addValidationResult(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_ATENCION_4,
					ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_ATENCION_DIAGNOSTICO_4_ES_MANDATORIA);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_ATENCION_4)
				&& ValidationSupport.isNotDefined(tipoatenciondiagnostico4)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}

}
