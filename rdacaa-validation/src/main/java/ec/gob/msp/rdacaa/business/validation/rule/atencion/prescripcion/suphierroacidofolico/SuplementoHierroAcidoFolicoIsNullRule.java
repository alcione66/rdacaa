package ec.gob.msp.rdacaa.business.validation.rule.atencion.prescripcion.suphierroacidofolico;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 2)
public class SuplementoHierroAcidoFolicoIsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String suplementoHierroAcidoFolico = getVariableFromMap(RdacaaVariableKeyCatalog.PRESCRIPCION_SUPLEMENTOS_ACIDO_FOLICO);
		return ValidationSupport.isNullOrEmptyString(suplementoHierroAcidoFolico);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PRESCRIPCION_SUPLEMENTOS_ACIDO_FOLICO,
				ValidationResultCatalogConstants.CODIGO_ERROR_PRESCRIPCION_SUPLEMENTOS_ACIDO_FOLICO_NULO);
		return RuleState.BREAK;
	}

}
