package ec.gob.msp.rdacaa.business.validation.rule.persona.sexo;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;


@Rule(order = 2)
public class SexoIsNullRule  extends BaseRule<String>{
	@When
	public boolean when() {
		String sexo = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO);
		return ValidationSupport.isNullOrEmptyString(sexo);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO,
				ValidationResultCatalogConstants.CODIGO_ERROR_SEXO_NULO);
		return RuleState.BREAK;
	}
}
