package ec.gob.msp.rdacaa.business.validation.rule.atencion.sivan.madreperlactancia;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.deliveredtechnologies.rulebook.model.RuleBook;

import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.RdacaaRuleExecutor;

@Component
public class MadrePeriodoLactanciaRuleGroupExecutor extends RdacaaRuleExecutor {

	@Autowired
	public MadrePeriodoLactanciaRuleGroupExecutor(
			@Qualifier("ReglasMadrePeriodoLactancia") RuleBook<RdacaaRawRowResult> ruleBook) {
		super();
		this.ruleBook = ruleBook;
		this.variableKey = RdacaaVariableKeyCatalog.SIVAN_ES_MADRE_PERIODO_LACTANCIA;
	}
}
