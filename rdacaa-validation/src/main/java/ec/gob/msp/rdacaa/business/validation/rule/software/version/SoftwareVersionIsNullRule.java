package ec.gob.msp.rdacaa.business.validation.rule.software.version;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 2)
public class SoftwareVersionIsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String softwareVersion = getVariableFromMap(RdacaaVariableKeyCatalog.SOFTWARE_VERSION);
		return ValidationSupport.isNullOrEmptyString(softwareVersion);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.SOFTWARE_VERSION,
				ValidationResultCatalogConstants.CODIGO_ERROR_SOFTWARE_VERSION_NULO);
		return RuleState.BREAK;
	}

}
