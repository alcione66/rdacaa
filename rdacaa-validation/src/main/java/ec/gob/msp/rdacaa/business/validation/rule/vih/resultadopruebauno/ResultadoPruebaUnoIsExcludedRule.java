package ec.gob.msp.rdacaa.business.validation.rule.vih.resultadopruebauno;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/

@Rule(order = 3)
public class ResultadoPruebaUnoIsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		Boolean valor = false;
		if(!variableHasErrors(RdacaaVariableKeyCatalog.VIH_CODIGO_PRIMERA_PRUEBA)){
			valor = isExcludedFromValidation(RdacaaVariableKeyCatalog.VIH_VALOR_RESULTADO_PRIMERA_PRUEBA);
		}
		return valor;
	}

	@Then
	public RuleState then() {
		
		String resultado = getVariableFromMap(RdacaaVariableKeyCatalog.VIH_VALOR_RESULTADO_PRIMERA_PRUEBA);
		if(!ValidationSupport.isNotDefined(resultado)){
			addValidationResult(RdacaaVariableKeyCatalog.VIH_VALOR_RESULTADO_PRIMERA_PRUEBA,
					ValidationResultCatalogConstants.CODIGO_ERROR_VIH_RESULTADO_PRUEBA_UNO_NO_DEFINIDA);
		}
		
		return RuleState.BREAK;

	}
}
