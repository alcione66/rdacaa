package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.tallacorregida;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

@Rule(order = 6)
public class TallaCorregidaCalculateForAgeRule extends UtilsRule {

	private static final int EDAD_UN_ANIO_SEIS_MES_UN_DIA = 10601;
	private static final int EDAD_DOS_ANIO_CERO_MES_UN_DIA = 20001;
	private static final int EDAD_CINCO_ANIO_UN_MES_CERO_DIA = 50100;

	private static final String DE_PIE = "0";
	private static final String ACOSTADO = "1";

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {

		String tallaCorregida = "999990000066666";
		String talla = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA);
		String tipoTomaTalla = getVariableFromMap(
				RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA);
		boolean tallaHasErrors = variableHasErrors(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA);
		boolean tipoTomaTallaHasErrors = variableHasErrors(
				RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA);
		Integer edadAnioMesDias = getEdadAnioMesDias();

		boolean valuesNotDefined = ValidationSupport.isNotDefined(talla)
				|| ValidationSupport.isNotDefined(tipoTomaTalla);

		setRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA, tallaCorregida);

		if (edadAnioMesDias == null) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TALLA_CORREGIDA_FECHAS_FORMATO_INCORRECTO);
		}

		if (tallaHasErrors) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TALLA_CORREGIDA_TALLA_TIENE_ERRORES);
		}

		if (tipoTomaTallaHasErrors) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TALLA_CORREGIDA_TIPO_TOMA_TIENE_ERRORES);
		}

		if (valuesNotDefined) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TALLA_CORREGIDA_TALLA_O_TIPO_TOMA_NO_DEFINIDO);
		}

		if (tallaHasErrors || tipoTomaTallaHasErrors || edadAnioMesDias == null || valuesNotDefined) {
			return RuleState.BREAK;
		}

		Double tallaCorregidaDouble = calcularTallaCorregidaParaEdad(talla, tipoTomaTalla, edadAnioMesDias);
		tallaCorregida = tallaCorregidaDouble.toString();

		setRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA, tallaCorregida);

		addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA,
				ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_TALLA_CORREGIDA_VALOR);
		return RuleState.NEXT;

	}

	private Double calcularTallaCorregidaParaEdad(String talla, String tipoTomaTalla, Integer edadAniosMesDias) {
		Double tallaCorregidaValor = Double.parseDouble(talla);
		boolean isTallaDePie = DE_PIE.equals(tipoTomaTalla);
		boolean isTallaAcostado = ACOSTADO.equals(tipoTomaTalla);

		if (edadAniosMesDias >= EDAD_UN_ANIO_SEIS_MES_UN_DIA && edadAniosMesDias < EDAD_DOS_ANIO_CERO_MES_UN_DIA
				&& isTallaDePie) {
			tallaCorregidaValor = tallaCorregidaValor + 0.7d;
		} else if (edadAniosMesDias >= EDAD_DOS_ANIO_CERO_MES_UN_DIA
				&& edadAniosMesDias < EDAD_CINCO_ANIO_UN_MES_CERO_DIA && isTallaAcostado) {
			tallaCorregidaValor = tallaCorregidaValor - 0.7d;
		}
		return tallaCorregidaValor;
	}

}
