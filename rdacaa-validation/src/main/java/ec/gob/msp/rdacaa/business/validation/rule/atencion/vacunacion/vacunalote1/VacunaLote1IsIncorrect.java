package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunalote1;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class VacunaLote1IsIncorrect extends BaseRule<String> {

	@When
	public boolean when() {
		String vacunaLote1 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_LOTE_1);
		return !StringUtils.isAlphanumeric(vacunaLote1) || ValidationSupport.isStringMoreThanXCharacters(vacunaLote1, 20) ;
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_LOTE_1,
				ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_LOTE_1_ES_INCORRECTA);
		return RuleState.BREAK;
	}
}
