package ec.gob.msp.rdacaa.business.validation.rule;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.deliveredtechnologies.rulebook.Fact;

import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;

@Component
public class RowRuleExecutor {
	
	private static final Logger logger = LoggerFactory.getLogger(RowRuleExecutor.class);

	@Autowired
	private List<RdacaaRuleExecutor> listaGrupoReglas = new ArrayList<>();

	@PostConstruct
	private void init() {
		// ordenando las reglas para determinar el orden de ejecucion
		listaGrupoReglas.sort((gr1, gr2) -> gr1.getOrder().compareTo(gr2.getOrder()));
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	public Optional<RdacaaRawRowResult> executeRuleGroups(RdacaaRawRow input) {
		try {
			RowItem atencionUUID = input.get(RdacaaVariableKeyCatalog.ATENCION_CODIGO_UUID);
			logger.debug("ATENCION_CODIGO_UUID {}", atencionUUID.getItemValue());
		}catch (Exception e) {
			logger.error("RdacaaRawRow input es nulo");
		}
		
		RdacaaRawRowResult resultTotal = new RdacaaRawRowResult();

		long tiempoInicio = System.currentTimeMillis();
		listaGrupoReglas.forEach(rule -> {
			Fact resultTotalFact = new Fact("rdacaa_row_result", resultTotal);
			Optional<RdacaaRawRowResult> resultForEach = rule.execute(input, resultTotalFact);
			RdacaaRawRowResult currentResult = resultForEach.isPresent() ? resultForEach.get()
					: new RdacaaRawRowResult();
			resultTotal.putAll(currentResult);

		});
		
		long tiempoFin = System.currentTimeMillis();
		long tiempoTotal = tiempoFin - tiempoInicio;
		logger.debug("Tiempo Total fila {}", tiempoTotal);

		return Optional.of(resultTotal);
	}

}
