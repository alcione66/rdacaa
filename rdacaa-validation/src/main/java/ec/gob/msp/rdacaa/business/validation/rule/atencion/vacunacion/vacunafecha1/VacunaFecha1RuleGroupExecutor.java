package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunafecha1;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.deliveredtechnologies.rulebook.model.RuleBook;

import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.RdacaaRuleExecutor;

@Component
public class VacunaFecha1RuleGroupExecutor extends RdacaaRuleExecutor {

	@Autowired
	public VacunaFecha1RuleGroupExecutor(
			@Qualifier("ReglasVacunaFecha1") RuleBook<RdacaaRawRowResult> ruleBook) {
		super();
		this.ruleBook = ruleBook;
		this.variableKey = RdacaaVariableKeyCatalog.VACUNAS_CODIGO_FECHA_1;
	}
}
