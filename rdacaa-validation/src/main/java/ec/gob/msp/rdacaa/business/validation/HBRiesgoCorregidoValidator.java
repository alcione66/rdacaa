/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ec.gob.msp.rdacaa.business.service.MedicionPacienteAlertasService;
import ec.gob.msp.rdacaa.business.service.SignosVitalesReglasValidacion;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalog;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.common.Validator;

/**
 *
 * @author Saulo Velasco
 */
@Component
public class HBRiesgoCorregidoValidator extends ValidationSupport implements Validator<String[]> {

	private final Logger logger = LoggerFactory.getLogger(HBRiesgoCorregidoValidator.class);

	@Autowired
	private MedicionPacienteAlertasService medicionPacienteAlertasService;

	private static final Integer IS_EMBARAZADA_GRUPO_PRIORITARIO_ID = 634;

	@Override
	public List<ValidationResult> validate(String[] valorEdadSexoIsEmbarazada) {

		List<ValidationResult> listaErrores = new ArrayList<>();

		if (isNullOrEmptyString(valorEdadSexoIsEmbarazada[0])) {
			listaErrores.add(ValidationResultCatalog.HB_RIESGO_CORREGIDO_VACIO.getValidationError());
		} else {
			String valor = valorEdadSexoIsEmbarazada[0];
			Integer edad = Integer.parseInt(valorEdadSexoIsEmbarazada[1]);
			String sexo = valorEdadSexoIsEmbarazada[2];
			logger.debug("valorEdadSexoIsEmbarazada[3] {}", valorEdadSexoIsEmbarazada[3]);
			Integer grupoPrioritarioEmbarazada = Boolean.parseBoolean(valorEdadSexoIsEmbarazada[3])
					? IS_EMBARAZADA_GRUPO_PRIORITARIO_ID
					: null;
			Optional<ValidationResult> warning = procesarHBRiesgoCorregidoParaEdad(valor, edad, sexo,
					grupoPrioritarioEmbarazada);
			if (warning.isPresent()) {
				listaErrores.add(warning.get());
			}
		}

		return listaErrores;
	}

	private Optional<ValidationResult> procesarHBRiesgoCorregidoParaEdad(String valor, Integer edad, String sexo,
			Integer grupoPrioritarioEmbarazada) {
		logger.debug("valor HBC {}", valor);
		logger.debug("valor grupoPrioritarioEmbarazada {}", grupoPrioritarioEmbarazada);
		
		try {
			SignosVitalesReglasValidacion alertaFound = medicionPacienteAlertasService
					.calculateIndicadorAnemiaHBRiesgoCorregido(valor, edad, sexo, grupoPrioritarioEmbarazada);
			if (alertaFound != null) {
				logger.debug("minimo alerta {}", alertaFound.getValorMinimoAlerta());
				logger.debug("maximo alerta {}", alertaFound.getValorMaximoAlerta());
				ValidationResult warning = ValidationResultCatalog.HB_RIESGO_CORREGIDO_ALERTA.getValidationError();
				String message = warning.getMessage();
				message = message.replace("alerta", alertaFound.getAlerta());
				logger.debug("alerta {}", alertaFound.getAlerta());
				warning = new ValidationResult(warning.getCode(), message);
				warning.setColor(alertaFound.getColor());
				return Optional.of(warning);
			}
		} catch (Exception e) {
			//Se controla mas adelante
		}

		return Optional.empty();
	}

}
