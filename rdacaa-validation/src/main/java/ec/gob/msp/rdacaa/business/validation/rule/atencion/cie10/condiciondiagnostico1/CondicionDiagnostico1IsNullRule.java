package ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.condiciondiagnostico1;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 2)
public class CondicionDiagnostico1IsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String condiciondiagnostico1 = getVariableFromMap(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CONDICION_DIAG_1);
		return ValidationSupport.isNullOrEmptyString(condiciondiagnostico1);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CONDICION_DIAG_1,
				ValidationResultCatalogConstants.CODIGO_ERROR_CONDICION_DIAGNOSTICO_1_NULO);
		return RuleState.BREAK;
	}

}
