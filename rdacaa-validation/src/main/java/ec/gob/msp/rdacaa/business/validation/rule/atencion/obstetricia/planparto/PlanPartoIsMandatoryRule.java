package ec.gob.msp.rdacaa.business.validation.rule.atencion.obstetricia.planparto;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 4)
public class PlanPartoIsMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		return true;

	}

	@Then
	public RuleState then() {

		String planParto = getVariableFromMap(RdacaaVariableKeyCatalog.OBSTETRICIA_TIENE_PLAN_PARTO);
		if (isMandatory(RdacaaVariableKeyCatalog.OBSTETRICIA_TIENE_PLAN_PARTO)
				&& ValidationSupport.isNotDefined(planParto)) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_TIENE_PLAN_PARTO,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_TIENE_PLAN_PARTO_ES_MANDATORIA);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.OBSTETRICIA_TIENE_PLAN_PARTO)
				&& ValidationSupport.isNotDefined(planParto)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}

}
