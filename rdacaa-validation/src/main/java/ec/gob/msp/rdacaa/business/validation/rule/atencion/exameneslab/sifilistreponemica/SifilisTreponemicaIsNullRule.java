package ec.gob.msp.rdacaa.business.validation.rule.atencion.exameneslab.sifilistreponemica;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 2)
public class SifilisTreponemicaIsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String sifilisTreponemica = getVariableFromMap(RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_SIFILIS_TREPONEMICA_RESULTADO);
		return ValidationSupport.isNullOrEmptyString(sifilisTreponemica);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_SIFILIS_TREPONEMICA_RESULTADO,
				ValidationResultCatalogConstants.CODIGO_ERROR_EXAMENES_LABORATORIO_SIFILIS_TREPONEMICA_RESULTADO_NULO);
		return RuleState.BREAK;
	}

}
