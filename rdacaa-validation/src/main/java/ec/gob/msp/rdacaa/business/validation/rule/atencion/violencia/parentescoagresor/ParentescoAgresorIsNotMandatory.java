package ec.gob.msp.rdacaa.business.validation.rule.atencion.violencia.parentescoagresor;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 5)
public class ParentescoAgresorIsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		
		String parentescoAgresor = getVariableFromMap(RdacaaVariableKeyCatalog.VIOLENCIA_PARENTESCO_AGRESOR);
		return (!isMandatory(RdacaaVariableKeyCatalog.VIOLENCIA_PARENTESCO_AGRESOR) &&
				ValidationSupport.isNotDefined(parentescoAgresor));
		
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}
