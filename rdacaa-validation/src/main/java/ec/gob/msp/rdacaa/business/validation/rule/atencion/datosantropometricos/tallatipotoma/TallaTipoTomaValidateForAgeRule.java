package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.tallatipotoma;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

@Rule(order = 6)
public class TallaTipoTomaValidateForAgeRule extends UtilsRule {

	private static final int EDAD_CINCO_ANIO_CERO_MES_CERO_DIA = 50000;
	private static final int EDAD_UN_ANIO_SEIS_MES_UN_DIA = 10601;
	private static final int EDAD_CINCO_ANIO_UN_MES_CERO_DIA = 50100;
	private static final String DE_PIE = "0";
	private static final String ACOSTADO = "1";

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {
		String talla = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA);
		String tipoTomaTalla = getVariableFromMap(
				RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA);
		Integer edadAnioMesDias = getEdadAnioMesDias();
		
		if(ValidationSupport.isNullOrEmptyString(talla)) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TIPO_TOMA_MEDIDA_TALLA_VALOR_TALLA_NO_PUEDE_SER_NULO);
			// error
			return RuleState.BREAK;
		}
		
		if (edadAnioMesDias == null) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TIPO_TOMA_MEDIDA_TALLA_FECHAS_FORMATO_INCORRECTO);
			// error
			return RuleState.BREAK;
		}

		if (edadAnioMesDias < EDAD_UN_ANIO_SEIS_MES_UN_DIA) {
			return validateTipoTomaTallaEdadMenor010601(tipoTomaTalla);
		} else if (edadAnioMesDias < EDAD_CINCO_ANIO_UN_MES_CERO_DIA) {
			return validateTipoTomaTallaEdadMenor050101(talla, tipoTomaTalla, edadAnioMesDias);
		} else {
			return validateTipoTomaTallaEdadMayorIgual050101(talla, tipoTomaTalla);
		}

	}

	private RuleState validateTipoTomaTallaEdadMenor010601(String tipoTomaTalla) {
		if (ValidationSupport.isNotDefined(tipoTomaTalla)) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TIPO_TOMA_MEDIDA_TALLA_ES_MANDATORIA_MENOR_050000);
			return RuleState.BREAK;
		} else if (tipoTomaTalla.equals(ACOSTADO)) {
			// no error
			return RuleState.NEXT;
		} else {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TIPO_TOMA_MEDIDA_TALLA_ACOSTADO_MENOR_010601_DEBE_SER_ACOSTADO);
			return RuleState.BREAK;
		}
	}

	private RuleState validateTipoTomaTallaEdadMenor050101(String talla, String tipoTomaTalla,
			Integer edadAnioMesDias) {
		if (ValidationSupport.isNotDefined(tipoTomaTalla) && edadAnioMesDias < EDAD_CINCO_ANIO_CERO_MES_CERO_DIA) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TIPO_TOMA_MEDIDA_TALLA_ES_MANDATORIA_MENOR_050000);
			return RuleState.BREAK;
		} else if (ValidationSupport.isNotDefined(talla) && ValidationSupport.isNotDefined(tipoTomaTalla)
				&& edadAnioMesDias < EDAD_CINCO_ANIO_UN_MES_CERO_DIA) {
			// no error
			return RuleState.NEXT;
		} else if (tipoTomaTalla.equals(ACOSTADO) || tipoTomaTalla.equals(DE_PIE)) {
			// no error
			return RuleState.NEXT;
		} else {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TIPO_TOMA_MEDIDA_TALLA_SOLO_DE_PIE_ACOSTADO);
			return RuleState.BREAK;
		}
	}

	private RuleState validateTipoTomaTallaEdadMayorIgual050101(String talla, String tipoTomaTalla) {

		if (ValidationSupport.isNotDefined(talla)
				&& (ValidationSupport.isNotDefined(tipoTomaTalla) || tipoTomaTalla.equals(DE_PIE))) {
			// no error
			return RuleState.NEXT;
		} else if (!ValidationSupport.isNotDefined(talla) && tipoTomaTalla.equals(DE_PIE)) {
			return RuleState.NEXT;
		} else {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TIPO_TOMA_MEDIDA_TALLA_MAYOR_050100_ES_DE_PIE);
			return RuleState.BREAK;
		}
	}
}
