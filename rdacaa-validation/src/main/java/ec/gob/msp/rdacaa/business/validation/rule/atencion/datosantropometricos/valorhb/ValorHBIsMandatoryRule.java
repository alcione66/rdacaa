package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.valorhb;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 4)
public class ValorHBIsMandatoryRule extends BaseRule<String> {
	@When
	public boolean when() {
		return true;

	}

	@Then
	public RuleState then() {

		String valorHB = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB);
		if (isMandatory(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB)
				&& ValidationSupport.isNotDefined(valorHB)) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_VALOR_HB_ES_MANDATORIA);
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}
	}
}
