package ec.gob.msp.rdacaa.business.validation.rule.atencion.grupoprioritario.estrategiamsp;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */

@Rule(order = 3)
public class EstrategiaMspIsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.ESTRATEGIA_MSP);
	}

	@Then
	public RuleState then() {

		String estrategiamsp = getVariableFromMap(RdacaaVariableKeyCatalog.ESTRATEGIA_MSP);
		if (!ValidationSupport.isNotDefined(estrategiamsp)) {
			addValidationResult(RdacaaVariableKeyCatalog.ESTRATEGIA_MSP,
					ValidationResultCatalogConstants.CODIGO_ERROR_ESTRATEGIA_MSP_NO_DEFINIDA);
		}

		return RuleState.BREAK;

	}
}
