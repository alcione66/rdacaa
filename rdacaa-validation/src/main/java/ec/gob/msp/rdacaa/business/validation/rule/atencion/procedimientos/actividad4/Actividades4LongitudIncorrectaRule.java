package ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.actividad4;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 7)
public class Actividades4LongitudIncorrectaRule extends BaseRule<String>{
	@When
	public boolean when() {
		String actividades4 = getVariableFromMap(
				RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_4);
		return ValidationSupport.isStringMoreThanXCharacters(actividades4, 2);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_4,
				ValidationResultCatalogConstants.CODIGO_ERROR_ACTIVIDAD_4_LONGITUD_INCORRECTA);
		return RuleState.BREAK;
	}
}
