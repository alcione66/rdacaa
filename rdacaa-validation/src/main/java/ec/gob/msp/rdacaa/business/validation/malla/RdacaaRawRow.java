package ec.gob.msp.rdacaa.business.validation.malla;

import java.util.concurrent.ConcurrentHashMap;

public class RdacaaRawRow extends ConcurrentHashMap<String, RowItem> {

	private static final long serialVersionUID = 8502946881090635089L;

	public RdacaaRawRow addField(String key, RowItem value) {
		super.put(key, value);
		return this;
	}
}
