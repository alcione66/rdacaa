package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.talla;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 2)
public class TallaIsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String talla = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA);
		return ValidationSupport.isNullOrEmptyString(talla);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA,
				ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TALLA_NULO);
		return RuleState.BREAK;
	}
}
