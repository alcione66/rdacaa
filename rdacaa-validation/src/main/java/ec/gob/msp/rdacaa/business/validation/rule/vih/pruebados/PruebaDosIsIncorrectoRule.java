package ec.gob.msp.rdacaa.business.validation.rule.vih.pruebados;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 6)
public class PruebaDosIsIncorrectoRule extends BaseRule<String> {

	@When
	public boolean when() {
		String prueba = getVariableFromMap(RdacaaVariableKeyCatalog.VIH_CODIGO_SEGUNDA_PRUEBA);
		if(StringUtils.isNumeric(prueba)){
			return !this.validationQueryService.isCodigoVihPruebaValido(Integer.valueOf(prueba));
		}
		return true;
	}

	@Then
	public void then() {
		addValidationResult(RdacaaVariableKeyCatalog.VIH_CODIGO_SEGUNDA_PRUEBA,
				ValidationResultCatalogConstants.CODIGO_ERROR_VIH_CODIGO_PRUEBA_DOS_INCORRECTO);
	}
}
