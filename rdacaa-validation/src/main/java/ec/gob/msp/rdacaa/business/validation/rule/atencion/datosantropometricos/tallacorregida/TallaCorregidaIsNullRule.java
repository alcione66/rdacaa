package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.tallacorregida;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 2)
public class TallaCorregidaIsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String tallaCorregida = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA);
		return ValidationSupport.isNullOrEmptyString(tallaCorregida);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA,
				ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TALLA_CORREGIDA_NULO);
		return RuleState.BREAK;
	}
}
