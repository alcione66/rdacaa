package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunalote1;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 4)
public class VacunaLote1IsMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		return true;

	}

	@Then
	public RuleState then() {

		String vacunaLote1 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_LOTE_1);
		if (isMandatory(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_LOTE_1)
				&& ValidationSupport.isNotDefined(vacunaLote1)) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_LOTE_1,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_LOTE_1_ES_MANDATORIA);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_LOTE_1)
				&& ValidationSupport.isNotDefined(vacunaLote1)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}

}
