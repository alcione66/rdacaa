package ec.gob.msp.rdacaa.business.validation.rule.atencion.exameneslab.sifilistreponemica;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class SifilisTreponemicaIsIncorrect extends UtilsRule {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {

		String sifilisTreponemica = getVariableFromMap(
				RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_SIFILIS_TREPONEMICA_RESULTADO);
		String semGestacion = getVariableFromMap(RdacaaVariableKeyCatalog.OBSTETRICIA_VALOR_SEMANA_GESTACION);
		boolean semGestacionHasErrors = variableHasErrors(RdacaaVariableKeyCatalog.OBSTETRICIA_VALOR_SEMANA_GESTACION);

		boolean isEmbarazada = isEmbarazada();
		boolean isMujer = "F".equals(getSexoString());

		boolean isValidValue = "1".equals(sifilisTreponemica.trim()) || "2".equals(sifilisTreponemica.trim())
				|| "3".equals(sifilisTreponemica.trim());

		if (!isValidValue) {
			addValidationResult(RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_SIFILIS_TREPONEMICA_RESULTADO,
					ValidationResultCatalogConstants.CODIGO_ERROR_EXAMENES_LABORATORIO_SIFILIS_TREPONEMICA_RESULTADO_ES_INCORRECTO);
		}

		boolean requiereMujerEmbarazadaParaValorValido = !(isMujer && isEmbarazada) && isValidValue;
		if (requiereMujerEmbarazadaParaValorValido) {
			addValidationResult(RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_SIFILIS_TREPONEMICA_RESULTADO,
					ValidationResultCatalogConstants.CODIGO_ERROR_EXAMENES_LABORATORIO_SIFILIS_TREPONEMICA_RESULTADO_REQUIERE_MUJER_EMBARAZADA);
		}

		if (semGestacionHasErrors) {
			addValidationResult(RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_SIFILIS_TREPONEMICA_RESULTADO,
					ValidationResultCatalogConstants.CODIGO_ERROR_EXAMENES_LABORATORIO_SIFILIS_TREPONEMICA_RESULTADO_SEMANA_GESTACION_TIENE_ERRORES);
		}

		boolean esSemana20 = !semGestacionHasErrors && Double.parseDouble(semGestacion) > 20
				&& Double.parseDouble(semGestacion) < 21;

		if (esSemana20 && !ValidationSupport.isNotDefined(sifilisTreponemica)) {
			addValidationResult(RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_SIFILIS_TREPONEMICA_RESULTADO,
					ValidationResultCatalogConstants.CODIGO_ERROR_EXAMENES_LABORATORIO_SIFILIS_TREPONEMICA_RESULTADO_SEMANA_20_DEBE_TENER_DEFAULT_VALUE);
		}

		if (!isValidValue || requiereMujerEmbarazadaParaValorValido || semGestacionHasErrors
				|| (esSemana20 && !ValidationSupport.isNotDefined(sifilisTreponemica))) {
			return RuleState.BREAK;
		}

		return RuleState.NEXT;
	}
}
