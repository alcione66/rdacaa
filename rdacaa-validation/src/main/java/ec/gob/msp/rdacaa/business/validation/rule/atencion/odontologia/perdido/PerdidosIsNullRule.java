package ec.gob.msp.rdacaa.business.validation.rule.atencion.odontologia.perdido;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 2)
public class PerdidosIsNullRule extends BaseRule<String>{
	@When
	public boolean when() {
		String dientesPerdidos = getVariableFromMap(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_PERDIDAS);
		return ValidationSupport.isNullOrEmptyString(dientesPerdidos);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_PERDIDAS,
				ValidationResultCatalogConstants.CODIGO_ERROR_DIENTES_PERDIDOS_NULO);
		return RuleState.BREAK;
	}
}
