package ec.gob.msp.rdacaa.business.validation.rule.atencion.prescripcion.suphierrojarabe;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class SuplementoHierroJarabeIsIncorrect extends UtilsRule {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {
		
		String suplementoHierroJarabe = getVariableFromMap(
				RdacaaVariableKeyCatalog.PRESCRIPCION_SUPLEMENTOS_VITAMINA_A);

		Integer edadAniosMesesDias = getEdadAnioMesDias();

		boolean isValidValue = "0".equals(suplementoHierroJarabe) || "1".equals(suplementoHierroJarabe);

		if (!isValidValue) {
			addValidationResult(RdacaaVariableKeyCatalog.PRESCRIPCION_SUPLEMENTOS_HIERRO_JARABE,
					ValidationResultCatalogConstants.CODIGO_ERROR_PRESCRIPCION_SUPLEMENTOS_HIERRO_JARABE_ES_INCORRECTO);
		}

		if (edadAniosMesesDias == null) {
			addValidationResult(RdacaaVariableKeyCatalog.PRESCRIPCION_SUPLEMENTOS_HIERRO_JARABE,
					ValidationResultCatalogConstants.CODIGO_ERROR_PRESCRIPCION_SUPLEMENTOS_HIERRO_JARABE_FECHAS_INVALIDAS);
			return RuleState.BREAK;
		}

		boolean requiereEdad24mesesA59meses = !(edadAniosMesesDias >= 20000 && edadAniosMesesDias < 50000)
				&& !ValidationSupport.isNotDefined(suplementoHierroJarabe);
		if (requiereEdad24mesesA59meses) {
			addValidationResult(RdacaaVariableKeyCatalog.PRESCRIPCION_SUPLEMENTOS_HIERRO_JARABE,
					ValidationResultCatalogConstants.CODIGO_ERROR_PRESCRIPCION_SUPLEMENTOS_HIERRO_JARABE_DEBE_TENER_DEFAULT_VALUE);
		}
		
		if(!isValidValue || requiereEdad24mesesA59meses) {
			return RuleState.BREAK;
		}

		return RuleState.NEXT;
	}
}
