package ec.gob.msp.rdacaa.business.validation.database.common;

public class VariableNotFoundException extends RuntimeException {

	private static final long serialVersionUID = -532132473902591591L;

	public VariableNotFoundException() {
		super();
	}

	public VariableNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public VariableNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public VariableNotFoundException(String message) {
		super(message);
	}

	public VariableNotFoundException(Throwable cause) {
		super(cause);
	}
	
}
