package ec.gob.msp.rdacaa.business.validation.rule.atencion.obstetricia.fum;

import java.util.Date;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;
import ec.gob.msp.rdacaa.utilitario.fecha.FechasUtil;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class FUMIsIncorrect extends UtilsRule {

	private static final String CODIGO_METODO_FUM = "2577";

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {
		String fum = getVariableFromMap(RdacaaVariableKeyCatalog.OBSTETRICIA_FECHA_ULTIMA_MESTRUACION);
		String fechaAtencionString = getVariableFromMap(RdacaaVariableKeyCatalog.ATENCION_FECHA);
		String metodoSemanagestacion = getVariableFromMap(
				RdacaaVariableKeyCatalog.OBSTETRICIA_CODIGO_METODO_DET_SEMANAS_GESTACION);
		boolean metodoSemGestacionHasErrors = variableHasErrors(
				RdacaaVariableKeyCatalog.OBSTETRICIA_CODIGO_METODO_DET_SEMANAS_GESTACION);

		Integer edadAniosMesesDias = getEdadAnioMesDias();
		boolean isEmbarazada = isEmbarazada();
		boolean isMujer = "F".equals(getSexoString());

		boolean isFUMValida = isFechaValida(fum);

		boolean isFechaAtencionValida = isFechaValida(fechaAtencionString);

		if (!isFechaAtencionValida) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_FECHA_ULTIMA_MESTRUACION,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_FECHA_ULTIMA_MESTRUACION_FECHA_ATENCION_ES_INCORRECTA);
		}

		if (!isFUMValida) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_FECHA_ULTIMA_MESTRUACION,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_FECHA_ULTIMA_MESTRUACION_NO_FORMATO);
		}

		if (metodoSemGestacionHasErrors) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_FECHA_ULTIMA_MESTRUACION,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_FECHA_ULTIMA_MESTRUACION_METODO_SIN_ERRORES);
		}

		boolean validaSoloParaMetodoFUM = !metodoSemGestacionHasErrors
				&& !CODIGO_METODO_FUM.equals(metodoSemanagestacion)
				&& !ValidationSupport.isNotDefined(metodoSemanagestacion);
		if (validaSoloParaMetodoFUM) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_FECHA_ULTIMA_MESTRUACION,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_FECHA_ULTIMA_MESTRUACION_DEBE_TENER_DEFAULT_VALUE);
		}

		if (!isFechaAtencionValida || !isFUMValida || validaSoloParaMetodoFUM) {
			return RuleState.BREAK;
		}

		Date fumDate = getFechaFromString(fum);
		Date fechaAtencion = getFechaFromString(fechaAtencionString);
		Date fecDesde = FechasUtil.operarFecha(fechaAtencion, 0, -9, 0);

		boolean fumFueraDeRagoEdad = fumDate.after(fechaAtencion) || !(fumDate.equals(fechaAtencion)
				|| fumDate.before(fechaAtencion) || fumDate.equals(fecDesde) || fumDate.after(fecDesde));
		if (fumFueraDeRagoEdad) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_FECHA_ULTIMA_MESTRUACION,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_FECHA_ULTIMA_MESTRUACION_FUERA_RANGO);
		}

		boolean requiereMujerEmbarazadaParaValorValido = !(isMujer && isEmbarazada) && !fumFueraDeRagoEdad;
		if (requiereMujerEmbarazadaParaValorValido) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_FECHA_ULTIMA_MESTRUACION,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_FECHA_ULTIMA_MESTRUACION_REQUIERE_MUJER_EMBARAZADA);
		}

		if (edadAniosMesesDias == null) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_FECHA_ULTIMA_MESTRUACION,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_FECHA_ULTIMA_MESTRUACION_FECHAS_INVALIDAS);
			return RuleState.BREAK;
		}

		boolean requiereMayor8AniosMenor59Anios = !(edadAniosMesesDias >= 80000 && edadAniosMesesDias <= 590000)
				&& !ValidationSupport.isNotDefined(fum);
		if (requiereMayor8AniosMenor59Anios) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_FECHA_ULTIMA_MESTRUACION,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_FECHA_ULTIMA_MESTRUACION_DEBE_TENER_DEFAULT_VALUE);
		}

		if (fumFueraDeRagoEdad || requiereMujerEmbarazadaParaValorValido || requiereMayor8AniosMenor59Anios) {
			return RuleState.BREAK;
		}

		return RuleState.NEXT;
	}
}
