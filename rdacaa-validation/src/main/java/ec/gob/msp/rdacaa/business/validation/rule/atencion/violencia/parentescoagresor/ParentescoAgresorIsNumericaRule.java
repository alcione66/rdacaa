package ec.gob.msp.rdacaa.business.validation.rule.atencion.violencia.parentescoagresor;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 6)
public class ParentescoAgresorIsNumericaRule extends BaseRule<String> {

	@When
	public boolean when() {
		String sexo = getVariableFromMap(RdacaaVariableKeyCatalog.VIOLENCIA_PARENTESCO_AGRESOR);
		return !StringUtils.isNumeric(sexo);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.VIOLENCIA_PARENTESCO_AGRESOR,
				ValidationResultCatalogConstants.CODIGO_ERROR_VIOLENCIA_PARENTESCO_AGRESOR_NUMERICO);
		return RuleState.BREAK;
	}
}
