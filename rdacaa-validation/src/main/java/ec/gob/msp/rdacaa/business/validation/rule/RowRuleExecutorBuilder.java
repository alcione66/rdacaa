package ec.gob.msp.rdacaa.business.validation.rule;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ec.gob.msp.rdacaa.business.entity.Variablexespecialidad;
import ec.gob.msp.rdacaa.business.service.VariablexespecialidadService;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;

@Component
public class RowRuleExecutorBuilder {

	@Autowired
	private VariablexespecialidadService variablexespecialidadService;

	@Autowired
	private RowRuleExecutor rowRuleExecutor;

	public Optional<RdacaaRawRowResult> executeRuleGroupsByCondiciones(RdacaaRawRow input) {

		Integer especialidadId = Integer.valueOf((String)input.get(RdacaaVariableKeyCatalog.ATENCION_CODIGO_ESPECIALIDAD).getItemValue());

		List<Variablexespecialidad> variablesValidar = variablexespecialidadService.findAllVariablesXExpecialidad(especialidadId);

		variablesValidar.forEach(variablexespecialidad -> {
			
			RowItem rowItem = input.get(variablexespecialidad.getVariableId().getCodigo());

			rowItem.setExcludedFromValidation(false);
			if(variablexespecialidad.getIsmandatorio() == 1) {
				rowItem.setMandatory(true);
			}else {
				rowItem.setMandatory(false);
			}
			
		});

		return rowRuleExecutor.executeRuleGroups(input);

	}

}
