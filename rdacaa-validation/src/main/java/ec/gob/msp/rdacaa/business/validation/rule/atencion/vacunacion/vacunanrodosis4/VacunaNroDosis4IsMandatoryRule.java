package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunanrodosis4;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 4)
public class VacunaNroDosis4IsMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		return true;

	}

	@Then
	public RuleState then() {

		String vacunaNroDosis4 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_DOSIS_4);
		if (isMandatory(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_DOSIS_4)
				&& ValidationSupport.isNotDefined(vacunaNroDosis4)) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_DOSIS_4,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_DOSIS_4_ES_MANDATORIA);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_DOSIS_4)
				&& ValidationSupport.isNotDefined(vacunaNroDosis4)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}

}
