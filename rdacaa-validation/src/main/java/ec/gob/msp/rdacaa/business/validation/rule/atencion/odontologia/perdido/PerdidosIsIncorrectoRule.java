package ec.gob.msp.rdacaa.business.validation.rule.atencion.odontologia.perdido;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;
import ec.gob.msp.rdacaa.utilitario.enumeracion.ComunEnum;
import ec.gob.msp.rdacaa.utilitario.fecha.FechasUtil;

@Rule(order = 8)
public class PerdidosIsIncorrectoRule extends BaseRule<String>{

	private static final String ODONTOLOGIA_ESPECIALIDAD_ID = "669";
	private static final String ODONTOLOGIA_ESPECIALIDAD_RURAL_ID = "1519";
	
	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {
		Integer edadAniosMesDias = 0;
		String fechaNacimiento = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO);
		Date fecNac = FechasUtil.formateadorStringAFecha(ComunEnum.PATRON_FECHA6.getDescripcion(), fechaNacimiento);
		String fechaAtencion = getVariableFromMap(RdacaaVariableKeyCatalog.ATENCION_FECHA);
		Date fecHasta = FechasUtil.formateadorStringAFecha(ComunEnum.PATRON_FECHA6.getDescripcion(), fechaAtencion);
		Integer anios = FechasUtil.getDiffDatesDesdeHasta(fecNac, fecHasta, 0);
		Integer meses = FechasUtil.getDiffDatesDesdeHasta(fecNac, fecHasta, 1);
		Integer dias = FechasUtil.getDiffDatesDesdeHasta(fecNac, fecHasta, 2);
		String especialidadId = getVariableFromMap(RdacaaVariableKeyCatalog.ATENCION_CODIGO_ESPECIALIDAD);
		String piezasPerdidas = getVariableFromMap(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_PERDIDAS);
				
		edadAniosMesDias = Integer.parseInt(concatenarEdad(anios, meses, dias));
		boolean piezasPerdidasHasErrors = false;
		if (StringUtils.isNumeric(piezasPerdidas) 
                        && edadAniosMesDias >= 120000
                        && (especialidadId.equals(ODONTOLOGIA_ESPECIALIDAD_ID) ||  especialidadId.equals(ODONTOLOGIA_ESPECIALIDAD_RURAL_ID)) && 
                        !this.validationQueryService.validarRango(0,32,Integer.valueOf(piezasPerdidas))){
				piezasPerdidasHasErrors = true;
				addValidationResult(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_PERDIDAS,
						ValidationResultCatalogConstants.CODIGO_ERROR_DIENTES_PERDIDOS_INCORRECTO);
        }else if (StringUtils.isNumeric(piezasPerdidas) && edadAniosMesDias < 120000) {
        	piezasPerdidasHasErrors = !"0".equals(piezasPerdidas);
        	if(piezasPerdidasHasErrors) {
        		addValidationResult(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_PERDIDAS,
						ValidationResultCatalogConstants.CODIGO_ERROR_DIENTES_PERDIDOS_INCORRECTO);
        	}
        }
		
		boolean piezasCariadasHasErrors = variableHasErrors(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_CARIADAS);
		boolean piezasExtraidasHasErrors = variableHasErrors(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_EXTRAIDAS);
		boolean piezasObturadasHasErrors = variableHasErrors(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_OBTURADAS);
		
		if(!piezasCariadasHasErrors && !piezasExtraidasHasErrors && !piezasObturadasHasErrors && !piezasPerdidasHasErrors) {
			String piezasCariadas = getVariableFromMap(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_CARIADAS);
			String piezasExtraidas = getVariableFromMap(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_EXTRAIDAS);
			String piezasObturadas = getVariableFromMap(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_OBTURADAS);
			
			boolean isCantidadPiezasTotalValido = this.validationQueryService.validarRango(0,32,Integer.valueOf(piezasCariadas)+
					Integer.valueOf(piezasExtraidas)+Integer.valueOf(piezasObturadas)
					+Integer.valueOf(piezasPerdidas));
			if(!isCantidadPiezasTotalValido) {
				addValidationResult(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_PERDIDAS,
						ValidationResultCatalogConstants.CODIGO_ERROR_DIENTES_TOTAL_NUMERO_INCORRECTO);
			}
		}
		
		return RuleState.BREAK;
	}
	
	private String concatenarEdad(Integer edadAnios, Integer edadMeses, Integer edadDias) {
        return StringUtils.leftPad(edadAnios.toString(), 3, "0") + StringUtils.leftPad(edadMeses.toString(), 2, "0")
                + StringUtils.leftPad(edadDias.toString(), 2, "0");
    }
}
