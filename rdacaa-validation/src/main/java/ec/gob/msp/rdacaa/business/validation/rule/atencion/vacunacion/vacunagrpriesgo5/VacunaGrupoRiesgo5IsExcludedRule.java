package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunagrpriesgo5;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 3)
public class VacunaGrupoRiesgo5IsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_GRUPO_RIESGO_5);
	}

	@Then
	public RuleState then() {

		String vacunaEsquema1 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_GRUPO_RIESGO_5);
		if (!ValidationSupport.isNotDefined(vacunaEsquema1)) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_GRUPO_RIESGO_5,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_GRUPO_RIESGO_5_NO_DEFINIDA);
		}

		return RuleState.BREAK;

	}
}
