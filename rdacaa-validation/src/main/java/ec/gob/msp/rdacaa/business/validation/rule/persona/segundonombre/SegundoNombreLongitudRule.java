package ec.gob.msp.rdacaa.business.validation.rule.persona.segundonombre;

import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 5)
public class SegundoNombreLongitudRule extends BaseRule<String> {

	private static final int TRES_CARACTERES = 3;

	@When
	public boolean when() {
		String segundoNombre = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_NOMBRE);
		return !ValidationSupport.isNullOrEmptyString(segundoNombre)
				&& ValidationSupport.isStringLessThanXCharacters(segundoNombre, TRES_CARACTERES);
	}

	@Then
	public void then() {
		addValidationResult(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_NOMBRE,
				ValidationResultCatalogConstants.CODIGO_ERROR_SEGUNDO_NOMBRE_LONG);
	}

}
