package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacuna3;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.entity.Vacuna;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class Vacuna3IsIncorrectRule extends UtilsRule {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {

		String vacuna3 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_APLICACION_3);
		Optional<Vacuna> vacunaOpt = validationQueryService.findVacunaById(vacuna3);

		if (!(StringUtils.isNumeric(vacuna3) && vacunaOpt.isPresent())) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_APLICACION_3,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_APLICACION_3_ES_INCORRECTO);
			return RuleState.BREAK;
		}

		Vacuna vacuna = vacunaOpt.get();

		String sexo = getSexoString();

		if (!((vacuna.getHombre().equals(1) && sexo.equals("M")) || (vacuna.getMujer().equals(1) && sexo.equals("F"))
				|| (vacuna.getIntersexual().equals(1) && sexo.equals("I")))) {
			
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_APLICACION_3,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_APLICACION_3_SEXO_INCORRECTO);
			return RuleState.BREAK;
		}
		
		return RuleState.NEXT;

	}
}
