package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunafecha4;

import java.util.Date;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class VacunaFecha4IsIncorrectRule extends UtilsRule {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {

		String vacunaFechaString4 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_FECHA_4);
		String fechaAtencionString = getVariableFromMap(RdacaaVariableKeyCatalog.ATENCION_FECHA);
		boolean isFechaVacunaValida = isFechaValida(vacunaFechaString4) && 
				getFechaFromString(vacunaFechaString4).after(getFechaFromString("2018-12-31"));
		if (!isFechaVacunaValida) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_FECHA_4,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_FECHA_4_ES_INCORRECTA);
			return RuleState.BREAK;
		}

		boolean isFechaAtencionValida =isFechaValida(fechaAtencionString) && 
				getFechaFromString(fechaAtencionString).after(getFechaFromString("2018-12-31"));
		if (!isFechaAtencionValida) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_FECHA_4,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_FECHA_4_FECHA_ATENCION_ES_INCORRECTA);
			return RuleState.BREAK;
		}

		Date vacunaFecha4 = getFechaFromString(vacunaFechaString4);
		Date fechaAtencion = getFechaFromString(fechaAtencionString);
		
		if(vacunaFecha4.after(fechaAtencion)) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_FECHA_4,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_FECHA_4_POSTERIOR_FECHA_ATENCION);
			return RuleState.BREAK;
		}
		
		return RuleState.NEXT;
	}
}
