package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.tallatipotoma;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 5)
public class TallaTipoTomaIsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		
		String tallaTipoToma = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA);
		return (!isMandatory(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA) &&
				ValidationSupport.isNotDefined(tallaTipoToma));
		
	}

	@Then
	public RuleState then() {
		return RuleState.NEXT;
	}
}
