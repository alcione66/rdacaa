package ec.gob.msp.rdacaa.business.validation.rule.atencion.sivan.madreperlactancia;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 5)
public class MadrePeriodoLactanciaIsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		String madrePeriodoLactancia = getVariableFromMap(RdacaaVariableKeyCatalog.SIVAN_ES_MADRE_PERIODO_LACTANCIA);
		return (!isMandatory(RdacaaVariableKeyCatalog.SIVAN_ES_MADRE_PERIODO_LACTANCIA)
				&& ValidationSupport.isNotDefined(madrePeriodoLactancia));
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}
