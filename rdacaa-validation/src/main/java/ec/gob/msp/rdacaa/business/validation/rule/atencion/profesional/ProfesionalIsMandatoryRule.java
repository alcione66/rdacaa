package ec.gob.msp.rdacaa.business.validation.rule.atencion.profesional;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 4)
public class ProfesionalIsMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		return true;

	}

	@Then
	public RuleState then() {

		String codProfesional = getVariableFromMap(RdacaaVariableKeyCatalog.PROFESIONAL_CODIGO);
		if (isMandatory(RdacaaVariableKeyCatalog.PROFESIONAL_CODIGO)
				&& ValidationSupport.isNotDefined(codProfesional)) {
			addValidationResult(RdacaaVariableKeyCatalog.PROFESIONAL_CODIGO,
					ValidationResultCatalogConstants.CODIGO_ERROR_PROFESIONAL_CODIGO_ES_MANDATORIA);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.PROFESIONAL_CODIGO)
				&& ValidationSupport.isNotDefined(codProfesional)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}

}
