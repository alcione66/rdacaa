/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.atencion.grupoprioritario.gpr_prioritario1;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;
import ec.gob.msp.rdacaa.utilitario.enumeracion.ComunEnum;
import ec.gob.msp.rdacaa.utilitario.fecha.FechasUtil;

import java.util.Date;

import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 7)
public class GrupoPrioritario1IsIncorrectoRule extends BaseRule<String> {
	@When
	public boolean when() {
		String sexo = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO);
		String fechaNacimiento = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO);
		Date fecNac = FechasUtil.formateadorStringAFecha(ComunEnum.PATRON_FECHA6.getDescripcion(), fechaNacimiento);
		String fechaAtencion = getVariableFromMap(RdacaaVariableKeyCatalog.ATENCION_FECHA);
		Date fecHasta = FechasUtil.formateadorStringAFecha(ComunEnum.PATRON_FECHA6.getDescripcion(), fechaAtencion);
		Integer anios = FechasUtil.getDiffDatesDesdeHasta(fecNac, fecHasta, 0);
		Integer meses = FechasUtil.getDiffDatesDesdeHasta(fecNac, fecHasta, 1);
		Integer dias = FechasUtil.getDiffDatesDesdeHasta(fecNac, fecHasta, 2);

		String grupoprioritario1 = getVariableFromMap(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1);
		if (StringUtils.isNumeric(grupoprioritario1)) {
			return !this.validationQueryService.isValidCodigoGrupoPrioritarioByEdadAndSexo(Integer.valueOf(grupoprioritario1),
					anios, meses, dias, Integer.valueOf(sexo));
		}
		return true;
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1,
				ValidationResultCatalogConstants.CODIGO_ERROR_GRUPO_PRIORITARIO_1_INCORRECTO);
		return RuleState.BREAK;
	}
}
