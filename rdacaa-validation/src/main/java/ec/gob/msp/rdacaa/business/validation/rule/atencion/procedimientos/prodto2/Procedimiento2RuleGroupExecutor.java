package ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.prodto2;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.deliveredtechnologies.rulebook.model.RuleBook;

import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.RdacaaRuleExecutor;
@Component
public class Procedimiento2RuleGroupExecutor extends RdacaaRuleExecutor{
	@Autowired
	public Procedimiento2RuleGroupExecutor(@Qualifier("ReglasProcedimiento2") RuleBook<RdacaaRawRowResult> ruleBook) {
		super();
		this.ruleBook = ruleBook;
		this.variableKey = RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_2;
	}
}
