package ec.gob.msp.rdacaa.business.validation.rule.atencion.exameneslab.sifilistratamiento;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 2)
public class SifilisTratamientoIsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String sifilisTratamiento = getVariableFromMap(RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_SIFILIS_TRATAMIENTO);
		return ValidationSupport.isNullOrEmptyString(sifilisTratamiento);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_SIFILIS_TRATAMIENTO,
				ValidationResultCatalogConstants.CODIGO_ERROR_EXAMENES_LABORATORIO_SIFILIS_TRATAMIENTO_NULO);
		return RuleState.BREAK;
	}

}
