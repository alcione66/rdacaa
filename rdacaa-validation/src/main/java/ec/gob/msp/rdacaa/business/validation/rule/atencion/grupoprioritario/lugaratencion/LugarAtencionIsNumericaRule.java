package ec.gob.msp.rdacaa.business.validation.rule.atencion.grupoprioritario.lugaratencion;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author miguel.faubla
*/
@Rule(order = 6)
public class LugarAtencionIsNumericaRule extends BaseRule<String>  {
    @When
    public boolean when() {
        String lugaratencion = getVariableFromMap(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_LUGAR_ATENCION);
        return !StringUtils.isNumeric(lugaratencion);
    }

    @Then
    public RuleState then() {
        addValidationResult(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_LUGAR_ATENCION, ValidationResultCatalogConstants.CODIGO_ERROR_LUGAR_ATENCION_ESTABLECIMIENTO_NONUMERICO);
        return RuleState.BREAK;
    }
}