package ec.gob.msp.rdacaa.business.validation.rule.vih.resultadopruebauno;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 4)
public class ResultadoPruebaUnoIsMandatoryRule extends BaseRule<String>  {

	@When
	public boolean when() {
		
		Boolean valor = false;
		if(!variableHasErrors(RdacaaVariableKeyCatalog.VIH_CODIGO_PRIMERA_PRUEBA)){
			valor = true;
		}
		return valor;
	}

	@Then
	public RuleState then() {

		String prueba = getVariableFromMap(RdacaaVariableKeyCatalog.VIH_VALOR_RESULTADO_PRIMERA_PRUEBA);
		if (isMandatory(RdacaaVariableKeyCatalog.VIH_VALOR_RESULTADO_PRIMERA_PRUEBA)
				&& ValidationSupport.isNotDefined(prueba)) {
			addValidationResult(RdacaaVariableKeyCatalog.VIH_VALOR_RESULTADO_PRIMERA_PRUEBA,
					ValidationResultCatalogConstants.CODIGO_ERROR_VIH_RESULTADO_PRUEBA_UNO_ES_MANDATORIA);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.VIH_VALOR_RESULTADO_PRIMERA_PRUEBA)
				&& ValidationSupport.isNotDefined(prueba)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}
}
