package ec.gob.msp.rdacaa.business.validation.rule.atencion.exameneslab.resbacteriuria;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 5)
public class ResultadoBacteriuriaIsNotMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		String resultadoBacteriuria = getVariableFromMap(RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_RESULTADO_BACTERIURIA);
		return (!isMandatory(RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_RESULTADO_BACTERIURIA)
				&& ValidationSupport.isNotDefined(resultadoBacteriuria));
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}
