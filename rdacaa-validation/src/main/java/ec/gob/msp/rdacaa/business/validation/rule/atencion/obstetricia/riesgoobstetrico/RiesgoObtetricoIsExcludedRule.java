package ec.gob.msp.rdacaa.business.validation.rule.atencion.obstetricia.riesgoobstetrico;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */

@Rule(order = 3)
public class RiesgoObtetricoIsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.OBSTETRICIA_CODIGO_RIESGO_OBSTETRICO);
	}

	@Then
	public RuleState then() {

		String riesgoObtetrico = getVariableFromMap(RdacaaVariableKeyCatalog.OBSTETRICIA_CODIGO_RIESGO_OBSTETRICO);
		if (!ValidationSupport.isNotDefined(riesgoObtetrico)) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_CODIGO_RIESGO_OBSTETRICO,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_CODIGO_RIESGO_OBSTETRICO_NO_DEFINIDA);
		}

		return RuleState.BREAK;

	}
}
