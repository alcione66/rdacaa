/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.numcertificadomedico;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class NumeroCertificadoMedicoIsNumericaRule extends BaseRule<String> {
	@When
	public boolean when() {
		String numerocertificadomedico = getVariableFromMap(
				RdacaaVariableKeyCatalog.DIAGNOSTICO_NUMERO_CERTIFICADO_MEDICO_UNICO);
		return !StringUtils.isNumeric(numerocertificadomedico);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.DIAGNOSTICO_NUMERO_CERTIFICADO_MEDICO_UNICO,
				ValidationResultCatalogConstants.CODIGO_ERROR_NUMERO_CERTIFICADO_MEDICO_NONUMERICO);
		return RuleState.BREAK;
	}
}
