package ec.gob.msp.rdacaa.business.validation.rule.atencion.profespecialidad;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 4)
public class EspecialidadProfesionalIsMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		return true;

	}

	@Then
	public RuleState then() {

		String especialidadProfesional = getVariableFromMap(RdacaaVariableKeyCatalog.ATENCION_CODIGO_ESPECIALIDAD);
		if (isMandatory(RdacaaVariableKeyCatalog.ATENCION_CODIGO_ESPECIALIDAD)
				&& ValidationSupport.isNotDefined(especialidadProfesional)) {
			addValidationResult(RdacaaVariableKeyCatalog.ATENCION_CODIGO_ESPECIALIDAD,
					ValidationResultCatalogConstants.CODIGO_ERROR_ATENCION_CODIGO_ESPECIALIDAD_ES_MANDATORIA);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.ATENCION_CODIGO_ESPECIALIDAD)
				&& ValidationSupport.isNotDefined(especialidadProfesional)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}

}
