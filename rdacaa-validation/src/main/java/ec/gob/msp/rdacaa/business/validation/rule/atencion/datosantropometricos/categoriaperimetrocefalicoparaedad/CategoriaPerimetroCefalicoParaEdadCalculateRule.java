package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.categoriaperimetrocefalicoparaedad;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Given;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.service.ScoreZAndCategoriaService;
import ec.gob.msp.rdacaa.business.service.SignosVitalesReglasValidacion;
import ec.gob.msp.rdacaa.business.service.SignosVitalesReglasValidacionNotFoundException;
import ec.gob.msp.rdacaa.business.service.ScoreZAndCategoriaService.PercentilDetalleNotFoundException;
import ec.gob.msp.rdacaa.business.service.ScoreZAndCategoriaService.PercentilNotFoundException;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

@Rule(order = 6)
public class CategoriaPerimetroCefalicoParaEdadCalculateRule extends UtilsRule {

	@Given("scoreZAndCategoriaService")
	private ScoreZAndCategoriaService scoreZAndCategoriaService;

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {
		
		String categoriaPerimetroCefalicoParaEdad = "999990000066666";
		String puntajeZPerimetroCefalicoParaEdad = getRdacaaRawRowResultValue(
				RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PERIMETRO_CEFALICO_PARA_EDAD);
		boolean puntajeZPerimetroCefalicoParaEdadHasErrors = variableHasErrors(
				RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PERIMETRO_CEFALICO_PARA_EDAD);
		Integer edadAnioMesDias = getEdadAnioMesDias();
		String sexo = getSexoString();
		
		boolean valuesNotDefined = ValidationSupport.isNotDefined(puntajeZPerimetroCefalicoParaEdad);
		
		setRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PERIMETRO_CEFALICO_PARA_EDAD,
				categoriaPerimetroCefalicoParaEdad);

		if (edadAnioMesDias == null ) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PERIMETRO_CEFALICO_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_CATEGORIA_PERIM_CEFALICO_EDAD_FECHAS_FORMATO_INCORRECTO);
		}

		if (sexo == null) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PERIMETRO_CEFALICO_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_CATEGORIA_PERIM_CEFALICO_EDAD_SEXO_INCORRECTO);
		}
		
		if (puntajeZPerimetroCefalicoParaEdadHasErrors) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PERIMETRO_CEFALICO_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_CATEGORIA_PERIM_CEFALICO_EDAD_PZ_CEFALICO_EDAD_ERROR);
		}

		if (valuesNotDefined) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PERIMETRO_CEFALICO_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_CATEGORIA_PERIM_CEFALICO_EDAD_PZ_CEFALICO_EDAD_NO_DEFINIDO);
		}
		
		if (edadAnioMesDias == null || sexo == null || puntajeZPerimetroCefalicoParaEdadHasErrors || valuesNotDefined) {
			return RuleState.BREAK;
		}
		
		try {
			SignosVitalesReglasValidacion categoriaPerimetroCefalicoParaEdadAlerta = scoreZAndCategoriaService
					.calcularCategoriaPerimetroCefalicoParaEdad(Double.parseDouble(puntajeZPerimetroCefalicoParaEdad), edadAnioMesDias,
							sexo);
			categoriaPerimetroCefalicoParaEdad = categoriaPerimetroCefalicoParaEdadAlerta.getIdSignoVitalAlerta().toString();
		} catch (PercentilNotFoundException | PercentilDetalleNotFoundException
				| SignosVitalesReglasValidacionNotFoundException e) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PERIMETRO_CEFALICO_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_CATEGORIA_PERIM_CEFALICO_EDAD_NO_APLICA);

			return RuleState.BREAK;
		}
		
		setRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PERIMETRO_CEFALICO_PARA_EDAD,
				categoriaPerimetroCefalicoParaEdad);

		addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PERIMETRO_CEFALICO_PARA_EDAD,
				ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_CATEGORIA_PERIM_CEFALICO_EDAD_VALOR);

		return RuleState.NEXT;
	}
}
