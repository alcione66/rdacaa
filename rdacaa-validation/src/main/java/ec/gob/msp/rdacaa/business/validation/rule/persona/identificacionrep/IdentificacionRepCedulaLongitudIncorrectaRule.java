/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.persona.identificacionrep;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;
import ec.gob.msp.rdacaa.business.service.ConstantesDetalleCatalogo;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author eduardo
 */
@Rule(order = 6)
public class IdentificacionRepCedulaLongitudIncorrectaRule extends BaseRule<String>{
    @When
	public boolean when() {
		String tipoIdentificacionRep = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_IDENTIFICACION_REPRESENTANTE);
		String identificacionRep = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION_REPRESENTANTE);
		Boolean valor = false;
		if(Integer.valueOf(tipoIdentificacionRep) == ConstantesDetalleCatalogo.DETALLECATALOGOCEDULA){
			valor = ValidationSupport.isStringMoreThanXCharacters(identificacionRep, 10);
			if(!valor){
				valor = ValidationSupport.isStringLessThanXCharacters(identificacionRep, 10);
			}
		}
		return valor;
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION_REPRESENTANTE,
				ValidationResultCatalogConstants.CODIGO_ERROR_IDENTIFICACION_REP_CEDULA_LONGITUD_INCORRECTA);
		return RuleState.BREAK;
	}
}
