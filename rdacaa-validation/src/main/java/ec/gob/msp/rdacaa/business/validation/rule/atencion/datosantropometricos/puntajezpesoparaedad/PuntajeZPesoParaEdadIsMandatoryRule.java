package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.puntajezpesoparaedad;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 4)
public class PuntajeZPesoParaEdadIsMandatoryRule extends BaseRule<String> {
	@When
	public boolean when() {
		return true;

	}

	@Then
	public RuleState then() {

		String puntajeZPesoParaEdad = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_EDAD);
		if (isMandatory(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_EDAD)
				&& ValidationSupport.isNotDefined(puntajeZPesoParaEdad)) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PUNTAJE_Z_PESO_EDAD_ES_MANDATORIA);
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}
	}
}
