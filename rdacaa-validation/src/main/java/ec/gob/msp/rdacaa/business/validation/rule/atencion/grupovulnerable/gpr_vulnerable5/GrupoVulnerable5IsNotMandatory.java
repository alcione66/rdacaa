package ec.gob.msp.rdacaa.business.validation.rule.atencion.grupovulnerable.gpr_vulnerable5;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 5)
public class GrupoVulnerable5IsNotMandatory extends BaseRule<String>{

	@When
	public boolean when() {
		String GrupoVulnerable5 = getVariableFromMap(RdacaaVariableKeyCatalog.GRUPO_VULNERABLE_5);
		return (!isMandatory(RdacaaVariableKeyCatalog.GRUPO_VULNERABLE_5) &&
				ValidationSupport.isNotDefined(GrupoVulnerable5));
		
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}
