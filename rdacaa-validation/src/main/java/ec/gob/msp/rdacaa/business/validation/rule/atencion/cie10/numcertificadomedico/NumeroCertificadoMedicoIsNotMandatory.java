package ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.numcertificadomedico;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 5)
public class NumeroCertificadoMedicoIsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		String numerocertificadomedico = getVariableFromMap(
				RdacaaVariableKeyCatalog.DIAGNOSTICO_NUMERO_CERTIFICADO_MEDICO_UNICO);
		return (!isMandatory(RdacaaVariableKeyCatalog.DIAGNOSTICO_NUMERO_CERTIFICADO_MEDICO_UNICO)
				&& ValidationSupport.isNotDefined(numerocertificadomedico));
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}
