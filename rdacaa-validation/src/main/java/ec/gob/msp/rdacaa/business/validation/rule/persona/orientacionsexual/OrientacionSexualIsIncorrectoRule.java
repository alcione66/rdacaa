package ec.gob.msp.rdacaa.business.validation.rule.persona.orientacionsexual;

import java.util.Date;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;
import ec.gob.msp.rdacaa.utilitario.enumeracion.ComunEnum;
import ec.gob.msp.rdacaa.utilitario.fecha.FechasUtil;

/**
*
* @author dmurillo
*/
@Rule(order = 7)
public class OrientacionSexualIsIncorrectoRule extends BaseRule<String> {

	@When
	public boolean when() {
		
		String sexo = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO);
		String orientacionSexual = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_ORIENTACION_SEXUAL);
		String fechaNacimiento = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO);
		Date fecNac = FechasUtil.formateadorStringAFecha(ComunEnum.PATRON_FECHA6.getDescripcion(), fechaNacimiento);
		String fechaAtencion = getVariableFromMap(RdacaaVariableKeyCatalog.ATENCION_FECHA);
		Date fecHasta = FechasUtil.formateadorStringAFecha(ComunEnum.PATRON_FECHA6.getDescripcion(), fechaAtencion);
		Integer anios = FechasUtil.getDiffDatesDesdeHasta(fecNac, fecHasta,0);
		
		return !this.validationQueryService.isCodigoOrientacionSexualValido(Integer.valueOf(orientacionSexual), Integer.valueOf(sexo), anios);
	}
                                  
	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PERSONA_CODIGO_ORIENTACION_SEXUAL,
				ValidationResultCatalogConstants.CODIGO_ERROR_ORIENTACIONSEXUAL_INCORRECTO);
		return RuleState.BREAK;
	}
}
