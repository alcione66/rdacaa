/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.atencion.grupoprioritario.gpr_prioritario3;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class GrupoPrioritario3IsNumericaRule extends BaseRule<String> {
	@When
	public boolean when() {
		String grupoprioritario3 = getVariableFromMap(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_3);
		return !StringUtils.isNumeric(grupoprioritario3);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_3,
				ValidationResultCatalogConstants.CODIGO_ERROR_GRUPO_PRIORITARIO_3_NONUMERICO);
		return RuleState.BREAK;
	}
}
