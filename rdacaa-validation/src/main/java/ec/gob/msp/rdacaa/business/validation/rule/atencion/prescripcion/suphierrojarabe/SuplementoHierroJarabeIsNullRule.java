package ec.gob.msp.rdacaa.business.validation.rule.atencion.prescripcion.suphierrojarabe;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 2)
public class SuplementoHierroJarabeIsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String suplementoHierroJarabe = getVariableFromMap(RdacaaVariableKeyCatalog.PRESCRIPCION_SUPLEMENTOS_HIERRO_JARABE);
		return ValidationSupport.isNullOrEmptyString(suplementoHierroJarabe);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PRESCRIPCION_SUPLEMENTOS_HIERRO_JARABE,
				ValidationResultCatalogConstants.CODIGO_ERROR_PRESCRIPCION_SUPLEMENTOS_HIERRO_JARABE_NULO);
		return RuleState.BREAK;
	}

}
