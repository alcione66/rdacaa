package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.valorhb;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

@Rule(order = 6)
public class ValorHBValidateForAgeRule extends UtilsRule {

    private static final Double VALOR_HB_RIESGO_MIN = 5.00d;
    private static final Double VALOR_HB_RIESGO_MAX = 30.00d;
    private static final int EDAD_SEIS_MES_CERO_DIA = 600;
    
	@When
	public boolean when() {
		return true;
	}
	
	@Then
	public RuleState then() {
		String valorHB = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB);
		Integer edadAnioMesDias = getEdadAnioMesDias();
		
		if (edadAnioMesDias == null) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_VALOR_HB_FECHAS_FORMATO_INCORRECTO);
			// error
			return RuleState.BREAK;
		}
		
		if (edadAnioMesDias < EDAD_SEIS_MES_CERO_DIA) {
			if (ValidationSupport.isNotDefined(valorHB)) {
				// no error
				return RuleState.NEXT;
				
			} else {
				addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB,
						ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_VALOR_HB_NO_ES_MANDATORIA_MENOR_000600);
				// error
				return RuleState.BREAK;
			}
		} else {
			if (ValidationSupport.isNotDefined(valorHB)) {
				// no error
				return RuleState.BREAK;
			} else {
				// validar valor hb
				return validarValorHB(valorHB);
			}
		}
	}
	
	private RuleState validarValorHB(String valorHBString) {
		Double valorHB;
		try {
			valorHB = Double.valueOf(valorHBString);
		} catch (Exception e) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_VALOR_HB_NO_ES_NUMERO_VALIDO);
			return RuleState.BREAK;
		}

		if (valorHB >= VALOR_HB_RIESGO_MIN && valorHB <= VALOR_HB_RIESGO_MAX) {
			return RuleState.NEXT;
		} else {
			ValidationResult error = getValidationResultFromMap(
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_VALOR_HB_FUERA_RANGO);
			error.setMessage(error.getMessage() + " DEBE ESTAR ENTRE LOS VALORES DE " + VALOR_HB_RIESGO_MIN + " A "
					+ VALOR_HB_RIESGO_MAX);
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB, error);
			return RuleState.BREAK;
		}
	}

}
