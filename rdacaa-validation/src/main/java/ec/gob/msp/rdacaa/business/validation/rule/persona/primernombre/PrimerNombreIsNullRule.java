package ec.gob.msp.rdacaa.business.validation.rule.persona.primernombre;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 2)
public class PrimerNombreIsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String primerApellido = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_PRIMER_NOMBRE);
		return ValidationSupport.isNullOrEmptyString(primerApellido);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PERSONA_PRIMER_NOMBRE,
				ValidationResultCatalogConstants.CODIGO_ERROR_PRIMER_NOMBRE_NULO);
		return RuleState.BREAK;
	}
}
