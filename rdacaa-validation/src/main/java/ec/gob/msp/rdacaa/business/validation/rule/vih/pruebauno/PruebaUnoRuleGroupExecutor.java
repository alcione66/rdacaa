package ec.gob.msp.rdacaa.business.validation.rule.vih.pruebauno;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.deliveredtechnologies.rulebook.model.RuleBook;

import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.RdacaaRuleExecutor;

/**
*
* @author dmurillo
*/
@Component
public class PruebaUnoRuleGroupExecutor  extends RdacaaRuleExecutor {

	@Autowired
	public PruebaUnoRuleGroupExecutor(
			@Qualifier("ruleBookReglasVihPruebaUno") RuleBook<RdacaaRawRowResult> ruleBook) {
		super();
		this.ruleBook = ruleBook;
		this.variableKey = RdacaaVariableKeyCatalog.VIH_CODIGO_PRIMERA_PRUEBA;
	}
}
