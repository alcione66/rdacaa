package ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.tipodiagnostico1;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */

@Rule(order = 3)
public class TipoDiagnostico1IsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_1);
	}

	@Then
	public RuleState then() {

		String tipodiagnostico1 = getVariableFromMap(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_1);
		if (!ValidationSupport.isNotDefined(tipodiagnostico1)) {
			addValidationResult(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_1,
					ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_DIAGNOSTICO_1_NO_DEFINIDA);
		}

		return RuleState.BREAK;

	}
}
