/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.persona.archivo;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author eduardo
 */
@Rule(order = 7)
public class ArchivoLongitudIncorrectaRule extends BaseRule<String>{
        
        @When
	public boolean when() {
		String archivo = getVariableFromMap(
				RdacaaVariableKeyCatalog.PERSONA_NUMERO_ARCHIVO);
		return ValidationSupport.isStringLessThanXCharacters(archivo, 6) || 
				ValidationSupport.isStringMoreThanXCharacters(archivo, 10);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PERSONA_NUMERO_ARCHIVO,
				ValidationResultCatalogConstants.CODIGO_ERROR_ARCHIVO_LONGITUD_INCORRECTA);
		return RuleState.BREAK;
	}
}
