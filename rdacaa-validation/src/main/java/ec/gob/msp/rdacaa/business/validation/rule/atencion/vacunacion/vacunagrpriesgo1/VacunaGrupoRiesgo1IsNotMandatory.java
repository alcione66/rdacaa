package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunagrpriesgo1;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 5)
public class VacunaGrupoRiesgo1IsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		String vacunaGrupoRiesgo1 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_GRUPO_RIESGO_1);
		return (!isMandatory(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_GRUPO_RIESGO_1)
				&& ValidationSupport.isNotDefined(vacunaGrupoRiesgo1));
	}

	@Then
	public RuleState then() {
		return RuleState.NEXT;
	}
}
