package ec.gob.msp.rdacaa.business.validation.rule.atencion.sivan.leche24horas;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 4)
public class Leche24HorasIsMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		return true;

	}

	@Then
	public RuleState then() {

		String leche24Horas = getVariableFromMap(RdacaaVariableKeyCatalog.SIVAN_RECIBIO_LECHE_ULTIMAS_24H);
		if (isMandatory(RdacaaVariableKeyCatalog.SIVAN_RECIBIO_LECHE_ULTIMAS_24H)
				&& ValidationSupport.isNotDefined(leche24Horas)) {
			addValidationResult(RdacaaVariableKeyCatalog.SIVAN_RECIBIO_LECHE_ULTIMAS_24H,
					ValidationResultCatalogConstants.CODIGO_ERROR_SIVAN_RECIBIO_LECHE_ULTIMAS_24H_ES_MANDATORIA);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.SIVAN_RECIBIO_LECHE_ULTIMAS_24H)
				&& ValidationSupport.isNotDefined(leche24Horas)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}

}
