package ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.codigocie3;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 2)
public class CodigoCie3IsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String codigocie3 = getVariableFromMap(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_3);
		return ValidationSupport.isNullOrEmptyString(codigocie3);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_3,
				ValidationResultCatalogConstants.CODIGO_ERROR_COD_DIAGNOSTICO_CIE10_3_NULO);
		return RuleState.BREAK;
	}

}
