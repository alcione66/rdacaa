package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.talla;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Given;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.service.SignosVitalesReglasValidacion;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

@Rule(order = 6)
public class TallaValidateForAgeRule extends UtilsRule {

	private static final int EDAD_CINCO_ANIO_UN_MES_CERO_DIA = 50100;
	private static final int TALLA_MAXIMA = 300;
	private static final int TALLA_MINIMA = 38;

	@Given("reglasTalla")
	private List<SignosVitalesReglasValidacion> reglasTalla;

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {
		String talla = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA);
		Integer edadAnioMesDias = getEdadAnioMesDias();
		String sexo = getSexoString();

		if (edadAnioMesDias == null) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TALLA_FECHAS_FORMATO_INCORRECTO);
		}

		if (sexo == null) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TALLA_SEXO_INCORRECTO);
		}

		if (sexo == null || edadAnioMesDias == null) {
			// error
			return RuleState.BREAK;
		}

		if (isEmbarazada() || edadAnioMesDias < EDAD_CINCO_ANIO_UN_MES_CERO_DIA) {
			if (ValidationSupport.isNotDefined(talla)) {
				addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA,
						ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TALLA_ES_MANDATORIA_MENOR_050100_ANIOS_O_EMBARAZADA);
				// error
				return RuleState.BREAK;
			} else {
				// no error
				// validar peso
				return validarTalla(talla, edadAnioMesDias, sexo);
			}
		} else {
			if (ValidationSupport.isNotDefined(talla)) {
				// no error
				return RuleState.BREAK;
			} else {
				// no error
				// validar peso
				return validarTalla(talla, edadAnioMesDias, sexo);
			}
		}

	}

	private RuleState validarTalla(String tallaString, Integer edadAnioMesDias, String sexo) {
		Double talla;
		try {
			talla = Double.valueOf(tallaString);
		} catch (Exception e) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TALLA_NO_ES_NUMERO_VALIDO);
			return RuleState.BREAK;
		}

		BigDecimal tallaBD = BigDecimal.valueOf(talla);

		if (talla >= TALLA_MINIMA && talla <= TALLA_MAXIMA) {

			Optional<ValidationResult> procesarTallaParaEdad = procesarTallaParaEdad(tallaBD, edadAnioMesDias, sexo);
			if (procesarTallaParaEdad.isPresent()) {
				addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, procesarTallaParaEdad.get());
			}

			return RuleState.NEXT;
		} else {
			ValidationResult error = getValidationResultFromMap(
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TALLA_FUERA_RANGO);
			error.setMessage(error.getMessage() + " DEBE ESTAR ENTRE LOS VALORES DE " + TALLA_MINIMA + " A "
					+ TALLA_MAXIMA + " cm");
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, error);
			return RuleState.BREAK;
		}
	}

	private Optional<ValidationResult> procesarTallaParaEdad(BigDecimal talla, Integer edad, String sexo) {
		for (SignosVitalesReglasValidacion regla : reglasTalla) {
			if (isEdadPacienteDentroRango(edad, regla.getEdadminima(), regla.getEdadmaxima())
					&& isSexoCorrespondiente(sexo, regla.getSexo())
					&& isValorFueraDeRango(talla, regla.getValorminimo(), regla.getValormaximo())) {
				ValidationResult warning = getValidationResultFromMap(
						ValidationResultCatalogConstants.CODIGO_ADV_DAT_ANT_TALLA_FUERA_RANGO_PARA_EDAD);
				String message = warning.getMessage();
				message = message.replace("act", talla.toString());
				message = message.replace("tallamin", regla.getValorminimo().toString());
				message = message.replace("tallamax", regla.getValormaximo().toString());
				message = message.replace("edadmin", regla.getEdadminimaAnios() + " años " + regla.getEdadminimaMeses()
						+ " meses " + regla.getEdadminimaDias() + " días");
				message = message.replace("edadmax", regla.getEdadmaximaAnios() + " años " + regla.getEdadmaximaMeses()
						+ " meses " + regla.getEdadmaximaDias() + " días");
				warning.setMessage(message);
				return Optional.of(warning);
			}
		}
		return Optional.empty();
	}

}
