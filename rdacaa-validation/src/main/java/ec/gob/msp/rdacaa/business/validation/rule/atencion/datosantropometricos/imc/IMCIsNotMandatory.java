package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.imc;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 5)
public class IMCIsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		
		String imc = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_INDICE_MASA_CORPORAL);
		return (!isMandatory(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_INDICE_MASA_CORPORAL) &&
				ValidationSupport.isNotDefined(imc));
		
	}

	@Then
	public RuleState then() {
		return RuleState.NEXT;
	}
}
