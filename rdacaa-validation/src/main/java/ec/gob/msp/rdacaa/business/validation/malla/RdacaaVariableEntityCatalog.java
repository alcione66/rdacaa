package ec.gob.msp.rdacaa.business.validation.malla;

import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ec.gob.msp.rdacaa.business.entity.Variable;
import ec.gob.msp.rdacaa.business.service.VariableService;

@Component
public class RdacaaVariableEntityCatalog {

	@Autowired
	private VariableService variableService;

	private Map<String, Variable> variableMap;
	
	private Set<String> setVariablesStrings;
	
	@PostConstruct
	private void init() {
		variableMap = new ConcurrentHashMap<>();
		setVariablesStrings = new HashSet<>();
		List<Variable> variables = variableService.findAll();
		variables.forEach(variable -> { variableMap.put(variable.getCodigo(), variable);
		setVariablesStrings.add(variable.getCodigo());
		});
	}

	public Variable get(String key) {
		return variableMap.get(key);
	}

	public Map<String, Variable> getVariableMap() {
		return variableMap;
	}
	
	public Set<String> getSetVariablesStrings(){
		return new HashSet<>(setVariablesStrings);
	}

}
