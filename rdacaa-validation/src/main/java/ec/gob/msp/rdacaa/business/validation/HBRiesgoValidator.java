/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalog;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import static ec.gob.msp.rdacaa.business.validation.common.ValidationSupport.isNullOrEmptyString;
import ec.gob.msp.rdacaa.business.validation.common.Validator;
import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import org.springframework.stereotype.Component;

/**
 *
 * @author msp
 */
@Component
public class HBRiesgoValidator extends ValidationSupport implements Validator<String[]> {

    private static final BigDecimal VALOR_HB_RIESGO_MIN = new BigDecimal("5.00");
    private static final BigDecimal VALOR_HB_RIESGO_MAX = new BigDecimal("30.00");
    private static final String ALERTA_ROJA = "rojo";

    @Override
    public List<ValidationResult> validate(String[] valorHBRiesgo) {

        List<ValidationResult> listaErrores = new ArrayList<>();

        if (isNullOrEmptyString(valorHBRiesgo[0])) {
            listaErrores.add(ValidationResultCatalog.HB_RIESGO_VACIO.getValidationError());
        } else {
            BigDecimal valor = new BigDecimal(valorHBRiesgo[0]);
            Optional<ValidationResult> warning = procesarHBRiesgo(valor);
            if (warning.isPresent()) {
                listaErrores.add(warning.get());
            }
        }

        return listaErrores;

    }

    private Optional<ValidationResult> procesarHBRiesgo(BigDecimal valor) {

        if (valor.compareTo(VALOR_HB_RIESGO_MIN) < 0) {
            ValidationResult warning = ValidationResultCatalog.HB_RIESGO_ALERTA_MIN.getValidationError();
            warning = new ValidationResult(warning.getCode(), warning.getMessage(), ALERTA_ROJA);
            return Optional.of(warning);
        } else if (valor.compareTo(VALOR_HB_RIESGO_MAX) > 0) {
            ValidationResult warning = ValidationResultCatalog.HB_RIESGO_ALERTA_MAX.getValidationError();
            warning = new ValidationResult(warning.getCode(), warning.getMessage(), ALERTA_ROJA);
            return Optional.of(warning);
        }
        return Optional.empty();
    }

}
