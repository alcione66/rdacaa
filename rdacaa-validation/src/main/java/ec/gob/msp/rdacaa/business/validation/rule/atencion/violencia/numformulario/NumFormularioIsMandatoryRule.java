/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.atencion.violencia.numformulario;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author eduardo
 */
@Rule(order = 4)
public class NumFormularioIsMandatoryRule extends BaseRule<String> {
 
    @When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {

		String parroquia = getVariableFromMap(RdacaaVariableKeyCatalog.VIOLENCIA_NUMERO_SERIE);
		if (isMandatory(RdacaaVariableKeyCatalog.VIOLENCIA_NUMERO_SERIE)
				&& ValidationSupport.isNotDefined(parroquia)) {
			addValidationResult(RdacaaVariableKeyCatalog.VIOLENCIA_NUMERO_SERIE,
					ValidationResultCatalogConstants.CODIGO_ERROR_VIOLENCIA_NUMFORMULARIO_ES_MANDATORIA);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.VIOLENCIA_NUMERO_SERIE)
				&& ValidationSupport.isNotDefined(parroquia)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}
}
