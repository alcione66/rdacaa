package ec.gob.msp.rdacaa.business.validation.rule.atencion.referencia.subsistema;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 6)
public class SubsistemaReferenciaIsNumericaRule extends BaseRule<String> {
	
	@When
	public boolean when() {
		String subsistemaReferencia = getVariableFromMap(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_SUBSISTEMA);
		return !StringUtils.isNumeric(subsistemaReferencia);
	}
	
	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_SUBSISTEMA,
				ValidationResultCatalogConstants.CODIGO_ERROR_REFERENCIA_SUBSISTEMA_NO_NUMERICO);
		return RuleState.BREAK;
	}

}
