package ec.gob.msp.rdacaa.business.validation.rule.atencion.prescripcion.suphierromultivita;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.deliveredtechnologies.rulebook.model.RuleBook;

import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.RdacaaRuleExecutor;

@Component
public class SuplementoHierroMultivitaminasRuleGroupExecutor extends RdacaaRuleExecutor {

	@Autowired
	public SuplementoHierroMultivitaminasRuleGroupExecutor(
			@Qualifier("ReglasSuplementoHierroMultivitaminas") RuleBook<RdacaaRawRowResult> ruleBook) {
		super();
		this.ruleBook = ruleBook;
		this.variableKey = RdacaaVariableKeyCatalog.PRESCRIPCION_SUPLEMENTOS_HIERRO_MULTIVITAMINAS;
	}
}
