package ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.codigocie2;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 7)
public class CodigoCie2LongitudIncorrectaRule extends BaseRule<String> {

	@When
	public boolean when() {
		String codigocie2 = getVariableFromMap(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_2);
		Boolean valor = false;
		valor = ValidationSupport.isStringMoreThanXCharacters(codigocie2, 5);
		if (!valor) {
			valor = ValidationSupport.isStringLessThanXCharacters(codigocie2, 3);
		}

		return valor;
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_2,
				ValidationResultCatalogConstants.CODIGO_ERROR_COD_DIAGNOSTICO_CIE10_2_LONGITUD_INCORRECTA);
		return RuleState.BREAK;
	}
}
