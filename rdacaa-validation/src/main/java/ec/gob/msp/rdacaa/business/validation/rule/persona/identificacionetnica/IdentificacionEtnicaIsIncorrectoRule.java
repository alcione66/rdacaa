package ec.gob.msp.rdacaa.business.validation.rule.persona.identificacionetnica;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.service.ConstantesDetalleCatalogo;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 6)
public class IdentificacionEtnicaIsIncorrectoRule extends BaseRule<String> {
	

	@When
	public boolean when() {
		String pais = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD);
		String identificacionEtnica = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_AUTOIDENTIFICACION_ETNICA);
		boolean valor = true;
		if(pais != null){
			if(Integer.valueOf(pais) == ConstantesDetalleCatalogo.NACIONALIDADECUADOR){
				valor = !this.validationQueryService.isIdentidadEtnicaValido(Integer.valueOf(identificacionEtnica));
			}else{
				if(identificacionEtnica.equals(ConstantesDetalleCatalogo.NOAPLICAAUTOIDENTIFICACIONETNICA)){
					valor = false;
				}
			}
		}
		return valor;
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PERSONA_CODIGO_AUTOIDENTIFICACION_ETNICA,
				ValidationResultCatalogConstants.CODIGO_ERROR_IDENTIFICACION_ETNICA_INCORRECTO);
		return RuleState.BREAK;
	}
}
