package ec.gob.msp.rdacaa.business.validation.rule.atencion.exameneslab.sifilisnotreponemica;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.deliveredtechnologies.rulebook.model.RuleBook;

import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.RdacaaRuleExecutor;

@Component
public class SifilisNoTreponemicaRuleGroupExecutor extends RdacaaRuleExecutor {
	
	@Autowired
	public SifilisNoTreponemicaRuleGroupExecutor(
			@Qualifier("ReglasSifilisNoTreponemica") RuleBook<RdacaaRawRowResult> ruleBook) {
		super();
		this.ruleBook = ruleBook;
		this.variableKey = RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_SIFILIS_NO_TREPONEMICA_RESULTADO;
	}
}
