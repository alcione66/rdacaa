package ec.gob.msp.rdacaa.business.validation.rule.persona.tipoidentificacion;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author dmurillo
 */
@Rule(order = 6)
public class TipoIdentificacionIsNumericaRule extends BaseRule<String> {
	
	@When
	public boolean when() {
		String tipoIdentificacion = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_IDENTIFICACION);
		return !StringUtils.isNumeric(tipoIdentificacion);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_IDENTIFICACION,
				ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_IDENTIFICACION_NUMERICA);
		return RuleState.BREAK;
	}

}
