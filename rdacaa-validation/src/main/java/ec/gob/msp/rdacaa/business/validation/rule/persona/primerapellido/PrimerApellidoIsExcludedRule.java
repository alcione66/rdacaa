package ec.gob.msp.rdacaa.business.validation.rule.persona.primerapellido;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/

@Rule(order = 3)
public class PrimerApellidoIsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.PERSONA_PRIMER_APELLIDO);
	}

	@Then
	public RuleState then() {
		
		String tipoIdentificacion = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_PRIMER_APELLIDO);
		if(!ValidationSupport.isNotDefined(tipoIdentificacion)){
			addValidationResult(RdacaaVariableKeyCatalog.PERSONA_PRIMER_APELLIDO,
					ValidationResultCatalogConstants.CODIGO_ERROR_PRIMER_APELLIDO_NO_DEFINIDO);
		}
		
		return RuleState.BREAK;

	}
}
