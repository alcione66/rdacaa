package ec.gob.msp.rdacaa.business.validation.rule.atencion.odontologia.perdido;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 5)
public class PerdidosIsNotMandatory extends BaseRule<String> {
	@When
	public boolean when() {
		
		String dientesPerdidas = getVariableFromMap(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_PERDIDAS);
		return (!isMandatory(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_PERDIDAS) &&
				ValidationSupport.isNotDefined(dientesPerdidas));
		
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}
