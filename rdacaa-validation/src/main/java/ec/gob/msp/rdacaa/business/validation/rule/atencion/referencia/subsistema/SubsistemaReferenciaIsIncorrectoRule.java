package ec.gob.msp.rdacaa.business.validation.rule.atencion.referencia.subsistema;

import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 7)
public class SubsistemaReferenciaIsIncorrectoRule extends BaseRule<String> {

	@When
	public boolean when() {
		String subsistemaReferencia = getVariableFromMap(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_SUBSISTEMA);
		return !this.validationQueryService.isCodigoSubsistemaReferenciaValido(Integer.valueOf(subsistemaReferencia));
	}

	@Then
	public void then() {
		addValidationResult(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_SUBSISTEMA,
				ValidationResultCatalogConstants.CODIGO_ERROR_REFERENCIA_SUBSISTEMA_INCORRECTO);
	}
}
