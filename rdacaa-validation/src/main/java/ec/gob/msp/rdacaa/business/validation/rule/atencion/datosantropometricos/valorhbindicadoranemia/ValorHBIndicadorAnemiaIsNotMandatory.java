package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.valorhbindicadoranemia;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 5)
public class ValorHBIndicadorAnemiaIsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		
		String indicadorAnemia = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_INDICADOR_ANEMIA);
		return (!isMandatory(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_INDICADOR_ANEMIA) &&
				ValidationSupport.isNotDefined(indicadorAnemia));
		
	}

	@Then
	public RuleState then() {
		return RuleState.NEXT;
	}
}
