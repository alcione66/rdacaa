package ec.gob.msp.rdacaa.business.validation.rule.atencion.obstetricia.plantransporte;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 2)
public class PlanTransporteIsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String planTransporte = getVariableFromMap(RdacaaVariableKeyCatalog.OBSTETRICIA_TIENE_PLAN_TRANSPORTE);
		return ValidationSupport.isNullOrEmptyString(planTransporte);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_TIENE_PLAN_TRANSPORTE,
				ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_TIENE_PLAN_TRANSPORTE_NULO);
		return RuleState.BREAK;
	}

}
