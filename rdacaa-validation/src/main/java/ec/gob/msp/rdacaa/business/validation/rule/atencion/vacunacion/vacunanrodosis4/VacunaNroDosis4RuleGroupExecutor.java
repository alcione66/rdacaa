package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunanrodosis4;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.deliveredtechnologies.rulebook.model.RuleBook;

import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.RdacaaRuleExecutor;

@Component
public class VacunaNroDosis4RuleGroupExecutor extends RdacaaRuleExecutor {

	@Autowired
	public VacunaNroDosis4RuleGroupExecutor(
			@Qualifier("ReglasVacunaNroDosis4") RuleBook<RdacaaRawRowResult> ruleBook) {
		super();
		this.ruleBook = ruleBook;
		this.variableKey = RdacaaVariableKeyCatalog.VACUNAS_CODIGO_DOSIS_4;
	}
}
