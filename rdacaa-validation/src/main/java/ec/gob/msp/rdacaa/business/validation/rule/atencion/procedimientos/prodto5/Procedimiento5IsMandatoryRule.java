package ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.prodto5;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 4)
public class Procedimiento5IsMandatoryRule extends BaseRule<String> {
	@When
	public boolean when() {
		return true;

	}
	
	@Then
	public RuleState then() {

		String procedimiento5 = getVariableFromMap(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_5);
		if (isMandatory(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_5)
				&& ValidationSupport.isNotDefined(procedimiento5)) {
			addValidationResult(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_5,
					ValidationResultCatalogConstants.CODIGO_ERROR_PROCEDIMIENTO_5_ES_MANDATORIO);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_5)
				&& ValidationSupport.isNotDefined(procedimiento5)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}
}
