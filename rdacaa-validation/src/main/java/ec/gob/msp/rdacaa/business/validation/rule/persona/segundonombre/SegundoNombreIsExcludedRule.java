package ec.gob.msp.rdacaa.business.validation.rule.persona.segundonombre;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/

@Rule(order = 2)
public class SegundoNombreIsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_NOMBRE);
	}

	@Then
	public RuleState then() {
		
		String tipoIdentificacion = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_NOMBRE);
		if(!ValidationSupport.isNotDefined(tipoIdentificacion)){
			addValidationResult(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_NOMBRE,
					ValidationResultCatalogConstants.CODIGO_ERROR_SEGUNDO_NOMBRE_NO_DEFINIDO);
		}
		
		return RuleState.BREAK;

	}
}
