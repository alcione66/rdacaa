package ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.actividad3;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 4)
public class Actividades3IsMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		return true;

	}

	@Then
	public RuleState then() {

		String actividades3 = getVariableFromMap(
				RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_3);
		if (isMandatory(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_3)
				&& ValidationSupport.isNotDefined(actividades3)) {
			addValidationResult(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_3,
					ValidationResultCatalogConstants.CODIGO_ERROR_ACTIVIDAD_3_ES_MANDATORIO);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_3)
				&& ValidationSupport.isNotDefined(actividades3)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}
}
