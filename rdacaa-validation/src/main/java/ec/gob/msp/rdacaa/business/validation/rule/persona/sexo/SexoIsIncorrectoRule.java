package ec.gob.msp.rdacaa.business.validation.rule.persona.sexo;

import java.util.Date;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;
import ec.gob.msp.rdacaa.utilitario.enumeracion.ComunEnum;
import ec.gob.msp.rdacaa.utilitario.fecha.FechasUtil;

/**
*
* @author dmurillo
*/
@Rule(order = 7)
public class SexoIsIncorrectoRule extends BaseRule<String> {

	@When
	public boolean when() {
		String sexo = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO);
		String fechaNacimiento = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO);
		Date fecNac = FechasUtil.formateadorStringAFecha(ComunEnum.PATRON_FECHA6.getDescripcion(), fechaNacimiento);
		String fechaAtencion = getVariableFromMap(RdacaaVariableKeyCatalog.ATENCION_FECHA);
		Date fecHasta = FechasUtil.formateadorStringAFecha(ComunEnum.PATRON_FECHA6.getDescripcion(), fechaAtencion);
		Integer anios = FechasUtil.getDiffDatesDesdeHasta(fecNac, fecHasta,0);
		Integer meses = FechasUtil.getDiffDatesDesdeHasta(fecNac, fecHasta,1);
		Integer dias = FechasUtil.getDiffDatesDesdeHasta(fecNac, fecHasta,2);
		return !this.validationQueryService.isCodigoSexoValido(Integer.valueOf(sexo), anios, meses, dias );
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO,
				ValidationResultCatalogConstants.CODIGO_ERROR_SEXO_INCORRECTO);
		return RuleState.BREAK;
	}
}
