package ec.gob.msp.rdacaa.business.validation.rule.atencion.grupoprioritario.lugaratencion;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */

@Rule(order = 3)
public class LugarAtencionIsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_LUGAR_ATENCION);
	}

	@Then
	public RuleState then() {

		String lugaratencion = getVariableFromMap(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_LUGAR_ATENCION);
		if (!ValidationSupport.isNotDefined(lugaratencion)) {
			addValidationResult(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_LUGAR_ATENCION,
					ValidationResultCatalogConstants.CODIGO_ERROR_LUGAR_ATENCION_ESTABLECIMIENTO_NO_DEFINIDA);
		}

		return RuleState.BREAK;

	}
}
