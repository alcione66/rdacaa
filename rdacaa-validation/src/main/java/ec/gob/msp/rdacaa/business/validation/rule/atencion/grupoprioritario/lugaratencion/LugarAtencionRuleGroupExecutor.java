package ec.gob.msp.rdacaa.business.validation.rule.atencion.grupoprioritario.lugaratencion;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.deliveredtechnologies.rulebook.model.RuleBook;

import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.RdacaaRuleExecutor;

@Component
public class LugarAtencionRuleGroupExecutor extends RdacaaRuleExecutor {
    
    @Autowired
    public LugarAtencionRuleGroupExecutor(@Qualifier("ReglasLugarAtencion") RuleBook<RdacaaRawRowResult> ruleBook){
        super();
        this.ruleBook = ruleBook;
        this.variableKey = RdacaaVariableKeyCatalog.ESTABLECIMIENTO_LUGAR_ATENCION;
    }

}
