package ec.gob.msp.rdacaa.business.validation.rule.persona.pueblos;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 2)
public class PueblosIsNullRule  extends BaseRule<String>  {

	@When
	public boolean when() {
		String pueblo = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_PUEBLO);
		return ValidationSupport.isNullOrEmptyString(pueblo);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PERSONA_CODIGO_PUEBLO,
				ValidationResultCatalogConstants.CODIGO_ERROR_PUEBLOS_NULO);
		return RuleState.BREAK;
	}
}
