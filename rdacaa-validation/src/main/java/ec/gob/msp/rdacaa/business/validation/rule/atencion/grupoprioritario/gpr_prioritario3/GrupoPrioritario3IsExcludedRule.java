package ec.gob.msp.rdacaa.business.validation.rule.atencion.grupoprioritario.gpr_prioritario3;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */

@Rule(order = 3)
public class GrupoPrioritario3IsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_3);
	}

	@Then
	public RuleState then() {

		String grupoprioritario3 = getVariableFromMap(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_3);
		if (!ValidationSupport.isNotDefined(grupoprioritario3)) {
			addValidationResult(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_3,
					ValidationResultCatalogConstants.CODIGO_ERROR_GRUPO_PRIORITARIO_3_NO_DEFINIDA);
		}

		return RuleState.BREAK;

	}
}
