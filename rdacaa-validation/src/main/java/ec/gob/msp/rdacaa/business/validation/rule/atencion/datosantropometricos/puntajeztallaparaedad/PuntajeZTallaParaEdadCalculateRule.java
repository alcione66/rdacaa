package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.puntajeztallaparaedad;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Given;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.service.ScoreZAndCategoriaService;
import ec.gob.msp.rdacaa.business.service.SignosVitalesReglasValidacionNotFoundException;
import ec.gob.msp.rdacaa.business.service.ScoreZAndCategoriaService.PercentilDetalleNotFoundException;
import ec.gob.msp.rdacaa.business.service.ScoreZAndCategoriaService.PercentilNotFoundException;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

@Rule(order = 6)
public class PuntajeZTallaParaEdadCalculateRule extends UtilsRule {

	@Given("scoreZAndCategoriaService")
	private ScoreZAndCategoriaService scoreZAndCategoriaService;

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {
		String puntajeZTallaParaEdad = "999990000066666";
		String tallaCorregida = getRdacaaRawRowResultValue(
				RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA);
		boolean tallaCorregidaHasErrors = variableHasErrors(
				RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA);
		Integer edadAnioMesDias = getEdadAnioMesDias();
		Integer edadEnMeses = getEdadMeses();
		Integer edadEnDias = getEdadDias();
		String sexo = getSexoString();
		
		boolean valuesNotDefined = ValidationSupport.isNotDefined(tallaCorregida);

		setRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_TALLA_PARA_EDAD,
				puntajeZTallaParaEdad);

		if (edadAnioMesDias == null || edadEnMeses == null || edadEnDias == null) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_TALLA_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PUNTAJE_Z_TALLA_EDAD_FECHAS_FORMATO_INCORRECTO);
		}

		if (tallaCorregidaHasErrors) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_TALLA_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PUNTAJE_Z_TALLA_EDAD_TALLA_CORREGIDA_TIENE_ERRORES);
		}

		if (sexo == null) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_TALLA_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PUNTAJE_Z_TALLA_EDAD_SEXO_INCORRECTO);
		}
		
		if(valuesNotDefined) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_TALLA_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_PUNTAJE_Z_TALLA_EDAD_TALLA_CORREGIDA_NO_DEFINIDO);
		}

		if ((edadAnioMesDias == null || edadEnMeses == null || edadEnDias == null) || sexo == null
				|| tallaCorregidaHasErrors || valuesNotDefined) {
			return RuleState.BREAK;
		}

		try {
			Double scoreZTallaParaEdad = scoreZAndCategoriaService.calcularScoreZTallaParaEdad(
					Double.parseDouble(tallaCorregida), edadAnioMesDias, edadEnMeses, edadEnDias, sexo);
			puntajeZTallaParaEdad = scoreZTallaParaEdad.toString();
		} catch (PercentilNotFoundException | PercentilDetalleNotFoundException
				| SignosVitalesReglasValidacionNotFoundException e) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_TALLA_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_PUNTAJE_Z_TALLA_EDAD_NO_APLICA);
			
			return RuleState.BREAK;
		}

		setRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_TALLA_PARA_EDAD,
				puntajeZTallaParaEdad);
		
		addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_TALLA_PARA_EDAD,
				ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_PUNTAJE_Z_TALLA_EDAD_VALOR);
		
		return RuleState.NEXT;
	}
}
