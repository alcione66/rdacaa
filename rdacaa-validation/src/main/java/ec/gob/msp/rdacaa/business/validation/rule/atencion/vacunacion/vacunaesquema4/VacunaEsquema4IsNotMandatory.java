package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunaesquema4;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 5)
public class VacunaEsquema4IsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		String vacunaEsquema4 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_ESQUEMA_4);
		return (!isMandatory(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_ESQUEMA_4)
				&& ValidationSupport.isNotDefined(vacunaEsquema4));
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}
