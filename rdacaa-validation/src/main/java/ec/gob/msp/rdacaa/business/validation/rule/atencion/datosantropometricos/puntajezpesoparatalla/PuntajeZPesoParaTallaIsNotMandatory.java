package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.puntajezpesoparatalla;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 5)
public class PuntajeZPesoParaTallaIsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		
		String puntajeZPesoParaTalla = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_TALLA);
		return (!isMandatory(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_TALLA) &&
				ValidationSupport.isNotDefined(puntajeZPesoParaTalla));
		
	}

	@Then
	public RuleState then() {
		return RuleState.NEXT;
	}
}
