package ec.gob.msp.rdacaa.business.validation.rule.atencion.sivan.alimentos24horas;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 5)
public class Alimentos24HorasIsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		String alimentos24Horas = getVariableFromMap(RdacaaVariableKeyCatalog.SIVAN_RECIBIO_ALIMENTOS_SOLIDOS_ULTIMAS_24H);
		return (!isMandatory(RdacaaVariableKeyCatalog.SIVAN_RECIBIO_ALIMENTOS_SOLIDOS_ULTIMAS_24H)
				&& ValidationSupport.isNotDefined(alimentos24Horas));
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}
