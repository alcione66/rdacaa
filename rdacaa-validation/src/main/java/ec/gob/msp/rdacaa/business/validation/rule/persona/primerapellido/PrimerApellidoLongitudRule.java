package ec.gob.msp.rdacaa.business.validation.rule.persona.primerapellido;

import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 6)
public class PrimerApellidoLongitudRule extends BaseRule<String> {

	private static final int TRES_CARACTERES = 3;

	@When
	public boolean when() {
		String primerApellido = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_PRIMER_APELLIDO);
		return ValidationSupport.isStringLessThanXCharacters(primerApellido, TRES_CARACTERES);
	}

	@Then
	public void then() {
		addValidationResult(RdacaaVariableKeyCatalog.PERSONA_PRIMER_APELLIDO,
				ValidationResultCatalogConstants.CODIGO_ERROR_PRIMER_APELLIDO_LONG);
	}

}
