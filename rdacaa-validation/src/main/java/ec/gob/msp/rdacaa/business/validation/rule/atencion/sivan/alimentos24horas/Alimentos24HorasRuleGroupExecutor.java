package ec.gob.msp.rdacaa.business.validation.rule.atencion.sivan.alimentos24horas;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.deliveredtechnologies.rulebook.model.RuleBook;

import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.RdacaaRuleExecutor;

@Component
public class Alimentos24HorasRuleGroupExecutor extends RdacaaRuleExecutor {

	@Autowired
	public Alimentos24HorasRuleGroupExecutor(
			@Qualifier("Reglas24HorasAlimentos") RuleBook<RdacaaRawRowResult> ruleBook) {
		super();
		this.ruleBook = ruleBook;
		this.variableKey = RdacaaVariableKeyCatalog.SIVAN_RECIBIO_ALIMENTOS_SOLIDOS_ULTIMAS_24H;
	}
}
