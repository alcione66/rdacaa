package ec.gob.msp.rdacaa.business.validation.rule.vih.cd4;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 4)
public class CD4IsMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {

		String prueba = getVariableFromMap(RdacaaVariableKeyCatalog.VIH_CD4);
		if (isMandatory(RdacaaVariableKeyCatalog.VIH_CD4)
				&& ValidationSupport.isNotDefined(prueba)) {
			addValidationResult(RdacaaVariableKeyCatalog.VIH_CD4,
					ValidationResultCatalogConstants.CODIGO_ERROR_VIH_CD4_ES_MANDATORIA);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.VIH_CD4)
				&& ValidationSupport.isNotDefined(prueba)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}
}
