package ec.gob.msp.rdacaa.business.validation.common;

import java.io.Serializable;
import java.util.List;

public interface Validator <K> extends Serializable{

    List<ValidationResult> validate(K k);

}
