package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunafecha2;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 5)
public class VacunaFecha2IsNotMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		String vacunaFecha2 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_FECHA_2);
		return (!isMandatory(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_FECHA_2)
				&& ValidationSupport.isNotDefined(vacunaFecha2));
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}
