package ec.gob.msp.rdacaa.business.validation.rule;

import java.math.BigDecimal;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;

import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.utilitario.enumeracion.ComunEnum;
import ec.gob.msp.rdacaa.utilitario.fecha.FechasUtil;

public class UtilsRule extends BaseRule<String> {

	private static final String COD_INTERSEXUAL = "114";
	private static final String COD_MUJER = "17";
	private static final String COD_HOMBRE = "16";
	
	private static final String AMBOS_SEXOS = "A";

	public String getSexoString() {
		String sexo = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO);
		if(COD_HOMBRE.equals(sexo)) {
			return "M";
		}else if(COD_MUJER.equals(sexo)) {
			return "F";
		}else if (COD_INTERSEXUAL.equals(sexo)) {
			return "I";
		}else {
			return null;
		}
	}
	
	public Integer getSexoInteger() {
		Integer sexoId = null;
		try {
			sexoId = Integer.parseInt(getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO));
		}catch (Exception e) {
			sexoId = null;
		}
		return sexoId;
	}
	
	public boolean isEmbarazada() {

		List<String> listaGrupoPrioritarios = new ArrayList<>();
		listaGrupoPrioritarios.add(getVariableFromMap(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1));
		listaGrupoPrioritarios.add(getVariableFromMap(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_2));
		listaGrupoPrioritarios.add(getVariableFromMap(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_3));
		listaGrupoPrioritarios.add(getVariableFromMap(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_4));
		listaGrupoPrioritarios.add(getVariableFromMap(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_5));

		return listaGrupoPrioritarios.stream()
				.anyMatch(gpr -> ComunEnum.CATALOGO_ID_EMBARAZADA.getDescripcion().equals(gpr));
	}

	public Integer getEdadAnioMesDias() {
		String fechaNacimiento = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO);
		String fechaAtencion = getVariableFromMap(RdacaaVariableKeyCatalog.ATENCION_FECHA);
		Date fecNac = null;
		Date fecHasta = null;
		Integer edadAnioMesDia = null;
		try {
			fecNac = FechasUtil.formateadorStringAFecha(ComunEnum.PATRON_FECHA6.getDescripcion(), fechaNacimiento);
			fecHasta = FechasUtil.formateadorStringAFecha(ComunEnum.PATRON_FECHA6.getDescripcion(), fechaAtencion);
			if(fecNac != null && fecHasta != null) {
				Integer anios = FechasUtil.getDiffDatesDesdeHasta(fecNac, fecHasta, 0);
				Integer meses = FechasUtil.getDiffDatesDesdeHasta(fecNac, fecHasta, 1);
				Integer dias = FechasUtil.getDiffDatesDesdeHasta(fecNac, fecHasta, 2);
				edadAnioMesDia = Integer.parseInt(concatenarEdad(anios, meses, dias));
			}
		} catch (Exception e) {
			// Excepcion gestionada
		}

		return edadAnioMesDia;
	}
	
	public Integer getEdadAnioParcial() {
		String fechaNacimiento = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO);
		String fechaAtencion = getVariableFromMap(RdacaaVariableKeyCatalog.ATENCION_FECHA);
		Date fecNac = null;
		Date fecHasta = null;
		Integer edadAnio = null;
		try {
			fecNac = FechasUtil.formateadorStringAFecha(ComunEnum.PATRON_FECHA6.getDescripcion(), fechaNacimiento);
			fecHasta = FechasUtil.formateadorStringAFecha(ComunEnum.PATRON_FECHA6.getDescripcion(), fechaAtencion);
			if(fecNac != null && fecHasta != null) {
				edadAnio = FechasUtil.getDiffDatesDesdeHasta(fecNac, fecHasta, 0);
			}
		} catch (Exception e) {
			// Excepcion gestionada
		}

		return edadAnio;
	}
	
	public Integer getEdadMesParcial() {
		String fechaNacimiento = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO);
		String fechaAtencion = getVariableFromMap(RdacaaVariableKeyCatalog.ATENCION_FECHA);
		Date fecNac = null;
		Date fecHasta = null;
		Integer edadMes = null;
		try {
			fecNac = FechasUtil.formateadorStringAFecha(ComunEnum.PATRON_FECHA6.getDescripcion(), fechaNacimiento);
			fecHasta = FechasUtil.formateadorStringAFecha(ComunEnum.PATRON_FECHA6.getDescripcion(), fechaAtencion);
			if(fecNac != null && fecHasta != null) {
				edadMes = FechasUtil.getDiffDatesDesdeHasta(fecNac, fecHasta, 1);
			}
		} catch (Exception e) {
			// Excepcion gestionada
		}

		return edadMes;
	}
	
	public Integer getEdadDiasParcial() {
		String fechaNacimiento = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO);
		String fechaAtencion = getVariableFromMap(RdacaaVariableKeyCatalog.ATENCION_FECHA);
		Date fecNac = null;
		Date fecHasta = null;
		Integer edadMesParcial = null;
		try {
			fecNac = FechasUtil.formateadorStringAFecha(ComunEnum.PATRON_FECHA6.getDescripcion(), fechaNacimiento);
			fecHasta = FechasUtil.formateadorStringAFecha(ComunEnum.PATRON_FECHA6.getDescripcion(), fechaAtencion);
			if(fecNac != null && fecHasta != null) {
				edadMesParcial = FechasUtil.getDiffDatesDesdeHasta(fecNac, fecHasta, 2);
			}
		} catch (Exception e) {
			// Excepcion gestionada
		}

		return edadMesParcial;
	}
	
	public Integer getEdadAnios() {
		String fechaNacimiento = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO);
		String fechaAtencion = getVariableFromMap(RdacaaVariableKeyCatalog.ATENCION_FECHA);
		Date fecNac = null;
		Date fecHasta = null;
		Integer edadAnios = null;
		try {
			fecNac = FechasUtil.formateadorStringAFecha(ComunEnum.PATRON_FECHA6.getDescripcion(), fechaNacimiento);
			fecHasta = FechasUtil.formateadorStringAFecha(ComunEnum.PATRON_FECHA6.getDescripcion(), fechaAtencion);
			edadAnios =  (int) ChronoUnit.YEARS
					.between(fecNac.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(), fecHasta.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
		} catch (Exception e) {
			// Excepcion gestionada
		}
		return edadAnios;
	}
	
	public Integer getEdadMeses() {
		String fechaNacimiento = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO);
		String fechaAtencion = getVariableFromMap(RdacaaVariableKeyCatalog.ATENCION_FECHA);
		Date fecNac = null;
		Date fecHasta = null;
		Integer edadMeses = null;
		try {
			fecNac = FechasUtil.formateadorStringAFecha(ComunEnum.PATRON_FECHA6.getDescripcion(), fechaNacimiento);
			fecHasta = FechasUtil.formateadorStringAFecha(ComunEnum.PATRON_FECHA6.getDescripcion(), fechaAtencion);
			edadMeses =  (int) ChronoUnit.MONTHS
					.between(fecNac.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(), fecHasta.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
		} catch (Exception e) {
			// Excepcion gestionada
		}
		return edadMeses;
	}
	
	public boolean isFechaValida(String fecha) {
		Date fecNac = null;
		try {
			fecNac = FechasUtil.formateadorStringAFecha(ComunEnum.PATRON_FECHA6.getDescripcion(), fecha);
		} catch (Exception e) {
			fecNac = null;
		}
		return fecNac != null;
	}
	
	public Date getFechaFromString(String fecha) {
		Date fecNac = null;
		try {
			fecNac = FechasUtil.formateadorStringAFecha(ComunEnum.PATRON_FECHA6.getDescripcion(), fecha);
		} catch (Exception e) {
			fecNac = null;
		}
		return fecNac;
	}
	
	public Integer getEdadDias() {
		String fechaNacimiento = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO);
		String fechaAtencion = getVariableFromMap(RdacaaVariableKeyCatalog.ATENCION_FECHA);
		Date fecNac = null;
		Date fecHasta = null;
		Integer edadDias = null;
		try {
			fecNac = FechasUtil.formateadorStringAFecha(ComunEnum.PATRON_FECHA6.getDescripcion(), fechaNacimiento);
			fecHasta = FechasUtil.formateadorStringAFecha(ComunEnum.PATRON_FECHA6.getDescripcion(), fechaAtencion);
			edadDias =  (int) ChronoUnit.DAYS
					.between(fecNac.toInstant().atZone(ZoneId.systemDefault()).toLocalDate(), fecHasta.toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
		} catch (Exception e) {
			// Excepcion gestionada
		}
		return edadDias;
	}

	private String concatenarEdad(Integer edadAnios, Integer edadMeses, Integer edadDias) {
		return StringUtils.leftPad(edadAnios.toString(), 3, "0") + StringUtils.leftPad(edadMeses.toString(), 2, "0")
				+ StringUtils.leftPad(edadDias.toString(), 2, "0");
	}
	
	protected boolean isEdadPacienteDentroRango(Integer edad, Integer edadMin, Integer edadMax) {
		return edad >= edadMin && edad <= edadMax;
	}

	protected boolean isSexoCorrespondiente(String sexoIn, String sexoRegla) {
		return sexoRegla.equals(sexoIn) || sexoRegla.equals(AMBOS_SEXOS);
	}

	protected boolean isValorFueraDeRango(BigDecimal valor, BigDecimal valorMin, BigDecimal valorMax) {
		return !(valor.compareTo(valorMin) >= 0 && valor.compareTo(valorMax) <= 0);
	}

	protected Integer getAlturaEstablecimientoSalud(String codEstSalud) {
		Integer cod;
		Integer altura;
		try {
			cod = Integer.parseInt(codEstSalud);
		} catch (Exception e) {
			return null;
		}
		
		try {
			altura = validationQueryService.findOneByEntidadId(cod).getZ17s();
			if(altura == null) {
				altura = 0;
			}
		} catch (Exception e) {
			return null;
		}

		return altura;
	}
	
	protected boolean isValidUUID(String uuidString) {
		return uuidString != null && uuidString.length() == 36 && uuidString.split("-").length == 5
				&& UUID.fromString(uuidString).toString().equals(uuidString);
	}
}
