/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ec.gob.msp.rdacaa.business.service.SignosVitalesReglasValidacion;
import ec.gob.msp.rdacaa.business.service.SignosvitalesreglasService;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalog;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.common.Validator;

/**
 *
 * @author Saulo Velasco
 */
@Component
public class TallaValidator extends ValidationSupport implements Validator<String[]> {

	@Autowired
	private SignosvitalesreglasService signosvitalesreglasService;
	private List<SignosVitalesReglasValidacion> reglasTalla;
	private static final BigDecimal TALLA_MIN_FUERA_RANGO = new BigDecimal("38.0");
	private static final BigDecimal TALLA_MAX_FUERA_RANGO = new BigDecimal("300.0");
	private static final String AMARILLO = "amarillo";
	private static final String AMBOS_SEXOS = "A";

	@PostConstruct
	private void init() {
		reglasTalla = signosvitalesreglasService.findAllReglasTalla();
	}

	@Override
	public List<ValidationResult> validate(String[] tallaEdadSexo) {

		List<ValidationResult> listaErrores = new ArrayList<>();

		if (isNullOrEmptyString(tallaEdadSexo[0])) {
			listaErrores.add(ValidationResultCatalog.TALLA_VACIA.getValidationError());
		} else {
			BigDecimal talla = new BigDecimal(tallaEdadSexo[0]);
			Integer edad = Integer.parseInt(tallaEdadSexo[1]);
			String sexo = tallaEdadSexo[2];
			Optional<ValidationResult> warning = procesarTallaParaEdad(talla, edad, sexo);
			if (warning.isPresent()) {
				listaErrores.add(warning.get());
			}
		}

		return listaErrores;
	}

	private Optional<ValidationResult> procesarTallaParaEdad(BigDecimal talla, Integer edad, String sexo) {
		for (SignosVitalesReglasValidacion regla : reglasTalla) {
			if (isEdadPacienteDentroRango(edad, regla.getEdadminima(), regla.getEdadmaxima())
					&& isSexoCorrespondiente(sexo, regla.getSexo())
					&& isTallaFueraDeRango(talla, regla.getValorminimo(), regla.getValormaximo())) {
				ValidationResult warning = ValidationResultCatalog.TALLA_FUERA_RANGO_PARA_EDAD.getValidationError();
				String message = warning.getMessage();
				message = message.replace("act", talla.toString());
				message = message.replace("tallamin", regla.getValorminimo().toString());
				message = message.replace("tallamax", regla.getValormaximo().toString());
				message = message.replace("edadmin", regla.getEdadminimaAnios() + " años " + regla.getEdadminimaMeses() + " meses " + regla.getEdadminimaDias() + " días" );
				message = message.replace("edadmax", regla.getEdadmaximaAnios() + " años " + regla.getEdadmaximaMeses() + " meses " + regla.getEdadmaximaDias() + " días");
				warning = new ValidationResult(warning.getCode(), message);
				warning.setColor(AMARILLO);
				return Optional.of(warning);
			}
		}
		if (isTallaFueraDeRango(talla, TALLA_MIN_FUERA_RANGO, TALLA_MAX_FUERA_RANGO)) {
			ValidationResult warning = ValidationResultCatalog.TALLA_FUERA_RANGO_PARA_EDAD.getValidationError();
			String message = warning.getMessage();
			message = message.replace("act", talla.toString());
			message = message.replace("tallamin", TALLA_MIN_FUERA_RANGO.toString());
			message = message.replace("tallamax", TALLA_MAX_FUERA_RANGO.toString());
			message = message.replace(" PARA LA EDAD DE edadmin / edadmax", "");
			warning = new ValidationResult(warning.getCode(), message);
			warning.setColor(AMARILLO);
			return Optional.of(warning);
		}
		return Optional.empty();
	}

	private boolean isEdadPacienteDentroRango(Integer edad, Integer edadMin, Integer edadMax) {
		return edad >= edadMin && edad <= edadMax;
	}

	private boolean isSexoCorrespondiente(String sexoIn, String sexoRegla) {
		return sexoRegla.equals(sexoIn) || sexoRegla.equals(AMBOS_SEXOS);
	}

	private boolean isTallaFueraDeRango(BigDecimal talla, BigDecimal tallaMin, BigDecimal tallaMax) {
		return !(talla.compareTo(tallaMin) >= 0 && talla.compareTo(tallaMax) <= 0);
	}
}
