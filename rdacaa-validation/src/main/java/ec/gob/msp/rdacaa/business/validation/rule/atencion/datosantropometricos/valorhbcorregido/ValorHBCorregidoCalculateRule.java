package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.valorhbcorregido;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;
import ec.gob.msp.rdacaa.utilitario.medico.UtilitarioMedico;

@Rule(order = 6)
public class ValorHBCorregidoCalculateRule extends UtilsRule {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {
		String valorHBCorregido = "999990000066666";
		String valorHB = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB);
		String codEstSalud = getVariableFromMap(RdacaaVariableKeyCatalog.ESTABLECIMENTO_SALUD_CODIGO);
		Integer alturaEstSalud = getAlturaEstablecimientoSalud(codEstSalud);
		boolean valorHBHasErrors = variableHasErrors(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB);
		
		boolean valuesNotDefined = ValidationSupport.isNotDefined(valorHB);

		setRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB_CORREGIDO, valorHBCorregido);
		
		if (valorHBHasErrors) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB_CORREGIDO,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_VALOR_HB_CORREGIDO_VALORHB_ERROR);
		}

		if (alturaEstSalud == null) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB_CORREGIDO,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_VALOR_HB_CORREGIDO_EST_SALUD_ERROR);
		}
		
		if(valuesNotDefined) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB_CORREGIDO,
					ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_VALOR_HB_CORREGIDO_VALORHB_NO_DEFINIDO);
		}

		if (valorHBHasErrors || alturaEstSalud == null || valuesNotDefined) {
			return RuleState.BREAK;
		}

		Double valorHBRiesgoCorregido = calcularValorHBRiesgoCorregido(valorHB, alturaEstSalud);
		valorHBCorregido = valorHBRiesgoCorregido.toString();

		setRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB_CORREGIDO, valorHBCorregido);

		addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB_CORREGIDO,
				ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_VALOR_HB_CORREGIDO_VALOR);
		return RuleState.NEXT;

	}

	private Double calcularValorHBRiesgoCorregido(String valorHB, Integer alturaEstSalud) {
		Double[] valorRiesgoCorregidoFactor = UtilitarioMedico
				.calcularValorHBRiesgoCorregido(Double.parseDouble(valorHB), alturaEstSalud);
		return valorRiesgoCorregidoFactor[0];
	}
}
