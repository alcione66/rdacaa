package ec.gob.msp.rdacaa.business.validation.rule.persona.tiposeguro;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 4)
public class TipoSeguroIsMandatoryRule extends BaseRule<String> {
	
	@When
	public boolean when() {
		return true;

	}

	@Then
	public RuleState then() {

		String tipoIdentificacion = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEGURO);
		if (isMandatory(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEGURO)
				&& ValidationSupport.isNotDefined(tipoIdentificacion)) {
			addValidationResult(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEGURO,
					ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_SEGURO_ES_MANDATORIO);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEGURO)
				&& ValidationSupport.isNotDefined(tipoIdentificacion)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}
}
