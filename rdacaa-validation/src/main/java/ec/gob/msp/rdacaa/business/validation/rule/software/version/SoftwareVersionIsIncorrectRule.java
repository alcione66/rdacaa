package ec.gob.msp.rdacaa.business.validation.rule.software.version;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class SoftwareVersionIsIncorrectRule extends UtilsRule {
	
	private static final String VERSION = "2.0.0";

	@When
	public boolean when() {
		String softwareVersion = getVariableFromMap(RdacaaVariableKeyCatalog.SOFTWARE_VERSION);
		return !VERSION.equals(softwareVersion);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.SOFTWARE_VERSION,
				ValidationResultCatalogConstants.CODIGO_ERROR_SOFTWARE_VERSION_ES_INCORRECTO);
		return RuleState.BREAK;
	}

}
