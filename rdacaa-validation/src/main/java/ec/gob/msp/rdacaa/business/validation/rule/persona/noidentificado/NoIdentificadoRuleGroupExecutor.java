package ec.gob.msp.rdacaa.business.validation.rule.persona.noidentificado;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.deliveredtechnologies.rulebook.Fact;
import com.deliveredtechnologies.rulebook.model.RuleBook;
import com.deliveredtechnologies.rulebook.util.ArrayUtils;

import ec.gob.msp.rdacaa.business.service.ParroquiaService;
import ec.gob.msp.rdacaa.business.service.ValidationQueryService;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogMap;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.RdacaaRuleExecutor;

/**
 *
 * @author dmurillo
 */
@Component
public class NoIdentificadoRuleGroupExecutor extends RdacaaRuleExecutor {

	private ParroquiaService parroquiaService;

	@Autowired
	public NoIdentificadoRuleGroupExecutor(@Qualifier("ReglasNoIdentificado") RuleBook<RdacaaRawRowResult> ruleBook,
			ParroquiaService parroquiaService) {
		super();
		this.ruleBook = ruleBook;
		this.variableKey = RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION;
		this.parroquiaService = parroquiaService;
	}

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Optional<RdacaaRawRowResult> execute(RdacaaRawRow input, Fact... extraFacts) {
		Fact parroquiaServiceFact = new Fact("parroquiaService", parroquiaService);
		Fact[] extra = ArrayUtils.combine(extraFacts, new Fact[] {parroquiaServiceFact});
		return super.execute(input, extra);
	}

}
