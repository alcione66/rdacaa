package ec.gob.msp.rdacaa.business.validation.rule.persona.orientacionsexual;

import java.util.Date;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;
import ec.gob.msp.rdacaa.utilitario.enumeracion.ComunEnum;
import ec.gob.msp.rdacaa.utilitario.fecha.FechasUtil;


@Rule(order = 2)
public class OrientacionSexualIsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String orientacionSexual = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_ORIENTACION_SEXUAL);
		String fechaNacimiento = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO);
		Date fecNac = null;
		if(fechaNacimiento != null){
			fecNac = FechasUtil.formateadorStringAFecha(ComunEnum.PATRON_FECHA6.getDescripcion(), fechaNacimiento);
		}else{
			return false;
		}
		String fechaAtencion = getVariableFromMap(RdacaaVariableKeyCatalog.ATENCION_FECHA);
		Date fecHasta = null;
		if(fechaAtencion != null){
			fecHasta = FechasUtil.formateadorStringAFecha(ComunEnum.PATRON_FECHA6.getDescripcion(), fechaAtencion);
		}else{
			return false;
		}
		Integer anios = FechasUtil.getDiffDatesDesdeHasta(fecNac, fecHasta,0);
		Integer meses = FechasUtil.getDiffDatesDesdeHasta(fecNac, fecHasta,1);
		Integer dias = FechasUtil.getDiffDatesDesdeHasta(fecNac, fecHasta,2);
		if(!validationQueryService.isEvaluateOrientacionSexual(anios, meses, dias)){
			return false;
		}
		return ValidationSupport.isNullOrEmptyString(orientacionSexual);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PERSONA_CODIGO_ORIENTACION_SEXUAL,
				ValidationResultCatalogConstants.CODIGO_ERROR_ORIENTACIONSEXUAL_NULO);
		return RuleState.BREAK;
	}
}
