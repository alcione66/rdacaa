package ec.gob.msp.rdacaa.business.validation.rule.atencion.violencia.parentescoagresor;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 4)
public class ParentescoAgresorIsMandatoryRule extends BaseRule<String> {

    @When
	public boolean when() {
		return true;

	}

	@Then
	public RuleState then() {

		String codigoParentesco = getVariableFromMap(RdacaaVariableKeyCatalog.VIOLENCIA_PARENTESCO_AGRESOR);
		if (isMandatory(RdacaaVariableKeyCatalog.VIOLENCIA_PARENTESCO_AGRESOR)
				&& ValidationSupport.isNotDefined(codigoParentesco)) {
			addValidationResult(RdacaaVariableKeyCatalog.VIOLENCIA_PARENTESCO_AGRESOR,
					ValidationResultCatalogConstants.CODIGO_ERROR_VIOLENCIA_LESIONES_OCURRIDAS_ES_MANDATORIA);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.VIOLENCIA_PARENTESCO_AGRESOR)
				&& ValidationSupport.isNotDefined(codigoParentesco)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}
}
