package ec.gob.msp.rdacaa.business.validation.rule.atencion.profesional;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 5)
public class ProfesionalIsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		String codProfesional = getVariableFromMap(RdacaaVariableKeyCatalog.PROFESIONAL_CODIGO);
		return (!isMandatory(RdacaaVariableKeyCatalog.PROFESIONAL_CODIGO)
				&& ValidationSupport.isNotDefined(codProfesional));
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}
