package ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.condiciondiagnostico4;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 2)
public class CondicionDiagnostico4IsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String condiciondiagnostico4 = getVariableFromMap(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CONDICION_DIAG_4);
		return ValidationSupport.isNullOrEmptyString(condiciondiagnostico4);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CONDICION_DIAG_4,
				ValidationResultCatalogConstants.CODIGO_ERROR_CONDICION_DIAGNOSTICO_4_NULO);
		return RuleState.BREAK;
	}

}
