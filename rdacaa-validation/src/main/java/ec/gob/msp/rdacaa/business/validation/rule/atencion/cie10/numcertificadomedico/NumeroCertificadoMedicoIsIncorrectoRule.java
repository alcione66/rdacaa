/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.numcertificadomedico;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.service.ConstantesDetalleCatalogo;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 8)
public class NumeroCertificadoMedicoIsIncorrectoRule extends BaseRule<String> {
	@When
	public boolean when() {
		String codigodiagnostico1 = getVariableFromMap(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_1);
		String codigodiagnostico2 = getVariableFromMap(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_2);
		String codigodiagnostico3 = getVariableFromMap(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_3);
		String codigodiagnostico4 = getVariableFromMap(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_4);
		String codigodiagnostico5 = getVariableFromMap(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_5);
		if(
			(codigodiagnostico1 != null && codigodiagnostico1.equals(ConstantesDetalleCatalogo.CIE_CERTIFICADO_MEDICO_UNICO)) ||
			(codigodiagnostico2 != null && codigodiagnostico2.equals(ConstantesDetalleCatalogo.CIE_CERTIFICADO_MEDICO_UNICO)) ||
			(codigodiagnostico3 != null && codigodiagnostico3.equals(ConstantesDetalleCatalogo.CIE_CERTIFICADO_MEDICO_UNICO)) ||
			(codigodiagnostico4 != null && codigodiagnostico4.equals(ConstantesDetalleCatalogo.CIE_CERTIFICADO_MEDICO_UNICO)) ||
			(codigodiagnostico5 != null && codigodiagnostico5.equals(ConstantesDetalleCatalogo.CIE_CERTIFICADO_MEDICO_UNICO))
		) {
			return false;
		}

		return true;
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.DIAGNOSTICO_NUMERO_CERTIFICADO_MEDICO_UNICO,
				ValidationResultCatalogConstants.CODIGO_ERROR_NUMERO_CERTIFICADO_MEDICO_INCORRECTO);
		return RuleState.BREAK;
	}
}
