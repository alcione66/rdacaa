package ec.gob.msp.rdacaa.business.validation.rule.persona.segundoapellido;

import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 5)
public class SegundoApellidoLongitudRule extends BaseRule<String> {

	private static final int TRES_CARACTERES = 3;

	@When
	public boolean when() {
		String segundoApellido = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_APELLIDO);
		return !ValidationSupport.isNullOrEmptyString(segundoApellido)
				&& ValidationSupport.isStringLessThanXCharacters(segundoApellido, TRES_CARACTERES);
	}

	@Then
	public void then() {
		addValidationResult(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_APELLIDO,
				ValidationResultCatalogConstants.CODIGO_ERROR_SEGUNDO_APELLIDO_LONG);
	}
}
