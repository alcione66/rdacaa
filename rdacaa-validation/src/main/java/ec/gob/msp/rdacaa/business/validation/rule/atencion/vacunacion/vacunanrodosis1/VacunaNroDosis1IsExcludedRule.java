package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunanrodosis1;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */

@Rule(order = 3)
public class VacunaNroDosis1IsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_DOSIS_1);
	}

	@Then
	public RuleState then() {

		String vacunaNroDosis1 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_DOSIS_1);
		if (!ValidationSupport.isNotDefined(vacunaNroDosis1)) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_DOSIS_1,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_DOSIS_1_NO_DEFINIDA);
		}

		return RuleState.BREAK;

	}
}
