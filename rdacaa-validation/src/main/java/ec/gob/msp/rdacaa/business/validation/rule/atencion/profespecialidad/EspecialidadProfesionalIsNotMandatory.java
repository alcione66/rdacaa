package ec.gob.msp.rdacaa.business.validation.rule.atencion.profespecialidad;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 5)
public class EspecialidadProfesionalIsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		String especialidadProfesional = getVariableFromMap(RdacaaVariableKeyCatalog.ATENCION_CODIGO_ESPECIALIDAD);
		return (!isMandatory(RdacaaVariableKeyCatalog.ATENCION_CODIGO_ESPECIALIDAD)
				&& ValidationSupport.isNotDefined(especialidadProfesional));
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}
