package ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.prodto3;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 2)
public class Procedimiento3IsNullRule extends BaseRule<String> {

	@When
    public boolean when() {
        String procedimiento3 = getVariableFromMap(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_3);
        return ValidationSupport.isNullOrEmptyString(procedimiento3);
    }
	
	@Then
    public RuleState then() {
        addValidationResult(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_3,
        		ValidationResultCatalogConstants.CODIGO_ERROR_PROCEDIMIENTO_3_NULO);
        return RuleState.BREAK;
    }
}
