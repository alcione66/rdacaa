package ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.prodto4;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 6)
public class Procedimiento4IsNumericaRule extends BaseRule<String> {

	@When
    public boolean when() {
        String procedimiento4 = getVariableFromMap(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_4);
        return !StringUtils.isNumeric(procedimiento4);
    }

    @Then
    public RuleState then() {
        addValidationResult(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_4,
        		ValidationResultCatalogConstants.CODIGO_ERROR_PROCEDIMIENTO_4_NO_NUMERICO);
        return RuleState.BREAK;
    }
}
