package ec.gob.msp.rdacaa.business.validation.rule.atencion.uuid;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 2)
public class AtencionUUIDIsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String atencionUUID = getVariableFromMap(RdacaaVariableKeyCatalog.ATENCION_CODIGO_UUID);
		return ValidationSupport.isNullOrEmptyString(atencionUUID);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.ATENCION_CODIGO_UUID,
				ValidationResultCatalogConstants.CODIGO_ERROR_ATENCION_CODIGO_UUID_NULO);
		return RuleState.BREAK;
	}

}
