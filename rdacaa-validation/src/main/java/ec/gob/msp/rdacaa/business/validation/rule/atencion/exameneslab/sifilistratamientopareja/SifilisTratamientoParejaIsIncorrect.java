package ec.gob.msp.rdacaa.business.validation.rule.atencion.exameneslab.sifilistratamientopareja;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class SifilisTratamientoParejaIsIncorrect extends UtilsRule {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {
		String sifilisTratamientoPareja = getVariableFromMap(RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_SIFILIS_TRATAMIENTO_PAREJA);

		boolean isEmbarazada = isEmbarazada();
		boolean isMujer = "F".equals(getSexoString());

		boolean isValidValue = "1".equals(sifilisTratamientoPareja.trim()) || "2".equals(sifilisTratamientoPareja.trim())
				|| "3".equals(sifilisTratamientoPareja.trim());
		
		if (!isValidValue) {
			addValidationResult(RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_SIFILIS_TRATAMIENTO_PAREJA,
					ValidationResultCatalogConstants.CODIGO_ERROR_EXAMENES_LABORATORIO_SIFILIS_TRATAMIENTO_PAREJA_ES_INCORRECTO);
		}

		boolean requiereMujerEmbarazadaParaValorValido = !(isMujer && isEmbarazada) && isValidValue;
		if (requiereMujerEmbarazadaParaValorValido) {
			addValidationResult(RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_SIFILIS_TRATAMIENTO_PAREJA,
					ValidationResultCatalogConstants.CODIGO_ERROR_EXAMENES_LABORATORIO_SIFILIS_TRATAMIENTO_PAREJA_REQUIERE_MUJER_EMBARAZADA);
		}
		
		if (!isValidValue || requiereMujerEmbarazadaParaValorValido ) {
			return RuleState.BREAK;
		}
		
		return RuleState.NEXT;
	}
}
