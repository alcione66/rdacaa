package ec.gob.msp.rdacaa.business.validation.rule.atencion.grupovulnerable.gpr_vulnerable5;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 4)
public class GrupoVulnerable5IsMandatoryRule extends BaseRule<String>{

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {

		String grupoVulnerable5 = getVariableFromMap(RdacaaVariableKeyCatalog.GRUPO_VULNERABLE_5);
		if (isMandatory(RdacaaVariableKeyCatalog.GRUPO_VULNERABLE_5)
				&& ValidationSupport.isNotDefined(grupoVulnerable5)) {
			addValidationResult(RdacaaVariableKeyCatalog.GRUPO_VULNERABLE_5,
					ValidationResultCatalogConstants.CODIGO_ERROR_GRUPO_VULNERABLE_5_ES_MANDATORIA);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.GRUPO_VULNERABLE_5)
				&& ValidationSupport.isNotDefined(grupoVulnerable5)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}
}
