package ec.gob.msp.rdacaa.business.validation.rule.persona.parroquia;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 4)
public class ParroquiaIsMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		return true;

	}

	@Then
	public RuleState then() {

		String parroquia = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_PARROQUIA);
		if (isMandatory(RdacaaVariableKeyCatalog.PERSONA_CODIGO_PARROQUIA)
				&& ValidationSupport.isNotDefined(parroquia)) {
			addValidationResult(RdacaaVariableKeyCatalog.PERSONA_CODIGO_PARROQUIA,
					ValidationResultCatalogConstants.CODIGO_ERROR_CODIGO_PARROQUIA_ES_MANDATORIA);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.PERSONA_CODIGO_PARROQUIA)
				&& ValidationSupport.isNotDefined(parroquia)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}
}
