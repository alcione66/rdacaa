package ec.gob.msp.rdacaa.business.validation.rule.atencion.grupoprioritario.gpr_prioritario4;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 5)
public class GrupoPrioritario4IsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		String grupoprioritario4 = getVariableFromMap(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_4);
		return (!isMandatory(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_4)
				&& ValidationSupport.isNotDefined(grupoprioritario4));
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}
