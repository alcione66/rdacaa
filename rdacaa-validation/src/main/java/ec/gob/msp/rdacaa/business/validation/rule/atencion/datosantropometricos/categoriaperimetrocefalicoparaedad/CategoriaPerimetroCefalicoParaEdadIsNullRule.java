package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.categoriaperimetrocefalicoparaedad;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 2)
public class CategoriaPerimetroCefalicoParaEdadIsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String categoriaPerimetroCefalicoParaEdad = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PERIMETRO_CEFALICO_PARA_EDAD);
		return ValidationSupport.isNullOrEmptyString(categoriaPerimetroCefalicoParaEdad);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PERIMETRO_CEFALICO_PARA_EDAD,
				ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_CATEGORIA_PERIM_CEFALICO_EDAD_NULO);
		return RuleState.BREAK;
	}
}
