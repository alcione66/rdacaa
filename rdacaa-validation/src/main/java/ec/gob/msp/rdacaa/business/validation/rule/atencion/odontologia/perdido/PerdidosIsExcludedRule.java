package ec.gob.msp.rdacaa.business.validation.rule.atencion.odontologia.perdido;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 3)
public class PerdidosIsExcludedRule extends BaseRule<String>{
	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_PERDIDAS);
	}

	@Then
	public RuleState then() {

		String dientesPerdidos = getVariableFromMap(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_PERDIDAS);
		if (!ValidationSupport.isNotDefined(dientesPerdidos)) {
			addValidationResult(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_PERDIDAS,
					ValidationResultCatalogConstants.CODIGO_ERROR_DIENTES_PERDIDOS_NO_DEFINIDO);
		}

		return RuleState.BREAK;

	}
}
