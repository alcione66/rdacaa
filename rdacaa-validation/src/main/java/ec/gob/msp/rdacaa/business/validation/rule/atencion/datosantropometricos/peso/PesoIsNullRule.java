package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.peso;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 2)
public class PesoIsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String peso = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO);
		return ValidationSupport.isNullOrEmptyString(peso);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO,
				ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PESO_NULO);
		return RuleState.BREAK;
	}
}
