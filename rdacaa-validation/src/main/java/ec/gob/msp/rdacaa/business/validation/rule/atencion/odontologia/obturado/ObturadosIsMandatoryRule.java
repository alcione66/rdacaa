package ec.gob.msp.rdacaa.business.validation.rule.atencion.odontologia.obturado;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 4)
public class ObturadosIsMandatoryRule extends BaseRule<String>{
	@When
	public boolean when() {
		return true;
	}
	
	@Then
	public RuleState then() {

		String dientesObturados = getVariableFromMap(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_OBTURADAS);
		if (isMandatory(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_OBTURADAS) 
				&& ValidationSupport.isNotDefined(dientesObturados)) {
			addValidationResult(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_OBTURADAS,
					ValidationResultCatalogConstants.CODIGO_ERROR_DIENTES_OBTURADOS_ES_MANDATORIO);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_OBTURADAS)
				&& ValidationSupport.isNotDefined(dientesObturados)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}
}
