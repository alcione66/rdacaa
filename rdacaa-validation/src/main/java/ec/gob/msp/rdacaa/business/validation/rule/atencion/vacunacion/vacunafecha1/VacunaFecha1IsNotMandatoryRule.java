package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunafecha1;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 5)
public class VacunaFecha1IsNotMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		String vacunaFecha1 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_FECHA_1);
		return (!isMandatory(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_FECHA_1)
				&& ValidationSupport.isNotDefined(vacunaFecha1));
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}
