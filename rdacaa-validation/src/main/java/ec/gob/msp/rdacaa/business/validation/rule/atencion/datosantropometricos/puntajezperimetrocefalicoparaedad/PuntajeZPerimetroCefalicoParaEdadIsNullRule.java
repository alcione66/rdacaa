package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.puntajezperimetrocefalicoparaedad;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 2)
public class PuntajeZPerimetroCefalicoParaEdadIsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String puntajeZPerimetroCefalicoParaEdad = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PERIMETRO_CEFALICO_PARA_EDAD);
		return ValidationSupport.isNullOrEmptyString(puntajeZPerimetroCefalicoParaEdad);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PERIMETRO_CEFALICO_PARA_EDAD,
				ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PUNTAJE_Z_PERIM_CEFALICO_EDAD_NULO);
		return RuleState.BREAK;
	}
}
