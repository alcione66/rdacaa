package ec.gob.msp.rdacaa.business.validation.rule.atencion.grupoprioritario.gpr_prioritario1;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 5)
public class GrupoPrioritario1IsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		String grupoprioritario1 = getVariableFromMap(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1);
		return (!isMandatory(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1)
				&& ValidationSupport.isNotDefined(grupoprioritario1));
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}
