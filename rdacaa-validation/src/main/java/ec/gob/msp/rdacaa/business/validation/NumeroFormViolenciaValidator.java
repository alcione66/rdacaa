/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalog;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import static ec.gob.msp.rdacaa.business.validation.common.ValidationSupport.isNullOrEmptyString;
import static ec.gob.msp.rdacaa.business.validation.common.ValidationSupport.isStringLessThanXCharacters;
import ec.gob.msp.rdacaa.business.validation.common.Validator;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author eduardo
 */
public class NumeroFormViolenciaValidator extends ValidationSupport implements Validator<String>{
    
    private static final int CARACTERES = 1;

	public List<ValidationResult> validate(String nombre) {
		
		List<ValidationResult> listaErrores = new ArrayList<>();
		
		if(isNullOrEmptyString(nombre)) {
			listaErrores.add(ValidationResultCatalog.NOMBRE_VACIO_O_NULL.getValidationError());
		} else if(isStringLessThanXCharacters(nombre,CARACTERES)) {
			listaErrores.add(ValidationResultCatalog.NOMBRE_MENOR_TRES_CARACTERES.getValidationError());
		}
		
		return listaErrores;
	}
}
