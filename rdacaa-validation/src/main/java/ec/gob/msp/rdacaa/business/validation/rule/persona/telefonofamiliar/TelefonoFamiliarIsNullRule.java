/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.persona.telefonofamiliar;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author eduardo
 */
@Rule(order = 2)
public class TelefonoFamiliarIsNullRule extends BaseRule<String> {
    @When
	public boolean when() {
		String archivo = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_TELEFONO_FAMILIAR);
		return ValidationSupport.isNullOrEmptyString(archivo);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PERSONA_TELEFONO_FAMILIAR,
				ValidationResultCatalogConstants.CODIGO_ERROR_TELEFONO_FAMILIAR_NULL);
		return RuleState.BREAK;
	}
}
