package ec.gob.msp.rdacaa.business.validation.rule.atencion.obstetricia.embplanificado;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 4)
public class EmbarazoPlanificadoIsMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		return true;

	}

	@Then
	public RuleState then() {

		String embarazoPlanificado = getVariableFromMap(RdacaaVariableKeyCatalog.OBSTETRICIA_ES_EMBARAZO_PLANIFICADO);
		if (isMandatory(RdacaaVariableKeyCatalog.OBSTETRICIA_ES_EMBARAZO_PLANIFICADO)
				&& ValidationSupport.isNotDefined(embarazoPlanificado)) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_ES_EMBARAZO_PLANIFICADO,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_ES_EMBARAZO_PLANIFICADO_ES_MANDATORIA);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.OBSTETRICIA_ES_EMBARAZO_PLANIFICADO)
				&& ValidationSupport.isNotDefined(embarazoPlanificado)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}

}
