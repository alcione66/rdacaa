package ec.gob.msp.rdacaa.business.validation.rule.persona.nacionalidadetnica;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 6)
public class NacionalidadEtnicaIsIncorrectoRule extends BaseRule<String> {

	private static final int INDIGENA = 18;
	
	@When
	public boolean when() {
		String identificacionEtnica = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_AUTOIDENTIFICACION_ETNICA);
		String nacionalidadEtnica = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD_ETNICA);
		boolean valor = true;
		if(identificacionEtnica != null && Integer.valueOf(identificacionEtnica) == INDIGENA && nacionalidadEtnica != null){
			valor = !this.validationQueryService.isNacionalidadEtnicaValido(Integer.valueOf(nacionalidadEtnica));
		}
		return valor;
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD_ETNICA,
				ValidationResultCatalogConstants.CODIGO_ERROR_NACIONALIDAD_ETNICA_INCORRECTO);
		return RuleState.BREAK;
	}
}
