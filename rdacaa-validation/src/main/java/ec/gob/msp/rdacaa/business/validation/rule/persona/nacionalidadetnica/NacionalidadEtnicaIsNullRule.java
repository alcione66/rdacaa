package ec.gob.msp.rdacaa.business.validation.rule.persona.nacionalidadetnica;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 2)
public class NacionalidadEtnicaIsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String nacionalidad = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD_ETNICA);
		return ValidationSupport.isNullOrEmptyString(nacionalidad);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD_ETNICA,
				ValidationResultCatalogConstants.CODIGO_ERROR_NACIONALIDAD_ETNICA_NULO);
		return RuleState.BREAK;
	}
}
