package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.puntajezimcparaedad;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 4)
public class PuntajeZIMCParaEdadIsMandatoryRule extends BaseRule<String> {
	@When
	public boolean when() {
		return true;

	}

	@Then
	public RuleState then() {

		String puntajeZIMCParaEdad = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_IMC_PARA_EDAD);
		if (isMandatory(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_IMC_PARA_EDAD)
				&& ValidationSupport.isNotDefined(puntajeZIMCParaEdad)) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_IMC_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PUNTAJE_Z_IMC_EDAD_ES_MANDATORIA);
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}
	}
}
