package ec.gob.msp.rdacaa.business.validation.rule.atencion.obstetricia.riesgoobstetrico;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 2)
public class RiesgoObtetricoIsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String riesgoObtetrico = getVariableFromMap(RdacaaVariableKeyCatalog.OBSTETRICIA_CODIGO_RIESGO_OBSTETRICO);
		return ValidationSupport.isNullOrEmptyString(riesgoObtetrico);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_CODIGO_RIESGO_OBSTETRICO,
				ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_CODIGO_RIESGO_OBSTETRICO_NULO);
		return RuleState.BREAK;
	}

}
