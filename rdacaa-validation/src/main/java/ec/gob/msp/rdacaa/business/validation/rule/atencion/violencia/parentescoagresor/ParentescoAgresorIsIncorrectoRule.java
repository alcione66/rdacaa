package ec.gob.msp.rdacaa.business.validation.rule.atencion.violencia.parentescoagresor;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 7)
public class ParentescoAgresorIsIncorrectoRule extends BaseRule<String>  {

	private static final int IDENTIFICA_AGRESOR_SI = 2623;
	
	@When
	public boolean when() {
		String parentescoAgresor = getVariableFromMap(RdacaaVariableKeyCatalog.VIOLENCIA_PARENTESCO_AGRESOR);
		String identificaAgresor = getVariableFromMap(RdacaaVariableKeyCatalog.VIOLENCIA_IDENTIFICA_AGRESOR);

		boolean valor = true;
		if(identificaAgresor != null && Integer.valueOf(identificaAgresor) == IDENTIFICA_AGRESOR_SI){
			valor = !this.validationQueryService.isCodigoParentescoAgresorValido(Integer.valueOf(parentescoAgresor));
		}
		return valor;
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.VIOLENCIA_PARENTESCO_AGRESOR,
				ValidationResultCatalogConstants.CODIGO_ERROR_VIOLENCIA_PARENTESCO_AGRESOR_INCORRECTO);
		return RuleState.BREAK;
	}
}
