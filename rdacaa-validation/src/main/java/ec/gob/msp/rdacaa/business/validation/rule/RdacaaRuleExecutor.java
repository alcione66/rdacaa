package ec.gob.msp.rdacaa.business.validation.rule;

import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;

import com.deliveredtechnologies.rulebook.Fact;
import com.deliveredtechnologies.rulebook.FactMap;
import com.deliveredtechnologies.rulebook.NameValueReferableMap;
import com.deliveredtechnologies.rulebook.Result;
import com.deliveredtechnologies.rulebook.model.RuleBook;

import ec.gob.msp.rdacaa.business.service.ValidationQueryService;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogMap;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableEntityCatalog;

public abstract class RdacaaRuleExecutor implements RuleExecutor {
	
	private final Logger logger = LoggerFactory.getLogger(RdacaaRuleExecutor.class);

	public Integer getOrder() {
		logger.debug("variableKey {}", variableKey);
		return rdacaaVariableEntityCatalog.get(variableKey).getOrden();
	}

	protected String variableKey;
	
	protected RuleBook<RdacaaRawRowResult> ruleBook;
	
	@Autowired
	protected RdacaaVariableEntityCatalog rdacaaVariableEntityCatalog;

	@Autowired
	protected ValidationResultCatalogMap validationResultCatalogMap;

	@Autowired
	protected ValidationQueryService validationQueryService;

	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Optional<RdacaaRawRowResult> execute(RdacaaRawRow input, Fact... extraFacts) {
		NameValueReferableMap<Object> facts = new FactMap<>();
		facts.setValue("rdacaa_row", input);
		facts.setValue("validacion_variable_catalog", rdacaaVariableEntityCatalog);		
		facts.setValue("validacion_result_catalog", validationResultCatalogMap);
		facts.setValue("validacion_query_service", validationQueryService);
		for (Fact fact : extraFacts) {
			facts.put(fact);
		}
		ruleBook.run(facts);
		Optional<Result<RdacaaRawRowResult>> result = ruleBook.getResult();

		return result.isPresent() ? Optional.of(result.get().getValue()) : Optional.empty();
	}

}
