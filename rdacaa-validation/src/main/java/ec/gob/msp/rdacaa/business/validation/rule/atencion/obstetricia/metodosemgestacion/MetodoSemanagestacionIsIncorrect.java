package ec.gob.msp.rdacaa.business.validation.rule.atencion.obstetricia.metodosemgestacion;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class MetodoSemanagestacionIsIncorrect extends UtilsRule {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {
		String metodoSemanagestacion = getVariableFromMap(RdacaaVariableKeyCatalog.OBSTETRICIA_CODIGO_METODO_DET_SEMANAS_GESTACION);
		
		Integer edadAniosMesesDias = getEdadAnioMesDias();
		boolean isEmbarazada = isEmbarazada();
		boolean isMujer = "F".equals(getSexoString());
		
		Integer metodoSemanagestacionNumber = null;
		
		try {
			metodoSemanagestacionNumber = Integer.parseInt(metodoSemanagestacion);
		} catch (Exception e) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_CODIGO_METODO_DET_SEMANAS_GESTACION,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_CODIGO_METODO_DET_SEMANAS_GESTACION_NO_NUMERICO);
			return RuleState.BREAK;
		}

		boolean isValidValue = validationQueryService.isIdMetodosSemanasGestacionValido(metodoSemanagestacionNumber);
		if (!isValidValue) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_CODIGO_METODO_DET_SEMANAS_GESTACION,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_CODIGO_METODO_DET_SEMANAS_GESTACION_ES_INCORRECTO);
		}
		
		boolean requiereMujerEmbarazadaParaValorValido = !(isMujer && isEmbarazada) && isValidValue;
		if (requiereMujerEmbarazadaParaValorValido) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_CODIGO_METODO_DET_SEMANAS_GESTACION,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_CODIGO_METODO_DET_SEMANAS_GESTACION_REQUIERE_MUJER_EMBARAZADA);
		}

		if (edadAniosMesesDias == null) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_CODIGO_METODO_DET_SEMANAS_GESTACION,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_CODIGO_METODO_DET_SEMANAS_GESTACION_FECHAS_INVALIDAS);
			return RuleState.BREAK;
		}

		boolean requiereMayor8AniosMenor59Anios = !(edadAniosMesesDias >= 80000 && edadAniosMesesDias <= 590000)
				&& !ValidationSupport.isNotDefined(metodoSemanagestacion);
		if (requiereMayor8AniosMenor59Anios) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_CODIGO_METODO_DET_SEMANAS_GESTACION,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_CODIGO_METODO_DET_SEMANAS_GESTACION_DEBE_TENER_DEFAULT_VALUE);
		}
		
		if(!isValidValue || requiereMujerEmbarazadaParaValorValido || requiereMayor8AniosMenor59Anios) {
			return RuleState.BREAK;
		}

		return RuleState.NEXT;
	}
}
