package ec.gob.msp.rdacaa.business.validation.rule.atencion.fechaatencion;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 2)
public class FechaAtencionIsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String fechaAtencion = getVariableFromMap(RdacaaVariableKeyCatalog.ATENCION_FECHA);
		return ValidationSupport.isNullOrEmptyString(fechaAtencion);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.ATENCION_FECHA,
				ValidationResultCatalogConstants.CODIGO_ERROR_ATENCION_FECHA_NULO);
		return RuleState.BREAK;
	}

}
