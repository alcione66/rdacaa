package ec.gob.msp.rdacaa.business.validation.database.common;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ec.gob.msp.rdacaa.business.entity.Variable;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableEntityCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;

@Component
public class MapperFunction implements Function<Object, Object> {
	
	private static final Logger logger = LoggerFactory.getLogger(MapperFunction.class);

	@Autowired
	private RdacaaVariableEntityCatalog rdacaaVariableEntityCatalog;

	@Override
	public Object apply(Object resultSet) {
		List<RdacaaRawRow> listRows = new ArrayList<>();

		if (resultSet instanceof ResultSet) {

			ResultSet personaAtencionResultSet = (ResultSet) resultSet;

			try {
				mapToRdacaaRawRow(listRows, personaAtencionResultSet);
			} catch (SQLException e) {
				throw new CustomSQLException("Error obteniendo datos del ResultSet", e);
			}

		}

		return listRows;
	}

	private void mapToRdacaaRawRow(List<RdacaaRawRow> listRows, ResultSet resultSet)
			throws SQLException {

		ResultSetMetaData metadata = resultSet.getMetaData();
		int columnCount = metadata.getColumnCount();
		while (resultSet.next()) {
			
			List<String> notFoundColumns = new ArrayList<>();
			Set<String> setVariablesStrings = rdacaaVariableEntityCatalog.getSetVariablesStrings();
			
			RdacaaRawRow row = new RdacaaRawRow();
		
			for (int i = 1; i <= columnCount; i++) {
				String columnName = metadata.getColumnName(i);
				if(!columnName.contains("_DESC") && !columnName.equals("KS_CIDENT") && !columnName.equals("KU_CIDENT") 
						&& !columnName.equals("KU_KEYID")) {
					
					setVariablesStrings.remove(columnName);
					Variable variable = rdacaaVariableEntityCatalog.get(columnName);

					logger.debug("columnName {}", columnName);
							
					RowItem item = new RowItem();
					item.setItemValue(resultSet.getString(i));
					item.setExcludedFromValidation(true);

					if (variable != null || columnName.equals(RdacaaVariableKeyCatalog.PERSONA_CODIGO_UUID)) {
						row.addField(columnName, item);
					} else {
						notFoundColumns.add(columnName);
					}
					
					logger.debug("valor {}", item.getItemValue());
				}
				
			}
			
			if(!setVariablesStrings.isEmpty()) {
				StringBuilder columnames = new StringBuilder();
				setVariablesStrings.forEach(columnName -> columnames.append(columnName+"\n"));
				logger.error("{} {}", columnames , " MissingVariableException" );
				throw new MissingVariableException("Variable(s) "+ columnames.toString() + " faltante(s) en el archivo" );
			}
			
			if(notFoundColumns.isEmpty()) {
				listRows.add(row);
			} else{
				StringBuilder columnames = new StringBuilder();
				notFoundColumns.forEach(columnName -> columnames.append(columnName+"\n"));
				logger.error("{} {}", columnames , " VariableNotFoundException" );
				throw new VariableNotFoundException("Variable(s) "+ columnames.toString() + " no encontrada(s) en la base de datos" );
			}
		}
	}
	
}
