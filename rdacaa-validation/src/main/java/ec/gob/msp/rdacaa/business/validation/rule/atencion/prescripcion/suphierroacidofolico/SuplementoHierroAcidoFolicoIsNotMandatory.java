package ec.gob.msp.rdacaa.business.validation.rule.atencion.prescripcion.suphierroacidofolico;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 5)
public class SuplementoHierroAcidoFolicoIsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		String suplementoHierroAcidoFolico = getVariableFromMap(RdacaaVariableKeyCatalog.PRESCRIPCION_SUPLEMENTOS_ACIDO_FOLICO);
		return (!isMandatory(RdacaaVariableKeyCatalog.PRESCRIPCION_SUPLEMENTOS_ACIDO_FOLICO)
				&& ValidationSupport.isNotDefined(suplementoHierroAcidoFolico));
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}
