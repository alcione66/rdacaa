package ec.gob.msp.rdacaa.business.validation.rule.atencion.profespecialidad;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class EspecialidadProfesionalIsIncorrect extends BaseRule<String> {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {
		String especialidadProfesional = getVariableFromMap(RdacaaVariableKeyCatalog.ATENCION_CODIGO_ESPECIALIDAD);

		Integer especialidadProfesionalInt = null;

		try {
			especialidadProfesionalInt = Integer.parseInt(especialidadProfesional);
		} catch (Exception e) {
			addValidationResult(RdacaaVariableKeyCatalog.ATENCION_CODIGO_ESPECIALIDAD,
					ValidationResultCatalogConstants.CODIGO_ERROR_ATENCION_CODIGO_ESPECIALIDAD_NO_NUMERICO);
			return RuleState.BREAK;
		}

		boolean isValidValue = validationQueryService.isIdEspecialidadValido(especialidadProfesionalInt);
		if (!isValidValue) {
			addValidationResult(RdacaaVariableKeyCatalog.ATENCION_CODIGO_ESPECIALIDAD,
					ValidationResultCatalogConstants.CODIGO_ERROR_ATENCION_CODIGO_ESPECIALIDAD_ES_INCORRECTO);
		}

		if (!isValidValue) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}
	}
}
