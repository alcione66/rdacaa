package ec.gob.msp.rdacaa.business.validation.rule.atencion.grupovulnerable.gpr_vulnerable4;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 3)
public class GrupoVulnerable4IsExcludedRule extends BaseRule<String>{

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.GRUPO_VULNERABLE_4);
	}

	@Then
	public RuleState then() {
		
		String grupoVulnerable4 = getVariableFromMap(RdacaaVariableKeyCatalog.GRUPO_VULNERABLE_4);
		if(!ValidationSupport.isNotDefined(grupoVulnerable4)){
			addValidationResult(RdacaaVariableKeyCatalog.GRUPO_VULNERABLE_4,
					ValidationResultCatalogConstants.CODIGO_ERROR_GRUPO_VULNERABLE_4_NO_DEFINIDA);
		}
		
		return RuleState.BREAK;

	}
}
