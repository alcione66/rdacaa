package ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.prodto3;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 5)
public class Procedimiento3IsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		String procedimiento3 = getVariableFromMap(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_3);
		return (!isMandatory(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_3)
				&& ValidationSupport.isNotDefined(procedimiento3));
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}
