package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacuna5;

import java.util.Optional;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.entity.Vacuna;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class Vacuna5IsIncorrectRule extends UtilsRule {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {

		String vacuna5 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_APLICACION_5);
		Optional<Vacuna> vacunaOpt = validationQueryService.findVacunaById(vacuna5);

		if (!(StringUtils.isNumeric(vacuna5) && vacunaOpt.isPresent())) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_APLICACION_5,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_APLICACION_5_ES_INCORRECTO);
			return RuleState.BREAK;
		}

		Vacuna vacuna = vacunaOpt.get();

		String sexo = getSexoString();

		if (!((vacuna.getHombre().equals(1) && sexo.equals("M")) || (vacuna.getMujer().equals(1) && sexo.equals("F"))
				|| (vacuna.getIntersexual().equals(1) && sexo.equals("I")))) {
			
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_APLICACION_5,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_APLICACION_5_SEXO_INCORRECTO);
			return RuleState.BREAK;
		}
		
		return RuleState.NEXT;

	}
}
