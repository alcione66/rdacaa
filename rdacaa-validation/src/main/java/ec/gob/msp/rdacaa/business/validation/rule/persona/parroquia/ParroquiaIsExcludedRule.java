package ec.gob.msp.rdacaa.business.validation.rule.persona.parroquia;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/

@Rule(order = 3)
public class ParroquiaIsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.PERSONA_CODIGO_PARROQUIA);
	}

	@Then
	public RuleState then() {
		
		String parroquia = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_PARROQUIA);
		if(!ValidationSupport.isNotDefined(parroquia)){
			addValidationResult(RdacaaVariableKeyCatalog.PERSONA_CODIGO_PARROQUIA,
					ValidationResultCatalogConstants.CODIGO_ERROR_CODIGO_PARROQUIA_NO_DEFINIDA);
		}
		
		return RuleState.BREAK;

	}
}
