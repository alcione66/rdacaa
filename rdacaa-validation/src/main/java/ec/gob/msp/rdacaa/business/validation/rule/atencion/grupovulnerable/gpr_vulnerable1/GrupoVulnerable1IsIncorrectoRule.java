/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.atencion.grupovulnerable.gpr_vulnerable1;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

/**
 *
 * @author eduardo
 */
@Rule(order = 6)
public class GrupoVulnerable1IsIncorrectoRule extends UtilsRule {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {
		String grupoVulnerable1 = getVariableFromMap(RdacaaVariableKeyCatalog.GRUPO_VULNERABLE_1);

		boolean isNumeric = StringUtils.isNumeric(grupoVulnerable1);
		if (!isNumeric) {
			addValidationResult(RdacaaVariableKeyCatalog.GRUPO_VULNERABLE_1,
					ValidationResultCatalogConstants.CODIGO_ERROR_GRUPO_VULNERABLE_1_NONUMERICO);
		}

		boolean isEdadValida = getEdadAnioMesDias() != null;
		if (!isEdadValida) {
			addValidationResult(RdacaaVariableKeyCatalog.GRUPO_VULNERABLE_1,
					ValidationResultCatalogConstants.CODIGO_ERROR_GRUPO_VULNERABLE_1_REQUIERE_FECHA_NAC_FECHA_ATENCION_VALIDA);
		}

		boolean isSexoValido = getSexoString() != null;
		if (!isSexoValido) {
			addValidationResult(RdacaaVariableKeyCatalog.GRUPO_VULNERABLE_1,
					ValidationResultCatalogConstants.CODIGO_ERROR_GRUPO_VULNERABLE_1_REQUIERE_SEXO_VALIDO);
		}

		if (!isNumeric || !isEdadValida || !isSexoValido) {
			return RuleState.BREAK;
		}

		Integer edadAnios = getEdadAnioParcial();
		Integer edadMeses = getEdadMesParcial();
		Integer edadDias = getEdadDiasParcial();

		Integer sexoId = getSexoInteger();

		boolean isEmbarazada = isEmbarazada();

		Integer isEmbarazadaId = isEmbarazada ? 1 : 0;

		boolean isGrupoVulnerableIncorrecto = !this.validationQueryService
				.isGrupoVulnerableValido(Integer.valueOf(grupoVulnerable1))
				|| !this.validationQueryService.isGrupoVulnerableValidoPorEdadSexoEmbarazada(
						Integer.parseInt(grupoVulnerable1), edadAnios, edadMeses, edadDias, sexoId, isEmbarazadaId);

		if (isGrupoVulnerableIncorrecto) {
			addValidationResult(RdacaaVariableKeyCatalog.GRUPO_VULNERABLE_1,
					ValidationResultCatalogConstants.CODIGO_ERROR_GRUPO_VULNERABLE_1_INCORRECTO);
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}
	}
}
