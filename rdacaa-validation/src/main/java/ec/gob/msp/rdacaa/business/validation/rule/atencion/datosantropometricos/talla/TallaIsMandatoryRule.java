package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.talla;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 4)
public class TallaIsMandatoryRule extends BaseRule<String> {
	@When
	public boolean when() {
		return true;

	}

	@Then
	public RuleState then() {

		String talla = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA);
		if (isMandatory(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA)
				&& ValidationSupport.isNotDefined(talla)) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TALLA_ES_MANDATORIA);
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}
	}
}
