package ec.gob.msp.rdacaa.business.validation.rule.persona.fechanacimiento;

import java.util.Date;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;
import ec.gob.msp.rdacaa.utilitario.enumeracion.ComunEnum;
import ec.gob.msp.rdacaa.utilitario.fecha.FechasUtil;

@Rule(order = 6)
public class FechaNacimientoNotInRangeRule extends BaseRule<String>{

	
	@When
	public boolean when() {
		String fechaNacimiento = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO);
		Date fecNac = FechasUtil.formateadorStringAFecha(ComunEnum.PATRON_FECHA6.getDescripcion(), fechaNacimiento);
		String fechaAtencion = getVariableFromMap(RdacaaVariableKeyCatalog.ATENCION_FECHA);
		Date fecHasta = FechasUtil.formateadorStringAFecha(ComunEnum.PATRON_FECHA6.getDescripcion(), fechaAtencion);
		Date fecDesde = FechasUtil.operarFecha(fecHasta, -120, 0, 0);
		return FechasUtil.estaEntreDosFecha(fecDesde, fecHasta, fecNac);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO,
				ValidationResultCatalogConstants.CODIGO_ERROR_FECHANACIMIENTO_FUERA_DE_RANGO);
		return RuleState.BREAK;
	}
	
}
