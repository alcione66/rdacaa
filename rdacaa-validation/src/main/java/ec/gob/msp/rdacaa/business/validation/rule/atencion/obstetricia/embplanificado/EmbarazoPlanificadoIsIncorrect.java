package ec.gob.msp.rdacaa.business.validation.rule.atencion.obstetricia.embplanificado;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class EmbarazoPlanificadoIsIncorrect extends UtilsRule {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {
		String embarazoPlanificado = getVariableFromMap(RdacaaVariableKeyCatalog.OBSTETRICIA_ES_EMBARAZO_PLANIFICADO);
		Integer edadAniosMesesDias = getEdadAnioMesDias();

		boolean isValidValue = "0".equals(embarazoPlanificado) || "1".equals(embarazoPlanificado);
		
		boolean isEmbarazada = isEmbarazada();
		boolean isMujer = "F".equals(getSexoString());

		if (!isValidValue) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_ES_EMBARAZO_PLANIFICADO,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_ES_EMBARAZO_PLANIFICADO_ES_INCORRECTO);
		}
		
		boolean requiereMujerEmbarazadaParaValorValido = !(isMujer && isEmbarazada) && isValidValue;
		if (requiereMujerEmbarazadaParaValorValido) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_ES_EMBARAZO_PLANIFICADO,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_ES_EMBARAZO_PLANIFICADO_REQUIERE_MUJER_EMBARAZADA);
		}

		if (edadAniosMesesDias == null) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_ES_EMBARAZO_PLANIFICADO,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_ES_EMBARAZO_PLANIFICADO_FECHAS_INVALIDAS);
			return RuleState.BREAK;
		}

		boolean requiereMayor8AniosMenor59Anios = !(edadAniosMesesDias >= 80000 && edadAniosMesesDias <= 590000)
				&& !ValidationSupport.isNotDefined(embarazoPlanificado);
		if (requiereMayor8AniosMenor59Anios) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_ES_EMBARAZO_PLANIFICADO,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_ES_EMBARAZO_PLANIFICADO_DEBE_TENER_DEFAULT_VALUE);
		}
		
		if(!isValidValue || requiereMujerEmbarazadaParaValorValido || requiereMayor8AniosMenor59Anios) {
			return RuleState.BREAK;
		}

		return RuleState.NEXT;
	}
}
