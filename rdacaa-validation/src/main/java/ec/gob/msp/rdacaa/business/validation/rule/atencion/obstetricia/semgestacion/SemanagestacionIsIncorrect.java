package ec.gob.msp.rdacaa.business.validation.rule.atencion.obstetricia.semgestacion;

import java.util.Date;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;
import ec.gob.msp.rdacaa.utilitario.medico.UtilitarioMedico;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class SemanagestacionIsIncorrect extends UtilsRule {

	private static final double MAXIMO_SEMANAS_GESTACION = 42.00d;

	private static final String CODIGO_METODO_FUM = "2577";

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {
		String semGestacion = getVariableFromMap(RdacaaVariableKeyCatalog.OBSTETRICIA_VALOR_SEMANA_GESTACION);

		Double semGestacionNumber = null;

		try {
			semGestacionNumber = Double.parseDouble(semGestacion);
		} catch (Exception e) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_VALOR_SEMANA_GESTACION,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_VALOR_SEMANA_GESTACION_NO_NUMERICO);
			return RuleState.BREAK;
		}

		boolean valorSuperaMaximo = semGestacionNumber > MAXIMO_SEMANAS_GESTACION;
		if (valorSuperaMaximo) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_VALOR_SEMANA_GESTACION,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_VALOR_SEMANA_GESTACION_SUPERA_MAXIMO);
			return RuleState.BREAK;
		}

		String metodoSemanagestacion = getVariableFromMap(
				RdacaaVariableKeyCatalog.OBSTETRICIA_CODIGO_METODO_DET_SEMANAS_GESTACION);
		boolean fumHasErrors = variableHasErrors(RdacaaVariableKeyCatalog.OBSTETRICIA_FECHA_ULTIMA_MESTRUACION);
		boolean metodoSemGestacionHasErrors = variableHasErrors(
				RdacaaVariableKeyCatalog.OBSTETRICIA_CODIGO_METODO_DET_SEMANAS_GESTACION);
		if (CODIGO_METODO_FUM.equals(metodoSemanagestacion) && (fumHasErrors || metodoSemGestacionHasErrors)) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_VALOR_SEMANA_GESTACION,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_VALOR_SEMANA_GESTACION_FUM_O_METODO_ERRORES);
		}

		if (CODIGO_METODO_FUM.equals(metodoSemanagestacion) && !metodoSemGestacionHasErrors && !fumHasErrors ) {
			String fum = getVariableFromMap(RdacaaVariableKeyCatalog.OBSTETRICIA_FECHA_ULTIMA_MESTRUACION);
			Date fumDate = getFechaFromString(fum);
                        String fechaAtencionString = getVariableFromMap(RdacaaVariableKeyCatalog.ATENCION_FECHA);
                        Date fechaAtencion = getFechaFromString(fechaAtencionString);
			Double semanagestacionactual = UtilitarioMedico.calcularSemanasEmbarazo(fumDate, fechaAtencion);
			if (!semanagestacionactual.equals(semGestacionNumber)) {
				ValidationResult error = getValidationResultFromMap(
						ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_VALOR_SEMANA_GESTACION_NO_IGUAL_CALCULADO);
				error.setMessage(error.getMessage() + " " + semanagestacionactual);
				addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_VALOR_SEMANA_GESTACION, error);
			}
		}

		Integer edadAniosMesesDias = getEdadAnioMesDias();
		boolean isEmbarazada = isEmbarazada();
		boolean isMujer = "F".equals(getSexoString());

		boolean requiereMujerEmbarazadaParaValorValido = !(isMujer && isEmbarazada) && !valorSuperaMaximo;
		if (requiereMujerEmbarazadaParaValorValido) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_VALOR_SEMANA_GESTACION,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_VALOR_SEMANA_GESTACION_REQUIERE_MUJER_EMBARAZADA);
		}

		if (edadAniosMesesDias == null) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_VALOR_SEMANA_GESTACION,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_VALOR_SEMANA_GESTACION_FECHAS_INVALIDAS);
			return RuleState.BREAK;
		}

		boolean requiereMayor8AniosMenor59Anios = !(edadAniosMesesDias >= 80000 && edadAniosMesesDias <= 590000)
				&& !ValidationSupport.isNotDefined(semGestacion);
		if (requiereMayor8AniosMenor59Anios) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_VALOR_SEMANA_GESTACION,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_VALOR_SEMANA_GESTACION_DEBE_TENER_DEFAULT_VALUE);
		}

		if (requiereMujerEmbarazadaParaValorValido || requiereMayor8AniosMenor59Anios) {
			return RuleState.BREAK;
		}

		return RuleState.NEXT;

	}
}
