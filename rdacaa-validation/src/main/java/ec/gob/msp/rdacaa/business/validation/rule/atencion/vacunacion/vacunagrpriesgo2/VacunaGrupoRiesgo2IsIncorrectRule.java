package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunagrpriesgo2;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

@Rule(order = 6)
public class VacunaGrupoRiesgo2IsIncorrectRule extends UtilsRule {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {

		String vacuna2Id = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_APLICACION_2);
		String vacunaGrupoRiesgo2 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_GRUPO_RIESGO_2);
		
		boolean vacuna2IdHasErrors = variableHasErrors(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_APLICACION_2);
		
		if(vacuna2IdHasErrors || ValidationSupport.isNotDefined(vacunaGrupoRiesgo2)) {
			return RuleState.NEXT;
		}

		if (!(StringUtils.isNumeric(vacuna2Id) && StringUtils.isNumeric(vacunaGrupoRiesgo2))) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_GRUPO_RIESGO_2,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_GRUPO_RIESGO_2_COD_VACUNA_DOSIS_NUMERICOS);
			return RuleState.BREAK;
		}
		
		boolean isHombre = "M".equals(getSexoString());
		boolean isMujer = "F".equals(getSexoString());
		boolean isIntersexual = "I".equals(getSexoString());
		
		if(getSexoString() == null) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_GRUPO_RIESGO_2,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_GRUPO_RIESGO_2_SEXO_INCORRECTO);
			return RuleState.BREAK;
		}
		
		boolean vacunaHasGrupoRiesgo = validationQueryService.vacunaHasGrupoRiesgo(vacuna2Id, isHombre, isMujer, isIntersexual);
		
		if(vacunaHasGrupoRiesgo && !ValidationSupport.isNotDefined(vacunaGrupoRiesgo2)) {
			if(!validationQueryService.isGrupoRiesgoValido(vacuna2Id, vacunaGrupoRiesgo2, isHombre, isMujer, isIntersexual)) {
				addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_GRUPO_RIESGO_2,
						ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_GRUPO_RIESGO_2_DEBE_TENER_GRUPO_RIESGO_VALIDO);
				return RuleState.BREAK;
			}else {
				return RuleState.NEXT;
			}
		} else if(!vacunaHasGrupoRiesgo && !ValidationSupport.isNotDefined(vacunaGrupoRiesgo2)) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_GRUPO_RIESGO_2,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_GRUPO_RIESGO_2_DEBE_TENER_GRUPO_RIESGO_NO_DEFINIDO);
			return RuleState.BREAK;
		} else if(!vacunaHasGrupoRiesgo && ValidationSupport.isNotDefined(vacunaGrupoRiesgo2)) {
			return RuleState.NEXT;
		} else if(vacunaHasGrupoRiesgo && ValidationSupport.isNotDefined(vacunaGrupoRiesgo2)) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_GRUPO_RIESGO_2,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_GRUPO_RIESGO_2_DEBE_TENER_GRUPO_RIESGO_VALIDO);
			return RuleState.BREAK;
		}
		
		return RuleState.NEXT;
	}
}
