package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunaesquema5;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class VacunaEsquema5IsIncorrectRule extends UtilsRule {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {

		String vacunaEsquema5 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_ESQUEMA_5);
		String vacuna5Id = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_APLICACION_5);
		String vacunaNroDosis5 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_DOSIS_5);

		if (!(StringUtils.isNumeric(vacunaEsquema5) && StringUtils.isNumeric(vacuna5Id)
				&& StringUtils.isNumeric(vacunaNroDosis5))) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_ESQUEMA_5,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_ESQUEMA_5_COD_VACUNA_DOSIS_NUMERICOS);
			return RuleState.BREAK;
		}

		Integer edadAnioParcial = getEdadAnioParcial();

		Integer edadMesParcial = getEdadMesParcial();

		Integer edadDiasParcial = getEdadDiasParcial();

		if (!validationQueryService.isEsquemaVacunacionValido(vacunaEsquema5, vacuna5Id, vacunaNroDosis5)) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_ESQUEMA_5,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_ESQUEMA_5_ES_INCORRECTO);
			return RuleState.BREAK;
		} else if (edadAnioParcial != null && edadMesParcial != null && edadDiasParcial != null
				&& validationQueryService.verifyApplicationVacunaByEdadAndEsquemaId(edadDiasParcial, edadMesParcial,
				edadAnioParcial, Integer.parseInt(vacunaEsquema5)) == null) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_ESQUEMA_5,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_ESQUEMA_5_EDAD_INCORRECTA);
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}
		
	}
}
