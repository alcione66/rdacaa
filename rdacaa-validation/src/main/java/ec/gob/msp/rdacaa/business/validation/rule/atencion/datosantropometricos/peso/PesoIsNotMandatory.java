package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.peso;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 5)
public class PesoIsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		
		String peso = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO);
		return (!isMandatory(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO) &&
				ValidationSupport.isNotDefined(peso));
		
	}

	@Then
	public RuleState then() {
		return RuleState.NEXT;
	}
}
