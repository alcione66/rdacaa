package ec.gob.msp.rdacaa.business.validation.database.common;

public class CustomSQLException extends RuntimeException {

	private static final long serialVersionUID = -1386029621548472631L;

	public CustomSQLException() {
		super();
	}

	public CustomSQLException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public CustomSQLException(String message, Throwable cause) {
		super(message, cause);
	}

	public CustomSQLException(String message) {
		super(message);
	}

	public CustomSQLException(Throwable cause) {
		super(cause);
	}
	
}
