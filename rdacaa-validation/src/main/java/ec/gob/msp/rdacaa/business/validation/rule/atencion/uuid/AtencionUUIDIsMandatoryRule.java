package ec.gob.msp.rdacaa.business.validation.rule.atencion.uuid;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 4)
public class AtencionUUIDIsMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		return true;

	}

	@Then
	public RuleState then() {

		String atencionUUID = getVariableFromMap(RdacaaVariableKeyCatalog.ATENCION_CODIGO_UUID);
		if (isMandatory(RdacaaVariableKeyCatalog.ATENCION_CODIGO_UUID)
				&& ValidationSupport.isNotDefined(atencionUUID)) {
			addValidationResult(RdacaaVariableKeyCatalog.ATENCION_CODIGO_UUID,
					ValidationResultCatalogConstants.CODIGO_ERROR_ATENCION_CODIGO_UUID_ES_MANDATORIA);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.ATENCION_CODIGO_UUID)
				&& ValidationSupport.isNotDefined(atencionUUID)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}

}
