package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunaesquema3;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class VacunaEsquema3IsIncorrectRule extends UtilsRule {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {

		String vacunaEsquema3 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_ESQUEMA_3);
		String vacuna3Id = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_APLICACION_3);
		String vacunaNroDosis3 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_DOSIS_3);

		if (!(StringUtils.isNumeric(vacunaEsquema3) && StringUtils.isNumeric(vacuna3Id)
				&& StringUtils.isNumeric(vacunaNroDosis3))) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_ESQUEMA_3,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_ESQUEMA_3_COD_VACUNA_DOSIS_NUMERICOS);
			return RuleState.BREAK;
		}

		Integer edadAnioParcial = getEdadAnioParcial();

		Integer edadMesParcial = getEdadMesParcial();

		Integer edadDiasParcial = getEdadDiasParcial();

		if (!validationQueryService.isEsquemaVacunacionValido(vacunaEsquema3, vacuna3Id, vacunaNroDosis3)) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_ESQUEMA_3,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_ESQUEMA_3_ES_INCORRECTO);
			return RuleState.BREAK;
		} else if (edadAnioParcial != null && edadMesParcial != null && edadDiasParcial != null
				&& validationQueryService.verifyApplicationVacunaByEdadAndEsquemaId(edadDiasParcial, edadMesParcial,
				edadAnioParcial, Integer.parseInt(vacunaEsquema3)) == null) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_ESQUEMA_3,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_ESQUEMA_3_EDAD_INCORRECTA);
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}
		
	}
}
