package ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.tipoatencion1;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 5)
public class TipoAtencionDiag1IsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		String tipoatenciondiagnostico1 = getVariableFromMap(
				RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_ATENCION_1);
		return (!isMandatory(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_ATENCION_1)
				&& ValidationSupport.isNotDefined(tipoatenciondiagnostico1));
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}
