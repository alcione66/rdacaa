package ec.gob.msp.rdacaa.business.validation.rule.atencion.obstetricia.fum;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 2)
public class FUMIsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String fum = getVariableFromMap(RdacaaVariableKeyCatalog.OBSTETRICIA_FECHA_ULTIMA_MESTRUACION);
		return ValidationSupport.isNullOrEmptyString(fum);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_FECHA_ULTIMA_MESTRUACION,
				ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_FECHA_ULTIMA_MESTRUACION_NULO);
		return RuleState.BREAK;
	}

}
