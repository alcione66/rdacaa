package ec.gob.msp.rdacaa.business.validation.rule.atencion.sivan.madreperlactancia;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class MadrePeriodoLactanciaIsIncorrect extends UtilsRule {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {
		String madrePeriodoLactancia = getVariableFromMap(RdacaaVariableKeyCatalog.SIVAN_ES_MADRE_PERIODO_LACTANCIA);

		Integer edadAniosMesDias = getEdadAnioMesDias();

		boolean isValidValue = "0".equals(madrePeriodoLactancia) || "1".equals(madrePeriodoLactancia);

		boolean isMujer = "F".equals(getSexoString());

		if (!isValidValue) {
			addValidationResult(RdacaaVariableKeyCatalog.SIVAN_ES_MADRE_PERIODO_LACTANCIA,
					ValidationResultCatalogConstants.CODIGO_ERROR_SIVAN_ES_MADRE_PERIODO_LACTANCIA_ES_INCORRECTO);
		}

		boolean requiereEdadValorValido = !(isMujer && edadAniosMesDias != null && edadAniosMesDias >= 80000
				&& edadAniosMesDias < 560000);
		if (requiereEdadValorValido) {
			addValidationResult(RdacaaVariableKeyCatalog.SIVAN_ES_MADRE_PERIODO_LACTANCIA,
					ValidationResultCatalogConstants.CODIGO_ERROR_SIVAN_ES_MADRE_PERIODO_LACTANCIA_REQUIERE_FECHAS_VALIDAS);
		}

		if (!isValidValue || requiereEdadValorValido) {
			return RuleState.BREAK;
		}

		return RuleState.NEXT;
	}
}
