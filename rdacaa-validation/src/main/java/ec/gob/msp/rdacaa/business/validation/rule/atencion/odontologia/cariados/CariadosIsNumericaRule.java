package ec.gob.msp.rdacaa.business.validation.rule.atencion.odontologia.cariados;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 6)
public class CariadosIsNumericaRule extends BaseRule<String>{

	@When
	public boolean when() {
		String dientesCariados = getVariableFromMap(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_CARIADAS);
		return !StringUtils.isNumeric(dientesCariados);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_CARIADAS,
				ValidationResultCatalogConstants.CODIGO_ERROR_DIENTES_CARIADOS_NO_NUMERICO);
		return RuleState.BREAK;
	}
}
