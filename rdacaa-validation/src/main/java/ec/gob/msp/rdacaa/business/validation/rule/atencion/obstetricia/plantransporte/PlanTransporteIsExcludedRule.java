package ec.gob.msp.rdacaa.business.validation.rule.atencion.obstetricia.plantransporte;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */

@Rule(order = 3)
public class PlanTransporteIsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.OBSTETRICIA_TIENE_PLAN_TRANSPORTE);
	}

	@Then
	public RuleState then() {

		String planTransporte = getVariableFromMap(RdacaaVariableKeyCatalog.OBSTETRICIA_TIENE_PLAN_TRANSPORTE);
		if (!ValidationSupport.isNotDefined(planTransporte)) {
			addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_TIENE_PLAN_TRANSPORTE,
					ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_TIENE_PLAN_TRANSPORTE_NO_DEFINIDA);
		}

		return RuleState.BREAK;

	}
}
