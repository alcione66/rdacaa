package ec.gob.msp.rdacaa.business.validation.rule.persona.interconsulta;

import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 7)
public class InterconsultaIsIncorrectoRule extends BaseRule<String> {

	@When
	public boolean when() {
		String interconsulta = getVariableFromMap(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_INTERCONSULTA_TIPO);
		return !this.validationQueryService.isCodigoTipoInterconsultaValido(Integer.valueOf(interconsulta));
	}

	@Then
	public void then() {
		addValidationResult(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_INTERCONSULTA_TIPO,
				ValidationResultCatalogConstants.CODIGO_ERROR_CODIGO_INTERCONSULTA_INCORRECTO);
	}
}
