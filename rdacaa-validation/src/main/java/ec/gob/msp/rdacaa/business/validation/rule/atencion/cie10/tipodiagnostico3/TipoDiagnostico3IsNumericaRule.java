/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.tipodiagnostico3;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class TipoDiagnostico3IsNumericaRule extends BaseRule<String> {
	@When
	public boolean when() {
		String tipodiagnostico3 = getVariableFromMap(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_3);
		return !StringUtils.isNumeric(tipodiagnostico3);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_3,
				ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_DIAGNOSTICO_3_NONUMERICO);
		return RuleState.BREAK;
	}
}
