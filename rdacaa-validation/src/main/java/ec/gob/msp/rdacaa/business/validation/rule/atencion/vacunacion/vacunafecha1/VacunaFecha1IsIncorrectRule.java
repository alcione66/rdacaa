package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunafecha1;

import java.util.Date;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class VacunaFecha1IsIncorrectRule extends UtilsRule {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {

		String vacunaFechaString1 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_FECHA_1);
		String fechaAtencionString = getVariableFromMap(RdacaaVariableKeyCatalog.ATENCION_FECHA);
		boolean isFechaVacunaValida = isFechaValida(vacunaFechaString1) && 
				getFechaFromString(vacunaFechaString1).after(getFechaFromString("2018-12-31"));
		if (!isFechaVacunaValida) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_FECHA_1,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_FECHA_1_ES_INCORRECTA);
			return RuleState.BREAK;
		}

		boolean isFechaAtencionValida =isFechaValida(fechaAtencionString) && 
				getFechaFromString(fechaAtencionString).after(getFechaFromString("2018-12-31"));
		if (!isFechaAtencionValida) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_FECHA_1,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_FECHA_1_FECHA_ATENCION_ES_INCORRECTA);
			return RuleState.BREAK;
		}

		Date vacunaFecha1 = getFechaFromString(vacunaFechaString1);
		Date fechaAtencion = getFechaFromString(fechaAtencionString);
		
		if(vacunaFecha1.after(fechaAtencion)) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_FECHA_1,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_FECHA_1_POSTERIOR_FECHA_ATENCION);
			return RuleState.BREAK;
		}
		
		return RuleState.NEXT;
	}
}
