package ec.gob.msp.rdacaa.business.validation.database.common;

public class MissingVariableException extends RuntimeException {

	private static final long serialVersionUID = 300012677501921505L;

	public MissingVariableException() {
		super();
	}

	public MissingVariableException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public MissingVariableException(String message, Throwable cause) {
		super(message, cause);
	}

	public MissingVariableException(String message) {
		super(message);
	}

	public MissingVariableException(Throwable cause) {
		super(cause);
	}

}
