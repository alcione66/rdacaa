package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunanrodosis3;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class VacunaNroDosis3IsIncorrectRule extends BaseRule<String> {

	@When
	public boolean when() {
		String vacunaNroDosis3 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_DOSIS_3);
		return !StringUtils.isNumeric(vacunaNroDosis3);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_DOSIS_3,
				ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_DOSIS_3_NO_ES_NUMERICO);
		return RuleState.BREAK;
	}
}
