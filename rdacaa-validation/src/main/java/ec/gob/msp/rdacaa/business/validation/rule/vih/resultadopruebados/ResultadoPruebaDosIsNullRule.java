package ec.gob.msp.rdacaa.business.validation.rule.vih.resultadopruebados;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 2)
public class ResultadoPruebaDosIsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		Boolean valor = false;
		if(!variableHasErrors(RdacaaVariableKeyCatalog.VIH_CODIGO_SEGUNDA_PRUEBA)){
			String resultado = getVariableFromMap(RdacaaVariableKeyCatalog.VIH_VALOR_RESULTADO_SEGUNDA_PRUEBA);
			valor = ValidationSupport.isNullOrEmptyString(resultado);
		}
		return valor;
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.VIH_VALOR_RESULTADO_SEGUNDA_PRUEBA,
				ValidationResultCatalogConstants.CODIGO_ERROR_VIH_RESULTADO_PRUEBA_DOS_NULO);
		return RuleState.BREAK;
	}
}
