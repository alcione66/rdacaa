package ec.gob.msp.rdacaa.business.validation.rule.atencion.obstetricia.planparto;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 5)
public class PlanPartoIsNotMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		String planParto = getVariableFromMap(RdacaaVariableKeyCatalog.OBSTETRICIA_TIENE_PLAN_PARTO);
		return (!isMandatory(RdacaaVariableKeyCatalog.OBSTETRICIA_TIENE_PLAN_PARTO)
				&& ValidationSupport.isNotDefined(planParto));
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}
