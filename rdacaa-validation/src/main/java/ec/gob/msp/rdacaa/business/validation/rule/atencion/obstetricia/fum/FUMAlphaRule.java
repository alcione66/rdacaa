/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.atencion.obstetricia.fum;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableValueResult;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 1)
public class FUMAlphaRule extends BaseRule<String> {
	@When
	public boolean when() {
		this.result = RdacaaRawRowResult.getInstance();
		List<ValidationResult> listaErrores = new ArrayList<>();
		this.result.addResultToField(RdacaaVariableKeyCatalog.OBSTETRICIA_FECHA_ULTIMA_MESTRUACION, RdacaaVariableValueResult
				.getInstance(getVariableFromMap(RdacaaVariableKeyCatalog.OBSTETRICIA_FECHA_ULTIMA_MESTRUACION), listaErrores));
		return true;
	}

	@Then
	public RuleState then() {
		return RuleState.NEXT;
	}
}
