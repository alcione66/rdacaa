package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunagrpriesgo2;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 4)
public class VacunaGrupoRiesgo2IsMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		return true;

	}

	@Then
	public RuleState then() {

		String vacunaGrupoRiesgo1 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_GRUPO_RIESGO_2);
		if (isMandatory(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_GRUPO_RIESGO_2)
				&& ValidationSupport.isNotDefined(vacunaGrupoRiesgo1)) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_GRUPO_RIESGO_2,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_GRUPO_RIESGO_2_ES_MANDATORIA);
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}

}
