package ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.tipodiagnostico5;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 5)
public class TipoDiagnostico5IsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		String tipodiagnostico5 = getVariableFromMap(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_5);
		return (!isMandatory(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_5)
				&& ValidationSupport.isNotDefined(tipodiagnostico5));
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}
