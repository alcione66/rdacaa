package ec.gob.msp.rdacaa.business.validation.rule.vih.pruebados;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.deliveredtechnologies.rulebook.model.RuleBook;

import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.RdacaaRuleExecutor;

/**
*
* @author dmurillo
*/
@Component
public class PruebaDosRuleGroupExecutor  extends RdacaaRuleExecutor {

	@Autowired
	public PruebaDosRuleGroupExecutor(
			@Qualifier("ruleBookReglasVihPruebaDos") RuleBook<RdacaaRawRowResult> ruleBook) {
		super();
		this.ruleBook = ruleBook;
		this.variableKey = RdacaaVariableKeyCatalog.VIH_CODIGO_SEGUNDA_PRUEBA;
	}
}
