package ec.gob.msp.rdacaa.business.validation.database.common;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.function.Function;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ec.gob.msp.rdacaa.seguridades.util.SeguridadUtil;
import ec.gob.msp.rdacaa.utilitario.exportdatabase.AtencionCipherDto;

public class DecipheringLocalCipheredDataFunction implements Function<Object[], Object> {

	private static final Logger logger = LoggerFactory.getLogger(DecipheringLocalCipheredDataFunction.class);

	@Override
	public Object apply(Object[] cipherObjects) {

		List<AtencionCipherDto> listaCipherDto = new ArrayList<>();

		if (cipherObjects[0] instanceof ResultSet) {
			ResultSet rs = (ResultSet) cipherObjects[0];
			
			byte[] key = (byte[]) cipherObjects[1];
			
			String username = (String) cipherObjects[2];

			try {
				mapToAtencionCipherDto(listaCipherDto, rs);
				decipherVariables(listaCipherDto, key, username);
			} catch (SQLException e) {
				throw new CustomSQLException("Error obteniendo datos del ResultSet", e);
			}
		}

		return listaCipherDto;
	}

	private void decipherVariables(List<AtencionCipherDto> listaCipherDto, byte[] key, String username){
		
		listaCipherDto.parallelStream().forEach(dto -> {
			try {
				String ksCident = SeguridadUtil.descifrarString(dto.getKsCident(), key, username);
				dto.setKsCident(ksCident);
				logger.debug("ksCident => {}", ksCident);
			} catch (GeneralSecurityException | IOException e) {
				throw new DecipheringErrorException("Error descifrando el archivo", e);
			}
		});
	}

	private void mapToAtencionCipherDto(List<AtencionCipherDto> listaCipherDto, ResultSet rs) throws SQLException {

		while (rs.next()) {
			AtencionCipherDto dto = new AtencionCipherDto();
			dto.setUuid(rs.getString("COD_UUID"));
			dto.setKsCident(rs.getString("KS_CIDENT"));
			dto.setKuCident(rs.getString("KU_CIDENT"));
			dto.setKuKeyid(rs.getString("KU_KEYID"));
			listaCipherDto.add(dto);
		}
	}

}
