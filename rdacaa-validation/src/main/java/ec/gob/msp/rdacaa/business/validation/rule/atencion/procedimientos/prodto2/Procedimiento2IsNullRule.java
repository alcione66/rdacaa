package ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.prodto2;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 2)
public class Procedimiento2IsNullRule extends BaseRule<String>{

	@When
    public boolean when() {
        String procedimiento2 = getVariableFromMap(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_2);
        return ValidationSupport.isNullOrEmptyString(procedimiento2);
    }
	
	@Then
    public RuleState then() {
        addValidationResult(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_2,
        		ValidationResultCatalogConstants.CODIGO_ERROR_PROCEDIMIENTO_2_NULO);
        return RuleState.BREAK;
    }
}
