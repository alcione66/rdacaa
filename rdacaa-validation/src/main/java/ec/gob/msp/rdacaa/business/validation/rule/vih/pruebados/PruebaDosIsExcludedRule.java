package ec.gob.msp.rdacaa.business.validation.rule.vih.pruebados;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/

@Rule(order = 3)
public class PruebaDosIsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.VIH_CODIGO_SEGUNDA_PRUEBA);
	}

	@Then
	public RuleState then() {
		
		String prueba = getVariableFromMap(RdacaaVariableKeyCatalog.VIH_CODIGO_SEGUNDA_PRUEBA);
		if(!ValidationSupport.isNotDefined(prueba)){
			addValidationResult(RdacaaVariableKeyCatalog.VIH_CODIGO_SEGUNDA_PRUEBA,
					ValidationResultCatalogConstants.CODIGO_ERROR_VIH_CODIGO_PRUEBA_DOS_NO_DEFINIDA);
		}
		
		return RuleState.BREAK;

	}
}
