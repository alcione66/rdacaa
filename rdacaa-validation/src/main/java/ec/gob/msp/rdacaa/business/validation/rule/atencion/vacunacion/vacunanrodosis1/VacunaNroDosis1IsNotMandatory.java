package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunanrodosis1;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 5)
public class VacunaNroDosis1IsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		String vacunaNroDosis1 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_DOSIS_1);
		return (!isMandatory(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_DOSIS_1)
				&& ValidationSupport.isNotDefined(vacunaNroDosis1));
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}
