/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.persona.tipobono;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author eduardo
 */
@Rule(order = 2)
public class TipoBonoIsNullRule extends BaseRule<String> {
        
        @When
	public boolean when() {
		String tipoBono = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_BONO);
		return ValidationSupport.isNullOrEmptyString(tipoBono);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_BONO,
				ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_BONO_NULL);
		return RuleState.BREAK;
	}
}
