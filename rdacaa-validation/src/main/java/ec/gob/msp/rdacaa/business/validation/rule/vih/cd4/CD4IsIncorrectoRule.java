package ec.gob.msp.rdacaa.business.validation.rule.vih.cd4;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 6)
public class CD4IsIncorrectoRule  extends BaseRule<String> {

	@When
	public boolean when() {
		String prueba = getVariableFromMap(RdacaaVariableKeyCatalog.VIH_CD4);
		if(StringUtils.isNumeric(prueba)){
			return !this.validationQueryService.validarRango(500,1500,Integer.valueOf(prueba));
		}
		return true;
	}

	@Then
	public void then() {
		addValidationResult(RdacaaVariableKeyCatalog.VIH_CD4,
				ValidationResultCatalogConstants.CODIGO_ERROR_VIH_CD4_INCORRECTO);
	}
}
