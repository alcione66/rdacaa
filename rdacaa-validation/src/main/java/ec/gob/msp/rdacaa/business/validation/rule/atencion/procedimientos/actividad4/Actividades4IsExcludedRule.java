package ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.actividad4;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 3)
public class Actividades4IsExcludedRule extends BaseRule<String>{

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_4);
	}

	@Then
	public RuleState then() {

		String actividades4 = getVariableFromMap(
				RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_4);
		if (!ValidationSupport.isNotDefined(actividades4)) {
			addValidationResult(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_4,
					ValidationResultCatalogConstants.CODIGO_ERROR_ACTIVIDAD_4_NO_DEFINIDO);
		}

		return RuleState.BREAK;

	}
}
