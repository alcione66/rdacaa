package ec.gob.msp.rdacaa.business.validation.rule.atencion.prescripcion.suphierroacidofolico;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class SuplementoHierroAcidoFolicoIsIncorrect extends UtilsRule {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {
		String suplementoHierroAcidoFolico = getVariableFromMap(
				RdacaaVariableKeyCatalog.PRESCRIPCION_SUPLEMENTOS_ACIDO_FOLICO);

		Integer edadAniosMesesDias = getEdadAnioMesDias();

		boolean isValidValue = "0".equals(suplementoHierroAcidoFolico) || "1".equals(suplementoHierroAcidoFolico);
		
		boolean isEmbarazada = isEmbarazada();
		boolean isMujer = "F".equals(getSexoString());

		if (!isValidValue) {
			addValidationResult(RdacaaVariableKeyCatalog.PRESCRIPCION_SUPLEMENTOS_ACIDO_FOLICO,
					ValidationResultCatalogConstants.CODIGO_ERROR_PRESCRIPCION_SUPLEMENTOS_ACIDO_FOLICO_ES_INCORRECTO);
		}
		
		boolean requiereMujerEmbarazadaParaValorValido = !(isMujer && isEmbarazada) && isValidValue;
		if (requiereMujerEmbarazadaParaValorValido) {
			addValidationResult(RdacaaVariableKeyCatalog.PRESCRIPCION_SUPLEMENTOS_ACIDO_FOLICO,
					ValidationResultCatalogConstants.CODIGO_ERROR_PRESCRIPCION_SUPLEMENTOS_HIERRO_JARABE_REQUIERE_MUJER_EMBARAZADA);
		}

		if (edadAniosMesesDias == null) {
			addValidationResult(RdacaaVariableKeyCatalog.PRESCRIPCION_SUPLEMENTOS_ACIDO_FOLICO,
					ValidationResultCatalogConstants.CODIGO_ERROR_PRESCRIPCION_SUPLEMENTOS_ACIDO_FOLICO_FECHAS_INVALIDAS);
			return RuleState.BREAK;
		}

		boolean requiereMayor8Anios = edadAniosMesesDias < 80000
				&& !ValidationSupport.isNotDefined(suplementoHierroAcidoFolico);
		if (requiereMayor8Anios) {
			addValidationResult(RdacaaVariableKeyCatalog.PRESCRIPCION_SUPLEMENTOS_ACIDO_FOLICO,
					ValidationResultCatalogConstants.CODIGO_ERROR_PRESCRIPCION_SUPLEMENTOS_ACIDO_FOLICO_DEBE_TENER_DEFAULT_VALUE);
		}
		
		if(!isValidValue || requiereMujerEmbarazadaParaValorValido || requiereMayor8Anios) {
			return RuleState.BREAK;
		}

		return RuleState.NEXT;
	}
}
