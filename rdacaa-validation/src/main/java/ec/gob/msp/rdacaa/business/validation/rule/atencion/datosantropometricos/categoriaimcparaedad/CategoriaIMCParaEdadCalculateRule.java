package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.categoriaimcparaedad;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Given;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.service.ScoreZAndCategoriaService;
import ec.gob.msp.rdacaa.business.service.SignosVitalesReglasValidacion;
import ec.gob.msp.rdacaa.business.service.SignosVitalesReglasValidacionNotFoundException;
import ec.gob.msp.rdacaa.business.service.ScoreZAndCategoriaService.PercentilDetalleNotFoundException;
import ec.gob.msp.rdacaa.business.service.ScoreZAndCategoriaService.PercentilNotFoundException;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

@Rule(order = 6)
public class CategoriaIMCParaEdadCalculateRule extends UtilsRule {

	@Given("scoreZAndCategoriaService")
	private ScoreZAndCategoriaService scoreZAndCategoriaService;

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {
		String categoriaIMCParaEdad = "999990000066666";
		String puntajeZIMCparaEdad = getRdacaaRawRowResultValue(
				RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_IMC_PARA_EDAD);
		boolean puntajeZIMCparaEdadHasErrors = variableHasErrors(
				RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_IMC_PARA_EDAD);
		String sexo = getSexoString();
		Integer edadAnioMesDias = getEdadAnioMesDias();

		boolean valuesNotDefined = ValidationSupport.isNotDefined(puntajeZIMCparaEdad);

		setRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_IMC_PARA_EDAD,
				categoriaIMCParaEdad);

		if (edadAnioMesDias == null) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_IMC_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_CATEGORIA_IMC_EDAD_FECHAS_FORMATO_INCORRECTO);
		}

		if (sexo == null) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_IMC_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_CATEGORIA_IMC_EDAD_SEXO_INCORRECTO);
		}
		
		if (puntajeZIMCparaEdadHasErrors) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_IMC_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_CATEGORIA_IMC_EDAD_PZ_IMC_EDAD_ERROR);
		}
		
		if (valuesNotDefined) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_IMC_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_CATEGORIA_IMC_EDAD_PZ_IMC_EDAD_NO_DEFINIDO);
		}

		if (edadAnioMesDias == null || sexo == null || puntajeZIMCparaEdadHasErrors || valuesNotDefined) {
			return RuleState.BREAK;
		}

		try {
			SignosVitalesReglasValidacion categoriaIMCparaEdad = scoreZAndCategoriaService
					.calcularCategoriaIMCparaEdad(Double.parseDouble(puntajeZIMCparaEdad), edadAnioMesDias, sexo);
			categoriaIMCParaEdad = categoriaIMCparaEdad.getIdSignoVitalAlerta().toString();
		} catch (PercentilNotFoundException | PercentilDetalleNotFoundException
				| SignosVitalesReglasValidacionNotFoundException e) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_IMC_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_CATEGORIA_IMC_EDAD_NO_APLICA);

			return RuleState.BREAK;
		}
		
		setRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_IMC_PARA_EDAD,
				categoriaIMCParaEdad);

		addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_IMC_PARA_EDAD,
				ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_CATEGORIA_IMC_EDAD_VALOR);

		return RuleState.NEXT;
	}
}
