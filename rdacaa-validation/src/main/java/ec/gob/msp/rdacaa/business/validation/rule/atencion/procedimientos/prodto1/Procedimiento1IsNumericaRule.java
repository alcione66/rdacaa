package ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.prodto1;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 6)
public class Procedimiento1IsNumericaRule extends BaseRule<String>  {

	@When
    public boolean when() {
        String procedimiento1 = getVariableFromMap(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_1);
        return !StringUtils.isNumeric(procedimiento1);
    }

    @Then
    public RuleState then() {
        addValidationResult(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_1,
        		ValidationResultCatalogConstants.CODIGO_ERROR_PROCEDIMIENTO_1_NO_NUMERICO);
        return RuleState.BREAK;
    }
}
