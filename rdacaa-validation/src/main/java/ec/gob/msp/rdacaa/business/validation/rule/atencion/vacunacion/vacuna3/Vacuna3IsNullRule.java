package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacuna3;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 2)
public class Vacuna3IsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String vacuna3 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_APLICACION_3);
		return ValidationSupport.isNullOrEmptyString(vacuna3);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_APLICACION_3,
				ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_APLICACION_3_NULO);
		return RuleState.BREAK;
	}

}
