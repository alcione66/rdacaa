package ec.gob.msp.rdacaa.business.validation.rule.atencion.prescripcion.suphierroacidofolico;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */

@Rule(order = 3)
public class SuplementoHierroAcidoFolicoIsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.PRESCRIPCION_SUPLEMENTOS_ACIDO_FOLICO);
	}

	@Then
	public RuleState then() {

		String suplementoHierroAcidoFolico = getVariableFromMap(RdacaaVariableKeyCatalog.PRESCRIPCION_SUPLEMENTOS_ACIDO_FOLICO);
		if (!ValidationSupport.isNotDefined(suplementoHierroAcidoFolico)) {
			addValidationResult(RdacaaVariableKeyCatalog.PRESCRIPCION_SUPLEMENTOS_ACIDO_FOLICO,
					ValidationResultCatalogConstants.CODIGO_ERROR_PRESCRIPCION_SUPLEMENTOS_ACIDO_FOLICO_NO_DEFINIDA);
		}

		return RuleState.BREAK;

	}
}
