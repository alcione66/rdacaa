/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.persona.telefonopaciente;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author eduardo
 */
@Rule(order = 6)
public class TelefonoPacienteIsNumericaRule extends BaseRule<String> {
    @When
	public boolean when() {
		String archivo = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_TELEFONO_PACIENTE);
		return !StringUtils.isNumeric(archivo);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PERSONA_TELEFONO_PACIENTE,
				ValidationResultCatalogConstants.CODIGO_ERROR_TELEFONO_PACIENTE_NONUMERICO);
		return RuleState.BREAK;
	}
}
