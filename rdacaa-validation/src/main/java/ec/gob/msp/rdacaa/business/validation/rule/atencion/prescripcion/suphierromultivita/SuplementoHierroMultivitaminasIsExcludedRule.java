package ec.gob.msp.rdacaa.business.validation.rule.atencion.prescripcion.suphierromultivita;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */

@Rule(order = 3)
public class SuplementoHierroMultivitaminasIsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.PRESCRIPCION_SUPLEMENTOS_HIERRO_MULTIVITAMINAS);
	}

	@Then
	public RuleState then() {

		String suplementoHierroMultivitaminas = getVariableFromMap(RdacaaVariableKeyCatalog.PRESCRIPCION_SUPLEMENTOS_HIERRO_MULTIVITAMINAS);
		if (!ValidationSupport.isNotDefined(suplementoHierroMultivitaminas)) {
			addValidationResult(RdacaaVariableKeyCatalog.PRESCRIPCION_SUPLEMENTOS_HIERRO_MULTIVITAMINAS,
					ValidationResultCatalogConstants.CODIGO_ERROR_PRESCRIPCION_SUPLEMENTOS_HIERRO_MULTIVITAMINAS_NO_DEFINIDA);
		}

		return RuleState.BREAK;

	}
}
