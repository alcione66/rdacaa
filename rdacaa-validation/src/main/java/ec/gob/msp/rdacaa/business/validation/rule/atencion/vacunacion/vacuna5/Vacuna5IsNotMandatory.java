package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacuna5;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 5)
public class Vacuna5IsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		String vacuna5 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_APLICACION_5);
		return (!isMandatory(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_APLICACION_5)
				&& ValidationSupport.isNotDefined(vacuna5));
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}
