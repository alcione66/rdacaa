package ec.gob.msp.rdacaa.business.validation.rule.persona.pueblos;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 5)
public class PueblosIsNotMandatory  extends BaseRule<String> {
	@When
	public boolean when() {
		
		String pueblo = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_PUEBLO);
		return (!isMandatory(RdacaaVariableKeyCatalog.PERSONA_CODIGO_PUEBLO) &&
				ValidationSupport.isNotDefined(pueblo));
		
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}
