package ec.gob.msp.rdacaa.business.validation.rule.atencion.profesional;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */

@Rule(order = 3)
public class ProfesionalIsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.PROFESIONAL_CODIGO);
	}

	@Then
	public RuleState then() {

		String codProfesional = getVariableFromMap(RdacaaVariableKeyCatalog.PROFESIONAL_CODIGO);
		if (!ValidationSupport.isNotDefined(codProfesional)) {
			addValidationResult(RdacaaVariableKeyCatalog.PROFESIONAL_CODIGO,
					ValidationResultCatalogConstants.CODIGO_ERROR_PROFESIONAL_CODIGO_NO_DEFINIDA);
		}

		return RuleState.BREAK;

	}
}
