package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.perimetrocefalico;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

@Rule(order = 6)
public class PerimetroCefalicoValidateForAgeRule extends UtilsRule {

	private static final double PERIMETRO_CEFALICO_MINIMO = 20.0;
	private static final double PERIMETRO_CEFALICO_MAXIMO = 54.0;
	private static final int EDAD_CINCO_ANIO_UN_MES_CERO_DIA = 50100;
	
	
	@When
	public boolean when() {
		return true;
	}
	
	@Then
	public RuleState then() {
		String perimetroCefalico = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PERIMETRO_CEFALICO);
		Integer edadAnioMesDias = getEdadAnioMesDias();
		
		if (edadAnioMesDias == null) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PERIMETRO_CEFALICO,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PER_CEFALICO_FECHAS_FORMATO_INCORRECTO);
			// error
			return RuleState.BREAK;
		}

		if (edadAnioMesDias < EDAD_CINCO_ANIO_UN_MES_CERO_DIA) {
			if (ValidationSupport.isNotDefined(perimetroCefalico)) {
				addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PERIMETRO_CEFALICO,
						ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PER_CEFALICO_ES_MANDATORIA_MENOR_050100_ANIOS);
				// error
				return RuleState.BREAK;
			} else {
				// no error
				// validar peso
				return validarPerimetroCefalico(perimetroCefalico);
			}
		} else {
			if (ValidationSupport.isNotDefined(perimetroCefalico)) {
				// no error
				return RuleState.NEXT;
			} else {
				addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PERIMETRO_CEFALICO,
						ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PER_CEFALICO_ES_NO_DEFINIDO_MAYOR_050100_ANIOS);
				// error
				return RuleState.BREAK;
			}
		}

	}
	
	private RuleState validarPerimetroCefalico(String perimetroCefalicoString) {
		Double perimetroCefalico;
		try {
			perimetroCefalico = Double.valueOf(perimetroCefalicoString);
		} catch (Exception e) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PERIMETRO_CEFALICO,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PER_CEFALICO_NO_ES_NUMERO_VALIDO);
			return RuleState.BREAK;
		}

		if (perimetroCefalico >= PERIMETRO_CEFALICO_MINIMO && perimetroCefalico <= PERIMETRO_CEFALICO_MAXIMO) {
			return RuleState.NEXT;
		} else {
			ValidationResult error = getValidationResultFromMap(
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PER_CEFALICO_FUERA_RANGO);
			error.setMessage(error.getMessage() + " DEBE ESTAR ENTRE LOS VALORES DE " + PERIMETRO_CEFALICO_MINIMO + " A "
					+ PERIMETRO_CEFALICO_MAXIMO + " cm");
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PERIMETRO_CEFALICO, error);
			return RuleState.BREAK;
		}
	}

}
