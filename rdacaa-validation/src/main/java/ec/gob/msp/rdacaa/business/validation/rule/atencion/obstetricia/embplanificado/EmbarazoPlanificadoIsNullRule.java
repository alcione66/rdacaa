package ec.gob.msp.rdacaa.business.validation.rule.atencion.obstetricia.embplanificado;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 2)
public class EmbarazoPlanificadoIsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String embarazoPlanificado = getVariableFromMap(RdacaaVariableKeyCatalog.OBSTETRICIA_ES_EMBARAZO_PLANIFICADO);
		return ValidationSupport.isNullOrEmptyString(embarazoPlanificado);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.OBSTETRICIA_ES_EMBARAZO_PLANIFICADO,
				ValidationResultCatalogConstants.CODIGO_ERROR_OBSTETRICIA_ES_EMBARAZO_PLANIFICADO_NULO);
		return RuleState.BREAK;
	}

}
