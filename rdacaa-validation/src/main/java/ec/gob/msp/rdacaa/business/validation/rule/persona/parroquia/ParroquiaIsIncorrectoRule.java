package ec.gob.msp.rdacaa.business.validation.rule.persona.parroquia;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 6)
public class ParroquiaIsIncorrectoRule extends BaseRule<String> {
	
	@When
	public boolean when() {
		String parroquia = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_PARROQUIA);
		return !this.validationQueryService.isParroquiaValido(parroquia);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PERSONA_CODIGO_PARROQUIA,
				ValidationResultCatalogConstants.CODIGO_ERROR_CODIGO_PARROQUIA_INCORRECTO);
		return RuleState.BREAK;
	}
}
