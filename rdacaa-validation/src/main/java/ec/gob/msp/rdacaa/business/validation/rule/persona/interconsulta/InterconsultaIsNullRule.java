package ec.gob.msp.rdacaa.business.validation.rule.persona.interconsulta;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 2)
public class InterconsultaIsNullRule extends BaseRule<String>{

	
    @When
	public boolean when() {
		String interconsulta = getVariableFromMap(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_INTERCONSULTA_TIPO);
		return ValidationSupport.isNullOrEmptyString(interconsulta);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_INTERCONSULTA_TIPO,
				ValidationResultCatalogConstants.CODIGO_ERROR_CODIGO_INTERCONSULTA_NULO);
		return RuleState.BREAK;
	}
}
