package ec.gob.msp.rdacaa.business.validation.rule.atencion.grupovulnerable.gpr_vulnerable3;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 5)
public class GrupoVulnerable3IsNotMandatory extends BaseRule<String>{

	@When
	public boolean when() {
		String GrupoVulnerable3 = getVariableFromMap(RdacaaVariableKeyCatalog.GRUPO_VULNERABLE_3);
		return (!isMandatory(RdacaaVariableKeyCatalog.GRUPO_VULNERABLE_3) &&
				ValidationSupport.isNotDefined(GrupoVulnerable3));
		
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}
