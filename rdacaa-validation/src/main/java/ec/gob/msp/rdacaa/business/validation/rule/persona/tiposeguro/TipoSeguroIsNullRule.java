/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.persona.tiposeguro;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author eduardo
 */
@Rule(order = 2)
public class TipoSeguroIsNullRule extends BaseRule<String> {
    @When
	public boolean when() {
		String tipoSeguro = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEGURO);
		return ValidationSupport.isNullOrEmptyString(tipoSeguro);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEGURO,
				ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_SEGURO_NULL);
		return RuleState.BREAK;
	}
}
