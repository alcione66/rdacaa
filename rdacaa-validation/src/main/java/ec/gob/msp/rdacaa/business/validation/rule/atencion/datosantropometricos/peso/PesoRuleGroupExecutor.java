package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.peso;

import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.deliveredtechnologies.rulebook.Fact;
import com.deliveredtechnologies.rulebook.model.RuleBook;
import com.deliveredtechnologies.rulebook.util.ArrayUtils;

import ec.gob.msp.rdacaa.business.service.SignosVitalesReglasValidacion;
import ec.gob.msp.rdacaa.business.service.SignosvitalesreglasService;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.RdacaaRuleExecutor;

@Component
public class PesoRuleGroupExecutor extends RdacaaRuleExecutor  {
	
	@Autowired
    private SignosvitalesreglasService signosvitalesreglasService;
    private List<SignosVitalesReglasValidacion> reglasPeso;

	@Autowired
	public PesoRuleGroupExecutor(
			@Qualifier("ReglasDatoAntropometricoPeso") RuleBook<RdacaaRawRowResult> ruleBook) {
		super();
		this.ruleBook = ruleBook;
		this.variableKey = RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO;
	}
	
    @PostConstruct
    private void init() {
        reglasPeso = signosvitalesreglasService.findAllReglasPeso();
    }
    
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
    public Optional<RdacaaRawRowResult> execute(RdacaaRawRow input, Fact... extraFacts) {
    	Fact reglasPesoFact = new Fact("reglasPeso", reglasPeso);
    	Fact[] extra = ArrayUtils.combine(extraFacts, new Fact[] {reglasPesoFact});
		return super.execute(input, extra);
    }
}
