package ec.gob.msp.rdacaa.business.validation.rule.atencion.obstetricia.fum;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.deliveredtechnologies.rulebook.model.RuleBook;

import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.RdacaaRuleExecutor;

@Component
public class FUMRuleGroupExecutor extends RdacaaRuleExecutor {

	@Autowired
	public FUMRuleGroupExecutor(
			@Qualifier("ReglasFUM") RuleBook<RdacaaRawRowResult> ruleBook) {
		super();
		this.ruleBook = ruleBook;
		this.variableKey = RdacaaVariableKeyCatalog.OBSTETRICIA_FECHA_ULTIMA_MESTRUACION;
	}
}
