package ec.gob.msp.rdacaa.business.validation.rule.atencion.exameneslab.sifilistratamientopareja;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 5)
public class SifilisTratamientoParejaIsNotMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		String sifilisTratamientoPareja = getVariableFromMap(RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_SIFILIS_TRATAMIENTO_PAREJA);
		return (!isMandatory(RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_SIFILIS_TRATAMIENTO_PAREJA)
				&& ValidationSupport.isNotDefined(sifilisTratamientoPareja));
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}
