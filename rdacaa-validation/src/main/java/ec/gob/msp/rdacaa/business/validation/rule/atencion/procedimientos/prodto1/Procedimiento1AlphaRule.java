package ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.prodto1;

import java.util.ArrayList;
import java.util.List;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableValueResult;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 1)
public class Procedimiento1AlphaRule extends BaseRule<String> {

	@When
    public boolean when() {
        this.result = RdacaaRawRowResult.getInstance();
        List<ValidationResult> listaErrores = new ArrayList<>();
        this.result.addResultToField(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_1,
        		RdacaaVariableValueResult.getInstance(getVariableFromMap(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CODIGO_1), listaErrores));
        return true;
    }
	
	@Then
    public RuleState then() {
        return RuleState.NEXT;
    }
}
