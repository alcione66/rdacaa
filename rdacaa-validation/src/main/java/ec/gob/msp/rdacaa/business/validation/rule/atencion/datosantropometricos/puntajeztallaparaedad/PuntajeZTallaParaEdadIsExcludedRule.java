package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.puntajeztallaparaedad;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */

@Rule(order = 3)
public class PuntajeZTallaParaEdadIsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_TALLA_PARA_EDAD);
	}

	@Then
	public RuleState then() {

		String codigocie1 = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_TALLA_PARA_EDAD);
		if (!ValidationSupport.isNotDefined(codigocie1)) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_TALLA_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PUNTAJE_Z_TALLA_EDAD_NO_DEFINIDA);
		}

		return RuleState.BREAK;

	}
}
