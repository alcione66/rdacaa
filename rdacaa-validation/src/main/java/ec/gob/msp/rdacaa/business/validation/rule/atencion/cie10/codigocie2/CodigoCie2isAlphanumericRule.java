package ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.codigocie2;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class CodigoCie2isAlphanumericRule extends BaseRule<String> {
	@When
	public boolean when() {
		String codigocie2 = getVariableFromMap(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_2);
		return !StringUtils.isAlphanumeric(codigocie2);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_2,
				ValidationResultCatalogConstants.CODIGO_ERROR_COD_DIAGNOSTICO_CIE10_2_NOALFANUMERICO);
		return RuleState.BREAK;
	}
}
