package ec.gob.msp.rdacaa.business.validation.rule.atencion.fechaatencion;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 5)
public class FechaAtencionIsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		String fechaAtencion = getVariableFromMap(RdacaaVariableKeyCatalog.ATENCION_FECHA);
		return (!isMandatory(RdacaaVariableKeyCatalog.ATENCION_FECHA)
				&& ValidationSupport.isNotDefined(fechaAtencion));
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}
