package ec.gob.msp.rdacaa.business.validation.rule.persona.identificacionetnica;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 4)
public class IdentificacionEtnicaIsMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		return true;

	}

	@Then
	public RuleState then() {

		String identificacionEtnica = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_AUTOIDENTIFICACION_ETNICA);
		if (isMandatory(RdacaaVariableKeyCatalog.PERSONA_CODIGO_AUTOIDENTIFICACION_ETNICA)
				&& ValidationSupport.isNotDefined(identificacionEtnica)) {
			addValidationResult(RdacaaVariableKeyCatalog.PERSONA_CODIGO_AUTOIDENTIFICACION_ETNICA,
					ValidationResultCatalogConstants.CODIGO_ERROR_IDENTIFICACION_ETNICA_ES_MANDATORIA);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.PERSONA_CODIGO_AUTOIDENTIFICACION_ETNICA)
				&& ValidationSupport.isNotDefined(identificacionEtnica)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}
}
