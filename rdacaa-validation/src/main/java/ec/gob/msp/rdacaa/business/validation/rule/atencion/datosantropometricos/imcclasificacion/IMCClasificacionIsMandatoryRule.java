package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.imcclasificacion;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 4)
public class IMCClasificacionIsMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {

		String imcClasificacion = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_IMC_CLASIFICACION);
		if (isMandatory(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_IMC_CLASIFICACION)
				&& ValidationSupport.isNotDefined(imcClasificacion)) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_IMC_CLASIFICACION,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_IMC_CLASIFICACION_ES_MANDATORIA);
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}
	}
}
