package ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.actividad2;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 6)
public class Actividades2IsNumericaRule extends BaseRule<String> {

	@When
	public boolean when() {
		String actividades2 = getVariableFromMap(
				RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_2);
		return !StringUtils.isNumeric(actividades2);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_2,
				ValidationResultCatalogConstants.CODIGO_ERROR_ACTIVIDAD_2_NO_NUMERICO);
		return RuleState.BREAK;
	}
}
