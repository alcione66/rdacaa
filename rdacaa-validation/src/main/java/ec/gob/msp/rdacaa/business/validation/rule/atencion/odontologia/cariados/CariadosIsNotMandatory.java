package ec.gob.msp.rdacaa.business.validation.rule.atencion.odontologia.cariados;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;
@Rule(order = 5)
public class CariadosIsNotMandatory extends BaseRule<String>{

	@When
	public boolean when() {
		
		String dientesCariados = getVariableFromMap(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_CARIADAS);
		return (!isMandatory(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_CARIADAS) &&
				ValidationSupport.isNotDefined(dientesCariados));
		
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}
