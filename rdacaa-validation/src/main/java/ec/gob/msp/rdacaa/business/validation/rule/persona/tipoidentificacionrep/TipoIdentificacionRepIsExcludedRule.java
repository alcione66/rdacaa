package ec.gob.msp.rdacaa.business.validation.rule.persona.tipoidentificacionrep;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/

@Rule(order = 3)
public class TipoIdentificacionRepIsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_IDENTIFICACION_REPRESENTANTE);
	}

	@Then
	public RuleState then() {
		
		String tipoIdentificacion = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_IDENTIFICACION_REPRESENTANTE);
		if(!ValidationSupport.isNotDefined(tipoIdentificacion)){
			addValidationResult(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_IDENTIFICACION_REPRESENTANTE,
					ValidationResultCatalogConstants.CODIGO_ERROR_IDENTIFICACION_REP_NO_DEFINIDA);
		}
		
		return RuleState.BREAK;

	}
}
