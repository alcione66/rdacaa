package ec.gob.msp.rdacaa.business.validation.rule.persona.segundonombre;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.deliveredtechnologies.rulebook.model.RuleBook;

import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.RdacaaRuleExecutor;

@Component
public class SegundoNombreRuleGroupExecutor extends RdacaaRuleExecutor {

	@Autowired
	public SegundoNombreRuleGroupExecutor(@Qualifier("ReglasSegundoNombre") RuleBook<RdacaaRawRowResult> ruleBook) {
		super();
		this.ruleBook = ruleBook;
		this.variableKey = RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_NOMBRE;
	}

}
