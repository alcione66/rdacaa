/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.tipodiagnostico1;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.service.ConstantesDetalleCatalogo;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;
import org.apache.commons.lang3.StringUtils;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 7)
public class TipoDiagnostico1IsIncorrectoRule extends BaseRule<String> {
	@When
	public boolean when() {
		String codigocie1 = getVariableFromMap(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_1);
		String tipodiagnostico1 = getVariableFromMap(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_1);
		String caracter = codigocie1.trim().toUpperCase().substring(0, 1);

		if (StringUtils.isNumeric(tipodiagnostico1)) {
			if (caracter.equals("Z")
					&& Integer.valueOf(tipodiagnostico1).equals(ConstantesDetalleCatalogo.TIP_DIAG_MORBILIDAD)) {
				return true;
			} else if (!caracter.equals("Z")
					&& Integer.valueOf(tipodiagnostico1).equals(ConstantesDetalleCatalogo.TIP_DIAG_PREVENCION)) {
				return true;
			} 

			return !this.validationQueryService.isCodTipoDiagnosticoValido(Integer.valueOf(tipodiagnostico1));
		}
		return true;
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_DIAG_1,
				ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_DIAGNOSTICO_1_INCORRECTO);
		return RuleState.BREAK;
	}
}
