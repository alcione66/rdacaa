package ec.gob.msp.rdacaa.business.validation.rule.atencion.sivan.madreperlactancia;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */

@Rule(order = 3)
public class MadrePeriodoLactanciaIsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.SIVAN_ES_MADRE_PERIODO_LACTANCIA);
	}

	@Then
	public RuleState then() {

		String madrePeriodoLactancia = getVariableFromMap(RdacaaVariableKeyCatalog.SIVAN_ES_MADRE_PERIODO_LACTANCIA);
		if (!ValidationSupport.isNotDefined(madrePeriodoLactancia)) {
			addValidationResult(RdacaaVariableKeyCatalog.SIVAN_ES_MADRE_PERIODO_LACTANCIA,
					ValidationResultCatalogConstants.CODIGO_ERROR_SIVAN_ES_MADRE_PERIODO_LACTANCIA_NO_DEFINIDA);
		}

		return RuleState.BREAK;

	}
}
