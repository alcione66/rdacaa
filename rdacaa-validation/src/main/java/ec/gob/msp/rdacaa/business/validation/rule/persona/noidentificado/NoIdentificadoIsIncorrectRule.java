package ec.gob.msp.rdacaa.business.validation.rule.persona.noidentificado;

import java.text.SimpleDateFormat;
import java.util.Date;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Given;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.service.ConstantesDetalleCatalogo;
import ec.gob.msp.rdacaa.business.service.ParroquiaService;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;
import ec.gob.msp.rdacaa.utilitario.secuencial.UtilitarioSequencial;

@Rule(order = 4)
public class NoIdentificadoIsIncorrectRule extends BaseRule<String>  {
	
	@Given("parroquiaService")
	private ParroquiaService parroquiaService;
	
	@When
	public boolean when() {
		String tipoIdentificacion = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_IDENTIFICACION);
		String identificacion = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION);
		String primerNombre = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_PRIMER_NOMBRE);
		String segundoNombre = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_NOMBRE);
		String primerApellido = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_PRIMER_APELLIDO);
		String segundoApellido = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_APELLIDO);
		String fechaNacimiento = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO);
		String paisId = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD);
		String parroquiaId = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_PARROQUIA);
		Boolean valor = false;
		String numeronoidentificado = "";
		if(Integer.valueOf(tipoIdentificacion) == ConstantesDetalleCatalogo.DETALLECATALOGONOIDENTIFICADO){
			SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
			Date fcNac;
			try {
				fcNac = format.parse(fechaNacimiento);
				numeronoidentificado = UtilitarioSequencial.obtenerSequencialNoidentificado(primerNombre, segundoNombre, primerApellido, segundoApellido, fcNac , Integer.parseInt(paisId), parroquiaService.findOneByParroquiaId(Integer.valueOf(parroquiaId)).getCantonId().getProvinciaId().getCodprovincia());
				valor = identificacion.equals(numeronoidentificado) ? false : true;
			} catch (Exception e) {
				return true;
			}
		}
		return valor;
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION,
				ValidationResultCatalogConstants.CODIGO_ERROR_IDENTIFICACION_INCORRECTO);
		return RuleState.BREAK;
	}
}
