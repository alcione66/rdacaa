package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunaesquema2;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 4)
public class VacunaEsquema2IsMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		return true;

	}

	@Then
	public RuleState then() {

		String vacunaEsquema2 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_ESQUEMA_2);
		if (isMandatory(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_ESQUEMA_2)
				&& ValidationSupport.isNotDefined(vacunaEsquema2)) {
			addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_ESQUEMA_2,
					ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_ESQUEMA_2_ES_MANDATORIA);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_ESQUEMA_2)
				&& ValidationSupport.isNotDefined(vacunaEsquema2)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}

}
