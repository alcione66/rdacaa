package ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.numcertificadomedico;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 2)
public class NumeroCertificadoMedicoIsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String numerocertificadomedico = getVariableFromMap(
				RdacaaVariableKeyCatalog.DIAGNOSTICO_NUMERO_CERTIFICADO_MEDICO_UNICO);
		return ValidationSupport.isNullOrEmptyString(numerocertificadomedico);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.DIAGNOSTICO_NUMERO_CERTIFICADO_MEDICO_UNICO,
				ValidationResultCatalogConstants.CODIGO_ERROR_NUMERO_CERTIFICADO_MEDICO_NULO);
		return RuleState.BREAK;
	}

}
