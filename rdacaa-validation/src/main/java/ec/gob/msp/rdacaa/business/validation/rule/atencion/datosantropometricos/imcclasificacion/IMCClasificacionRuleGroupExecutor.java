package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.imcclasificacion;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import com.deliveredtechnologies.rulebook.Fact;
import com.deliveredtechnologies.rulebook.model.RuleBook;
import com.deliveredtechnologies.rulebook.util.ArrayUtils;

import ec.gob.msp.rdacaa.business.service.MedicionPacienteAlertasService;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.RdacaaRuleExecutor;

@Component
public class IMCClasificacionRuleGroupExecutor extends RdacaaRuleExecutor {
	
	@Autowired
	private MedicionPacienteAlertasService medicionPacienteAlertasService;

	@Autowired
	public IMCClasificacionRuleGroupExecutor(
			@Qualifier("ReglasDatoAntropometricoIMCClasificacion") RuleBook<RdacaaRawRowResult> ruleBook) {
		super();
		this.ruleBook = ruleBook;
		this.variableKey = RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_IMC_CLASIFICACION;
	}
	
	@SuppressWarnings({ "rawtypes", "unchecked" })
	@Override
	public Optional<RdacaaRawRowResult> execute(RdacaaRawRow input, Fact... extraFacts) {
		Fact medicionPaciente = new Fact("medicionPacienteAlertasService", medicionPacienteAlertasService);
		Fact[] extra = ArrayUtils.combine(extraFacts, new Fact[] {medicionPaciente});
		return super.execute(input, extra);
	}
}
