package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.perimetrocefalico;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 2)
public class PerimetroCefalicoIsNullRule extends BaseRule<String> {

	@When
	public boolean when() {
		String perimetroCefalico = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PERIMETRO_CEFALICO);
		return ValidationSupport.isNullOrEmptyString(perimetroCefalico);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PERIMETRO_CEFALICO,
				ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PER_CEFALICO_NULO);
		return RuleState.BREAK;
	}
}
