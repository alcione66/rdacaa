/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.tipoatencion4;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.service.ConstantesDetalleCatalogo;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;
import ec.gob.msp.rdacaa.utilitario.enumeracion.ComunEnum;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 7)
public class TipoAtencionDiag4IsIncorrectoRule extends BaseRule<String> {
	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {

		String tipoatenciondiagnostico1 = getVariableFromMap(
				RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_ATENCION_4);
		if (StringUtils.isNumeric(tipoatenciondiagnostico1) && !this.validationQueryService
				.isCodTipoAtencionDiagnosticoValido(Integer.valueOf(tipoatenciondiagnostico1))) {
			addValidationResult(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_ATENCION_4,
					ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_ATENCION_DIAGNOSTICO_4_INCORRECTO);
			return RuleState.BREAK;
		}

		if ((tipoatenciondiagnostico1.equals(ComunEnum.IDENTIFICADOR_ENFERMERIA.getDescripcion())
				|| tipoatenciondiagnostico1.equals(ComunEnum.IDENTIFICADOR_ENFERMERIA_RURAL.getDescripcion())
				|| tipoatenciondiagnostico1.equals(ComunEnum.IDENTIFICADOR_AUXILIAR_ENFERMERIA.getDescripcion()))
				&& !tipoatenciondiagnostico1.equals(String.valueOf(ConstantesDetalleCatalogo.TIPO_ATENCION_PRIMERA))
		) {
			addValidationResult(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_ATENCION_4,
					ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_ATENCION_DIAGNOSTICO_4_INCORRECTO_ENFERMERIA);
			return RuleState.BREAK;
		} 

		return RuleState.NEXT;
	}
}
