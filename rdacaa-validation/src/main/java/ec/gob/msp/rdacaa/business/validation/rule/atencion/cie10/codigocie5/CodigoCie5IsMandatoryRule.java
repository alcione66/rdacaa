package ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.codigocie5;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 4)
public class CodigoCie5IsMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		return true;
	}

	@Then
	public RuleState then() {

		String codigocie5 = getVariableFromMap(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_5);
		if (isMandatory(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_5)
				&& ValidationSupport.isNotDefined(codigocie5)) {
			addValidationResult(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_5,
					ValidationResultCatalogConstants.CODIGO_ERROR_COD_DIAGNOSTICO_CIE10_5_ES_MANDATORIA);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CIE_5)
				&& ValidationSupport.isNotDefined(codigocie5)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}

}
