package ec.gob.msp.rdacaa.business.validation.rule.atencion.vacunacion.vacunanrodosis5;

import org.apache.commons.lang3.StringUtils;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 6)
public class VacunaNroDosis5IsIncorrectRule extends BaseRule<String> {

	@When
	public boolean when() {
		String vacunaNroDosis5 = getVariableFromMap(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_DOSIS_5);
		return !StringUtils.isNumeric(vacunaNroDosis5);
	}

	@Then
	public RuleState then() {
		addValidationResult(RdacaaVariableKeyCatalog.VACUNAS_CODIGO_DOSIS_5,
				ValidationResultCatalogConstants.CODIGO_ERROR_VACUNAS_CODIGO_DOSIS_5_NO_ES_NUMERICO);
		return RuleState.BREAK;
	}
}
