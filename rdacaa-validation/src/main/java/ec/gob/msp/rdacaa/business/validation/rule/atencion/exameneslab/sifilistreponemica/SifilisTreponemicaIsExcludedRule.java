package ec.gob.msp.rdacaa.business.validation.rule.atencion.exameneslab.sifilistreponemica;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */

@Rule(order = 3)
public class SifilisTreponemicaIsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_SIFILIS_TREPONEMICA_RESULTADO);
	}

	@Then
	public RuleState then() {

		String sifilisTreponemica = getVariableFromMap(RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_SIFILIS_TREPONEMICA_RESULTADO);
		if (!ValidationSupport.isNotDefined(sifilisTreponemica)) {
			addValidationResult(RdacaaVariableKeyCatalog.EXAMENES_LABORATORIO_SIFILIS_TREPONEMICA_RESULTADO,
					ValidationResultCatalogConstants.CODIGO_ERROR_EXAMENES_LABORATORIO_SIFILIS_TREPONEMICA_RESULTADO_NO_DEFINIDA);
		}

		return RuleState.BREAK;

	}
}
