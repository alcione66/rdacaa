package ec.gob.msp.rdacaa.business.validation.rule.atencion.grupoprioritario.gpr_prioritario3;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 4)
public class GrupoPrioritario3IsMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		return true;

	}

	@Then
	public RuleState then() {

		String grupoprioritario3 = getVariableFromMap(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_3);
		if (isMandatory(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_3)
				&& ValidationSupport.isNotDefined(grupoprioritario3)) {
			addValidationResult(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_3,
					ValidationResultCatalogConstants.CODIGO_ERROR_GRUPO_PRIORITARIO_3_ES_MANDATORIA);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_3)
				&& ValidationSupport.isNotDefined(grupoprioritario3)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}

}
