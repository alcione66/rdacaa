package ec.gob.msp.rdacaa.business.validation.rule.persona.fechanacimiento;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 4)
public class FechaNacimientoIsMandatoryRule extends BaseRule<String> {

	@When
	public boolean when() {
		return true;

	}

	@Then
	public RuleState then() {

		String tipoIdentificacion = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO);
		if (isMandatory(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO)
				&& ValidationSupport.isNotDefined(tipoIdentificacion)) {
			addValidationResult(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO,
					ValidationResultCatalogConstants.CODIGO_ERROR_FECHANACIMIENTO_ES_MANDATORIA);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO)
				&& ValidationSupport.isNotDefined(tipoIdentificacion)) {
			return RuleState.BREAK;
		} else {
			return RuleState.NEXT;
		}

	}
}
