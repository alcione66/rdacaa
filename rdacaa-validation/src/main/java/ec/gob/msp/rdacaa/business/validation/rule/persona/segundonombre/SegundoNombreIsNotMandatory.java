package ec.gob.msp.rdacaa.business.validation.rule.persona.segundonombre;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 4)
public class SegundoNombreIsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		
		String tipoIdentificacion = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_NOMBRE);
		return (!isMandatory(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_NOMBRE) &&
				ValidationSupport.isNotDefined(tipoIdentificacion));
		
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}
