package ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.puntajezpesoparaedad;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Given;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.service.ScoreZAndCategoriaService;
import ec.gob.msp.rdacaa.business.service.ScoreZAndCategoriaService.PercentilDetalleNotFoundException;
import ec.gob.msp.rdacaa.business.service.ScoreZAndCategoriaService.PercentilNotFoundException;
import ec.gob.msp.rdacaa.business.service.SignosVitalesReglasValidacionNotFoundException;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.UtilsRule;

@Rule(order = 6)
public class PuntajeZPesoParaEdadCalculateRule extends UtilsRule {
	
	@Given("scoreZAndCategoriaService")
	private ScoreZAndCategoriaService scoreZAndCategoriaService;

	@When
	public boolean when() {
		return true;
	}
	
	@Then
	public RuleState then() {
		String puntajeZPesoParaEdad = "999990000066666";
		String peso = getVariableFromMap(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO);
		boolean pesoHasErrors = variableHasErrors(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO);
		String sexo = getSexoString();
		Integer edadAnioMesDias = getEdadAnioMesDias();
		Integer edadEnMeses = getEdadMeses();
		Integer edadEnDias = getEdadDias();
		
		boolean valuesNotDefined = ValidationSupport.isNotDefined(peso);
		
		setRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_EDAD,
				puntajeZPesoParaEdad);

		if (edadAnioMesDias == null || edadEnMeses == null || edadEnDias == null) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PUNTAJE_Z_PESO_EDAD_FECHAS_FORMATO_INCORRECTO);
		}

		if (sexo == null) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PUNTAJE_Z_PESO_EDAD_SEXO_INCORRECTO);
		}

		if (pesoHasErrors) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PUNTAJE_Z_PESO_EDAD_PESO_ERROR);
		}
		
		if(valuesNotDefined) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_PUNTAJE_Z_PESO_EDAD_PESO_NO_DEFINIDO);
		}

		if ((edadAnioMesDias == null || edadEnMeses == null || edadEnDias == null) || sexo == null
				|| pesoHasErrors || valuesNotDefined) {
			return RuleState.BREAK;
		}
		
		try {
			Double scoreZPesoParaEdad = scoreZAndCategoriaService.calcularScoreZPesoParaEdad(
					Double.parseDouble(peso), edadAnioMesDias, edadEnMeses, edadEnDias,
					sexo);
			puntajeZPesoParaEdad = scoreZPesoParaEdad.toString();
		} catch (PercentilNotFoundException | PercentilDetalleNotFoundException
				| SignosVitalesReglasValidacionNotFoundException e) {
			addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_EDAD,
					ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_PUNTAJE_Z_PESO_EDAD_NO_APLICA);
			
			return RuleState.BREAK;
		}
		
		setRdacaaRawRowResultValue(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_EDAD,
				puntajeZPesoParaEdad);
		
		addValidationResult(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_EDAD,
				ValidationResultCatalogConstants.CODIGO_INFO_DAT_ANT_PUNTAJE_Z_PESO_EDAD_VALOR);
		
		return RuleState.NEXT;
	}
}
