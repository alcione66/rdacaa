package ec.gob.msp.rdacaa.business.validation.rule.atencion.referencia.subsistema;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

@Rule(order = 3)
public class SubsistemaReferenciaIsExcludedRule extends BaseRule<String> {

	@When
	public boolean when() {
		return isExcludedFromValidation(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_SUBSISTEMA);
	}
	
	@Then
	public RuleState then() {
		
		String parroquia = getVariableFromMap(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_SUBSISTEMA);
		if(!ValidationSupport.isNotDefined(parroquia)){
			addValidationResult(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_SUBSISTEMA,
					ValidationResultCatalogConstants.CODIGO_ERROR_CODIGO_PARROQUIA_NO_DEFINIDA);
		}
		
		return RuleState.BREAK;

	}
}
