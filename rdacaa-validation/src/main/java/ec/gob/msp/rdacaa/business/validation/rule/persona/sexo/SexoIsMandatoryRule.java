package ec.gob.msp.rdacaa.business.validation.rule.persona.sexo;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
*
* @author dmurillo
*/
@Rule(order = 4)
public class SexoIsMandatoryRule extends BaseRule<String> {
	
	@When
	public boolean when() {
		return true;

	}

	@Then
	public RuleState then() {

		String tipoIdentificacion = getVariableFromMap(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO);
		if (isMandatory(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO)
				&& ValidationSupport.isNotDefined(tipoIdentificacion)) {
			addValidationResult(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO,
					ValidationResultCatalogConstants.CODIGO_ERROR_SEXO_ES_MANDATORIO);
			return RuleState.BREAK;
		} else if (!isMandatory(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO)
				&& ValidationSupport.isNotDefined(tipoIdentificacion)) {
			return RuleState.NEXT;
		} else {
			return RuleState.NEXT;
		}

	}
}
