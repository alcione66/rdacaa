package ec.gob.msp.rdacaa.business.validation.rule.atencion.grupoprioritario.tipoatencion;

import com.deliveredtechnologies.rulebook.RuleState;
import com.deliveredtechnologies.rulebook.annotation.Rule;
import com.deliveredtechnologies.rulebook.annotation.Then;
import com.deliveredtechnologies.rulebook.annotation.When;

import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.rule.BaseRule;

/**
 *
 * @author miguel.faubla
 */
@Rule(order = 5)
public class TipoAtencionIsNotMandatory extends BaseRule<String> {

	@When
	public boolean when() {
		String tipoatencion = getVariableFromMap(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_TIPO_ATENCION);
		return (!isMandatory(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_TIPO_ATENCION)
				&& ValidationSupport.isNotDefined(tipoatencion));
	}

	@Then
	public RuleState then() {
		return RuleState.BREAK;
	}
}
