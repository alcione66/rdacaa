/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.service.ConstantesDetalleCatalogo;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.grupoprioritario.estrategiamsp.EstrategiaMspRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

/**
 *
 * @author miguel.faubla
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class EstrategiaMspRuleGroupExecutorTest {
	@Autowired
	private EstrategiaMspRuleGroupExecutor estrategiaMspRuleGroupExecutor;

	@Test
	public void whenEstrategiaMspIsExcludedAndValorIsNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem estrategiamspId = new RowItem();
		estrategiamspId.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
		estrategiamspId.setExcludedFromValidation(true);
		input.addField(RdacaaVariableKeyCatalog.ESTRATEGIA_MSP, estrategiamspId);
		Optional<RdacaaRawRowResult> sa = estrategiaMspRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Tipo de Estrategia debe ser nulo",
				sa.get().get(RdacaaVariableKeyCatalog.ESTRATEGIA_MSP).getValidationResultList().isEmpty());
	}

	@Test
	public void whenEstrategiaMspIsExcludedAndValorIsNotNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem estrategiamspId = new RowItem();
		estrategiamspId.setItemValue("167");
		estrategiamspId.setExcludedFromValidation(true);
		input.addField(RdacaaVariableKeyCatalog.ESTRATEGIA_MSP, estrategiamspId);
		Optional<RdacaaRawRowResult> sa = estrategiaMspRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Tipo de Estrategia no debe ser nulo", sa.get()
				.get(RdacaaVariableKeyCatalog.ESTRATEGIA_MSP)
				.hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_ESTRATEGIA_MSP_NO_DEFINIDA)
				.isPresent());
	}

	@Test
	public void whenEstrategiaMspIsNotExcludedAndValorIsNotNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem estrategiamspId = new RowItem();
		estrategiamspId.setItemValue("1645");
		estrategiamspId.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ESTRATEGIA_MSP, estrategiamspId);
		Optional<RdacaaRawRowResult> sa = estrategiaMspRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Tipo de Estrategia no debe ser nulo",
				sa.get().get(RdacaaVariableKeyCatalog.ESTRATEGIA_MSP).getValidationResultList().isEmpty());
	}

	@Test
	public void whenEstrategiaMspIsNull_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem estrategiamspId = new RowItem();
		estrategiamspId.setItemValue(null);
		estrategiamspId.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ESTRATEGIA_MSP, estrategiamspId);
		Optional<RdacaaRawRowResult> sa = estrategiaMspRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El campo es obligatorio",
				sa.get().get(RdacaaVariableKeyCatalog.ESTRATEGIA_MSP)
						.hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_ESTRATEGIA_MSP_NULO)
						.isPresent());
	}

	@Test
	public void whenEstrategiaMspIsNotNumeric_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem estrategiamspId = new RowItem();
		estrategiamspId.setItemValue("MEDICOBARRIO");
		estrategiamspId.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ESTRATEGIA_MSP, estrategiamspId);
		Optional<RdacaaRawRowResult> sa = estrategiaMspRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Se espera un error para caracteres no numéricos ", sa.get()
				.get(RdacaaVariableKeyCatalog.ESTRATEGIA_MSP)
				.hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_ESTRATEGIA_MSP_NONUMERICO)
				.isPresent());
	}

	@Test
	public void whenEstrategiaMspIsIncorrecto_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem estrategiamspId = new RowItem();
		estrategiamspId.setItemValue("28");
		estrategiamspId.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ESTRATEGIA_MSP, estrategiamspId);
		Optional<RdacaaRawRowResult> sa = estrategiaMspRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El campo es incorrecto", sa.get().get(RdacaaVariableKeyCatalog.ESTRATEGIA_MSP)
				.hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_ESTRATEGIA_MSP_INCORRECTO)
				.isPresent());
	}

	@Test
	public void whenEstrategiaMspIsCorrect_thenOk() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem estrategiamspId = new RowItem();
		estrategiamspId.setItemValue("1645");
		estrategiamspId.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ESTRATEGIA_MSP, estrategiamspId);
		Optional<RdacaaRawRowResult> sa = estrategiaMspRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("No se esperan errores para Tipo de Estrategia correcto",
				sa.get().get(RdacaaVariableKeyCatalog.ESTRATEGIA_MSP).getValidationResultList().isEmpty());
	}

	@Test
	public void whenEstrategiaMspIsMandatory() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem estrategiamspId = new RowItem();
		estrategiamspId.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
		estrategiamspId.setExcludedFromValidation(false);
		estrategiamspId.setMandatory(true);
		input.addField(RdacaaVariableKeyCatalog.ESTRATEGIA_MSP, estrategiamspId);
		Optional<RdacaaRawRowResult> sa = estrategiaMspRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Tipo de Estrategia es mandatorio",
				sa.get().get(RdacaaVariableKeyCatalog.ESTRATEGIA_MSP)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_ESTRATEGIA_MSP_ES_MANDATORIA)
						.isPresent());
	}

	@Test
	public void whenEstrategiaMspIsMadatoryAndCorrectValue() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem estrategiamspId = new RowItem();
		estrategiamspId.setItemValue("1645");
		estrategiamspId.setExcludedFromValidation(false);
		estrategiamspId.setMandatory(true);
		input.addField(RdacaaVariableKeyCatalog.ESTRATEGIA_MSP, estrategiamspId);
		Optional<RdacaaRawRowResult> sa = estrategiaMspRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Tipo de Estrategia es mandatorio", sa.get()
				.get(RdacaaVariableKeyCatalog.ESTRATEGIA_MSP).getValidationResultList().isEmpty());
	}

}
