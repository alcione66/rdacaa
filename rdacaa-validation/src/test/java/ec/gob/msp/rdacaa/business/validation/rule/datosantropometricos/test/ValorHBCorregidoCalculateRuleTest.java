package ec.gob.msp.rdacaa.business.validation.rule.datosantropometricos.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.deliveredtechnologies.rulebook.Fact;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.valorhb.ValorHBRuleGroupExecutor;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.valorhbcorregido.ValorHBCorregidoRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class ValorHBCorregidoCalculateRuleTest {

	@Autowired
	private ValorHBCorregidoRuleGroupExecutor valorHBCorregidoRuleGroupExecutor;

	@Autowired
	private ValorHBRuleGroupExecutor valorHBRuleGroupExecutor;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void whenAlturaYValorHBHasErrors_thenError() {
		RdacaaRawRowResult resultTotal = new RdacaaRawRowResult();
		RdacaaRawRow input = new RdacaaRawRow();

		// Establecimiento Salud
		RowItem estSalud = new RowItem();
		estSalud.setItemValue("asasa");
		estSalud.setExcludedFromValidation(false);
		estSalud.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.ESTABLECIMENTO_SALUD_CODIGO, estSalud);

		// Valor HB
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("XXXX-03-01");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-XX");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem valorHB = new RowItem();
		valorHB.setItemValue("20");
		valorHB.setExcludedFromValidation(false);
		valorHB.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB, valorHB);

		Optional<RdacaaRawRowResult> sa1 = valorHBRuleGroupExecutor.execute(input);
		
		sa1.ifPresent(r -> {
			System.out.println(r.toString());
		});

		resultTotal.putAll(sa1.get());

		Fact resultTotalFact1 = new Fact("rdacaa_row_result", resultTotal);

		RowItem valorHBCorregido = new RowItem();
		valorHBCorregido.setItemValue("20");
		valorHBCorregido.setExcludedFromValidation(false);
		valorHBCorregido.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB_CORREGIDO, valorHBCorregido);

		Optional<RdacaaRawRowResult> sa2 = valorHBCorregidoRuleGroupExecutor.execute(input, resultTotalFact1);

		sa2.ifPresent(r -> {
			System.out.println(r.toString());
		});

		Assert.assertTrue(
				"Se espera DATO_ANTROPOMETRICO_VALOR_HB_CORREGIDO , "
						+ "CODIGO_ERROR_DAT_ANT_VALOR_HB_CORREGIDO_EST_SALUD_ERROR, ",
				sa2.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB_CORREGIDO)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_VALOR_HB_CORREGIDO_VALORHB_ERROR)
						.isPresent()
				&& sa2.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB_CORREGIDO)
								.hasValidationResultByCode(
										ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_VALOR_HB_CORREGIDO_EST_SALUD_ERROR)
								.isPresent());
	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void whenAlturaYValorHBOK_thenNoError() {
		RdacaaRawRowResult resultTotal = new RdacaaRawRowResult();
		RdacaaRawRow input = new RdacaaRawRow();

		// Establecimiento Salud
		RowItem estSalud = new RowItem();
		estSalud.setItemValue("1746");
		estSalud.setExcludedFromValidation(false);
		estSalud.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.ESTABLECIMENTO_SALUD_CODIGO, estSalud);

		// Valor HB
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2018-03-01");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-01");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem valorHB = new RowItem();
		valorHB.setItemValue("20");
		valorHB.setExcludedFromValidation(false);
		valorHB.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB, valorHB);

		Optional<RdacaaRawRowResult> sa1 = valorHBRuleGroupExecutor.execute(input);
		
		sa1.ifPresent(r -> {
			System.out.println(r.toString());
		});

		resultTotal.putAll(sa1.get());

		Fact resultTotalFact1 = new Fact("rdacaa_row_result", resultTotal);

		RowItem valorHBCorregido = new RowItem();
		valorHBCorregido.setItemValue("999990000066666");
		valorHBCorregido.setExcludedFromValidation(false);
		valorHBCorregido.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB_CORREGIDO, valorHBCorregido);

		Optional<RdacaaRawRowResult> sa2 = valorHBCorregidoRuleGroupExecutor.execute(input, resultTotalFact1);

		sa2.ifPresent(r -> {
			System.out.println(r.toString());
		});

		Assert.assertTrue(
				"No se esperan errores ",
				!sa2.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB_CORREGIDO).hasErrors()
				&& sa2.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB_CORREGIDO).getValue().equals("18.7"));
	}
}
