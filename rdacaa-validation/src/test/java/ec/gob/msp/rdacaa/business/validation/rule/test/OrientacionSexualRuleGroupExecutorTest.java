package ec.gob.msp.rdacaa.business.validation.rule.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.persona.orientacionsexual.OrientacionSexualRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

/**
*
* @author dmurillo
*/
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class OrientacionSexualRuleGroupExecutorTest {

	@Autowired
	private OrientacionSexualRuleGroupExecutor orientacionSexualRuleGroupExecutor;
	
	@Test
    public void whenOrientacionSexualIsExcludedAndValorIsNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("999990000066666");
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_ORIENTACION_SEXUAL, ritem);
        Optional<RdacaaRawRowResult> sa = orientacionSexualRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("OrientacionSexual debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_ORIENTACION_SEXUAL).getValidationResultList().isEmpty());
    }
    
    @Test
    public void whenOrientacionSexualIsNotExcludedAndValorIsNotNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("17");
    	ritem.setExcludedFromValidation(false);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_ORIENTACION_SEXUAL, ritem);
        Optional<RdacaaRawRowResult> sa = orientacionSexualRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("OrientacionSexual debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_ORIENTACION_SEXUAL).getValidationResultList().isEmpty());
    }
	
	@Test
    public void whenOrientacionSexualIsNull_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("");
        ritem.setExcludedFromValidation(false);
        RowItem ritemFN = new RowItem();
        ritemFN.setItemValue("2008-09-01");
        ritemFN.setExcludedFromValidation(false);
        RowItem ritemFA = new RowItem();
        ritemFA.setItemValue("2018-09-14");
        ritemFA.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_ORIENTACION_SEXUAL, ritem);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, ritemFN);
        input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, ritemFA);
        Optional<RdacaaRawRowResult> sa = orientacionSexualRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("El campo OrientacionSexual es obligatorio",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_ORIENTACION_SEXUAL)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_ORIENTACIONSEXUAL_NULO)
                        .isPresent());
    }
	
	@Test
    public void whenOrientacionSexualIsNotNumeric_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("REDACAA");
        ritem.setExcludedFromValidation(false);
        RowItem ritemFN = new RowItem();
        ritemFN.setItemValue("2008-09-01");
        ritemFN.setExcludedFromValidation(false);
        RowItem ritemFA = new RowItem();
        ritemFA.setItemValue("2018-09-14");
        ritemFA.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_ORIENTACION_SEXUAL, ritem);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, ritemFN);
        input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, ritemFA);
        Optional<RdacaaRawRowResult> sa = orientacionSexualRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Se espera un error para caracteres no numericos ",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_ORIENTACION_SEXUAL)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_ORIENTACIONSEXUAL_NONUMERICA)
                        .isPresent());
    }
	
	@Test
    public void whenOrientacionSexualIsIncorrecto_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritemCS = new RowItem();
        ritemCS.setItemValue("16");
        ritemCS.setExcludedFromValidation(false);
        RowItem ritem = new RowItem();
        ritem.setItemValue("671");
        ritem.setExcludedFromValidation(false);
        RowItem ritemFN = new RowItem();
        ritemFN.setItemValue("1979-11-05");
        ritemFN.setExcludedFromValidation(false);
        RowItem ritemFA = new RowItem();
        ritemFA.setItemValue("2018-12-10");
        ritemFA.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, ritemCS);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_ORIENTACION_SEXUAL, ritem);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, ritemFN);
        input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, ritemFA);
        Optional<RdacaaRawRowResult> sa = orientacionSexualRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("El campo es incorrecto",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_ORIENTACION_SEXUAL)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_ORIENTACIONSEXUAL_INCORRECTO)
                        .isPresent());
    }
	
	@Test
    public void whenOrientacionSexualIsCorrect_thenOk() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritemCS = new RowItem();
        ritemCS.setItemValue("17");
        ritemCS.setExcludedFromValidation(false);
        RowItem ritem = new RowItem();
        ritem.setItemValue("671");
        ritem.setExcludedFromValidation(false);
        RowItem ritemFN = new RowItem();
        ritemFN.setItemValue("1989-09-01");
        ritemFN.setExcludedFromValidation(false);
        RowItem ritemFA = new RowItem();
        ritemFA.setItemValue("2018-09-14");
        ritemFA.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, ritemCS);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_ORIENTACION_SEXUAL, ritem);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, ritemFN);
        input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, ritemFA);
        Optional<RdacaaRawRowResult> sa = orientacionSexualRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("No se espera error en este test",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_ORIENTACION_SEXUAL).getValidationResultList().isEmpty());
    }
	
}
