package ec.gob.msp.rdacaa.business.validation.rule.datosantropometricos.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.deliveredtechnologies.rulebook.Fact;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.categoriapesoparaedad.CategoriaPesoParaEdadRuleGroupExecutor;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.peso.PesoRuleGroupExecutor;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.puntajezpesoparaedad.PuntajeZPesoParaEdadRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class PesoValidateForAgeRuleTest {

	@Autowired
	private PesoRuleGroupExecutor pesoRuleGroupExecutor;
	
	@Autowired
	private PuntajeZPesoParaEdadRuleGroupExecutor puntajeZPesoParaEdadRuleGroupExecutor;
	
	@Autowired
	private CategoriaPesoParaEdadRuleGroupExecutor categoriaPesoParaEdadRuleGroupExecutor;

	@Test
	public void whenEdadMenor050000AndIsNotDefined_thenError() {
		RdacaaRawRowResult resultTotal = new RdacaaRawRowResult();
		
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem sexo = new RowItem();
		sexo.setItemValue("16");// hombre
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2018-05-06");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2019-11-23");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem peso = new RowItem();
		peso.setItemValue("999990000066666");
		peso.setExcludedFromValidation(false);
		peso.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO, peso);

		Optional<RdacaaRawRowResult> sa1 = pesoRuleGroupExecutor.execute(input);
		sa1.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa1.get());

		Fact resultTotalFact1 = new Fact("rdacaa_row_result", resultTotal);
		
		//PZ Peso para edad
		RowItem pzPesoEdad = new RowItem();
		pzPesoEdad.setItemValue("999990000066666");
		pzPesoEdad.setExcludedFromValidation(false);
		pzPesoEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_EDAD, pzPesoEdad);
		
		Optional<RdacaaRawRowResult> sa2 = puntajeZPesoParaEdadRuleGroupExecutor.execute(input, resultTotalFact1);

		sa2.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa2.get());

		Fact resultTotalFact2 = new Fact("rdacaa_row_result", resultTotal);
		
		//Cat PZ Peso para edad
		
		RowItem catPzPesoEdad = new RowItem();
		catPzPesoEdad.setItemValue("999990000066666");
		catPzPesoEdad.setExcludedFromValidation(false);
		catPzPesoEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PESO_PARA_EDAD, catPzPesoEdad);
		
		Optional<RdacaaRawRowResult> sa3 = categoriaPesoParaEdadRuleGroupExecutor.execute(input, resultTotalFact2);

		sa3.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		Assert.assertTrue("El peso es obligatorio para edad menor de 5 años",
				sa1.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO).hasValidationResultByCode(
						ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PESO_ES_MANDATORIA_MENOR_050100_ANIOS_O_EMBARAZADA)
						.isPresent());
	}

	@Test
	public void whenEdadMenor050000AndDateWithError_thenError() {
		RdacaaRawRowResult resultTotal = new RdacaaRawRowResult();
		
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem sexo = new RowItem();
		sexo.setItemValue("16");// hombre
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2018-05-XX");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("XXXX-11-23");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem peso = new RowItem();
		peso.setItemValue("999990000066666");
		peso.setExcludedFromValidation(false);
		peso.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO, peso);

		Optional<RdacaaRawRowResult> sa1 = pesoRuleGroupExecutor.execute(input);
		sa1.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa1.get());

		Fact resultTotalFact1 = new Fact("rdacaa_row_result", resultTotal);
		
		//PZ Peso para edad
		RowItem pzPesoEdad = new RowItem();
		pzPesoEdad.setItemValue("999990000066666");
		pzPesoEdad.setExcludedFromValidation(false);
		pzPesoEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_EDAD, pzPesoEdad);
		
		Optional<RdacaaRawRowResult> sa2 = puntajeZPesoParaEdadRuleGroupExecutor.execute(input, resultTotalFact1);

		sa2.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa2.get());

		Fact resultTotalFact2 = new Fact("rdacaa_row_result", resultTotal);
		
		//Cat PZ Peso para edad
		
		RowItem catPzPesoEdad = new RowItem();
		catPzPesoEdad.setItemValue("999990000066666");
		catPzPesoEdad.setExcludedFromValidation(false);
		catPzPesoEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PESO_PARA_EDAD, catPzPesoEdad);
		
		Optional<RdacaaRawRowResult> sa3 = categoriaPesoParaEdadRuleGroupExecutor.execute(input, resultTotalFact2);

		sa3.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		Assert.assertTrue("Se espera error para fechas incorrectas",
				sa1.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PESO_FECHAS_FORMATO_INCORRECTO)
						.isPresent());
	}

	@Test
	public void whenEdadMenor050000AndSexoWithError_thenError() {
		RdacaaRawRowResult resultTotal = new RdacaaRawRowResult();
		
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem sexo = new RowItem();
		sexo.setItemValue("31");// hombre
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2018-05-06");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-23");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem peso = new RowItem();
		peso.setItemValue("999990000066666");
		peso.setExcludedFromValidation(false);
		peso.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO, peso);

		Optional<RdacaaRawRowResult> sa1 = pesoRuleGroupExecutor.execute(input);
		sa1.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa1.get());

		Fact resultTotalFact1 = new Fact("rdacaa_row_result", resultTotal);
		
		//PZ Peso para edad
		RowItem pzPesoEdad = new RowItem();
		pzPesoEdad.setItemValue("999990000066666");
		pzPesoEdad.setExcludedFromValidation(false);
		pzPesoEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_EDAD, pzPesoEdad);
		
		Optional<RdacaaRawRowResult> sa2 = puntajeZPesoParaEdadRuleGroupExecutor.execute(input, resultTotalFact1);

		sa2.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa2.get());

		Fact resultTotalFact2 = new Fact("rdacaa_row_result", resultTotal);
		
		//Cat PZ Peso para edad
		
		RowItem catPzPesoEdad = new RowItem();
		catPzPesoEdad.setItemValue("999990000066666");
		catPzPesoEdad.setExcludedFromValidation(false);
		catPzPesoEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PESO_PARA_EDAD, catPzPesoEdad);
		
		Optional<RdacaaRawRowResult> sa3 = categoriaPesoParaEdadRuleGroupExecutor.execute(input, resultTotalFact2);

		sa3.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		Assert.assertTrue("Se espera error para fechas incorrectas",
				sa1.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PESO_FECHAS_SEXO_INCORRECTO)
						.isPresent());
	}

	@Test
	public void whenIsMujerEmbarazada_thenError() {
		RdacaaRawRowResult resultTotal = new RdacaaRawRowResult();
		
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem sexo = new RowItem();
		sexo.setItemValue("17");// mujer
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		RowItem grupoPrioritarioEmbarazada = new RowItem();
		grupoPrioritarioEmbarazada.setItemValue("634");
		grupoPrioritarioEmbarazada.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1, grupoPrioritarioEmbarazada);
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2000-05-06");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-23");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem peso = new RowItem();
		peso.setItemValue("999990000066666");
		peso.setExcludedFromValidation(false);
		peso.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO, peso);

		Optional<RdacaaRawRowResult> sa1 = pesoRuleGroupExecutor.execute(input);
		sa1.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa1.get());

		Fact resultTotalFact1 = new Fact("rdacaa_row_result", resultTotal);
		
		//PZ Peso para edad
		RowItem pzPesoEdad = new RowItem();
		pzPesoEdad.setItemValue("999990000066666");
		pzPesoEdad.setExcludedFromValidation(false);
		pzPesoEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_EDAD, pzPesoEdad);
		
		Optional<RdacaaRawRowResult> sa2 = puntajeZPesoParaEdadRuleGroupExecutor.execute(input, resultTotalFact1);

		sa2.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa2.get());

		Fact resultTotalFact2 = new Fact("rdacaa_row_result", resultTotal);
		
		//Cat PZ Peso para edad
		
		RowItem catPzPesoEdad = new RowItem();
		catPzPesoEdad.setItemValue("999990000066666");
		catPzPesoEdad.setExcludedFromValidation(false);
		catPzPesoEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PESO_PARA_EDAD, catPzPesoEdad);
		
		Optional<RdacaaRawRowResult> sa3 = categoriaPesoParaEdadRuleGroupExecutor.execute(input, resultTotalFact2);

		sa3.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		Assert.assertTrue("El peso es obligatorio para mujeres embarazadas",
				sa1.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO).hasValidationResultByCode(
						ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PESO_ES_MANDATORIA_MENOR_050100_ANIOS_O_EMBARAZADA)
						.isPresent());
	}

	@Test
	public void whenIsMujerEmbarazadaValorCorrecto_thenNoError() {
		RdacaaRawRowResult resultTotal = new RdacaaRawRowResult();
		
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem sexo = new RowItem();
		sexo.setItemValue("17");// mujer
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		RowItem grupoPrioritarioEmbarazada = new RowItem();
		grupoPrioritarioEmbarazada.setItemValue("634");
		grupoPrioritarioEmbarazada.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1, grupoPrioritarioEmbarazada);
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2000-05-06");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-23");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem peso = new RowItem();
		peso.setItemValue("60");
		peso.setExcludedFromValidation(false);
		peso.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO, peso);

		Optional<RdacaaRawRowResult> sa1 = pesoRuleGroupExecutor.execute(input);
		sa1.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa1.get());

		Fact resultTotalFact1 = new Fact("rdacaa_row_result", resultTotal);
		
		//PZ Peso para edad
		RowItem pzPesoEdad = new RowItem();
		pzPesoEdad.setItemValue("999990000066666");
		pzPesoEdad.setExcludedFromValidation(false);
		pzPesoEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_EDAD, pzPesoEdad);
		
		Optional<RdacaaRawRowResult> sa2 = puntajeZPesoParaEdadRuleGroupExecutor.execute(input, resultTotalFact1);

		sa2.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa2.get());

		Fact resultTotalFact2 = new Fact("rdacaa_row_result", resultTotal);
		
		//Cat PZ Peso para edad
		
		RowItem catPzPesoEdad = new RowItem();
		catPzPesoEdad.setItemValue("999990000066666");
		catPzPesoEdad.setExcludedFromValidation(false);
		catPzPesoEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PESO_PARA_EDAD, catPzPesoEdad);
		
		Optional<RdacaaRawRowResult> sa3 = categoriaPesoParaEdadRuleGroupExecutor.execute(input, resultTotalFact2);

		sa3.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		Assert.assertTrue("El valor de prueba es correcto o debe existir errores",
				!sa1.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO).hasErrors());
	}

	@Test
	public void whenIsMujerEmbarazadaValorNoNumerico_thenError() {
		RdacaaRawRowResult resultTotal = new RdacaaRawRowResult();
		
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem sexo = new RowItem();
		sexo.setItemValue("17");// mujer
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		RowItem grupoPrioritarioEmbarazada = new RowItem();
		grupoPrioritarioEmbarazada.setItemValue("634");
		grupoPrioritarioEmbarazada.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1, grupoPrioritarioEmbarazada);
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2000-05-06");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-23");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem peso = new RowItem();
		peso.setItemValue("as");
		peso.setExcludedFromValidation(false);
		peso.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO, peso);

		Optional<RdacaaRawRowResult> sa1 = pesoRuleGroupExecutor.execute(input);
		sa1.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa1.get());

		Fact resultTotalFact1 = new Fact("rdacaa_row_result", resultTotal);
		
		//PZ Peso para edad
		RowItem pzPesoEdad = new RowItem();
		pzPesoEdad.setItemValue("999990000066666");
		pzPesoEdad.setExcludedFromValidation(false);
		pzPesoEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_EDAD, pzPesoEdad);
		
		Optional<RdacaaRawRowResult> sa2 = puntajeZPesoParaEdadRuleGroupExecutor.execute(input, resultTotalFact1);

		sa2.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa2.get());

		Fact resultTotalFact2 = new Fact("rdacaa_row_result", resultTotal);
		
		//Cat PZ Peso para edad
		
		RowItem catPzPesoEdad = new RowItem();
		catPzPesoEdad.setItemValue("999990000066666");
		catPzPesoEdad.setExcludedFromValidation(false);
		catPzPesoEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PESO_PARA_EDAD, catPzPesoEdad);
		
		Optional<RdacaaRawRowResult> sa3 = categoriaPesoParaEdadRuleGroupExecutor.execute(input, resultTotalFact2);

		sa3.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		Assert.assertTrue("El valor de prueba es correcto o debe existir errores",
				sa1.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO).hasValidationResultByCode(
						ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PESO_NO_ES_NUMERO_VALIDO).isPresent());
	}

	@Test
	public void whenEdadMenor050000ValorCorrecto_thenWarning() {
		RdacaaRawRowResult resultTotal = new RdacaaRawRowResult();
		
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem sexo = new RowItem();
		sexo.setItemValue("17");// mujer
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2015-05-06");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-23");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem peso = new RowItem();
		peso.setItemValue("80");
		peso.setExcludedFromValidation(false);
		peso.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO, peso);

		Optional<RdacaaRawRowResult> sa1 = pesoRuleGroupExecutor.execute(input);
		sa1.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa1.get());

		Fact resultTotalFact1 = new Fact("rdacaa_row_result", resultTotal);
		
		//PZ Peso para edad
		RowItem pzPesoEdad = new RowItem();
		pzPesoEdad.setItemValue("999990000066666");
		pzPesoEdad.setExcludedFromValidation(false);
		pzPesoEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_EDAD, pzPesoEdad);
		
		Optional<RdacaaRawRowResult> sa2 = puntajeZPesoParaEdadRuleGroupExecutor.execute(input, resultTotalFact1);

		sa2.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa2.get());

		Fact resultTotalFact2 = new Fact("rdacaa_row_result", resultTotal);
		
		//Cat PZ Peso para edad
		
		RowItem catPzPesoEdad = new RowItem();
		catPzPesoEdad.setItemValue("999990000066666");
		catPzPesoEdad.setExcludedFromValidation(false);
		catPzPesoEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PESO_PARA_EDAD, catPzPesoEdad);
		
		Optional<RdacaaRawRowResult> sa3 = categoriaPesoParaEdadRuleGroupExecutor.execute(input, resultTotalFact2);

		sa3.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		Assert.assertTrue(
				"Para los parametros de la prueba debe exitir la ADV CODIGO_WARN_DAT_ANT_PESO_FUERA_RANGO_PARA_EDAD",
				sa1.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ADV_DAT_ANT_PESO_FUERA_RANGO_PARA_EDAD)
						.isPresent() && !sa1.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO).hasErrors());
	}

	@Test
	public void whenEdadMenor050000ValorFueraRango_thenError() {
		RdacaaRawRowResult resultTotal = new RdacaaRawRowResult();
		
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem sexo = new RowItem();
		sexo.setItemValue("17");// mujer
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2015-05-06");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-23");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem peso = new RowItem();
		peso.setItemValue("600");
		peso.setExcludedFromValidation(false);
		peso.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO, peso);

		Optional<RdacaaRawRowResult> sa1 = pesoRuleGroupExecutor.execute(input);
		sa1.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa1.get());

		Fact resultTotalFact1 = new Fact("rdacaa_row_result", resultTotal);
		
		//PZ Peso para edad
		RowItem pzPesoEdad = new RowItem();
		pzPesoEdad.setItemValue("999990000066666");
		pzPesoEdad.setExcludedFromValidation(false);
		pzPesoEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_PESO_PARA_EDAD, pzPesoEdad);
		
		Optional<RdacaaRawRowResult> sa2 = puntajeZPesoParaEdadRuleGroupExecutor.execute(input, resultTotalFact1);

		sa2.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa2.get());

		Fact resultTotalFact2 = new Fact("rdacaa_row_result", resultTotal);
		
		//Cat PZ Peso para edad
		
		RowItem catPzPesoEdad = new RowItem();
		catPzPesoEdad.setItemValue("999990000066666");
		catPzPesoEdad.setExcludedFromValidation(false);
		catPzPesoEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_PESO_PARA_EDAD, catPzPesoEdad);
		
		Optional<RdacaaRawRowResult> sa3 = categoriaPesoParaEdadRuleGroupExecutor.execute(input, resultTotalFact2);

		sa3.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		Assert.assertTrue("Para los parametros de la prueba debe exitir CODIGO_ERROR_DAT_ANT_PESO_FUERA_RANGO", sa1.get()
				.get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO)
				.hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_PESO_FUERA_RANGO)
				.isPresent());
	}
}
