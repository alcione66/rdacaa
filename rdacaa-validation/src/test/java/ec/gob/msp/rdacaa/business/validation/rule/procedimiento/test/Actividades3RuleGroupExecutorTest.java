package ec.gob.msp.rdacaa.business.validation.rule.procedimiento.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.service.ConstantesDetalleCatalogo;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.actividad1.Actividades1RuleGroupExecutor;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.actividad3.Actividades3RuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class Actividades3RuleGroupExecutorTest {
	@Autowired
	private Actividades3RuleGroupExecutor actividades3RuleGroupExecutor;
	
	@Test
	public void whenActividades3IsNull_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue(null);
		ritem.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_3, ritem);
		Optional<RdacaaRawRowResult> sa = actividades3RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El campo es obligatorio",
				sa.get().get(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_3)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_ACTIVIDAD_3_NULO)
						.isPresent());
	}
	
	@Test
	public void whenActividades3IsExcludedAndValorIsNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
		ritem.setExcludedFromValidation(true);
		input.addField(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_3, ritem);
		Optional<RdacaaRawRowResult> sa = actividades3RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El numero de actividades debe ser nulo",
				sa.get().get(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_3)
						.getValidationResultList().isEmpty());
	}
	
	@Test
	public void whenActividades3IsMandatory() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
		ritem.setExcludedFromValidation(false);
		ritem.setMandatory(true);
		input.addField(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_3, ritem);
		Optional<RdacaaRawRowResult> sa = actividades3RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Cantidad de actividades es mandatorio",
				sa.get().get(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_3)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_ACTIVIDAD_3_ES_MANDATORIO)
						.isPresent());
	}
	
	@Test
	public void whenActividades3IsNotNumeric_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue("asdfasdfasd");
		ritem.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_3, ritem);
		Optional<RdacaaRawRowResult> sa = actividades3RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Se espera un error para caracteres no numéricos ",
				sa.get().get(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_3)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_ACTIVIDAD_3_NO_NUMERICO)
						.isPresent());
	}
	
	@Test
	public void whenActividades3LongitudIncorrect_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue("9999");
		ritem.setExcludedFromValidation(false);

		input.addField(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_3, ritem);
		Optional<RdacaaRawRowResult> sa = actividades3RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Se espera un error longitud incorrecta", sa.get()
				.get(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_3)
				.hasValidationResultByCode(
						ValidationResultCatalogConstants.CODIGO_ERROR_ACTIVIDAD_3_LONGITUD_INCORRECTA)
				.isPresent());
	}
}
