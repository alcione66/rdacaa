package ec.gob.msp.rdacaa.business.validation.rule.odontologia.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.odontologia.extraido.ExtraidosRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class DientesExtraidosRuleGroupExecutorTest {
	@Autowired
    private ExtraidosRuleGroupExecutor extraidosRuleGroupExecutor;
	
    @Test
	public void whenNumeroDientesCariadosIsNull_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue(null);
		ritem.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_EXTRAIDAS, ritem);
		Optional<RdacaaRawRowResult> sa = extraidosRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El campo es obligatorio",
				sa.get().get(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_EXTRAIDAS)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_DIENTES_EXTRAIDOS_NULO)
						.isPresent());
	}
    
    @Test
	public void whenCodigoDientesExtraidosIsMandatoryAndCorrectValue() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem itemFechaNac = new RowItem();
		itemFechaNac.setItemValue("2008-10-11");
		itemFechaNac.setExcludedFromValidation(false);
		RowItem itemFechaAtenc = new RowItem();
		itemFechaAtenc.setItemValue("2018-10-11");
		itemFechaAtenc.setExcludedFromValidation(false);
		RowItem itemEspecialidad = new RowItem();
		itemEspecialidad.setItemValue("669"); // Odontologia
		itemEspecialidad.setExcludedFromValidation(false);
		RowItem mandatory = new RowItem();
		mandatory.setItemValue("5");
		mandatory.setExcludedFromValidation(false);
		mandatory.setMandatory(true);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, itemFechaNac);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, itemFechaAtenc);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_CODIGO_ESPECIALIDAD, itemEspecialidad);
		input.addField(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_EXTRAIDAS, mandatory);
		Optional<RdacaaRawRowResult> sa = extraidosRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El valor es mandatorio",
				sa.get().get(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_EXTRAIDAS).getValidationResultList().isEmpty());
	}
}
