/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.violencia.test;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.violencia.lesionesocurridas.LesionesOcurridasRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author eduardo
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class LesionesOcurridasRuleGroupExecutorTest {
    
    @Autowired
    private LesionesOcurridasRuleGroupExecutor lesionesOcurridasRuleGroupExecutor;
        
    @Test
    public void whenIdentificaAgresorIsExcludedAndValorIsNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("999990000066666");
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.VIOLENCIA_CODIGO_LESION, ritem);
        Optional<RdacaaRawRowResult> sa = lesionesOcurridasRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Codigo Lesion debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.VIOLENCIA_CODIGO_LESION).getValidationResultList().isEmpty());
    }
    
    @Test
    public void whenIdentificaAgresorIsNotExcludedAndValorIsNotNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("2621");
    	ritem.setExcludedFromValidation(false);
    	input.addField(RdacaaVariableKeyCatalog.VIOLENCIA_CODIGO_LESION, ritem);
        Optional<RdacaaRawRowResult> sa = lesionesOcurridasRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Identifica Agresor debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.VIOLENCIA_CODIGO_LESION).getValidationResultList().isEmpty());
    }
    
        @Test
    public void whenCodigoLesionIsNull_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue(null);
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.VIOLENCIA_CODIGO_LESION, ritem);
        Optional<RdacaaRawRowResult> sa = lesionesOcurridasRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("El Codigo Lesion es obligatorio",
                sa.get().get(RdacaaVariableKeyCatalog.VIOLENCIA_CODIGO_LESION)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_VIOLENCIA_LESIONES_OCURRIDAS_NULO)
                        .isPresent());
    }
    
    @Test
    public void whenCodigoLesioinIsNotNumeric_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("sdf");
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.VIOLENCIA_CODIGO_LESION, ritem);
        Optional<RdacaaRawRowResult> sa = lesionesOcurridasRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Se espera un error para caracteres no numericos ",
                sa.get().get(RdacaaVariableKeyCatalog.VIOLENCIA_CODIGO_LESION)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_VIOLENCIA_LESIONES_OCURRIDAS_NUMERICO)
                        .isPresent());
    }
    
    @Test
    public void whenCodigoLesionIsNotCorrect_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("1000");
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.VIOLENCIA_CODIGO_LESION, ritem);
        Optional<RdacaaRawRowResult> sa = lesionesOcurridasRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Se espera un error para codigo incorrecto ",
                sa.get().get(RdacaaVariableKeyCatalog.VIOLENCIA_CODIGO_LESION)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_VIOLENCIA_LESIONES_OCURRIDAS_INCORRECTO)
                        .isPresent());
    }
    
    @Test
    public void whenCodigoLesionIsCorrect_thenOk() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("2619");
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.VIOLENCIA_CODIGO_LESION, ritem);
        Optional<RdacaaRawRowResult> sa = lesionesOcurridasRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("No se esperan errores para Codigo Lesion correcto",
                sa.get().get(RdacaaVariableKeyCatalog.VIOLENCIA_CODIGO_LESION).getValidationResultList().isEmpty());
    }
}
