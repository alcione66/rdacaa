package ec.gob.msp.rdacaa.business.validation.rule.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.persona.segundonombre.SegundoNombreRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class SegundoNombreRuleGroupExecutorTest {

	@Autowired
	private SegundoNombreRuleGroupExecutor segundoNombreRuleGroupExecutor;

	@Test
    public void whenSegundoNombreIsExcludedAndValorIsNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("999990000066666");
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_NOMBRE, ritem);
        Optional<RdacaaRawRowResult> sa = segundoNombreRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("SegundoNombre debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_NOMBRE).getValidationResultList().isEmpty());
    }
    
    @Test
    public void whenSegundoNombreIsExcludedAndValorIsNotNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("REDACAA");
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_NOMBRE, ritem);
        Optional<RdacaaRawRowResult> sa = segundoNombreRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("SegundoNombre debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_NOMBRE)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_SEGUNDO_NOMBRE_NO_DEFINIDO)
                        .isPresent());
    }
    
    @Test
    public void whenSegundoNombreIsNotExcludedAndValorIsNotNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("REDACAA");
    	ritem.setExcludedFromValidation(false);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_NOMBRE, ritem);
        Optional<RdacaaRawRowResult> sa = segundoNombreRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("SegundoNombre debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_NOMBRE).getValidationResultList().isEmpty());
    }
	
	@Test
	public void whenSegundoNombreMenorTresCaracteres_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue("AC");
		ritem.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_NOMBRE, ritem);
		Optional<RdacaaRawRowResult> sa = segundoNombreRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Se espera un error para Segundo Nombre menor a 3 caracteres",
				sa.get().get(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_NOMBRE)
						.hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_SEGUNDO_NOMBRE_LONG)
						.isPresent());
	}

	@Test
	public void whenSegundoNombreIsMayorTresCaracteres_thenNoError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue("MANUEL");
		ritem.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_NOMBRE, ritem);
		Optional<RdacaaRawRowResult> sa = segundoNombreRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("No se espera notificaciones",
				sa.get().get(RdacaaVariableKeyCatalog.PERSONA_SEGUNDO_NOMBRE).getValidationResultList().isEmpty());
	}
}
