package ec.gob.msp.rdacaa.business.validation.rule.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.persona.primernombre.PrimerNombreRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class PrimerNombreRuleGroupExecutorTest {

	@Autowired
	private PrimerNombreRuleGroupExecutor primerNombreRuleGroupExecutor;
	
	@Test
    public void whenPrimerNombreIsExcludedAndValorIsNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("999990000066666");
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_PRIMER_NOMBRE, ritem);
        Optional<RdacaaRawRowResult> sa = primerNombreRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("PrimerNombre debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_PRIMER_NOMBRE).getValidationResultList().isEmpty());
    }
    
    @Test
    public void whenPrimerNombreIsExcludedAndValorIsNotNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("RDACAA");
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_PRIMER_NOMBRE, ritem);
        Optional<RdacaaRawRowResult> sa = primerNombreRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("PrimerNombre debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_PRIMER_NOMBRE)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_PRIMER_NOMBRE_NO_DEFINIDO)
                        .isPresent());
    }
    
    @Test
    public void whenPrimerNombreIsNotExcludedAndValorIsNotNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("RDACAA");
    	ritem.setExcludedFromValidation(false);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_PRIMER_NOMBRE, ritem);
        Optional<RdacaaRawRowResult> sa = primerNombreRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("PrimerNombre debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_PRIMER_NOMBRE).getValidationResultList().isEmpty());
    }


	@Test
	public void whenPrimerNombreIsNull_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue(null);
		ritem.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_PRIMER_NOMBRE, ritem);
		Optional<RdacaaRawRowResult> sa = primerNombreRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Primer Nombre es obligatorio",
				sa.get().get(RdacaaVariableKeyCatalog.PERSONA_PRIMER_NOMBRE)
						.hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_PRIMER_NOMBRE_NULO)
						.isPresent());
	}

	@Test
	public void whenPrimerNombreIsMenorTresCaracteres_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue("AC");
		ritem.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_PRIMER_NOMBRE, ritem);
		Optional<RdacaaRawRowResult> sa = primerNombreRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Se espera un error para Primer Nombre menor a 3 caracteres",
				sa.get().get(RdacaaVariableKeyCatalog.PERSONA_PRIMER_NOMBRE)
						.hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_PRIMER_NOMBRE_LONG)
						.isPresent());
	}
	
	@Test
	public void whenPrimerNombreIsMayorTresCaracteres_thenNoError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue("GARCIA");
		ritem.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_PRIMER_NOMBRE, ritem);
		Optional<RdacaaRawRowResult> sa = primerNombreRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("No se espera notificaciones",
				sa.get().get(RdacaaVariableKeyCatalog.PERSONA_PRIMER_NOMBRE).getValidationResultList().isEmpty());
	}
	
	
}
