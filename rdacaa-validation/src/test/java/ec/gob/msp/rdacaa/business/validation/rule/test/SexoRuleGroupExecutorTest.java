package ec.gob.msp.rdacaa.business.validation.rule.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.persona.sexo.SexoRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

/**
*
* @author dmurillo
*/
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class SexoRuleGroupExecutorTest {

	@Autowired
	private SexoRuleGroupExecutor sexoRuleGroupExecutor;
	
	@Test
    public void whenSexoIsExcludedAndValorIsNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("999990000066666");
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, ritem);
        Optional<RdacaaRawRowResult> sa = sexoRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Sexo debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO).getValidationResultList().isEmpty());
    }
    
    @Test
    public void whenSexoIsExcludedAndValorIsNotNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("114");
    	ritem.setExcludedFromValidation(true);
	    RowItem ritemFN = new RowItem();
        ritemFN.setItemValue("2017-09-01");
        ritemFN.setExcludedFromValidation(false);
        RowItem ritemFA = new RowItem();
        ritemFA.setItemValue("2018-09-14");
        ritemFA.setExcludedFromValidation(false);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, ritem);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, ritemFN);
        input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, ritemFA);
        Optional<RdacaaRawRowResult> sa = sexoRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Sexo debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_SEXO_NO_DEFINIDO)
                        .isPresent());
    }

	
	@Test
    public void whenSexoIsNull_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue(null);
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, ritem);
        Optional<RdacaaRawRowResult> sa = sexoRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("El campo es obligatorio",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_SEXO_NULO)
                        .isPresent());
    }
	
	@Test
    public void whenSexoIsNotNumeric_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("REDACAA");
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, ritem);
        Optional<RdacaaRawRowResult> sa = sexoRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Se espera un error para caracteres no numericos ",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_SEXO_NO_NUMERICO)
                        .isPresent());
    }
	
	@Test
    public void whenSexoIsIncorrecto_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("114");
        ritem.setExcludedFromValidation(false);
        RowItem ritemFN = new RowItem();
        ritemFN.setItemValue("2017-09-01");
        ritemFN.setExcludedFromValidation(false);
        RowItem ritemFA = new RowItem();
        ritemFA.setItemValue("2018-09-14");
        ritemFA.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, ritem);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, ritemFN);
        input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, ritemFA);
        Optional<RdacaaRawRowResult> sa = sexoRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("El campo es incorrecto",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_SEXO_INCORRECTO)
                        .isPresent());
    }
	
	@Test
    public void whenSexoIsCorrecto_thenOk() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("17");
        ritem.setExcludedFromValidation(false);
        RowItem ritemFN = new RowItem();
        ritemFN.setItemValue("2017-09-01");
        ritemFN.setExcludedFromValidation(false);
        RowItem ritemFA = new RowItem();
        ritemFA.setItemValue("2018-09-14");
        ritemFA.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, ritem);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, ritemFN);
        input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, ritemFA);
        Optional<RdacaaRawRowResult> sa = sexoRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("No se espera error para este test",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO).getValidationResultList().isEmpty());
    }
}
