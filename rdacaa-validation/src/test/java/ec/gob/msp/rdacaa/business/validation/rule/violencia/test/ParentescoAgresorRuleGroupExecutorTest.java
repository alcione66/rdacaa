package ec.gob.msp.rdacaa.business.validation.rule.violencia.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.violencia.parentescoagresor.ParentescoAgresorRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class ParentescoAgresorRuleGroupExecutorTest {
	
	@Autowired
    private ParentescoAgresorRuleGroupExecutor parentescoAgresorRuleGroupExecutor;
	
	@Test
    public void whenParentescoAgresorIsExcludedAndValorIsNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("999990000066666");
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.VIOLENCIA_PARENTESCO_AGRESOR, ritem);
        Optional<RdacaaRawRowResult> sa = parentescoAgresorRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Parentesco Agresor debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.VIOLENCIA_PARENTESCO_AGRESOR).getValidationResultList().isEmpty());
    }
	
	@Test
    public void whenParentescoAgresorIsExcludedAndValorIsNotNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("2604");
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.VIOLENCIA_PARENTESCO_AGRESOR, ritem);
        Optional<RdacaaRawRowResult> sa = parentescoAgresorRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Parentesco Agresor debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.VIOLENCIA_PARENTESCO_AGRESOR)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_VIOLENCIA_PARENTESCO_AGRESOR_NO_DEFINIDO)
                        .isPresent());
    }
	
	   @Test
	    public void whenParentescoAgresorIsNotExcludedAndValorIsNotNull(){
	    	RdacaaRawRow input = new RdacaaRawRow();
	    	RowItem ritem = new RowItem();
	    	ritem.setItemValue("2604");
	    	ritem.setExcludedFromValidation(false);
	    	input.addField(RdacaaVariableKeyCatalog.VIOLENCIA_PARENTESCO_AGRESOR, ritem);
	    	
	    	String identificaAgresorSi = "2623";
	        RowItem identificaAgresor = new RowItem();
	        identificaAgresor.setItemValue(identificaAgresorSi);
	        identificaAgresor.setExcludedFromValidation(false);
	        input.addField(RdacaaVariableKeyCatalog.VIOLENCIA_IDENTIFICA_AGRESOR, identificaAgresor);
	    	
	        Optional<RdacaaRawRowResult> sa = parentescoAgresorRuleGroupExecutor.execute(input);
	        sa.ifPresent(r -> {
	            System.out.println(r.toString());
	        });
	        Assert.assertTrue("Parentesco Agresor debe ser nulo",
	                sa.get().get(RdacaaVariableKeyCatalog.VIOLENCIA_PARENTESCO_AGRESOR).getValidationResultList().isEmpty());
	    }
	   
	   @Test
	    public void whenParentescoAgresorIsNull_thenError() {
	        RdacaaRawRow input = new RdacaaRawRow();
	        RowItem ritem = new RowItem();
	        ritem.setItemValue(null);
	        ritem.setExcludedFromValidation(false);
	        input.addField(RdacaaVariableKeyCatalog.VIOLENCIA_PARENTESCO_AGRESOR, ritem);
	        Optional<RdacaaRawRowResult> sa = parentescoAgresorRuleGroupExecutor.execute(input);
	        sa.ifPresent(r -> {
	            System.out.println(r.toString());
	        });
	        Assert.assertTrue("El valor para parentesco del agresor es obligatorio",
	                sa.get().get(RdacaaVariableKeyCatalog.VIOLENCIA_PARENTESCO_AGRESOR)
	                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_VIOLENCIA_PARENTESCO_AGRESOR_NULO)
	                        .isPresent());
	    }
	   
	   @Test
	    public void whenParentescoAgresorIsNotNumeric_thenError() {
	        RdacaaRawRow input = new RdacaaRawRow();
	        RowItem ritem = new RowItem();
	        ritem.setItemValue("sdf");
	        ritem.setExcludedFromValidation(false);
	        input.addField(RdacaaVariableKeyCatalog.VIOLENCIA_PARENTESCO_AGRESOR, ritem);
	        Optional<RdacaaRawRowResult> sa = parentescoAgresorRuleGroupExecutor.execute(input);
	        sa.ifPresent(r -> {
	            System.out.println(r.toString());
	        });
	        Assert.assertTrue("Se espera un error para caracteres no numericos ",
	                sa.get().get(RdacaaVariableKeyCatalog.VIOLENCIA_PARENTESCO_AGRESOR)
	                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_VIOLENCIA_PARENTESCO_AGRESOR_NUMERICO)
	                        .isPresent());
	    }
	   
	   @Test
	    public void whenIdentificaAgresorIsNotCorrect_thenError() {
	        RdacaaRawRow input = new RdacaaRawRow();
	        RowItem ritem = new RowItem();
	        ritem.setItemValue("100");
	        ritem.setExcludedFromValidation(false);
	        input.addField(RdacaaVariableKeyCatalog.VIOLENCIA_PARENTESCO_AGRESOR, ritem);
	        Optional<RdacaaRawRowResult> sa = parentescoAgresorRuleGroupExecutor.execute(input);
	        sa.ifPresent(r -> {
	            System.out.println(r.toString());
	        });
	        Assert.assertTrue("Se espera un error para código de parentesco agresor incorrecto ",
	                sa.get().get(RdacaaVariableKeyCatalog.VIOLENCIA_PARENTESCO_AGRESOR)
	                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_VIOLENCIA_PARENTESCO_AGRESOR_INCORRECTO)
	                        .isPresent());
	    }
	    @Test
	    public void whenParentescoAgresorIsCorrect_thenOk() {
	        RdacaaRawRow input = new RdacaaRawRow();
	        RowItem ritem = new RowItem();
	        ritem.setItemValue("2604");
	        ritem.setExcludedFromValidation(false);
	        input.addField(RdacaaVariableKeyCatalog.VIOLENCIA_PARENTESCO_AGRESOR, ritem);
	        
	        String identificaAgresorSi = "2623";
	        RowItem identificaAgresor = new RowItem();
	        identificaAgresor.setItemValue(identificaAgresorSi);
	        identificaAgresor.setExcludedFromValidation(false);
	        input.addField(RdacaaVariableKeyCatalog.VIOLENCIA_IDENTIFICA_AGRESOR, identificaAgresor);
	        
	        Optional<RdacaaRawRowResult> sa = parentescoAgresorRuleGroupExecutor.execute(input);
	        sa.ifPresent(r -> {
	            System.out.println(r.toString());
	        });
	        Assert.assertTrue("No se esperan errores para parentesco agresor correcto",
	                sa.get().get(RdacaaVariableKeyCatalog.VIOLENCIA_PARENTESCO_AGRESOR).getValidationResultList().isEmpty());
	    }

}
