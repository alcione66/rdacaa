package ec.gob.msp.rdacaa.business.validation.rule.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.service.ConstantesDetalleCatalogo;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.grupoprioritario.gpr_prioritario1.GrupoPrioritario1RuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

/**
 *
 * @author miguel.faubla
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class GrupoPrioritario1RuleGroupExecutorTest {

	@Autowired
	private GrupoPrioritario1RuleGroupExecutor grupoPrioritario1RuleGroupExecutor;

	@Test
	public void whenGrupoPrioritario1IsExcludedAndValorIsNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem grupoprioritarioId = new RowItem();
		grupoprioritarioId.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
		grupoprioritarioId.setExcludedFromValidation(true);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1, grupoprioritarioId);
		Optional<RdacaaRawRowResult> sa = grupoPrioritario1RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Grupo Prioritario 1 debe ser nulo",
				sa.get().get(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1).getValidationResultList().isEmpty());
	}

	@Test
	public void whenGrupoPrioritario1IsExcludedAndValorIsNotNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem grupoprioritarioId = new RowItem();
		grupoprioritarioId.setItemValue("643"); // Trabajador/A Sexual
		grupoprioritarioId.setExcludedFromValidation(true);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1, grupoprioritarioId);
		Optional<RdacaaRawRowResult> sa = grupoPrioritario1RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Grupo Prioritario 1 no debe ser nulo",
				sa.get().get(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_GRUPO_PRIORITARIO_1_NO_DEFINIDA)
						.isPresent());
	}

	@Test
	public void whenGrupoPrioritario1IsNotExcludedAndValorIsNotNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem itemSexo = new RowItem();
		itemSexo.setItemValue("16"); // Hombre
		itemSexo.setExcludedFromValidation(false);
		RowItem itemFechaNac = new RowItem();
		itemFechaNac.setItemValue("2017-09-01");
		itemFechaNac.setExcludedFromValidation(false);
		RowItem itemFechaAtenc = new RowItem();
		itemFechaAtenc.setItemValue("2018-09-14");
		itemFechaAtenc.setExcludedFromValidation(false);
		RowItem grupoprioritarioId = new RowItem();
		grupoprioritarioId.setItemValue("638"); // Maltrato Infantil
		grupoprioritarioId.setExcludedFromValidation(false);
		
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, itemSexo);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, itemFechaNac);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, itemFechaAtenc);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1, grupoprioritarioId);
		Optional<RdacaaRawRowResult> sa = grupoPrioritario1RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Grupo Prioritario 1 no debe ser nulo",
				sa.get().get(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1).getValidationResultList().isEmpty());
	}

	@Test
	public void whenGrupoPrioritario1IsNull_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem grupoprioritarioId = new RowItem();
		grupoprioritarioId.setItemValue(null);
		grupoprioritarioId.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1, grupoprioritarioId);
		Optional<RdacaaRawRowResult> sa = grupoPrioritario1RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El campo es obligatorio", sa.get().get(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1)
				.hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_GRUPO_PRIORITARIO_1_NULO)
				.isPresent());
	}

	@Test
	public void whenGrupoPrioritario1IsNotNumeric_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem grupoprioritarioId = new RowItem();
		grupoprioritarioId.setItemValue("GRUPO PRIORITARIO 1");
		grupoprioritarioId.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1, grupoprioritarioId);
		Optional<RdacaaRawRowResult> sa = grupoPrioritario1RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Se espera un error para caracteres no numéricos ",
				sa.get().get(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_GRUPO_PRIORITARIO_1_NONUMERICO)
						.isPresent());
	}

	@Test
	public void whenGrupoPrioritario1IsIncorrecto_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem itemSexo = new RowItem();
		itemSexo.setItemValue("114");
		itemSexo.setExcludedFromValidation(false);
		RowItem itemGrupoPrio = new RowItem();
		itemGrupoPrio.setItemValue("7148");
		itemGrupoPrio.setExcludedFromValidation(false);
		RowItem itemFechaNac = new RowItem();
		itemFechaNac.setItemValue("2017-09-01");
		itemFechaNac.setExcludedFromValidation(false);
		RowItem itemFechaAtenc = new RowItem();
		itemFechaAtenc.setItemValue("2018-09-14");
		itemFechaAtenc.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, itemSexo);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, itemFechaNac);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, itemFechaAtenc);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1, itemGrupoPrio);
		Optional<RdacaaRawRowResult> sa = grupoPrioritario1RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El campo es incorrecto",
				sa.get().get(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_GRUPO_PRIORITARIO_1_INCORRECTO)
						.isPresent());
	}

	@Test
	public void whenGrupoPrioritario1IsCorrect_thenOk() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem itemSexo = new RowItem();
		itemSexo.setItemValue("17"); // Mujer
		itemSexo.setExcludedFromValidation(false);
		RowItem grupoprioritarioId = new RowItem();
		grupoprioritarioId.setItemValue("634"); // Embarazadas
		grupoprioritarioId.setExcludedFromValidation(false);
		RowItem itemFechaNac = new RowItem();
		itemFechaNac.setItemValue("2000-09-01");
		itemFechaNac.setExcludedFromValidation(false);
		RowItem itemFechaAtenc = new RowItem();
		itemFechaAtenc.setItemValue("2018-09-14");
		itemFechaAtenc.setExcludedFromValidation(false);

		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, itemSexo);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, itemFechaNac);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, itemFechaAtenc);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1, grupoprioritarioId);
		Optional<RdacaaRawRowResult> sa = grupoPrioritario1RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("No se esperan errores para Grupo Prioritario 1 correcto",
				sa.get().get(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1).getValidationResultList().isEmpty());
	}

	@Test
	public void whenGrupoPrioritario1IsMandatory() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem grupoprioritarioId = new RowItem();
		grupoprioritarioId.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
		grupoprioritarioId.setExcludedFromValidation(false);
		grupoprioritarioId.setMandatory(true);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1, grupoprioritarioId);
		Optional<RdacaaRawRowResult> sa = grupoPrioritario1RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Grupo Prioritario 1 es mandatorio",
				sa.get().get(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_GRUPO_PRIORITARIO_1_ES_MANDATORIA)
						.isPresent());
	}

	@Test
	public void whenGrupoPrioritario1IsMadatoryAndCorrectValue() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem itemSexo = new RowItem();
		itemSexo.setItemValue("17"); // Mujer
		itemSexo.setExcludedFromValidation(false);
		RowItem itemFechaNac = new RowItem();
		itemFechaNac.setItemValue("2017-09-01");
		itemFechaNac.setExcludedFromValidation(false);
		RowItem itemFechaAtenc = new RowItem();
		itemFechaAtenc.setItemValue("2018-09-14");
		itemFechaAtenc.setExcludedFromValidation(false);
		RowItem grupoprioritarioId = new RowItem();
		grupoprioritarioId.setItemValue("638"); // Maltrato Infantil
		grupoprioritarioId.setExcludedFromValidation(false);
		grupoprioritarioId.setMandatory(true);
		
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, itemSexo);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, itemFechaNac);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, itemFechaAtenc);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1, grupoprioritarioId);
		Optional<RdacaaRawRowResult> sa = grupoPrioritario1RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Grupo Prioritario 1 es mandatorio",
				sa.get().get(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1).getValidationResultList().isEmpty());
	}

}
