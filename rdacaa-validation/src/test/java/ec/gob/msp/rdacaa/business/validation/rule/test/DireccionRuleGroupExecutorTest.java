/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.test;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.persona.direccion.DireccionRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author eduardo
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class DireccionRuleGroupExecutorTest {
    
    @Autowired
    private DireccionRuleGroupExecutor direccionRuleGroupExecutor;
    
    @Test
    public void whenDireccionIsNull_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue(null);
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_DIRECCION_DOMICILIO, ritem);
        Optional<RdacaaRawRowResult> sa = direccionRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("El descripcion de la Direccion es obligatoria",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_DIRECCION_DOMICILIO)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_DIRECCION_NULL)
                        .isPresent());
    }
    
    @Test
    public void whenDireccionLongitudIncorrect_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue("22");
		ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_DIRECCION_DOMICILIO, ritem);
        Optional<RdacaaRawRowResult> sa = direccionRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Se espera un error longitud incorrecta",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_DIRECCION_DOMICILIO)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_DIRECCION_LONGITUD_INCORRECTA)
                        .isPresent());
	}
    
    @Test
    public void whenDireccionLongitudIsCorrect_thenOk() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue("Las Casas");
		ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_DIRECCION_DOMICILIO, ritem);
        Optional<RdacaaRawRowResult> sa = direccionRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("No se esperan errores",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_DIRECCION_DOMICILIO).getValidationResultList().isEmpty());
	}
    
}
