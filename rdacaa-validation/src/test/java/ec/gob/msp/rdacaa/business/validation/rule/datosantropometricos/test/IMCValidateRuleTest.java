package ec.gob.msp.rdacaa.business.validation.rule.datosantropometricos.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.deliveredtechnologies.rulebook.Fact;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.categoriaimcparaedad.CategoriaIMCParaEdadRuleGroupExecutor;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.imc.IMCRuleGroupExecutor;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.peso.PesoRuleGroupExecutor;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.puntajezimcparaedad.PuntajeZIMCParaEdadRuleGroupExecutor;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.talla.TallaRuleGroupExecutor;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.tallacorregida.TallaCorregidaRuleGroupExecutor;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.tallatipotoma.TallaTipoTomaRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class IMCValidateRuleTest {

	private static final String DE_PIE = "0";
	private static final String ACOSTADO = "1";

	@Autowired
	private PesoRuleGroupExecutor pesoRuleGroupExecutor;

	@Autowired
	private TallaRuleGroupExecutor tallaRuleGroupExecutor;

	@Autowired
	private TallaCorregidaRuleGroupExecutor tallaCorregidaRuleGroupExecutor;

	@Autowired
	private IMCRuleGroupExecutor imcRuleGroupExecutor;

	@Autowired
	private TallaTipoTomaRuleGroupExecutor tallaTipoTomaRuleGroupExecutor;
	
	@Autowired
	private PuntajeZIMCParaEdadRuleGroupExecutor puntajeZIMCParaEdadRuleGroupExecutor;
	
	@Autowired
	private CategoriaIMCParaEdadRuleGroupExecutor categoriaIMCParaEdadRuleGroupExecutor;

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void whenPesoTallaCorregidaHasErrors_thenError() {
		RdacaaRawRowResult resultTotal = new RdacaaRawRowResult();
		// ERROR PESO
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem sexo = new RowItem();
		sexo.setItemValue("16");// hombre
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2018-05-06");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2019-11-23");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem peso = new RowItem();
		peso.setItemValue("999990000066666");
		peso.setExcludedFromValidation(false);
		peso.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO, peso);
		Optional<RdacaaRawRowResult> sa1 = pesoRuleGroupExecutor.execute(input);

		resultTotal.putAll(sa1.get());

		Fact resultTotalFact1 = new Fact("rdacaa_row_result", resultTotal);

		// ERROR TALLA
		RowItem talla = new RowItem();
		talla.setItemValue("asasa");
		talla.setExcludedFromValidation(false);
		talla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, talla);

		Optional<RdacaaRawRowResult> sa2 = tallaRuleGroupExecutor.execute(input, resultTotalFact1);

		resultTotal.putAll(sa2.get());

		Fact resultTotalFact2 = new Fact("rdacaa_row_result", resultTotal);

		// Tipo Toma Talla
		RowItem tipoTomaTalla = new RowItem();
		tipoTomaTalla.setItemValue(ACOSTADO);
		tipoTomaTalla.setExcludedFromValidation(false);
		tipoTomaTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA, tipoTomaTalla);

		Optional<RdacaaRawRowResult> sa3 = tallaTipoTomaRuleGroupExecutor.execute(input, resultTotalFact2);

		resultTotal.putAll(sa3.get());

		Fact resultTotalFact3 = new Fact("rdacaa_row_result", resultTotal);

		RowItem tallaCorregida = new RowItem();
		tallaCorregida.setItemValue("999990000066666");
		tallaCorregida.setExcludedFromValidation(false);
		tallaCorregida.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA, tallaCorregida);

		Optional<RdacaaRawRowResult> sa4 = tallaCorregidaRuleGroupExecutor.execute(input, resultTotalFact3);

		resultTotal.putAll(sa4.get());

		Fact resultTotalFact4 = new Fact("rdacaa_row_result", resultTotal);

		// IMC
		RowItem imc = new RowItem();
		imc.setItemValue("999990000066666");
		imc.setExcludedFromValidation(false);
		imc.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_INDICE_MASA_CORPORAL, imc);

		Optional<RdacaaRawRowResult> sa5 = imcRuleGroupExecutor.execute(input, resultTotalFact4);

		sa5.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa5.get());

		Fact resultTotalFact5 = new Fact("rdacaa_row_result", resultTotal);
		
		// PZ IMC Edad
		RowItem pzIMCEdad = new RowItem();
		pzIMCEdad.setItemValue("999990000066666");
		pzIMCEdad.setExcludedFromValidation(false);
		pzIMCEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_IMC_PARA_EDAD, pzIMCEdad);

		Optional<RdacaaRawRowResult> sa6 = puntajeZIMCParaEdadRuleGroupExecutor.execute(input, resultTotalFact5);

		sa6.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa6.get());

		Fact resultTotalFact6 = new Fact("rdacaa_row_result", resultTotal);
		
		// Cat PZ IMC Edad
		RowItem catPzIMCEdad = new RowItem();
		catPzIMCEdad.setItemValue("999990000066666");
		catPzIMCEdad.setExcludedFromValidation(false);
		catPzIMCEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_IMC_PARA_EDAD, catPzIMCEdad);

		Optional<RdacaaRawRowResult> sa7 = categoriaIMCParaEdadRuleGroupExecutor.execute(input, resultTotalFact6);
		
		sa7.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		Assert.assertTrue(
				"Se espera CODIGO_ERROR_DAT_ANT_IMC_PESO_TIENE_ERRORES y CODIGO_ERROR_DAT_ANT_IMC_TALLA_CORREGIDA_TIENE_ERRORES",
				sa5.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_INDICE_MASA_CORPORAL)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_IMC_PESO_TIENE_ERRORES)
						.isPresent()
						&& sa5.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_INDICE_MASA_CORPORAL)
								.hasValidationResultByCode(
										ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_IMC_TALLA_CORREGIDA_TIENE_ERRORES)
								.isPresent());

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void whenPesoTallaCorregidaOK_thenNoError() {
		RdacaaRawRowResult resultTotal = new RdacaaRawRowResult();
		RdacaaRawRow input = new RdacaaRawRow();
		// PESO
		RowItem sexo = new RowItem();
		sexo.setItemValue("17");// mujer
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		RowItem grupoPrioritarioEmbarazada = new RowItem();
		grupoPrioritarioEmbarazada.setItemValue("634");
		grupoPrioritarioEmbarazada.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1, grupoPrioritarioEmbarazada);
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2000-05-06");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-23");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem peso = new RowItem();
		peso.setItemValue("65");
		peso.setExcludedFromValidation(false);
		peso.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO, peso);

		Optional<RdacaaRawRowResult> sa1 = pesoRuleGroupExecutor.execute(input);

		sa1.ifPresent(r -> {
			System.out.println(r.toString());
		});

		resultTotal.putAll(sa1.get());

		Fact resultTotalFact1 = new Fact("rdacaa_row_result", resultTotal);

		// TALLA
		RowItem talla = new RowItem();
		talla.setItemValue("150");
		talla.setExcludedFromValidation(false);
		talla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, talla);

		Optional<RdacaaRawRowResult> sa2 = tallaRuleGroupExecutor.execute(input, resultTotalFact1);

		sa2.ifPresent(r -> {
			System.out.println(r.toString());
		});

		resultTotal.putAll(sa2.get());

		Fact resultTotalFact2 = new Fact("rdacaa_row_result", resultTotal);

		// Tipo Toma Talla
		RowItem tipoTomaTalla = new RowItem();
		tipoTomaTalla.setItemValue(DE_PIE);
		tipoTomaTalla.setExcludedFromValidation(false);
		tipoTomaTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA, tipoTomaTalla);

		Optional<RdacaaRawRowResult> sa3 = tallaTipoTomaRuleGroupExecutor.execute(input, resultTotalFact2);

		sa3.ifPresent(r -> {
			System.out.println(r.toString());
		});

		resultTotal.putAll(sa3.get());

		Fact resultTotalFact3 = new Fact("rdacaa_row_result", resultTotal);

		RowItem tallaCorregida = new RowItem();
		tallaCorregida.setItemValue("999990000066666");
		tallaCorregida.setExcludedFromValidation(false);
		tallaCorregida.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA, tallaCorregida);

		Optional<RdacaaRawRowResult> sa4 = tallaCorregidaRuleGroupExecutor.execute(input, resultTotalFact3);

		sa4.ifPresent(r -> {
			System.out.println(r.toString());
		});

		resultTotal.putAll(sa4.get());

		Fact resultTotalFact4 = new Fact("rdacaa_row_result", resultTotal);

		// IMC
		RowItem imc = new RowItem();
		imc.setItemValue("999990000066666");
		imc.setExcludedFromValidation(false);
		imc.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_INDICE_MASA_CORPORAL, imc);

		Optional<RdacaaRawRowResult> sa5 = imcRuleGroupExecutor.execute(input, resultTotalFact4);

		sa5.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa5.get());

		Fact resultTotalFact5 = new Fact("rdacaa_row_result", resultTotal);
		
		// PZ IMC Edad
		RowItem pzIMCEdad = new RowItem();
		pzIMCEdad.setItemValue("999990000066666");
		pzIMCEdad.setExcludedFromValidation(false);
		pzIMCEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_IMC_PARA_EDAD, pzIMCEdad);

		Optional<RdacaaRawRowResult> sa6 = puntajeZIMCParaEdadRuleGroupExecutor.execute(input, resultTotalFact5);

		sa6.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa6.get());

		Fact resultTotalFact6 = new Fact("rdacaa_row_result", resultTotal);
		
		// Cat PZ IMC Edad
		RowItem catPzIMCEdad = new RowItem();
		catPzIMCEdad.setItemValue("999990000066666");
		catPzIMCEdad.setExcludedFromValidation(false);
		catPzIMCEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_IMC_PARA_EDAD, catPzIMCEdad);

		Optional<RdacaaRawRowResult> sa7 = categoriaIMCParaEdadRuleGroupExecutor.execute(input, resultTotalFact6);
		
		sa7.ifPresent(r -> {
			System.out.println(r.toString());
		});


		Assert.assertTrue("No se esperan errores",
				!sa5.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_INDICE_MASA_CORPORAL).hasErrors());

	}

	@SuppressWarnings({ "unchecked", "rawtypes" })
	@Test
	public void whenIMCMayor200_thenError() {
		RdacaaRawRowResult resultTotal = new RdacaaRawRowResult();
		RdacaaRawRow input = new RdacaaRawRow();
		// PESO
		RowItem sexo = new RowItem();
		sexo.setItemValue("17");// mujer
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		RowItem grupoPrioritarioEmbarazada = new RowItem();
		grupoPrioritarioEmbarazada.setItemValue("634");
		grupoPrioritarioEmbarazada.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1, grupoPrioritarioEmbarazada);
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2000-05-06");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-23");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem peso = new RowItem();
		peso.setItemValue("201");
		peso.setExcludedFromValidation(false);
		peso.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PESO, peso);

		Optional<RdacaaRawRowResult> sa1 = pesoRuleGroupExecutor.execute(input);
		
		sa1.ifPresent(r -> {
			System.out.println(r.toString());
		});

		resultTotal.putAll(sa1.get());

		Fact resultTotalFact1 = new Fact("rdacaa_row_result", resultTotal);

		// TALLA
		RowItem talla = new RowItem();
		talla.setItemValue("100");
		talla.setExcludedFromValidation(false);
		talla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, talla);

		Optional<RdacaaRawRowResult> sa2 = tallaRuleGroupExecutor.execute(input, resultTotalFact1);

		sa2.ifPresent(r -> {
			System.out.println(r.toString());
		});

		resultTotal.putAll(sa2.get());

		Fact resultTotalFact2 = new Fact("rdacaa_row_result", resultTotal);

		// Tipo Toma Talla
		RowItem tipoTomaTalla = new RowItem();
		tipoTomaTalla.setItemValue(DE_PIE);
		tipoTomaTalla.setExcludedFromValidation(false);
		tipoTomaTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA, tipoTomaTalla);

		Optional<RdacaaRawRowResult> sa3 = tallaTipoTomaRuleGroupExecutor.execute(input, resultTotalFact2);

		sa3.ifPresent(r -> {
			System.out.println(r.toString());
		});

		resultTotal.putAll(sa3.get());

		Fact resultTotalFact3 = new Fact("rdacaa_row_result", resultTotal);

		RowItem tallaCorregida = new RowItem();
		tallaCorregida.setItemValue("999990000066666");
		tallaCorregida.setExcludedFromValidation(false);
		tallaCorregida.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA_CORREGIDA, tallaCorregida);

		Optional<RdacaaRawRowResult> sa4 = tallaCorregidaRuleGroupExecutor.execute(input, resultTotalFact3);
		
		sa4.ifPresent(r -> {
			System.out.println(r.toString());
		});

		resultTotal.putAll(sa4.get());

		Fact resultTotalFact4 = new Fact("rdacaa_row_result", resultTotal);

		// IMC
		RowItem imc = new RowItem();
		imc.setItemValue("999990000066666");
		imc.setExcludedFromValidation(false);
		imc.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_INDICE_MASA_CORPORAL, imc);

		Optional<RdacaaRawRowResult> sa5= imcRuleGroupExecutor.execute(input, resultTotalFact4);

		sa5.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa5.get());

		Fact resultTotalFact5 = new Fact("rdacaa_row_result", resultTotal);
		
		// PZ IMC Edad
		RowItem pzIMCEdad = new RowItem();
		pzIMCEdad.setItemValue("999990000066666");
		pzIMCEdad.setExcludedFromValidation(false);
		pzIMCEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_PUNTAJE_Z_IMC_PARA_EDAD, pzIMCEdad);

		Optional<RdacaaRawRowResult> sa6 = puntajeZIMCParaEdadRuleGroupExecutor.execute(input, resultTotalFact5);

		sa6.ifPresent(r -> {
			System.out.println(r.toString());
		});
		
		resultTotal.putAll(sa6.get());

		Fact resultTotalFact6 = new Fact("rdacaa_row_result", resultTotal);
		
		// Cat PZ IMC Edad
		RowItem catPzIMCEdad = new RowItem();
		catPzIMCEdad.setItemValue("999990000066666");
		catPzIMCEdad.setExcludedFromValidation(false);
		catPzIMCEdad.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CATEGORIA_IMC_PARA_EDAD, catPzIMCEdad);

		Optional<RdacaaRawRowResult> sa7 = categoriaIMCParaEdadRuleGroupExecutor.execute(input, resultTotalFact6);
		
		sa7.ifPresent(r -> {
			System.out.println(r.toString());
		});


		Assert.assertTrue("Se espera CODIGO_ERR_DAT_ANT_IMC_MAYOR_200",
				sa5.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_INDICE_MASA_CORPORAL)
						.hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERR_DAT_ANT_IMC_MAYOR_200)
						.isPresent());
	}
	
}
