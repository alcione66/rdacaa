package ec.gob.msp.rdacaa.business.validation.rule.gvulnerable.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.grupovulnerable.gpr_vulnerable2.GrupoVulnerable2RuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class GrupoVulnerable2RuleGroupExecutorTest {
	@Autowired
	private GrupoVulnerable2RuleGroupExecutor grupoVulnerable2RuleGroupExecutor;

	@Test
	public void whenCodigoGrupoVulnerable2IsNull_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue(null);
		ritem.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_VULNERABLE_2, ritem);
		Optional<RdacaaRawRowResult> sa = grupoVulnerable2RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Codigo para Grupo Vulnerable 2 es obligatorio", sa.get()
				.get(RdacaaVariableKeyCatalog.GRUPO_VULNERABLE_2)
				.hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_GRUPO_VULNERABLE_2_NULO)
				.isPresent());
	}

	@Test
	public void whenGrupoVulnerable2IsIncorrecto_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue("1325854"); // Valor no aplicable
		ritem.setExcludedFromValidation(false);

		// fecha nacimiento
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2000-11-03");
		fechaNac.setExcludedFromValidation(false);
		// fecha atencion
		RowItem fechaAt = new RowItem();
		fechaAt.setItemValue("2018-11-03");
		fechaAt.setExcludedFromValidation(false);
		// sexo
		RowItem sexo = new RowItem();
		sexo.setItemValue("17");
		sexo.setExcludedFromValidation(false);
		// embarazada
		RowItem embarazada = new RowItem();
		embarazada.setItemValue("634");
		embarazada.setExcludedFromValidation(false);

		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAt);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1, embarazada);

		input.addField(RdacaaVariableKeyCatalog.GRUPO_VULNERABLE_2, ritem);
		Optional<RdacaaRawRowResult> sa = grupoVulnerable2RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El campo es incorrecto",
				sa.get().get(RdacaaVariableKeyCatalog.GRUPO_VULNERABLE_2)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_GRUPO_VULNERABLE_2_INCORRECTO)
						.isPresent());
	}

	@Test
	public void whenGrupoVulnerable2IsCorrect_thenOk() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue("2641");
		ritem.setExcludedFromValidation(false);

		// fecha nacimiento
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2000-11-03");
		fechaNac.setExcludedFromValidation(false);
		// fecha atencion
		RowItem fechaAt = new RowItem();
		fechaAt.setItemValue("2018-11-03");
		fechaAt.setExcludedFromValidation(false);
		// sexo
		RowItem sexo = new RowItem();
		sexo.setItemValue("17");
		sexo.setExcludedFromValidation(false);
		// embarazada
		RowItem embarazada = new RowItem();
		embarazada.setItemValue("634");
		embarazada.setExcludedFromValidation(false);

		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAt);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1, embarazada);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_VULNERABLE_2, ritem);
		Optional<RdacaaRawRowResult> sa = grupoVulnerable2RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue(
				"No se esperan errores para codigo Grupo Vulnerable 2 en pantalla de GRUPOS VULNERABLES correcto",
				sa.get().get(RdacaaVariableKeyCatalog.GRUPO_VULNERABLE_2).getValidationResultList().isEmpty());
	}
}
