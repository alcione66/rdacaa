/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.test;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

/**
 *
 * @author Saulo Velasco
 */
@SpringBootApplication
@EnableJpaRepositories(basePackages = "ec.gob.msp.rdacaa.business.repository")
@EntityScan("ec.gob.msp.rdacaa.business.entity")
@ComponentScan(basePackages = {"ec.gob.msp.rdacaa.system.database", "ec.gob.msp.rdacaa.business.service",
    "ec.gob.msp.rdacaa.business.validation"})
@PropertySource(value = {"classpath:application-test.properties"})
public class ValidationTestContext {

    public static void main(String[] args) {
        ConfigurableApplicationContext context = new SpringApplicationBuilder(ValidationTestContext.class).headless(false).run(args);
    }
}
