package ec.gob.msp.rdacaa.business.validation.rule.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.service.ConstantesDetalleCatalogo;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.grupoprioritario.lugaratencion.LugarAtencionRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

/**
 *
 * @author miguel.faubla
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class LugarAtencionRuleGroupExecutorTest {

	@Autowired
	private LugarAtencionRuleGroupExecutor lugarAtencionRuleGroupExecutor;

	@Test
	public void whenLugarAtencionIsExcludedAndValorIsNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem lugarAtencionId = new RowItem();
		lugarAtencionId.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
		lugarAtencionId.setExcludedFromValidation(true);
		input.addField(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_LUGAR_ATENCION, lugarAtencionId);
		Optional<RdacaaRawRowResult> sa = lugarAtencionRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Lugar de Atención debe ser nulo", sa.get()
				.get(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_LUGAR_ATENCION).getValidationResultList().isEmpty());
	}

	@Test
	public void whenLugarAtencionIsExcludedAndValorIsNotNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem lugarAtencionId = new RowItem();
		lugarAtencionId.setItemValue("4568");
		lugarAtencionId.setExcludedFromValidation(true);
		input.addField(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_LUGAR_ATENCION, lugarAtencionId);
		Optional<RdacaaRawRowResult> sa = lugarAtencionRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Lugar de Atención no debe ser nulo",
				sa.get().get(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_LUGAR_ATENCION).hasValidationResultByCode(
						ValidationResultCatalogConstants.CODIGO_ERROR_LUGAR_ATENCION_ESTABLECIMIENTO_NO_DEFINIDA)
						.isPresent());
	}

	@Test
	public void whenLugarAtencionIsNotExcludedAndValorIsNotNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem itemSexo = new RowItem();
		itemSexo.setItemValue("16"); // Hombre
		itemSexo.setExcludedFromValidation(false);
		RowItem lugarAtencionId = new RowItem();
		lugarAtencionId.setItemValue("716"); // Albergues creados por desastre
		lugarAtencionId.setExcludedFromValidation(false);
		lugarAtencionId.setMandatory(true);
		RowItem itemFechaNac = new RowItem(); // Fecha Nacimiento
		itemFechaNac.setItemValue("2017-09-01");
		itemFechaNac.setExcludedFromValidation(false);
		RowItem itemFechaAtenc = new RowItem();
		itemFechaAtenc.setItemValue("2018-10-31"); // Fecha Atencion
		itemFechaAtenc.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, itemSexo);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, itemFechaNac);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, itemFechaAtenc);
		input.addField(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_LUGAR_ATENCION, lugarAtencionId);
		Optional<RdacaaRawRowResult> sa = lugarAtencionRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Lugar de Atención no debe ser nulo", sa.get()
				.get(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_LUGAR_ATENCION).getValidationResultList().isEmpty());
	}

	@Test
	public void whenLugarAtencionIsNull_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem lugarAtencionId = new RowItem();
		lugarAtencionId.setItemValue(null);
		lugarAtencionId.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_LUGAR_ATENCION, lugarAtencionId);
		Optional<RdacaaRawRowResult> sa = lugarAtencionRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El campo es obligatorio",
				sa.get().get(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_LUGAR_ATENCION)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_LUGAR_ATENCION_ESTABLECIMIENTO_NULO)
						.isPresent());
	}

	@Test
	public void whenLugarAtencionIsNotNumeric_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem lugarAtencionId = new RowItem();
		lugarAtencionId.setItemValue("ATENCION");
		lugarAtencionId.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_LUGAR_ATENCION, lugarAtencionId);
		Optional<RdacaaRawRowResult> sa = lugarAtencionRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Se espera un error para caracteres no numéricos ",
				sa.get().get(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_LUGAR_ATENCION)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_LUGAR_ATENCION_ESTABLECIMIENTO_NONUMERICO)
						.isPresent());
	}

	@Test
	public void whenLugarAtencionIsIncorrecto_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem itemSexo = new RowItem();
		itemSexo.setItemValue("114");
		itemSexo.setExcludedFromValidation(false);
		RowItem itemLugarAtenc = new RowItem();
		itemLugarAtenc.setItemValue("7148");
		itemLugarAtenc.setExcludedFromValidation(false);
		RowItem itemFechaNac = new RowItem();
		itemFechaNac.setItemValue("2017-09-01");
		itemFechaNac.setExcludedFromValidation(false);
		RowItem itemFechaAtenc = new RowItem();
		itemFechaAtenc.setItemValue("2018-09-14");
		itemFechaAtenc.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, itemSexo);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, itemFechaNac);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, itemFechaAtenc);
		input.addField(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_LUGAR_ATENCION, itemLugarAtenc);
		Optional<RdacaaRawRowResult> sa = lugarAtencionRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El campo es incorrecto",
				sa.get().get(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_LUGAR_ATENCION)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_LUGAR_ATENCION_ESTABLECIMIENTO_INCORRECTO)
						.isPresent());
	}

	@Test
	public void whenLugarAtencionIsCorrect_thenOk() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem itemSexo = new RowItem();
		itemSexo.setItemValue("16"); // Hombre
		itemSexo.setExcludedFromValidation(false);
		RowItem lugarAtencionId = new RowItem();
		lugarAtencionId.setItemValue("716"); // Albergues creados por desastre
		lugarAtencionId.setExcludedFromValidation(false);
		lugarAtencionId.setMandatory(true);
		RowItem itemFechaNac = new RowItem(); // Fecha Nacimiento
		itemFechaNac.setItemValue("2017-09-01");
		itemFechaNac.setExcludedFromValidation(false);
		RowItem itemFechaAtenc = new RowItem();
		itemFechaAtenc.setItemValue("2018-10-31"); // Fecha Atencion
		itemFechaAtenc.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, itemSexo);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, itemFechaNac);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, itemFechaAtenc);
		input.addField(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_LUGAR_ATENCION, lugarAtencionId);
		Optional<RdacaaRawRowResult> sa = lugarAtencionRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("No se esperan errores para Lugar de atención correcto", sa.get()
				.get(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_LUGAR_ATENCION).getValidationResultList().isEmpty());
	}

	@Test
	public void whenLugarAtencionIsMandatory() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem lugarAtencionId = new RowItem();
		lugarAtencionId.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
		lugarAtencionId.setExcludedFromValidation(false);
		lugarAtencionId.setMandatory(true);
		input.addField(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_LUGAR_ATENCION, lugarAtencionId);
		Optional<RdacaaRawRowResult> sa = lugarAtencionRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Lugar de Atención es mandatorio",
				sa.get().get(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_LUGAR_ATENCION).hasValidationResultByCode(
						ValidationResultCatalogConstants.CODIGO_ERROR_LUGAR_ATENCION_ESTABLECIMIENTO_ES_MANDATORIA)
						.isPresent());
	}

	@Test
	public void whenLugarAtencionIsMadatoryAndCorrectValue() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem itemSexo = new RowItem();
		itemSexo.setItemValue("16"); // Hombre
		itemSexo.setExcludedFromValidation(false);
		RowItem lugarAtencionId = new RowItem();
		lugarAtencionId.setItemValue("716"); // Albergues creados por desastre
		lugarAtencionId.setExcludedFromValidation(false);
		lugarAtencionId.setMandatory(true);
		RowItem itemFechaNac = new RowItem(); // Fecha Nacimiento
		itemFechaNac.setItemValue("2017-09-01");
		itemFechaNac.setExcludedFromValidation(false);
		RowItem itemFechaAtenc = new RowItem();
		itemFechaAtenc.setItemValue("2018-10-31"); // Fecha Atencion
		itemFechaAtenc.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, itemSexo);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, itemFechaNac);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, itemFechaAtenc);
		input.addField(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_LUGAR_ATENCION, lugarAtencionId);
		Optional<RdacaaRawRowResult> sa = lugarAtencionRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Lugar de Atención es mandatorio", sa.get()
				.get(RdacaaVariableKeyCatalog.ESTABLECIMIENTO_LUGAR_ATENCION).getValidationResultList().isEmpty());
	}

}
