/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.test;

import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.validation.IndiceMasaCorporalValidator;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalog;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

/**
 *
 * @author Saulo Velasco
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class IndiceMasaCorporalValidatorTest {

	@Autowired
	private IndiceMasaCorporalValidator indiceMasaCorporalValidator;

	@Test
	public void whenIMCNullOrEmpty_thenIMCVacioError() {
		String[] imcEdadTalla = { "", "050600", "170" };
		List<ValidationResult> listaErrores = indiceMasaCorporalValidator.validate(imcEdadTalla);
		Optional<ValidationResult> error = listaErrores.parallelStream().filter(e -> {
			return e.getCode().equals(ValidationResultCatalog.IMC_VACIO.getValidationError().getCode());
		}).findAny();
		Assert.assertTrue("Se espera un error para IMC nulo", error.isPresent());
	}
}
