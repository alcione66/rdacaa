/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.test;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.persona.identificacionrep.IdentificacionRepRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author eduardo
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class IdentificacionRepRuleGroupExecutorTest {
    @Autowired
	private IdentificacionRepRuleGroupExecutor identificacionRepRuleGroupExecutor;
    
    @Test
    public void whenIdentificacionRepIsNull_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue(null);
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION_REPRESENTANTE, ritem);
        Optional<RdacaaRawRowResult> sa = identificacionRepRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("El Tipo de Identificacion Representante es obligatorio",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION_REPRESENTANTE)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_IDENTIFICACION_REP_NULO)
                        .isPresent());
    }
	
//	@Test
//    public void whenIdentificacionCedulaRepLongitudIncorrect_thenError() {
//		RdacaaRawRow input = new RdacaaRawRow();
//		RowItem ritemTI = new RowItem();
//        ritemTI.setItemValue("6");
//        ritemTI.setExcludedFromValidation(false);
//		RowItem ritem = new RowItem();
//		ritem.setItemValue("111222333");
//		ritem.setExcludedFromValidation(false);
//		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_IDENTIFICACION, ritemTI);
//        input.addField(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION_REPRESENTANTE, ritem);
//        Optional<RdacaaRawRowResult> sa = identificacionRepRuleGroupExecutor.execute(input);
//        sa.ifPresent(r -> {
//            System.out.println(r.toString());
//        });
//        Assert.assertTrue("Se espera un error longitud incorrecta",
//                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION_REPRESENTANTE)
//                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_IDENTIFICACION_REP_CEDULA_LONGITUD_INCORRECTA)
//                        .isPresent());
//	}
	
//	@Test
//    public void whenIdentificacionCedulaRepIsIncorrect_thenError() {
//		RdacaaRawRow input = new RdacaaRawRow();
//		RowItem ritemTI = new RowItem();
//        ritemTI.setItemValue("6");
//        ritemTI.setExcludedFromValidation(false);
//		RowItem ritem = new RowItem();
//		ritem.setItemValue("1815432116");
//		ritem.setExcludedFromValidation(false);
//		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_IDENTIFICACION, ritemTI);
//        input.addField(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION_REPRESENTANTE, ritem);
//        Optional<RdacaaRawRowResult> sa = identificacionRepRuleGroupExecutor.execute(input);
//        sa.ifPresent(r -> {
//            System.out.println(r.toString());
//        });
//        Assert.assertTrue("Se espera un error validacion incorrecta",
//                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION_REPRESENTANTE)
//                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_IDENTIFICACION_REP_INCORRECTO)
//                        .isPresent());
//	}
	
	@Test
    public void whenIdentificacionCedulaRepIsCorrect_thenOk() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritemTI = new RowItem();
        ritemTI.setItemValue("6");
        ritemTI.setExcludedFromValidation(false);
		RowItem ritem = new RowItem();
		ritem.setItemValue("1715932115");
		ritem.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_IDENTIFICACION, ritemTI);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION_REPRESENTANTE, ritem);
        Optional<RdacaaRawRowResult> sa = identificacionRepRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("No se espera errores",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION_REPRESENTANTE).getValidationResultList().isEmpty());
	}
}
