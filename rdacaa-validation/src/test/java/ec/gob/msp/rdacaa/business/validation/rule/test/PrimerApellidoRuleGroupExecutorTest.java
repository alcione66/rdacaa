package ec.gob.msp.rdacaa.business.validation.rule.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.persona.primerapellido.PrimerApellidoRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class PrimerApellidoRuleGroupExecutorTest {

	@Autowired
	private PrimerApellidoRuleGroupExecutor primerApellidoRuleGroupExecutor;

	@Test
    public void whenPrimerApellidoIsExcludedAndValorIsNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("999990000066666");
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_PRIMER_APELLIDO, ritem);
        Optional<RdacaaRawRowResult> sa = primerApellidoRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("PrimerApellido debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_PRIMER_APELLIDO).getValidationResultList().isEmpty());
    }
    
    @Test
    public void whenPrimerApellidoIsExcludedAndValorIsNotNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("REDACAA");
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_PRIMER_APELLIDO, ritem);
        Optional<RdacaaRawRowResult> sa = primerApellidoRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("PrimerApellido debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_PRIMER_APELLIDO)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_PRIMER_APELLIDO_NO_DEFINIDO)
                        .isPresent());
    }
    
    @Test
    public void whenPrimerApellidoIsNotExcludedAndValorIsNotNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("REDACAA");
    	ritem.setExcludedFromValidation(false);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_PRIMER_APELLIDO, ritem);
        Optional<RdacaaRawRowResult> sa = primerApellidoRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("PrimerApellido debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_PRIMER_APELLIDO).getValidationResultList().isEmpty());
    }
	
	@Test
	public void whenPrimerApellidoIsNull_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue(null);
		ritem.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_PRIMER_APELLIDO, ritem);
		Optional<RdacaaRawRowResult> sa = primerApellidoRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Primer Apellido es obligatorio",
				sa.get().get(RdacaaVariableKeyCatalog.PERSONA_PRIMER_APELLIDO)
						.hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_PRIMER_APELLIDO_NULO)
						.isPresent());
	}

	@Test
	public void whenPrimerApellidoIsMenorTresCaracteres_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue("AC");
		ritem.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_PRIMER_APELLIDO, ritem);
		Optional<RdacaaRawRowResult> sa = primerApellidoRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Se espera un error para Primer Apellido menor a 3 caracteres",
				sa.get().get(RdacaaVariableKeyCatalog.PERSONA_PRIMER_APELLIDO)
						.hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_PRIMER_APELLIDO_LONG)
						.isPresent());
	}
	
	@Test
	public void whenPrimerApellidoIsMayorTresCaracteres_thenNoError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue("GARCIA");
		ritem.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_PRIMER_APELLIDO, ritem);
		Optional<RdacaaRawRowResult> sa = primerApellidoRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("No se espera notificaciones",
				sa.get().get(RdacaaVariableKeyCatalog.PERSONA_PRIMER_APELLIDO).getValidationResultList().isEmpty());
	}
}
