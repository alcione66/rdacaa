/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.odontologia.test;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.odontologia.obturado.ObturadosRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;
import java.util.Optional;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

/**
 *
 * @author eduardo
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class DientesOpturadosRuleGroupExecutorTest {
    @Autowired
    private ObturadosRuleGroupExecutor dientesObturadosRuleGroupExecutor;
    
    @Test
	public void whenNumeroDientesOBturadosIsNull_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue(null);
		ritem.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_OBTURADAS, ritem);
		Optional<RdacaaRawRowResult> sa = dientesObturadosRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El campo es obligatorio",
				sa.get().get(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_OBTURADAS)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_DIENTES_OBTURADOS_NULO)
						.isPresent());
	}
    
    @Test
	public void whenCodigoDientesOpturadosIsMandatoryAndCorrectValue() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem itemFechaNac = new RowItem();
		itemFechaNac.setItemValue("1989-07-03");
		itemFechaNac.setExcludedFromValidation(false);
		RowItem itemFechaAtenc = new RowItem();
		itemFechaAtenc.setItemValue("2018-10-11");
		itemFechaAtenc.setExcludedFromValidation(false);
		RowItem itemEspecialidad = new RowItem();
		itemEspecialidad.setItemValue("669"); // Odontologia
		itemEspecialidad.setExcludedFromValidation(false);
		RowItem mandatory = new RowItem();
		mandatory.setItemValue("5");
		mandatory.setExcludedFromValidation(false);
		mandatory.setMandatory(true);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, itemFechaNac);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, itemFechaAtenc);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_CODIGO_ESPECIALIDAD, itemEspecialidad);
		input.addField(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_OBTURADAS, mandatory);
		Optional<RdacaaRawRowResult> sa = dientesObturadosRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El valor es mandatorio",
				sa.get().get(RdacaaVariableKeyCatalog.ODONTOLOGIA_CANTIDAD_PIEZAS_OBTURADAS).getValidationResultList().isEmpty());
	}
}
