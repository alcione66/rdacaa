package ec.gob.msp.rdacaa.business.validation.rule.datosantropometricos.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.deliveredtechnologies.rulebook.Fact;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.valorhb.ValorHBRuleGroupExecutor;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.valorhbcorregido.ValorHBCorregidoRuleGroupExecutor;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.valorhbindicadoranemia.ValorHBIndicadorAnemiaRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class ValorHBIndicadorAnemiaCalculateRuleTest {

	@Autowired
	private ValorHBCorregidoRuleGroupExecutor valorHBCorregidoRuleGroupExecutor;

	@Autowired
	private ValorHBRuleGroupExecutor valorHBRuleGroupExecutor;

	@Autowired
	private ValorHBIndicadorAnemiaRuleGroupExecutor valorHBIndicadorAnemiaRuleGroupExecutor;

	@Test
	public void whenEdadSexoValorHBCorregidoWithErrors_thenError() {
		RdacaaRawRowResult resultTotal = new RdacaaRawRowResult();
		RdacaaRawRow input = new RdacaaRawRow();

		RowItem sexo = new RowItem();
		sexo.setItemValue("jj");// mujer
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);

		RowItem grupoPrioritarioEmbarazada = new RowItem();
		grupoPrioritarioEmbarazada.setItemValue("6748");
		grupoPrioritarioEmbarazada.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1, grupoPrioritarioEmbarazada);

		// Establecimiento Salud
		RowItem estSalud = new RowItem();
		estSalud.setItemValue("asasa");
		estSalud.setExcludedFromValidation(false);
		estSalud.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.ESTABLECIMENTO_SALUD_CODIGO, estSalud);

		// Valor HB
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("XXXX-03-01");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-XX");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem valorHB = new RowItem();
		valorHB.setItemValue("20");
		valorHB.setExcludedFromValidation(false);
		valorHB.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB, valorHB);

		Optional<RdacaaRawRowResult> sa1 = valorHBRuleGroupExecutor.execute(input);

		sa1.ifPresent(r -> {
			System.out.println(r.toString());
		});

		resultTotal.putAll(sa1.get());

		Fact resultTotalFact1 = new Fact("rdacaa_row_result", resultTotal);

		RowItem valorHBCorregido = new RowItem();
		valorHBCorregido.setItemValue("20");
		valorHBCorregido.setExcludedFromValidation(false);
		valorHBCorregido.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB_CORREGIDO, valorHBCorregido);

		Optional<RdacaaRawRowResult> sa2 = valorHBCorregidoRuleGroupExecutor.execute(input, resultTotalFact1);

		sa2.ifPresent(r -> {
			System.out.println(r.toString());
		});

		resultTotal.putAll(sa2.get());

		Fact resultTotalFact2 = new Fact("rdacaa_row_result", resultTotal);

		RowItem valorHBCorregidoIndicadorAnemia = new RowItem();
		valorHBCorregidoIndicadorAnemia.setItemValue("sdasa");
		valorHBCorregidoIndicadorAnemia.setExcludedFromValidation(false);
		valorHBCorregidoIndicadorAnemia.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_INDICADOR_ANEMIA, valorHBCorregidoIndicadorAnemia);

		Optional<RdacaaRawRowResult> sa3 = valorHBIndicadorAnemiaRuleGroupExecutor.execute(input, resultTotalFact2);

		sa3.ifPresent(r -> {
			System.out.println(r.toString());
		});

		Assert.assertTrue(
				"Se espera CODIGO_ERROR_DAT_ANT_CODIGO_INDICADOR_ANEMIA_FECHAS_FORMATO_INCORRECTO , "
						+ "CODIGO_ERROR_DAT_ANT_CODIGO_INDICADOR_ANEMIA_SEXO_INCORRECTO, "
						+ "CODIGO_ERROR_DAT_ANT_CODIGO_INDICADOR_ANEMIA_VALORHB_CORREGIDO_ERROR",
				sa3.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_INDICADOR_ANEMIA)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_CODIGO_INDICADOR_ANEMIA_FECHAS_FORMATO_INCORRECTO)
						.isPresent()
						&& sa3.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_INDICADOR_ANEMIA)
								.hasValidationResultByCode(
										ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_CODIGO_INDICADOR_ANEMIA_SEXO_INCORRECTO)
								.isPresent()
						&& sa3.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_INDICADOR_ANEMIA)
								.hasValidationResultByCode(
										ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_CODIGO_INDICADOR_ANEMIA_VALORHB_CORREGIDO_ERROR)
								.isPresent());

	}

	@Test
	public void whenEdadSexoValorHBCorregidoOK_thenNoError() {
		RdacaaRawRowResult resultTotal = new RdacaaRawRowResult();
		RdacaaRawRow input = new RdacaaRawRow();

		RowItem sexo = new RowItem();
		sexo.setItemValue("17");// mujer
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);

		RowItem grupoPrioritarioEmbarazada = new RowItem();
		grupoPrioritarioEmbarazada.setItemValue("634");
		grupoPrioritarioEmbarazada.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1, grupoPrioritarioEmbarazada);

		// Establecimiento Salud
		RowItem estSalud = new RowItem();
		estSalud.setItemValue("1746");
		estSalud.setExcludedFromValidation(false);
		estSalud.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.ESTABLECIMENTO_SALUD_CODIGO, estSalud);

		// Valor HB
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2000-01-01");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-01");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem valorHB = new RowItem();
		valorHB.setItemValue("20");
		valorHB.setExcludedFromValidation(false);
		valorHB.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB, valorHB);

		Optional<RdacaaRawRowResult> sa1 = valorHBRuleGroupExecutor.execute(input);

		sa1.ifPresent(r -> {
			System.out.println(r.toString());
		});

		resultTotal.putAll(sa1.get());

		Fact resultTotalFact1 = new Fact("rdacaa_row_result", resultTotal);

		RowItem valorHBCorregido = new RowItem();
		valorHBCorregido.setItemValue("20");
		valorHBCorregido.setExcludedFromValidation(false);
		valorHBCorregido.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_VALOR_HB_CORREGIDO, valorHBCorregido);

		Optional<RdacaaRawRowResult> sa2 = valorHBCorregidoRuleGroupExecutor.execute(input, resultTotalFact1);

		sa2.ifPresent(r -> {
			System.out.println(r.toString());
		});

		resultTotal.putAll(sa2.get());

		Fact resultTotalFact2 = new Fact("rdacaa_row_result", resultTotal);

		RowItem valorHBCorregidoIndicadorAnemia = new RowItem();
		valorHBCorregidoIndicadorAnemia.setItemValue("sdasa");
		valorHBCorregidoIndicadorAnemia.setExcludedFromValidation(false);
		valorHBCorregidoIndicadorAnemia.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_INDICADOR_ANEMIA, valorHBCorregidoIndicadorAnemia);

		Optional<RdacaaRawRowResult> sa3 = valorHBIndicadorAnemiaRuleGroupExecutor.execute(input, resultTotalFact2);

		sa3.ifPresent(r -> {
			System.out.println(r.toString());
		});

	}

}
