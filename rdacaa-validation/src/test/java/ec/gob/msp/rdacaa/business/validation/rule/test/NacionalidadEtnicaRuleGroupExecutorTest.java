package ec.gob.msp.rdacaa.business.validation.rule.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.persona.nacionalidadetnica.NacionalidadEtnicaRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

/**
*
* @author dmurillo
*/
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class NacionalidadEtnicaRuleGroupExecutorTest {
	
	@Autowired
	private NacionalidadEtnicaRuleGroupExecutor nacionalidadEtnicaRuleGroupExecutor;
	
	
	@Test
    public void whenNacionalidadEtnicaIsExcludedAndValorIsNull(){
		RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("999990000066666");
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD_ETNICA, ritem);
        Optional<RdacaaRawRowResult> sa = nacionalidadEtnicaRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Nacionalidad Etnica debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD_ETNICA).getValidationResultList().isEmpty());
	}
	
	@Test
    public void whenNacionalidadEtnicaIsExcludedAndValorIsNotNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("1");
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD_ETNICA, ritem);
        Optional<RdacaaRawRowResult> sa = nacionalidadEtnicaRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Nacionalidad debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD_ETNICA)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_NACIONALIDAD_ETNICA_NO_DEFINIDA)
                        .isPresent());
    }
    
	
	@Test
    public void whenNacionalidadIsNull_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue(null);
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD_ETNICA, ritem);
        Optional<RdacaaRawRowResult> sa = nacionalidadEtnicaRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("El campo es obligatorio",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD_ETNICA)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_NACIONALIDAD_ETNICA_NULO)
                        .isPresent());
    }
	
	@Test
    public void whenNacionalidadIsIncorrecto_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("300");
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD_ETNICA, ritem);
        Optional<RdacaaRawRowResult> sa = nacionalidadEtnicaRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("El campo es incorrecto",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_CODIGO_NACIONALIDAD_ETNICA)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_NACIONALIDAD_ETNICA_INCORRECTO)
                        .isPresent());
    }
}
