package ec.gob.msp.rdacaa.business.validation.rule.procedimiento.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.service.ConstantesDetalleCatalogo;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.actividad1.Actividades1RuleGroupExecutor;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.procedimientos.actividad5.Actividades5RuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class Actividades5RuleGroupExecutorTest {
	
	@Autowired
	private Actividades5RuleGroupExecutor actividades5RuleGroupExecutor;
	
	@Test
	public void whenActividades5IsNull_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue(null);
		ritem.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_5, ritem);
		Optional<RdacaaRawRowResult> sa = actividades5RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El campo es obligatorio",
				sa.get().get(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_5)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_ACTIVIDAD_5_NULO)
						.isPresent());
	}
	
	@Test
	public void whenActividades5IsExcludedAndValorIsNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
		ritem.setExcludedFromValidation(true);
		input.addField(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_5, ritem);
		Optional<RdacaaRawRowResult> sa = actividades5RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El numero de actividades debe ser nulo",
				sa.get().get(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_5)
						.getValidationResultList().isEmpty());
	}
	
	@Test
	public void whenActividades5IsMandatory() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
		ritem.setExcludedFromValidation(false);
		ritem.setMandatory(true);
		input.addField(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_5, ritem);
		Optional<RdacaaRawRowResult> sa = actividades5RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El Cantidad de actividades es mandatorio",
				sa.get().get(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_5)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_ACTIVIDAD_5_ES_MANDATORIO)
						.isPresent());
	}
	
	@Test
	public void whenActividades5IsNotNumeric_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue("asdfasdfasd");
		ritem.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_5, ritem);
		Optional<RdacaaRawRowResult> sa = actividades5RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Se espera un error para caracteres no numéricos ",
				sa.get().get(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_5)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_ACTIVIDAD_5_NO_NUMERICO)
						.isPresent());
	}
	
	@Test
	public void whenActividades5LongitudIncorrect_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue("9999");
		ritem.setExcludedFromValidation(false);

		input.addField(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_5, ritem);
		Optional<RdacaaRawRowResult> sa = actividades5RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Se espera un error longitud incorrecta", sa.get()
				.get(RdacaaVariableKeyCatalog.PROCEDIMIENTOS_CANTIDAD_5)
				.hasValidationResultByCode(
						ValidationResultCatalogConstants.CODIGO_ERROR_ACTIVIDAD_5_LONGITUD_INCORRECTA)
				.isPresent());
	}
}
