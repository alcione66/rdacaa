package ec.gob.msp.rdacaa.business.validation.rule.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.persona.identificacion.IdentificacionRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

/**
*
* @author dmurillo
*/
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class IdentificacionRuleGroupExecutorTest {

	@Autowired
	private IdentificacionRuleGroupExecutor identificacionRuleGroupExecutor;
	
	@Test
    public void whenIdentificacionIsExcludedAndValorIsNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("999990000066666");
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION, ritem);
        Optional<RdacaaRawRowResult> sa = identificacionRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("La identificacion debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION).getValidationResultList().isEmpty());
    }
    
    @Test
    public void whenIdentificacionIsExcludedAndValorIsNotNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("1715932115");
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION, ritem);
        Optional<RdacaaRawRowResult> sa = identificacionRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("La identificacion debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_IDENTIFICACION_NO_DEFINIDA)
                        .isPresent());
    }
    
    @Test
    public void whenIdentificacionIsNotExcludedAndValorIsNotNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("1715932115");
    	ritem.setExcludedFromValidation(false);
    	input.addField(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION, ritem);
        Optional<RdacaaRawRowResult> sa = identificacionRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("La identificacion debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION).getValidationResultList().isEmpty());
    }
	
	@Test
    public void whenIdentificacionIsNull_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue(null);
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION, ritem);
        Optional<RdacaaRawRowResult> sa = identificacionRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("La identificacion es obligatorio",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_IDENTIFICACION_NULO)
                        .isPresent());
    }
	
	@Test
    public void whenIdentificacionCedulaLongitudIncorrect_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritemTI = new RowItem();
        ritemTI.setItemValue("6");
        ritemTI.setExcludedFromValidation(false);
		RowItem ritem = new RowItem();
		ritem.setItemValue("111222333");
		ritem.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_IDENTIFICACION, ritemTI);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION, ritem);
        Optional<RdacaaRawRowResult> sa = identificacionRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Se espera un error longitud incorrecta",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_IDENTIFICACION_CEDULA_LONGITUD_INCORRECTA)
                        .isPresent());
	}
	
	@Test
    public void whenIdentificacionCedulaIsIncorrect_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritemTI = new RowItem();
        ritemTI.setItemValue("6");
        ritemTI.setExcludedFromValidation(false);
		RowItem ritem = new RowItem();
		ritem.setItemValue("1815432116");
		ritem.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_IDENTIFICACION, ritemTI);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION, ritem);
        Optional<RdacaaRawRowResult> sa = identificacionRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Se espera un error validacion incorrecta",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_IDENTIFICACION_INCORRECTO)
                        .isPresent());
	}
	
	@Test
    public void whenIdentificacionCedulaIsCorrect_thenOk() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritemTI = new RowItem();
        ritemTI.setItemValue("6");
        ritemTI.setExcludedFromValidation(false);
		RowItem ritem = new RowItem();
		ritem.setItemValue("1715932115");
		ritem.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_TIPO_IDENTIFICACION, ritemTI);
        input.addField(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION, ritem);
        Optional<RdacaaRawRowResult> sa = identificacionRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("No se espera errores",
                sa.get().get(RdacaaVariableKeyCatalog.PERSONA_IDENTIFICACION).getValidationResultList().isEmpty());
	}
	
}
