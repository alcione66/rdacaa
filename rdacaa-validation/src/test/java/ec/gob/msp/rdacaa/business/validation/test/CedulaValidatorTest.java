/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.test;

import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import ec.gob.msp.rdacaa.business.validation.CedulaValidator;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalog;

/**
 *
 * @author dmurillo
 */
public class CedulaValidatorTest {
    
    private CedulaValidator cedulaValidator;
    private static final String CEDULANONUMERICA = "1715AB2116";
    private static final String CEDULANUMERICA = "1715932115";
    
    @Before
    public void init(){
        cedulaValidator = new CedulaValidator();
    }
    
    @Test
    public void whenCaracteresNoNumericos_then_CedulaInvalida(){
        List<ValidationResult> listaErrores = cedulaValidator.validate(CEDULANONUMERICA);
         Optional<ValidationResult> errorCedula = listaErrores.parallelStream().filter(error -> {
            return error.equals(ValidationResultCatalog.CEDULA_INVALIDA.getValidationError());
        }).findAny();
        Assert.assertTrue("La cédula no puede ser válida", errorCedula.isPresent());
    } 
    
    @Test
    public void whenCaracteresNumericos_then_CedulaValida(){
        List<ValidationResult> listaErrores = cedulaValidator.validate(CEDULANUMERICA);
        Optional<ValidationResult> errorCedula = listaErrores.parallelStream().filter(error -> {
            return error.equals(ValidationResultCatalog.CEDULA_INVALIDA.getValidationError());
        }).findAny();
        Assert.assertTrue("La cédula es correcta", !errorCedula.isPresent());
    }
}
