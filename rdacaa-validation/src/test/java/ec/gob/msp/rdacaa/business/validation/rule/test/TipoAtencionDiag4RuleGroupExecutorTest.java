package ec.gob.msp.rdacaa.business.validation.rule.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.service.ConstantesDetalleCatalogo;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.tipoatencion4.TipoAtencionDiag4RuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

/**
 *
 * @author miguel.faubla
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class TipoAtencionDiag4RuleGroupExecutorTest {

	@Autowired
	private TipoAtencionDiag4RuleGroupExecutor tipoAtencionDiag4RuleGroupExecutor;

	@Test
	public void whenTipoAtencionDiag4IsExcludedAndValorIsNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem tipoatencionDiagId = new RowItem();
		tipoatencionDiagId.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
		tipoatencionDiagId.setExcludedFromValidation(true);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_ATENCION_4, tipoatencionDiagId);
		Optional<RdacaaRawRowResult> sa = tipoAtencionDiag4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El tipo de atencion_4 en pantalla de diagnosticos debe ser nulo", sa.get()
				.get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_ATENCION_4).getValidationResultList().isEmpty());
	}

	@Test
	public void whenTipoAtencionDiag4IsExcludedAndValorIsNotNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem tipoatencionDiagId = new RowItem();
		tipoatencionDiagId.setItemValue("710"); // Primera
		tipoatencionDiagId.setExcludedFromValidation(true);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_ATENCION_4, tipoatencionDiagId);
		Optional<RdacaaRawRowResult> sa = tipoAtencionDiag4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El tipo de atencion_4 en pantalla de diagnosticos no debe ser nulo",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_ATENCION_4)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_ATENCION_DIAGNOSTICO_4_NO_DEFINIDA)
						.isPresent());
	}

	@Test
	public void whenTipoAtencionDiag4IsNotExcludedAndValorIsNotNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem tipoatencionDiagId = new RowItem();
		tipoatencionDiagId.setItemValue("711"); // Subsecuente
		tipoatencionDiagId.setExcludedFromValidation(false);

		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_ATENCION_4, tipoatencionDiagId);
		Optional<RdacaaRawRowResult> sa = tipoAtencionDiag4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El tipo de atencion_4 en pantalla de diagnosticos no debe ser nulo", sa.get()
				.get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_ATENCION_4).getValidationResultList().isEmpty());
	}

	@Test
	public void whenTipoAtencionDiag4IsNull_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem tipoatencionDiagId = new RowItem();
		tipoatencionDiagId.setItemValue(null);
		tipoatencionDiagId.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_ATENCION_4, tipoatencionDiagId);
		Optional<RdacaaRawRowResult> sa = tipoAtencionDiag4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El campo es obligatorio",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_ATENCION_4)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_ATENCION_DIAGNOSTICO_4_NULO)
						.isPresent());
	}

	@Test
	public void whenTipoAtencionDiag4IsNotNumeric_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem tipoatencionDiagId = new RowItem();
		tipoatencionDiagId.setItemValue("ATENCION");
		tipoatencionDiagId.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_ATENCION_4, tipoatencionDiagId);
		Optional<RdacaaRawRowResult> sa = tipoAtencionDiag4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Se espera un error para caracteres no numéricos ",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_ATENCION_4)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_ATENCION_DIAGNOSTICO_4_NONUMERICO)
						.isPresent());
	}

	@Test
	public void whenTipoAtencionDiag4IsIncorrecto_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem tipoatencionDiagId = new RowItem();
		tipoatencionDiagId.setItemValue("228"); // Valor incorrecto
		tipoatencionDiagId.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_ATENCION_4, tipoatencionDiagId);
		Optional<RdacaaRawRowResult> sa = tipoAtencionDiag4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El campo es incorrecto",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_ATENCION_4)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_ATENCION_DIAGNOSTICO_4_INCORRECTO)
						.isPresent());
	}

	@Test
	public void whenTipoAtencionDiag4IsCorrect_thenOk() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem tipoatencionDiagId = new RowItem();
		tipoatencionDiagId.setItemValue("710"); // Primera
		tipoatencionDiagId.setExcludedFromValidation(false);

		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_ATENCION_4, tipoatencionDiagId);
		Optional<RdacaaRawRowResult> sa = tipoAtencionDiag4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("No se esperan errores para tipo de atencion_4 en pantalla de diagnosticos correcto", sa.get()
				.get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_ATENCION_4).getValidationResultList().isEmpty());
	}

	@Test
	public void whenTipoAtencionDiag4IsMandatory() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem tipoatencionDiagId = new RowItem();
		tipoatencionDiagId.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
		tipoatencionDiagId.setExcludedFromValidation(false);
		tipoatencionDiagId.setMandatory(true);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_ATENCION_4, tipoatencionDiagId);
		Optional<RdacaaRawRowResult> sa = tipoAtencionDiag4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El tipo de atencion_4 en pantalla de diagnosticos es mandatorio",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_ATENCION_4)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_TIPO_ATENCION_DIAGNOSTICO_4_ES_MANDATORIA)
						.isPresent());
	}

	@Test
	public void whenTipoAtencionDiag4IsMandatoryAndCorrectValue() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem tipoatencionDiagId = new RowItem();
		tipoatencionDiagId.setItemValue("710"); // Primera
		tipoatencionDiagId.setExcludedFromValidation(false);
		tipoatencionDiagId.setMandatory(true);

		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_ATENCION_4, tipoatencionDiagId);
		Optional<RdacaaRawRowResult> sa = tipoAtencionDiag4RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El tipo de atencion_4 en pantalla de diagnosticos es mandatorio", sa.get()
				.get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_TIPO_ATENCION_4).getValidationResultList().isEmpty());
	}

}
