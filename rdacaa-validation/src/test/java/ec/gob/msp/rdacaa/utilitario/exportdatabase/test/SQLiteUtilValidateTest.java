package ec.gob.msp.rdacaa.utilitario.exportdatabase.test;

import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import com.google.crypto.tink.config.TinkConfig;

import ec.gob.msp.rdacaa.business.validation.database.common.CipheringPublicKeyPlainDataFunction;
import ec.gob.msp.rdacaa.business.validation.database.common.DecipheringLocalCipheredDataFunction;
import ec.gob.msp.rdacaa.business.validation.database.common.DecipheringWithPrivateKeyDataFunction;
import ec.gob.msp.rdacaa.business.validation.database.common.MapperFunction;
import ec.gob.msp.rdacaa.business.validation.database.common.PreparingResultsFunction;
import ec.gob.msp.rdacaa.business.validation.database.common.ValidatorFunction;
import ec.gob.msp.rdacaa.seguridades.util.SeguridadUtil;
import ec.gob.msp.rdacaa.test.ValidationTestContext;
import ec.gob.msp.rdacaa.utilitario.exportdatabase.CipheredDataParams;
import ec.gob.msp.rdacaa.utilitario.exportdatabase.SQLiteUtil;

@Ignore
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class SQLiteUtilValidateTest {

	private static final String USERNAME = "1715927586";
	private static final String PASSWORD = "Msp2018*";
	private static final Logger logger = LoggerFactory.getLogger(SQLiteUtilValidateTest.class);
	private byte[] KEY = { 8, 20, 14, -76, 47, 97, -81, -8, -108, 31, 57, -34, -46, 23, -48, 28, 40, -4, -30, -20, 40,
			-21, -39, -66, 83, 35, 27, 92, 78, -50, -68, 47, 89, 85, -102, 125, -51, -80, -69, 115, -120, -106, -67, 4,
			101, 36, -51, 37, 13, 33, -33, -45, -101, 79, -16, -63, 30, -4, 114, -86, -76, -103, -111, 38, 92, 53, -72,
			43, 22, -124, 35, 34, -70, 116, 96, -13, -106, 1, -103, -93, -36, 80, -61, -57, -119, 114, -97, -33, 29,
			-49, 40, 66, -12, 85, 16, 26, 62, -90, -4, 65, -34, 71, 22, -55, 83, -82, 119, -128, 66, -109, 92, 73, 119,
			60, -57, -23, -42, 3, -127, 69, -77, 4, 95, 42, -74, 52, -65, -113, 73, -31, -47, 92, 10, 31, 24, -66, -126,
			81, 43, 37, -68, 47, 16, 31, 89, 17 };
	private byte[] SALT = { 116, 29, 104, 64, -10, -114, 36, 87, 100, -44, 98, 107, 109, 50, 37, -69 };
	private byte[] CIPHERED_KEY = { 8, -39, -79, -65, -94, 4, 18, 110, 10, 98, 10, 58, 116, 121, 112, 101, 46, 103, 111,
			111, 103, 108, 101, 97, 112, 105, 115, 46, 99, 111, 109, 47, 103, 111, 111, 103, 108, 101, 46, 99, 114, 121,
			112, 116, 111, 46, 116, 105, 110, 107, 46, 67, 104, 97, 67, 104, 97, 50, 48, 80, 111, 108, 121, 49, 51, 48,
			53, 75, 101, 121, 18, 34, 18, 32, -15, -84, 107, -115, -32, -90, 68, 55, 39, 27, -3, 25, 25, 83, 32, -84,
			-85, 23, -124, 115, -61, 33, 4, 25, -56, -30, -66, 74, 115, 77, -55, -44, 24, 1, 16, 1, 24, -39, -79, -65,
			-94, 4, 32, 1 };

	@Autowired
	private MapperFunction mapperFunction;

	private DecipheringLocalCipheredDataFunction decipheringLocalCipheredDataFunction = new DecipheringLocalCipheredDataFunction();

	@Autowired
	private ValidatorFunction validatorFunction;

	@Autowired
	private PreparingResultsFunction preparingResultsFunction;

	@Autowired
	private CipheringPublicKeyPlainDataFunction cipheringPublicKeyPlainDataFunction;

	private DecipheringWithPrivateKeyDataFunction decipheringWithPrivateKeyDataFunction = new DecipheringWithPrivateKeyDataFunction();

	@Before
	public void init() throws GeneralSecurityException {
		// Initialize the config.
		TinkConfig.register();
	}

	@Test
	public void whenDataInputLocalCiphered_thenValidateAndExport() throws IOException, GeneralSecurityException {

		ClassLoader classLoader = getClass().getClassLoader();

		File fileArchivoAValidar = new File(classLoader.getResource("validateTestDataLocalCiphered.rdacaa").getFile());

		String pathDirArchivoAValidar = fileArchivoAValidar.getAbsolutePath();

		logger.info(pathDirArchivoAValidar);

		byte[] cipheredKey = SeguridadUtil.descifrarConPasswordYSalt(PASSWORD, SALT, KEY, USERNAME);

		Object[] credentials = { cipheredKey, USERNAME };
		
		CipheredDataParams cipheredDataParams = new CipheredDataParams(credentials, true, false, null);

		File validatedFile = SQLiteUtil.exportAfterValidationMeshCipheredFile(pathDirArchivoAValidar, decipheringLocalCipheredDataFunction,
				mapperFunction, validatorFunction, preparingResultsFunction, cipheredDataParams);
		
		boolean exists = (null != validatedFile && validatedFile.exists());
		Assert.assertTrue("Se espera que la base se cree en la ruta ingresada", exists);
	}

	@Test
	public void whenDataInputPrivateKeyCiphered_thenValidateAndExport() throws IOException, GeneralSecurityException {

		ClassLoader classLoader = getClass().getClassLoader();

		File fileArchivoAValidar = new File(classLoader.getResource("validateTestDataPrivateKeyCiphered.rdacaa").getFile());

		String pathDirArchivoAValidar = fileArchivoAValidar.getAbsolutePath();

		String privatekeyPath = "/opt/rdacaa/privatekey/private-keyset.cfg";

		logger.info(pathDirArchivoAValidar);
		logger.info(privatekeyPath);

		CipheredDataParams cipheredDataParams = new CipheredDataParams(null, false, true, privatekeyPath);

		File validatedFile = SQLiteUtil.exportAfterValidationMeshCipheredFile(pathDirArchivoAValidar, decipheringWithPrivateKeyDataFunction,
				mapperFunction, validatorFunction, preparingResultsFunction, cipheredDataParams);

		boolean exists = (null != validatedFile && validatedFile.exists());
		Assert.assertTrue("Se espera que la base se cree en la ruta ingresada", exists);
	}

	@Test
	public void whenDataInputPlain_thenValidateAndExport() throws IOException, GeneralSecurityException {

		ClassLoader classLoader = getClass().getClassLoader();

		File fileArchivoAValidar = new File(classLoader.getResource("validateTestDataPlain.rdacaa").getFile());

		String pathDirArchivoAValidar = fileArchivoAValidar.getAbsolutePath();

		logger.info(pathDirArchivoAValidar);

		File validatedFile = SQLiteUtil.exportAfterValidationMeshPlainFile(pathDirArchivoAValidar,
				cipheringPublicKeyPlainDataFunction, mapperFunction,
				validatorFunction, preparingResultsFunction);

		boolean exists = (null != validatedFile && validatedFile.exists());
		Assert.assertTrue("Se espera que la base se cree en la ruta ingresada", exists);
	}

}
