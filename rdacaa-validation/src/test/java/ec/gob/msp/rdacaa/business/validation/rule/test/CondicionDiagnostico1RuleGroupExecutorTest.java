/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.rule.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.service.ConstantesDetalleCatalogo;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.cie10.condiciondiagnostico1.CondicionDiagnostico1RuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

/**
 *
 * @author miguel.faubla
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class CondicionDiagnostico1RuleGroupExecutorTest {
	@Autowired
	private CondicionDiagnostico1RuleGroupExecutor condicionDiagnostico1RuleGroupExecutor;

	@Test
	public void whenCondicionDiagnostico1IsExcludedAndValorIsNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem condiciondiagnosticoId = new RowItem();
		condiciondiagnosticoId.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
		condiciondiagnosticoId.setExcludedFromValidation(true);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CONDICION_DIAG_1, condiciondiagnosticoId);
		Optional<RdacaaRawRowResult> sa = condicionDiagnostico1RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("La condicion de diagnosticos1 debe ser nulo", sa.get()
				.get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CONDICION_DIAG_1).getValidationResultList().isEmpty());
	}

	@Test
	public void whenCondicionDiagnostico1IsExcludedAndValorIsNotNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem condiciondiagnosticoId = new RowItem();
		condiciondiagnosticoId.setItemValue("224"); // Definitivo Inicial
		condiciondiagnosticoId.setExcludedFromValidation(true);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CONDICION_DIAG_1, condiciondiagnosticoId);
		Optional<RdacaaRawRowResult> sa = condicionDiagnostico1RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("La condicion de diagnosticos1 no debe ser nulo",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CONDICION_DIAG_1)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_CONDICION_DIAGNOSTICO_1_NO_DEFINIDA)
						.isPresent());
	}

	@Test
	public void whenCondicionDiagnostico1IsNotExcludedAndValorIsNotNull() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem condiciondiagnosticoId = new RowItem();
		condiciondiagnosticoId.setItemValue("225"); // Definitivo Inicial Confirmado por Laboratorio
		condiciondiagnosticoId.setExcludedFromValidation(false);

		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CONDICION_DIAG_1, condiciondiagnosticoId);
		Optional<RdacaaRawRowResult> sa = condicionDiagnostico1RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("La condicion de diagnosticos1 no debe ser nulo", sa.get()
				.get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CONDICION_DIAG_1).getValidationResultList().isEmpty());
	}

	@Test
	public void whenCondicionDiagnostico1IsNull_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem condiciondiagnosticoId = new RowItem();
		condiciondiagnosticoId.setItemValue(null);
		condiciondiagnosticoId.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CONDICION_DIAG_1, condiciondiagnosticoId);
		Optional<RdacaaRawRowResult> sa = condicionDiagnostico1RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El campo es obligatorio",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CONDICION_DIAG_1)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_CONDICION_DIAGNOSTICO_1_NULO)
						.isPresent());
	}

	@Test
	public void whenCondicionDiagnostico1IsNotNumeric_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem condiciondiagnosticoId = new RowItem();
		condiciondiagnosticoId.setItemValue("CON_DIAGNOSTICO");
		condiciondiagnosticoId.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CONDICION_DIAG_1, condiciondiagnosticoId);
		Optional<RdacaaRawRowResult> sa = condicionDiagnostico1RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Se espera un error para caracteres no numéricos ",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CONDICION_DIAG_1)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_CONDICION_DIAGNOSTICO_1_NONUMERICO)
						.isPresent());
	}

	@Test
	public void whenCondicionDiagnostico1IsIncorrecto_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem condiciondiagnosticoId = new RowItem();
		condiciondiagnosticoId.setItemValue("183"); // Valor no aplicable
		condiciondiagnosticoId.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CONDICION_DIAG_1, condiciondiagnosticoId);
		Optional<RdacaaRawRowResult> sa = condicionDiagnostico1RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El campo es incorrecto",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CONDICION_DIAG_1)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_CONDICION_DIAGNOSTICO_1_INCORRECTO)
						.isPresent());
	}

	@Test
	public void whenCondicionDiagnostico1IsCorrect_thenOk() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem condiciondiagnosticoId = new RowItem();
		condiciondiagnosticoId.setItemValue("226"); // Definitivo Control
		condiciondiagnosticoId.setExcludedFromValidation(false);

		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CONDICION_DIAG_1, condiciondiagnosticoId);
		Optional<RdacaaRawRowResult> sa = condicionDiagnostico1RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("No se esperan errores para condicion de diagnosticos1 en pantalla de diagnosticos correcto",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CONDICION_DIAG_1).getValidationResultList()
						.isEmpty());
	}

	@Test
	public void whenCondicionDiagnostico1IsMandatory() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem condiciondiagnosticoId = new RowItem();
		condiciondiagnosticoId.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
		condiciondiagnosticoId.setExcludedFromValidation(false);
		condiciondiagnosticoId.setMandatory(true);
		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CONDICION_DIAG_1, condiciondiagnosticoId);
		Optional<RdacaaRawRowResult> sa = condicionDiagnostico1RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("La condicion de diagnosticos1 en pantalla de diagnosticos es mandatorio",
				sa.get().get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CONDICION_DIAG_1)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_CONDICION_DIAGNOSTICO_1_ES_MANDATORIA)
						.isPresent());
	}
	
	@Test
	public void whenCondicionDiagnostico1IsMandatoryAndCorrectValue() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem condiciondiagnosticoId = new RowItem();
		condiciondiagnosticoId.setItemValue("223"); // Presuntivo
		condiciondiagnosticoId.setExcludedFromValidation(false);
		condiciondiagnosticoId.setMandatory(true);

		input.addField(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CONDICION_DIAG_1, condiciondiagnosticoId);
		Optional<RdacaaRawRowResult> sa = condicionDiagnostico1RuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("La condicion de diagnosticos1 en pantalla de diagnosticos es mandatorio", sa.get()
				.get(RdacaaVariableKeyCatalog.DIAGNOSTICO_CODIGO_CONDICION_DIAG_1).getValidationResultList().isEmpty());
	}

}
