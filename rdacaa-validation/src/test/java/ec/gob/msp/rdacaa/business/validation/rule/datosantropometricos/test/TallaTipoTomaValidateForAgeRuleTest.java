package ec.gob.msp.rdacaa.business.validation.rule.datosantropometricos.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.tallatipotoma.TallaTipoTomaRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class TallaTipoTomaValidateForAgeRuleTest {

	private static final String DE_PIE = "0";
	private static final String ACOSTADO = "1";

	@Autowired
	private TallaTipoTomaRuleGroupExecutor tallaTipoTomaRuleGroupExecutor;

	@Test
	public void whenEdadMenor010601thenValidateAcostado_thenNoError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2018-05-06");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-23");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem talla = new RowItem();
		talla.setItemValue("999990000066666");
		talla.setExcludedFromValidation(false);
		talla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, talla);
		RowItem tipoTomaTalla = new RowItem();
		tipoTomaTalla.setItemValue(ACOSTADO);
		tipoTomaTalla.setExcludedFromValidation(false);
		tipoTomaTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA, tipoTomaTalla);

		Optional<RdacaaRawRowResult> sa = tallaTipoTomaRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El valor de prueba es correcto no debe existir errores",
				!sa.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA).hasErrors());
	}

	@Test
	public void whenEdadMenor010601thenValidateAcostado_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2018-05-06");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-23");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem talla = new RowItem();
		talla.setItemValue("999990000066666");
		talla.setExcludedFromValidation(false);
		talla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, talla);
		RowItem tipoTomaTalla = new RowItem();
		tipoTomaTalla.setItemValue("999990000066666");
		tipoTomaTalla.setExcludedFromValidation(false);
		tipoTomaTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA, tipoTomaTalla);

		Optional<RdacaaRawRowResult> sa = tallaTipoTomaRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El valor de prueba es correcto no debe existir errores", sa.get()
				.get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA)
				.hasValidationResultByCode(
						ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TIPO_TOMA_MEDIDA_TALLA_ES_MANDATORIA_MENOR_050000)
				.isPresent());
	}

	@Test
	public void whenEdadMenor010601DePiethenValidateAcostado_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2018-05-06");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-23");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem talla = new RowItem();
		talla.setItemValue("999990000066666");
		talla.setExcludedFromValidation(false);
		talla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, talla);
		RowItem tipoTomaTalla = new RowItem();
		tipoTomaTalla.setItemValue(DE_PIE);
		tipoTomaTalla.setExcludedFromValidation(false);
		tipoTomaTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA, tipoTomaTalla);

		Optional<RdacaaRawRowResult> sa = tallaTipoTomaRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El valor de prueba es correcto no debe existir errores", sa.get()
				.get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA)
				.hasValidationResultByCode(
						ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TIPO_TOMA_MEDIDA_TALLA_ACOSTADO_MENOR_010601_DEBE_SER_ACOSTADO)
				.isPresent());
	}

	@Test
	public void whenEdadMenor010601TallaNulathenValidateAcostado_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2018-05-06");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-23");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem talla = new RowItem();
		talla.setItemValue(null);
		talla.setExcludedFromValidation(false);
		talla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, talla);
		RowItem tipoTomaTalla = new RowItem();
		tipoTomaTalla.setItemValue(ACOSTADO);
		tipoTomaTalla.setExcludedFromValidation(false);
		tipoTomaTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA, tipoTomaTalla);

		Optional<RdacaaRawRowResult> sa = tallaTipoTomaRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue(
				"Debe existir el error CODIGO_ERROR_DAT_ANT_TIPO_TOMA_MEDIDA_TALLA_VALOR_TALLA_NO_PUEDE_SER_NULO",
				sa.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TIPO_TOMA_MEDIDA_TALLA_VALOR_TALLA_NO_PUEDE_SER_NULO)
						.isPresent());
	}

	@Test
	public void whenEdadMenor010601FechasInvalidasthenValidateAcostado_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("XXXX-05-06");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-XX");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem talla = new RowItem();
		talla.setItemValue("100");
		talla.setExcludedFromValidation(false);
		talla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, talla);
		RowItem tipoTomaTalla = new RowItem();
		tipoTomaTalla.setItemValue(ACOSTADO);
		tipoTomaTalla.setExcludedFromValidation(false);
		tipoTomaTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA, tipoTomaTalla);

		Optional<RdacaaRawRowResult> sa = tallaTipoTomaRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Debe existir el error CODIGO_ERROR_DAT_ANT_TIPO_TOMA_MEDIDA_TALLA_FECHAS_FORMATO_INCORRECTO",
				sa.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TIPO_TOMA_MEDIDA_TALLA_FECHAS_FORMATO_INCORRECTO)
						.isPresent());
	}

	@Test
	public void whenEdadMenor050000YMayor010601thenValidateDePie_thenNoError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2014-12-01");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-12-01");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem talla = new RowItem();
		talla.setItemValue("999990000066666");
		talla.setExcludedFromValidation(false);
		talla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, talla);
		RowItem tipoTomaTalla = new RowItem();
		tipoTomaTalla.setItemValue(DE_PIE);
		tipoTomaTalla.setExcludedFromValidation(false);
		tipoTomaTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA, tipoTomaTalla);

		Optional<RdacaaRawRowResult> sa = tallaTipoTomaRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El valor de prueba es correcto no debe existir errores",
				!sa.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA).hasErrors());
	}

	@Test
	public void whenEdadMenor050000YMayor010601NoDefinidothenValidateAcostado_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2014-12-01");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-12-01");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem talla = new RowItem();
		talla.setItemValue("999990000066666");
		talla.setExcludedFromValidation(false);
		talla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, talla);
		RowItem tipoTomaTalla = new RowItem();
		tipoTomaTalla.setItemValue("999990000066666");
		tipoTomaTalla.setExcludedFromValidation(false);
		tipoTomaTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA, tipoTomaTalla);

		Optional<RdacaaRawRowResult> sa = tallaTipoTomaRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Debe existir error CODIGO_ERROR_DAT_ANT_TIPO_TOMA_MEDIDA_TALLA_ES_MANDATORIA_MENOR_050000",
				sa.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TIPO_TOMA_MEDIDA_TALLA_ES_MANDATORIA_MENOR_050000)
						.isPresent());
	}

	@Test
	public void whenEdadMenor050000YMayor010601SoloDePieOAcostadothenValidateAcostado_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2014-12-01");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-12-01");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem talla = new RowItem();
		talla.setItemValue("999990000066666");
		talla.setExcludedFromValidation(false);
		talla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, talla);
		RowItem tipoTomaTalla = new RowItem();
		tipoTomaTalla.setItemValue("3");
		tipoTomaTalla.setExcludedFromValidation(false);
		tipoTomaTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA, tipoTomaTalla);

		Optional<RdacaaRawRowResult> sa = tallaTipoTomaRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Debe existir error CODIGO_ERROR_DAT_ANT_TIPO_TOMA_MEDIDA_TALLA_SOLO_DE_PIE_ACOSTADO", sa
				.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA)
				.hasValidationResultByCode(
						ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TIPO_TOMA_MEDIDA_TALLA_SOLO_DE_PIE_ACOSTADO)
				.isPresent());
	}

	@Test
	public void whenEdadMenor050000YMayor010601thenValidateAcostado_thenNoError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2014-12-01");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-12-01");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem talla = new RowItem();
		talla.setItemValue("999990000066666");
		talla.setExcludedFromValidation(false);
		talla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, talla);
		RowItem tipoTomaTalla = new RowItem();
		tipoTomaTalla.setItemValue(ACOSTADO);
		tipoTomaTalla.setExcludedFromValidation(false);
		tipoTomaTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA, tipoTomaTalla);

		Optional<RdacaaRawRowResult> sa = tallaTipoTomaRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El valor de prueba es correcto no debe existir errores",
				!sa.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA).hasErrors());
	}

	@Test
	public void whenEdadMayorIgual050000YMenor50100thenValidateAcostadoODePie_thenNoError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2013-12-01");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-12-01");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem talla = new RowItem();
		talla.setItemValue("999990000066666");
		talla.setExcludedFromValidation(false);
		talla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, talla);
		RowItem tipoTomaTalla = new RowItem();
		tipoTomaTalla.setItemValue("999990000066666");
		tipoTomaTalla.setExcludedFromValidation(false);
		tipoTomaTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA, tipoTomaTalla);

		Optional<RdacaaRawRowResult> sa = tallaTipoTomaRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El valor de prueba es correcto no debe existir errores",
				!sa.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA).hasErrors());
	}

	@Test
	public void whenEdadMayor50100TallaNoDefinidathenValidateDePie_thenNoError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2013-12-01");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-12-02");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem talla = new RowItem();
		talla.setItemValue("999990000066666");
		talla.setExcludedFromValidation(false);
		talla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, talla);
		RowItem tipoTomaTalla = new RowItem();
		tipoTomaTalla.setItemValue(DE_PIE);
		tipoTomaTalla.setExcludedFromValidation(false);
		tipoTomaTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA, tipoTomaTalla);

		Optional<RdacaaRawRowResult> sa = tallaTipoTomaRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El valor de prueba es correcto no debe existir errores",
				!sa.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA).hasErrors());
	}

	@Test
	public void whenEdadMayor50100TallaConValorthenValidateDePie_thenNoError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2013-12-01");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-12-02");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem talla = new RowItem();
		talla.setItemValue("100");
		talla.setExcludedFromValidation(false);
		talla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, talla);
		RowItem tipoTomaTalla = new RowItem();
		tipoTomaTalla.setItemValue(DE_PIE);
		tipoTomaTalla.setExcludedFromValidation(false);
		tipoTomaTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA, tipoTomaTalla);

		Optional<RdacaaRawRowResult> sa = tallaTipoTomaRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El valor de prueba es correcto no debe existir errores",
				!sa.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA).hasErrors());
	}

	@Test
	public void whenEdadMayor50100NoDepieTallaConValorthenValidateDePie_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2010-12-01");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-12-02");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem talla = new RowItem();
		talla.setItemValue("100");
		talla.setExcludedFromValidation(false);
		talla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, talla);
		RowItem tipoTomaTalla = new RowItem();
		tipoTomaTalla.setItemValue(ACOSTADO);
		tipoTomaTalla.setExcludedFromValidation(false);
		tipoTomaTalla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA, tipoTomaTalla);

		Optional<RdacaaRawRowResult> sa = tallaTipoTomaRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Debe existir error CODIGO_ERROR_DAT_ANT_TIPO_TOMA_MEDIDA_TALLA_MAYOR_050100_ES_DE_PIE", sa.get()
				.get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_CODIGO_TIPO_TOMA_MEDIDA_TALLA)
				.hasValidationResultByCode(
						ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TIPO_TOMA_MEDIDA_TALLA_MAYOR_050100_ES_DE_PIE)
				.isPresent());
	}

}
