package ec.gob.msp.rdacaa.business.validation.rule.referencia.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.service.ConstantesDetalleCatalogo;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.referencia.subsistema.SubsistemaReferenciaRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class ReferenciaSubsistemaRuleGroupExecutorTest {
	
	@Autowired
    private SubsistemaReferenciaRuleGroupExecutor subsistemaReferenciaRuleGroupExecutor;
	
	@Test
    public void whenReferenciaSubsistemaIsExcludedAndValorIsNull(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
    	ritem.setExcludedFromValidation(true);
    	input.addField(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_SUBSISTEMA, ritem);
        Optional<RdacaaRawRowResult> sa = subsistemaReferenciaRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Codigo para subsistema de referencia debe ser nulo",
                sa.get().get(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_SUBSISTEMA).getValidationResultList().isEmpty());
    }
	
	@Test
    public void whenReferenciaSubsistemaIsNull_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue(null);
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_SUBSISTEMA, ritem);
        Optional<RdacaaRawRowResult> sa = subsistemaReferenciaRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Pueblos es obligatorio",
                sa.get().get(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_SUBSISTEMA)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_REFERENCIA_SUBSISTEMA_NULO)
                        .isPresent());
    }
	
	@Test
	public void whenSubsistemaReferenciaIsMandatory() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem ritem = new RowItem();
		ritem.setItemValue(ConstantesDetalleCatalogo.NOT_DEFINED_VALUE);
		ritem.setExcludedFromValidation(false);
		ritem.setMandatory(true);
		input.addField(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_SUBSISTEMA, ritem);
		Optional <RdacaaRawRowResult> sa = subsistemaReferenciaRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {System.out.println(r.toString());});
		Assert.assertTrue("El codigo de subsistema es mandatorio",
				sa.get().get(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_SUBSISTEMA)
				.hasValidationResultByCode(
						ValidationResultCatalogConstants.CODIGO_ERROR_REFERENCIA_SUBSISTEMA_ES_MANDATORIA)
				.isPresent()
				);
	}
	
	@Test
    public void whenSubsistemaReferenciaIsMadatoryAndCorrectValue(){
    	RdacaaRawRow input = new RdacaaRawRow();
    	RowItem ritem = new RowItem();
    	ritem.setItemValue("2585");
    	ritem.setExcludedFromValidation(false);
    	ritem.setMandatory(true);
    	input.addField(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_SUBSISTEMA, ritem);
        Optional<RdacaaRawRowResult> sa = subsistemaReferenciaRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("El codigo de subsistema es mandatorio",
                sa.get().get(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_SUBSISTEMA).getValidationResultList().isEmpty());
    }
	
	@Test
    public void whenSubsistemaReferenciaIsNotNumeric_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("dfasdf");
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_SUBSISTEMA, ritem);
        Optional<RdacaaRawRowResult> sa = subsistemaReferenciaRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Se espera un error para caracteres no numericos ",
                sa.get().get(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_SUBSISTEMA)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_REFERENCIA_SUBSISTEMA_NO_NUMERICO)
                        .isPresent());
    }
	
    @Test
    public void whenSubsistemaReferenciaIsNotCorrect_thenError() {
        RdacaaRawRow input = new RdacaaRawRow();
        RowItem ritem = new RowItem();
        ritem.setItemValue("666");
        ritem.setExcludedFromValidation(false);
        input.addField(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_SUBSISTEMA, ritem);
        Optional<RdacaaRawRowResult> sa = subsistemaReferenciaRuleGroupExecutor.execute(input);
        sa.ifPresent(r -> {
            System.out.println(r.toString());
        });
        Assert.assertTrue("Se espera un error para codigo incorrecto ",
                sa.get().get(RdacaaVariableKeyCatalog.REFERENCIA_CONTRAREFERENCIA_CODIGO_SUBSISTEMA)
                        .hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_REFERENCIA_SUBSISTEMA_INCORRECTO)
                        .isPresent());
    }

}
