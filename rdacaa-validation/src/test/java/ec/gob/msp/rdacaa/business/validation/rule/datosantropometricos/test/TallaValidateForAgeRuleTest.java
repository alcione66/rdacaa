package ec.gob.msp.rdacaa.business.validation.rule.datosantropometricos.test;

import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalogConstants;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRow;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaRawRowResult;
import ec.gob.msp.rdacaa.business.validation.malla.RdacaaVariableKeyCatalog;
import ec.gob.msp.rdacaa.business.validation.malla.RowItem;
import ec.gob.msp.rdacaa.business.validation.rule.atencion.datosantropometricos.talla.TallaRuleGroupExecutor;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class TallaValidateForAgeRuleTest {

	@Autowired
	private TallaRuleGroupExecutor tallaRuleGroupExecutor;

	@Test
	public void whenEdadMenor050000AndIsNotDefined_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem sexo = new RowItem();
		sexo.setItemValue("16");// hombre
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2018-05-06");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2019-11-23");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem talla = new RowItem();
		talla.setItemValue("999990000066666");
		talla.setExcludedFromValidation(false);
		talla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, talla);

		Optional<RdacaaRawRowResult> sa = tallaRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El peso es obligatorio para edad menor de 5 años",
				sa.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA).hasValidationResultByCode(
						ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TALLA_ES_MANDATORIA_MENOR_050100_ANIOS_O_EMBARAZADA)
						.isPresent());
	}

	@Test
	public void whenEdadMenor050000AndDateWithError_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem sexo = new RowItem();
		sexo.setItemValue("16");// hombre
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2018-05-XX");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("XXXX-11-23");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem talla = new RowItem();
		talla.setItemValue("999990000066666");
		talla.setExcludedFromValidation(false);
		talla.setMandatory(false);

		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, talla);

		Optional<RdacaaRawRowResult> sa = tallaRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Se espera error para fechas incorrectas",
				sa.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TALLA_FECHAS_FORMATO_INCORRECTO)
						.isPresent());
	}

	@Test
	public void whenEdadMenor050000AndSexoWithError_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem sexo = new RowItem();
		sexo.setItemValue("36");// hombre
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2018-05-06");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-23");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem talla = new RowItem();
		talla.setItemValue("999990000066666");
		talla.setExcludedFromValidation(false);
		talla.setMandatory(false);

		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, talla);

		Optional<RdacaaRawRowResult> sa = tallaRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Se espera error para fechas incorrectas",
				sa.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TALLA_SEXO_INCORRECTO)
						.isPresent());
	}

	@Test
	public void whenIsMujerEmbarazada_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem sexo = new RowItem();
		sexo.setItemValue("17");// mujer
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		RowItem grupoPrioritarioEmbarazada = new RowItem();
		grupoPrioritarioEmbarazada.setItemValue("634");
		grupoPrioritarioEmbarazada.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1, grupoPrioritarioEmbarazada);
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2000-05-06");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-23");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem talla = new RowItem();
		talla.setItemValue("999990000066666");
		talla.setExcludedFromValidation(false);
		talla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, talla);

		Optional<RdacaaRawRowResult> sa = tallaRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El peso es obligatorio para mujeres embarazadas",
				sa.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA).hasValidationResultByCode(
						ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TALLA_ES_MANDATORIA_MENOR_050100_ANIOS_O_EMBARAZADA)
						.isPresent());
	}

	@Test
	public void whenIsMujerEmbarazadaValorCorrecto_thenNoError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem sexo = new RowItem();
		sexo.setItemValue("17");// mujer
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		RowItem grupoPrioritarioEmbarazada = new RowItem();
		grupoPrioritarioEmbarazada.setItemValue("634");
		grupoPrioritarioEmbarazada.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1, grupoPrioritarioEmbarazada);
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2000-05-06");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-23");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem talla = new RowItem();
		talla.setItemValue("60");
		talla.setExcludedFromValidation(false);
		talla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, talla);

		Optional<RdacaaRawRowResult> sa = tallaRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("El valor de prueba es correcto no debe existir errores",
				!sa.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA).hasErrors());
	}

	@Test
	public void whenIsMujerEmbarazadaValorNoNumerico_thenNoError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem sexo = new RowItem();
		sexo.setItemValue("17");// mujer
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		RowItem grupoPrioritarioEmbarazada = new RowItem();
		grupoPrioritarioEmbarazada.setItemValue("634");
		grupoPrioritarioEmbarazada.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.GRUPO_PRIORITARIO_1, grupoPrioritarioEmbarazada);
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2000-05-06");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-23");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem talla = new RowItem();
		talla.setItemValue("tere");
		talla.setExcludedFromValidation(false);
		talla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, talla);

		Optional<RdacaaRawRowResult> sa = tallaRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Debe existir error CODIGO_ERROR_DAT_ANT_TALLA_NO_ES_NUMERO_VALIDO",
				sa.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA).hasValidationResultByCode(
						ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TALLA_NO_ES_NUMERO_VALIDO).isPresent());
	}

	@Test
	public void whenEdadMenor050000ValorCorrecto_thenWarning() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem sexo = new RowItem();
		sexo.setItemValue("17");// mujer
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2015-05-06");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-23");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem talla = new RowItem();
		talla.setItemValue("80");
		talla.setExcludedFromValidation(false);
		talla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, talla);

		Optional<RdacaaRawRowResult> sa = tallaRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue(
				"Para los parametros de la prueba debe exitir la ADV CODIGO_WARN_DAT_ANT_TALLA_FUERA_RANGO_PARA_EDAD",
				sa.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA)
						.hasValidationResultByCode(
								ValidationResultCatalogConstants.CODIGO_ADV_DAT_ANT_TALLA_FUERA_RANGO_PARA_EDAD)
						.isPresent() && !sa.get().get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA).hasErrors());
	}

	@Test
	public void whenEdadMenor050000ValorFueraRango_thenError() {
		RdacaaRawRow input = new RdacaaRawRow();
		RowItem sexo = new RowItem();
		sexo.setItemValue("17");// mujer
		sexo.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_CODIGO_SEXO, sexo);
		RowItem fechaNac = new RowItem();
		fechaNac.setItemValue("2015-05-06");
		fechaNac.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.PERSONA_FECHA_NACIMIENTO, fechaNac);
		RowItem fechaAten = new RowItem();
		fechaAten.setItemValue("2018-11-23");
		fechaAten.setExcludedFromValidation(false);
		input.addField(RdacaaVariableKeyCatalog.ATENCION_FECHA, fechaAten);
		RowItem talla = new RowItem();
		talla.setItemValue("600");
		talla.setExcludedFromValidation(false);
		talla.setMandatory(false);
		input.addField(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA, talla);

		Optional<RdacaaRawRowResult> sa = tallaRuleGroupExecutor.execute(input);
		sa.ifPresent(r -> {
			System.out.println(r.toString());
		});
		Assert.assertTrue("Para los parametros de la prueba debe exitir CODIGO_ERROR_DAT_ANT_PESO_FUERA_RANGO", sa.get()
				.get(RdacaaVariableKeyCatalog.DATO_ANTROPOMETRICO_TALLA)
				.hasValidationResultByCode(ValidationResultCatalogConstants.CODIGO_ERROR_DAT_ANT_TALLA_FUERA_RANGO)
				.isPresent());
	}

}
