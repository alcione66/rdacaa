/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.validation.test;

import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.validation.TallaValidator;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalog;
import ec.gob.msp.rdacaa.test.ValidationTestContext;

/**
 *
 * @author Saulo Velasco
 */
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = ValidationTestContext.class)
public class TallaValidatorTest {

    @Autowired
    private TallaValidator tallaValidator;
    private String TALLA_MAX_FUERA_RANGO = "300.0";
    private String TALLA_MIN_FUERA_RANGO = "20.0";

    @Test
    public void whenTallaNullOrEmpty_thenPesoVacioError() {
        String[] tallaEdadSexo = {"", "050600", "F"};
        List<ValidationResult> listaErrores = tallaValidator.validate(tallaEdadSexo);
        Optional<ValidationResult> error = listaErrores.parallelStream().filter(e -> {
            return e.getCode().equals(ValidationResultCatalog.TALLA_VACIA.getValidationError().getCode());

        }).findAny();
        Assert.assertTrue("Se espera un error para talla nula", error.isPresent());
    }

    /**
     * Min 142.86 Max 183.42 Edad Min 18-08-00 Edad Max 18-08-30 F
     */
    @Test
    public void whenMujerTallaFueraRangoEdad_thenError() {
        String[] tallaEdadSexo = {"130.00", "180800", "F"};
        List<ValidationResult> listaErrores = tallaValidator.validate(tallaEdadSexo);
        Optional<ValidationResult> error = listaErrores.parallelStream().filter(e -> {
            return e.getCode().equals(ValidationResultCatalog.TALLA_FUERA_RANGO_PARA_EDAD.getValidationError().getCode());

        }).findAny();
        Assert.assertTrue("Se espera un error TALLA_FUERA_RANGO_PARA_EDAD para talla valor fuera de rango", error.isPresent());
    }

    /**
     * Min 142.86 Max 183.42 Edad Min 18-08-00 Edad Max 18-08-30 F
     */
    @Test
    public void whenMujerTallaDentroRangoEdad_thenNoError() {
        String[] tallaEdadSexo = {"150.00", "180800", "F"};
        List<ValidationResult> listaErrores = tallaValidator.validate(tallaEdadSexo);
        Assert.assertTrue("No se esperan errores para valor dentro de rango", listaErrores.isEmpty());
    }

    /**
     * Cuando edad mayor a 18-11-30 entonces error para talla >=
     * TALLA_MIN_FUERA_RANGO 20.0 y peso <= TALLA_MAX_FUERA_RANGO 300.0 fuera de
     * rango
     */
    @Test
    public void whenAllSexoMayorEdadFueraRangoPeso_thenError() {
        String[] tallaEdadSexo = {"400.00", "190000", "F"};
        List<ValidationResult> listaErrores = tallaValidator.validate(tallaEdadSexo);
        Optional<ValidationResult> error = listaErrores.parallelStream().filter(e -> {
            return e.getCode().equals(ValidationResultCatalog.TALLA_FUERA_RANGO_PARA_EDAD.getValidationError().getCode());

        }).findAny();
        Assert.assertTrue("Se espera un error TALLA_FUERA_RANGO_PARA_EDAD para peso valor fuera de rango >= "
                + TALLA_MIN_FUERA_RANGO + " y <= " + TALLA_MAX_FUERA_RANGO, error.isPresent());
    }

    /**
     * Cuando edad mayor a 18-11-30 entonces error para talla >=
     * TALLA_MIN_FUERA_RANGO 20.0 y peso <= TALLA_MAX_FUERA_RANGO 300.0 fuera de
     * rango
     */
    @Test
    public void whenAllSexoMayorEdadDentroRangoPeso_thenNoError() {
        String[] tallaEdadSexo = {"220.00", "190000", "F"};
        List<ValidationResult> listaErrores = tallaValidator.validate(tallaEdadSexo);
        Assert.assertTrue("No se espera error para rango >= "
                + TALLA_MAX_FUERA_RANGO + " y <= " + TALLA_MAX_FUERA_RANGO, listaErrores.isEmpty());
    }
}
