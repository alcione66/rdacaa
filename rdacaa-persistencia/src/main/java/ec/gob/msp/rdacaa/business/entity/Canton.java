package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "canton")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Canton.findAll", query = "SELECT c FROM Canton c") })
public class Canton implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", nullable = false)
	private Integer id;
	@Basic(optional = false)
	@Column(name = "descripcion", nullable = false, length = 100)
	private String descripcion;
	@Column(name = "codcanton", length = 4)
	private String codcanton;
	@Column(name = "coddistrito", length = 5)
	private String coddistrito;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "cantonId")
	private List<Parroquia> parroquiaList;
	@JoinColumn(name = "provincia_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Provincia provinciaId;

	public Canton() {
	}

	public Canton(Integer id) {
		this.id = id;
	}

	public Canton(Integer id, String descripcion) {
		this.id = id;
		this.descripcion = descripcion;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getCodcanton() {
		return codcanton;
	}

	public void setCodcanton(String codcanton) {
		this.codcanton = codcanton;
	}

	public String getCoddistrito() {
		return coddistrito;
	}

	public void setCoddistrito(String coddistrito) {
		this.coddistrito = coddistrito;
	}

	@XmlTransient
	public List<Parroquia> getParroquiaList() {
		return parroquiaList;
	}

	public void setParroquiaList(List<Parroquia> parroquiaList) {
		this.parroquiaList = parroquiaList;
	}

	public Provincia getProvinciaId() {
		return provinciaId;
	}

	public void setProvinciaId(Provincia provinciaId) {
		this.provinciaId = provinciaId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Canton other = (Canton) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ec.gob.msp.rdacaa.business.entity.Canton[ id=" + id + " ]";
	}

}
