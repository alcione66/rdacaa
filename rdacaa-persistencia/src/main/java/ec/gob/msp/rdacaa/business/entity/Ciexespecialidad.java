package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "ciexespecialidad", uniqueConstraints = { @UniqueConstraint(columnNames = { "id" }) })
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Ciexespecialidad.findAll", query = "SELECT c FROM Ciexespecialidad c") })
public class Ciexespecialidad implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@Basic(optional = false)
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "estado")
	private Integer estado;
	@JoinColumn(name = "cie_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Cie cieId;
	@JoinColumn(name = "ctespecialidad_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Detallecatalogo ctespecialidadId;

	public Ciexespecialidad() {
	}

	public Ciexespecialidad(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public Cie getCieId() {
		return cieId;
	}

	public void setCieId(Cie cieId) {
		this.cieId = cieId;
	}

	public Detallecatalogo getCtespecialidadId() {
		return ctespecialidadId;
	}

	public void setCtespecialidadId(Detallecatalogo ctespecialidadId) {
		this.ctespecialidadId = ctespecialidadId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Ciexespecialidad other = (Ciexespecialidad) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ec.gob.msp.rdacaa.business.entity.Ciexespecialidad[ id=" + id + " ]";
	}

}
