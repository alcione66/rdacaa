//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.01.07 a las 12:19:14 PM ECT 
//


package ec.gob.msp.rdacaa.seguridades.xml;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Clase Java para anonymous complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="infoEntidad">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/>
 *                   &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="ruc" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="nombreOficial" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="nombreComercial" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="entidadId" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/>
 *                   &lt;element name="direccion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="direccionreferencia" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="numestablecimiento" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
 *                   &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                   &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                   &lt;element name="parroquia_id" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/>
 *                   &lt;element name="z17s" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/>
 *                   &lt;element name="tipoestablecimiento_id" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
 *                   &lt;element name="tipoestablecimiento_descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="perfilPersona">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="persona" maxOccurs="unbounded">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/>
 *                             &lt;element name="catalogoTipoIdentificacion" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
 *                             &lt;element name="numeroIdentificacion" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/>
 *                             &lt;element name="noIdentificado" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
 *                             &lt;element name="apellidoPaterno" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="apellidoMaterno" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="primerNombre" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="segundoNombre" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="nombreCompleto" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="pais" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="parroquia" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="fechaNacimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                             &lt;element name="catalogoSexo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                             &lt;element name="usuarioId" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/>
 *                             &lt;element name="usuario" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/>
 *                             &lt;element name="estadoUsuario" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
 *                             &lt;element name="cambiarClave" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
 *                             &lt;element name="fechaCreacionUsuario" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *                             &lt;element name="especialidad" maxOccurs="unbounded" minOccurs="0">
 *                               &lt;complexType>
 *                                 &lt;complexContent>
 *                                   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                                     &lt;sequence>
 *                                       &lt;element name="especialidad" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/>
 *                                       &lt;element name="descripcionEspecialidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *                                     &lt;/sequence>
 *                                   &lt;/restriction>
 *                                 &lt;/complexContent>
 *                               &lt;/complexType>
 *                             &lt;/element>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "infoEntidad",
    "perfilPersona"
})
@XmlRootElement(name = "profileUserPras")
public class ProfileUserPras {

    @XmlElement(required = true)
    protected ProfileUserPras.InfoEntidad infoEntidad;
    @XmlElement(required = true)
    protected ProfileUserPras.PerfilPersona perfilPersona;

    /**
     * Obtiene el valor de la propiedad infoEntidad.
     * 
     * @return
     *     possible object is
     *     {@link ProfileUserPras.InfoEntidad }
     *     
     */
    public ProfileUserPras.InfoEntidad getInfoEntidad() {
        return infoEntidad;
    }

    /**
     * Define el valor de la propiedad infoEntidad.
     * 
     * @param value
     *     allowed object is
     *     {@link ProfileUserPras.InfoEntidad }
     *     
     */
    public void setInfoEntidad(ProfileUserPras.InfoEntidad value) {
        this.infoEntidad = value;
    }

    /**
     * Obtiene el valor de la propiedad perfilPersona.
     * 
     * @return
     *     possible object is
     *     {@link ProfileUserPras.PerfilPersona }
     *     
     */
    public ProfileUserPras.PerfilPersona getPerfilPersona() {
        return perfilPersona;
    }

    /**
     * Define el valor de la propiedad perfilPersona.
     * 
     * @param value
     *     allowed object is
     *     {@link ProfileUserPras.PerfilPersona }
     *     
     */
    public void setPerfilPersona(ProfileUserPras.PerfilPersona value) {
        this.perfilPersona = value;
    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/>
     *         &lt;element name="codigo" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="ruc" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="nombreOficial" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="nombreComercial" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="entidadId" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/>
     *         &lt;element name="direccion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="direccionreferencia" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="numestablecimiento" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
     *         &lt;element name="telefono" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *         &lt;element name="email" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *         &lt;element name="parroquia_id" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/>
     *         &lt;element name="z17s" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/>
     *         &lt;element name="tipoestablecimiento_id" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
     *         &lt;element name="tipoestablecimiento_descripcion" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "id",
        "codigo",
        "ruc",
        "nombreOficial",
        "nombreComercial",
        "entidadId",
        "direccion",
        "direccionreferencia",
        "numestablecimiento",
        "telefono",
        "email",
        "parroquiaId",
        "z17S",
        "tipoestablecimientoId",
        "tipoestablecimientoDescripcion"
    })
    public static class InfoEntidad {

        @XmlSchemaType(name = "unsignedShort")
        protected int id;
        @XmlElement(required = true)
        protected String codigo;
        @XmlElement(required = true)
        protected String ruc;
        @XmlElement(required = true)
        protected String nombreOficial;
        @XmlElement(required = true)
        protected String nombreComercial;
        @XmlSchemaType(name = "unsignedShort")
        protected int entidadId;
        @XmlElement(required = true)
        protected String direccion;
        @XmlElement(required = true)
        protected String direccionreferencia;
        @XmlSchemaType(name = "unsignedByte")
        protected String numestablecimiento;
        @XmlElement(required = true)
        protected String telefono;
        @XmlElement(required = true)
        protected String email;
        @XmlElement(name = "parroquia_id")
        @XmlSchemaType(name = "unsignedShort")
        protected int parroquiaId;
        @XmlElement(name = "z17s")
        @XmlSchemaType(name = "unsignedShort")
        protected int z17S;
        @XmlElement(name = "tipoestablecimiento_id")
        @XmlSchemaType(name = "unsignedByte")
        protected int tipoestablecimientoId;
        @XmlElement(name = "tipoestablecimiento_descripcion", required = true)
        protected String tipoestablecimientoDescripcion;

        /**
         * Obtiene el valor de la propiedad id.
         * 
         */
        public int getId() {
            return id;
        }

        /**
         * Define el valor de la propiedad id.
         * 
         */
        public void setId(int value) {
            this.id = value;
        }

        /**
         * Obtiene el valor de la propiedad codigo.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public String getCodigo() {
            return codigo;
        }

        /**
         * Define el valor de la propiedad codigo.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setCodigo(String value) {
            this.codigo = value;
        }

        /**
         * Obtiene el valor de la propiedad ruc.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public String getRuc() {
            return ruc;
        }

        /**
         * Define el valor de la propiedad ruc.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setRuc(String value) {
            this.ruc = value;
        }

        /**
         * Obtiene el valor de la propiedad nombreOficial.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNombreOficial() {
            return nombreOficial;
        }

        /**
         * Define el valor de la propiedad nombreOficial.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNombreOficial(String value) {
            this.nombreOficial = value;
        }

        /**
         * Obtiene el valor de la propiedad nombreComercial.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public String getNombreComercial() {
            return nombreComercial;
        }

        /**
         * Define el valor de la propiedad nombreComercial.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setNombreComercial(String value) {
            this.nombreComercial = value;
        }

        /**
         * Obtiene el valor de la propiedad entidadId.
         * 
         */
        public int getEntidadId() {
            return entidadId;
        }

        /**
         * Define el valor de la propiedad entidadId.
         * 
         */
        public void setEntidadId(int value) {
            this.entidadId = value;
        }

        /**
         * Obtiene el valor de la propiedad direccion.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDireccion() {
            return direccion;
        }

        /**
         * Define el valor de la propiedad direccion.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDireccion(String value) {
            this.direccion = value;
        }

        /**
         * Obtiene el valor de la propiedad direccionreferencia.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public String getDireccionreferencia() {
            return direccionreferencia;
        }

        /**
         * Define el valor de la propiedad direccionreferencia.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setDireccionreferencia(String value) {
            this.direccionreferencia = value;
        }

        /**
         * Obtiene el valor de la propiedad numestablecimiento.
         * 
         */
        public String getNumestablecimiento() {
            return numestablecimiento;
        }

        /**
         * Define el valor de la propiedad numestablecimiento.
         * 
         */
        public void setNumestablecimiento(String value) {
            this.numestablecimiento = value;
        }

        /**
         * Obtiene el valor de la propiedad telefono.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTelefono() {
            return telefono;
        }

        /**
         * Define el valor de la propiedad telefono.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTelefono(String value) {
            this.telefono = value;
        }

        /**
         * Obtiene el valor de la propiedad email.
         * 
         * @return
         *     possible object is
         *     {@link Object }
         *     
         */
        public String getEmail() {
            return email;
        }

        /**
         * Define el valor de la propiedad email.
         * 
         * @param value
         *     allowed object is
         *     {@link Object }
         *     
         */
        public void setEmail(String value) {
            this.email = value;
        }

        /**
         * Obtiene el valor de la propiedad parroquiaId.
         * 
         */
        public int getParroquiaId() {
            return parroquiaId;
        }

        /**
         * Define el valor de la propiedad parroquiaId.
         * 
         */
        public void setParroquiaId(int value) {
            this.parroquiaId = value;
        }

        /**
         * Obtiene el valor de la propiedad z17S.
         * 
         */
        public int getZ17S() {
            return z17S;
        }

        /**
         * Define el valor de la propiedad z17S.
         * 
         */
        public void setZ17S(int value) {
            this.z17S = value;
        }

        /**
         * Obtiene el valor de la propiedad tipoestablecimientoId.
         * 
         */
        public int getTipoestablecimientoId() {
            return tipoestablecimientoId;
        }

        /**
         * Define el valor de la propiedad tipoestablecimientoId.
         * 
         */
        public void setTipoestablecimientoId(int value) {
            this.tipoestablecimientoId = value;
        }

        /**
         * Obtiene el valor de la propiedad tipoestablecimientoDescripcion.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTipoestablecimientoDescripcion() {
            return tipoestablecimientoDescripcion;
        }

        /**
         * Define el valor de la propiedad tipoestablecimientoDescripcion.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTipoestablecimientoDescripcion(String value) {
            this.tipoestablecimientoDescripcion = value;
        }

    }


    /**
     * <p>Clase Java para anonymous complex type.
     * 
     * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="persona" maxOccurs="unbounded">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/>
     *                   &lt;element name="catalogoTipoIdentificacion" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
     *                   &lt;element name="numeroIdentificacion" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/>
     *                   &lt;element name="noIdentificado" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
     *                   &lt;element name="apellidoPaterno" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="apellidoMaterno" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="primerNombre" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="segundoNombre" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="nombreCompleto" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="pais" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="parroquia" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="fechaNacimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                   &lt;element name="catalogoSexo" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                   &lt;element name="usuarioId" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/>
     *                   &lt;element name="usuario" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/>
     *                   &lt;element name="estadoUsuario" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
     *                   &lt;element name="cambiarClave" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
     *                   &lt;element name="fechaCreacionUsuario" type="{http://www.w3.org/2001/XMLSchema}date"/>
     *                   &lt;element name="especialidad" maxOccurs="unbounded" minOccurs="0">
     *                     &lt;complexType>
     *                       &lt;complexContent>
     *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                           &lt;sequence>
     *                             &lt;element name="especialidad" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/>
     *                             &lt;element name="descripcionEspecialidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
     *                           &lt;/sequence>
     *                         &lt;/restriction>
     *                       &lt;/complexContent>
     *                     &lt;/complexType>
     *                   &lt;/element>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "persona"
    })
    public static class PerfilPersona {

        @XmlElement(required = true)
        protected List<ProfileUserPras.PerfilPersona.Persona> persona;

        /**
         * Gets the value of the persona property.
         * 
         * <p>
         * This accessor method returns a reference to the live list,
         * not a snapshot. Therefore any modification you make to the
         * returned list will be present inside the JAXB object.
         * This is why there is not a <CODE>set</CODE> method for the persona property.
         * 
         * <p>
         * For example, to add a new item, do as follows:
         * <pre>
         *    getPersona().add(newItem);
         * </pre>
         * 
         * 
         * <p>
         * Objects of the following type(s) are allowed in the list
         * {@link ProfileUserPras.PerfilPersona.Persona }
         * 
         * 
         */
        public List<ProfileUserPras.PerfilPersona.Persona> getPersona() {
            if (persona == null) {
                persona = new ArrayList<ProfileUserPras.PerfilPersona.Persona>();
            }
            return this.persona;
        }


        /**
         * <p>Clase Java para anonymous complex type.
         * 
         * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="id" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/>
         *         &lt;element name="catalogoTipoIdentificacion" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
         *         &lt;element name="numeroIdentificacion" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/>
         *         &lt;element name="noIdentificado" type="{http://www.w3.org/2001/XMLSchema}anyType"/>
         *         &lt;element name="apellidoPaterno" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="apellidoMaterno" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="primerNombre" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="segundoNombre" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="nombreCompleto" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="pais" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="parroquia" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="fechaNacimiento" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *         &lt;element name="catalogoSexo" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *         &lt;element name="usuarioId" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/>
         *         &lt;element name="usuario" type="{http://www.w3.org/2001/XMLSchema}unsignedInt"/>
         *         &lt;element name="estadoUsuario" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
         *         &lt;element name="cambiarClave" type="{http://www.w3.org/2001/XMLSchema}unsignedByte"/>
         *         &lt;element name="fechaCreacionUsuario" type="{http://www.w3.org/2001/XMLSchema}date"/>
         *         &lt;element name="especialidad" maxOccurs="unbounded" minOccurs="0">
         *           &lt;complexType>
         *             &lt;complexContent>
         *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *                 &lt;sequence>
         *                   &lt;element name="especialidad" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/>
         *                   &lt;element name="descripcionEspecialidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
         *                 &lt;/sequence>
         *               &lt;/restriction>
         *             &lt;/complexContent>
         *           &lt;/complexType>
         *         &lt;/element>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "id",
            "catalogoTipoIdentificacion",
            "numeroIdentificacion",
            "noIdentificado",
            "apellidoPaterno",
            "apellidoMaterno",
            "primerNombre",
            "segundoNombre",
            "nombreCompleto",
            "pais",
            "parroquia",
            "fechaNacimiento",
            "catalogoSexo",
            "usuarioId",
            "usuario",
            "estadoUsuario",
            "cambiarClave",
            "fechaCreacionUsuario",
            "especialidad"
        })
        public static class Persona {

            @XmlSchemaType(name = "unsignedInt")
            protected long id;
            @XmlSchemaType(name = "unsignedByte")
            protected int catalogoTipoIdentificacion;
            @XmlSchemaType(name = "unsignedInt")
            protected String numeroIdentificacion;
            @XmlElement(required = true)
            protected Object noIdentificado;
            @XmlElement(required = true)
            protected String apellidoPaterno;
            @XmlElement(required = true)
            protected String apellidoMaterno;
            @XmlElement(required = true)
            protected String primerNombre;
            @XmlElement(required = true)
            protected String segundoNombre;
            @XmlElement(required = true)
            protected String nombreCompleto;
            @XmlElement(required = true)
            protected int pais;
            @XmlElement(required = true)
            protected String parroquia;
            @XmlElement(required = true)
            @XmlSchemaType(name = "date")
            protected String fechaNacimiento;
            @XmlElement(required = true)
            protected int catalogoSexo;
            @XmlSchemaType(name = "unsignedShort")
            protected int usuarioId;
            protected String usuario;
            protected int estadoUsuario;
            protected int cambiarClave;
            @XmlElement(required = true)
            @XmlSchemaType(name = "date")
            protected String fechaCreacionUsuario;
            protected List<ProfileUserPras.PerfilPersona.Persona.Especialidad> especialidad;

            /**
             * Obtiene el valor de la propiedad id.
             * 
             */
            public long getId() {
                return id;
            }

            /**
             * Define el valor de la propiedad id.
             * 
             */
            public void setId(long value) {
                this.id = value;
            }

            /**
             * Obtiene el valor de la propiedad catalogoTipoIdentificacion.
             * 
             */
            public int getCatalogoTipoIdentificacion() {
                return catalogoTipoIdentificacion;
            }

            /**
             * Define el valor de la propiedad catalogoTipoIdentificacion.
             * 
             */
            public void setCatalogoTipoIdentificacion(int value) {
                this.catalogoTipoIdentificacion = value;
            }

            /**
             * Obtiene el valor de la propiedad numeroIdentificacion.
             * 
             */
            public String getNumeroIdentificacion() {
                return numeroIdentificacion;
            }

            /**
             * Define el valor de la propiedad numeroIdentificacion.
             * 
             */
            public void setNumeroIdentificacion(String value) {
                this.numeroIdentificacion = value;
            }

            /**
             * Obtiene el valor de la propiedad noIdentificado.
             * 
             * @return
             *     possible object is
             *     {@link Object }
             *     
             */
            public Object getNoIdentificado() {
                return noIdentificado;
            }

            /**
             * Define el valor de la propiedad noIdentificado.
             * 
             * @param value
             *     allowed object is
             *     {@link Object }
             *     
             */
            public void setNoIdentificado(Object value) {
                this.noIdentificado = value;
            }

            /**
             * Obtiene el valor de la propiedad apellidoPaterno.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getApellidoPaterno() {
                return apellidoPaterno;
            }

            /**
             * Define el valor de la propiedad apellidoPaterno.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setApellidoPaterno(String value) {
                this.apellidoPaterno = value;
            }

            /**
             * Obtiene el valor de la propiedad apellidoMaterno.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getApellidoMaterno() {
                return apellidoMaterno;
            }

            /**
             * Define el valor de la propiedad apellidoMaterno.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setApellidoMaterno(String value) {
                this.apellidoMaterno = value;
            }

            /**
             * Obtiene el valor de la propiedad primerNombre.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getPrimerNombre() {
                return primerNombre;
            }

            /**
             * Define el valor de la propiedad primerNombre.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPrimerNombre(String value) {
                this.primerNombre = value;
            }

            /**
             * Obtiene el valor de la propiedad segundoNombre.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getSegundoNombre() {
                return segundoNombre;
            }

            /**
             * Define el valor de la propiedad segundoNombre.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setSegundoNombre(String value) {
                this.segundoNombre = value;
            }

            /**
             * Obtiene el valor de la propiedad nombreCompleto.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getNombreCompleto() {
                return nombreCompleto;
            }

            /**
             * Define el valor de la propiedad nombreCompleto.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setNombreCompleto(String value) {
                this.nombreCompleto = value;
            }

            /**
             * Obtiene el valor de la propiedad pais.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public int getPais() {
                return pais;
            }

            /**
             * Define el valor de la propiedad pais.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setPais(int value) {
                this.pais = value;
            }

            /**
             * Obtiene el valor de la propiedad parroquia.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getParroquia() {
                return parroquia;
            }

            /**
             * Define el valor de la propiedad parroquia.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setParroquia(String value) {
                this.parroquia = value;
            }

            /**
             * Obtiene el valor de la propiedad fechaNacimiento.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public String getFechaNacimiento() {
                return fechaNacimiento;
            }

            /**
             * Define el valor de la propiedad fechaNacimiento.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setFechaNacimiento(String value) {
                this.fechaNacimiento = value;
            }

            /**
             * Obtiene el valor de la propiedad catalogoSexo.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public int getCatalogoSexo() {
                return catalogoSexo;
            }

            /**
             * Define el valor de la propiedad catalogoSexo.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setCatalogoSexo(int value) {
                this.catalogoSexo = value;
            }

            /**
             * Obtiene el valor de la propiedad usuarioId.
             * 
             */
            public int getUsuarioId() {
                return usuarioId;
            }

            /**
             * Define el valor de la propiedad usuarioId.
             * 
             */
            public void setUsuarioId(int value) {
                this.usuarioId = value;
            }

            /**
             * Obtiene el valor de la propiedad usuario.
             * 
             */
            public String getUsuario() {
                return usuario;
            }

            /**
             * Define el valor de la propiedad usuario.
             * 
             */
            public void setUsuario(String value) {
                this.usuario = value;
            }

            /**
             * Obtiene el valor de la propiedad estadoUsuario.
             * 
             */
            public int getEstadoUsuario() {
                return estadoUsuario;
            }

            /**
             * Define el valor de la propiedad estadoUsuario.
             * 
             */
            public void setEstadoUsuario(int value) {
                this.estadoUsuario = value;
            }

            /**
             * Obtiene el valor de la propiedad cambiarClave.
             * 
             */
            public int getCambiarClave() {
                return cambiarClave;
            }

            /**
             * Define el valor de la propiedad cambiarClave.
             * 
             */
            public void setCambiarClave(int value) {
                this.cambiarClave = value;
            }

            /**
             * Obtiene el valor de la propiedad fechaCreacionUsuario.
             * 
             * @return
             *     possible object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public String getFechaCreacionUsuario() {
                return fechaCreacionUsuario;
            }

            /**
             * Define el valor de la propiedad fechaCreacionUsuario.
             * 
             * @param value
             *     allowed object is
             *     {@link XMLGregorianCalendar }
             *     
             */
            public void setFechaCreacionUsuario(String value) {
                this.fechaCreacionUsuario = value;
            }

            /**
             * Gets the value of the especialidad property.
             * 
             * <p>
             * This accessor method returns a reference to the live list,
             * not a snapshot. Therefore any modification you make to the
             * returned list will be present inside the JAXB object.
             * This is why there is not a <CODE>set</CODE> method for the especialidad property.
             * 
             * <p>
             * For example, to add a new item, do as follows:
             * <pre>
             *    getEspecialidad().add(newItem);
             * </pre>
             * 
             * 
             * <p>
             * Objects of the following type(s) are allowed in the list
             * {@link ProfileUserPras.PerfilPersona.Persona.Especialidad }
             * 
             * 
             */
            public List<ProfileUserPras.PerfilPersona.Persona.Especialidad> getEspecialidad() {
                if (especialidad == null) {
                    especialidad = new ArrayList<ProfileUserPras.PerfilPersona.Persona.Especialidad>();
                }
                return this.especialidad;
            }


            /**
             * <p>Clase Java para anonymous complex type.
             * 
             * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
             * 
             * <pre>
             * &lt;complexType>
             *   &lt;complexContent>
             *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
             *       &lt;sequence>
             *         &lt;element name="especialidad" type="{http://www.w3.org/2001/XMLSchema}unsignedShort"/>
             *         &lt;element name="descripcionEspecialidad" type="{http://www.w3.org/2001/XMLSchema}string"/>
             *       &lt;/sequence>
             *     &lt;/restriction>
             *   &lt;/complexContent>
             * &lt;/complexType>
             * </pre>
             * 
             * 
             */
            @XmlAccessorType(XmlAccessType.FIELD)
            @XmlType(name = "", propOrder = {
                "especialidad",
                "descripcionEspecialidad"
            })
            public static class Especialidad {

                @XmlSchemaType(name = "unsignedShort")
                protected int especialidad;
                @XmlElement(required = true)
                protected String descripcionEspecialidad;

                /**
                 * Obtiene el valor de la propiedad especialidad.
                 * 
                 */
                public int getEspecialidad() {
                    return especialidad;
                }

                /**
                 * Define el valor de la propiedad especialidad.
                 * 
                 */
                public void setEspecialidad(int value) {
                    this.especialidad = value;
                }

                /**
                 * Obtiene el valor de la propiedad descripcionEspecialidad.
                 * 
                 * @return
                 *     possible object is
                 *     {@link String }
                 *     
                 */
                public String getDescripcionEspecialidad() {
                    return descripcionEspecialidad;
                }

                /**
                 * Define el valor de la propiedad descripcionEspecialidad.
                 * 
                 * @param value
                 *     allowed object is
                 *     {@link String }
                 *     
                 */
                public void setDescripcionEspecialidad(String value) {
                    this.descripcionEspecialidad = value;
                }

            }

        }

    }

}
