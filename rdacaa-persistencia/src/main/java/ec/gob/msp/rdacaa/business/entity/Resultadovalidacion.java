package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "resultadovalidacion", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"id"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Resultadovalidacion.findAll", query = "SELECT r FROM Resultadovalidacion r")})
public class Resultadovalidacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "uuidatencion", length = 250)
    private String uuidatencion;
    @Column(name = "notificaciondescripcion", length = 250)
    private String notificaciondescripcion;
    @Column(name = "valor", length = 50)
    private String valor;
    @JoinColumn(name = "notificacion_id", referencedColumnName = "id")
    @ManyToOne
    private Notificacion notificacionId;
    @JoinColumn(name = "variable_id", referencedColumnName = "id")
    @ManyToOne
    private Variable variableId;

    public Resultadovalidacion() {
    }

    public Resultadovalidacion(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUuidatencion() {
        return uuidatencion;
    }

    public void setUuidatencion(String uuidatencion) {
        this.uuidatencion = uuidatencion;
    }

    public String getNotificaciondescripcion() {
        return notificaciondescripcion;
    }

    public void setNotificaciondescripcion(String notificaciondescripcion) {
        this.notificaciondescripcion = notificaciondescripcion;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public Notificacion getNotificacionId() {
        return notificacionId;
    }

    public void setNotificacionId(Notificacion notificacionId) {
        this.notificacionId = notificacionId;
    }

    public Variable getVariableId() {
        return variableId;
    }

    public void setVariableId(Variable variableId) {
        this.variableId = variableId;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Resultadovalidacion other = (Resultadovalidacion) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

    @Override
    public String toString() {
        return "ec.gob.msp.rdacaa.business.entity.Resultadovalidacion[ id=" + id + " ]";
    }
    
}
