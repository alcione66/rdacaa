/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Immutable;

/**
 *
 * @author msp
 */
@Entity
@Immutable
@Table(name = "VARIABLES_DATOS_OBSTETRICOS", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VariablesDatosObstetricos.findAll", query = "SELECT v FROM VariablesDatosObstetricos v")})
public class VariablesDatosObstetricos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID_ATENCION")
    private Integer idAtencion;
    @Column(name = "UUID_ATENCION", length = 2000000000)
    private String uuidAtencion;
    @Column(name = "COD_OBS_RIES_OBSTETRICO")
    private Integer codObsRiesObstetrico;
    @Column(name = "COD_OBS_RIES_OBSTETRICO_DESC", length = 2000000000)
    private String codObsRiesObstetricoDesc;
    @Column(name = "COD_OBS_PLA_PARTO")
    private Integer codObsPlaParto;
    @Column(name = "COD_OBS_PLA_PARTO_DESC", length = 2000000000)
    private String codObsPlaPartoDesc;
    @Column(name = "COD_OBS_PLA_TRANSPORTE")
    private Integer codObsPlaTransporte;
    @Column(name = "COD_OBS_PLA_TRANSPORTE_DESC", length = 2000000000)
    private String codObsPlaTransporteDesc;
    @Column(name = "COD_OBS_EMB_PLANIFICADO")
    private Integer codObsEmbPlanificado;
    @Column(name = "COD_OBS_EMB_PLANIFICADO_DESC", length = 2000000000)
    private String codObsEmbPlanificadoDesc;
    @Column(name = "COD_OBS_MET_DET_SEMANAS_GESTACION")
    private Integer codObsMetDetSemanasGestacion;
    @Column(name = "COD_OBS_MET_DET_SEMANAS_GESTACION_DESC", length = 2000000000)
    private String codObsMetDetSemanasGestacionDesc;
    @Column(name = "OBS_FEC_ULT_MENSTRUACION", length = 2000000000)
    private String codObsFum;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "OBS_SEM_GESTACION", precision = 2000000000, scale = 10)
    private Double obsSemGestacion;

    public VariablesDatosObstetricos() {
    }

    public Integer getIdAtencion() {
        return idAtencion;
    }

    public void setIdAtencion(Integer idAtencion) {
        this.idAtencion = idAtencion;
    }

    public String getUuidAtencion() {
        return uuidAtencion;
    }

    public void setUuidAtencion(String uuidAtencion) {
        this.uuidAtencion = uuidAtencion;
    }

    public Integer getCodObsRiesObstetrico() {
        return codObsRiesObstetrico;
    }

    public void setCodObsRiesObstetrico(Integer codObsRiesObstetrico) {
        this.codObsRiesObstetrico = codObsRiesObstetrico;
    }

    public String getCodObsRiesObstetricoDesc() {
        return codObsRiesObstetricoDesc;
    }

    public void setCodObsRiesObstetricoDesc(String codObsRiesObstetricoDesc) {
        this.codObsRiesObstetricoDesc = codObsRiesObstetricoDesc;
    }

    public Integer getCodObsPlaParto() {
        return codObsPlaParto;
    }

    public void setCodObsPlaParto(Integer codObsPlaParto) {
        this.codObsPlaParto = codObsPlaParto;
    }

    public String getCodObsPlaPartoDesc() {
        return codObsPlaPartoDesc;
    }

    public void setCodObsPlaPartoDesc(String codObsPlaPartoDesc) {
        this.codObsPlaPartoDesc = codObsPlaPartoDesc;
    }

    public Integer getCodObsPlaTransporte() {
        return codObsPlaTransporte;
    }

    public void setCodObsPlaTransporte(Integer codObsPlaTransporte) {
        this.codObsPlaTransporte = codObsPlaTransporte;
    }

    public String getCodObsPlaTransporteDesc() {
        return codObsPlaTransporteDesc;
    }

    public void setCodObsPlaTransporteDesc(String codObsPlaTransporteDesc) {
        this.codObsPlaTransporteDesc = codObsPlaTransporteDesc;
    }

    public Integer getCodObsEmbPlanificado() {
        return codObsEmbPlanificado;
    }

    public void setCodObsEmbPlanificado(Integer codObsEmbPlanificado) {
        this.codObsEmbPlanificado = codObsEmbPlanificado;
    }

    public String getCodObsEmbPlanificadoDesc() {
        return codObsEmbPlanificadoDesc;
    }

    public void setCodObsEmbPlanificadoDesc(String codObsEmbPlanificadoDesc) {
        this.codObsEmbPlanificadoDesc = codObsEmbPlanificadoDesc;
    }

    public Integer getCodObsMetDetSemanasGestacion() {
        return codObsMetDetSemanasGestacion;
    }

    public void setCodObsMetDetSemanasGestacion(Integer codObsMetDetSemanasGestacion) {
        this.codObsMetDetSemanasGestacion = codObsMetDetSemanasGestacion;
    }

    public String getCodObsMetDetSemanasGestacionDesc() {
        return codObsMetDetSemanasGestacionDesc;
    }

    public void setCodObsMetDetSemanasGestacionDesc(String codObsMetDetSemanasGestacionDesc) {
        this.codObsMetDetSemanasGestacionDesc = codObsMetDetSemanasGestacionDesc;
    }

    public String getCodObsFum() {
        return codObsFum;
    }

    public void setCodObsFum(String codObsFum) {
        this.codObsFum = codObsFum;
    }

    public Double getObsSemGestacion() {
        return obsSemGestacion;
    }

    public void setObsSemGestacion(Double obsSemGestacion) {
        this.obsSemGestacion = obsSemGestacion;
    }
    
}
