package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "entidad")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Entidad.findAll", query = "SELECT e FROM Entidad e") })
public class Entidad implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "codigo", length = 20)
	private String codigo;
	@Column(name = "ruc", length = 20)
	private String ruc;
	@Basic(optional = false)
	@Column(name = "nombreoficial", nullable = false, length = 250)
	private String nombreoficial;
	@Column(name = "nombrecomercial", length = 150)
	private String nombrecomercial;
	@Column(name = "direccion", length = 255)
	private String direccion;
	@Column(name = "direccionreferencia", length = 255)
	private String direccionreferencia;
	@Column(name = "numestablecimiento", length = 5)
	private String numestablecimiento;
	@Column(name = "telefono", length = 100)
	private String telefono;
	@Column(name = "email", length = 80)
	private String email;
	@Column(name = "parroquia_id")
	private Integer parroquiaId;
	@Column(name = "z17s")
	private Integer z17s;
	@OneToMany(mappedBy = "entidadId")
	private List<Entidad> entidadList;
	@JoinColumn(name = "entidad_id", referencedColumnName = "id")
	@ManyToOne
	private Entidad entidadId;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "entidadId")
	private List<Usuario> usuarioList;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "entidadId")
	private List<Referenciaatencion> referenciaatencionList;

	@JoinColumn(name = "circuito_id", referencedColumnName = "id")
	@ManyToOne
	private Circuito circuitoId;
	@JoinColumn(name = "ubicacion_parroquia_id", referencedColumnName = "id")
	@ManyToOne
	private Parroquia ubicacionParroquiaId;
	@JoinColumn(name = "tipoestablecimiento_id", referencedColumnName = "id")
	@ManyToOne
	private Tipoestablecimiento tipoestablecimientoId;

	public Entidad() {
	}

	public Entidad(Integer id) {
		this.id = id;
	}

	public Entidad(Integer id, String nombreoficial) {
		this.id = id;
		this.nombreoficial = nombreoficial;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getRuc() {
		return ruc;
	}

	public void setRuc(String ruc) {
		this.ruc = ruc;
	}

	public String getNombreoficial() {
		return nombreoficial;
	}

	public void setNombreoficial(String nombreoficial) {
		this.nombreoficial = nombreoficial;
	}

	public String getNombrecomercial() {
		return nombrecomercial;
	}

	public void setNombrecomercial(String nombrecomercial) {
		this.nombrecomercial = nombrecomercial;
	}

	public String getDireccion() {
		return direccion;
	}

	public void setDireccion(String direccion) {
		this.direccion = direccion;
	}

	public String getDireccionreferencia() {
		return direccionreferencia;
	}

	public void setDireccionreferencia(String direccionreferencia) {
		this.direccionreferencia = direccionreferencia;
	}

	public String getNumestablecimiento() {
		return numestablecimiento;
	}

	public void setNumestablecimiento(String numestablecimiento) {
		this.numestablecimiento = numestablecimiento;
	}

	public String getTelefono() {
		return telefono;
	}

	public void setTelefono(String telefono) {
		this.telefono = telefono;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Integer getParroquiaId() {
		return parroquiaId;
	}

	public void setParroquiaId(Integer parroquiaId) {
		this.parroquiaId = parroquiaId;
	}

	public Integer getZ17s() {
		return z17s;
	}

	public void setZ17s(Integer z17s) {
		this.z17s = z17s;
	}

	@XmlTransient
	public List<Entidad> getEntidadList() {
		return entidadList;
	}

	public void setEntidadList(List<Entidad> entidadList) {
		this.entidadList = entidadList;
	}

	public Entidad getEntidadId() {
		return entidadId;
	}

	public void setEntidadId(Entidad entidadId) {
		this.entidadId = entidadId;
	}

	@XmlTransient
	public List<Usuario> getUsuarioList() {
		return usuarioList;
	}

	public void setUsuarioList(List<Usuario> usuarioList) {
		this.usuarioList = usuarioList;
	}

	@XmlTransient
	public List<Referenciaatencion> getReferenciaatencionList() {
		return referenciaatencionList;
	}

	public void setReferenciaatencionList(List<Referenciaatencion> referenciaatencionList) {
		this.referenciaatencionList = referenciaatencionList;
	}

	public Circuito getCircuitoId() {
		return circuitoId;
	}

	public void setCircuitoId(Circuito circuitoId) {
		this.circuitoId = circuitoId;
	}

	public Parroquia getUbicacionParroquiaId() {
		return ubicacionParroquiaId;
	}

	public void setUbicacionParroquiaId(Parroquia ubicacionParroquiaId) {
		this.ubicacionParroquiaId = ubicacionParroquiaId;
	}

	public Tipoestablecimiento getTipoestablecimientoId() {
		return tipoestablecimientoId;
	}

	public void setTipoestablecimientoId(Tipoestablecimiento tipoestablecimientoId) {
		this.tipoestablecimientoId = tipoestablecimientoId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Entidad other = (Entidad) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ec.gob.msp.rdacaa.business.entity.Entidad[ id=" + id + " ]";
	}

}
