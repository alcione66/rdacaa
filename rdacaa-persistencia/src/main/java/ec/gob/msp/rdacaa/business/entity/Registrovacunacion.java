package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "registrovacunacion", uniqueConstraints = { @UniqueConstraint(columnNames = { "id" }) })
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Registrovacunacion.findAll", query = "SELECT r FROM Registrovacunacion r") })
public class Registrovacunacion implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "valor")
	private Integer valor;
	@Column(name = "lote", length = 50)
	private String lote;
	@JoinColumn(name = "atencionmedica_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Atencionmedica atencionmedicaId;
	@Column(name = "fecharegistro")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecharegistro;
	@JoinColumn(name = "esquemavacunacion_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Esquemavacunacion esquemavacunacionId;
    @JoinColumn(name = "ctgruporiesgo_id", referencedColumnName = "id")
    @ManyToOne
    private Detallecatalogo ctgruporiesgoId;
	@Column(name = "orden", nullable = false)
	private Integer orden;

	public Registrovacunacion() {
	}

	public Registrovacunacion(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getValor() {
		return valor;
	}

	public void setValor(Integer valor) {
		this.valor = valor;
	}

	public String getLote() {
		return lote;
	}

	public void setLote(String lote) {
		this.lote = lote;
	}

	public Atencionmedica getAtencionmedicaId() {
		return atencionmedicaId;
	}

	public void setAtencionmedicaId(Atencionmedica atencionmedicaId) {
		this.atencionmedicaId = atencionmedicaId;
	}

	public Date getFecharegistro() {
		return fecharegistro;
	}

	public void setFecharegistro(Date fecharegistro) {
		this.fecharegistro = fecharegistro;
	}

	public Esquemavacunacion getEsquemavacunacionId() {
		return esquemavacunacionId;
	}

	public void setEsquemavacunacionId(Esquemavacunacion esquemavacunacionId) {
		this.esquemavacunacionId = esquemavacunacionId;
	}
	
    public Detallecatalogo getCtgruporiesgoId() {
        return ctgruporiesgoId;
    }

    public void setCtgruporiesgoId(Detallecatalogo ctgruporiesgoId) {
        this.ctgruporiesgoId = ctgruporiesgoId;
    }

	public Integer getOrden() {
		return orden;
	}

	public void setOrden(Integer orden) {
		this.orden = orden;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Registrovacunacion other = (Registrovacunacion) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ec.gob.msp.rdacaa.business.entity.Registrovacunacion[ id=" + id + " ]";
	}

}
