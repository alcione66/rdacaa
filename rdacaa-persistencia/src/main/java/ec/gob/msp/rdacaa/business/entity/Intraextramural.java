package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "intraextramural")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Intraextramural.findAll", query = "SELECT i FROM Intraextramural i") })
public class Intraextramural implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", nullable = false)
	private Integer id;
	@JoinColumn(name = "atencionmedica_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Atencionmedica atencionmedicaId;
	@JoinColumn(name = "ctlugaratencion_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Detallecatalogo ctlugaratencionId;
	@JoinColumn(name = "cttipoatencion_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Detallecatalogo cttipoatencionId;

	public Intraextramural() {
	}

	public Intraextramural(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Atencionmedica getAtencionmedicaId() {
		return atencionmedicaId;
	}

	public void setAtencionmedicaId(Atencionmedica atencionmedicaId) {
		this.atencionmedicaId = atencionmedicaId;
	}

	public Detallecatalogo getCtlugaratencionId() {
		return ctlugaratencionId;
	}

	public void setCtlugaratencionId(Detallecatalogo ctlugaratencionId) {
		this.ctlugaratencionId = ctlugaratencionId;
	}

	public Detallecatalogo getCttipoatencionId() {
		return cttipoatencionId;
	}

	public void setCttipoatencionId(Detallecatalogo cttipoatencionId) {
		this.cttipoatencionId = cttipoatencionId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Intraextramural other = (Intraextramural) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ec.gob.msp.rdacaa.business.entity.Intraextramural[ id=" + id + " ]";
	}

}
