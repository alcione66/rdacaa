package ec.gob.msp.rdacaa.business.service;

import lombok.Getter;

@Getter
public class ValoresAntropometricos {

	private Double peso;
	private Double talla;
	private Double imc;
	private Double perimetroCefalico;

	public ValoresAntropometricos(Double peso, Double talla, Double imc, Double perimetroCefalico) {
		super();
		this.peso = peso;
		this.talla = talla;
		this.imc = imc;
		this.perimetroCefalico = perimetroCefalico;
	}
	
	public static ValoresAntropometricos getInstance(Double peso, Double talla, Double imc, Double perimetroCefalico) {
		return new ValoresAntropometricos(peso, talla, imc, perimetroCefalico) ;
	}
}