package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "usuario", uniqueConstraints = { @UniqueConstraint(columnNames = { "id" }) })
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Usuario.findAll", query = "SELECT u FROM Usuario u") })
public class Usuario implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "idpras")
	private Integer idpras;
	@Column(name = "clave", length = 256)
	private String clave;
	@Column(name = "login", length = 16)
	private String login;
	@Column(name = "cambiarclave")
	private Integer cambiarclave;
	@Column(name = "fechacreacion")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fechacreacion;
	@Column(name = "procesador", length = 500)
	private String procesador;
	@Column(name = "memoria", length = 500)
	private String memoria;
	@Column(name = "publickey")
	private String publickey;
	@Column(name = "key")
	private byte[] key;
	@Column(name = "salt")
	private byte[] salt;
	@JoinColumn(name = "ctestado_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Detallecatalogo ctestadoId;
	@JoinColumn(name = "entidad_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Entidad entidadId;
	@JoinColumn(name = "persona_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Persona personaId;

	public Usuario() {
	}

	public Usuario(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getIdpras() {
		return idpras;
	}

	public void setIdpras(Integer idpras) {
		this.idpras = idpras;
	}

	public String getClave() {
		return clave;
	}

	public void setClave(String clave) {
		this.clave = clave;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public Integer getCambiarclave() {
		return cambiarclave;
	}

	public void setCambiarclave(Integer cambiarclave) {
		this.cambiarclave = cambiarclave;
	}

	public Date getFechacreacion() {
		return fechacreacion;
	}

	public void setFechacreacion(Date fechacreacion) {
		this.fechacreacion = fechacreacion;
	}

	public String getProcesador() {
		return procesador;
	}

	public void setProcesador(String procesador) {
		this.procesador = procesador;
	}

	public String getMemoria() {
		return memoria;
	}

	public void setMemoria(String memoria) {
		this.memoria = memoria;
	}

	public byte[] getKey() {
		return key;
	}

	public void setKey(byte[] key) {
		this.key = key;
	}

	public byte[] getSalt() {
		return salt;
	}

	public void setSalt(byte[] salt) {
		this.salt = salt;
	}

	public Detallecatalogo getCtestadoId() {
		return ctestadoId;
	}

	public void setCtestadoId(Detallecatalogo ctestadoId) {
		this.ctestadoId = ctestadoId;
	}

	public Entidad getEntidadId() {
		return entidadId;
	}

	public void setEntidadId(Entidad entidadId) {
		this.entidadId = entidadId;
	}

	public Persona getPersonaId() {
		return personaId;
	}

	public void setPersonaId(Persona personaId) {
		this.personaId = personaId;
	}
	
	

	public String getPublickey() {
		return publickey;
	}

	public void setPublickey(String publickey) {
		this.publickey = publickey;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Usuario other = (Usuario) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ec.gob.msp.rdacaa.business.entity.Usuario[ id=" + id + " ]";
	}

}
