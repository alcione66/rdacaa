package ec.gob.msp.rdacaa.business.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.common.collect.Lists;
import com.querydsl.core.types.dsl.BooleanExpression;

import ec.gob.msp.rdacaa.business.entity.Accesoformaespecialidad;
import ec.gob.msp.rdacaa.business.entity.QAccesoformaespecialidad;
import ec.gob.msp.rdacaa.business.repository.AccesoformaespecialidadRepository;

/**
 * Generated by Spring Data Generator on 20/08/2018
 */
@Service
public class AccesoformaespecialidadService {
    private static final String CAMPO_ORDEN = "orden";
    private final AccesoformaespecialidadRepository accesoformaespecialidadRepository;

    @Autowired
    public AccesoformaespecialidadService(AccesoformaespecialidadRepository accesoformaespecialidadRepository) {
        this.accesoformaespecialidadRepository = accesoformaespecialidadRepository;
    }

    public List<Accesoformaespecialidad> findAllByEspecialidadId(Integer idEspecialidad){
        QAccesoformaespecialidad qAccesoformaespecialidad = QAccesoformaespecialidad.accesoformaespecialidad;
        BooleanExpression isEspecialidad = qAccesoformaespecialidad.ctespecialidadId.id.eq(idEspecialidad);
        return Lists.newArrayList(accesoformaespecialidadRepository.findAll(isEspecialidad, GenericUtil.orderFieldASC(CAMPO_ORDEN)));
    }
}
