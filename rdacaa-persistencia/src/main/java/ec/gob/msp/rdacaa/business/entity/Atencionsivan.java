package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "atencionsivan")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Atencionsivan.findAll", query = "SELECT a FROM Atencionsivan a") })
public class Atencionsivan implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "madrelactancia")
	private Integer madrelactancia;
	@Column(name = "recibiolechematerna")
	private Integer recibiolechematerna;
	@Column(name = "cosumioalimentosol")
	private Integer cosumioalimentosol;
	@Column(name = "observacion", length = 250)
	private String observacion;
	@JoinColumn(name = "atencionmedica_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Atencionmedica atencionmedicaId;

	public Atencionsivan() {
	}

	public Atencionsivan(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getMadrelactancia() {
		return madrelactancia;
	}

	public void setMadrelactancia(Integer madrelactancia) {
		this.madrelactancia = madrelactancia;
	}

	public Integer getRecibiolechematerna() {
		return recibiolechematerna;
	}

	public void setRecibiolechematerna(Integer recibiolechematerna) {
		this.recibiolechematerna = recibiolechematerna;
	}

	public Integer getCosumioalimentosol() {
		return cosumioalimentosol;
	}

	public void setCosumioalimentosol(Integer cosumioalimentosol) {
		this.cosumioalimentosol = cosumioalimentosol;
	}

	public String getObservacion() {
		return observacion;
	}

	public void setObservacion(String observacion) {
		this.observacion = observacion;
	}

	public Atencionmedica getAtencionmedicaId() {
		return atencionmedicaId;
	}

	public void setAtencionmedicaId(Atencionmedica atencionmedicaId) {
		this.atencionmedicaId = atencionmedicaId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Atencionsivan other = (Atencionsivan) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ec.gob.msp.rdacaa.business.entity.Atencionsivan[ id=" + id + " ]";
	}

}
