package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "atencionviolencia", uniqueConstraints = { @UniqueConstraint(columnNames = { "id" }) })
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Atencionviolencia.findAll", query = "SELECT a FROM Atencionviolencia a") })
public class Atencionviolencia implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "numserieformulario")
	private String numserieformulario;
	@JoinColumn(name = "atencionmedica_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Atencionmedica atencionmedicaId;
	@JoinColumn(name = "ctidentificaagresor_id", referencedColumnName = "id")
	@ManyToOne
	private Detallecatalogo ctidentificaagresorId;
	@JoinColumn(name = "ctlesionesocurridas_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Detallecatalogo ctlesionesocurridasId;
	@JoinColumn(name = "ctparentesco_id", referencedColumnName = "id")
	@ManyToOne
	private Detallecatalogo ctparentescoId;

	public Atencionviolencia() {
	}

	public Atencionviolencia(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getNumserieformulario() {
		return numserieformulario;
	}

	public void setNumserieformulario(String numserieformulario) {
		this.numserieformulario = numserieformulario;
	}

	public Atencionmedica getAtencionmedicaId() {
		return atencionmedicaId;
	}

	public void setAtencionmedicaId(Atencionmedica atencionmedicaId) {
		this.atencionmedicaId = atencionmedicaId;
	}

	public Detallecatalogo getCtidentificaagresorId() {
		return ctidentificaagresorId;
	}

	public void setCtidentificaagresorId(Detallecatalogo ctidentificaagresorId) {
		this.ctidentificaagresorId = ctidentificaagresorId;
	}

	public Detallecatalogo getCtlesionesocurridasId() {
		return ctlesionesocurridasId;
	}

	public void setCtlesionesocurridasId(Detallecatalogo ctlesionesocurridasId) {
		this.ctlesionesocurridasId = ctlesionesocurridasId;
	}

	public Detallecatalogo getCtparentescoId() {
		return ctparentescoId;
	}

	public void setCtparentescoId(Detallecatalogo ctparentescoId) {
		this.ctparentescoId = ctparentescoId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		Atencionviolencia other = (Atencionviolencia) obj;
		if (id == null) {
			if (other.id != null) {
				return false;
			}
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ec.gob.msp.rdacaa.business.entity.Atencionviolencia[ id=" + id + " ]";
	}

}
