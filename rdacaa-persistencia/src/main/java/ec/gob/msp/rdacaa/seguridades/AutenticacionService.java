/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.seguridades;

import ec.gob.msp.rdacaa.business.entity.Accesoformaespecialidad;
import java.util.ArrayList;
import java.util.List;

import org.mindrot.jbcrypt.BCrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.msp.rdacaa.business.entity.Entidadpersona;
import ec.gob.msp.rdacaa.business.entity.Usuario;
import ec.gob.msp.rdacaa.business.service.AccesoformaespecialidadService;
import ec.gob.msp.rdacaa.business.service.EntidadpersonaService;
import ec.gob.msp.rdacaa.business.service.UsuarioService;
import ec.gob.msp.rdacaa.seguridades.util.SeguridadUtil;

/**
 *
 * @author jazael.faubla
 */
@Service
public class AutenticacionService {
    private static final Logger logger = LoggerFactory.getLogger(AutenticacionService.class);

    private final UsuarioService usuarioService;
    private final EntidadpersonaService entidadpersonaService;
    private final AccesoformaespecialidadService accesoformaespecialidadService;

    @Autowired
    public AutenticacionService(UsuarioService usuarioService, EntidadpersonaService entidadpersonaService, AccesoformaespecialidadService accesoformaespecialidadService) {
        this.usuarioService = usuarioService;
        this.entidadpersonaService = entidadpersonaService;
        this.accesoformaespecialidadService = accesoformaespecialidadService;
    }

    public List<Object> getCredenciales(String usuario, String contrasena) {
        ArrayList<Object> attributesUser = new ArrayList<>();
        try {
            Usuario usr = usuarioService.findOneByLogin(usuario);
            if (usr != null && SeguridadUtil.verificarContrasena(usr.getClave(),contrasena)) {
                attributesUser.add(usr);
                List<Entidadpersona> listEntPer = searchEntidadPersona(usr.getPersonaId().getId());
                if (!listEntPer.isEmpty()) {
                    attributesUser.add(listEntPer);
                } else {
                	attributesUser.add(new ArrayList<Entidadpersona>());
                }
            }
        } catch (Exception e) {
            logger.error("Las credenciales no pueden ser validadas",e);
        }

        return attributesUser;
    }

    public Usuario searchUsuario(String usuario) {
        return usuarioService.findOneByLogin(usuario);
    }

    public Usuario findUsuarioCambiaClave(String usuario) {
        return usuarioService.findOneByLoginCambioClave(usuario);
    }

    private List<Entidadpersona> searchEntidadPersona(Integer idPersona) {
        return entidadpersonaService.findAllByPersonaId(idPersona);
    }

    public static Boolean isValidPassword(String passwordHash, String passwordInput) {
        Boolean isValidPassword = false;
        if (BCrypt.checkpw(passwordInput, passwordHash)) {
                logger.debug("It matches");
                isValidPassword = true;
        } else {
                logger.debug("It does not match");
                isValidPassword = false;
        }
        return isValidPassword;
    }

    public List<Accesoformaespecialidad> getPanelesFormaEspecialidad(Integer idEspecialidad) {
        return accesoformaespecialidadService.findAllByEspecialidadId(idEspecialidad);
    }
}
