package ec.gob.msp.rdacaa.utilitario.exportdatabase;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.function.Function;

import org.apache.commons.lang3.ArrayUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class SQLiteUtil {

	private SQLiteUtil() {
	}

	private static final Logger logger = LoggerFactory.getLogger(SQLiteUtil.class);

	public static File createDatabase(String path) {

		path = (path == null || path.isEmpty()) ? "sqlite.db" : path;
		File databaseFile = null;

		try (Connection conn = createSqliteConnection(path)) {
			DatabaseMetaData meta = conn.getMetaData();
			logger.debug("The driver name is {}", meta.getDriverName());
			logger.debug("A new database has been created.");
		} catch (SQLException e) {
			logger.error("Error creating database", e);
			return null;
		}

		databaseFile = new File(path);

		return databaseFile;
	}

	public static File sqlBaseOrigenToBaseDestino(String pathOrigen, String pathDestino, String sqlDatosPersona,
			String sqlOrigenToDestinoAtencion, Object... params) throws IOException {

		cleanUp(pathDestino);

		File databaseFileExport = new File(pathDestino);

		databaseFileExport = createDatabase(pathDestino);

		try ( Connection conn = createInMemorySqliteConnection();
				PreparedStatement pstmtAttachOrigen = conn.prepareStatement("attach database ? as origen;");
				PreparedStatement pstmtAttachDestino = conn.prepareStatement("attach database ? as destino; ");

				PreparedStatement pstmtDetachOrigen = conn.prepareStatement("detach database origen;");
				PreparedStatement pstmtDetachDestino = conn.prepareStatement("detach database destino; ");) {

			pstmtAttachOrigen.setString(1, pathOrigen);
			pstmtAttachDestino.setString(1, pathDestino);

			logger.debug(pathOrigen);
			logger.debug(pathDestino);

			pstmtAttachOrigen.execute();
			pstmtAttachDestino.execute();

			if (sqlDatosPersona != null && !sqlDatosPersona.isEmpty()) {
				executeExportStatement(sqlDatosPersona, conn);
			}
			executeExportStatement(sqlOrigenToDestinoAtencion, conn, params);

			pstmtDetachOrigen.execute();
			pstmtDetachDestino.execute();

		} catch (SQLException e) {
			logger.error("Error exporting data", e);
			saveExceptionToFile(pathDestino, e);
			cleanUp(pathDestino);
			return null;
		} 

		return databaseFileExport;
	}

	private static void executeExportStatement(String sqlOrigenToDestino, Connection conn, Object... params)
			throws SQLException {
		try (PreparedStatement pstmtSqlOrigenToDestino = conn.prepareStatement(sqlOrigenToDestino);) {
			if (null != params && params.length > 0) {
				for (int i = 0; i < params.length; i++) {
					pstmtSqlOrigenToDestino.setObject(i + 1, params[i]);
				}
			}
			pstmtSqlOrigenToDestino.execute();
		} catch (SQLException e) {
			logger.error("Error in executeExportStatement", e);
			throw e;
		}
	}

	private static boolean cleanUp(String path) throws IOException {
		return Files.deleteIfExists(Paths.get(path));
	}

	private static Connection createSqliteConnection(String path) throws SQLException {
		String url = "jdbc:sqlite:" + path;
		return DriverManager.getConnection(url);
	}
	
	private static Connection createInMemorySqliteConnection() throws SQLException {
		String url = "jdbc:sqlite::memory:";
		return DriverManager.getConnection(url);
	}

	public static Optional<int[]> fileHasUUIDsAtencionXPersona(String validatedRdacaaFilePath) {
		try (Connection conn = createSqliteConnection(validatedRdacaaFilePath);
				PreparedStatement uuidsPersonaNull = conn.prepareStatement(
						"select count(rowid) from persona where UUID_PERSONA is null or trim(UUID_PERSONA)='';");
				PreparedStatement uuidsAtencionNull = conn.prepareStatement(
						"select count(rowid) from atencion where KS_CIDENT is null or trim(KS_CIDENT)='';");
				PreparedStatement hasUUIDsAtencionXPersonaPS = conn.prepareStatement(
						"select (select count(rowid) from atencion) = ap.apcount from (select count(a.rowid) as apcount from atencion a INNER JOIN persona p on (a.KS_CIDENT = p.UUID_PERSONA)) as ap;");
				ResultSet countUuidsPersonaNullRS = uuidsPersonaNull.executeQuery();
				ResultSet countUuidsAtencionNullRS = uuidsAtencionNull.executeQuery();
				ResultSet hasUUIDsAtencionXPersonaRS = hasUUIDsAtencionXPersonaPS.executeQuery();) {

			countUuidsPersonaNullRS.next();
			countUuidsAtencionNullRS.next();
			hasUUIDsAtencionXPersonaRS.next();

			int countUuidsPersonaNull = countUuidsPersonaNullRS.getInt(1);
			int countUuidsAtencionNull = countUuidsAtencionNullRS.getInt(1);
			int hasUUIDsAtencionXPersona = hasUUIDsAtencionXPersonaRS.getInt(1);

			return Optional.of(new int[] { countUuidsPersonaNull, countUuidsAtencionNull, hasUUIDsAtencionXPersona });

		} catch (Exception e) {
			logger.error("Error checking fileHasUUIDsAtencionXPersona", e);
			return Optional.empty();
		}
	}

	public static Optional<List<List<String[]>>> fileHasValidUUID(String validatedRdacaaFilePath) {

		try (Connection conn = createSqliteConnection(validatedRdacaaFilePath);
				PreparedStatement uuidsPersona = conn.prepareStatement("select p.UUID_PERSONA, count(rowid) from persona p GROUP BY p.UUID_PERSONA;");
				PreparedStatement uuidsAtencion = conn.prepareStatement("select a.COD_UUID, count(rowid) from atencion a GROUP BY a.COD_UUID;");
				ResultSet uuidsPersonaRS = uuidsPersona.executeQuery();
				ResultSet uuidsAtencionExistsRS = uuidsAtencion.executeQuery();) {

			List<String[]> listaInvalidUUIDsPersona = new ArrayList<>();
			List<String[]> listaInvalidUUIDsAtencion = new ArrayList<>();
			while (uuidsPersonaRS.next()) {
				String uuidString = uuidsPersonaRS.getString(1);
				Integer countUUID = uuidsPersonaRS.getInt(2);
				boolean isValid = isValidUUID(uuidString) && countUUID.equals(1) ;
				if (!isValid) {
					listaInvalidUUIDsPersona.add(new String[] {uuidString,countUUID.toString()});
				}
			}

			while (uuidsAtencionExistsRS.next()) {
				String uuidString = uuidsAtencionExistsRS.getString(1);
				Integer countUUID = uuidsAtencionExistsRS.getInt(2);
				boolean isValid = isValidUUID(uuidString) && countUUID.equals(1) ;
				if (!isValid) {
					listaInvalidUUIDsAtencion.add(new String[] {uuidString,countUUID.toString()});
				}
			}

			List<List<String[]>> listasUUIDs = new ArrayList<>();
			listasUUIDs.add(listaInvalidUUIDsPersona);
			listasUUIDs.add(listaInvalidUUIDsAtencion);

			return Optional.of(listasUUIDs);

		} catch (Exception e) {
			logger.error("Error checking valid UUIDs in rdacaa file", e);
			return Optional.empty();
		}
	}

	private static boolean isValidUUID(String uuidString) {
		return uuidString != null && uuidString.length() == 36 && uuidString.split("-").length == 5
				&& UUID.fromString(uuidString).toString().equals(uuidString);
	}

	public static int isFileSameProfesional(String validatedRdacaaFilePath) {

		try (Connection conn = createSqliteConnection(validatedRdacaaFilePath);
				PreparedStatement profesionales = conn.prepareStatement(
						"select count (*) from (select COD_PROFESIONAL,count(COD_PROFESIONAL) from atencion group by COD_PROFESIONAL )");
				ResultSet countProfesionalesRS = profesionales.executeQuery();) {

			countProfesionalesRS.next();

			int countProfesionales = countProfesionalesRS.getInt(1);

			return countProfesionales == 1 ? 1 : 0;

		} catch (Exception e) {
			logger.error("Error checking same profesional in rdacaa file", e);
			return -1;
		}
	}

	public static int isFileSameEstablecimiento(String validatedRdacaaFilePath) {

		try (Connection conn = createSqliteConnection(validatedRdacaaFilePath);
				PreparedStatement establecimiento = conn.prepareStatement(
						"select count (*) from (select COD_EST_SALUD,count(COD_EST_SALUD) from atencion group by COD_EST_SALUD ); ");
				ResultSet countEstablecimientoRS = establecimiento.executeQuery();) {

			countEstablecimientoRS.next();

			int countEstablecimiento = countEstablecimientoRS.getInt(1);

			return countEstablecimiento == 1 ? 1 : 0;

		} catch (Exception e) {
			logger.error("Error checking same establecimiento in rdacaa file", e);
			return -1;
		}
	}

	public static int isFileStructureValid(String validatedRdacaaFilePath) {

		try (Connection conn = createSqliteConnection(validatedRdacaaFilePath);
				PreparedStatement atencionExists = conn.prepareStatement(
						"SELECT count(name) FROM sqlite_master WHERE type='table' AND name='atencion';");
				PreparedStatement personaExists = conn.prepareStatement(
						"SELECT count(name) FROM sqlite_master WHERE type='table' AND name='persona';");
				ResultSet countAtencionExistsRS = atencionExists.executeQuery();
				ResultSet countPersonaExistsRS = personaExists.executeQuery();) {

			countAtencionExistsRS.next();
			countPersonaExistsRS.next();

			int countErroresAtencion = countAtencionExistsRS.getInt(1);
			int countErroresPersona = countPersonaExistsRS.getInt(1);

			return countErroresAtencion == 1 && countErroresPersona == 1 ? 1 : 0;

		} catch (Exception e) {
			logger.error("Error checking tables in rdacaa file", e);
			return -1;
		}
	}

	public static int isFileDatesValid(String validatedRdacaaFilePath, Date fechaInicio, Date fechaFin) {

		try (Connection conn = createSqliteConnection(validatedRdacaaFilePath);
				PreparedStatement countAtencionesOutOfRange = conn
						.prepareStatement("select count(*) from atencion where FEC_ATENCION NOT BETWEEN ? AND ?;");) {

			int countAtencionesFechasInvalidas = executeDateQuery(fechaInicio, fechaFin, countAtencionesOutOfRange);

			return countAtencionesFechasInvalidas == 0 ? 1 : 0;

		} catch (Exception e) {
			logger.error("Error checking dates in rdacaa file", e);
			return -1;
		}
	}

	private static int executeDateQuery(Date fechaInicio, Date fechaFin, PreparedStatement countAtencionesOutOfRange)
			throws SQLException {
		countAtencionesOutOfRange.setString(1, fechaInicio.toString());
		countAtencionesOutOfRange.setString(2, fechaFin.toString());
		try (ResultSet countAtencionesOutOfRangeRS = countAtencionesOutOfRange.executeQuery();) {
			countAtencionesOutOfRangeRS.next();

			return countAtencionesOutOfRangeRS.getInt(1);
		}
	}

	public static boolean isValidatedFileWithoutErrors(String validatedRdacaaFilePath) throws FileNotFoundException {

		try (Connection conn = createSqliteConnection(validatedRdacaaFilePath);
				PreparedStatement countResultadoValidacionAtencion = conn
						.prepareStatement("select count(id) from resultado_validacion_atencion rva WHERE rva.NOTIFICACION_CODIGO like '%ERR%';");
				PreparedStatement countResultadoValidacionPersona = conn
						.prepareStatement("select count(id) from resultado_validacion_persona rvp WHERE rvp.NOTIFICACION_CODIGO like '%ERR%';");
				ResultSet countErroresAtencionRS = countResultadoValidacionAtencion.executeQuery();
				ResultSet countErroresPersonaRS = countResultadoValidacionPersona.executeQuery();) {

			countErroresAtencionRS.next();
			countErroresPersonaRS.next();

			int countErroresAtencion = countErroresAtencionRS.getInt(1);
			int countErroresPersona = countErroresPersonaRS.getInt(1);

			return countErroresAtencion == 0 && countErroresPersona == 0;

		} catch (Exception e) {
			logger.error("Error counting errors in rdacaa file", e);
			saveExceptionToFile(validatedRdacaaFilePath + "-count-errors", e);
		}

		return false;
	}
	
	public static boolean isValidatedFileWithoutErrors(Connection conn) throws SQLException {

		try (PreparedStatement countResultadoValidacionAtencion = conn.prepareStatement(
				"select count(id) from resultado_validacion_atencion rva WHERE rva.NOTIFICACION_CODIGO like '%ERR%';");
				PreparedStatement countResultadoValidacionPersona = conn.prepareStatement(
						"select count(id) from resultado_validacion_persona rvp WHERE rvp.NOTIFICACION_CODIGO like '%ERR%';");
				ResultSet countErroresAtencionRS = countResultadoValidacionAtencion.executeQuery();
				ResultSet countErroresPersonaRS = countResultadoValidacionPersona.executeQuery();) {

			countErroresAtencionRS.next();
			countErroresPersonaRS.next();

			int countErroresAtencion = countErroresAtencionRS.getInt(1);
			int countErroresPersona = countErroresPersonaRS.getInt(1);

			return countErroresAtencion == 0 && countErroresPersona == 0;
		}

	}

	/**
	 * 
	 * @param pathOrigen
	 * @param pathFileArchivoAValidar
	 * @param sqlExportErroresValidacion
	 * @param dataDecifrador
	 * @param dataMapper
	 * @param validator
	 * @param credentials                Array que contiene credentials[0] => key,
	 *                                   credentials[1] => username, credentials[2]
	 *                                   => privateKey
	 * @param params
	 * @return
	 * @throws IOException
	 */
	public static File exportAfterValidationMeshCipheredFile(String pathFileArchivoAValidar,
			Function<Object[], Object> dataDecifradorCifrador, Function<Object, Object> dataMapper,
			Function<Object, Object> validator, Function<Object, List<String[]>> preparingResults,
			CipheredDataParams cipheredDataParams) throws IOException {

		File archivoAValidar = new File(pathFileArchivoAValidar);
		String pathArchivoAValidar = archivoAValidar.getAbsolutePath();
		String pathOutput = archivoAValidar.getParent() + FileSystems.getDefault().getSeparator()
				+ com.google.common.io.Files.getNameWithoutExtension(pathFileArchivoAValidar) + "-VALIDATED.rdacaa";
		logger.info(pathOutput);

		cleanUp(pathOutput);

		File databaseFileExport = new File(pathOutput);

		databaseFileExport = createDatabase(pathOutput);

		try ( 
				Connection conn = createInMemorySqliteConnection();
				PreparedStatement pstmtAttachValidar = conn.prepareStatement("attach database ? as validar;");
				PreparedStatement pstmtAttachDestino = conn.prepareStatement("attach database ? as destino;");

				Statement pstmtCopyAtencionPersona = conn.createStatement();
				Statement pstmtCreateAtencionPersonaDecoded = conn.createStatement();

				Statement stmtExportNotificacionesValidacion = conn.createStatement();

				PreparedStatement pstmtDetachValidar = conn.prepareStatement("detach database validar;");
				PreparedStatement pstmtDetachDestino = conn.prepareStatement("detach database destino;");) {

			attachValidationFileAndDestino(pathArchivoAValidar, pathOutput, pstmtAttachValidar, pstmtAttachDestino);

			createTempAtencionTable(pstmtCopyAtencionPersona);

			decipheringData(dataDecifradorCifrador, cipheredDataParams, conn);

			createAtencionConsolidada(pstmtCreateAtencionPersonaDecoded, false);

			Object personaAtencionMappedResult = mapAtencionConsolidada(conn, dataMapper);

			// Validando las personas y la atención
			Object personaAtencionValidated = validator.apply(personaAtencionMappedResult);

			createExportFile(stmtExportNotificacionesValidacion);

			List<String[]> resultsToSave = preparingResults.apply(personaAtencionValidated);

			saveValidationResult(resultsToSave, conn);

			boolean isFileWithoutError = isValidatedFileWithoutErrors(conn);

			cleanPersonaValidationData(stmtExportNotificacionesValidacion);

			copyOriginalTablesExportFile(stmtExportNotificacionesValidacion, false, isFileWithoutError);

			pstmtDetachDestino.execute();
			pstmtDetachValidar.execute();

		} catch (Exception e) {
			logger.error("Error exporting validated data", e);
			saveExceptionToFile(pathOutput, e);
			cleanUp(pathOutput);
			databaseFileExport = null;
		} 

		return databaseFileExport;
	}

	@SuppressWarnings("unchecked")
	private static void decipheringData(Function<Object[], Object> dataDecifradorCifrador,
			CipheredDataParams cipheredDataParams, Connection conn) throws DataProcessingException {
		// Descifrando
		try (PreparedStatement pstmtObtainUUIDAndCipherFields = conn
				.prepareStatement("select a.COD_UUID, a.KS_CIDENT, a.KU_CIDENT, a.KU_KEYID from validar.atencion a;");
				PreparedStatement pstmtDescifrarAtencionPersona = conn
						.prepareStatement("update atencion set KS_CIDENT=? where COD_UUID=?");
				ResultSet uuidAndCipherFields = pstmtObtainUUIDAndCipherFields.executeQuery();) {

			Object[] cipherObjects = null;
			if (cipheredDataParams.isLocalCipher()) {
				cipherObjects = ArrayUtils.addAll(new Object[] { uuidAndCipherFields },
						cipheredDataParams.getCredentials());
			} else if (cipheredDataParams.isMSPCipher()) {
				cipherObjects = ArrayUtils.addAll(new Object[] { uuidAndCipherFields },
						cipheredDataParams.getPrivateKeypath());
			}

			List<AtencionCipherDto> listaCipherDto = (List<AtencionCipherDto>) dataDecifradorCifrador
					.apply(cipherObjects);

			for (AtencionCipherDto dto : listaCipherDto) {
				pstmtDescifrarAtencionPersona.setString(1, dto.getKsCident());
				pstmtDescifrarAtencionPersona.setString(2, dto.getUuid());
				pstmtDescifrarAtencionPersona.addBatch();
			}

			pstmtDescifrarAtencionPersona.executeBatch();
			pstmtDescifrarAtencionPersona.clearBatch();
		} catch (Exception e) {
			logger.error("Error descifrando", e);
			throw new DataProcessingException(e);
		}
	}

	private static void attachValidationFileAndDestino(String pathArchivoAValidar, String pathOutput,
			PreparedStatement pstmtAttachValidar, PreparedStatement pstmtAttachDestino) throws SQLException {
		pstmtAttachValidar.setString(1, pathArchivoAValidar);
		pstmtAttachDestino.setString(1, pathOutput);

		logger.info(pathArchivoAValidar);
		logger.info(pathOutput);

		pstmtAttachValidar.execute();
		pstmtAttachDestino.execute();
	}

	public static File exportAfterValidationMeshPlainFile(String pathFileArchivoAValidar,
			Function<Object, Object> dataCifrador, Function<Object, Object> dataMapper,
			Function<Object, Object> validator, Function<Object, List<String[]>> preparingResults) throws IOException {

		File archivoAValidar = new File(pathFileArchivoAValidar);
		String pathArchivoAValidar = archivoAValidar.getAbsolutePath();
		String pathOutput = archivoAValidar.getParent() + FileSystems.getDefault().getSeparator()
				+ com.google.common.io.Files.getNameWithoutExtension(pathFileArchivoAValidar) + "-VALIDATED.rdacaa";
		logger.info(pathOutput);

		cleanUp(pathOutput);

		File databaseFileExport = new File(pathOutput);

		databaseFileExport = createDatabase(pathOutput);

		try ( Connection conn = createInMemorySqliteConnection();
				PreparedStatement pstmtAttachValidar = conn.prepareStatement("attach database ? as validar;");
				PreparedStatement pstmtAttachDestino = conn.prepareStatement("attach database ? as destino;");

				Statement pstmtCopyAtencionPersona = conn.createStatement();
				Statement pstmtCreateAtencionPersonaDecoded = conn.createStatement();

				Statement stmtExportNotificacionesValidacion = conn.createStatement();

				PreparedStatement pstmtDetachValidar = conn.prepareStatement("detach database validar;");
				PreparedStatement pstmtDetachDestino = conn.prepareStatement("detach database destino;");) {

			attachValidationFileAndDestino(pathArchivoAValidar, pathOutput, pstmtAttachValidar, pstmtAttachDestino);

			createTempAtencionTable(pstmtCopyAtencionPersona);

			cipheringData(dataCifrador, conn);

			createAtencionConsolidada(pstmtCreateAtencionPersonaDecoded, true);

			Object personaAtencionMappedResult = mapAtencionConsolidada(conn, dataMapper);

			// Validando las personas y la atención
			Object personaAtencionValidated = validator.apply(personaAtencionMappedResult);

			createExportFile(stmtExportNotificacionesValidacion);

			List<String[]> resultsToSave = preparingResults.apply(personaAtencionValidated);

			saveValidationResult(resultsToSave, conn);
			
			cleanPersonaValidationData(stmtExportNotificacionesValidacion);
			
			copyOriginalTablesExportFile(stmtExportNotificacionesValidacion, true, null);
		
			pstmtDetachDestino.execute();
			pstmtDetachValidar.execute();

		} catch (Exception e) {
			logger.error("Error exporting validated data", e);
			saveExceptionToFile(pathOutput, e);
			cleanUp(pathOutput);
			databaseFileExport = null;
		} 

		return databaseFileExport;
	}

	private static void createTempAtencionTable(Statement pstmtCopyAtencionPersona) throws SQLException {
		pstmtCopyAtencionPersona.addBatch("create table atencion as select a.* from validar.atencion a;");
		pstmtCopyAtencionPersona.executeBatch();
	}

	@SuppressWarnings("unchecked")
	private static void cipheringData(Function<Object, Object> dataDecifradorCifrador, Connection conn)
			throws DataProcessingException {
		// Cifrando
		try (PreparedStatement pstmtObtainUUIDAndCipherFields = conn
				.prepareStatement("select a.COD_UUID, a.KS_CIDENT, a.KU_CIDENT, a.KU_KEYID from validar.atencion a;");
				PreparedStatement pstmtDescifrarAtencionPersona = conn
						.prepareStatement("update atencion set KS_CIDENT=?, KU_CIDENT=?, KU_KEYID=? where COD_UUID=?");
				ResultSet uuidAndCipherFields = pstmtObtainUUIDAndCipherFields.executeQuery();) {

			Object cipherObjects = uuidAndCipherFields;

			List<AtencionCipherDto> listaCipherDto = (List<AtencionCipherDto>) dataDecifradorCifrador
					.apply(cipherObjects);

			for (AtencionCipherDto dto : listaCipherDto) {
				pstmtDescifrarAtencionPersona.setString(1, "");
				pstmtDescifrarAtencionPersona.setString(2, dto.getKuCident());
				pstmtDescifrarAtencionPersona.setString(3, dto.getKuKeyid());
				pstmtDescifrarAtencionPersona.setString(4, dto.getUuid());
				pstmtDescifrarAtencionPersona.addBatch();
			}

			pstmtDescifrarAtencionPersona.executeBatch();
			pstmtDescifrarAtencionPersona.clearBatch();
		} catch (Exception e) {
			logger.error("Error cifrando", e);
			throw new DataProcessingException(e);
		}
	}

	private static void createAtencionConsolidada(Statement pstmtCreateAtencionPersonaDecoded, boolean isPlainFile)
			throws SQLException {
		// Unificando persona con atención
		if (isPlainFile) {
			pstmtCreateAtencionPersonaDecoded.addBatch(
					"create table atencion_consolidada as select a.*,p.* from validar.atencion a inner join validar.persona p on (p.UUID_PERSONA=a.KS_CIDENT);");
		} else {
			pstmtCreateAtencionPersonaDecoded.addBatch(
					"create table atencion_consolidada as select a.*,p.* from atencion a inner join validar.persona p on (p.UUID_PERSONA=a.KS_CIDENT);");
		}
		pstmtCreateAtencionPersonaDecoded.executeBatch();
		pstmtCreateAtencionPersonaDecoded.clearBatch();
	}

	private static Object mapAtencionConsolidada(Connection conn, Function<Object, Object> dataMapper)
			throws SQLException {
		// Mapeando datos para validación
		try (PreparedStatement pstmtObtainPersonaAtencionConsolidada = conn
				.prepareStatement("select ac.* from atencion_consolidada ac;");) {
			ResultSet rsAtencionConsolidada = pstmtObtainPersonaAtencionConsolidada.executeQuery();
			return dataMapper.apply(rsAtencionConsolidada);
		} catch (Exception e) {
			logger.error("Error in getAtencionConsolidada", e);
			throw e;
		}
	}

	private static void createExportFile(Statement stmtExportNotificacionesValidacion)
			throws SQLException {
		stmtExportNotificacionesValidacion.addBatch(
				"CREATE TABLE resultado_validacion_persona ( ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, UUIDPERSONA VARCHAR2 (36), VARIABLE_CODIGO VARCHAR2 (50), VALOR VARCHAR2 (50), NOTIFICACION_CODIGO VARCHAR2 (50), NOTIFICACION_DESCRIPCION VARCHAR2 (250) );");
		stmtExportNotificacionesValidacion.addBatch(
				"CREATE TABLE resultado_validacion_atencion ( ID INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL, UUIDATENCION VARCHAR2 (36),  VARIABLE_CODIGO VARCHAR2 (50), VALOR VARCHAR2 (50), NOTIFICACION_CODIGO VARCHAR2 (50), NOTIFICACION_DESCRIPCION VARCHAR2 (250) );");
		stmtExportNotificacionesValidacion.executeBatch();
		stmtExportNotificacionesValidacion.clearBatch();
	}
	
	private static void copyOriginalTablesExportFile(Statement stmtExportNotificacionesValidacion, boolean isPlainFile, 
			Boolean noErrors)
			throws SQLException {
		// Exportando los resultados
		stmtExportNotificacionesValidacion.addBatch(
				"CREATE TABLE destino.resultado_validacion_persona as select dp.* from resultado_validacion_persona dp;");
		stmtExportNotificacionesValidacion.addBatch(
				"CREATE TABLE destino.resultado_validacion_atencion as select da.* from resultado_validacion_atencion da;");
		stmtExportNotificacionesValidacion
				.addBatch("create table destino.persona as select p.* from validar.persona p;");
		if (isPlainFile || (noErrors != null && noErrors)) {
			stmtExportNotificacionesValidacion.addBatch("create table destino.atencion as select a.* from atencion a;");
		} else {
			stmtExportNotificacionesValidacion
					.addBatch("create table destino.atencion as select a.* from validar.atencion a;");
		}

		stmtExportNotificacionesValidacion.addBatch("drop table atencion;");
		stmtExportNotificacionesValidacion.executeBatch();
		stmtExportNotificacionesValidacion.clearBatch();
	}

	private static void saveValidationResult(List<String[]> resultsToSave, Connection conn) throws SQLException {

		try (PreparedStatement pstmtSaveOnDatabasePersona = conn.prepareStatement(
				"INSERT INTO resultado_validacion_persona ( UUIDPERSONA, VARIABLE_CODIGO, VALOR, NOTIFICACION_CODIGO, NOTIFICACION_DESCRIPCION ) VALUES ( ?, ?, ?, ?, ? );");

				PreparedStatement pstmtSaveOnDatabaseAtencion = conn.prepareStatement(
						"INSERT INTO resultado_validacion_atencion ( UUIDATENCION, VARIABLE_CODIGO, VALOR, NOTIFICACION_CODIGO, NOTIFICACION_DESCRIPCION ) VALUES ( ?, ?, ?, ?, ? );");) {
			for (String[] resultadoValidacion : resultsToSave) {

				if (resultadoValidacion[1] != null && !resultadoValidacion[1].isEmpty()) {

					pstmtSaveOnDatabasePersona.setString(1, resultadoValidacion[1]);
					pstmtSaveOnDatabasePersona.setString(2, resultadoValidacion[2]);
					pstmtSaveOnDatabasePersona.setString(3, resultadoValidacion[3]);
					pstmtSaveOnDatabasePersona.setString(4, resultadoValidacion[4]);
					pstmtSaveOnDatabasePersona.setString(5, resultadoValidacion[5]);
					pstmtSaveOnDatabasePersona.addBatch();
				}

				if (resultadoValidacion[0] != null && !resultadoValidacion[0].isEmpty()) {
					pstmtSaveOnDatabaseAtencion.setString(1, resultadoValidacion[0]);
					pstmtSaveOnDatabaseAtencion.setString(2, resultadoValidacion[2]);
					pstmtSaveOnDatabaseAtencion.setString(3, resultadoValidacion[3]);
					pstmtSaveOnDatabaseAtencion.setString(4, resultadoValidacion[4]);
					pstmtSaveOnDatabaseAtencion.setString(5, resultadoValidacion[5]);
					pstmtSaveOnDatabaseAtencion.addBatch();
				}
			}

			pstmtSaveOnDatabasePersona.executeBatch();
			pstmtSaveOnDatabasePersona.clearBatch();
			pstmtSaveOnDatabaseAtencion.executeBatch();
			pstmtSaveOnDatabaseAtencion.clearBatch();

		} catch (Exception e) {
			logger.error("Error in saveValidationResult", e);
			throw e;
		}

	}

	private static void cleanPersonaValidationData(Statement stmtExportNotificacionesValidacion) throws SQLException {
		stmtExportNotificacionesValidacion.addBatch(
				"DELETE FROM resultado_validacion_persona WHERE rowid NOT IN (SELECT min(rowid) FROM resultado_validacion_persona GROUP BY UUIDPERSONA, VARIABLE_CODIGO,NOTIFICACION_CODIGO);");
		stmtExportNotificacionesValidacion.addBatch(
				"update resultado_validacion_persona set ID=(select c.cnt from (select id as idrs,  (select cast (count(*) as integer ) from resultado_validacion_persona b  where a.id >= b.id ) as cnt from resultado_validacion_persona a) c WHERE ID=c.idrs);");
		stmtExportNotificacionesValidacion.executeBatch();
		stmtExportNotificacionesValidacion.clearBatch();
	}

	private static void saveExceptionToFile(String pathOutput, Exception e) throws FileNotFoundException {
		File errorFileLog = new File(pathOutput + ".log");
		PrintStream ps = new PrintStream(errorFileLog);
		e.printStackTrace(ps);
	}

	static class DataProcessingException extends Exception {
		private static final long serialVersionUID = 750323619376575309L;

		public DataProcessingException(Throwable cause) {
			super(cause);
		}
	}
}