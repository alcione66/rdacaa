/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Immutable;

/**
 *
 * @author msp
 */
@Entity
@Immutable
@Table(name = "VARIABLES_VACUNAS", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VariablesVacunas.findAll", query = "SELECT v FROM VariablesVacunas v")})
public class VariablesVacunas implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID_ATENCION")
    private Integer idAtencion;
    @Column(name = "UUID_ATENCION", length = 2000000000)
    private String uuidAtencion;
    @Column(name = "COD_VACUNA_1", length = 2000000000)
    private String codVacuna1;
    @Column(name = "COD_VACUNA_DESC_1", length = 2000000000)
    private String codVacunaDesc1;
    @Column(name = "COD_VACUNA_INFO_1", length = 2000000000)
    private String codVacunaInfo1;
    @Column(name = "COD_VAC_NRO_DOSIS_1", length = 2000000000)
    private String codVacNroDosis1;
    @Column(name = "VAC_LOTE_1", length = 2000000000)
    private String vacLote1;
    @Column(name = "COD_VAC_ESQUEMA_1", length = 2000000000)
    private String codVacEsquema1;
    @Column(name = "COD_VAC_ESQUEMA_DESC_1", length = 2000000000)
    private String codVacEsquemaDesc1;
    @Column(name = "VAC_FECHA_1", length = 2000000000)
    private String vacFecha1;
    @Column(name = "COD_VACUNA_2", length = 2000000000)
    private String codVacuna2;
    @Column(name = "COD_VACUNA_DESC_2", length = 2000000000)
    private String codVacunaDesc2;
    @Column(name = "COD_VACUNA_INFO_2", length = 2000000000)
    private String codVacunaInfo2;
    @Column(name = "COD_VAC_NRO_DOSIS_2", length = 2000000000)
    private String codVacNroDosis2;
    @Column(name = "VAC_LOTE_2", length = 2000000000)
    private String vacLote2;
    @Column(name = "COD_VAC_ESQUEMA_2", length = 2000000000)
    private String codVacEsquema2;
    @Column(name = "COD_VAC_ESQUEMA_DESC_2", length = 2000000000)
    private String codVacEsquemaDesc2;
    @Column(name = "VAC_FECHA_2", length = 2000000000)
    private String vacFecha2;
    @Column(name = "COD_VACUNA_3", length = 2000000000)
    private String codVacuna3;
    @Column(name = "COD_VACUNA_DESC_3", length = 2000000000)
    private String codVacunaDesc3;
    @Column(name = "COD_VACUNA_INFO_3", length = 2000000000)
    private String codVacunaInfo3;
    @Column(name = "COD_VAC_NRO_DOSIS_3", length = 2000000000)
    private String codVacNroDosis3;
    @Column(name = "VAC_LOTE_3", length = 2000000000)
    private String vacLote3;
    @Column(name = "COD_VAC_ESQUEMA_3", length = 2000000000)
    private String codVacEsquema3;
    @Column(name = "COD_VAC_ESQUEMA_DESC_3", length = 2000000000)
    private String codVacEsquemaDesc3;
    @Column(name = "VAC_FECHA_3", length = 2000000000)
    private String vacFecha3;
    @Column(name = "COD_VACUNA_4", length = 2000000000)
    private String codVacuna4;
    @Column(name = "COD_VACUNA_DESC_4", length = 2000000000)
    private String codVacunaDesc4;
    @Column(name = "COD_VACUNA_INFO_4", length = 2000000000)
    private String codVacunaInfo4;
    @Column(name = "COD_VAC_NRO_DOSIS_4", length = 2000000000)
    private String codVacNroDosis4;
    @Column(name = "VAC_LOTE_4", length = 2000000000)
    private String vacLote4;
    @Column(name = "COD_VAC_ESQUEMA_4", length = 2000000000)
    private String codVacEsquema4;
    @Column(name = "COD_VAC_ESQUEMA_DESC_4", length = 2000000000)
    private String codVacEsquemaDesc4;
    @Column(name = "VAC_FECHA_4", length = 2000000000)
    private String vacFecha4;
    @Column(name = "COD_VACUNA_5", length = 2000000000)
    private String codVacuna5;
    @Column(name = "COD_VACUNA_DESC_5", length = 2000000000)
    private String codVacunaDesc5;
    @Column(name = "COD_VACUNA_INFO_5", length = 2000000000)
    private String codVacunaInfo5;
    @Column(name = "COD_VAC_NRO_DOSIS_5", length = 2000000000)
    private String codVacNroDosis5;
    @Column(name = "VAC_LOTE_5", length = 2000000000)
    private String vacLote5;
    @Column(name = "COD_VAC_ESQUEMA_5", length = 2000000000)
    private String codVacEsquema5;
    @Column(name = "COD_VAC_ESQUEMA_DESC_5", length = 2000000000)
    private String codVacEsquemaDesc5;
    @Column(name = "VAC_FECHA_5", length = 2000000000)
    private String vacFecha5;
    @Column(name = "COD_VAC_GPR_RIESGO_1", length = 2000000000)
    private String codvacgprriesgo1;
    @Column(name = "COD_VAC_GPR_RIESGO_1_DESC", length = 2000000000)
    private String codvacgprriesgo1desc;
    @Column(name = "COD_VAC_GPR_RIESGO_2", length = 2000000000)
    private String codvacgprriesgo2;
    @Column(name = "COD_VAC_GPR_RIESGO_2_DESC", length = 2000000000)
    private String codvacgprriesgo2desc;
    @Column(name = "COD_VAC_GPR_RIESGO_3", length = 2000000000)
    private String codvacgprriesgo3;
    @Column(name = "COD_VAC_GPR_RIESGO_3_DESC", length = 2000000000)
    private String codvacgprriesgo3desc;
    @Column(name = "COD_VAC_GPR_RIESGO_4", length = 2000000000)
    private String codvacgprriesgo4;
    @Column(name = "COD_VAC_GPR_RIESGO_4_DESC", length = 2000000000)
    private String codvacgprriesgo4desc;
    @Column(name = "COD_VAC_GPR_RIESGO_5", length = 2000000000)
    private String codvacgprriesgo5;
    @Column(name = "COD_VAC_GPR_RIESGO_5_DESC", length = 2000000000)
    private String codvacgprriesgo5desc;
    

    public VariablesVacunas() {
    }

    public Integer getIdAtencion() {
        return idAtencion;
    }

    public void setIdAtencion(Integer idAtencion) {
        this.idAtencion = idAtencion;
    }

    public String getUuidAtencion() {
        return uuidAtencion;
    }

    public void setUuidAtencion(String uuidAtencion) {
        this.uuidAtencion = uuidAtencion;
    }

    public String getCodVacuna1() {
        return codVacuna1;
    }

    public void setCodVacuna1(String codVacuna1) {
        this.codVacuna1 = codVacuna1;
    }

    public String getCodVacunaDesc1() {
        return codVacunaDesc1;
    }

    public void setCodVacunaDesc1(String codVacunaDesc1) {
        this.codVacunaDesc1 = codVacunaDesc1;
    }

    public String getCodVacunaInfo1() {
        return codVacunaInfo1;
    }

    public void setCodVacunaInfo1(String codVacunaInfo1) {
        this.codVacunaInfo1 = codVacunaInfo1;
    }

    public String getCodVacNroDosis1() {
        return codVacNroDosis1;
    }

    public void setCodVacNroDosis1(String codVacNroDosis1) {
        this.codVacNroDosis1 = codVacNroDosis1;
    }

    public String getVacLote1() {
        return vacLote1;
    }

    public void setVacLote1(String vacLote1) {
        this.vacLote1 = vacLote1;
    }

    public String getCodVacEsquema1() {
        return codVacEsquema1;
    }

    public void setCodVacEsquema1(String codVacEsquema1) {
        this.codVacEsquema1 = codVacEsquema1;
    }

    public String getCodVacEsquemaDesc1() {
        return codVacEsquemaDesc1;
    }

    public void setCodVacEsquemaDesc1(String codVacEsquemaDesc1) {
        this.codVacEsquemaDesc1 = codVacEsquemaDesc1;
    }

    public String getVacFecha1() {
        return vacFecha1;
    }

    public void setVacFecha1(String vacFecha1) {
        this.vacFecha1 = vacFecha1;
    }

    public String getCodVacuna2() {
        return codVacuna2;
    }

    public void setCodVacuna2(String codVacuna2) {
        this.codVacuna2 = codVacuna2;
    }

    public String getCodVacunaDesc2() {
        return codVacunaDesc2;
    }

    public void setCodVacunaDesc2(String codVacunaDesc2) {
        this.codVacunaDesc2 = codVacunaDesc2;
    }

    public String getCodVacunaInfo2() {
        return codVacunaInfo2;
    }

    public void setCodVacunaInfo2(String codVacunaInfo2) {
        this.codVacunaInfo2 = codVacunaInfo2;
    }

    public String getCodVacNroDosis2() {
        return codVacNroDosis2;
    }

    public void setCodVacNroDosis2(String codVacNroDosis2) {
        this.codVacNroDosis2 = codVacNroDosis2;
    }

    public String getVacLote2() {
        return vacLote2;
    }

    public void setVacLote2(String vacLote2) {
        this.vacLote2 = vacLote2;
    }

    public String getCodVacEsquema2() {
        return codVacEsquema2;
    }

    public void setCodVacEsquema2(String codVacEsquema2) {
        this.codVacEsquema2 = codVacEsquema2;
    }

    public String getCodVacEsquemaDesc2() {
        return codVacEsquemaDesc2;
    }

    public void setCodVacEsquemaDesc2(String codVacEsquemaDesc2) {
        this.codVacEsquemaDesc2 = codVacEsquemaDesc2;
    }

    public String getVacFecha2() {
        return vacFecha2;
    }

    public void setVacFecha2(String vacFecha2) {
        this.vacFecha2 = vacFecha2;
    }

    public String getCodVacuna3() {
        return codVacuna3;
    }

    public void setCodVacuna3(String codVacuna3) {
        this.codVacuna3 = codVacuna3;
    }

    public String getCodVacunaDesc3() {
        return codVacunaDesc3;
    }

    public void setCodVacunaDesc3(String codVacunaDesc3) {
        this.codVacunaDesc3 = codVacunaDesc3;
    }

    public String getCodVacunaInfo3() {
        return codVacunaInfo3;
    }

    public void setCodVacunaInfo3(String codVacunaInfo3) {
        this.codVacunaInfo3 = codVacunaInfo3;
    }

    public String getCodVacNroDosis3() {
        return codVacNroDosis3;
    }

    public void setCodVacNroDosis3(String codVacNroDosis3) {
        this.codVacNroDosis3 = codVacNroDosis3;
    }

    public String getVacLote3() {
        return vacLote3;
    }

    public void setVacLote3(String vacLote3) {
        this.vacLote3 = vacLote3;
    }

    public String getCodVacEsquema3() {
        return codVacEsquema3;
    }

    public void setCodVacEsquema3(String codVacEsquema3) {
        this.codVacEsquema3 = codVacEsquema3;
    }

    public String getCodVacEsquemaDesc3() {
        return codVacEsquemaDesc3;
    }

    public void setCodVacEsquemaDesc3(String codVacEsquemaDesc3) {
        this.codVacEsquemaDesc3 = codVacEsquemaDesc3;
    }

    public String getVacFecha3() {
        return vacFecha3;
    }

    public void setVacFecha3(String vacFecha3) {
        this.vacFecha3 = vacFecha3;
    }

    public String getCodVacuna4() {
        return codVacuna4;
    }

    public void setCodVacuna4(String codVacuna4) {
        this.codVacuna4 = codVacuna4;
    }

    public String getCodVacunaDesc4() {
        return codVacunaDesc4;
    }

    public void setCodVacunaDesc4(String codVacunaDesc4) {
        this.codVacunaDesc4 = codVacunaDesc4;
    }

    public String getCodVacunaInfo4() {
        return codVacunaInfo4;
    }

    public void setCodVacunaInfo4(String codVacunaInfo4) {
        this.codVacunaInfo4 = codVacunaInfo4;
    }

    public String getCodVacNroDosis4() {
        return codVacNroDosis4;
    }

    public void setCodVacNroDosis4(String codVacNroDosis4) {
        this.codVacNroDosis4 = codVacNroDosis4;
    }

    public String getVacLote4() {
        return vacLote4;
    }

    public void setVacLote4(String vacLote4) {
        this.vacLote4 = vacLote4;
    }

    public String getCodVacEsquema4() {
        return codVacEsquema4;
    }

    public void setCodVacEsquema4(String codVacEsquema4) {
        this.codVacEsquema4 = codVacEsquema4;
    }

    public String getCodVacEsquemaDesc4() {
        return codVacEsquemaDesc4;
    }

    public void setCodVacEsquemaDesc4(String codVacEsquemaDesc4) {
        this.codVacEsquemaDesc4 = codVacEsquemaDesc4;
    }

    public String getVacFecha4() {
        return vacFecha4;
    }

    public void setVacFecha4(String vacFecha4) {
        this.vacFecha4 = vacFecha4;
    }

    public String getCodVacuna5() {
        return codVacuna5;
    }

    public void setCodVacuna5(String codVacuna5) {
        this.codVacuna5 = codVacuna5;
    }

    public String getCodVacunaDesc5() {
        return codVacunaDesc5;
    }

    public void setCodVacunaDesc5(String codVacunaDesc5) {
        this.codVacunaDesc5 = codVacunaDesc5;
    }

    public String getCodVacunaInfo5() {
        return codVacunaInfo5;
    }

    public void setCodVacunaInfo5(String codVacunaInfo5) {
        this.codVacunaInfo5 = codVacunaInfo5;
    }

    public String getCodVacNroDosis5() {
        return codVacNroDosis5;
    }

    public void setCodVacNroDosis5(String codVacNroDosis5) {
        this.codVacNroDosis5 = codVacNroDosis5;
    }

    public String getVacLote5() {
        return vacLote5;
    }

    public void setVacLote5(String vacLote5) {
        this.vacLote5 = vacLote5;
    }

    public String getCodVacEsquema5() {
        return codVacEsquema5;
    }

    public void setCodVacEsquema5(String codVacEsquema5) {
        this.codVacEsquema5 = codVacEsquema5;
    }

    public String getCodVacEsquemaDesc5() {
        return codVacEsquemaDesc5;
    }

    public void setCodVacEsquemaDesc5(String codVacEsquemaDesc5) {
        this.codVacEsquemaDesc5 = codVacEsquemaDesc5;
    }

    public String getVacFecha5() {
        return vacFecha5;
    }

    public void setVacFecha5(String vacFecha5) {
        this.vacFecha5 = vacFecha5;
    }

	public String getCodvacgprriesgo1() {
		return codvacgprriesgo1;
	}

	public void setCodvacgprriesgo1(String codvacgprriesgo1) {
		this.codvacgprriesgo1 = codvacgprriesgo1;
	}

	public String getCodvacgprriesgo1desc() {
		return codvacgprriesgo1desc;
	}

	public void setCodvacgprriesgo1desc(String codvacgprriesgo1desc) {
		this.codvacgprriesgo1desc = codvacgprriesgo1desc;
	}

	public String getCodvacgprriesgo2() {
		return codvacgprriesgo2;
	}

	public void setCodvacgprriesgo2(String codvacgprriesgo2) {
		this.codvacgprriesgo2 = codvacgprriesgo2;
	}

	public String getCodvacgprriesgo2desc() {
		return codvacgprriesgo2desc;
	}

	public void setCodvacgprriesgo2desc(String codvacgprriesgo2desc) {
		this.codvacgprriesgo2desc = codvacgprriesgo2desc;
	}

	public String getCodvacgprriesgo3() {
		return codvacgprriesgo3;
	}

	public void setCodvacgprriesgo3(String codvacgprriesgo3) {
		this.codvacgprriesgo3 = codvacgprriesgo3;
	}

	public String getCodvacgprriesgo3desc() {
		return codvacgprriesgo3desc;
	}

	public void setCodvacgprriesgo3desc(String codvacgprriesgo3desc) {
		this.codvacgprriesgo3desc = codvacgprriesgo3desc;
	}

	public String getCodvacgprriesgo4() {
		return codvacgprriesgo4;
	}

	public void setCodvacgprriesgo4(String codvacgprriesgo4) {
		this.codvacgprriesgo4 = codvacgprriesgo4;
	}

	public String getCodvacgprriesgo4desc() {
		return codvacgprriesgo4desc;
	}

	public void setCodvacgprriesgo4desc(String codvacgprriesgo4desc) {
		this.codvacgprriesgo4desc = codvacgprriesgo4desc;
	}

	public String getCodvacgprriesgo5() {
		return codvacgprriesgo5;
	}

	public void setCodvacgprriesgo5(String codvacgprriesgo5) {
		this.codvacgprriesgo5 = codvacgprriesgo5;
	}

	public String getCodvacgprriesgo5desc() {
		return codvacgprriesgo5desc;
	}

	public void setCodvacgprriesgo5desc(String codvacgprriesgo5desc) {
		this.codvacgprriesgo5desc = codvacgprriesgo5desc;
	}
    
    
}
