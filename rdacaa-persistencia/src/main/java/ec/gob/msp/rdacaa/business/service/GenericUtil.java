package ec.gob.msp.rdacaa.business.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.data.domain.Sort;
import org.springframework.data.domain.Sort.Order;

public class GenericUtil {

	private GenericUtil() {
	}

	public static Sort orderFieldASC(String... campo) {
		List<Order> listOrder = new ArrayList<>();
		for (int i = 0; i < campo.length; i++) {
			listOrder.add(Order.asc(campo[i]));
		}
		return Sort.by(listOrder);
	}

	public static Sort orderFieldDESC(String... campo) {
		List<Order> listOrder = new ArrayList<>();
		for (int i = 0; i < campo.length; i++) {
			listOrder.add(Order.desc(campo[i]));
		}
		return Sort.by(listOrder);
	}
}
