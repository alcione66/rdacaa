package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "detalleserviciotarifario", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"id"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Detalleserviciotarifario.findAll", query = "SELECT d FROM Detalleserviciotarifario d")})
public class Detalleserviciotarifario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "descripcion", length = 2000)
    private String descripcion;
    @Column(name = "estado")
    private Integer estado;
    @Column(name = "codigo", length = 100)
    private String codigo;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Column(name = "primerniveluvr", precision = 18, scale = 4)
    private BigDecimal primerniveluvr;
    @Column(name = "primernivelfcm", precision = 18, scale = 4)
    private BigDecimal primernivelfcm;
    @Column(name = "primernivelvalor", precision = 18, scale = 4)
    private BigDecimal primernivelvalor;
    @Column(name = "segundoniveluvr", precision = 18, scale = 4)
    private BigDecimal segundoniveluvr;
    @Column(name = "segundonivelfcm", precision = 18, scale = 4)
    private BigDecimal segundonivelfcm;
    @Column(name = "segundonivelvalor", precision = 18, scale = 4)
    private BigDecimal segundonivelvalor;
    @Column(name = "tercerniveluvr", precision = 18, scale = 4)
    private BigDecimal tercerniveluvr;
    @Column(name = "tercernivelfcm", precision = 18, scale = 4)
    private BigDecimal tercernivelfcm;
    @Column(name = "tercernivelvalor", precision = 18, scale = 4)
    private BigDecimal tercernivelvalor;
    @JoinColumn(name = "serviciotarifario_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Serviciotarifario serviciotarifarioId;

    public Detalleserviciotarifario() {
    }

    public Detalleserviciotarifario(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public BigDecimal getPrimerniveluvr() {
        return primerniveluvr;
    }

    public void setPrimerniveluvr(BigDecimal primerniveluvr) {
        this.primerniveluvr = primerniveluvr;
    }

    public BigDecimal getPrimernivelfcm() {
        return primernivelfcm;
    }

    public void setPrimernivelfcm(BigDecimal primernivelfcm) {
        this.primernivelfcm = primernivelfcm;
    }

    public BigDecimal getPrimernivelvalor() {
        return primernivelvalor;
    }

    public void setPrimernivelvalor(BigDecimal primernivelvalor) {
        this.primernivelvalor = primernivelvalor;
    }

    public BigDecimal getSegundoniveluvr() {
        return segundoniveluvr;
    }

    public void setSegundoniveluvr(BigDecimal segundoniveluvr) {
        this.segundoniveluvr = segundoniveluvr;
    }

    public BigDecimal getSegundonivelfcm() {
        return segundonivelfcm;
    }

    public void setSegundonivelfcm(BigDecimal segundonivelfcm) {
        this.segundonivelfcm = segundonivelfcm;
    }

    public BigDecimal getSegundonivelvalor() {
        return segundonivelvalor;
    }

    public void setSegundonivelvalor(BigDecimal segundonivelvalor) {
        this.segundonivelvalor = segundonivelvalor;
    }

    public BigDecimal getTercerniveluvr() {
        return tercerniveluvr;
    }

    public void setTercerniveluvr(BigDecimal tercerniveluvr) {
        this.tercerniveluvr = tercerniveluvr;
    }

    public BigDecimal getTercernivelfcm() {
        return tercernivelfcm;
    }

    public void setTercernivelfcm(BigDecimal tercernivelfcm) {
        this.tercernivelfcm = tercernivelfcm;
    }

    public BigDecimal getTercernivelvalor() {
        return tercernivelvalor;
    }

    public void setTercernivelvalor(BigDecimal tercernivelvalor) {
        this.tercernivelvalor = tercernivelvalor;
    }

    public Serviciotarifario getServiciotarifarioId() {
        return serviciotarifarioId;
    }

    public void setServiciotarifarioId(Serviciotarifario serviciotarifarioId) {
        this.serviciotarifarioId = serviciotarifarioId;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Detalleserviciotarifario other = (Detalleserviciotarifario) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

    @Override
    public String toString() {
        return "ec.gob.msp.rdacaa.business.entity.Detalleserviciotarifario[ id=" + id + " ]";
    }
    
}
