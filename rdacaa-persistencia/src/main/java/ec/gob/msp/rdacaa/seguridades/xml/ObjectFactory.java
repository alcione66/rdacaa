//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2019.01.07 a las 12:19:14 PM ECT 
//


package ec.gob.msp.rdacaa.seguridades.xml;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ec.gob.msp.rdacaa.seguridades.xml package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ec.gob.msp.rdacaa.seguridades.xml
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link ProfileUserPras }
     * 
     */
    public ProfileUserPras createProfileUserPras() {
        return new ProfileUserPras();
    }

    /**
     * Create an instance of {@link ProfileUserPras.PerfilPersona }
     * 
     */
    public ProfileUserPras.PerfilPersona createProfileUserPrasPerfilPersona() {
        return new ProfileUserPras.PerfilPersona();
    }

    /**
     * Create an instance of {@link ProfileUserPras.PerfilPersona.Persona }
     * 
     */
    public ProfileUserPras.PerfilPersona.Persona createProfileUserPrasPerfilPersonaPersona() {
        return new ProfileUserPras.PerfilPersona.Persona();
    }

    /**
     * Create an instance of {@link ProfileUserPras.InfoEntidad }
     * 
     */
    public ProfileUserPras.InfoEntidad createProfileUserPrasInfoEntidad() {
        return new ProfileUserPras.InfoEntidad();
    }

    /**
     * Create an instance of {@link ProfileUserPras.PerfilPersona.Persona.Especialidad }
     * 
     */
    public ProfileUserPras.PerfilPersona.Persona.Especialidad createProfileUserPrasPerfilPersonaPersonaEspecialidad() {
        return new ProfileUserPras.PerfilPersona.Persona.Especialidad();
    }

}
