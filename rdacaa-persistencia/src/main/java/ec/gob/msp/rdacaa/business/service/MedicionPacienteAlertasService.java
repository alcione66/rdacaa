package ec.gob.msp.rdacaa.business.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.msp.rdacaa.business.entity.Detallecatalogo;

@Service
public class MedicionPacienteAlertasService {

	private static final String AMBOS_SEXOS = "A";

	private SignosvitalesreglasService signosvitalesreglasService;

	private List<SignosVitalesReglasValidacion> reglasIndicadorAnemia;
	private List<SignosVitalesReglasValidacion> reglasIMC;

	@Autowired
	public MedicionPacienteAlertasService(SignosvitalesreglasService signosvitalesreglasService) {
		this.signosvitalesreglasService = signosvitalesreglasService;
	}

	@PostConstruct
	private void init() {
		reglasIndicadorAnemia = signosvitalesreglasService.findAllReglasAlertasHBRiesgoCorregido();
		reglasIMC = signosvitalesreglasService.findAllReglasAlertasIMC();
	}

	public SignosVitalesReglasValidacion calculateIndicadorAnemiaHBRiesgoCorregido(String valorHBRiesgoCorregido,
			Integer edadAniosMesDias, String sexo, Integer grupoPrioritario) {
		return findSignosVitalesReglasValidacion(reglasIndicadorAnemia, valorHBRiesgoCorregido, edadAniosMesDias, sexo,
				grupoPrioritario);
	}

	public SignosVitalesReglasValidacion calculateClasificacionIMC(String valorIMC,
			Integer edadAniosMesDias, String sexo) {
		return findSignosVitalesReglasValidacion(reglasIMC, valorIMC, edadAniosMesDias, sexo, null);
	}

	private SignosVitalesReglasValidacion findSignosVitalesReglasValidacion(List<SignosVitalesReglasValidacion> reglas,
			String valor, Integer edadAniosMesDias, String sexo, Integer grupoPrioritario) {

		BigDecimal valorBD = new BigDecimal(valor);

		Optional<SignosVitalesReglasValidacion> alertaFound = reglas.parallelStream().filter(regla -> {
			return isEdadPacienteDentroRango(edadAniosMesDias, regla.getEdadminima(), regla.getEdadmaxima())
					&& isValorDentroRango(valorBD, regla.getValorMinimoAlerta(), regla.getValorMaximoAlerta())
					&& isSexoCorrespondiente(sexo, regla.getSexo())
					&& isGrupoPrioritario(grupoPrioritario, regla.getGrupoPrioritario());
		}).findFirst();

		SignosVitalesReglasValidacion alerta = alertaFound.isPresent() ? alertaFound.get() : null;

		if (alerta == null) {
			throw new SignosVitalesReglasValidacionNotFoundException("Alerta no encontrada");
		}

		return alerta;
	}

	private boolean isGrupoPrioritario(Integer grupoPrioritarioIn, Detallecatalogo grupoPrioritarioRegla) {
		Boolean result = null;
		if (grupoPrioritarioIn != null && grupoPrioritarioRegla != null) {
			result = grupoPrioritarioRegla.getId().equals(grupoPrioritarioIn);
		} else if ((grupoPrioritarioIn == null && grupoPrioritarioRegla != null) || (grupoPrioritarioIn != null)) {
			result = false;
		} else {
			result = true;
		}
		return result;
	}

	private boolean isSexoCorrespondiente(String sexoIn, String sexoRegla) {
		return sexoRegla == null || sexoRegla.equals(sexoIn) || sexoRegla.equals(AMBOS_SEXOS);
	}

	private boolean isEdadPacienteDentroRango(Integer edad, Integer edadMin, Integer edadMax) {
		return edad >= edadMin && edad <= edadMax;
	}

	private boolean isValorDentroRango(BigDecimal valor, BigDecimal imcMin, BigDecimal imcMax) {
		return valor.compareTo(imcMin) >= 0 && valor.compareTo(imcMax) <= 0;
	}

}
