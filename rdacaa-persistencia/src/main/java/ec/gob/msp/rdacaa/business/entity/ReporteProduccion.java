package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Immutable;

/**
 *
 * @author msp
 */
@Entity
@Immutable
@Table(name = "REPORTE_PRODUCCION")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "ReporteProduccion.findAll", query = "SELECT r FROM ReporteProduccion r")})
public class ReporteProduccion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID")
    private Integer id;
    @Column(name = "CODIGOESTABLECIMIENTO", length = 2000000000)
    private String codigoestablecimiento;
    @Column(name = "NOMBREESTABLECIMIENTO", length = 2000000000)
    private String nombreestablecimiento;
    @Column(name = "NOMBREPROFESIONAL", length = 2000000000)
    private String nombreprofesional;
    @Column(name = "ESPECIALIDADID", length = 2000000000)
    private Integer especialidadId;
    @Column(name = "ESPECIALIDAPROFESIONAL", length = 2000000000)
    private String especialidaprofesional;
    @Column(name = "PREVENCIONPRIMERA", length = 2000000000)
    private String prevencionprimera;
    @Column(name = "PREVENCIONSUBSECUENTE", length = 2000000000)
    private String prevencionsubsecuente;
    @Column(name = "MORBILIDADPRIMERA", length = 2000000000)
    private String morbilidadprimera;
    @Column(name = "MORBILIDADSUBSECUENTE", length = 2000000000)
    private String morbilidadsubsecuente;
    @Column(name = "VACUNAS", length = 2000000000)
    private String vacunas;
    @Column(name = "FECHAATENCION", length = 2000000000)
    private String fechaatencion;
    @Column(name = "USUARIOCREACION_ID")
    private Integer usuariocreacionId;

    public ReporteProduccion() {
    }

    public String getCodigoestablecimiento() {
        return codigoestablecimiento;
    }

    public void setCodigoestablecimiento(String codigoestablecimiento) {
        this.codigoestablecimiento = codigoestablecimiento;
    }

    public String getNombreestablecimiento() {
        return nombreestablecimiento;
    }

    public void setNombreestablecimiento(String nombreestablecimiento) {
        this.nombreestablecimiento = nombreestablecimiento;
    }

    public String getNombreprofesional() {
        return nombreprofesional;
    }

    public void setNombreprofesional(String nombreprofesional) {
        this.nombreprofesional = nombreprofesional;
    }

    public String getEspecialidaprofesional() {
        return especialidaprofesional;
    }

    public void setEspecialidaprofesional(String especialidaprofesional) {
        this.especialidaprofesional = especialidaprofesional;
    }

    public String getPrevencionprimera() {
        return prevencionprimera;
    }

    public void setPrevencionprimera(String prevencionprimera) {
        this.prevencionprimera = prevencionprimera;
    }

    public String getPrevencionsubsecuente() {
        return prevencionsubsecuente;
    }

    public void setPrevencionsubsecuente(String prevencionsubsecuente) {
        this.prevencionsubsecuente = prevencionsubsecuente;
    }

    public String getMorbilidadprimera() {
        return morbilidadprimera;
    }

    public void setMorbilidadprimera(String morbilidadprimera) {
        this.morbilidadprimera = morbilidadprimera;
    }

    public String getMorbilidadsubsecuente() {
        return morbilidadsubsecuente;
    }

    public void setMorbilidadsubsecuente(String morbilidadsubsecuente) {
        this.morbilidadsubsecuente = morbilidadsubsecuente;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getFechaatencion() {
        return fechaatencion;
    }

    public void setFechaatencion(String fechaatencion) {
        this.fechaatencion = fechaatencion;
    }

    public Integer getUsuariocreacionId() {
        return usuariocreacionId;
    }

    public void setUsuariocreacionId(Integer usuariocreacionId) {
        this.usuariocreacionId = usuariocreacionId;
    }
    
    

	public Integer getEspecialidadId() {
		return especialidadId;
	}

	public void setEspecialidadId(Integer especialidadId) {
		this.especialidadId = especialidadId;
	}

	public String getVacunas() {
		return vacunas;
	}

	public void setVacunas(String vacunas) {
		this.vacunas = vacunas;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ReporteProduccion other = (ReporteProduccion) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}
    
}
