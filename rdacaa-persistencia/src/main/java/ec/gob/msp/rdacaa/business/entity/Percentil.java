/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "percentil", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"id"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Percentil.findAll", query = "SELECT p FROM Percentil p")})
public class Percentil implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "medida", length = 15)
    private String medida;
    @Column(name = "sexo", length = 2)
    private String sexo;
    @Column(name = "edadminima", length = 10)
    private String edadminima;
    @Column(name = "edadmaxima", length = 10)
    private String edadmaxima;
    @JoinColumn(name = "ctsignovital_id", referencedColumnName = "id")
    @ManyToOne
    private Detallecatalogo ctsignovitalId;

    public Percentil() {
    }

    public Percentil(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMedida() {
        return medida;
    }

    public void setMedida(String medida) {
        this.medida = medida;
    }

    public String getSexo() {
        return sexo;
    }

    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    public String getEdadminima() {
        return edadminima;
    }

    public void setEdadminima(String edadminima) {
        this.edadminima = edadminima;
    }

    public String getEdadmaxima() {
        return edadmaxima;
    }

    public void setEdadmaxima(String edadmaxima) {
        this.edadmaxima = edadmaxima;
    }

    public Detallecatalogo getCtsignovitalId() {
        return ctsignovitalId;
    }

    public void setCtsignovitalId(Detallecatalogo ctsignovitalId) {
        this.ctsignovitalId = ctsignovitalId;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Percentil other = (Percentil) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

    @Override
    public String toString() {
        return "ec.gob.msp.rdacaa.business.entity.Percentil[ id=" + id + " ]";
    }
    
}
