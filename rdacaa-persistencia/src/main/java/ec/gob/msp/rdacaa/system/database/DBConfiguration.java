package ec.gob.msp.rdacaa.system.database;

import javax.sql.DataSource;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;

@Configuration
@PropertySource(value = { "classpath:database-config-${rdacaa.profiles.active:default}.properties" })
public class DBConfiguration {

	private static final Logger logger = LoggerFactory.getLogger(DBConfiguration.class);

	@Autowired
	private Environment env;

	@Bean
	public DataSource dataSource() {
		DataSourceBuilder<?> dataSourceBuilder = DataSourceBuilder.create();
		dataSourceBuilder.driverClassName(env.getProperty("rdacaa.datasource.driver-class-name"));
		if (OS.UNIX.equals(OSInfo.getOs())) {
			dataSourceBuilder.url(env.getProperty("rdacaa.datasource.url.unix"));
			logger.info("rdacaa.datasource.url.unix {}", env.getProperty("rdacaa.datasource.url.unix"));
		} else if (OS.WINDOWS.equals(OSInfo.getOs())) {
			dataSourceBuilder.url(env.getProperty("rdacaa.datasource.url.windows"));
			logger.info("rdacaa.datasource.url.windows {}", env.getProperty("rdacaa.datasource.url.windows"));
		}
		return dataSourceBuilder.build();
	}
}
