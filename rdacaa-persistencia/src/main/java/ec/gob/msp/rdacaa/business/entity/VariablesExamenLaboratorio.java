/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Immutable;

/**
 *
 * @author msp
 */
@Entity
@Immutable
@Table(name = "VARIABLES_EXAMEN_LABORATORIO", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VariablesExamenLaboratorio.findAll", query = "SELECT v FROM VariablesExamenLaboratorio v")})
public class VariablesExamenLaboratorio implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID_ATENCION")
    private Integer idAtencion;
    @Column(name = "UUID_ATENCION", length = 2000000000)
    private String uuidAtencion;
    @Column(name = "COD_EXA_LAB_SIFILIS_NO_TREPONEMICA")
    private Integer codExaLabSifilisNoTreponemica;
    @Column(name = "COD_EXA_LAB_SIFILIS_NO_TREPONEMICA_DESC", length = 2000000000)
    private String codExaLabSifilisNoTreponemicaDesc;
    @Column(name = "COD_EXA_LAB_SIFILIS_TREPONEMICA_DESC", length = 2000000000)
    private String codExaLabSifilisTreponemicaDesc;
    @Column(name = "COD_EXA_LAB_SIFILIS_TREPONEMICA")
    private Integer codExaLabSifilisTreponemica;
    @Column(name = "COD_EXA_LAB_SIFILIS_TRATAMIENTO")
    private Integer codExaLabSifilisTratamiento;
    @Column(name = "COD_EXA_LAB_SIFILIS_TRATAMIENTO_DESC", length = 2000000000)
    private String codExaLabSifilisTratamientoDesc;
    @Column(name = "COD_EXA_LAB_SIFILIS_TRATAMIENTO_PAREJA")
    private Integer codExaLabSifilisTratamientoPareja;
    @Column(name = "COD_EXA_LAB_SIFILIS_TRATAMIENTO_PAREJA_DESC", length = 2000000000)
    private String codExaLabSifilisTratamientoParejaDesc;
    @Column(name = "COD_RES_BACTERIURIA")
    private Integer codResBacteriuria;
    @Column(name = "COD_RES_BACTERIURIA_DESC", length = 2000000000)
    private String codResBacteriuriaDesc;

    public VariablesExamenLaboratorio() {
    }

    public Integer getIdAtencion() {
        return idAtencion;
    }

    public void setIdAtencion(Integer idAtencion) {
        this.idAtencion = idAtencion;
    }

    public String getUuidAtencion() {
        return uuidAtencion;
    }

    public void setUuidAtencion(String uuidAtencion) {
        this.uuidAtencion = uuidAtencion;
    }

    public Integer getCodExaLabSifilisNoTreponemica() {
        return codExaLabSifilisNoTreponemica;
    }

    public void setCodExaLabSifilisNoTreponemica(Integer codExaLabSifilisNoTreponemica) {
        this.codExaLabSifilisNoTreponemica = codExaLabSifilisNoTreponemica;
    }

    public String getCodExaLabSifilisTreponemicaDesc() {
        return codExaLabSifilisTreponemicaDesc;
    }

    public void setCodExaLabSifilisTreponemicaDesc(String codExaLabSifilisTreponemicaDesc) {
        this.codExaLabSifilisTreponemicaDesc = codExaLabSifilisTreponemicaDesc;
    }

    public Integer getCodExaLabSifilisTreponemica() {
        return codExaLabSifilisTreponemica;
    }

    public void setCodExaLabSifilisTreponemica(Integer codExaLabSifilisTreponemica) {
        this.codExaLabSifilisTreponemica = codExaLabSifilisTreponemica;
    }

    public Integer getCodExaLabSifilisTratamiento() {
        return codExaLabSifilisTratamiento;
    }

    public void setCodExaLabSifilisTratamiento(Integer codExaLabSifilisTratamiento) {
        this.codExaLabSifilisTratamiento = codExaLabSifilisTratamiento;
    }

    public String getCodExaLabSifilisTratamientoDesc() {
        return codExaLabSifilisTratamientoDesc;
    }

    public void setCodExaLabSifilisTratamientoDesc(String codExaLabSifilisTratamientoDesc) {
        this.codExaLabSifilisTratamientoDesc = codExaLabSifilisTratamientoDesc;
    }

    public Integer getCodExaLabSifilisTratamientoPareja() {
        return codExaLabSifilisTratamientoPareja;
    }

    public void setCodExaLabSifilisTratamientoPareja(Integer codExaLabSifilisTratamientoPareja) {
        this.codExaLabSifilisTratamientoPareja = codExaLabSifilisTratamientoPareja;
    }

    public String getCodExaLabSifilisTratamientoParejaDesc() {
        return codExaLabSifilisTratamientoParejaDesc;
    }

    public void setCodExaLabSifilisTratamientoParejaDesc(String codExaLabSifilisTratamientoParejaDesc) {
        this.codExaLabSifilisTratamientoParejaDesc = codExaLabSifilisTratamientoParejaDesc;
    }

    public Integer getCodResBacteriuria() {
        return codResBacteriuria;
    }

    public void setCodResBacteriuria(Integer codResBacteriuria) {
        this.codResBacteriuria = codResBacteriuria;
    }

    public String getCodResBacteriuriaDesc() {
        return codResBacteriuriaDesc;
    }

    public void setCodResBacteriuriaDesc(String codResBacteriuriaDesc) {
        this.codResBacteriuriaDesc = codResBacteriuriaDesc;
    }

	public String getCodExaLabSifilisNoTreponemicaDesc() {
		return codExaLabSifilisNoTreponemicaDesc;
	}

	public void setCodExaLabSifilisNoTreponemicaDesc(String codExaLabSifilisNoTreponemicaDesc) {
		this.codExaLabSifilisNoTreponemicaDesc = codExaLabSifilisNoTreponemicaDesc;
	}
    
}
