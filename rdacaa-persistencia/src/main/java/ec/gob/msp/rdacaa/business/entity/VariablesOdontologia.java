/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Immutable;

/**
 *
 * @author msp
 */
@Entity
@Immutable
@Table(name = "VARIABLES_ODONTOLOGIA", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VariablesOdontologia.findAll", query = "SELECT v FROM VariablesOdontologia v")})
public class VariablesOdontologia implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID_ATENCION")
    private Integer idAtencion;
    @Column(name = "UUID_ATENCION", length = 2000000000)
    private String uuidAtencion;
    @Column(name = "ODON_DIE_CARIADOS")
    private Integer odonDieCariados;
    @Column(name = "ODON_DIE_EXTRAIDO")
    private Integer odonDieExtraido;
    @Column(name = "ODON_DIE_OBTURADO")
    private Integer odonDieObturado;
    @Column(name = "ODON_DIE_PERDIDO")
    private Integer odonDiePerdido;

    public VariablesOdontologia() {
    }

    public Integer getIdAtencion() {
        return idAtencion;
    }

    public void setIdAtencion(Integer idAtencion) {
        this.idAtencion = idAtencion;
    }

    public String getUuidAtencion() {
        return uuidAtencion;
    }

    public void setUuidAtencion(String uuidAtencion) {
        this.uuidAtencion = uuidAtencion;
    }

    public Integer getOdonDieCariados() {
        return odonDieCariados;
    }

    public void setOdonDieCariados(Integer odonDieCariados) {
        this.odonDieCariados = odonDieCariados;
    }

    public Integer getOdonDieExtraido() {
        return odonDieExtraido;
    }

    public void setOdonDieExtraido(Integer odonDieExtraido) {
        this.odonDieExtraido = odonDieExtraido;
    }

    public Integer getOdonDieObturado() {
        return odonDieObturado;
    }

    public void setOdonDieObturado(Integer odonDieObturado) {
        this.odonDieObturado = odonDieObturado;
    }

    public Integer getOdonDiePerdido() {
        return odonDiePerdido;
    }

    public void setOdonDiePerdido(Integer odonDiePerdido) {
        this.odonDiePerdido = odonDiePerdido;
    }
    
}
