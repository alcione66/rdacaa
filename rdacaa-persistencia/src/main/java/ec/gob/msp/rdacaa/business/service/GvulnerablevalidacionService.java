package ec.gob.msp.rdacaa.business.service;

import com.querydsl.core.types.dsl.BooleanExpression;
import ec.gob.msp.rdacaa.business.entity.Detallecatalogo;
import ec.gob.msp.rdacaa.business.entity.Gvulnerablevalidacion;
import ec.gob.msp.rdacaa.business.entity.QGvulnerablevalidacion;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.msp.rdacaa.business.repository.GvulnerablevalidacionRepository;
import java.util.ArrayList;
import java.util.List;
import org.apache.commons.lang3.StringUtils;

/**
 * Generated by Spring Data Generator on 17/10/2018
 */
@Service
public class GvulnerablevalidacionService {

	private static final int HOMBRE_ID = 16;
	private GvulnerablevalidacionRepository gvulnerablevalidacionRepository;

	@Autowired
	public GvulnerablevalidacionService(GvulnerablevalidacionRepository gvulnerablevalidacionRepository) {
		this.gvulnerablevalidacionRepository = gvulnerablevalidacionRepository;
	}

	public List<Detallecatalogo> findGruposVulnerablesFiltrados(Integer edadAnios, Integer edadMeses, Integer edadDias,
			Integer sexoId, Integer embarazaId) {
		List<Detallecatalogo> gruposVulnerables = new ArrayList<>();
		QGvulnerablevalidacion aGvulnerablevalidacion = QGvulnerablevalidacion.gvulnerablevalidacion;
		BooleanExpression isSexoHombreMujer = null;

		if (sexoId.equals(HOMBRE_ID)) {
			isSexoHombreMujer = aGvulnerablevalidacion.hombre.eq(1);
		} else if (embarazaId == 1) {
			isSexoHombreMujer = aGvulnerablevalidacion.mujer.eq(1).or(aGvulnerablevalidacion.embarazada.eq(embarazaId));
		} else {
			isSexoHombreMujer = aGvulnerablevalidacion.mujer.eq(1)
					.and(aGvulnerablevalidacion.embarazada.eq(embarazaId));
		}
		Integer edadIn = concatenarEdad(edadAnios, edadMeses, edadDias);

		Iterable<Gvulnerablevalidacion> gpValidacionLista = gvulnerablevalidacionRepository.findAll(isSexoHombreMujer,
				GenericUtil.orderFieldASC("id"));
		for (Gvulnerablevalidacion gvulnerablevalidacion : gpValidacionLista) {
			Integer edadMinValidacion = concatenarEdad(gvulnerablevalidacion.getEdadminanios(),
					gvulnerablevalidacion.getEdadminmeses(), gvulnerablevalidacion.getEdadmindias());
			Integer edadMaxValidacion = concatenarEdad(gvulnerablevalidacion.getEdadmaxanios(),
					gvulnerablevalidacion.getEdadmaxmeses(), gvulnerablevalidacion.getEdadmaxdias());
			if (edadIn >= edadMinValidacion && edadIn <= edadMaxValidacion) {
				gruposVulnerables.add(gvulnerablevalidacion.getCtgvulnerableId());
			}
		}
		return gruposVulnerables;
	}

	private Integer concatenarEdad(Integer edadAnios, Integer edadMeses, Integer edadDias) {
		String edadString = StringUtils.leftPad(edadAnios.toString(), 3, "0")
				+ StringUtils.leftPad(edadMeses.toString(), 2, "0") + StringUtils.leftPad(edadDias.toString(), 2, "0");
		return Integer.parseInt(edadString);

	}
}
