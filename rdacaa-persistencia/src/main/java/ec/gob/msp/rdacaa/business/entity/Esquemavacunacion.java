package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "esquemavacunacion", uniqueConstraints = { @UniqueConstraint(columnNames = { "id" }) })
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Esquemavacunacion.findAll", query = "SELECT e FROM Esquemavacunacion e") })
public class Esquemavacunacion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "dosis")
    private String dosis;
    @Column(name = "edadmindia")
    private Integer edadmindia;
    @Column(name = "edadminmes")
    private Integer edadminmes;
    @Column(name = "edadminanio")
    private Integer edadminanio;
    @Column(name = "edadmaxdia")
    private Integer edadmaxdia;
    @Column(name = "edadmaxmes")
    private Integer edadmaxmes;
    @Column(name = "edadmaxanio")
    private Integer edadmaxanio;
    @JoinColumn(name = "cttipoesquema_id", referencedColumnName = "id")
    @ManyToOne
    private Detallecatalogo cttipoesquemaId;
    @JoinColumn(name = "vacuna_id", referencedColumnName = "id")
    @ManyToOne
    private Vacuna vacunaId;

    public Esquemavacunacion() {
    }

    public Esquemavacunacion(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDosis() {
        return dosis;
    }

    public void setDosis(String dosis) {
        this.dosis = dosis;
    }

    public Integer getEdadmindia() {
        return edadmindia;
    }

    public void setEdadmindia(Integer edadmindia) {
        this.edadmindia = edadmindia;
    }

    public Integer getEdadminmes() {
        return edadminmes;
    }

    public void setEdadminmes(Integer edadminmes) {
        this.edadminmes = edadminmes;
    }

    public Integer getEdadminanio() {
        return edadminanio;
    }

    public void setEdadminanio(Integer edadminanio) {
        this.edadminanio = edadminanio;
    }

    public Integer getEdadmaxdia() {
        return edadmaxdia;
    }

    public void setEdadmaxdia(Integer edadmaxdia) {
        this.edadmaxdia = edadmaxdia;
    }

    public Integer getEdadmaxmes() {
        return edadmaxmes;
    }

    public void setEdadmaxmes(Integer edadmaxmes) {
        this.edadmaxmes = edadmaxmes;
    }

    public Integer getEdadmaxanio() {
        return edadmaxanio;
    }

    public void setEdadmaxanio(Integer edadmaxanio) {
        this.edadmaxanio = edadmaxanio;
    }

    public Detallecatalogo getCttipoesquemaId() {
        return cttipoesquemaId;
    }

    public void setCttipoesquemaId(Detallecatalogo cttipoesquemaId) {
        this.cttipoesquemaId = cttipoesquemaId;
    }

    public Vacuna getVacunaId() {
        return vacunaId;
    }

    public void setVacunaId(Vacuna vacunaId) {
        this.vacunaId = vacunaId;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Esquemavacunacion other = (Esquemavacunacion) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

    @Override
    public String toString() {
        return "ec.gob.msp.rdacaa.business.entity.Esquemavacunacion[ id=" + id + " ]";
    }
    
}
