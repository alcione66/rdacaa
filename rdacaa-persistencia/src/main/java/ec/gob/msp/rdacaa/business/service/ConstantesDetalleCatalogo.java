/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.service;

/**
 *
 * @author msp
 */
public class ConstantesDetalleCatalogo {

	private ConstantesDetalleCatalogo() {
	}

	public static final int TIPOS_DE_IDENTIFICACION = 3;
	public static final int SEXO = 5;
	public static final int NACIONALIDADES = 7;
	public static final int PUEBLOS_KICHWA = 32;
	public static final int ORIENTACION_SEXUAL = 36;
	public static final int IDENTIDAD_DE_GENERO = 37;
	public static final int AUTOIDENTIFICACION_ETNICA = 6;
	public static final int TIPO_BONO_ESTADO = 12;
	public static final int TIPO_SEGURO_SALUD = 11;
	public static final int AREA_RESIDENCIA = 9;
	public static final int REGIONES_NATURALES = 15;
	public static final int ESPECIALIDADES_MEDICAS = 16;
	public static final int GRUPOS_PRIORITARIOS = 35;
	public static final int TIPO_PERSONA = 131;
	public static final int CONDICION_DIAGNOSTICO = 23;
	public static final int FORMACION_PROFESIONAL = 132;
	public static final int LUGAR_DE_ATENCION = 42;
	public static final int TIPO_PACIENTE_ID = 2479;
	public static final int TIPO_PROFESIONAL_ID = 2480;
	public static final int TIPO_USUARIO_SISTEMA_ID = 2481;
	public static final int CATEG_RIESGO_OBSTETRICO_ID = 135;
	public static final int HOMBRE_ID = 16;
	public static final int MUJER_ID = 17;
	public static final int INTERSEXUAL_ID = 114;
	public static final int EDAD_IG = 10;
	public static final String ORIENTACION_MUJER = "Lesbiana";
	public static final String ORIENTACION_HOMBRE = "Gay";
	public static final int METODOS_SEMANAS_GESTACION = 136;
	public static final int TIPO_MEDICION_PESO = 461;
	public static final int TIPO_MEDICION_TALLA = 462;
	public static final int TIPO_MEDICION_PERIMETRO_CEFALICO = 463;
	public static final int TIPO_MEDICION_TALLA_CORREGIDA = 1459;
//	public static final int TIPO_TOMA_TALLA_DE_PIE = ;
//	public static final int TIPO_TOMA_TALLA_ACOSTADO = ;
	public static final int TIPO_MEDICION_IMC = 467;
	public static final int TIPO_MEDICION_HB_RIESGO = 1157;
	public static final int TIPO_MEDICION_HB_RIESGO_CORREGIDO = 1158;
	public static final int TIPO_MEDICION_TALLA_PARA_EDAD = 1173;
	public static final int TIPO_MEDICION_PESO_PARA_EDAD = 1174;
	public static final int TIPO_MEDICION_PESO_PARA_LONGITUD_TALLA = 1175;
	public static final int TIPO_MEDICION_IMC_PARA_EDAD = 1176;
	public static final int TIPO_MEDICION_PERIMETRO_CEFALICO_PARA_EDAD = 1749;
	public static final int VACUNAS = 130;
	public static final int DOSIS_ROTAVIRUS = 2469;
	public static final int DOSIS_NEUMOCOCO = 2470;
	public static final int MOTIVO_PRUEBA_VIH_ID = 128;
	public static final int VIA_TRANSMISION_VIH_ID = 129;
	public static final int TIPO_ATENCION = 140;
	public static final int TIPO_ATENCION_DIAGNOSTICO = 25;
	public static final int TIPO_DIAGNOSTICO = 24;
	public static final int PROCEDIMIENTOS = 133;
	public static final int RESULTADO_EXAMEN_BACTERIURIA = 137;
	public static final int SUBSISTEMA_R_C = 138;
	public static final int TIPO_INTERCONSULTA = 139;
	public static final int DOSIS_SARAMPION = 2476;
	public static final int ESTRATEGIAMSP = 110;
	public static final int DETALLECATALOGOCEDULA = 6;
	public static final int DETALLECATALOGONOIDENTIFICADO = 5;
	public static final int LESIONES_OCURRIDAS_AGRESOR = 145;
	public static final int PARENTESCO_AGRESOR = 143;
	public static final int IDENTIFICA_AGRESOR = 146;
    public static final int GRUPOS_VULNERABLES = 149; 
    public static final int PRUEBAS_VIH = 141; 
    public static final int TIP_DIAG_PREVENCION = 227;
    public static final int TIP_DIAG_MORBILIDAD = 228;
    public static final String ECUADOR = "EC";
    public static final String NOT_DEFINED_VALUE = "999990000066666";
    public static final String CIE_CERTIFICADO_MEDICO_UNICO = "Z027";
    public static final int NACIONALIDADECUADOR = 1;
    public static final String NOAPLICAAUTOIDENTIFICACIONETNICA = "485";
    public static final int TIPO_ATENCION_PRIMERA = 710;
    public static final int TIPO_ATENCION_SUBSECUENTE = 711;
    public static final int GRUPO_RIESGO_X_VACUNA = 53;
}
