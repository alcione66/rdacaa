package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "variablexcondicion", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"id"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Variablexcondicion.findAll", query = "SELECT v FROM Variablexcondicion v")})
public class Variablexcondicion implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "edad_minima")
    private Integer edadMinima;
    @Column(name = "edad_maxima")
    private Integer edadMaxima;
    @Column(name = "ismandatorio")
    private Integer ismandatorio;
    @Column(name = "estado")
    private Integer estado;
    @JoinColumn(name = "ctespecialidad_id", referencedColumnName = "id")
    @ManyToOne
    private Detallecatalogo ctespecialidadId;
    @JoinColumn(name = "ctgprioritario", referencedColumnName = "id")
    @ManyToOne
    private Detallecatalogo ctgprioritario;
    @JoinColumn(name = "ctsexo_id", referencedColumnName = "id")
    @ManyToOne
    private Detallecatalogo ctsexoId;
    @JoinColumn(name = "variable_id", referencedColumnName = "id")
    @ManyToOne
    private Variable variableId;
    @JoinColumn(name = "variabledependiente_id", referencedColumnName = "id")
    @ManyToOne
    private Variable variabledependienteId;

    public Variablexcondicion() {
    }

    public Variablexcondicion(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEdadMinima() {
        return edadMinima;
    }

    public void setEdadMinima(Integer edadMinima) {
        this.edadMinima = edadMinima;
    }

    public Integer getEdadMaxima() {
        return edadMaxima;
    }

    public void setEdadMaxima(Integer edadMaxima) {
        this.edadMaxima = edadMaxima;
    }

    public Integer getIsmandatorio() {
        return ismandatorio;
    }

    public void setIsmandatorio(Integer ismandatorio) {
        this.ismandatorio = ismandatorio;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Detallecatalogo getCtespecialidadId() {
        return ctespecialidadId;
    }

    public void setCtespecialidadId(Detallecatalogo ctespecialidadId) {
        this.ctespecialidadId = ctespecialidadId;
    }

    public Detallecatalogo getCtgprioritario() {
        return ctgprioritario;
    }

    public void setCtgprioritario(Detallecatalogo ctgprioritario) {
        this.ctgprioritario = ctgprioritario;
    }

    public Detallecatalogo getCtsexoId() {
        return ctsexoId;
    }

    public void setCtsexoId(Detallecatalogo ctsexoId) {
        this.ctsexoId = ctsexoId;
    }

    public Variable getVariableId() {
        return variableId;
    }

    public void setVariableId(Variable variableId) {
        this.variableId = variableId;
    }

    public Variable getVariabledependienteId() {
        return variabledependienteId;
    }

    public void setVariabledependienteId(Variable variabledependienteId) {
        this.variabledependienteId = variabledependienteId;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Variablexcondicion other = (Variablexcondicion) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

    @Override
    public String toString() {
        return "ec.gob.msp.rdacaa.business.entity.Variablexcondicion[ id=" + id + " ]";
    }
    
}
