package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "tipoestablecimiento", catalog = "rdacaa", schema = "public")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tipoestablecimiento.findAll", query = "SELECT t FROM Tipoestablecimiento t")})
public class Tipoestablecimiento implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "descripcion", length = 255)
    private String descripcion;
    @Column(name = "simbologia", length = 20)
    private String simbologia;
    @Column(name = "nivel")
    private Integer nivel;
    @Column(name = "estado")
    private Integer estado;
    @OneToMany(mappedBy = "tipoestablecimientoId")
    private List<Tipoestablecimiento> tipoestablecimientoList;
    @JoinColumn(name = "tipoestablecimiento_id", referencedColumnName = "id")
    @ManyToOne
    private Tipoestablecimiento tipoestablecimientoId;
    @OneToMany(mappedBy = "tipoestablecimientoId")
    private List<Entidad> entidadList;

    public Tipoestablecimiento() {
    }

    public Tipoestablecimiento(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getSimbologia() {
        return simbologia;
    }

    public void setSimbologia(String simbologia) {
        this.simbologia = simbologia;
    }

    public Integer getNivel() {
        return nivel;
    }

    public void setNivel(Integer nivel) {
        this.nivel = nivel;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    @XmlTransient
    public List<Tipoestablecimiento> getTipoestablecimientoList() {
        return tipoestablecimientoList;
    }

    public void setTipoestablecimientoList(List<Tipoestablecimiento> tipoestablecimientoList) {
        this.tipoestablecimientoList = tipoestablecimientoList;
    }

    public Tipoestablecimiento getTipoestablecimientoId() {
        return tipoestablecimientoId;
    }

    public void setTipoestablecimientoId(Tipoestablecimiento tipoestablecimientoId) {
        this.tipoestablecimientoId = tipoestablecimientoId;
    }

    @XmlTransient
    public List<Entidad> getEntidadList() {
        return entidadList;
    }

    public void setEntidadList(List<Entidad> entidadList) {
        this.entidadList = entidadList;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tipoestablecimiento other = (Tipoestablecimiento) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

    @Override
    public String toString() {
        return "ec.gob.msp.rdacaa.business.entity.Tipoestablecimiento[ id=" + id + " ]";
    }
    
}
