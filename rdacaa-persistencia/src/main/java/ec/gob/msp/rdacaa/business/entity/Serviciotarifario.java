package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "serviciotarifario", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"id"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Serviciotarifario.findAll", query = "SELECT s FROM Serviciotarifario s")})
public class Serviciotarifario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "descripcion", length = 2000)
    private String descripcion;
    @Column(name = "estado")
    private Integer estado;
    @OneToMany(mappedBy = "serviciotarifarioId")
    private List<Serviciotarifario> serviciotarifarioList;
    @JoinColumn(name = "serviciotarifario_id", referencedColumnName = "id")
    @ManyToOne
    private Serviciotarifario serviciotarifarioId;
    @JoinColumn(name = "tarifario_id", referencedColumnName = "id", nullable = false)
    @ManyToOne(optional = false)
    private Tarifario tarifarioId;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "serviciotarifarioId")
    private List<Detalleserviciotarifario> detalleserviciotarifarioList;

    public Serviciotarifario() {
    }

    public Serviciotarifario(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    @XmlTransient
    public List<Serviciotarifario> getServiciotarifarioList() {
        return serviciotarifarioList;
    }

    public void setServiciotarifarioList(List<Serviciotarifario> serviciotarifarioList) {
        this.serviciotarifarioList = serviciotarifarioList;
    }

    public Serviciotarifario getServiciotarifarioId() {
        return serviciotarifarioId;
    }

    public void setServiciotarifarioId(Serviciotarifario serviciotarifarioId) {
        this.serviciotarifarioId = serviciotarifarioId;
    }

    public Tarifario getTarifarioId() {
        return tarifarioId;
    }

    public void setTarifarioId(Tarifario tarifarioId) {
        this.tarifarioId = tarifarioId;
    }

    @XmlTransient
    public List<Detalleserviciotarifario> getDetalleserviciotarifarioList() {
        return detalleserviciotarifarioList;
    }

    public void setDetalleserviciotarifarioList(List<Detalleserviciotarifario> detalleserviciotarifarioList) {
        this.detalleserviciotarifarioList = detalleserviciotarifarioList;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Serviciotarifario other = (Serviciotarifario) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

    @Override
    public String toString() {
        return "ec.gob.msp.rdacaa.business.entity.Serviciotarifario[ id=" + id + " ]";
    }
    
}
