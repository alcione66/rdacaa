/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.seguridades;

import java.io.File;
import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;

import ec.gob.msp.rdacaa.seguridades.xml.ProfileUserPras;

/**
 *
 * @author dmurillo
 */
public class AccesoXmlService {
	
	private AccesoXmlService() {}
    
    public static ProfileUserPras processAcceso(File archivo) throws JAXBException{
        JAXBContext context = JAXBContext.newInstance(ProfileUserPras.class);
        return (ProfileUserPras) context.createUnmarshaller().unmarshal(archivo);
    }
}
