package ec.gob.msp.rdacaa.business.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.querydsl.QuerydslPredicateExecutor;
import org.springframework.stereotype.Repository;

import ec.gob.msp.rdacaa.business.entity.Detallecatalogo;

/**
 * Generated by Spring Data Generator on 20/06/2018
 */
@Repository
public interface DetallecatalogoRepository extends JpaRepository<Detallecatalogo, Integer>,
		JpaSpecificationExecutor<Detallecatalogo>, QuerydslPredicateExecutor<Detallecatalogo> {

}
