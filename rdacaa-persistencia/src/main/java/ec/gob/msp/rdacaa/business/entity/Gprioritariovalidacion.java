package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "gprioritariovalidacion")
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Gprioritariovalidacion.findAll", query = "SELECT g FROM Gprioritariovalidacion g") })
public class Gprioritariovalidacion implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
        @GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "edadminanios")
	private Integer edadminanios;
	@Column(name = "edadminmeses")
	private Integer edadminmeses;
	@Column(name = "edadmindias")
	private Integer edadmindias;
	@Column(name = "edadmaxanios")
	private Integer edadmaxanios;
	@Column(name = "edadmaxmeses")
	private Integer edadmaxmeses;
	@Column(name = "edadmaxdias")
	private Integer edadmaxdias;
	@Column(name = "hombre")
	private Integer hombre;
	@Column(name = "mujer")
	private Integer mujer;
	@Column(name = "estado")
	private Integer estado;
	@JoinColumn(name = "ctgprioritario_id", referencedColumnName = "id")
	@ManyToOne
	private Detallecatalogo ctgprioritarioId;

	public Gprioritariovalidacion() {
	}

	public Gprioritariovalidacion(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getEdadminanios() {
		return edadminanios;
	}

	public void setEdadminanios(Integer edadminanios) {
		this.edadminanios = edadminanios;
	}

	public Integer getEdadminmeses() {
		return edadminmeses;
	}

	public void setEdadminmeses(Integer edadminmeses) {
		this.edadminmeses = edadminmeses;
	}

	public Integer getEdadmindias() {
		return edadmindias;
	}

	public void setEdadmindias(Integer edadmindias) {
		this.edadmindias = edadmindias;
	}

	public Integer getEdadmaxanios() {
		return edadmaxanios;
	}

	public void setEdadmaxanios(Integer edadmaxanios) {
		this.edadmaxanios = edadmaxanios;
	}

	public Integer getEdadmaxmeses() {
		return edadmaxmeses;
	}

	public void setEdadmaxmeses(Integer edadmaxmeses) {
		this.edadmaxmeses = edadmaxmeses;
	}

	public Integer getEdadmaxdias() {
		return edadmaxdias;
	}

	public void setEdadmaxdias(Integer edadmaxdias) {
		this.edadmaxdias = edadmaxdias;
	}

	public Integer getHombre() {
		return hombre;
	}

	public void setHombre(Integer hombre) {
		this.hombre = hombre;
	}

	public Integer getMujer() {
		return mujer;
	}

	public void setMujer(Integer mujer) {
		this.mujer = mujer;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	public Detallecatalogo getCtgprioritarioId() {
		return ctgprioritarioId;
	}

	public void setCtgprioritarioId(Detallecatalogo ctgprioritarioId) {
		this.ctgprioritarioId = ctgprioritarioId;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Gprioritariovalidacion other = (Gprioritariovalidacion) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ec.gob.msp.rdacaa.business.entity.Gprioritariovalidacion[ id=" + id + " ]";
	}

}
