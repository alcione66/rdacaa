/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.service;

import java.math.BigDecimal;

import ec.gob.msp.rdacaa.business.entity.Detallecatalogo;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author Saulo Velasco
 */
@Getter
@Setter
public class SignosVitalesReglasValidacion {

	private Integer idSignoVitalRegla;
	private Integer idSignoVitalAlerta;
	private BigDecimal valorminimo;
	private BigDecimal valormaximo;
	private Integer edadminima;
	private Integer edadmaxima;
	private Integer edadminimaAnios;
	private Integer edadmaximaAnios;
	private Integer edadminimaMeses;
	private Integer edadmaximaMeses;
	private Integer edadminimaDias;
	private Integer edadmaximaDias;
	private String edadminimaString;
	private String edadmaximaString;
	private String sexo;
	private Detallecatalogo grupoPrioritario;
	private BigDecimal valorMinimoAlerta;
	private BigDecimal valorMaximoAlerta;
	private String alerta;
	private String color;

	@Override
	public String toString() {
		return "SignosVitalesReglasValidacion [idSignoVitalRegla=" + idSignoVitalRegla + ", idSignoVitalAlerta="
				+ idSignoVitalAlerta + ", valorminimo=" + valorminimo + ", valormaximo=" + valormaximo + ", edadminima="
				+ edadminima + ", edadmaxima=" + edadmaxima + ", edadminimaAnios=" + edadminimaAnios
				+ ", edadmaximaAnios=" + edadmaximaAnios + ", edadminimaMeses=" + edadminimaMeses + ", edadmaximaMeses="
				+ edadmaximaMeses + ", edadminimaDias=" + edadminimaDias + ", edadmaximaDias=" + edadmaximaDias
				+ ", edadminimaString=" + edadminimaString + ", edadmaximaString=" + edadmaximaString + ", sexo=" + sexo
				+ ", grupoPrioritario=" + grupoPrioritario + ", valorMinimoAlerta=" + valorMinimoAlerta
				+ ", valorMaximoAlerta=" + valorMaximoAlerta + ", alerta=" + alerta + ", color=" + color + "]";
	}

}
