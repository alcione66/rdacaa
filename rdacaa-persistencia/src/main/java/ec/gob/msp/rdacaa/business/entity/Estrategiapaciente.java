package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "estrategiapaciente", uniqueConstraints = { @UniqueConstraint(columnNames = { "id" }) })
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Estrategiapaciente.findAll", query = "SELECT e FROM Estrategiapaciente e") })
public class Estrategiapaciente implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "fecharegistro")
	@Temporal(TemporalType.TIMESTAMP)
	private Date fecharegistro;
	@Basic(optional = false)
	@Column(name = "orden", nullable = false)
	private int orden;
	@JoinColumn(name = "atencionmedica_id", referencedColumnName = "id", nullable = false)
	@ManyToOne(optional = false)
	private Atencionmedica atencionmedicaId;
	@JoinColumn(name = "ctestrategia_id", referencedColumnName = "id")
	@ManyToOne
	private Detallecatalogo ctestrategiaId;
	@JoinColumn(name = "persona_id", referencedColumnName = "id")
	@ManyToOne
	private Persona personaId;

	public Estrategiapaciente() {
	}

	public Estrategiapaciente(Integer id) {
		this.id = id;
	}

	public Estrategiapaciente(Integer id, int orden) {
		this.id = id;
		this.orden = orden;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getFecharegistro() {
		return fecharegistro;
	}

	public void setFecharegistro(Date fecharegistro) {
		this.fecharegistro = fecharegistro;
	}

	public int getOrden() {
		return orden;
	}

	public void setOrden(int orden) {
		this.orden = orden;
	}

	public Atencionmedica getAtencionmedicaId() {
		return atencionmedicaId;
	}

	public void setAtencionmedicaId(Atencionmedica atencionmedicaId) {
		this.atencionmedicaId = atencionmedicaId;
	}

	public Detallecatalogo getCtestrategiaId() {
		return ctestrategiaId;
	}

	public void setCtestrategiaId(Detallecatalogo ctestrategiaId) {
		this.ctestrategiaId = ctestrategiaId;
	}

	public Persona getPersonaId() {
		return personaId;
	}

	public void setPersonaId(Persona personaId) {
		this.personaId = personaId;
	}

	@Override
	public int hashCode() {
		int hash = 0;
		hash += (id != null ? id.hashCode() : 0);
		return hash;
	}

	@Override
	public boolean equals(Object object) {
		// TODO: Warning - this method won't work in the case the id fields are not set
		if (!(object instanceof Estrategiapaciente)) {
			return false;
		}
		Estrategiapaciente other = (Estrategiapaciente) object;
		if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ec.gob.msp.rdacaa.business.entity.Estrategiapaciente[ id=" + id + " ]";
	}

}
