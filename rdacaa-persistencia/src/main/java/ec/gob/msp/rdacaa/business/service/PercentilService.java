package ec.gob.msp.rdacaa.business.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.Expressions;

import ec.gob.msp.rdacaa.business.entity.Percentil;
import ec.gob.msp.rdacaa.business.entity.QPercentil;
import ec.gob.msp.rdacaa.business.repository.PercentilRepository;

/**
 * Generated by Spring Data Generator on 11/09/2018
 */
@Component
public class PercentilService {

	private PercentilRepository percentilRepository;

	private static final int EDAD_UN_ANIO_SEIS_MES_UN_DIA = 10601;
	private static final int EDAD_DOS_ANIO_CERO_MES_UN_DIA = 20001;
	private static final int EDAD_CINCO_ANIO_UN_MES_CERO_DIA = 50100;

	private static final int CT_PESO_PARA_LONGITUDTALLA = 1175;

	@Autowired
	public PercentilService(PercentilRepository percentilRepository) {
		this.percentilRepository = percentilRepository;
	}

	public Percentil findPercentilByCondicion(Integer singoVitalId, Integer edadAniosMesDias, String sexo,
			boolean isTallaDePie) {
		QPercentil qPercentil = QPercentil.percentil;
		BooleanExpression cumpleCondiciones = qPercentil.ctsignovitalId.id.eq(singoVitalId)
				.and(Expressions.stringTemplate("replace({0},'-','')", qPercentil.edadminima).castToNum(Integer.class)
						.loe(edadAniosMesDias))
				.and(Expressions.stringTemplate("replace({0},'-','')", qPercentil.edadmaxima).castToNum(Integer.class)
						.goe(edadAniosMesDias))
				.and(qPercentil.sexo.eq(sexo));

		if (singoVitalId.equals(CT_PESO_PARA_LONGITUDTALLA) && edadAniosMesDias < EDAD_UN_ANIO_SEIS_MES_UN_DIA) {
			cumpleCondiciones = cumpleCondiciones.and(qPercentil.medida.eq("L"));
		} else if (singoVitalId.equals(CT_PESO_PARA_LONGITUDTALLA) && edadAniosMesDias >= EDAD_UN_ANIO_SEIS_MES_UN_DIA
				&& edadAniosMesDias < EDAD_DOS_ANIO_CERO_MES_UN_DIA) {
			if (isTallaDePie) {
				cumpleCondiciones = cumpleCondiciones.and(qPercentil.medida.eq("T"));
			} else {
				cumpleCondiciones = cumpleCondiciones.and(qPercentil.medida.eq("L"));
			}
		} else if(singoVitalId.equals(CT_PESO_PARA_LONGITUDTALLA) && edadAniosMesDias >= EDAD_DOS_ANIO_CERO_MES_UN_DIA 
				&& edadAniosMesDias < EDAD_CINCO_ANIO_UN_MES_CERO_DIA) {
			if(isTallaDePie) {
				cumpleCondiciones = cumpleCondiciones.and(qPercentil.medida.eq("T"));
			}else {
				cumpleCondiciones = cumpleCondiciones.and(qPercentil.medida.eq("L"));
			}
		} else if (singoVitalId.equals(CT_PESO_PARA_LONGITUDTALLA) && edadAniosMesDias >= EDAD_CINCO_ANIO_UN_MES_CERO_DIA ) {
			cumpleCondiciones = cumpleCondiciones.and(qPercentil.medida.eq("T"));
		}

		Optional<Percentil> percentilFound = percentilRepository.findOne(cumpleCondiciones);

		return percentilFound.isPresent() ? percentilFound.get() : null;
	}
}
