package ec.gob.msp.rdacaa.system.database;

import org.hibernate.dialect.SQLiteDialect;
import org.hibernate.dialect.function.StandardSQLFunction;
import org.hibernate.type.StandardBasicTypes;

public class CustomSQLiteDialect extends SQLiteDialect {

	public CustomSQLiteDialect() {
		super();
		registerFunction("replace", new StandardSQLFunction("replace", StandardBasicTypes.STRING));
	}
}
