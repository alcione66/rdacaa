/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Immutable;

/**
 *
 * @author msp
 */
@Entity
@Immutable
@Table(name = "VARIABLES_VIH", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VariablesVih.findAll", query = "SELECT v FROM VariablesVih v")})
public class VariablesVih implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID_ATENCION")
    private Integer idAtencion;
    @Column(name = "UUID_ATENCION", length = 2000000000)
    private String uuidAtencion;
    @Column(name = "COD_VIH_MOT_PRUEBA")
    private Integer codVihMotPrueba;
    @Column(name = "COD_VIH_MOT_PRUEBA_DESC", length = 2000000000)
    private String codVihMotPruebaDesc;
    @Column(name = "COD_VIH_PRI_PRUEBA")
    private Integer codVihPriPrueba;
    @Column(name = "COD_VIH_PRI_PRUEBA_DESC", length = 2000000000)
    private String codVihPriPruebaDesc;
    @Column(name = "COD_VIH_PRI_PRUEBA_REACTIVA")
    private Integer codVihPriPruebaReactiva;
    @Column(name = "COD_VIH_PRI_PRUEBA_REACTIVA_DESC", length = 2000000000)
    private String codVihPriPruebaReactivaDesc;
    @Column(name = "COD_VIH_SEG_PRUEBA")
    private Integer codVihSegPrueba;
    @Column(name = "COD_VIH_SEG_PRUEBA_DESC", length = 2000000000)
    private String codVihSegPruebaDesc;
    @Column(name = "COD_VIH_SEG_PRUEBA_REACTIVA")
    private Integer codVihSegPruebaReactiva;
    @Column(name = "COD_VIH_SEG_PRUEBA_REACTIVA_DESC", length = 2000000000)
    private String codVihSegPruebaReactivaDesc;
    @Column(name = "COD_VIH_VIA_TRANSMISION")
    private Integer codVihViaTransmision;
    @Column(name = "COD_VIH_VIA_TRANSMISION_DESC", length = 2000000000)
    private String codVihViaTransmisionDesc;
    @Column(name = "VIH_CARGA_VIRAL")
    private Integer vihCargaViral;
    @Column(name = "VIH_CD4")
    private Integer vihCd4;

    public VariablesVih() {
    }

    public Integer getIdAtencion() {
        return idAtencion;
    }

    public void setIdAtencion(Integer idAtencion) {
        this.idAtencion = idAtencion;
    }

    public String getUuidAtencion() {
        return uuidAtencion;
    }

    public void setUuidAtencion(String uuidAtencion) {
        this.uuidAtencion = uuidAtencion;
    }

    public Integer getCodVihMotPrueba() {
        return codVihMotPrueba;
    }

    public void setCodVihMotPrueba(Integer codVihMotPrueba) {
        this.codVihMotPrueba = codVihMotPrueba;
    }

    public String getCodVihMotPruebaDesc() {
        return codVihMotPruebaDesc;
    }

    public void setCodVihMotPruebaDesc(String codVihMotPruebaDesc) {
        this.codVihMotPruebaDesc = codVihMotPruebaDesc;
    }

    public Integer getCodVihPriPrueba() {
        return codVihPriPrueba;
    }

    public void setCodVihPriPrueba(Integer codVihPriPrueba) {
        this.codVihPriPrueba = codVihPriPrueba;
    }

    public String getCodVihPriPruebaDesc() {
        return codVihPriPruebaDesc;
    }

    public void setCodVihPriPruebaDesc(String codVihPriPruebaDesc) {
        this.codVihPriPruebaDesc = codVihPriPruebaDesc;
    }

    public Integer getCodVihPriPruebaReactiva() {
        return codVihPriPruebaReactiva;
    }

    public void setCodVihPriPruebaReactiva(Integer codVihPriPruebaReactiva) {
        this.codVihPriPruebaReactiva = codVihPriPruebaReactiva;
    }

    public String getCodVihPriPruebaReactivaDesc() {
        return codVihPriPruebaReactivaDesc;
    }

    public void setCodVihPriPruebaReactivaDesc(String codVihPriPruebaReactivaDesc) {
        this.codVihPriPruebaReactivaDesc = codVihPriPruebaReactivaDesc;
    }

    public Integer getCodVihSegPrueba() {
        return codVihSegPrueba;
    }

    public void setCodVihSegPrueba(Integer codVihSegPrueba) {
        this.codVihSegPrueba = codVihSegPrueba;
    }

    public String getCodVihSegPruebaDesc() {
        return codVihSegPruebaDesc;
    }

    public void setCodVihSegPruebaDesc(String codVihSegPruebaDesc) {
        this.codVihSegPruebaDesc = codVihSegPruebaDesc;
    }

    public Integer getCodVihSegPruebaReactiva() {
        return codVihSegPruebaReactiva;
    }

    public void setCodVihSegPruebaReactiva(Integer codVihSegPruebaReactiva) {
        this.codVihSegPruebaReactiva = codVihSegPruebaReactiva;
    }

    public String getCodVihSegPruebaReactivaDesc() {
        return codVihSegPruebaReactivaDesc;
    }

    public void setCodVihSegPruebaReactivaDesc(String codVihSegPruebaReactivaDesc) {
        this.codVihSegPruebaReactivaDesc = codVihSegPruebaReactivaDesc;
    }

    public Integer getCodVihViaTransmision() {
        return codVihViaTransmision;
    }

    public void setCodVihViaTransmision(Integer codVihViaTransmision) {
        this.codVihViaTransmision = codVihViaTransmision;
    }

    public String getCodVihViaTransmisionDesc() {
        return codVihViaTransmisionDesc;
    }

    public void setCodVihViaTransmisionDesc(String codVihViaTransmisionDesc) {
        this.codVihViaTransmisionDesc = codVihViaTransmisionDesc;
    }

    public Integer getVihCargaViral() {
        return vihCargaViral;
    }

    public void setVihCargaViral(Integer vihCargaViral) {
        this.vihCargaViral = vihCargaViral;
    }

    public Integer getVihCd4() {
        return vihCd4;
    }

    public void setVihCd4(Integer vihCd4) {
        this.vihCd4 = vihCd4;
    }
    
}
