package ec.gob.msp.rdacaa.business.service;

import com.querydsl.core.types.dsl.BooleanExpression;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import ec.gob.msp.rdacaa.business.entity.Provincia;
import ec.gob.msp.rdacaa.business.entity.QProvincia;
import ec.gob.msp.rdacaa.business.repository.ProvinciaRepository;
import java.util.Optional;

/**
 * Generated by Spring Data Generator on 20/06/2018
 */
@Service
public class ProvinciaService {

	private ProvinciaRepository provinciaRepository;

	@Autowired
	public ProvinciaService(ProvinciaRepository provinciaRepository) {
		this.provinciaRepository = provinciaRepository;
	}

	public List<Provincia> findAllOrderedByDescripcion() {
		return provinciaRepository.findAll(GenericUtil.orderFieldASC("descripcion"));
	}

	public Provincia findOneByProvinciaId(Integer id) {
		QProvincia qProvincia = QProvincia.provincia;
		BooleanExpression isProvincia = qProvincia.id.eq(id);
		Optional<Provincia> provincia = provinciaRepository.findOne(isProvincia);
		return provincia.isPresent() ? provincia.get() : null;
	}

}
