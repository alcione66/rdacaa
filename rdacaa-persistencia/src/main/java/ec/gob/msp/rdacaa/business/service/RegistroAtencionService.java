package ec.gob.msp.rdacaa.business.service;

import java.util.List;
import java.util.stream.IntStream;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ec.gob.msp.rdacaa.business.entity.Atenciongprioritario;
import ec.gob.msp.rdacaa.business.entity.Atenciongvulnerable;
import ec.gob.msp.rdacaa.business.entity.Atencionmedica;
import ec.gob.msp.rdacaa.business.entity.Atencionobstetrica;
import ec.gob.msp.rdacaa.business.entity.Atencionprescripcion;
import ec.gob.msp.rdacaa.business.entity.Atencionsivan;
import ec.gob.msp.rdacaa.business.entity.Atencionviolencia;
import ec.gob.msp.rdacaa.business.entity.Detallecatalogo;
import ec.gob.msp.rdacaa.business.entity.Diagnosticoatencion;
import ec.gob.msp.rdacaa.business.entity.Entidad;
import ec.gob.msp.rdacaa.business.entity.Estrategiapaciente;
import ec.gob.msp.rdacaa.business.entity.Examenlaboratorio;
import ec.gob.msp.rdacaa.business.entity.Interconsulta;
import ec.gob.msp.rdacaa.business.entity.Intraextramural;
import ec.gob.msp.rdacaa.business.entity.Medicionpaciente;
import ec.gob.msp.rdacaa.business.entity.Odontologia;
import ec.gob.msp.rdacaa.business.entity.Persona;
import ec.gob.msp.rdacaa.business.entity.Procedimientoatencion;
import ec.gob.msp.rdacaa.business.entity.Referenciaatencion;
import ec.gob.msp.rdacaa.business.entity.Registrovacunacion;
import ec.gob.msp.rdacaa.business.entity.Usuario;
import ec.gob.msp.rdacaa.business.entity.Vihsida;

@Service
public class RegistroAtencionService {

	@Autowired
	private PersonaService personaService;
	@Autowired
	private AtencionmedicaService atencionmedicaService;
	@Autowired
	private AtenciongprioritarioService atenciongprioritarioService;
	@Autowired
	private AtenciongvulnerableService atenciongvulnerableService;
	@Autowired
	private IntraextramuralService intraextramuralService;
	@Autowired
	private MedicionpacienteService medicionpacienteService;
	@Autowired
	private AtencionobstetricaService atencionobstetricaService;
	@Autowired
	private VihsidaService vihsidaService;
	@Autowired
	private RegistrovacunacionService registrovacunacionService;
	@Autowired
	private AtencionsivanService atencionsivanService;
	@Autowired
	private ExamenlaboratorioService examenlaboratorioService;
	@Autowired
	private DiagnosticoatencionService diagnosticoatencionService;
	@Autowired
	private ProcedimientoatencionService procedimientoatencionService;
	@Autowired
	private OdontologiaService odontologiaService;
	@Autowired
	private AtencionprescripcionService atencionprescripcionService;
	@Autowired
	private ReferenciaatencionService referenciaAtencionService;
	@Autowired
	private InterconsultaService interconsultaService;
	@Autowired
	private AtencionviolenciaService atencionviolenciaService;
	@Autowired
	private EstrategiapacienteService estrategiapacienteService;

	@Transactional(propagation = Propagation.REQUIRED)
	public void executeRegistroAtencion(Persona persona, Intraextramural intraExtraMural,
			List<Atenciongprioritario> gruposPrioritarios, Atencionmedica atencionMedica,
			List<Medicionpaciente> listaVariablesAntropometricas, Persona profesional, Usuario usuario,
			Atencionobstetrica atencionobstetrica, Vihsida vihsida, Entidad establecimientoSalud,
			Detallecatalogo ctEspecialidad, List<Diagnosticoatencion> diagnosticoatencion, Atencionsivan atencionsivan,
			List<Registrovacunacion> registrovacunacion, Examenlaboratorio examenlaboratorio, Odontologia odontologia,
			Referenciaatencion referencia, Interconsulta interconsulta, Atencionprescripcion prescripcion,
			List<Procedimientoatencion> procedimientos, Atencionviolencia atencionViolencia,
			List<Atenciongvulnerable> atenciongvulnerable, Estrategiapaciente estrategiapaciente) {
		if (persona != null && atencionMedica != null) {
			Persona personaSaved = personaService.save(persona);

			atencionMedica.setEstado(1);
			Atencionmedica atencionMedicaSaved = atencionmedicaService.save(atencionMedica);

			// Save intraExtraMural
			if (intraExtraMural != null) {
				intraExtraMural.setAtencionmedicaId(atencionMedicaSaved);
				intraextramuralService.save(intraExtraMural);
			}

			// Save variables antropometricas
			if (!listaVariablesAntropometricas.isEmpty()) {
				listaVariablesAntropometricas.forEach(variableAntropometrica -> {
					variableAntropometrica.setAtencionmedicaId(atencionMedicaSaved);
					medicionpacienteService.save(variableAntropometrica);
				});
			}

			// Save atencionobstetrica
			if (atencionobstetrica != null) {
				atencionobstetrica.setAtencionmedicaId(atencionMedicaSaved);
				atencionobstetricaService.save(atencionobstetrica);
			}

			// Save vihsida
			if (vihsida != null) {
				vihsida.setAtencionmedicaId(atencionMedicaSaved);
				vihsidaService.save(vihsida);
			}

			// Save Diagnosticos CIE10
			if (diagnosticoatencion != null && !diagnosticoatencion.isEmpty()) {
				IntStream.range(0, diagnosticoatencion.size()).forEach(idx -> {
					diagnosticoatencion.get(idx).setAtencionmedicaId(atencionMedicaSaved);
					diagnosticoatencion.get(idx).setOrden(idx + 1);
					diagnosticoatencionService.save(diagnosticoatencion.get(idx));
				});
			}

			// Save Procedimientos
			if (procedimientos != null && !procedimientos.isEmpty()) {
				IntStream.range(0, procedimientos.size()).forEach(idx -> {
					procedimientos.get(idx).setAtencionmedicaId(atencionMedicaSaved);
					procedimientos.get(idx).setOrden(idx + 1);
					procedimientoatencionService.save(procedimientos.get(idx));
				});
			}

			// Save Registro de vacunación
			if (registrovacunacion != null && !registrovacunacion.isEmpty()) {
				IntStream.range(0, registrovacunacion.size()).forEach(idx -> {
					registrovacunacion.get(idx).setAtencionmedicaId(atencionMedicaSaved);
					registrovacunacion.get(idx).setOrden(idx + 1);
					registrovacunacionService.save(registrovacunacion.get(idx));
				});
			}

			// Save SIVAN
			if (atencionsivan != null) {
				atencionsivan.setAtencionmedicaId(atencionMedicaSaved);
				atencionsivanService.save(atencionsivan);
			}

			// Save Examen de Laboratorio
			if (examenlaboratorio != null) {
				examenlaboratorio.setAtencionmedicaId(atencionMedicaSaved);
				examenlaboratorioService.save(examenlaboratorio);
			}

			// Save Odontologia
			if (odontologia != null) {
				odontologia.setAtencionmedicaId(atencionMedicaSaved);
				odontologiaService.save(odontologia);
			}

			// Save Prescripcion
			if (prescripcion != null) {
				prescripcion.setAtencionId(atencionMedicaSaved);
				atencionprescripcionService.save(prescripcion);
			}

			// Save Referencia-Contrareferencia
			if (referencia != null) {
				referencia.setAtencionmedicaId(atencionMedicaSaved);
				referenciaAtencionService.save(referencia);
			}

			// Save Interconsulta
			if (interconsulta != null) {
				interconsulta.setAtencionmedicaId(atencionMedicaSaved);
				interconsultaService.save(interconsulta);
			}

			//	Save Estrategia
			if (estrategiapaciente != null) {
				estrategiapaciente.setAtencionmedicaId(atencionMedicaSaved);
				estrategiapaciente.setPersonaId(personaSaved);
				estrategiapaciente.setOrden(1);
				estrategiapacienteService.save(estrategiapaciente);
			}

			// Save Violencia
			if (atencionViolencia != null) {
				atencionViolencia.setAtencionmedicaId(atencionMedicaSaved);
				atencionviolenciaService.save(atencionViolencia);
			}

			// Save gruposPrioritarios
			if (gruposPrioritarios != null && !gruposPrioritarios.isEmpty()) {
				IntStream.range(0, gruposPrioritarios.size()).forEach(idx -> {
					gruposPrioritarios.get(idx).setAtencionmedicaId(atencionMedicaSaved);
					gruposPrioritarios.get(idx).setEstado(1);
					gruposPrioritarios.get(idx).setOrden(idx + 1);
					atenciongprioritarioService.save(gruposPrioritarios.get(idx));
				});
			}

			// Save Atencion Grupo Vulnerable
			if (atenciongvulnerable != null && !atenciongvulnerable.isEmpty()) {
				IntStream.range(0, atenciongvulnerable.size()).forEach(idx -> {
					atenciongvulnerable.get(idx).setAtencionmedicaId(atencionMedicaSaved);
					atenciongvulnerable.get(idx).setOrden(idx + 1);
					atenciongvulnerableService.save(atenciongvulnerable.get(idx));
				});
			}

		}

	}
}
