/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Immutable;

/**
 *
 * @author msp
 */
@Entity
@Immutable
@Table(name = "VARIABLES_DATOS_ANTROPOMETRICOS", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VariablesDatosAntropometricos.findAll", query = "SELECT v FROM VariablesDatosAntropometricos v")})
public class VariablesDatosAntropometricos implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID_ATENCION")
    private Integer idAtencion;
    @Column(name = "UUID_ATENCION", length = 2000000000)
    private String uuidAtencion;
    @Column(name = "DAT_ANT_TALLA", length = 2000000000)
    private String datAntTalla;
    @Column(name = "DAT_ANT_TALLA_CORREGIDA", length = 2000000000)
    private String datAntTallaCorregida;
    @Column(name = "DAT_ANT_PESO", length = 2000000000)
    private String datAntPeso;
    @Column(name = "DAT_ANT_PER_CEFALICO", length = 2000000000)
    private String datAntPerCefalico;
    @Column(name = "DAT_ANT_IMC", length = 2000000000)
    private String datAntImc;
    @Column(name = "DAT_ANT_IMC_CLASIFICACION", length = 2000000000)
    private String datAntImcClasificacion;
    @Column(name = "DAT_ANT_IMC_CLASIFICACION_DESC", length = 2000000000)
    private String datAntImcClasificacionDesc;
    @Column(name = "COD_DAT_ANT_TIP_TOMA_TALLA", length = 2000000000)
    private String codDatAntTipTomaTalla;
    @Column(name = "COD_DAT_ANT_TIP_TOMA_TALLA_DESC", length = 2000000000)
    private String codDatAntTipTomaTallaDesc;
    @Column(name = "DAT_ANT_PUNTAJE_Z_TE", length = 2000000000)
    private String datAntPuntajeZTe;
    @Column(name = "DAT_ANT_PUNTAJE_Z_PE", length = 2000000000)
    private String datAntPuntajeZPe;
    @Column(name = "DAT_ANT_PUNTAJE_Z_PT", length = 2000000000)
    private String datAntPuntajeZPt;
    @Column(name = "DAT_ANT_PUNTAJE_Z_IMC_E", length = 2000000000)
    private String datAntPuntajeZImcE;
    @Column(name = "DAT_ANT_PUNTAJE_Z_PC_E", length = 2000000000)
    private String datAntPuntajeZPcE;
    @Column(name = "DAT_ANT_CATEGORIA_TE", length = 2000000000)
    private String datAntCategoriaTe;
    @Column(name = "DAT_ANT_CATEGORIA_TE_DESC", length = 2000000000)
    private String datAntCategoriaTeDesc;
    @Column(name = "DAT_ANT_CATEGORIA_PE", length = 2000000000)
    private String datAntCategoriaPe;
    @Column(name = "DAT_ANT_CATEGORIA_PE_DESC", length = 2000000000)
    private String datAntCategoriaPeDesc;
    @Column(name = "DAT_ANT_CATEGORIA_PT", length = 2000000000)
    private String datAntCategoriaPt;
    @Column(name = "DAT_ANT_CATEGORIA_PT_DESC", length = 2000000000)
    private String datAntCategoriaPtDesc;
    @Column(name = "DAT_ANT_CATEGORIA_IMC_E", length = 2000000000)
    private String datAntCategoriaImcE;
    @Column(name = "DAT_ANT_CATEGORIA_IMC_E_DESC", length = 2000000000)
    private String datAntCategoriaImcEDesc;
    @Column(name = "DAT_ANT_CATEGORIA_PC_E", length = 2000000000)
    private String datAntCategoriaPcE;
    @Column(name = "DAT_ANT_CATEGORIA_PC_E_DESC", length = 2000000000)
    private String datAntCategoriaPcEDesc;
    @Column(name = "DAT_ANT_HB", length = 2000000000)
    private String datAntHb;
    @Column(name = "DAT_ANT_HB_CORREGIDO", length = 2000000000)
    private String datAntHbCorregido;
    @Column(name = "COD_IND_ANEMIA", length = 2000000000)
    private String codIndAnemia;
    @Column(name = "COD_IND_ANEMIA_DESC", length = 2000000000)
    private String codIndAnemiaDesc;

    public VariablesDatosAntropometricos() {
    }

    public Integer getIdAtencion() {
        return idAtencion;
    }

    public void setIdAtencion(Integer idAtencion) {
        this.idAtencion = idAtencion;
    }

    public String getUuidAtencion() {
        return uuidAtencion;
    }

    public void setUuidAtencion(String uuidAtencion) {
        this.uuidAtencion = uuidAtencion;
    }

    public String getDatAntTalla() {
        return datAntTalla;
    }

    public void setDatAntTalla(String datAntTalla) {
        this.datAntTalla = datAntTalla;
    }

    public String getDatAntTallaCorregida() {
        return datAntTallaCorregida;
    }

    public void setDatAntTallaCorregida(String datAntTallaCorregida) {
        this.datAntTallaCorregida = datAntTallaCorregida;
    }

    public String getDatAntPeso() {
        return datAntPeso;
    }

    public void setDatAntPeso(String datAntPeso) {
        this.datAntPeso = datAntPeso;
    }

    public String getDatAntPerCefalico() {
        return datAntPerCefalico;
    }

    public void setDatAntPerCefalico(String datAntPerCefalico) {
        this.datAntPerCefalico = datAntPerCefalico;
    }

    public String getDatAntImc() {
        return datAntImc;
    }

    public void setDatAntImc(String datAntImc) {
        this.datAntImc = datAntImc;
    }

    public String getDatAntImcClasificacion() {
        return datAntImcClasificacion;
    }

    public void setDatAntImcClasificacion(String datAntImcClasificacion) {
        this.datAntImcClasificacion = datAntImcClasificacion;
    }

    public String getDatAntImcClasificacionDesc() {
        return datAntImcClasificacionDesc;
    }

    public void setDatAntImcClasificacionDesc(String datAntImcClasificacionDesc) {
        this.datAntImcClasificacionDesc = datAntImcClasificacionDesc;
    }

    public String getCodDatAntTipTomaTalla() {
        return codDatAntTipTomaTalla;
    }

    public void setCodDatAntTipTomaTalla(String codDatAntTipTomaTalla) {
        this.codDatAntTipTomaTalla = codDatAntTipTomaTalla;
    }

    public String getCodDatAntTipTomaTallaDesc() {
        return codDatAntTipTomaTallaDesc;
    }

    public void setCodDatAntTipTomaTallaDesc(String codDatAntTipTomaTallaDescripcion) {
        this.codDatAntTipTomaTallaDesc= codDatAntTipTomaTallaDescripcion;
    }

    public String getDatAntPuntajeZTe() {
        return datAntPuntajeZTe;
    }

    public void setDatAntPuntajeZTe(String datAntPuntajeZTe) {
        this.datAntPuntajeZTe = datAntPuntajeZTe;
    }

    public String getDatAntPuntajeZPe() {
        return datAntPuntajeZPe;
    }

    public void setDatAntPuntajeZPe(String datAntPuntajeZPe) {
        this.datAntPuntajeZPe = datAntPuntajeZPe;
    }

    public String getDatAntPuntajeZPt() {
        return datAntPuntajeZPt;
    }

    public void setDatAntPuntajeZPt(String datAntPuntajeZPt) {
        this.datAntPuntajeZPt = datAntPuntajeZPt;
    }

    public String getDatAntPuntajeZImcE() {
        return datAntPuntajeZImcE;
    }

    public void setDatAntPuntajeZImcE(String datAntPuntajeZImcE) {
        this.datAntPuntajeZImcE = datAntPuntajeZImcE;
    }

    public String getDatAntPuntajeZPcE() {
        return datAntPuntajeZPcE;
    }

    public void setDatAntPuntajeZPcE(String datAntPuntajeZPcE) {
        this.datAntPuntajeZPcE = datAntPuntajeZPcE;
    }

    public String getDatAntCategoriaTe() {
        return datAntCategoriaTe;
    }

    public void setDatAntCategoriaTe(String datAntCategoriaTe) {
        this.datAntCategoriaTe = datAntCategoriaTe;
    }

    public String getDatAntCategoriaTeDesc() {
        return datAntCategoriaTeDesc;
    }

    public void setDatAntCategoriaTeDesc(String datAntCategoriaTeDesc) {
        this.datAntCategoriaTeDesc = datAntCategoriaTeDesc;
    }

    public String getDatAntCategoriaPe() {
        return datAntCategoriaPe;
    }

    public void setDatAntCategoriaPe(String datAntCategoriaPe) {
        this.datAntCategoriaPe = datAntCategoriaPe;
    }

    public String getDatAntCategoriaPeDesc() {
        return datAntCategoriaPeDesc;
    }

    public void setDatAntCategoriaPeDesc(String datAntCategoriaPeDesc) {
        this.datAntCategoriaPeDesc = datAntCategoriaPeDesc;
    }

    public String getDatAntCategoriaPt() {
        return datAntCategoriaPt;
    }

    public void setDatAntCategoriaPt(String datAntCategoriaPt) {
        this.datAntCategoriaPt = datAntCategoriaPt;
    }

    public String getDatAntCategoriaPtDesc() {
        return datAntCategoriaPtDesc;
    }

    public void setDatAntCategoriaPtDesc(String datAntCategoriaPtDesc) {
        this.datAntCategoriaPtDesc = datAntCategoriaPtDesc;
    }

    public String getDatAntCategoriaImcE() {
        return datAntCategoriaImcE;
    }

    public void setDatAntCategoriaImcE(String datAntCategoriaImcE) {
        this.datAntCategoriaImcE = datAntCategoriaImcE;
    }

    public String getDatAntCategoriaImcEDesc() {
        return datAntCategoriaImcEDesc;
    }

    public void setDatAntCategoriaImcEDesc(String datAntCategoriaImcEDesc) {
        this.datAntCategoriaImcEDesc = datAntCategoriaImcEDesc;
    }

    public String getDatAntCategoriaPcE() {
        return datAntCategoriaPcE;
    }

    public void setDatAntCategoriaPcE(String datAntCategoriaPcE) {
        this.datAntCategoriaPcE = datAntCategoriaPcE;
    }

    public String getDatAntCategoriaPcEDesc() {
        return datAntCategoriaPcEDesc;
    }

    public void setDatAntCategoriaPcEDesc(String datAntCategoriaPcEDesc) {
        this.datAntCategoriaPcEDesc = datAntCategoriaPcEDesc;
    }

    public String getDatAntHb() {
        return datAntHb;
    }

    public void setDatAntHb(String datAntHb) {
        this.datAntHb = datAntHb;
    }

    public String getDatAntHbCorregido() {
        return datAntHbCorregido;
    }

    public void setDatAntHbCorregido(String datAntHbCorregido) {
        this.datAntHbCorregido = datAntHbCorregido;
    }

    public String getCodIndAnemia() {
        return codIndAnemia;
    }

    public void setCodIndAnemia(String codIndAnemia) {
        this.codIndAnemia = codIndAnemia;
    }

    public String getCodIndAnemiaDesc() {
        return codIndAnemiaDesc;
    }

    public void setCodIndAnemiaDesc(String codIndAnemiaDesc) {
        this.codIndAnemiaDesc = codIndAnemiaDesc;
    }
    
}
