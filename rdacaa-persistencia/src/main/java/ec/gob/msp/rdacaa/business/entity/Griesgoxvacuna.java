/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;

/**
 *
 * @author saulo
 */
@Entity
@Table(name = "griesgoxvacuna", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"id"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Griesgoxvacuna.findAll", query = "SELECT g FROM Griesgoxvacuna g")})
public class Griesgoxvacuna implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "estado")
    private Integer estado;
    @Column(name = "hombre")
    private Integer hombre;
    @Column(name = "mujer")
    private Integer mujer;
    @Column(name = "intersexual")
    private Integer intersexual;
    @JoinColumn(name = "ctgriesgo_id", referencedColumnName = "id")
    @ManyToOne
    private Detallecatalogo ctgriesgoId;
    @JoinColumn(name = "vacuna_id", referencedColumnName = "id")
    @ManyToOne
    private Vacuna vacunaId;

    public Griesgoxvacuna() {
    }

    public Griesgoxvacuna(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    public Integer getHombre() {
        return hombre;
    }

    public void setHombre(Integer hombre) {
        this.hombre = hombre;
    }

    public Integer getMujer() {
        return mujer;
    }

    public void setMujer(Integer mujer) {
        this.mujer = mujer;
    }

    public Integer getIntersexual() {
        return intersexual;
    }

    public void setIntersexual(Integer intersexual) {
        this.intersexual = intersexual;
    }

    public Detallecatalogo getCtgriesgoId() {
        return ctgriesgoId;
    }

    public void setCtgriesgoId(Detallecatalogo ctgriesgoId) {
        this.ctgriesgoId = ctgriesgoId;
    }

    public Vacuna getVacunaId() {
        return vacunaId;
    }

    public void setVacunaId(Vacuna vacunaId) {
        this.vacunaId = vacunaId;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Griesgoxvacuna other = (Griesgoxvacuna) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

    @Override
    public String toString() {
        return "ec.gob.msp.rdacaa.business.entity.Griesgoxvacuna[ id=" + id + " ]";
    }
    
}
