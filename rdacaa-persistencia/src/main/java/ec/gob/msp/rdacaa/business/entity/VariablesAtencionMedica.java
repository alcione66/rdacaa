/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.xml.bind.annotation.XmlRootElement;

import org.hibernate.annotations.Immutable;

/**
 *
 * @author msp
 */
@Entity
@Immutable
@Table(name = "VARIABLES_ATENCION_MEDICA", catalog = "", schema = "")
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "VariablesAtencionMedica.findAll", query = "SELECT v FROM VariablesAtencionMedica v")})
public class VariablesAtencionMedica implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Column(name = "ID_ATENCION")
    private Integer idAtencion;
    @Column(name = "UUID_ATENCION", length = 2000000000)
    private String uuidAtencion;
    @Column(name = "COD_EST_SALUD")
    private Integer codEstSalud;
    @Column(name = "COD_EST_SALUD_NOMBRE_OFICIAL", length = 2000000000)
    private String codEstSaludNombreOficial;
    @Column(name = "FEC_ATENCION", length = 2000000000)
    private String fecAtencion;
    @Column(name = "COD_PROFESIONAL")
    private Integer codProfesional;
    @Column(name = "COD_PROFESIONAL_NUM_IDENT", length = 2000000000)
    private String codProfesionalNumIdent;
    @Column(name = "COD_ESPECIALIDAD")
    private Integer codEspecialidad;
    @Column(name = "COD_ESPECIALIDAD_DESC", length = 2000000000)
    private String codEspecialidadDesc;
    @Column(name = "COD_PER_TIPO_BONO")
    private Integer codPerTipoBono;
    @Column(name = "COD_PER_TIPO_BONO_DESC", length = 2000000000)
    private String codPerTipoBonoDesc;
    @Column(name = "COD_PER_SEGURO")
    private Integer codPerSeguro;
    @Column(name = "COD_PER_SEGURO_DESC", length = 2000000000)
    private String codPerSeguroDesc;

    public VariablesAtencionMedica() {
    }

    public Integer getIdAtencion() {
        return idAtencion;
    }

    public void setIdAtencion(Integer idAtencion) {
        this.idAtencion = idAtencion;
    }

    public String getUuidAtencion() {
        return uuidAtencion;
    }

    public void setUuidAtencion(String uuidAtencion) {
        this.uuidAtencion = uuidAtencion;
    }

    public Integer getCodEstSalud() {
        return codEstSalud;
    }

    public void setCodEstSalud(Integer codEstSalud) {
        this.codEstSalud = codEstSalud;
    }

    public String getCodEstSaludNombreOficial() {
        return codEstSaludNombreOficial;
    }

    public void setCodEstSaludNombreOficial(String codEstSaludNombreOficial) {
        this.codEstSaludNombreOficial = codEstSaludNombreOficial;
    }

    public String getFecAtencion() {
        return fecAtencion;
    }

    public void setFecAtencion(String fecAtencion) {
        this.fecAtencion = fecAtencion;
    }

    public Integer getCodProfesional() {
        return codProfesional;
    }

    public void setCodProfesional(Integer codProfesional) {
        this.codProfesional = codProfesional;
    }

    public String getCodProfesionalNumIdent() {
        return codProfesionalNumIdent;
    }

    public void setCodProfesionalNumIdent(String codProfesionalNumIdent) {
        this.codProfesionalNumIdent = codProfesionalNumIdent;
    }

    public Integer getCodEspecialidad() {
        return codEspecialidad;
    }

    public void setCodEspecialidad(Integer codEspecialidad) {
        this.codEspecialidad = codEspecialidad;
    }

    public String getCodEspecialidadDesc() {
        return codEspecialidadDesc;
    }

    public void setCodEspecialidadDesc(String codEspecialidadDesc) {
        this.codEspecialidadDesc = codEspecialidadDesc;
    }

    public Integer getCodPerTipoBono() {
        return codPerTipoBono;
    }

    public void setCodPerTipoBono(Integer codPerTipoBono) {
        this.codPerTipoBono = codPerTipoBono;
    }

    public String getCodPerTipoBonoDesc() {
        return codPerTipoBonoDesc;
    }

    public void setCodPerTipoBonoDesc(String codPerTipoBonoDesc) {
        this.codPerTipoBonoDesc = codPerTipoBonoDesc;
    }

    public Integer getCodPerSeguro() {
        return codPerSeguro;
    }

    public void setCodPerSeguro(Integer codPerSeguro) {
        this.codPerSeguro = codPerSeguro;
    }

    public String getCodPerSeguroDesc() {
        return codPerSeguroDesc;
    }

    public void setCodPerSeguroDesc(String codPerSeguroDesc) {
        this.codPerSeguroDesc = codPerSeguroDesc;
    }
    
}
