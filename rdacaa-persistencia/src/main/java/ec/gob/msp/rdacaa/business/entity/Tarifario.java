package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "tarifario", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"id"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Tarifario.findAll", query = "SELECT t FROM Tarifario t")})
public class Tarifario implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @Basic(optional = false)
    @Column(name = "id", nullable = false)
    private Integer id;
    @Column(name = "descripcion", length = 2000)
    private String descripcion;
    @Column(name = "vigenciadesde")
    @Temporal(TemporalType.TIMESTAMP)
    private Date vigenciadesde;
    @Column(name = "vigenciahasta")
    @Temporal(TemporalType.TIMESTAMP)
    private Date vigenciahasta;
    @Column(name = "estado")
    private Integer estado;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "tarifarioId")
    private List<Serviciotarifario> serviciotarifarioList;

    public Tarifario() {
    }

    public Tarifario(Integer id) {
        this.id = id;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public Date getVigenciadesde() {
        return vigenciadesde;
    }

    public void setVigenciadesde(Date vigenciadesde) {
        this.vigenciadesde = vigenciadesde;
    }

    public Date getVigenciahasta() {
        return vigenciahasta;
    }

    public void setVigenciahasta(Date vigenciahasta) {
        this.vigenciahasta = vigenciahasta;
    }

    public Integer getEstado() {
        return estado;
    }

    public void setEstado(Integer estado) {
        this.estado = estado;
    }

    @XmlTransient
    public List<Serviciotarifario> getServiciotarifarioList() {
        return serviciotarifarioList;
    }

    public void setServiciotarifarioList(List<Serviciotarifario> serviciotarifarioList) {
        this.serviciotarifarioList = serviciotarifarioList;
    }

    @Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

    @Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Tarifario other = (Tarifario) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}

    @Override
    public String toString() {
        return "ec.gob.msp.rdacaa.business.entity.Tarifario[ id=" + id + " ]";
    }
    
}
