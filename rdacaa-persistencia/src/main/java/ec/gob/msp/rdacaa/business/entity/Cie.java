package ec.gob.msp.rdacaa.business.entity;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author msp
 */
@Entity
@Table(name = "cie", uniqueConstraints = { @UniqueConstraint(columnNames = { "id" }) })
@XmlRootElement
@NamedQueries({ @NamedQuery(name = "Cie.findAll", query = "SELECT c FROM Cie c") })
public class Cie implements Serializable {

	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Basic(optional = false)
	@Column(name = "id", nullable = false)
	private Integer id;
	@Column(name = "versioncie", length = 10)
	private String versioncie;
	@Column(name = "codigo", length = 6)
	private String codigo;
	@Column(name = "nombre", length = 300)
	private String nombre;
	@Column(name = "edadmin")
	private Integer edadmin;
	@Column(name = "unidadedadmin", length = 1)
	private String unidadedadmin;
	@Column(name = "edadmax")
	private Integer edadmax;
	@Column(name = "unidadedadmax", length = 1)
	private String unidadedadmax;
	@Column(name = "estado")
	private Integer estado;
	@OneToMany(cascade = CascadeType.ALL, mappedBy = "cieId")
	private List<Cie> cieList;
	@JoinColumn(name = "cie_id", referencedColumnName = "id")
	@ManyToOne
	private Cie cieId;
	@JoinColumn(name = "ctsexo_id", referencedColumnName = "id")
	@ManyToOne
	private Detallecatalogo ctsexoId;

	public Cie() {
	}

	public Cie(Integer id) {
		this.id = id;
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getVersioncie() {
		return versioncie;
	}

	public void setVersioncie(String versioncie) {
		this.versioncie = versioncie;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public Integer getEdadmin() {
		return edadmin;
	}

	public void setEdadmin(Integer edadmin) {
		this.edadmin = edadmin;
	}

	public String getUnidadedadmin() {
		return unidadedadmin;
	}

	public void setUnidadedadmin(String unidadedadmin) {
		this.unidadedadmin = unidadedadmin;
	}

	public Integer getEdadmax() {
		return edadmax;
	}

	public void setEdadmax(Integer edadmax) {
		this.edadmax = edadmax;
	}

	public String getUnidadedadmax() {
		return unidadedadmax;
	}

	public void setUnidadedadmax(String unidadedadmax) {
		this.unidadedadmax = unidadedadmax;
	}

	@XmlTransient
	public List<Cie> getCieList() {
		return cieList;
	}

	public void setCieList(List<Cie> cieList) {
		this.cieList = cieList;
	}

	public Cie getCieId() {
		return cieId;
	}

	public void setCieId(Cie cieId) {
		this.cieId = cieId;
	}

	public Detallecatalogo getCtsexoId() {
		return ctsexoId;
	}

	public void setCtsexoId(Detallecatalogo ctsexoId) {
		this.ctsexoId = ctsexoId;
	}

	public Integer getEstado() {
		return estado;
	}

	public void setEstado(Integer estado) {
		this.estado = estado;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Cie other = (Cie) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id)) {
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "ec.gob.msp.rdacaa.business.entity.Cie[ id=" + id + " ]";
	}

}
