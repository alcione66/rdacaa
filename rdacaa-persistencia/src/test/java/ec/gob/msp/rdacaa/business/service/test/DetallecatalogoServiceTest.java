/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.business.service.test;

import static ec.gob.msp.rdacaa.business.service.ConstantesDetalleCatalogo.EDAD_IG;
import static ec.gob.msp.rdacaa.business.service.ConstantesDetalleCatalogo.HOMBRE_ID;
import static ec.gob.msp.rdacaa.business.service.ConstantesDetalleCatalogo.MUJER_ID;
import static ec.gob.msp.rdacaa.business.service.ConstantesDetalleCatalogo.ORIENTACION_HOMBRE;
import static ec.gob.msp.rdacaa.business.service.ConstantesDetalleCatalogo.ORIENTACION_MUJER;

import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.entity.Detallecatalogo;
import ec.gob.msp.rdacaa.business.service.ConstantesDetalleCatalogo;
import ec.gob.msp.rdacaa.business.service.DetallecatalogoService;
import ec.gob.msp.rdacaa.test.PersistenciaTestContext;

/**
 *
 * @author Saulo Velasco
 */
@Ignore
@RunWith(SpringRunner.class)
@ContextConfiguration(classes = PersistenciaTestContext.class)
public class DetallecatalogoServiceTest {

	private static final Logger logger = LoggerFactory.getLogger(DetallecatalogoServiceTest.class);

	@Autowired
	private DetallecatalogoService detallecatalogoService;

	@Test
	public void whenHombreEdadMenorEDAD_IG_thenNoIncluirORIENTACION_MUJER() {
		List<Detallecatalogo> listOrientacioneSexual = detallecatalogoService.findByCatalogoIdSexoIdEdad(HOMBRE_ID,
				EDAD_IG - 1);
		Optional<Detallecatalogo> orientacionMujer = listOrientacioneSexual.parallelStream().filter(orientacion -> {
			return orientacion.getDescripcion().equals(ORIENTACION_MUJER);

		}).findAny();
		Assert.assertTrue("Para sexo hombre y la edad menor a " + EDAD_IG + " no puede existir orientación sexual "
				+ ORIENTACION_MUJER, !orientacionMujer.isPresent());
	}

	@Test
	public void whenHombreEdadIgualEDAD_IG_thenNoIncluirORIENTACION_MUJER() {
		List<Detallecatalogo> listOrientacioneSexual = detallecatalogoService.findByCatalogoIdSexoIdEdad(HOMBRE_ID, EDAD_IG);
		Optional<Detallecatalogo> orientacionMujer = listOrientacioneSexual.parallelStream().filter(orientacion -> {
			return orientacion.getDescripcion().equals(ORIENTACION_MUJER);

		}).findAny();
		Assert.assertTrue("Para sexo hombre y la edad igual a " + EDAD_IG + " no puede existir orientación sexual "
				+ ORIENTACION_MUJER, !orientacionMujer.isPresent());
	}

	@Test
	public void whenHombreEdadMayorEDAD_IG_thenNoIncluirORIENTACION_MUJER() {
		List<Detallecatalogo> listOrientacioneSexual = detallecatalogoService.findByCatalogoIdSexoIdEdad(HOMBRE_ID,
				EDAD_IG + 1);
		Optional<Detallecatalogo> orientacionMujer = listOrientacioneSexual.parallelStream().filter(orientacion -> {
			return orientacion.getDescripcion().equals(ORIENTACION_MUJER);

		}).findAny();
		Assert.assertTrue("Para sexo hombre y la edad mayor a " + EDAD_IG + " no puede existir orientación sexual "
				+ ORIENTACION_MUJER, !orientacionMujer.isPresent());
	}

	@Test
	public void whenHombreEdadMenorEDAD_IG_thenNoIncluirORIENTACION_HOMBRE() {
		List<Detallecatalogo> listOrientacioneSexual = detallecatalogoService.findByCatalogoIdSexoIdEdad(HOMBRE_ID,
				EDAD_IG - 1);
		Optional<Detallecatalogo> orientacionMujer = listOrientacioneSexual.parallelStream().filter(orientacion -> {
			return orientacion.getDescripcion().equals(ORIENTACION_HOMBRE);

		}).findAny();
		Assert.assertTrue("Para sexo hombre y la edad menor a " + EDAD_IG + " no puede existir orientación sexual "
				+ ORIENTACION_HOMBRE, !orientacionMujer.isPresent());
	}

	@Test
	public void whenHombreEdadIgualEDAD_IG_thenIncluirORIENTACION_HOMBRE() {
		List<Detallecatalogo> listOrientacioneSexual = detallecatalogoService.findByCatalogoIdSexoIdEdad(HOMBRE_ID, EDAD_IG);
		Optional<Detallecatalogo> orientacionMujer = listOrientacioneSexual.parallelStream().filter(orientacion -> {
			return orientacion.getDescripcion().equals(ORIENTACION_HOMBRE);

		}).findAny();
		Assert.assertTrue(
				"Para sexo hombre y la edad igual a " + EDAD_IG + " debe existir orientación sexual " + ORIENTACION_HOMBRE,
				orientacionMujer.isPresent());
	}

	@Test
	public void whenHombreEdadMayorEDAD_IG_thenIncluirORIENTACION_HOMBRE() {
		List<Detallecatalogo> listOrientacioneSexual = detallecatalogoService.findByCatalogoIdSexoIdEdad(HOMBRE_ID,
				EDAD_IG + 1);
		Optional<Detallecatalogo> orientacionMujer = listOrientacioneSexual.parallelStream().filter(orientacion -> {
			return orientacion.getDescripcion().equals(ORIENTACION_HOMBRE);

		}).findAny();
		Assert.assertTrue(
				"Para sexo hombre y la edad mayor a " + EDAD_IG + " debe existir orientación sexual " + ORIENTACION_HOMBRE,
				orientacionMujer.isPresent());
	}

	@Test
	public void whenMujerEdadMenorEDAD_IG_thenNoIncluirORIENTACION_HOMBRE() {
		List<Detallecatalogo> listOrientacioneSexual = detallecatalogoService.findByCatalogoIdSexoIdEdad(MUJER_ID,
				EDAD_IG - 1);
		Optional<Detallecatalogo> orientacionMujer = listOrientacioneSexual.parallelStream().filter(orientacion -> {
			return orientacion.getDescripcion().equals(ORIENTACION_HOMBRE);

		}).findAny();
		Assert.assertTrue("Para sexo mujer y la la edad menor a " + EDAD_IG + " no puede existir orientación sexual "
				+ ORIENTACION_HOMBRE, !orientacionMujer.isPresent());
	}

	@Test
	public void whenMujerEdadIgualEDAD_IG_thenNoIncluirORIENTACION_HOMBRE() {
		List<Detallecatalogo> listOrientacioneSexual = detallecatalogoService.findByCatalogoIdSexoIdEdad(MUJER_ID, EDAD_IG);
		Optional<Detallecatalogo> orientacionMujer = listOrientacioneSexual.parallelStream().filter(orientacion -> {
			return orientacion.getDescripcion().equals(ORIENTACION_HOMBRE);

		}).findAny();
		Assert.assertTrue("Para sexo mujer y la la edad igual a " + EDAD_IG + " no puede existir orientación sexual "
				+ ORIENTACION_HOMBRE, !orientacionMujer.isPresent());
	}

	@Test
	public void whenMujerEdadMayorEDAD_IG_thenNoIncluirORIENTACION_HOMBRE() {
		List<Detallecatalogo> listOrientacioneSexual = detallecatalogoService.findByCatalogoIdSexoIdEdad(MUJER_ID,
				EDAD_IG + 1);
		Optional<Detallecatalogo> orientacionMujer = listOrientacioneSexual.parallelStream().filter(orientacion -> {
			return orientacion.getDescripcion().equals(ORIENTACION_HOMBRE);

		}).findAny();
		Assert.assertTrue("Para sexo mujer y la edad mayor a " + EDAD_IG + " no puede existir orientación sexual "
				+ ORIENTACION_HOMBRE, !orientacionMujer.isPresent());
	}

	@Test
	public void whenMujerEdadMenorEDAD_IG_thenNoIncluirORIENTACION_MUJER() {
		List<Detallecatalogo> listOrientacioneSexual = detallecatalogoService.findByCatalogoIdSexoIdEdad(MUJER_ID,
				EDAD_IG - 1);
		Optional<Detallecatalogo> orientacionMujer = listOrientacioneSexual.parallelStream().filter(orientacion -> {
			return orientacion.getDescripcion().equals(ORIENTACION_MUJER);

		}).findAny();
		Assert.assertTrue("Para sexo mujer y la la edad menor a " + EDAD_IG + " no puede existir orientación sexual "
				+ ORIENTACION_MUJER, !orientacionMujer.isPresent());
	}

	@Test
	public void whenMujerEdadIgualEDAD_IG_thenIncluirORIENTACION_MUJER() {
		List<Detallecatalogo> listOrientacioneSexual = detallecatalogoService.findByCatalogoIdSexoIdEdad(MUJER_ID, EDAD_IG);
		Optional<Detallecatalogo> orientacionMujer = listOrientacioneSexual.parallelStream().filter(orientacion -> {
			return orientacion.getDescripcion().equals(ORIENTACION_MUJER);

		}).findAny();
		Assert.assertTrue(
				"Para sexo mujer y la la edad igual a " + EDAD_IG + " debe existir orientación sexual " + ORIENTACION_MUJER,
				orientacionMujer.isPresent());
	}

	@Test
	public void whenMujerEdadMayorEDAD_IG_thenIncluirORIENTACION_MUJER() {
		List<Detallecatalogo> listOrientacioneSexual = detallecatalogoService.findByCatalogoIdSexoIdEdad(MUJER_ID,
				EDAD_IG + 1);
		Optional<Detallecatalogo> orientacionMujer = listOrientacioneSexual.parallelStream().filter(orientacion -> {
			return orientacion.getDescripcion().equals(ORIENTACION_MUJER);

		}).findAny();
		Assert.assertTrue(
				"Para sexo mujer y la edad mayor a " + EDAD_IG + " debe existir orientación sexual " + ORIENTACION_MUJER,
				orientacionMujer.isPresent());
	}

	@Test
	public void whenFindAllTiposIdentificacion_thenVerifyIsNotNull() {
		List<Detallecatalogo> listTiposIdentificacion = detallecatalogoService.findAllTiposIdentificacion();
		listTiposIdentificacion.forEach(detCat -> logger.info("Catalogo ID: " + detCat.getCatalogoId().getId()
				+ " DetalleCatalogo ID: " + detCat.getId() + " Descripción: " + detCat.getDescripcion()));
		Assert.assertTrue(!listTiposIdentificacion.isEmpty());
	}

	@Test
	public void whenFindAllSexos_thenVerifyIsNotNull() {
		List<Detallecatalogo> listSexos = detallecatalogoService.findAllSexos();
		listSexos.forEach(detCat -> logger.info("Catalogo ID: " + detCat.getCatalogoId().getId() + " DetalleCatalogo ID: "
				+ detCat.getId() + " Descripción: " + detCat.getDescripcion()));
		Assert.assertTrue(!listSexos.isEmpty());
	}

	@Test
	public void whenFindAllTiposAtencion_thenVerifyIsNotNull() {
		List<Detallecatalogo> listTiposAtencion = detallecatalogoService.findAllTiposAtencion();
		listTiposAtencion.forEach(detCat -> logger.info("Catalogo ID: " + detCat.getCatalogoId().getId()
				+ " DetalleCatalogo ID: " + detCat.getId() + " Descripción: " + detCat.getDescripcion()));
		Assert.assertTrue(!listTiposAtencion.isEmpty());
	}

	@Test
	public void whenFindAllNacionalidades_thenVerifyIsNotNull() {
		List<Detallecatalogo> listNacionalidades = detallecatalogoService.findAllNacionalidades();
		listNacionalidades.forEach(detCat -> logger.info("Catalogo ID: " + detCat.getCatalogoId().getId()
				+ " DetalleCatalogo ID: " + detCat.getId() + " Descripción: " + detCat.getDescripcion()));
		Assert.assertTrue(!listNacionalidades.isEmpty());
	}

	@Test
	public void whenFindAllPueblosKichwa_thenVerifyIsNotNull() {
		List<Detallecatalogo> listPueblosKichwa = detallecatalogoService.findAllPueblosKichwa();
		listPueblosKichwa.forEach(detCat -> logger.info("Catalogo ID: " + detCat.getCatalogoId().getId()
				+ " DetalleCatalogo ID: " + detCat.getId() + " Descripción: " + detCat.getDescripcion()));
		Assert.assertTrue(!listPueblosKichwa.isEmpty());
	}

	@Test
	public void whenFindAllOrientacionSexual_thenVerifyIsNotNull() {
		List<Detallecatalogo> listOrientacionSexual = detallecatalogoService.findAllOrientacionSexual();
		listOrientacionSexual.forEach(detCat -> logger.info("Catalogo ID: " + detCat.getCatalogoId().getId()
				+ " DetalleCatalogo ID: " + detCat.getId() + " Descripción: " + detCat.getDescripcion()));
		Assert.assertTrue(!listOrientacionSexual.isEmpty());
	}

	@Test
	public void whenFindAllIdentidadGenero_thenVerifyIsNotNull() {
		List<Detallecatalogo> listIdentidadGenero = detallecatalogoService.findAllIdentidadGenero();
		listIdentidadGenero.forEach(detCat -> logger.info("Catalogo ID: " + detCat.getCatalogoId().getId()
				+ " DetalleCatalogo ID: " + detCat.getId() + " Descripción: " + detCat.getDescripcion()));
		Assert.assertTrue(!listIdentidadGenero.isEmpty());
	}

	@Test
	public void whenFindAllAutoidentificacionEtnica_thenVerifyIsNotNull() {
		List<Detallecatalogo> listAutoidentificacionEtnica = detallecatalogoService.findAllAutoidentificacionEtnica();
		listAutoidentificacionEtnica.forEach(detCat -> logger.info("Catalogo ID: " + detCat.getCatalogoId().getId()
				+ " DetalleCatalogo ID: " + detCat.getId() + " Descripción: " + detCat.getDescripcion()));
		Assert.assertTrue(!listAutoidentificacionEtnica.isEmpty());
	}

	@Test
	public void whenFindAllTipoBonoEstado_thenVerifyIsNotNull() {
		List<Detallecatalogo> listTipoBonoEstado = detallecatalogoService.findAllTipoBonoEstado();
		listTipoBonoEstado.forEach(detCat -> logger.info("Catalogo ID: " + detCat.getCatalogoId().getId()
				+ " DetalleCatalogo ID: " + detCat.getId() + " Descripción: " + detCat.getDescripcion()));
		Assert.assertTrue(!listTipoBonoEstado.isEmpty());
	}

	@Test
	public void whenFindAllTipoSeguroSalud_thenVerifyIsNotNull() {
		List<Detallecatalogo> listTipoSeguroSalud = detallecatalogoService.findAllTipoSeguroSalud();
		listTipoSeguroSalud.forEach(detCat -> logger.info("Catalogo ID: " + detCat.getCatalogoId().getId()
				+ " DetalleCatalogo ID: " + detCat.getId() + " Descripción: " + detCat.getDescripcion()));
		Assert.assertTrue(!listTipoSeguroSalud.isEmpty());
	}

	@Test
	public void whenFindAllAreasResidencia_thenVerifyIsNotNull() {
		List<Detallecatalogo> listAreasResidencia = detallecatalogoService.findAllAreasResidencia();
		listAreasResidencia.forEach(detCat -> logger.info("Catalogo ID: " + detCat.getCatalogoId().getId()
				+ " DetalleCatalogo ID: " + detCat.getId() + " Descripción: " + detCat.getDescripcion()));
		Assert.assertTrue(!listAreasResidencia.isEmpty());
	}

	@Test
	public void whenFindAllRegionesNaturales_thenVerifyIsNotNull() {
		List<Detallecatalogo> listRegionesNaturales = detallecatalogoService.findAllRegionesNaturales();
		listRegionesNaturales.forEach(detCat -> logger.info("Catalogo ID: " + detCat.getCatalogoId().getId()
				+ " DetalleCatalogo ID: " + detCat.getId() + " Descripción: " + detCat.getDescripcion()));
		Assert.assertTrue(!listRegionesNaturales.isEmpty());
	}

	@Test
	public void whenFindAllEspecialidadesMedicas_thenVerifyIsNotNull() {
		List<Detallecatalogo> listEspecialidadesMedicas = detallecatalogoService.findAllEspecialidadesMedicas();
		listEspecialidadesMedicas.forEach(detCat -> logger.info("Catalogo ID: " + detCat.getCatalogoId().getId()
				+ " DetalleCatalogo ID: " + detCat.getId() + " Descripción: " + detCat.getDescripcion()));
		Assert.assertTrue(!listEspecialidadesMedicas.isEmpty());
	}

	@Test
	public void whenFindAllGruposPrioritarios_thenVerifyIsNotNull() {
		List<Detallecatalogo> listTiposAtencion = detallecatalogoService.findAllGruposPrioritarios();
		listTiposAtencion.forEach(detCat -> logger.info("Catalogo ID: " + detCat.getCatalogoId().getId()
				+ " DetalleCatalogo ID: " + detCat.getId() + " Descripción: " + detCat.getDescripcion()));
		Assert.assertTrue(!listTiposAtencion.isEmpty());
	}

	@Test
	public void whenFindAllTipoPersona_thenVerifyIsNotNull() {
		List<Detallecatalogo> listGruposPrioritarios = detallecatalogoService.findAllTipoPersona();
		listGruposPrioritarios.forEach(detCat -> logger.info("Catalogo ID: " + detCat.getCatalogoId().getId()
				+ " DetalleCatalogo ID: " + detCat.getId() + " Descripción: " + detCat.getDescripcion()));
		Assert.assertTrue(!listGruposPrioritarios.isEmpty());
	}

	@Test
	public void whenFindAllCondicionDiagnostico_thenVerifyIsNotNull() {
		List<Detallecatalogo> listCondicionDiagnostico = detallecatalogoService.findAllCondicionDiagnostico();
		listCondicionDiagnostico.forEach(detCat -> logger.info("Catalogo ID: " + detCat.getCatalogoId().getId()
				+ " DetalleCatalogo ID: " + detCat.getId() + " Descripción: " + detCat.getDescripcion()));
		Assert.assertTrue(!listCondicionDiagnostico.isEmpty());
	}

	@Test
	public void whenFindAllFormacionProfesional_thenVerifyIsNotNull() {
		List<Detallecatalogo> listFormacionProfesional = detallecatalogoService.findAllFormacionProfesional();
		listFormacionProfesional.forEach(detCat -> logger.info("Catalogo ID: " + detCat.getCatalogoId().getId()
				+ " DetalleCatalogo ID: " + detCat.getId() + " Descripción: " + detCat.getDescripcion()));
		Assert.assertTrue(!listFormacionProfesional.isEmpty());
	}

	@Test
	public void whenFindAllLugarAtencion_thenVerifyIsNotNull() {
		List<Detallecatalogo> listLugarAtencion = detallecatalogoService.findAllLugarAtencion();
		listLugarAtencion.forEach(detCat -> logger.info("Catalogo ID: " + detCat.getCatalogoId().getId()
				+ " DetalleCatalogo ID: " + detCat.getId() + " Descripción: " + detCat.getDescripcion()));
		Assert.assertTrue(!listLugarAtencion.isEmpty());
	}

	@Test
	public void whenFindAllCatRiesgoObstetrico_thenVerifyIsNotNull() {
		List<Detallecatalogo> listCatRiesgoObstetrico = detallecatalogoService.findAllCatRiesgoObstetrico();
		listCatRiesgoObstetrico.forEach(detCat -> logger.info("Catalogo ID: " + detCat.getCatalogoId().getId()
				+ " DetalleCatalogo ID: " + detCat.getId() + " Descripción: " + detCat.getDescripcion()));
		Assert.assertTrue(!listCatRiesgoObstetrico.isEmpty());
	}

	@Test
	public void whenFindAllMetodosSemanasGestacion_thenVerifyIsNotNull() {
		List<Detallecatalogo> listMetodosSemanasGestacion = detallecatalogoService.findAllMetodosSemanasGestacion();
		listMetodosSemanasGestacion.forEach(detCat -> logger.info("Catalogo ID: " + detCat.getCatalogoId().getId()
				+ " DetalleCatalogo ID: " + detCat.getId() + " Descripción: " + detCat.getDescripcion()));
		Assert.assertTrue(!listMetodosSemanasGestacion.isEmpty());
	}

	@Test
	public void whenFindAllVacunas_thenVerifyIsNotNull() {
		List<Detallecatalogo> listVacunas = detallecatalogoService.findAllVacunas();
		listVacunas.forEach(detCat -> logger.info("Catalogo ID: " + detCat.getCatalogoId().getId() + " DetalleCatalogo ID: "
				+ detCat.getId() + " Descripción: " + detCat.getDescripcion()));
		Assert.assertTrue(!listVacunas.isEmpty());
	}

	@Test
	public void whenFindAllMotivoPruebaVIH_thenVerifyIsNotNull() {
		List<Detallecatalogo> listMotivoPruebaVIH = detallecatalogoService.findAllMotivoPruebaVIH();
		listMotivoPruebaVIH.forEach(detCat -> logger.info("Catalogo ID: " + detCat.getCatalogoId().getId()
				+ " DetalleCatalogo ID: " + detCat.getId() + " Descripción: " + detCat.getDescripcion()));
		Assert.assertTrue(!listMotivoPruebaVIH.isEmpty());
	}

	@Test
	public void whenFindAllViasTransmisionVIH_thenVerifyIsNotNull() {
		List<Detallecatalogo> listViasTransmisionVIH = detallecatalogoService.findAllViasTransmisionVIH();
		listViasTransmisionVIH.forEach(detCat -> logger.info("Catalogo ID: " + detCat.getCatalogoId().getId()
				+ " DetalleCatalogo ID: " + detCat.getId() + " Descripción: " + detCat.getDescripcion()));
		Assert.assertTrue(!listViasTransmisionVIH.isEmpty());
	}

	@Test
	public void whenFindAllTipoAtencion_thenVerifyIsNotNull() {
		List<Detallecatalogo> listTipoAtencion = detallecatalogoService.findAllTipoAtencion();
		listTipoAtencion.forEach(detCat -> logger.info("Catalogo ID: " + detCat.getCatalogoId().getId()
				+ " DetalleCatalogo ID: " + detCat.getId() + " Descripción: " + detCat.getDescripcion()));
		Assert.assertTrue(!listTipoAtencion.isEmpty());
	}

	@Test
	public void whenFindAllTipoDiagnostico_thenVerifyIsNotNull() {
		List<Detallecatalogo> listTipoDiagnostico = detallecatalogoService.findAllTipoDiagnostico();
		listTipoDiagnostico.forEach(detCat -> logger.info("Catalogo ID: " + detCat.getCatalogoId().getId()
				+ " DetalleCatalogo ID: " + detCat.getId() + " Descripción: " + detCat.getDescripcion()));
		Assert.assertTrue(!listTipoDiagnostico.isEmpty());
	}

	@Test
	public void whenFindTipoPaciente_thenVerifyIsNotNull() {
		Detallecatalogo tipoPaciente = detallecatalogoService.findTipoPaciente();
		logger.info("Catalogo ID: " + tipoPaciente.getCatalogoId().getId() + " DetalleCatalogo ID: " + tipoPaciente.getId()
				+ " Descripción: " + tipoPaciente.getDescripcion());
		Assert.assertTrue(tipoPaciente.getId().equals(ConstantesDetalleCatalogo.TIPO_PACIENTE_ID));
	}

	@Test
	public void whenFindTipoProfesionalSalud_thenVerifyIsNotNull() {
		Detallecatalogo tipoProfesionalSalud = detallecatalogoService.findTipoProfesionalSalud();
		logger.info("Catalogo ID: " + tipoProfesionalSalud.getCatalogoId().getId() + " DetalleCatalogo ID: "
				+ tipoProfesionalSalud.getId() + " Descripción: " + tipoProfesionalSalud.getDescripcion());
		Assert.assertTrue(tipoProfesionalSalud.getId().equals(ConstantesDetalleCatalogo.TIPO_PROFESIONAL_ID));
	}

	@Test
	public void whenFindTipoUsuarioSistema_thenVerifyIsNotNull() {
		Detallecatalogo tipoUsuarioSistema = detallecatalogoService.findTipoUsuarioSistema();
		logger.info("Catalogo ID: " + tipoUsuarioSistema.getCatalogoId().getId() + " DetalleCatalogo ID: "
				+ tipoUsuarioSistema.getId() + " Descripción: " + tipoUsuarioSistema.getDescripcion());
		Assert.assertTrue(tipoUsuarioSistema.getId().equals(ConstantesDetalleCatalogo.TIPO_USUARIO_SISTEMA_ID));
	}

	@Test
	public void whenFindCatalogoMedicionPeso_thenVerifyIsNotNull() {
		Detallecatalogo catalogoMedicionPeso = detallecatalogoService.findCatalogoMedicionPeso();
		logger.info("Catalogo ID: " + catalogoMedicionPeso.getCatalogoId().getId() + " DetalleCatalogo ID: "
				+ catalogoMedicionPeso.getId() + " Descripción: " + catalogoMedicionPeso.getDescripcion());
		Assert.assertTrue(catalogoMedicionPeso.getId().equals(ConstantesDetalleCatalogo.TIPO_MEDICION_PESO));
	}

	@Test
	public void whenFindCatalogoMedicionTalla_thenVerifyIsNotNull() {
		Detallecatalogo catalogoMedicionTalla = detallecatalogoService.findCatalogoMedicionTalla();
		logger.info("Catalogo ID: " + catalogoMedicionTalla.getCatalogoId().getId() + " DetalleCatalogo ID: "
				+ catalogoMedicionTalla.getId() + " Descripción: " + catalogoMedicionTalla.getDescripcion());
		Assert.assertTrue(catalogoMedicionTalla.getId().equals(ConstantesDetalleCatalogo.TIPO_MEDICION_TALLA));
	}

	@Test
	public void whenFindCatalogoMedicionHBRiesgo_thenVerifyIsNotNull() {
		Detallecatalogo catalogoMedicionHBRiesgo = detallecatalogoService.findCatalogoMedicionHBRiesgo();
		logger.info("Catalogo ID: " + catalogoMedicionHBRiesgo.getCatalogoId().getId() + " DetalleCatalogo ID: "
				+ catalogoMedicionHBRiesgo.getId() + " Descripción: " + catalogoMedicionHBRiesgo.getDescripcion());
		Assert.assertTrue(catalogoMedicionHBRiesgo.getId().equals(ConstantesDetalleCatalogo.TIPO_MEDICION_HB_RIESGO));
	}

	@Test
	public void whenFindCatalogoMedicionHBRiesgoCorregido_thenVerifyIsNotNull() {
		Detallecatalogo catalogoMedicionHBRiesgoCorregido = detallecatalogoService.findCatalogoMedicionHBRiesgoCorregido();
		logger.info("Catalogo ID: " + catalogoMedicionHBRiesgoCorregido.getCatalogoId().getId() + " DetalleCatalogo ID: "
				+ catalogoMedicionHBRiesgoCorregido.getId() + " Descripción: "
				+ catalogoMedicionHBRiesgoCorregido.getDescripcion());
		Assert.assertTrue(catalogoMedicionHBRiesgoCorregido.getId()
				.equals(ConstantesDetalleCatalogo.TIPO_MEDICION_HB_RIESGO_CORREGIDO));
	}
}
