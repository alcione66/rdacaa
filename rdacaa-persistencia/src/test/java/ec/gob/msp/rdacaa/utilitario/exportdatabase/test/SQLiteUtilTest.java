package ec.gob.msp.rdacaa.utilitario.exportdatabase.test;

import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.sql.SQLException;

import org.junit.Assert;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import ec.gob.msp.rdacaa.utilitario.exportdatabase.SQLiteUtil;
import org.junit.Ignore;
@Ignore
public class SQLiteUtilTest {

	private static final Logger logger = LoggerFactory.getLogger(SQLiteUtilTest.class);

	@Test
	public void whenIsNullPath_thenDefaultCreation() {
		File sqliteDatabase = SQLiteUtil.createDatabase(null);
		boolean exists = (null != sqliteDatabase && sqliteDatabase.exists());
		if (exists) {
			sqliteDatabase.deleteOnExit();
		}
		Assert.assertTrue("Se espera que la base se cree en la ruta de ejecución", exists);
	}

	@Test
	public void whenPath_thenVerifyCreation() {
		String path = System.getProperty("java.io.tmpdir") + FileSystems.getDefault().getSeparator() + "test.db";
		File sqliteDatabase = SQLiteUtil.createDatabase(path);
		logger.info(path);
		boolean exists = (null != sqliteDatabase && sqliteDatabase.exists());
		if (exists) {
			sqliteDatabase.deleteOnExit();
		}
		Assert.assertTrue("Se espera que la base se cree en la ruta ingresada", exists);
	}

	@Test
	public void whenDataExistsExport_thenVerifyExport() throws SQLException, IOException {

		ClassLoader classLoader = getClass().getClassLoader();
		File fileOrigen = new File(classLoader.getResource("dataTest.rdacaa").getFile());
		String pathSalida = System.getProperty("java.io.tmpdir") + FileSystems.getDefault().getSeparator() +"dataTestSalida.rdacaa";

		logger.info(fileOrigen.getAbsolutePath());
		logger.info(pathSalida);
		
		String sqlExport = "CREATE TABLE destino.atencion AS SELECT * FROM origen.atencion;";
		File sqlBaseOrigenToBaseDestino = SQLiteUtil.sqlBaseOrigenToBaseDestino(fileOrigen.getAbsolutePath(), pathSalida,
				"", sqlExport);
		boolean exists = (null != sqlBaseOrigenToBaseDestino && sqlBaseOrigenToBaseDestino.exists());
		if (exists) {
			sqlBaseOrigenToBaseDestino.deleteOnExit();
		}
		Assert.assertTrue("Se espera que la base se cree en la ruta ingresada", exists);
	}
}
