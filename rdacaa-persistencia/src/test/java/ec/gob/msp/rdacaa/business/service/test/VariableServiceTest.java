package ec.gob.msp.rdacaa.business.service.test;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

import ec.gob.msp.rdacaa.business.entity.Variable;
import ec.gob.msp.rdacaa.business.service.VariableService;
import ec.gob.msp.rdacaa.test.PersistenciaTestContext;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes = PersistenciaTestContext.class)
public class VariableServiceTest {
	
	private static final Logger logger = LoggerFactory.getLogger(VariableServiceTest.class);

	@Autowired
	private VariableService variableService;
	
	@Test
	public void whenFindAll_thenVerifyListaIsNotEmpty() {
		List<Variable> variables = variableService.findAll();
		variables.forEach(variable->{logger.info(variable.toString());});
		Assert.assertTrue(!variables.isEmpty());
	}
	
}
