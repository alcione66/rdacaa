package ec.gob.msp.rdacaa.decryption.webservice.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;

import ec.gob.msp.rdacaa.decryption.webservice.pojo.DecryptionRdacaaRequest;
import ec.gob.msp.rdacaa.decryption.webservice.pojo.DecryptionRdacaaResponse;
import ec.gob.msp.rdacaa.decryption.webservice.process.WSDecryptionWebServiceProcess;

@Endpoint
public class WSDecryptionRdacaaEndpoint {

	private static final String NAMESPACE_URI = "http://msp.gob.ec/decryption-rdacaa";

	@Autowired
	private WSDecryptionWebServiceProcess rdacaaValidationWebServiceProcess;

	@PayloadRoot(namespace = NAMESPACE_URI, localPart = "DecryptionRdacaaRequest")
	@ResponsePayload
	public DecryptionRdacaaResponse validateRdacaaFile(@RequestPayload DecryptionRdacaaRequest request) {
		return rdacaaValidationWebServiceProcess.runRdacaaValidationWebServiceProcess(request);
	}
}
