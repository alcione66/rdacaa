package ec.gob.msp.rdacaa.decryption.webservice.config;

import java.net.InetAddress;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true, securedEnabled = true)
public class SecurityConfig extends WebSecurityConfigurerAdapter {

	private static final Logger logger = LoggerFactory.getLogger(SecurityConfig.class);

	private static final String IP_ADDRESS_PERMITIDA_PREDETERMINADA = "hasIpAddress('127.0.0.1')";

	@Autowired
	private Environment env;

	@Override
	protected void configure(HttpSecurity http) throws Exception {
		http.authorizeRequests().antMatchers("/login").permitAll().antMatchers("/**/*.wsdl").permitAll()
				.antMatchers("/soapws/**").access(loadAutorizedIPs()).anyRequest().authenticated().and().formLogin()
				.permitAll().and().csrf().disable();
	}

	/**
	 * Lee variable de configuración y genera un string para filtrar por ip en el
	 * siguiente formato hasIpAddress('172.32.4.20') or hasIpAddress('172.32.4.27')
	 * 
	 * @return Retorna las ips autorizadas, en caso de ips en formato incorrecto
	 *         solo da acceso al 127.0.0.1 del servidor
	 *
	 */
	private String loadAutorizedIPs() {
		String rawIPs = env.getProperty("DECRYPT_AUTHORIZED_IPS");
		if (rawIPs == null) {
			return IP_ADDRESS_PERMITIDA_PREDETERMINADA;
		} else {
			String[] ips = rawIPs.split(",");
			StringBuilder allowedIPs = new StringBuilder();
			for (int i = 0; i < ips.length; i++) {
				ips[i] = ips[i].trim();
				try {
					InetAddress.getByName(ips[i]);
				} catch (Exception e) {
					logger.error("{} formato inválido", ips[i]);
					return IP_ADDRESS_PERMITIDA_PREDETERMINADA;
				}
				allowedIPs.append("hasIpAddress('").append(ips[i]).append("')");
				if (i != ips.length - 1) {
					allowedIPs.append(" or ");
				}
			}
			return allowedIPs.toString();
		}
	}
}
