package ec.gob.msp.rdacaa.decryption.webservice.process;

import java.io.IOException;
import java.security.GeneralSecurityException;

import javax.annotation.PostConstruct;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import com.google.crypto.tink.KeysetHandle;
import com.google.crypto.tink.config.TinkConfig;

import ec.gob.msp.rdacaa.decryption.webservice.pojo.DecryptionRdacaaRequest;
import ec.gob.msp.rdacaa.decryption.webservice.pojo.DecryptionRdacaaResponse;
import ec.gob.msp.rdacaa.decryption.webservice.pojo.RespuestaDecryptionRdacaa;
import ec.gob.msp.rdacaa.seguridades.util.SeguridadUtil;

@Component
public class WSDecryptionWebServiceProcess {

	private static final Logger logger = LoggerFactory.getLogger(WSDecryptionWebServiceProcess.class);

	@Value("${privatekeyAuth}")
	private String privatekeyPath;

	private static final String RESPONSE_OK = "000";
	private static final String RESPONSE_OK_MESSAGE = "Texto descifrado correctamente";

	private static final String ERROR_DESCIFRANDO = "-001";
	private static final String ERROR_DESCIFRANDO_MESSAGE = "Error descifrando el texto, puede tratarse de texto no cifrado o corrupto";

	private KeysetHandle keysetHandle;

	@PostConstruct
	public void init() throws GeneralSecurityException, IOException {
		// Initialize the config.
		TinkConfig.register();
		keysetHandle = SeguridadUtil.leerLlavePrivada(privatekeyPath);
	}

	public DecryptionRdacaaResponse runRdacaaValidationWebServiceProcess(DecryptionRdacaaRequest request) {
		String textoCifrado = request.getTextoCifrado();
		try {
			String textoDescifrado = SeguridadUtil.descifrarPrivadoString(textoCifrado, keysetHandle);
			return createResponseObject(RESPONSE_OK, RESPONSE_OK_MESSAGE, textoDescifrado);
		} catch (Exception e) {
			logger.error("Error descifrando texto", e);
			return createResponseObject(ERROR_DESCIFRANDO, ERROR_DESCIFRANDO_MESSAGE, "");
		}
	}

	private DecryptionRdacaaResponse createResponseObject(String codigoMensaje, String mensaje,
			String textoDescifrado) {
		DecryptionRdacaaResponse decryptionRdacaaResponse = new DecryptionRdacaaResponse();
		RespuestaDecryptionRdacaa response = new RespuestaDecryptionRdacaa();
		response.setCodigoMensaje(codigoMensaje);
		response.setMensaje(mensaje);
		response.setTextoDescifrado(textoDescifrado);

		decryptionRdacaaResponse.setResponse(response);
		return decryptionRdacaaResponse;
	}

}
