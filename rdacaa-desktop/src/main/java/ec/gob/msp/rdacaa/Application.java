package ec.gob.msp.rdacaa;

import java.awt.EventQueue;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Profile;

import ec.gob.msp.rdacaa.principal.controller.AccesoPrincipalController;
import ec.gob.msp.rdacaa.principal.forma.Splash;
import ec.gob.msp.rdacaa.reporte.controller.AtencionesController;

@SpringBootApplication
@Profile(value = { "default", "prod" })
@ComponentScan(basePackages = { "ec.gob.msp" }, lazyInit = true)
public class Application {

	private static final Logger logger = LoggerFactory.getLogger(AtencionesController.class);

	public static void main(String[] args) {
		Splash splash = new Splash();
		splash.setVisible(true);

		ConfigurableApplicationContext context = new SpringApplicationBuilder(Application.class).headless(false)
				.run(args);

		EventQueue.invokeLater(() -> {
			AccesoPrincipalController accesoPrincipalController = context.getBean(AccesoPrincipalController.class);
			accesoPrincipalController.prepareAndOpenFrame();
			splash.setVisible(false);
			splash.removeAll();
		});

	}
}
