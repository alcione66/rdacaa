
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.amed.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.Serializable;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import ec.gob.msp.rdacaa.admision.model.DetalleCatalogoToStringConverter;
import ec.gob.msp.rdacaa.amed.forma.VIH;
import ec.gob.msp.rdacaa.amed.model.AmedCmbMotivoPruebaVihModel;
import ec.gob.msp.rdacaa.amed.model.CmbMotivoVIHCellRenderer;
import ec.gob.msp.rdacaa.business.entity.Detallecatalogo;
import ec.gob.msp.rdacaa.business.entity.Vihsida;
import ec.gob.msp.rdacaa.business.service.DetallecatalogoService;
import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.utilitario.forma.UtilitarioForma;

/**
 *
 * @author dmurillo
 */
@Controller
public class VIHController implements Serializable{

	private static final long serialVersionUID = 7910995143527526736L;
	private VIH vIHFrame;
	private final DetallecatalogoService detallecatalogoService;
	private Detallecatalogo detallecatalogoNull;
	private JComboBox<Detallecatalogo> cmbMotivoPruebaVIH;
	private JComboBox<Detallecatalogo> cmbPruebaUno;
	private JComboBox<Detallecatalogo> cmbPruebaDos;
	private JComboBox<Detallecatalogo> cmbViaTransmision;
	private JTextField txtCargaViral;
	private JTextField txtCD4;
	private JButton btnNuevo;
	private JRadioButton rdReactivaUno;
	private JRadioButton rdNoReactivaUno;
	private JRadioButton rdReactivaDos;
	private JRadioButton rdNoReactivaDos;
	
	private AmedCmbMotivoPruebaVihModel vihMotivosModel;
	private AmedCmbMotivoPruebaVihModel vihPruebaUnoModel;
	private AmedCmbMotivoPruebaVihModel vihPruebaDosModel;
	private AmedCmbMotivoPruebaVihModel vihViasModel;
	
	private ButtonGroup grupo1eraPrueba;
	private ButtonGroup grupo2daPrueba;
	private Object[] controlesValidar = new Object[9];
	
	@Autowired
    public VIHController(VIH vIHFrame, 
    		             DetallecatalogoService detallecatalogoService
    		            ){
		this.vIHFrame = vIHFrame;
		this.detallecatalogoService = detallecatalogoService;
	}
	
	@PostConstruct
    private void init() {
		this.cmbMotivoPruebaVIH = vIHFrame.getCmbMotivoPruebaVIH();
		this.cmbPruebaUno = vIHFrame.getCmbPruebaUno();
		this.cmbPruebaDos = vIHFrame.getCmbPruebaDos();
		this.cmbViaTransmision = vIHFrame.getCmbViaTransmision();
		this.rdReactivaUno = vIHFrame.getRdReactivaUno();
		this.rdNoReactivaUno = vIHFrame.getRdNoReactivaUno();
		this.rdReactivaDos = vIHFrame.getRdReactivaDos();
		this.rdNoReactivaDos = vIHFrame.getRdNoReactivaDos();
		this.btnNuevo = vIHFrame.getBtnNuevo();
		this.txtCargaViral = vIHFrame.getTxtCargaViral();
		this.txtCD4 = vIHFrame.getTxtCD4();
		this.grupo1eraPrueba = vIHFrame.getGrupo1eraPrueba();
		this.grupo2daPrueba = vIHFrame.getGrupo2daPrueba();
		this.vihMotivosModel = new AmedCmbMotivoPruebaVihModel();
		this.vihPruebaUnoModel = new AmedCmbMotivoPruebaVihModel();
		this.vihPruebaDosModel = new AmedCmbMotivoPruebaVihModel();
		this.vihViasModel = new AmedCmbMotivoPruebaVihModel();
		loadDefaultCombos();
		addAutocompleteToCombos();
		loadMotivo();
                
		loadPruebaUno();
		loadPruebaDos();
		loadVias();
                cmbMotivoPruebaVIH.addActionListener(dt -> enabledPruebasUno());
                cmbPruebaUno.addActionListener(dt -> enabledPruebasDos());
//                cmbPruebaUno.addActionListener(dt -> loadPruebaDos());
		txtCargaViral.addKeyListener(new java.awt.event.KeyAdapter() {
        	@Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
        		UtilitarioForma.soloNumeros(evt);
        		UtilitarioForma.validarLongitudCampo(evt, 8);
            }
        });
		txtCargaViral.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
				// No se necesita
			}

			@Override
			public void focusLost(FocusEvent e) {
				if(!isCargaViralCorrecta()){
					notificacionErrorCargaViral();
				}
			}
		});
		txtCD4.addKeyListener(new java.awt.event.KeyAdapter() {
        	@Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
        		UtilitarioForma.soloNumeros(evt);
        		UtilitarioForma.validarLongitudCampo(evt, 4);
            }
        });
		txtCD4.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
				// No se necesita
			}

			@Override
			public void focusLost(FocusEvent e) {
				if(!isCD4Correcto()){
					notificacionErrorCD4();
				}
			}
		});
//		enabledPruebas();
                enabledPruebasUno();
                enabledPruebasDos();
		btnNuevo.addMouseListener (new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {
				cleanInput();
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// No se usa
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// No se usa
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// No se usa
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// No se usa
			}
        });
		rdReactivaUno.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				validateReactiva();
			}
		});
		rdNoReactivaUno.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				validateReactiva();
			}
		});
		rdReactivaDos.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				validateReactiva();
			}
		});
		rdNoReactivaDos.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				validateReactiva();
			}
		});
		cmbPruebaUno.addActionListener(dt -> enabledRbPruebaUno());
		cmbPruebaDos.addActionListener(dt -> enabledRbPruebaDos());
		loadControles();
	}
	
	private void loadMotivo(){
		List<Detallecatalogo> motivos = detallecatalogoService.findAllMotivoPruebaVIH();
		vihMotivosModel.clear();
		vihMotivosModel.addElements(motivos);
        cmbMotivoPruebaVIH.setRenderer(new CmbMotivoVIHCellRenderer());
        cmbMotivoPruebaVIH.setModel(vihMotivosModel);
        cmbMotivoPruebaVIH.setSelectedItem(detallecatalogoNull);
	}
	
	private void loadPruebaUno(){
		List<Detallecatalogo> prueba = detallecatalogoService.findAllPruebasVIH();
		vihPruebaUnoModel.clear();
		vihPruebaUnoModel.addElements(prueba);
        cmbPruebaUno.setRenderer(new CmbMotivoVIHCellRenderer());
        cmbPruebaUno.setModel(vihPruebaUnoModel);
        cmbPruebaUno.setSelectedItem(detallecatalogoNull);
	}
	private void loadPruebaDos(){
		List<Detallecatalogo> prueba = detallecatalogoService.findAllPruebasVIH();
		vihPruebaDosModel.clear();
		vihPruebaDosModel.addElements(prueba);
        cmbPruebaDos.setRenderer(new CmbMotivoVIHCellRenderer());
        cmbPruebaDos.setModel(vihPruebaDosModel);
        cmbPruebaDos.setSelectedItem(detallecatalogoNull);
	}
	private void loadVias(){
		List<Detallecatalogo> vias = detallecatalogoService.findAllViasTransmisionVIH();
		vihViasModel.clear();
		vihViasModel.addElements(vias);
        cmbViaTransmision.setRenderer(new CmbMotivoVIHCellRenderer());
        cmbViaTransmision.setModel(vihViasModel);
        cmbViaTransmision.setSelectedItem(detallecatalogoNull);
	}
	private void loadDefaultCombos(){
		detallecatalogoNull = new Detallecatalogo(){
            private static final long serialVersionUID = 1L;
            boolean isNoSelectable = true;
        };
        detallecatalogoNull.setId(0);
        detallecatalogoNull.setDescripcion("Seleccione");
	}
	public void cleanInput() {
		limpiarCombos();
		txtCargaViral.setText("");
		txtCD4.setText(""); 
		UtilitarioForma.limpiarCampos(vIHFrame,grupo1eraPrueba, grupo2daPrueba, rdReactivaUno,rdNoReactivaUno,rdReactivaDos,rdNoReactivaDos);
//		enabledPruebas();
                enabledPruebasUno();
                enabledPruebasDos();
	}
	
	private void enabledPruebasUno() {
		if(vihMotivosModel.getSelectedItem().getId() != 0){
			controlesValidar[0] = cmbPruebaUno;
			controlesValidar[1] = cmbPruebaDos;
			controlesValidar[2] = "";
			controlesValidar[3] = "";
			controlesValidar[4] = "";
			controlesValidar[5] = "";
			controlesValidar[6] = "";
			controlesValidar[7] = grupo1eraPrueba;
			controlesValidar[8] = grupo2daPrueba;
			UtilitarioForma.habilitarControles(true, cmbPruebaUno);
		}else{
			controlesValidar[0] = "";
			controlesValidar[1] = "";
			controlesValidar[2] = "";
			controlesValidar[3] = "";
			controlesValidar[4] = "";
			controlesValidar[5] = "";
			controlesValidar[6] = "";
			controlesValidar[7] = "";
			controlesValidar[8] = "";
			UtilitarioForma.habilitarControles(false, cmbPruebaUno,cmbPruebaDos,cmbViaTransmision,rdReactivaUno,rdNoReactivaUno,rdReactivaDos,rdNoReactivaDos);
		}
	}
        private void enabledPruebasDos() {
		if(vihMotivosModel.getSelectedItem().getId() != 0){
			controlesValidar[0] = cmbPruebaUno;
			controlesValidar[1] = cmbPruebaDos;
			controlesValidar[2] = "";
			controlesValidar[3] = "";
			controlesValidar[4] = "";
			controlesValidar[5] = "";
			controlesValidar[6] = "";
			controlesValidar[7] = grupo1eraPrueba;
			controlesValidar[8] = grupo2daPrueba;
			UtilitarioForma.habilitarControles(true, cmbPruebaDos);
		}else{
			controlesValidar[0] = "";
			controlesValidar[1] = "";
			controlesValidar[2] = "";
			controlesValidar[3] = "";
			controlesValidar[4] = "";
			controlesValidar[5] = "";
			controlesValidar[6] = "";
			controlesValidar[7] = "";
			controlesValidar[8] = "";
			UtilitarioForma.habilitarControles(false, cmbPruebaUno,cmbPruebaDos,cmbViaTransmision,rdReactivaUno,rdNoReactivaUno,rdReactivaDos,rdNoReactivaDos);
		}
	}
	public Boolean validateForm(){
		Boolean validarForma = false;
		if(vihMotivosModel.getSelectedItem().getId() != 0){
			if(vihPruebaUnoModel.getSelectedItem().getId() != 0 && vihPruebaDosModel.getSelectedItem().getId() == 0){
				controlesValidar[1] = "";
				controlesValidar[5] = "";
				controlesValidar[6] = "";
				controlesValidar[8] = "";
			}else if(vihPruebaDosModel.getSelectedItem().getId() != 0 && vihPruebaUnoModel.getSelectedItem().getId() == 0){
				controlesValidar[0] = "";
				controlesValidar[3] = "";
				controlesValidar[4] = "";
				controlesValidar[7] = "";
			}
			validarForma = UtilitarioForma.validarCamposRequeridos(controlesValidar) && isCargaViralCorrecta()
					&& isCD4Correcto();
		}else{
			validarForma = isCargaViralCorrecta() && isCD4Correcto();
		}
		
		if(!isCargaViralCorrecta()){
			notificacionErrorCargaViral();
		}

		if(!isCD4Correcto()){
			notificacionErrorCD4();
		}
		
		VariableSesion.getInstanciaVariablesSession().setVihsida(null);

		if(validarForma){
			getVIH();
		}
		
		return validarForma;
	}
	
	private boolean isCD4Correcto() {
		if(!isCD4Empty()){
			double cd4 = Double.parseDouble(txtCD4.getText());
			return cd4 >= 500 && cd4 <= 1500;
		}
		return true;
	}

	private boolean isCD4Empty() {
		return txtCD4.getText() == null || txtCD4.getText().equals("");
	}

	private boolean isCargaViralCorrecta() {
		if(!isCargaViralEmpty() ){
			double cargaViral = Double.parseDouble(txtCargaViral.getText());
			return cargaViral >= 0 && cargaViral <= 10000000; 
		}
		return true;
	}

	private boolean isCargaViralEmpty() {
		return txtCargaViral.getText() == null || txtCargaViral.getText().equals("");
	}

	private void addAutocompleteToCombos() {
    	AutoCompleteDecorator.decorate(cmbMotivoPruebaVIH, new DetalleCatalogoToStringConverter());
        AutoCompleteDecorator.decorate(cmbPruebaUno, new DetalleCatalogoToStringConverter());
        AutoCompleteDecorator.decorate(cmbPruebaDos, new DetalleCatalogoToStringConverter());
        AutoCompleteDecorator.decorate(cmbViaTransmision, new DetalleCatalogoToStringConverter());
    }
	
	private void validateReactiva(){
		if(rdReactivaUno.isSelected() && rdReactivaDos.isSelected()){
			controlesValidar[2] = cmbViaTransmision;
			UtilitarioForma.habilitarControles(true, cmbViaTransmision);
		}else{
			controlesValidar[2] = "";
			UtilitarioForma.habilitarControles(false, cmbViaTransmision);
			UtilitarioForma.limpiarCampos(vIHFrame, cmbViaTransmision);
			cmbViaTransmision.setSelectedItem(detallecatalogoNull);
		}
	}
	private void enabledRbPruebaUno(){
		if(vihPruebaUnoModel.getSelectedItem().getId() != 0){
			controlesValidar[0] = cmbPruebaUno;
			controlesValidar[3] = rdReactivaUno;
			controlesValidar[4] = rdNoReactivaUno;
			controlesValidar[7] = grupo1eraPrueba;
			UtilitarioForma.habilitarControles(true, rdReactivaUno,rdNoReactivaUno);
		}else{
			controlesValidar[0] = "";
			controlesValidar[3] = "";
			controlesValidar[4] = "";
			controlesValidar[7] = "";
			UtilitarioForma.habilitarControles(false, rdReactivaUno,rdNoReactivaUno);
		}
	}
	private void enabledRbPruebaDos(){
		if(vihPruebaDosModel.getSelectedItem().getId() != 0){
			controlesValidar[1] = cmbPruebaDos;
			controlesValidar[5] = rdReactivaDos;
			controlesValidar[6] = rdNoReactivaDos;
			controlesValidar[8] = grupo2daPrueba;
			UtilitarioForma.habilitarControles(true, rdReactivaDos,rdNoReactivaDos);
		}else{
			controlesValidar[1] = "";
			controlesValidar[5] = "";
			controlesValidar[6] = "";
			controlesValidar[8] = "";
			UtilitarioForma.habilitarControles(false, rdReactivaDos,rdNoReactivaDos);
		}
	}
	private void getVIH(){
		if(vihMotivosModel.getSelectedItem().getId() != 0){
			Vihsida vihsida = new Vihsida();
			vihsida.setAtencionmedicaId(VariableSesion.getInstanciaVariablesSession().getAtencionMedica());
			if(!txtCargaViral.getText().trim().equals("")){
				vihsida.setCargaviral(Integer.valueOf(txtCargaViral.getText()));
			}
			if(!txtCD4.getText().trim().equals("")){
				vihsida.setCd4(Integer.valueOf(txtCD4.getText()));
			}
			vihsida.setCtmotivopruebaId(vihMotivosModel.getSelectedItem());
			if(vihPruebaUnoModel.getSelectedItem() != null){
				vihsida.setCtpruebaunoId(vihPruebaUnoModel.getSelectedItem());
			}
			if(vihPruebaDosModel.getSelectedItem() != null){
				vihsida.setCtpruebadosId(vihPruebaDosModel.getSelectedItem());
			}
			if(vihViasModel.getSelectedItem() != null && vihViasModel.getSelectedItem().getId() != 0 ){
				vihsida.setCtviastransmisionId(vihViasModel.getSelectedItem());
			}
			if(rdReactivaUno.isSelected()){
				vihsida.setReactivaonouno(1);
			}else if(rdNoReactivaDos.isSelected()){
				vihsida.setReactivaonouno(0);
			}
			if(rdReactivaDos.isSelected()){
				vihsida.setReactivaonodos(1);
			}else if(rdNoReactivaDos.isSelected()){
				vihsida.setReactivaonodos(0);
			}
			VariableSesion.getInstanciaVariablesSession().setVihsida(vihsida);
		}else if(!isCD4Empty() || !isCargaViralEmpty()) {
			Vihsida vihsida = new Vihsida();
			vihsida.setAtencionmedicaId(VariableSesion.getInstanciaVariablesSession().getAtencionMedica());
			if(!txtCargaViral.getText().trim().equals("")){
				vihsida.setCargaviral(Integer.valueOf(txtCargaViral.getText()));
			}
			if(!txtCD4.getText().trim().equals("")){
				vihsida.setCd4(Integer.valueOf(txtCD4.getText()));
			}
			VariableSesion.getInstanciaVariablesSession().setVihsida(vihsida);
		}
		
	}
	private void limpiarCombos(){
		cmbMotivoPruebaVIH.setSelectedItem(detallecatalogoNull);
		cmbPruebaUno.setSelectedItem(detallecatalogoNull);
		cmbPruebaDos.setSelectedItem(detallecatalogoNull);
		cmbViaTransmision.setSelectedItem(detallecatalogoNull);
	}
	private void loadControles(){
		controlesValidar[0] = cmbPruebaUno;
		controlesValidar[1] = cmbPruebaDos;
		controlesValidar[2] = cmbViaTransmision;
		controlesValidar[3] = rdReactivaUno;
		controlesValidar[4] = rdNoReactivaUno;
		controlesValidar[5] = rdReactivaDos;
		controlesValidar[6] = rdNoReactivaDos;
		controlesValidar[7] = grupo1eraPrueba;
		controlesValidar[8] = grupo2daPrueba;
	}

	private void notificacionErrorCargaViral() {
		UtilitarioForma.muestraMensaje("Error Carga Viral", vIHFrame, "ERROR", "Verificar valor registrado de Carga Viral, rango permitido de 0 a 10000000");
	}

	private void notificacionErrorCD4() {
		UtilitarioForma.muestraMensaje("Error CD4", vIHFrame, "ERROR", "Verificar valor registrado de CD4, rango permitido de 500 a 1500");
	}
}