/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.amed.controller;

import javax.annotation.PostConstruct;
import javax.swing.ButtonGroup;
import javax.swing.JRadioButton;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import ec.gob.msp.rdacaa.amed.forma.DatosSIVAN;
import ec.gob.msp.rdacaa.business.entity.Atencionsivan;
import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.utilitario.forma.UtilitarioForma;

/**
 *
 * @author eduardo
 */
@Controller
public class DatosSIVANController {

	private final DatosSIVAN datosSIVANFrame;

	private JRadioButton rbperiodolactanciaSI;
	private JRadioButton rbperiodolactanciaNO;
	private JRadioButton rbrecibiolechematSI;
	private JRadioButton rbrecibiolechematNO;
	private JRadioButton rbconsumioalimentosolidSI;
	private JRadioButton rbconsumioalimentosolidNO;

	private ButtonGroup btnGroupRecibioLecheMat;
	private ButtonGroup btnGroupConsumioAlimentoSolid;
	private ButtonGroup btnGroupPeriodoLactancia;
	private Object[] controlesValidar;

	private final VariableSesion variableSesion = VariableSesion.getInstanciaVariablesSession();

	@Autowired
	public DatosSIVANController(DatosSIVAN datosSIVANFrame) {
		this.datosSIVANFrame = datosSIVANFrame;
	}

	@PostConstruct
	private void init() {
		rbperiodolactanciaSI = datosSIVANFrame.getRbperiodolactanciaSI();
		rbperiodolactanciaNO = datosSIVANFrame.getRbperiodolactanciaNO();
		rbrecibiolechematSI = datosSIVANFrame.getRbrecibiolechematSI();
		rbrecibiolechematNO = datosSIVANFrame.getRbrecibiolechematNO();
		rbconsumioalimentosolidSI = datosSIVANFrame.getRbconsumioalimentosolidSI();
		rbconsumioalimentosolidNO = datosSIVANFrame.getRbconsumioalimentosolidNO();

		btnGroupRecibioLecheMat = datosSIVANFrame.getBtnGroupRecibioLecheMat();
		btnGroupConsumioAlimentoSolid = datosSIVANFrame.getBtnGroupConsumioAlimentoSolid();
		btnGroupPeriodoLactancia = datosSIVANFrame.getBtnGroupPeriodoLactancia();

	}

	public void getSIVAN() {
		Atencionsivan atencionsivan = new Atencionsivan();
		if (rbrecibiolechematSI.isEnabled() && rbrecibiolechematNO.isEnabled()) {
			if (btnGroupRecibioLecheMat.getSelection().equals(rbrecibiolechematSI.getModel())) {
				atencionsivan.setRecibiolechematerna(1);
			} else if (btnGroupRecibioLecheMat.getSelection().equals(rbrecibiolechematNO.getModel())) {
				atencionsivan.setRecibiolechematerna(0);
			}
		}
		if (rbconsumioalimentosolidSI.isEnabled() && rbconsumioalimentosolidNO.isEnabled()) {
			if (btnGroupConsumioAlimentoSolid.getSelection().equals(rbconsumioalimentosolidSI.getModel())) {
				atencionsivan.setCosumioalimentosol(1);
			} else if (btnGroupConsumioAlimentoSolid.getSelection().equals(rbconsumioalimentosolidNO.getModel())) {
				atencionsivan.setCosumioalimentosol(0);
			}
		}
		if (rbperiodolactanciaSI.isEnabled() && rbperiodolactanciaNO.isEnabled()) {
			if (btnGroupPeriodoLactancia.getSelection().equals(rbperiodolactanciaSI.getModel())) {
				atencionsivan.setMadrelactancia(1);
			} else if (btnGroupPeriodoLactancia.getSelection().equals(rbperiodolactanciaNO.getModel())) {
				atencionsivan.setMadrelactancia(0);
			}
		}
		atencionsivan.setObservacion("N/A");
		variableSesion.setAtencionsivan(atencionsivan);
	}

	public boolean validateForm() {
		boolean validarForma = false;
		controlesValidar = new Object[3];
		if (datosSIVANFrame.getBtnGroupConsumioAlimentoSolid() != null
				&& datosSIVANFrame.getBtnGroupPeriodoLactancia() != null
				&& datosSIVANFrame.getBtnGroupRecibioLecheMat() != null) {
			if (rbconsumioalimentosolidSI.isEnabled() && rbconsumioalimentosolidNO.isEnabled()) {
				controlesValidar[0] = datosSIVANFrame.getBtnGroupConsumioAlimentoSolid();
			}
			if (rbperiodolactanciaSI.isEnabled() && rbperiodolactanciaNO.isEnabled()) {
				controlesValidar[1] = datosSIVANFrame.getBtnGroupPeriodoLactancia();

			}
			if (rbrecibiolechematSI.isEnabled() && rbrecibiolechematNO.isEnabled()) {
				controlesValidar[2] = datosSIVANFrame.getBtnGroupRecibioLecheMat();
			}
			validarForma = UtilitarioForma.validarCamposRequeridos(controlesValidar);
			if (validarForma) {
				getSIVAN();
			}
		} else {
			validarForma = true;
		}
		
		return validarForma;
	}

	public void cleanInput() {
		btnGroupConsumioAlimentoSolid.clearSelection();
		btnGroupPeriodoLactancia.clearSelection();
		btnGroupRecibioLecheMat.clearSelection();
		rbperiodolactanciaSI.setSelected(false);
		rbperiodolactanciaNO.setSelected(false);
		rbrecibiolechematSI.setSelected(false);
		rbrecibiolechematNO.setSelected(false);
		rbconsumioalimentosolidSI.setSelected(false);
		rbconsumioalimentosolidNO.setSelected(false);
	}

}
