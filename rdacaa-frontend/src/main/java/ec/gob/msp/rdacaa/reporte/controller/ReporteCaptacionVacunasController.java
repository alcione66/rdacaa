package ec.gob.msp.rdacaa.reporte.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.swing.JButton;
import javax.swing.JRadioButton;
import javax.swing.JTable;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;

import com.github.lgooddatepicker.components.DatePicker;

import ec.gob.msp.rdacaa.business.entity.Atencionmedica;
import ec.gob.msp.rdacaa.business.entity.Esquemavacunacion;
import ec.gob.msp.rdacaa.business.entity.Intraextramural;
import ec.gob.msp.rdacaa.business.entity.Registrovacunacion;
import ec.gob.msp.rdacaa.business.service.AtencionmedicaService;
import ec.gob.msp.rdacaa.business.service.EsquemavacunacionService;
import ec.gob.msp.rdacaa.business.service.IntraextramuralService;
import ec.gob.msp.rdacaa.business.service.PersonaService;
import ec.gob.msp.rdacaa.business.service.RegistrovacunacionService;
import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.reporte.forma.ReporteCaptacionVacunas;
import ec.gob.msp.rdacaa.reporte.model.CaptacionTardiaReporte;
import ec.gob.msp.rdacaa.reporte.model.CaptacionVacunasModel;
import ec.gob.msp.rdacaa.reporte.utilitario.ReportesUtil;
import ec.gob.msp.rdacaa.seguridades.util.SeguridadUtil;
import ec.gob.msp.rdacaa.utilitario.forma.FormatoTabla;

@Controller
@Lazy(value = false)
public class ReporteCaptacionVacunasController {

	private static final Logger logger = LoggerFactory.getLogger(ReporteCaptacionVacunasController.class);
	private AtencionmedicaService atencionmedicaService;
	private RegistrovacunacionService registrovacunacionService;
	private PersonaService personaService;
	private EsquemavacunacionService esquemavacunacionService;
	private IntraextramuralService intraextramuralService;
	private JButton btnBuscar;
	private JButton btnDescargar;
	private JTable tblReporte;
	private DatePicker dttDesde;
	private DatePicker dttHasta;
	private List<CaptacionTardiaReporte> controles;
	private ReporteCaptacionVacunas reporteCaptacionVacunasFrame;
	private Integer tipoEsquema = 0;
	private JRadioButton temprana;
	private JRadioButton tardia;
	private List<Esquemavacunacion> esquema;
	private static final int ESQUEMATEMPRANO = 820;
	private static final int ESQUEMATARDIO = 821;
	private static final String DOSISUNO = "1";
	private static final String DOSISDOS = "2";
	private static final String DOSISTRES = "3";
	private static final String DOSISCUATRO = "4";
	private static final String DOSISCINCO = "5";
	private static final String DOSISSEIS = "6";
	private CaptacionVacunasModel modeloTabla;
	
	
	@Autowired
	public ReporteCaptacionVacunasController(ReporteCaptacionVacunas reporteCaptacionVacunasFrame,
			                                 RegistrovacunacionService registrovacunacionService,
			                                 AtencionmedicaService atencionmedicaService,
			                                 PersonaService personaService,
			                                 EsquemavacunacionService esquemavacunacionService,
			                                 IntraextramuralService intraextramuralService
			                                ){
		this.reporteCaptacionVacunasFrame = reporteCaptacionVacunasFrame;
		this.registrovacunacionService = registrovacunacionService;
		this.atencionmedicaService = atencionmedicaService;
		this.personaService = personaService;
		this.esquemavacunacionService = esquemavacunacionService;
		this.intraextramuralService = intraextramuralService;
	}
	
	@PostConstruct
	private void init() {
		this.dttDesde = reporteCaptacionVacunasFrame.getDttDesde();
		this.dttHasta = reporteCaptacionVacunasFrame.getDttHasta();
		this.btnBuscar = reporteCaptacionVacunasFrame.getBtnBuscar();
		this.btnDescargar = reporteCaptacionVacunasFrame.getBtnDescargar();
		this.tblReporte = reporteCaptacionVacunasFrame.getTblReporte();
		this.temprana = reporteCaptacionVacunasFrame.getRbTemprana();
		this.tardia = reporteCaptacionVacunasFrame.getRbTardia();

		btnBuscar.addActionListener(e -> consultar());
		btnDescargar.addActionListener(e -> exportarXLS());
		esquema = new ArrayList<>();
		cargarTabla();
		reporteCaptacionVacunasFrame.addInternalFrameListener( new  InternalFrameAdapter() {
			@Override
			public void internalFrameClosing(InternalFrameEvent e) {
				super.internalFrameClosing(e);
				cerrar();
			}
		});
	}
	
	private void consultar() {
		if (dttDesde.getDate() != null && dttHasta.getDate() != null) {
			if(temprana.isSelected()){
				tipoEsquema = ESQUEMATEMPRANO;
			}
			if(tardia.isSelected()){
				tipoEsquema = ESQUEMATARDIO;
			}
			ejecutarConsulta();
			cargarTabla();
		}
	}
	
	private void cargarTabla() {
		tblReporte.setDefaultRenderer(Object.class, new FormatoTabla());
		String[] columnCabecera = { "", "No", "Apellidos y Nombres", "Cédula", "Edad años", "Edad meses", "Edad días", "Sexo", "Pertenece al establecimiento de salud",
				"Provincia", "Cantón", "Parroquía", "Nacionalidad", "Autoidentificación Etnica", "Lugar de Atención", "Fecha de Atención"};
		String[] columnVacunas;
		int i = 0;
		String[] columnNames = null;
		if(!esquema.isEmpty()){
			columnVacunas = new String[esquema.size()];
			for (Esquemavacunacion ev : esquema) {
				columnVacunas[i] = ev.getVacunaId().getNombrevacuna() +" -Dosis "+ev.getDosis();
				i++;
			}
			int total = columnCabecera.length + columnVacunas.length;
			columnNames = new String[total];
			for (int j = 0; j < columnCabecera.length; j++) {
				columnNames[j] = columnCabecera[j];
			}
			for (int j = 0; j < columnVacunas.length; j++) {
				columnNames[j+ columnCabecera.length ] = columnVacunas[j];
			}
		}else{
			columnNames = columnCabecera;
		}
		
		
		modeloTabla = new CaptacionVacunasModel(columnNames);
		controles = controles == null ? new ArrayList<>() : controles;
		modeloTabla.addFila(controles);
		tblReporte.setModel(modeloTabla);
		tblReporte.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		modeloTabla.ocultaColumnaCodigo(tblReporte);
		modeloTabla.definirAnchoColumnas(tblReporte,definirTamano());
	}
	
	private void exportarXLS() {
		String autor = VariableSesion.getInstanciaVariablesSession().getUsuario().getPersonaId().getPrimernombre()
				+ " ";
		autor += VariableSesion.getInstanciaVariablesSession().getUsuario().getPersonaId().getPrimerapellido() + " ";
                StringBuilder fechas = new StringBuilder();
                fechas.append("DESDE: ").append(dttDesde.getDateStringOrEmptyString()).append(" HASTA: ").append(dttHasta.getDateStringOrEmptyString());
                String tituloReporte = "Reporte de Captación de Vacunas - Esquema ";
                if(tipoEsquema == ESQUEMATEMPRANO){
                    tituloReporte = tituloReporte + "Captación Temprana";
                }
		if(tipoEsquema == ESQUEMATARDIO){
                    tituloReporte = tituloReporte + "Captación Tardía";;
		}
		ReportesUtil.exportarXlsReportes(tblReporte, "CaptacionVacunas", autor, "xls",tituloReporte,fechas.toString());
	}
	
	private void ejecutarConsulta() {
		CaptacionTardiaReporte captacionTardiaReporte;
		controles = new ArrayList<>();
		List<Atencionmedica> atenciones = atencionmedicaService.findAllAtencionesByDates(
				VariableSesion.getInstanciaVariablesSession().getProfesional().getId(),
				Date.from(dttDesde.getDate().atStartOfDay(ZoneId.systemDefault()).toInstant()),
				Date.from(dttHasta.getDate().atStartOfDay(ZoneId.systemDefault()).toInstant()),
				VariableSesion.getInstanciaVariablesSession().getCtEspecialidad().getId());
		esquema = esquemavacunacionService.findAllEsquemaVacunacionByTipo(tipoEsquema);
		int i = 0;
		if (!atenciones.isEmpty()) {
			controles = new ArrayList<>();
			for (Atencionmedica atencion : atenciones) {
				captacionTardiaReporte = new CaptacionTardiaReporte();
				List<Registrovacunacion> registros = registrovacunacionService.findAllRegistroVacunaByAtencionMedicaAndTipo(atencion.getId(), tipoEsquema);
				if(registros != null && !registros.isEmpty()){
					captacionTardiaReporte.setAtencionMedica(atencion);
					String co = null;
					try {
						co = SeguridadUtil.descifrarString(atencion.getKsCident(),
								VariableSesion.getInstanciaVariablesSession().getUtilitario(),
								VariableSesion.getInstanciaVariablesSession().getUsuario().getLogin());
						captacionTardiaReporte.setPersona(personaService.findOneByPersonaUUID(co));
					} catch (GeneralSecurityException e) {
						logger.error("Error GeneralSecurityException {} ", e.getMessage());
					} catch (IOException e) {
						logger.error("Error IOException {} ", e.getMessage());
					}
					int[][] vacunas = new int[atenciones.size()][esquema.size()];
					
					for (Registrovacunacion registrovacunacion : registros) {
						int j = 0;
						for (Esquemavacunacion esquemavacunacion : esquema) {
							if(registrovacunacion.getEsquemavacunacionId().getId().equals(esquemavacunacion.getId())){
								vacunas[i][j] = 1;
								break;
							}
							j++;
						}
					}
					Intraextramural iv = intraextramuralService.findIntraextramuralByAtencionId(atencion.getId());
					if (iv != null) {
						captacionTardiaReporte.setIntraextramural(iv);
					}
					captacionTardiaReporte.setFechaColocacionVacuna(atencion.getFechaatencion());
					captacionTardiaReporte.setVacunaIPV(vacunas[i][0]);
					captacionTardiaReporte.setVacunaFiebreAmarilla(vacunas[i][1]);
					captacionTardiaReporte.setVacunaSRP(vacunas[i][2]);
					captacionTardiaReporte.setVacunaDPT(vacunas[i][3]);
					captacionTardiaReporte.setVacunaDTPediatrica(vacunas[i][4]);
					captacionTardiaReporte.setVacunadTAdulto(vacunas[i][5]);
					captacionTardiaReporte.setVacunaSR(vacunas[i][6]);
					captacionTardiaReporte.setVacunaHBPediatrica(vacunas[i][7]);
					captacionTardiaReporte.setVacunaHBAdulto(vacunas[i][8]);
					captacionTardiaReporte.setVacunafIPV(vacunas[i][9]);
					captacionTardiaReporte.setVacunaSRPDos(vacunas[i][10]);
			//captacionTardiaReporte.setVacunaOPV(vacunas[i][11]);
					captacionTardiaReporte.setVacunaDPTDos(vacunas[i][11]);
					captacionTardiaReporte.setVacunaDTPediatricaDos(vacunas[i][12]);
					captacionTardiaReporte.setVacunadTAdultoDos(vacunas[i][13]);
					captacionTardiaReporte.setVacunaSRDos(vacunas[i][14]);
					captacionTardiaReporte.setVacunaHBPediatricaDos(vacunas[i][15]);
					captacionTardiaReporte.setVacunaHBAdultoDos(vacunas[i][16]);
					captacionTardiaReporte.setVacunafIPVDos(vacunas[i][17]);
					captacionTardiaReporte.setVacunabOPV(vacunas[i][18]);
			//captacionTardiaReporte.setVacunaOPVDos(vacunas[i][20]);
					captacionTardiaReporte.setVacunaDPTTres(vacunas[i][19]);
					captacionTardiaReporte.setVacunaDTPediatricaTres(vacunas[i][20]);
					captacionTardiaReporte.setVacunadTAdultoTres(vacunas[i][21]);
					captacionTardiaReporte.setVacunaHBPediatricaTres(vacunas[i][22]);
					captacionTardiaReporte.setVacunaHBAdultoTres(vacunas[i][23]);
					//captacionTardiaReporte.setVacunabOPVDos(vacunas[i][24]);
			//captacionTardiaReporte.setVacunaOPVTres(vacunas[i][27]);
					captacionTardiaReporte.setVacunaDPTCuatro(vacunas[i][24]);
					captacionTardiaReporte.setVacunaDTPediatricaCuatro(vacunas[i][25]);
					if(tipoEsquema == ESQUEMATARDIO){
						captacionTardiaReporte.setVacunadTAdultoCuatro(vacunas[i][26]);
						captacionTardiaReporte.setVacunabOPVTres(vacunas[i][27]);
						captacionTardiaReporte.setVacunaDTPediatricaCinco(vacunas[i][28]);
						captacionTardiaReporte.setVacunadTAdultoCinco(vacunas[i][29]);
			/*captacionTardiaReporte.setVacunadTAdultoSeis(vacunas[i][34]);*/
					}
					controles.add(captacionTardiaReporte);
				}
				i++;
			}
		}
	}
	private int[] definirTamano(){
		int[] medida = new int[49];
		medida[0] = 30;
		medida[1] = 300;
		medida[2] = 150;
		medida[3] = 150;
		medida[4] = 150;
		medida[5] = 150;
		medida[6] = 150;
		medida[7] = 150;
		medida[8] = 150;
		medida[9] = 150;
		medida[10] = 150;
		for (int i = 11; i < medida.length; i++) {
			medida[i] = 100;
		}
		return medida;
	}
	private void cerrar(){
		
		dttDesde.setText("");
		dttHasta.setText("");
		if(modeloTabla != null){
			modeloTabla.clear();
		}
		
	}
}
