/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.amed.model;

import ec.gob.msp.rdacaa.business.entity.Entidad;
import javax.swing.DefaultListCellRenderer;
import java.awt.Component;
import javax.swing.JList;

/**
 *
 * @author eduardo
 */
public class CmbEntidadCellRenderer extends DefaultListCellRenderer{
    
    private static final long serialVersionUID = -5079108834113168615L;
    
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
			boolean cellHasFocus) {
		if (value instanceof Entidad) {
			Entidad entidad = (Entidad) value;
			value = entidad.getId() + " -- " + entidad.getNombreoficial();
		}
		return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
	}
}
