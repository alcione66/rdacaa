/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.amed.controller;


import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.swing.JButton;
import javax.swing.JComboBox;

import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import ec.gob.msp.rdacaa.admision.model.CantonToStringConverter;
import ec.gob.msp.rdacaa.admision.model.CmbCantonCellRenderer;
import ec.gob.msp.rdacaa.admision.model.CmbDetalleCatalogoCellRenderer;
import ec.gob.msp.rdacaa.admision.model.CmbParroquiaCellRenderer;
import ec.gob.msp.rdacaa.admision.model.CmbProvinciaCellRenderer;
import ec.gob.msp.rdacaa.admision.model.DetalleCatalogoToStringConverter;
import ec.gob.msp.rdacaa.admision.model.ParroquiaToStringConverter;
import ec.gob.msp.rdacaa.admision.model.ProvinciaToStringConverter;
import ec.gob.msp.rdacaa.amed.forma.Referencia;
import ec.gob.msp.rdacaa.amed.model.AmedCmbCantonModel;
import ec.gob.msp.rdacaa.amed.model.AmedCmbEntidadModel;
import ec.gob.msp.rdacaa.amed.model.AmedCmbParroquiaModel;
import ec.gob.msp.rdacaa.amed.model.AmedCmbProvinciaModel;
import ec.gob.msp.rdacaa.amed.model.AmedCmbSubsistemaModel;
import ec.gob.msp.rdacaa.amed.model.AmedCmbTipoInterconsultaModel;
import ec.gob.msp.rdacaa.amed.model.CmbEntidadCellRenderer;
import ec.gob.msp.rdacaa.amed.model.EntidadToStringConverter;
import ec.gob.msp.rdacaa.business.entity.Canton;
import ec.gob.msp.rdacaa.business.entity.Detallecatalogo;
import ec.gob.msp.rdacaa.business.entity.Entidad;
import ec.gob.msp.rdacaa.business.entity.Interconsulta;
import ec.gob.msp.rdacaa.business.entity.Parroquia;
import ec.gob.msp.rdacaa.business.entity.Provincia;
import ec.gob.msp.rdacaa.business.entity.Referenciaatencion;
import ec.gob.msp.rdacaa.business.service.CantonService;
import ec.gob.msp.rdacaa.business.service.DetallecatalogoService;
import ec.gob.msp.rdacaa.business.service.EntidadService;
import ec.gob.msp.rdacaa.business.service.ParroquiaService;
import ec.gob.msp.rdacaa.business.service.ProvinciaService;
import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.utilitario.forma.UtilitarioForma;

/**
 *
 * @author eduardo
 */
@Controller
public class ReferenciaController {

	private final Referencia referenciaFrame;
    private final DetallecatalogoService detallecatalogoService;
    private final AmedCmbSubsistemaModel amedCmbSubsistemaModel;
    private final AmedCmbTipoInterconsultaModel amedCmbTipoInterconsultaModel;
    private final AmedCmbEntidadModel amedCmbEntidadModel;
    private JComboBox<Detallecatalogo> cbmSubsistema;
    private JComboBox<Detallecatalogo> cmbTipoInterconsulta;
    private JComboBox<Entidad> cmbLugarReferido;
    private final EntidadService entidadService;
    private static final String SUBSISTEMA = "No Aplica";
    private Detallecatalogo detallecatalogoNull;
    private Entidad entidadNull;
    private JButton btnNuevo;
    private final VariableSesion variableSesion = VariableSesion.getInstanciaVariablesSession();
    private final JComboBox<Provincia> cmbProvincia;
    private final JComboBox<Canton> cmbCanton;
    private final JComboBox<Parroquia> cmbParroquia;
    private final ProvinciaService provinciaService;
    private final AmedCmbProvinciaModel amedCmbProvinciaModel;
    private final CantonService cantonService;
    private final AmedCmbCantonModel amedCmbCantonModel;
    private final ParroquiaService parroquiaService;
    private final AmedCmbParroquiaModel amedCmbParroquiaModel;
    private Provincia provinciaNull;
    private Canton cantonNull;
    private Parroquia parroquiaNull;
    private final Object[] controlesReferencia = new Object[2];
    private final Object[] controlesInterconsulta = new Object[1];


    @Autowired
    public ReferenciaController(
        Referencia referenciaFrame,
        DetallecatalogoService detallecatalogoService,
        EntidadService entidadService,
        AmedCmbEntidadModel amedCmbEntidadModel,
        AmedCmbProvinciaModel amedCmbProvinciaModel, ProvinciaService provinciaService,
        AmedCmbCantonModel amedCmbCantonModel, CantonService cantonService,
        AmedCmbParroquiaModel amedCmbParroquiaModel, ParroquiaService parroquiaService
    ) {
        this.referenciaFrame = referenciaFrame;
        this.detallecatalogoService = detallecatalogoService;
        this.entidadService = entidadService;
        this.amedCmbSubsistemaModel = new AmedCmbSubsistemaModel();
        this.amedCmbTipoInterconsultaModel = new AmedCmbTipoInterconsultaModel();
        this.amedCmbEntidadModel = amedCmbEntidadModel;
        this.provinciaService = provinciaService;
        this.amedCmbProvinciaModel = amedCmbProvinciaModel;
        this.cantonService = cantonService;
        this.amedCmbCantonModel = amedCmbCantonModel;
        this.parroquiaService = parroquiaService;
        this.amedCmbParroquiaModel = amedCmbParroquiaModel;
        this.cmbProvincia = referenciaFrame.getCmbProvincia();
        this.cmbCanton = referenciaFrame.getCmbCanton();
        this.cmbParroquia = referenciaFrame.getCmbParroquia();
    }
    
    @PostConstruct
    private void init() {
        loadDefaultCombos();
        this.cbmSubsistema = referenciaFrame.getCbmSubsistema();
        this.cmbTipoInterconsulta = referenciaFrame.getCmbTipoInterconsulta();
        this.cmbLugarReferido = referenciaFrame.getCmbLugarReferido();
        AutoCompleteDecorator.decorate(cbmSubsistema, new DetalleCatalogoToStringConverter());
        AutoCompleteDecorator.decorate(cmbTipoInterconsulta, new DetalleCatalogoToStringConverter());
        AutoCompleteDecorator.decorate(cmbLugarReferido, new EntidadToStringConverter());
        AutoCompleteDecorator.decorate(cmbProvincia, new ProvinciaToStringConverter());
        AutoCompleteDecorator.decorate(cmbCanton, new CantonToStringConverter());
        AutoCompleteDecorator.decorate(cmbParroquia, new ParroquiaToStringConverter());
        
        cbmSubsistema.addActionListener(e -> validateSubsistema());
        cmbProvincia.addActionListener(e -> loadCantones());
        cmbCanton.addActionListener(e -> loadParroquias());
        cmbParroquia.addActionListener(e -> loadEntidad());
        this.btnNuevo = referenciaFrame.getBtnNuevo();
        loadSubsistema();
        loadInterconsulta();
        loadProvincias();
        cmbCanton.setRenderer(new CmbCantonCellRenderer());
        cmbParroquia.setRenderer(new CmbParroquiaCellRenderer());  
        btnNuevo.addMouseListener (new MouseListener() {

			@Override
			public void mouseClicked(MouseEvent e) {
				cleanInputInterconsulta();
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// No se usa
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// No se usa
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// No se usa
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// No se usa
			}
        });
    }
    
    private void loadProvincias() {
        List<Provincia> provincias = provinciaService.findAllOrderedByDescripcion();
        amedCmbProvinciaModel.clear();
        amedCmbProvinciaModel.addElements(provincias);
        cmbProvincia.setRenderer(new CmbProvinciaCellRenderer());
        cmbProvincia.setModel(amedCmbProvinciaModel);
        cmbProvincia.setSelectedItem(provinciaNull);
    }

    private void loadCantones() {
        if (amedCmbProvinciaModel.getSelectedItem() != null) {
            List<Canton> cantones = cantonService.findAllByProvinciaId(amedCmbProvinciaModel.getSelectedItem().getId());
            amedCmbCantonModel.clear();
            amedCmbCantonModel.addElements(cantones);
            cmbCanton.setModel(amedCmbCantonModel);
            cmbCanton.setSelectedItem(cantonNull);
        } else {
            amedCmbCantonModel.clear();
        }
    }

    private void loadParroquias() {
        if (amedCmbProvinciaModel.getSelectedItem() != null && amedCmbCantonModel.getSelectedItem() != null) {
            List<Parroquia> parroquias = parroquiaService.findAllByProvinciaId(amedCmbCantonModel.getSelectedItem().getId());
            amedCmbParroquiaModel.clear();
            amedCmbParroquiaModel.addElements(parroquias);
            cmbParroquia.setModel(amedCmbParroquiaModel);
            cmbParroquia.setSelectedItem(parroquiaNull);
        } else {
            amedCmbParroquiaModel.clear();
        }

    }
    
    private void loadSubsistema() {
        List<Detallecatalogo> listaSubsistema = detallecatalogoService.findAllSubsistema();
        amedCmbSubsistemaModel.clear();
        amedCmbSubsistemaModel.addElements(listaSubsistema);
        cbmSubsistema.setRenderer(new CmbDetalleCatalogoCellRenderer());
        cbmSubsistema.setModel(amedCmbSubsistemaModel);
        cbmSubsistema.setSelectedItem(detallecatalogoNull);
    
    }
    
    private void loadInterconsulta() {
        List<Detallecatalogo> listaInterconsulta = detallecatalogoService.findAllTipoInterconsulta(); 
        amedCmbTipoInterconsultaModel.clear();
        amedCmbTipoInterconsultaModel.addElements(listaInterconsulta);
        cmbTipoInterconsulta.setRenderer(new CmbDetalleCatalogoCellRenderer());
        cmbTipoInterconsulta.setModel(amedCmbTipoInterconsultaModel);
        cmbTipoInterconsulta.setSelectedItem(detallecatalogoNull);
    }
    
    private void loadEntidad() {
        if (amedCmbSubsistemaModel.getSelectedItem() != null && !amedCmbSubsistemaModel.getSelectedItem().getDescripcion().equals(SUBSISTEMA)){
            if(amedCmbParroquiaModel.getSelectedItem() != null){
                List<Entidad> listaEntidades = entidadService.findAllByParroquiaId(amedCmbParroquiaModel.getSelectedItem().getId());
                amedCmbEntidadModel.clear();
                amedCmbEntidadModel.addElements(listaEntidades);
                cmbLugarReferido.setRenderer(new CmbEntidadCellRenderer());
                cmbLugarReferido.setModel(amedCmbEntidadModel);
                cmbLugarReferido.setSelectedItem(entidadNull);
                UtilitarioForma.habilitarControles(true, cmbLugarReferido, cmbProvincia, cmbCanton, cmbParroquia);
            }      
        } 
        validateSubsistema();
    }
    
    private void validateSubsistema(){
        if (amedCmbSubsistemaModel.getSelectedItem() != null && amedCmbSubsistemaModel.getSelectedItem().getDescripcion().equals(SUBSISTEMA)){
            UtilitarioForma.habilitarControles(false, cmbLugarReferido, cmbProvincia, cmbCanton, cmbParroquia);
            cleanNoAplica();
        }else{
            UtilitarioForma.habilitarControles(true, cmbLugarReferido, cmbProvincia, cmbCanton, cmbParroquia);
        }
    }
    
    public boolean validateForm() {
        boolean validarFormaRef = false;
        boolean validarFormaInt = false;
        if ( amedCmbSubsistemaModel.getSelectedItem() != null && 
            !amedCmbSubsistemaModel.getSelectedItem().getDescripcion().equals(SUBSISTEMA) ) {
            controlesReferencia[0] = cbmSubsistema;
            controlesReferencia[1] = cmbLugarReferido;
            validarFormaRef = UtilitarioForma.validarCamposRequeridos(controlesReferencia);
        } else if( amedCmbSubsistemaModel.getSelectedItem() != null && 
                   amedCmbSubsistemaModel.getSelectedItem().getDescripcion().equals(SUBSISTEMA) ){
            controlesReferencia[0] = cbmSubsistema;
            controlesReferencia[1] = new Object();
            validarFormaRef = UtilitarioForma.validarCamposRequeridos(controlesReferencia);
        }
        if(!cmbTipoInterconsulta.getSelectedObjects()[0].equals(detallecatalogoNull)) {
            controlesInterconsulta[0] = cmbTipoInterconsulta;
            validarFormaInt = UtilitarioForma.validarCamposRequeridos(controlesInterconsulta);
        }
  
        if (validarFormaRef) {
            getReferenciaatencion();
        }
        if (validarFormaInt) {
            getInterConsulta();
        }
        
        return validarFormaRef;
    }
    
    public void getReferenciaatencion() {
        Referenciaatencion referenciaatencion = new Referenciaatencion();
        referenciaatencion.setCtsubsistemaId(amedCmbSubsistemaModel.getSelectedItem());
        if(cmbTipoInterconsulta.getSelectedItem() != null && !cmbTipoInterconsulta.getSelectedObjects()[0].equals(detallecatalogoNull)) {
            referenciaatencion.setCttipoId(amedCmbTipoInterconsultaModel.getSelectedItem());
        }
        if(cmbLugarReferido.getSelectedItem() != null && !cmbLugarReferido.getSelectedObjects()[0].equals(entidadNull)) {
            referenciaatencion.setEntidadId(amedCmbEntidadModel.getSelectedItem());
        }
        referenciaatencion.setDescripcion("");
        
        variableSesion.setReferencia(referenciaatencion);
    }
    
    public void getInterConsulta() {
        Interconsulta interconsulta = new Interconsulta();
        interconsulta.setCttipoId(amedCmbTipoInterconsultaModel.getSelectedItem());
        interconsulta.setDescripcion("");
        
        variableSesion.setInterconsulta(interconsulta);
    }
    
    public void loadDefaultCombos() {
        detallecatalogoNull = new Detallecatalogo(){
            private static final long serialVersionUID = 1L;
            boolean isNoSelectable = true;
        };
        detallecatalogoNull.setId(0);
        detallecatalogoNull.setDescripcion("Seleccione");
        entidadNull = new Entidad(){
            private static final long serialVersionUID = 1L;
            boolean isNoSelectable = true;
        };
        entidadNull.setId(0);
        entidadNull.setNombreoficial("Seleccione");
        
        provinciaNull = new Provincia(){
            private static final long serialVersionUID = 1L;
            boolean isNoSelectable = true;
        };
        provinciaNull.setId(0);
        provinciaNull.setDescripcion("Seleccione");
        
        cantonNull = new Canton(){
            private static final long serialVersionUID = 1L;
            boolean isNoSelectable = true;
        };
        cantonNull.setId(0);
        cantonNull.setDescripcion("Seleccione");
        
        parroquiaNull = new Parroquia(){
            private static final long serialVersionUID = 1L;
            boolean isNoSelectable = true;
        };
        parroquiaNull.setId(0);
        parroquiaNull.setDescripcion("Seleccione");
    }

    public void cleanInput() {
        cbmSubsistema.setSelectedItem(detallecatalogoNull);
        cmbTipoInterconsulta.setSelectedItem(detallecatalogoNull);
        cmbLugarReferido.setSelectedItem(entidadNull);
        cmbProvincia.setSelectedItem(provinciaNull);
        cmbCanton.setSelectedItem(cantonNull);
        cmbParroquia.setSelectedItem(parroquiaNull);
    }
    
    public void cleanInputInterconsulta() {
        
        cmbTipoInterconsulta.setSelectedItem(detallecatalogoNull);
    }
    
    private void cleanNoAplica() {
        cmbLugarReferido.setSelectedItem(entidadNull);
        cmbProvincia.setSelectedItem(provinciaNull);
        cmbCanton.setSelectedItem(cantonNull);
        cmbParroquia.setSelectedItem(parroquiaNull);
    }
}
