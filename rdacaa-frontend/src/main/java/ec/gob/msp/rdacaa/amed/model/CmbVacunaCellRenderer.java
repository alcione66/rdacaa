/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.amed.model;

import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

import ec.gob.msp.rdacaa.business.entity.Vacuna;
import ec.gob.msp.rdacaa.business.service.VacunaXEsquema;

/**
 *
 * @author eduardo
 */
public class CmbVacunaCellRenderer extends DefaultListCellRenderer{
    
    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        if (value instanceof VacunaXEsquema) {
        	VacunaXEsquema vac = (VacunaXEsquema) value;
            value = vac.getNombreVacunaXNombreEsquema();
        }
        
        return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
    }
}
