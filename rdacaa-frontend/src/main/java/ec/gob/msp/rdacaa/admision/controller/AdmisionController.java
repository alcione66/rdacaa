/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.admision.controller;

import java.awt.EventQueue;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.ItemEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JRadioButton;
import javax.swing.JTextField;

import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.github.lgooddatepicker.components.DatePicker;
import com.github.lgooddatepicker.components.DatePickerSettings;
import com.github.lgooddatepicker.optionalusertools.DateVetoPolicy;

import ec.gob.msp.rdacaa.admision.forma.Admision;
import ec.gob.msp.rdacaa.admision.forma.CambioDPA;
import ec.gob.msp.rdacaa.admision.model.AdmisionCmbCantonModel;
import ec.gob.msp.rdacaa.admision.model.AdmisionCmbNacionalidadModel;
import ec.gob.msp.rdacaa.admision.model.AdmisionCmbParroquiaModel;
import ec.gob.msp.rdacaa.admision.model.AdmisionCmbProvinciaModel;
import ec.gob.msp.rdacaa.admision.model.AdmisionDetalleCatalogoModel;
import ec.gob.msp.rdacaa.admision.model.CantonToStringConverter;
import ec.gob.msp.rdacaa.admision.model.CmbCantonCellRenderer;
import ec.gob.msp.rdacaa.admision.model.CmbDetalleCatalogoCellRenderer;
import ec.gob.msp.rdacaa.admision.model.CmbNacionalidadCellRenderer;
import ec.gob.msp.rdacaa.admision.model.CmbParroquiaCellRenderer;
import ec.gob.msp.rdacaa.admision.model.CmbProvinciaCellRenderer;
import ec.gob.msp.rdacaa.admision.model.DetalleCatalogoToStringConverter;
import ec.gob.msp.rdacaa.admision.model.PaisToStringConverter;
import ec.gob.msp.rdacaa.admision.model.ParroquiaToStringConverter;
import ec.gob.msp.rdacaa.admision.model.ProvinciaToStringConverter;
import ec.gob.msp.rdacaa.business.entity.Atencionmedica;
import ec.gob.msp.rdacaa.business.entity.Canton;
import ec.gob.msp.rdacaa.business.entity.Detallecatalogo;
import ec.gob.msp.rdacaa.business.entity.Pais;
import ec.gob.msp.rdacaa.business.entity.Parroquia;
import ec.gob.msp.rdacaa.business.entity.Persona;
import ec.gob.msp.rdacaa.business.entity.Provincia;
import ec.gob.msp.rdacaa.business.service.CantonService;
import ec.gob.msp.rdacaa.business.service.CierreatencionesService;
import ec.gob.msp.rdacaa.business.service.ConstantesDetalleCatalogo;
import ec.gob.msp.rdacaa.business.service.DetallecatalogoService;
import ec.gob.msp.rdacaa.business.service.PaisService;
import ec.gob.msp.rdacaa.business.service.ParroquiaService;
import ec.gob.msp.rdacaa.business.service.PersonaService;
import ec.gob.msp.rdacaa.business.service.ProvinciaService;
import ec.gob.msp.rdacaa.business.service.TipoestablecimientoService;
import ec.gob.msp.rdacaa.business.validation.ArchivoValidator;
import ec.gob.msp.rdacaa.business.validation.CedulaValidator;
import ec.gob.msp.rdacaa.business.validation.IdentificacionExtrangeraValidator;
import ec.gob.msp.rdacaa.business.validation.IdentificacionRepValidator;
import ec.gob.msp.rdacaa.business.validation.PrimerNombreValidator;
import ec.gob.msp.rdacaa.business.validation.TelefonoValidator;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.control.swing.notifications.Notifications;
import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.utilitario.enumeracion.ComunEnum;
import ec.gob.msp.rdacaa.utilitario.fecha.FechasUtil;
import ec.gob.msp.rdacaa.utilitario.forma.UtilitarioForma;
import ec.gob.msp.rdacaa.utilitario.secuencial.UtilitarioSequencial;

/**
 *
 * @author msp
 */
@Controller
public class AdmisionController {

    private static final String KICHWA_SELECCION = "Kichwa";
    private static final String INDIGENA_SELECCION = "Indígena";
    private static final String CEDULA_VALIDACION = "Cédula de Identidad";
    private static final String NO_IDENTIFICADO = "No Identificado";
    private static final String CAMPO_SEGUROINDIRECTO = "Seguro Indirecto";
    private static final String PACIENTE = "Paciente";
    private static final String ANIOSMENOR = "5";
    private static final int ANIOSBEBE = 1;
    private CedulaValidator cedulaValidator;
    private IdentificacionRepValidator identificacionRepValidator;
    private IdentificacionExtrangeraValidator identExtrValidator;
    private PrimerNombreValidator nombreValidator;
    private ArchivoValidator archivoValidator; 
    private TelefonoValidator telefonoValidator;
    private static final int HOMBRE = 16;
    private static final int MUJER = 17;
    private static final int TRANSMASCULINO = 677;
    private static final int TRANSFEMENINO = 678;
    private static final int EDADINTERSEXUAL = 10;
    private static final int EDADSEGURO = 10;
    private static final String PAIS = "ECUATORIANO/A";
    private static final String COMBONULO = "Seleccione";
    private static final int VALIDAR_NUMERO_CELULAR = 0;
    private static final int VALIDAR_NUMERO_CONVENCIONAL = 1;
    private static final int TIPOENTIDAD = 65;
    
    private Admision admisionFrame;
    private CambioDPA cambioDPAFrame;

    private DetallecatalogoService detallecatalogoService;
    private PaisService paisService;
    private AdmisionCmbNacionalidadModel admisionCmbNacionalidadModel;
    private AdmisionDetalleCatalogoModel admisionCmbTipoIdentificacionModel;
    private AdmisionDetalleCatalogoModel admisionCmbTipoIdentificacionReModel;
    private AdmisionDetalleCatalogoModel admisionCmbSexoModel;
    private AdmisionDetalleCatalogoModel admisionCmbOrientacionSexualModel;
    private AdmisionDetalleCatalogoModel admisionCmbTipoSeguroModel;
    private AdmisionDetalleCatalogoModel admisionCmbIdenEtnicaModel;
    private AdmisionDetalleCatalogoModel admisionCmbNacEtnicaModel;
    private AdmisionDetalleCatalogoModel admisionCmbPueblosModel;
    private AdmisionDetalleCatalogoModel admisionCmbTipoBonoModel;
    private AdmisionDetalleCatalogoModel admisionCmbIdentGenero;
    private ProvinciaService provinciaService;
    private AdmisionCmbProvinciaModel admisionCmbProvinciaModel;
    private CantonService cantonService;
    private AdmisionCmbCantonModel admisionCmbCantonModel;
    private ParroquiaService parroquiaService;
    private AdmisionCmbParroquiaModel admisionCmbParroquiaModel;
    private PersonaService personaService; 
    private CierreatencionesService cierreatencionesService;
    private TipoestablecimientoService tipoestablecimientoService; 

    private JComboBox<Pais> cmbNacionalidad;
    private JComboBox<Detallecatalogo> cmbTipoIdentificacion;
    private JComboBox<Detallecatalogo> cmbTipoIdentificacionRepresentante;
    private JComboBox<Detallecatalogo> cmbSexo;
    private JComboBox<Detallecatalogo> cmbOrientacionSexual;
    private JComboBox<Detallecatalogo> cmbTipoSeguro;
    private JComboBox<Detallecatalogo> cmbIdenEtnica;
    private JComboBox<Detallecatalogo> cmbNacEtnica;
    private JComboBox<Detallecatalogo> cmbPueblos;
    private JComboBox<Detallecatalogo> cmbTipoBono;
    private JComboBox<Provincia> cmbProvincia;
    private JComboBox<Canton> cmbCanton;
    private JComboBox<Parroquia> cmbParroquia;
    private JComboBox<Detallecatalogo> cmbIdentGenero;
    private Detallecatalogo detallecatalogoNull;
    private Pais paisNull;
    private Provincia provinciaNull;
    private Canton cantonNull;
    private Parroquia parroquiaNull;
    

    private DatePicker dttFechaNacimiento;
    private DatePicker dttFechaAtencion;

    private JTextField txtIdentificacion;
    private JTextField txtIdenRepresentante;
    private JTextField txtPrimerNombre;
    private JTextField txtSegundoNombre;
    private JTextField txtPrimerApellido;
    private JTextField txtSegundoApellido;
    private JTextField txtTelefonoUno;
    private JTextField txtDireccion;
    private JTextField txtTelefonoDos;
    private JTextField txtArchivo;
    
    private Object[] controlesValidar = new Object[22];
    
    private JRadioButton rbConvencionalPaciente;
    private JRadioButton rbCelularPaciente;
    
    private JRadioButton rbConvencionalRepresentante;
    private JRadioButton rbCelularRepresentante;
    
    private JLabel lblEdad;
    private List<ValidationResult> listaErrores;
    private JButton btnCambioDpa;
    private CambioDPAController cambioDPAController;

    @Autowired
    public AdmisionController(Admision admisionFrame, AdmisionCmbNacionalidadModel admisionCmbNacionalidadModel,
            PaisService paisService, DetallecatalogoService detallecatalogoService,
            AdmisionCmbProvinciaModel admisionCmbProvinciaModel, ProvinciaService provinciaService,
            AdmisionCmbCantonModel admisionCmbCantonModel, CantonService cantonService,
            AdmisionCmbParroquiaModel admisionCmbParroquiaModel, ParroquiaService parroquiaService,
            PersonaService personaService, CierreatencionesService cierreatencionesService,
            CambioDPA cambioDPAFrame,
            CambioDPAController cambioDPAController,
            TipoestablecimientoService tipoestablecimientoService
    ) {
        this.admisionFrame = admisionFrame;
        this.admisionCmbNacionalidadModel = admisionCmbNacionalidadModel;
        this.paisService = paisService;
        this.detallecatalogoService = detallecatalogoService;
        this.admisionCmbTipoIdentificacionModel = new AdmisionDetalleCatalogoModel();
        this.admisionCmbTipoIdentificacionReModel = new AdmisionDetalleCatalogoModel();
        this.admisionCmbSexoModel = new AdmisionDetalleCatalogoModel();
        this.admisionCmbOrientacionSexualModel = new AdmisionDetalleCatalogoModel();
        this.admisionCmbTipoSeguroModel = new AdmisionDetalleCatalogoModel();
        this.admisionCmbIdenEtnicaModel = new AdmisionDetalleCatalogoModel();
        this.admisionCmbNacEtnicaModel = new AdmisionDetalleCatalogoModel();
        this.admisionCmbPueblosModel = new AdmisionDetalleCatalogoModel();
        this.admisionCmbTipoBonoModel = new AdmisionDetalleCatalogoModel();
        this.admisionCmbIdentGenero = new AdmisionDetalleCatalogoModel();
        this.provinciaService = provinciaService;
        this.admisionCmbProvinciaModel = admisionCmbProvinciaModel;
        this.cantonService = cantonService;
        this.admisionCmbCantonModel = admisionCmbCantonModel;
        this.parroquiaService = parroquiaService;
        this.admisionCmbParroquiaModel = admisionCmbParroquiaModel;

        this.cedulaValidator = new CedulaValidator();
        this.identificacionRepValidator = new IdentificacionRepValidator();
        this.identExtrValidator = new IdentificacionExtrangeraValidator();
        this.nombreValidator = new PrimerNombreValidator();
        this.archivoValidator = new ArchivoValidator();
        this.telefonoValidator = new TelefonoValidator();
        this.personaService = personaService;
        this.cierreatencionesService = cierreatencionesService;
        this.cambioDPAFrame = cambioDPAFrame;
        this.cambioDPAController = cambioDPAController;
        this.tipoestablecimientoService = tipoestablecimientoService;
    }

    @PostConstruct
    private void init() {
        this.cmbNacionalidad = admisionFrame.getCmbNacionalidad();
        this.cmbTipoIdentificacion = admisionFrame.getCmbTipoIdentificacion();
        this.cmbTipoIdentificacionRepresentante = admisionFrame.getCmbTipoIdentRepresentante();
        this.cmbSexo = admisionFrame.getCmbSexo();
        this.cmbOrientacionSexual = admisionFrame.getCmbOrientacionSexual();
        this.cmbTipoSeguro = admisionFrame.getCmbTipoSeguro();
        this.cmbIdenEtnica = admisionFrame.getCmbIdenEtnica();
        this.cmbNacEtnica = admisionFrame.getCmbNacEtnica();
        this.cmbPueblos = admisionFrame.getCmbPueblos();
        this.cmbProvincia = admisionFrame.getCmbProvincia();
        this.cmbCanton = admisionFrame.getCmbCanton();
        this.cmbParroquia = admisionFrame.getCmbParroquia();
        this.cmbTipoBono = admisionFrame.getCmbTipoBono();
        this.cmbIdentGenero = admisionFrame.getCmbIdentGenero(); 

        addAutocompleteToCombos();
        loadDefaultCombos();
        loadControles();

        cmbIdenEtnica.addActionListener(e -> loadCatalogoNacionalidadEtnica());
        cmbNacEtnica.addActionListener(e -> loadCatalogoPueblos());
        cmbProvincia.addActionListener(e -> loadCantones());
        cmbCanton.addActionListener(e -> loadParroquias());
 
        loadPaises();
        loadProvincias();
        loadCatalogoAutoIdentificacionEtnica();
        loadCatalogosIndividuales();
        cmbNacionalidad.addActionListener(e -> validateNacionalidad());
        this.dttFechaAtencion = admisionFrame.getDttFechaAtencion();
        dttFechaAtencion.addDateChangeListener(dt -> enabledRepresentante());
        this.dttFechaNacimiento = admisionFrame.getDttFechaNacimiento();
        dttFechaNacimiento.addDateChangeListener(dt -> enabledRepresentante());
        cmbTipoIdentificacion.addActionListener(dt -> validateTipoIdentificacion());

        this.txtIdentificacion = admisionFrame.getTxtIdentificacion();
        txtIdentificacion.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
            	//No es usado
            }

            @Override
            public void focusLost(FocusEvent e) {
                
                EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    validateIdentificacion();  
                    /*Actualiza el panel.*/
                    admisionFrame.validate();
                    admisionFrame.updateUI();
                }});
            }
        });
        txtIdentificacion.addKeyListener(new java.awt.event.KeyAdapter() {
        	@Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                if (admisionCmbTipoIdentificacionModel.getSelectedItem().getDescripcion().contains(CEDULA_VALIDACION)) {
                    UtilitarioForma.soloNumeros(evt);
                    UtilitarioForma.validarLongitudCampo(evt, 10);
                }else{
                    UtilitarioForma.validarLongitudCampo(evt, 20);
                }
            }
        });
        cmbTipoIdentificacionRepresentante.addActionListener(dt -> validateTipoIdentificacionRe());
        
        this.txtIdenRepresentante = admisionFrame.getTxtIdenRepresentante();
        txtIdenRepresentante.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
            	//No es usado
            }

            @Override
            public void focusLost(FocusEvent e) {
                validateIdenRepresentante();
            }
        });
        txtIdenRepresentante.addKeyListener(new java.awt.event.KeyAdapter() {
        	@Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                if (admisionCmbTipoIdentificacionModel.getSelectedItem().getDescripcion().contains(CEDULA_VALIDACION)) {
                    UtilitarioForma.soloNumeros(evt);
                    UtilitarioForma.validarLongitudCampo(evt, 10);
                }else{
                    UtilitarioForma.validarLongitudCampo(evt, 20);
                }
            }
        });
        this.txtPrimerNombre = admisionFrame.getTxtPrimerNombre();
        txtPrimerNombre.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
            	//No es usado
            }

            @Override
            public void focusLost(FocusEvent e) {
                validatePrimerNombre();
            }
        });
        txtPrimerNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                UtilitarioForma.soloLetras(evt);
                UtilitarioForma.validarLongitudCampo(evt, 50);
                UtilitarioForma.aMayusculas(evt);
            }
        });
        this.txtSegundoNombre = admisionFrame.getTxtSegundoNombre();
        txtSegundoNombre.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                UtilitarioForma.soloLetras(evt);
                UtilitarioForma.validarLongitudCampo(evt, 50);
                UtilitarioForma.aMayusculas(evt);
            }
        });
        this.txtPrimerApellido = admisionFrame.getTxtPrimerApellido();
        txtPrimerApellido.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
            	//No es usado
            }

            @Override
            public void focusLost(FocusEvent e) {
                validatePrimerApellido();
            }
        });
        txtPrimerApellido.addKeyListener(new java.awt.event.KeyAdapter() {
        	@Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                UtilitarioForma.soloLetras(evt);
                UtilitarioForma.validarLongitudCampo(evt, 50);
                UtilitarioForma.aMayusculas(evt);
            }
        });
        this.txtSegundoApellido = admisionFrame.getTxtSegundoApellido();
        txtSegundoApellido.addKeyListener(new java.awt.event.KeyAdapter() {
        	@Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                UtilitarioForma.soloLetras(evt);
                UtilitarioForma.validarLongitudCampo(evt, 50);
                UtilitarioForma.aMayusculas(evt);
            }
        });
        this.rbConvencionalPaciente = admisionFrame.getRbConvencionalPaciente();
        this.rbCelularPaciente = admisionFrame.getRbCelularPaciente();
        
        this.txtTelefonoUno = admisionFrame.getTxtTelefonoUno();
        txtTelefonoUno.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
            	//No es usado
            }

            @Override
            public void focusLost(FocusEvent e) {
            	 if(rbConvencionalPaciente.isSelected()){
            		 validateTelefonoUno(0);
                 }else {
                 if(rbCelularPaciente.isSelected()){
                	 validateTelefonoUno(1);
                 }
            }
        }});
        
        txtTelefonoUno.addKeyListener(new java.awt.event.KeyAdapter() {
        	@Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                UtilitarioForma.soloNumeros(evt);
                if(rbConvencionalPaciente.isSelected()){
                    UtilitarioForma.validarLongitudCampo(evt, 9);
                }else {
                if(rbCelularPaciente.isSelected()){
                    UtilitarioForma.validarLongitudCampo(evt, 10);
                }
            }}
        });
        this.txtDireccion = admisionFrame.getTxtDireccion();
        txtDireccion.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
            	//No es usado
            }

            @Override
            public void focusLost(FocusEvent e) {
                validateDireccion();
            }
        });
        txtDireccion.addKeyListener(new java.awt.event.KeyAdapter() {
        	@Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                UtilitarioForma.validarLongitudCampo(evt, 250);
                UtilitarioForma.aMayusculas(evt);
            }
        });
        this.rbConvencionalRepresentante = admisionFrame.getRbConvencionalRepresentante();
        this.rbCelularRepresentante = admisionFrame.getRbCelularRepresentante();
        
        this.txtTelefonoDos = admisionFrame.getTxtTelefonoDos();
        
        txtTelefonoDos.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
            	//No es usado
            }

            @Override
            public void focusLost(FocusEvent e) {
            	 if(rbConvencionalRepresentante.isSelected()){
            		 validateTelefonoDos(0);
                 }else {
                 if(rbCelularRepresentante.isSelected()){
                	 validateTelefonoDos(1);
                 }
            }
        }});
        
        txtTelefonoDos.addKeyListener(new java.awt.event.KeyAdapter() {
        	@Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                UtilitarioForma.soloNumeros(evt);
                if(rbConvencionalRepresentante.isSelected()){
                    UtilitarioForma.validarLongitudCampo(evt, 9);
                }
                if(rbCelularRepresentante.isSelected()){
                    UtilitarioForma.validarLongitudCampo(evt, 10);
                }
            }
        });

        
        this.lblEdad = admisionFrame.getLblEdad();
        this.txtArchivo = admisionFrame.getTxtArchivo();
        txtArchivo.addKeyListener(new java.awt.event.KeyAdapter() {
        	@Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                UtilitarioForma.validarLongitudCampo(evt, 10);
                UtilitarioForma.soloNumeros(evt);
            }
        });
        txtArchivo.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
            	//No es usado
            }

            @Override
            public void focusLost(FocusEvent e) {
                validateArchivo();
            }
        });
        this.btnCambioDpa = admisionFrame.getBtnCambioDpa();
        btnCambioDpa.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                changeDPA();
            }

            @Override
            public void mousePressed(MouseEvent e) {
            	//no se usa
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            	//no se usa
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            	//no se usa
            }

            @Override
            public void mouseExited(MouseEvent e) {
            	//no se usa
            }
        });
        this.cmbSexo.addItemListener(e -> { 
        	if (e.getStateChange() == ItemEvent.SELECTED) {
        		Detallecatalogo item = (Detallecatalogo) e.getItem();
        		if( VariableSesion.getInstanciaVariablesSession().getPersona() != null &&
        				!item.getId().equals(VariableSesion.getInstanciaVariablesSession()
        				.getPersona().getCtsexoId().getId())) {
        			VariableSesion.getInstanciaVariablesSession().setCambioInformacion(1);
            		VariableSesion.getInstanciaVariablesSession().setCambioInformacionDiagnostico(1);
            		VariableSesion.getInstanciaVariablesSession().setCambioInformacionVacunas(1);
        		}
        		
	       }
        });
        loadEntidad();
    }
    
    private void loadPaises() {
        List<Pais> paises = paisService.findAllOrderedByNacionalidad();
        admisionCmbNacionalidadModel.clear();
        admisionCmbNacionalidadModel.addElements(paises);
        cmbNacionalidad.setRenderer(new CmbNacionalidadCellRenderer());
        cmbNacionalidad.setModel(admisionCmbNacionalidadModel);
        cmbNacionalidad.setSelectedItem(paisNull);
    }

    private void loadCatalogosIndividuales() {
        List<Detallecatalogo> tiposIdentificacion = detallecatalogoService.findAllTiposIdentificacion();
        admisionCmbTipoIdentificacionModel.clear();
        admisionCmbTipoIdentificacionModel.addElements(tiposIdentificacion);
        cmbTipoIdentificacion.setRenderer(new CmbDetalleCatalogoCellRenderer());
        cmbTipoIdentificacion.setModel(admisionCmbTipoIdentificacionModel);
        cmbTipoIdentificacion.setSelectedItem(detallecatalogoNull);

        admisionCmbTipoIdentificacionReModel.clear();
        admisionCmbTipoIdentificacionReModel.addElements(tiposIdentificacion);
        cmbTipoIdentificacionRepresentante.setRenderer(new CmbDetalleCatalogoCellRenderer());
        cmbTipoIdentificacionRepresentante.setModel(admisionCmbTipoIdentificacionReModel);
        cmbTipoIdentificacionRepresentante.setSelectedItem(detallecatalogoNull);

        List<Detallecatalogo> tiposSeguro = detallecatalogoService.findAllTipoSeguroSalud();
        admisionCmbTipoSeguroModel.clear();
        admisionCmbTipoSeguroModel.addElements(tiposSeguro);
        cmbTipoSeguro.setRenderer(new CmbDetalleCatalogoCellRenderer());
        cmbTipoSeguro.setModel(admisionCmbTipoSeguroModel);
        cmbTipoSeguro.setSelectedItem(detallecatalogoNull);
        
        List<Detallecatalogo> tiposBono = detallecatalogoService.findAllTipoBonoEstado();
        admisionCmbTipoBonoModel.clear();
        admisionCmbTipoBonoModel.addElements(tiposBono);
        cmbTipoBono.setRenderer(new CmbDetalleCatalogoCellRenderer());
        cmbTipoBono.setModel(admisionCmbTipoBonoModel);
        cmbTipoBono.setSelectedItem(detallecatalogoNull);

        cmbNacEtnica.setRenderer(new CmbDetalleCatalogoCellRenderer());
        cmbPueblos.setRenderer(new CmbDetalleCatalogoCellRenderer());
        cmbCanton.setRenderer(new CmbCantonCellRenderer());
        cmbOrientacionSexual.setRenderer(new CmbDetalleCatalogoCellRenderer());
        cmbOrientacionSexual.setSelectedItem(detallecatalogoNull);
        cmbSexo.setRenderer(new CmbDetalleCatalogoCellRenderer());
        cmbSexo.setSelectedItem(detallecatalogoNull);
        cmbSexo.addActionListener(e -> loadOrientacionSexual());
        loadCantones();
        cmbParroquia.setRenderer(new CmbParroquiaCellRenderer());
        loadParroquias();
    }

    private void loadCatalogoAutoIdentificacionEtnica() {
        List<Detallecatalogo> listaAutoIdentificacionEtnica = detallecatalogoService.findAllAutoidentificacionEtnica();
        admisionCmbIdenEtnicaModel.clear();
        admisionCmbIdenEtnicaModel.addElements(listaAutoIdentificacionEtnica);
        cmbIdenEtnica.setRenderer(new CmbDetalleCatalogoCellRenderer());
        cmbIdenEtnica.setModel(admisionCmbIdenEtnicaModel);
        cmbIdenEtnica.setSelectedItem(detallecatalogoNull);
    }

    private void loadCatalogoNacionalidadEtnica() {
        Object[] controles = {cmbNacEtnica,cmbPueblos};
        if (admisionCmbIdenEtnicaModel.getSelectedItem() != null
                && admisionCmbIdenEtnicaModel.getSelectedItem().getDescripcion().contains(INDIGENA_SELECCION)) {
            UtilitarioForma.habilitarControles(true, controles);
            List<Detallecatalogo> listaNacionalidadEtnica = detallecatalogoService.findAllNacionalidades();
            admisionCmbNacEtnicaModel.clear();
            admisionCmbNacEtnicaModel.addElements(listaNacionalidadEtnica);
            cmbNacEtnica.setModel(admisionCmbNacEtnicaModel);
            cmbNacEtnica.setSelectedItem(detallecatalogoNull);
            controlesValidar[17] = cmbNacEtnica;
        } else {
            controlesValidar[17] = "";
            controlesValidar[18] = "";
            admisionCmbNacEtnicaModel.clear();
            admisionCmbPueblosModel.clear();
            UtilitarioForma.habilitarControles(false, controles);
        }
    }

    private void loadCatalogoPueblos() {
        Object[] controles = {cmbPueblos};
        if (admisionCmbNacEtnicaModel.getSelectedItem() != null
                && admisionCmbNacEtnicaModel.getSelectedItem().getDescripcion().contains(KICHWA_SELECCION)) {
            UtilitarioForma.habilitarControles(true, controles);
            List<Detallecatalogo> listaNacionalidadEtnica = detallecatalogoService.findAllPueblosKichwa();
            admisionCmbPueblosModel.clear();
            admisionCmbPueblosModel.addElements(listaNacionalidadEtnica);
            cmbPueblos.setModel(admisionCmbPueblosModel);
            cmbPueblos.setSelectedItem(detallecatalogoNull);
            controlesValidar[18] = cmbPueblos;
        } else {
            controlesValidar[18] = "";
            admisionCmbPueblosModel.clear();
            UtilitarioForma.habilitarControles(false, controles);
        }
    }

    private void loadProvincias() {
        List<Provincia> provincias = provinciaService.findAllOrderedByDescripcion();
        admisionCmbProvinciaModel.clear();
        admisionCmbProvinciaModel.addElements(provincias);
        cmbProvincia.setRenderer(new CmbProvinciaCellRenderer());
        cmbProvincia.setModel(admisionCmbProvinciaModel);
        cmbProvincia.setSelectedItem(provinciaNull);
    }

    private void loadCantones() {
        if (admisionCmbProvinciaModel.getSelectedItem() != null) {
            List<Canton> cantones = cantonService.findAllByProvinciaId(admisionCmbProvinciaModel.getSelectedItem().getId());
            admisionCmbCantonModel.clear();
            admisionCmbCantonModel.addElements(cantones);
            cmbCanton.setModel(admisionCmbCantonModel);
            cmbCanton.setSelectedItem(cantonNull);
        } else {
            admisionCmbCantonModel.clear();
        }

    }

    private void loadParroquias() {
        if (admisionCmbProvinciaModel.getSelectedItem() != null && admisionCmbCantonModel.getSelectedItem() != null) {
            List<Parroquia> parroquias = parroquiaService.findAllByProvinciaId(admisionCmbCantonModel.getSelectedItem().getId());
            admisionCmbParroquiaModel.clear();
            admisionCmbParroquiaModel.addElements(parroquias);
            cmbParroquia.setModel(admisionCmbParroquiaModel);
            cmbParroquia.setSelectedItem(parroquiaNull);
        } else {
            admisionCmbParroquiaModel.clear();
        }

    }


    private void validateIdentificacion() {
        if (admisionCmbTipoIdentificacionModel.getSelectedItem() != null && !ValidationSupport.isNullOrEmptyString(txtIdentificacion.getText())) {
            if (!admisionCmbTipoIdentificacionModel.getSelectedItem().getDescripcion().contains(NO_IDENTIFICADO)){
                if (admisionCmbTipoIdentificacionModel.getSelectedItem().getDescripcion().contains(CEDULA_VALIDACION)) {
                    listaErrores = cedulaValidator.validate(txtIdentificacion.getText());
                }else{
                    listaErrores = identExtrValidator.validate(txtIdentificacion.getText());
                }
            }
            if (!listaErrores.isEmpty()) {
                Notifications.showFormValidationAlert(listaErrores, "Validación de Identificación");
            }else{
                cargaDatosPaciente();
            }
        }
    }

    private void validateIdenRepresentante() {
        if (admisionCmbTipoIdentificacionReModel.getSelectedItem() != null && !ValidationSupport.isNullOrEmptyString(txtIdenRepresentante.getText())) {
            if (admisionCmbTipoIdentificacionReModel.getSelectedItem().getDescripcion().contains(CEDULA_VALIDACION)) {
                listaErrores = identificacionRepValidator.validate(txtIdenRepresentante.getText());
            }else{
                listaErrores = new ArrayList<>();
            }
            if (!listaErrores.isEmpty()) {
                Notifications.showFormValidationAlert(listaErrores, "Validación de Identificación");
            }
        }
    }

    private void validatePrimerNombre() {
    	if (!ValidationSupport.isNullOrEmptyString(txtPrimerNombre.getText())) {
    		listaErrores = nombreValidator.validate(txtPrimerNombre.getText());
            if (!listaErrores.isEmpty()) {
                Notifications.showFormValidationAlert(listaErrores, "Validación Primer Nombre");
            }
    	}
    }

    private void validatePrimerApellido() {
    	if (!ValidationSupport.isNullOrEmptyString(txtPrimerApellido.getText())) {
    		listaErrores = nombreValidator.validate(txtPrimerApellido.getText());
            if (!listaErrores.isEmpty()) {
                Notifications.showFormValidationAlert(listaErrores, "Validación Primer Apellido");
            }
    	}
    }

    private void validateTelefonoUno(int tipoContacto) {
    	if (!ValidationSupport.isNullOrEmptyString(txtTelefonoUno.getText())) {
    		listaErrores = telefonoValidator.validateTipoContacto(txtTelefonoUno.getText(), tipoContacto);
            if (!listaErrores.isEmpty()) {
                Notifications.showFormValidationAlert(listaErrores, "Validación Teléfono");
            }
    	}
    }
    
    private void validateTelefonoDos(int tipoContacto) {
    	if (!ValidationSupport.isNullOrEmptyString(txtTelefonoDos.getText())) {
    		listaErrores = telefonoValidator.validateTipoContacto(txtTelefonoDos.getText(), tipoContacto);
            if (!listaErrores.isEmpty()) {
                Notifications.showFormValidationAlert(listaErrores, "Validación Teléfono");
            }
    	}
    }
    
    private List<ValidationResult> validateTelefonoUnoReturn(int tipoContacto) {
    	if (!ValidationSupport.isNullOrEmptyString(txtTelefonoUno.getText())) {
    		listaErrores = telefonoValidator.validateTipoContacto(txtTelefonoUno.getText(), tipoContacto);
            if (!listaErrores.isEmpty()) {
                Notifications.showFormValidationAlert(listaErrores, "Validación Teléfono");
            }
    	}
    	return listaErrores;
    }
    
    private List<ValidationResult> validateTelefonoDosReturn(int tipoContacto) {
    	if (!ValidationSupport.isNullOrEmptyString(txtTelefonoDos.getText())) {
    		listaErrores = telefonoValidator.validateTipoContacto(txtTelefonoDos.getText(), tipoContacto);
            if (!listaErrores.isEmpty()) {
                Notifications.showFormValidationAlert(listaErrores, "Validación Teléfono");
            }
    	}
    	return listaErrores;
    }
    
    
    private void validateArchivo() {
    	if (!ValidationSupport.isNullOrEmptyString(txtArchivo.getText())) {
    		listaErrores = archivoValidator.validate(txtArchivo.getText());
            if (!listaErrores.isEmpty()) {
                Notifications.showFormValidationAlert(listaErrores, "Validación Archivo");
            }
    	}
    }

    private void validateDireccion() {
    	if (!ValidationSupport.isNullOrEmptyString(txtDireccion.getText())) {
    		listaErrores = nombreValidator.validate(txtDireccion.getText());
            if (!listaErrores.isEmpty()) {
                Notifications.showFormValidationAlert(listaErrores, "Validación Dirección");
            }
    	}
    }
    public void getPersona() throws GeneralSecurityException, IOException{
         
        Persona persona = VariableSesion.getInstanciaVariablesSession().getPersona();
        Persona personaConsultada = personaService.findOneByPersonaNumIdentificacion(txtIdentificacion.getText());
        if(persona == null ||  persona.getId() == null || personaConsultada == null){
            persona = new Persona();
        }
        if(!ValidationSupport.isNullOrEmptyString(txtArchivo.getText())){
            persona.setNumeroarchivo(txtArchivo.getText());
        }
        if(admisionCmbParroquiaModel.getSelectedItem() != null && !admisionCmbParroquiaModel.getSelectedItem().getDescripcion().equals(COMBONULO)){
            persona.setParroquiaId(admisionCmbParroquiaModel.getSelectedItem());
        }       
        if(!ValidationSupport.isNullOrEmptyString(txtIdentificacion.getText())){
            persona.setNumerohistoriaclinica(txtIdentificacion.getText());
            persona.setNumeroidentificacion(txtIdentificacion.getText());
        }
        if(!ValidationSupport.isNullOrEmptyString(txtTelefonoUno.getText())){
            persona.setTelefonopaciente(txtTelefonoUno.getText());
        }
        if(!ValidationSupport.isNullOrEmptyString(txtTelefonoDos.getText())){
            persona.setTelefonofamiliar(txtTelefonoDos.getText());
        }
        if(!ValidationSupport.isNullOrEmptyString(txtDireccion.getText())){
            persona.setDirecciondomicilio(txtDireccion.getText());
        }
        if(!ValidationSupport.isNullOrEmptyString(txtPrimerNombre.getText())){
            persona.setPrimernombre(txtPrimerNombre.getText().trim());
        }
        if(!ValidationSupport.isNullOrEmptyString(txtSegundoNombre.getText())){
            persona.setSegundonombre(txtSegundoNombre.getText().trim());
        }
        if(!ValidationSupport.isNullOrEmptyString(txtPrimerApellido.getText())){
            persona.setPrimerapellido(txtPrimerApellido.getText().trim());
        }
        if(!ValidationSupport.isNullOrEmptyString(txtSegundoApellido.getText())){
            persona.setSegundoapellido(txtSegundoApellido.getText().trim());
        }
        if(dttFechaAtencion.getDate() != null){
        	VariableSesion.getInstanciaVariablesSession().setFechaAtencion(Date.from(dttFechaAtencion.getDate().atStartOfDay(ZoneId.systemDefault()).toInstant()));
        }
        if(dttFechaNacimiento.getDate() != null){
        	persona.setFechanacimiento(Date.from(dttFechaNacimiento.getDate().atStartOfDay(ZoneId.systemDefault()).toInstant()));
        }
        persona.setEstado(1);
        if(admisionCmbIdenEtnicaModel.getSelectedItem() != null && !admisionCmbIdenEtnicaModel.getSelectedItem().getDescripcion().equals(COMBONULO)){
            persona.setCtetniaId(admisionCmbIdenEtnicaModel.getSelectedItem());
        }
        if(admisionCmbPueblosModel.getSelectedItem() != null && !admisionCmbPueblosModel.getSelectedItem().getDescripcion().equals(COMBONULO)){
            persona.setCtpuebloId(admisionCmbPueblosModel.getSelectedItem());
        }
        if(admisionCmbNacEtnicaModel.getSelectedItem() != null && !admisionCmbNacEtnicaModel.getSelectedItem().getDescripcion().equals(COMBONULO)){
            persona.setCtnacionalidadetnicaId(admisionCmbNacEtnicaModel.getSelectedItem());
        }
        if(admisionCmbTipoBonoModel.getSelectedItem() != null && !admisionCmbTipoBonoModel.getSelectedItem().getDescripcion().equals(COMBONULO)){
            persona.setCttipobonoId(admisionCmbTipoBonoModel.getSelectedItem());
        }
        if(admisionCmbSexoModel.getSelectedItem() != null && !admisionCmbSexoModel.getSelectedItem().getDescripcion().equals(COMBONULO)){
            persona.setCtsexoId(admisionCmbSexoModel.getSelectedItem());
        }
        if(admisionCmbIdentGenero.getSelectedItem() != null && !admisionCmbIdentGenero.getSelectedItem().getDescripcion().equals(COMBONULO)){
            persona.setCtidentidadgeneroId(admisionCmbIdentGenero.getSelectedItem());
        }
        if(admisionCmbOrientacionSexualModel.getSelectedItem() != null && !admisionCmbOrientacionSexualModel.getSelectedItem().getDescripcion().equals(COMBONULO)){
            persona.setCtorientacionsexualId(admisionCmbOrientacionSexualModel.getSelectedItem());
        }
        if(admisionCmbTipoSeguroModel.getSelectedItem() != null && !admisionCmbTipoSeguroModel.getSelectedItem().getDescripcion().equals(COMBONULO)){
            persona.setCttiposegurosaludId(admisionCmbTipoSeguroModel.getSelectedItem());
        }
        if(admisionCmbTipoIdentificacionModel.getSelectedItem() != null && !admisionCmbTipoIdentificacionModel.getSelectedItem().getDescripcion().equals(COMBONULO)){
            persona.setCttipoidentificacionId(admisionCmbTipoIdentificacionModel.getSelectedItem());
        }
        if(admisionCmbNacionalidadModel.getSelectedItem() != null && !admisionCmbNacionalidadModel.getSelectedItem().getDescripcion().equals(COMBONULO)){
            persona.setPaisId(admisionCmbNacionalidadModel.getSelectedItem());
        }
        if(admisionCmbTipoIdentificacionReModel.getSelectedItem() != null && !admisionCmbTipoIdentificacionReModel.getSelectedItem().getDescripcion().equals(COMBONULO)){
            persona.setCttipoidentrepteId(admisionCmbTipoIdentificacionReModel.getSelectedItem());
        }
        if(!ValidationSupport.isNullOrEmptyString(txtIdenRepresentante.getText())){
            persona.setNumidentificacionrepte(txtIdenRepresentante.getText());
        }
        persona.setCttipopersonaId(findTipoPaciente());
        generarSequencialNoidentificado(persona);
        VariableSesion.getInstanciaVariablesSession().setIsMujer(false);
        VariableSesion.getInstanciaVariablesSession().setIsHombre(false);
        VariableSesion.getInstanciaVariablesSession().setIsInterSexual(false);
        switch (persona.getCtsexoId().getId()) {
            case MUJER:
                VariableSesion.getInstanciaVariablesSession().setIsMujer(true);
                break;
            case HOMBRE:
                VariableSesion.getInstanciaVariablesSession().setIsHombre(true);
                break;
            default:
                VariableSesion.getInstanciaVariablesSession().setIsInterSexual(true);
                break;
        }
        
        if(persona.getUuidpersona() == null || persona.getUuidpersona().equals("")){
        	persona.setUuidpersona(UUID.randomUUID().toString());
        }
        
        
        
        VariableSesion.getInstanciaVariablesSession().setPersona(persona);
        Atencionmedica atencionMedica = new Atencionmedica();
		atencionMedica.setFechacreacion(new Date());
		atencionMedica.setFechaatencion(VariableSesion.getInstanciaVariablesSession().getFechaAtencion());
		atencionMedica.setCttipobonoId(VariableSesion.getInstanciaVariablesSession().getPersona().getCttipobonoId());
		atencionMedica.setCttiposegurosaludId(VariableSesion.getInstanciaVariablesSession().getPersona().getCttiposegurosaludId());
		atencionMedica.setCtespecialidadmedicaId(VariableSesion.getInstanciaVariablesSession().getCtEspecialidad());
		atencionMedica.setUsuariocreacionId(VariableSesion.getInstanciaVariablesSession().getProfesional());
		VariableSesion.getInstanciaVariablesSession().setAtencionMedica(atencionMedica);
        
    }
    
    private void enabledRepresentante(){
    	VariableSesion.getInstanciaVariablesSession().setCambioInformacion(1);
		VariableSesion.getInstanciaVariablesSession().setCambioInformacionDiagnostico(1);
		VariableSesion.getInstanciaVariablesSession().setCambioInformacionVacunas(1);
        if(dttFechaNacimiento.getDate() != null && dttFechaAtencion.getDate() != null){
        	VariableSesion.getInstanciaVariablesSession().setAniosPersona(FechasUtil.getDiffDatesDesdeHasta(Date.from(dttFechaNacimiento.getDate().atStartOfDay(ZoneId.systemDefault()).toInstant()),Date.from(dttFechaAtencion.getDate().atStartOfDay(ZoneId.systemDefault()).toInstant()),0));
            VariableSesion.getInstanciaVariablesSession().setMesesPersona(FechasUtil.getDiffDatesDesdeHasta(Date.from(dttFechaNacimiento.getDate().atStartOfDay(ZoneId.systemDefault()).toInstant()),Date.from(dttFechaAtencion.getDate().atStartOfDay(ZoneId.systemDefault()).toInstant()),1));
            VariableSesion.getInstanciaVariablesSession().setDiasPersona(FechasUtil.getDiffDatesDesdeHasta(Date.from(dttFechaNacimiento.getDate().atStartOfDay(ZoneId.systemDefault()).toInstant()),Date.from(dttFechaAtencion.getDate().atStartOfDay(ZoneId.systemDefault()).toInstant()),2));
            
            if(VariableSesion.getInstanciaVariablesSession().getAniosPersona() < 0 || VariableSesion.getInstanciaVariablesSession().getMesesPersona() < 0 || VariableSesion.getInstanciaVariablesSession().getDiasPersona() < 0){
            	dttFechaAtencion.clear();
            	UtilitarioForma.muestraMensaje("Validación", admisionFrame, "ERROR", "Existe un problema con las fechas de Atención o Nacimiento por favor verificar ");
            }else{
            	lblEdad.setText(VariableSesion.getInstanciaVariablesSession().getAniosPersona() + " años "+ VariableSesion.getInstanciaVariablesSession().getMesesPersona() + " meses " + VariableSesion.getInstanciaVariablesSession().getDiasPersona() + " días");
                
                VariableSesion.getInstanciaVariablesSession().setFechaNacimiento(dttFechaNacimiento.getDate());
                
                /*
                 VALIDACION REPRESENTANTE 
                */
                Object[] controles = {cmbTipoIdentificacionRepresentante,admisionFrame.getTxtIdenRepresentante()};
                if(VariableSesion.getInstanciaVariablesSession().getAniosPersona() > Integer.parseInt(ANIOSMENOR)){
                    UtilitarioForma.habilitarControles(false, controles);
                    controlesValidar[5] = "";
                    controlesValidar[20] = "";
                    UtilitarioForma.reSetearControles(admisionFrame, txtIdenRepresentante);
                    UtilitarioForma.limpiarCampos(admisionFrame, controles);
                    cmbTipoIdentificacionRepresentante.setSelectedItem(null);
                }else{
                    UtilitarioForma.habilitarControles(true, controles);
                    controlesValidar[5] = admisionFrame.getTxtIdenRepresentante();
                    controlesValidar[20] = cmbTipoIdentificacionRepresentante;
                }
                /*
                 VALIDACION IDENTIDAD DE GENERO Y ORIENTACION SEXUAL
                */
                if(VariableSesion.getInstanciaVariablesSession().getAniosPersona() >= EDADINTERSEXUAL){
                    if(VariableSesion.getInstanciaVariablesSession().getMesesPersona() > 0 || VariableSesion.getInstanciaVariablesSession().getDiasPersona() > 0  ){
    	            	controlesValidar[10] = cmbOrientacionSexual;
    	                controlesValidar[11] = cmbIdentGenero;
    	                cmbIdentGenero.setSelectedItem(detallecatalogoNull);
                        cmbOrientacionSexual.setSelectedItem(detallecatalogoNull);
    	                UtilitarioForma.habilitarControles(true, cmbIdentGenero, cmbOrientacionSexual);
                    }else{
                    	UtilitarioForma.reSetearControles(admisionFrame, cmbIdentGenero, cmbOrientacionSexual);
                        cmbIdentGenero.setSelectedItem(detallecatalogoService.findOneByCatalogoIdNoAplica(ConstantesDetalleCatalogo.IDENTIDAD_DE_GENERO));
                        cmbOrientacionSexual.setSelectedItem(detallecatalogoNull);
                        controlesValidar[10] = "";
                        controlesValidar[11] = "";
                        UtilitarioForma.habilitarControles(false, cmbIdentGenero, cmbOrientacionSexual);
                    }
                }else{
                    UtilitarioForma.reSetearControles(admisionFrame, cmbIdentGenero, cmbOrientacionSexual);
                    cmbIdentGenero.setSelectedItem(detallecatalogoService.findOneByCatalogoIdNoAplica(ConstantesDetalleCatalogo.IDENTIDAD_DE_GENERO));
                    cmbOrientacionSexual.setSelectedItem(detallecatalogoNull);
                    controlesValidar[10] = "";
                    controlesValidar[11] = "";
                    UtilitarioForma.habilitarControles(false, cmbIdentGenero, cmbOrientacionSexual);

                }
                loadSexoByEdad();
                
                /*
                 * VALIDA TIPO DE SEGURO < 18 ANIOS
                 * */
                if(VariableSesion.getInstanciaVariablesSession().getAniosPersona() < EDADSEGURO){
                	cmbTipoSeguro.setSelectedItem(detallecatalogoService.findOneByCatalogoDescripcion(ConstantesDetalleCatalogo.TIPO_SEGURO_SALUD,CAMPO_SEGUROINDIRECTO));
                }else if(VariableSesion.getInstanciaVariablesSession().getAniosPersona() == EDADSEGURO){
                	if(VariableSesion.getInstanciaVariablesSession().getMesesPersona() == 0 || VariableSesion.getInstanciaVariablesSession().getDiasPersona() == 0){
                		cmbTipoSeguro.setSelectedItem(detallecatalogoService.findOneByCatalogoDescripcion(ConstantesDetalleCatalogo.TIPO_SEGURO_SALUD,CAMPO_SEGUROINDIRECTO));
                	}else{
                		cmbTipoSeguro.setSelectedItem(detallecatalogoNull);
                	}
                }else{
                	cmbTipoSeguro.setSelectedItem(detallecatalogoNull);
                }
            }
        }
    }
    
    public boolean validateForm(){
        boolean validarForma;
        validarForma = UtilitarioForma.validarCamposRequeridos(controlesValidar);
        if(validarForma){
            validarForma = validateAll();
            if(validarForma){
                try {
                            getPersona();
                    } catch (GeneralSecurityException | IOException e) {
                            UtilitarioForma.muestraMensaje("Validación", admisionFrame, "ERROR", "Existe un problema "+e.getMessage() );
                    }
            }
        }
        
       return validarForma;
    }

    private void addAutocompleteToCombos() {
    	AutoCompleteDecorator.decorate(cmbTipoSeguro, new DetalleCatalogoToStringConverter());
        AutoCompleteDecorator.decorate(cmbIdenEtnica, new DetalleCatalogoToStringConverter());
        AutoCompleteDecorator.decorate(cmbNacEtnica, new DetalleCatalogoToStringConverter());
        AutoCompleteDecorator.decorate(cmbPueblos, new DetalleCatalogoToStringConverter());
        AutoCompleteDecorator.decorate(cmbNacionalidad, new PaisToStringConverter());
        AutoCompleteDecorator.decorate(cmbProvincia, new ProvinciaToStringConverter());
        AutoCompleteDecorator.decorate(cmbCanton, new CantonToStringConverter());
        AutoCompleteDecorator.decorate(cmbParroquia, new ParroquiaToStringConverter());
        AutoCompleteDecorator.decorate(cmbSexo, new DetalleCatalogoToStringConverter());
        AutoCompleteDecorator.decorate(cmbOrientacionSexual, new DetalleCatalogoToStringConverter());
        AutoCompleteDecorator.decorate(cmbIdentGenero, new DetalleCatalogoToStringConverter());
        AutoCompleteDecorator.decorate(cmbTipoBono, new DetalleCatalogoToStringConverter());
        AutoCompleteDecorator.decorate(cmbTipoIdentificacion, new DetalleCatalogoToStringConverter());
        AutoCompleteDecorator.decorate(cmbTipoIdentificacionRepresentante, new DetalleCatalogoToStringConverter());
    }
    
    private void generarSequencialNoidentificado(Persona persona) {
        if(persona.getCttipoidentificacionId().getId() == Integer.parseInt(ComunEnum.CT_NOIDENTIFICADOS.getDescripcion())) {
            try {
            	String numeronoidentificado = UtilitarioSequencial.obtenerSequencialNoidentificado(persona.getPrimernombre(), persona.getSegundonombre(), persona.getPrimerapellido(), persona.getSegundoapellido(), persona.getFechanacimiento(), persona.getPaisId().getId(), persona.getParroquiaId().getCantonId().getProvinciaId().getCodprovincia());
                persona.setNumeroidentificacion(numeronoidentificado);
                persona.setNumerohistoriaclinica(numeronoidentificado);
            } catch (Exception ex) {
                Logger.getLogger(AdmisionController.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
    private void validateTipoIdentificacion(){
        if (admisionCmbTipoIdentificacionModel.getSelectedItem() != null && admisionCmbTipoIdentificacionModel.getSelectedItem().getDescripcion().contains(NO_IDENTIFICADO)) {
            controlesValidar[0] = "";
            UtilitarioForma.limpiarCampos(admisionFrame, txtIdentificacion);
            UtilitarioForma.activarControles(admisionFrame, false, txtIdentificacion);
        }else{
            controlesValidar[0] = txtIdentificacion;
            UtilitarioForma.limpiarCampos(admisionFrame, txtIdentificacion);
            UtilitarioForma.activarControles(admisionFrame, true, txtIdentificacion);
        }
    }
    private void validateTipoIdentificacionRe(){
        if (admisionCmbTipoIdentificacionReModel.getSelectedItem() != null) {
        	if(admisionCmbTipoIdentificacionReModel.getSelectedItem().getDescripcion().contains(NO_IDENTIFICADO)){
                controlesValidar[5] = "";
                UtilitarioForma.limpiarCampos(admisionFrame, txtIdenRepresentante);
                UtilitarioForma.activarControles(admisionFrame, false, txtIdenRepresentante);
        	}else{
        		controlesValidar[5] = admisionFrame.getTxtIdenRepresentante();
                UtilitarioForma.activarControles(admisionFrame, true, txtIdenRepresentante);
        	}
        }
    }
    
    private Detallecatalogo findTipoPaciente(){
       return detallecatalogoService.findTipoPaciente();
    }
    
    private void loadOrientacionSexual(){
        VariableSesion.getInstanciaVariablesSession().setBanderaCambioSexoGrupoP(true);
        List<Detallecatalogo> orientacionesSexuales = null;
        Integer aniosPersona = VariableSesion.getInstanciaVariablesSession().getAniosPersona();
        if(aniosPersona != null && aniosPersona >= EDADINTERSEXUAL && admisionCmbSexoModel != null && admisionCmbSexoModel.getSelectedItem() != null){
            orientacionesSexuales = detallecatalogoService.findByCatalogoIdSexoIdEdad(admisionCmbSexoModel.getSelectedItem().getId(), VariableSesion.getInstanciaVariablesSession().getAniosPersona().intValue());
            admisionCmbOrientacionSexualModel.clear();
            admisionCmbOrientacionSexualModel.addElements(orientacionesSexuales);
            cmbOrientacionSexual.setModel(admisionCmbOrientacionSexualModel);
            cmbOrientacionSexual.setSelectedItem(detallecatalogoNull);
        }else{
        	orientacionesSexuales = new ArrayList<>();
        	orientacionesSexuales.add(detallecatalogoService.findOneByCatalogoIdNoAplica(ConstantesDetalleCatalogo.ORIENTACION_SEXUAL));
            validateFechaNacimiento();
            admisionCmbOrientacionSexualModel.clear();
            admisionCmbOrientacionSexualModel.addElements(orientacionesSexuales);
            cmbOrientacionSexual.setModel(admisionCmbOrientacionSexualModel);
        }
        loadIdentidadGeneros();
    }
    
    private void validateFechaNacimiento(){
        UtilitarioForma.validarCamposRequeridos(dttFechaNacimiento);
    }
    
    private void cargaDatosPaciente(){
        Persona persona = personaService.findOneByPersonaNumIdentificacion(txtIdentificacion.getText());
        if(persona != null && persona.getCttipopersonaId().getDescripcion().equals(PACIENTE)){
            VariableSesion.getInstanciaVariablesSession().setPersona(persona);
            if(persona.getCttipoidentificacionId() != null){
                cmbTipoIdentificacion.setSelectedItem(detallecatalogoService.findOneByDetalleId(persona.getCttipoidentificacionId().getId()));
            }
            if(persona.getFechanacimiento() != null){
                dttFechaNacimiento.setDate(Instant.ofEpochMilli(persona.getFechanacimiento().getTime()).atZone(ZoneId.systemDefault()).toLocalDate());
            }
            if(persona.getPrimernombre() != null){
                txtPrimerNombre.setText(persona.getPrimernombre());
            }
            if(persona.getSegundonombre() != null){
                txtSegundoNombre.setText(persona.getSegundonombre());
            }
            if(persona.getPrimerapellido() != null){
                txtPrimerApellido.setText(persona.getPrimerapellido());
            }
            if(persona.getSegundoapellido() != null){
                txtSegundoApellido.setText(persona.getSegundoapellido());
            }
            if(persona.getPaisId() != null){
                cmbNacionalidad.setSelectedItem(paisService.findOneByPaisId(persona.getPaisId().getId()));
            }
            if(persona.getCtetniaId() != null){
                cmbIdenEtnica.setSelectedItem(detallecatalogoService.findOneByDetalleId(persona.getCtetniaId().getId()));
            }
            if(persona.getCtnacionalidadetnicaId() != null){
                cmbNacEtnica.setSelectedItem(detallecatalogoService.findOneByDetalleId(persona.getCtnacionalidadetnicaId().getId()));
            }
            if(persona.getCtpuebloId() != null){
                cmbPueblos.setSelectedItem(detallecatalogoService.findOneByDetalleId(persona.getCtpuebloId().getId()));
            }
            if(persona.getCtsexoId() != null){
                cmbSexo.setSelectedItem(detallecatalogoService.findOneByDetalleId(persona.getCtsexoId().getId()));
            }
            if(persona.getCtorientacionsexualId() != null){
                cmbOrientacionSexual.setSelectedItem(detallecatalogoService.findOneByDetalleId(persona.getCtorientacionsexualId().getId()));
            }
            if(persona.getCtidentidadgeneroId() != null){
                cmbIdentGenero.setSelectedItem(detallecatalogoService.findOneByDetalleId(persona.getCtidentidadgeneroId().getId()));
            }
            if(persona.getCttiposegurosaludId() != null){
                cmbTipoSeguro.setSelectedItem(detallecatalogoService.findOneByDetalleId(persona.getCttiposegurosaludId().getId()));
            }
            if(persona.getCttipobonoId() != null){
                cmbTipoBono.setSelectedItem(detallecatalogoService.findOneByDetalleId(persona.getCttipobonoId().getId()));
            }
            if(persona.getParroquiaId() != null){
                cmbProvincia.setSelectedItem(provinciaService.findOneByProvinciaId(persona.getParroquiaId().getCantonId().getProvinciaId().getId()));
                cmbCanton.setSelectedItem(cantonService.findOneByCantonId(persona.getParroquiaId().getCantonId().getId()));
                cmbParroquia.setSelectedItem(parroquiaService.findOneByParroquiaId(persona.getParroquiaId().getId()));
            }
            if(persona.getTelefonopaciente() != null){
                txtTelefonoUno.setText(persona.getTelefonopaciente());
                if(persona.getTelefonopaciente().length() == 9){
                    rbConvencionalRepresentante.setSelected(true);
                }else if(persona.getTelefonopaciente().length() == 10){
                    rbCelularRepresentante.setSelected(true);
                }
            }
            if(persona.getDirecciondomicilio() != null){
                txtDireccion.setText(persona.getDirecciondomicilio());
            }
            if(persona.getCttipoidentrepteId() != null){
                cmbTipoIdentificacionRepresentante.setSelectedItem(detallecatalogoService.findOneByDetalleId(persona.getCttipoidentrepteId().getId()));
            }
            if(persona.getNumidentificacionrepte() != null){
                txtIdenRepresentante.setText(persona.getNumidentificacionrepte());
            }
            if(persona.getTelefonofamiliar() != null){
                txtTelefonoDos.setText(persona.getTelefonofamiliar());
                if(persona.getTelefonofamiliar().length() == 9){
                    rbConvencionalRepresentante.setSelected(true);
                }else if(persona.getTelefonofamiliar().length() == 10){
                    rbCelularRepresentante.setSelected(true);
                }
            }
            if(persona.getNumeroarchivo() != null){
                txtArchivo.setText(persona.getNumeroarchivo().toString());
            }
            txtIdentificacion.setText(persona.getNumeroidentificacion());
        }
    }
    private void loadSexoByEdad(){
        List<Detallecatalogo> sexos = null;
        admisionCmbSexoModel.clear();
        if(VariableSesion.getInstanciaVariablesSession().getAniosPersona() <= ANIOSBEBE){
        	if(VariableSesion.getInstanciaVariablesSession().getAniosPersona() == 0){
        		sexos = detallecatalogoService.findAllSexos();
        	}else if(VariableSesion.getInstanciaVariablesSession().getAniosPersona() == 1 && VariableSesion.getInstanciaVariablesSession().getMesesPersona() == 0 && VariableSesion.getInstanciaVariablesSession().getDiasPersona() == 0){
        		sexos = detallecatalogoService.findAllSexos();
        	}else{
        		sexos = detallecatalogoService.findAllSexosNoIntersexual();
        	}
        }else{
            sexos = detallecatalogoService.findAllSexosNoIntersexual();
        }
        admisionCmbSexoModel.addElements(sexos);
        cmbSexo.setModel(admisionCmbSexoModel);
        cmbSexo.setSelectedItem(detallecatalogoNull);
        changeInformation();
    }
    
    private boolean validateAll(){
        boolean validar = true;
        if (admisionCmbTipoIdentificacionModel.getSelectedItem() != null && !ValidationSupport.isNullOrEmptyString(txtIdentificacion.getText())) {
            if (!admisionCmbTipoIdentificacionModel.getSelectedItem().getDescripcion().contains(NO_IDENTIFICADO)){
                if (admisionCmbTipoIdentificacionModel.getSelectedItem().getDescripcion().contains(CEDULA_VALIDACION)) {
                    listaErrores = cedulaValidator.validate(txtIdentificacion.getText());
                }else{
                    listaErrores = identExtrValidator.validate(txtIdentificacion.getText());
                }
            }
            if (!listaErrores.isEmpty()) {
                Notifications.showFormValidationAlert(listaErrores, "Validación de Identificación");
                return false;
            }
        }
        
        if(rbConvencionalPaciente.isSelected()){
        	listaErrores = validateTelefonoUnoReturn(0);
        }else if(rbCelularPaciente.isSelected()){
        	listaErrores = validateTelefonoUnoReturn(1);
        }
        
        if(!listaErrores.isEmpty()){
        	return false;
        }

        if(rbConvencionalRepresentante.isSelected()){
        	listaErrores = validateTelefonoDosReturn(0);
        }else if(rbCelularRepresentante.isSelected()){
        	listaErrores = validateTelefonoDosReturn(1);
        }
        
        if(!listaErrores.isEmpty()){
        	return false;
        }
        
        if(listaErrores.isEmpty()){
            validateIdenRepresentante();
            if(!listaErrores.isEmpty()){
            	return false;
            }
            validateArchivo();
        	if(!listaErrores.isEmpty()){
        		return false;
            }
        	validar = validateIdentificacionPacRep();
        }else{
            validar = false;
        }
        return validar;
    }
    
    private void loadDefaultCombos(){
        detallecatalogoNull = new Detallecatalogo(){
            private static final long serialVersionUID = 1L;
            boolean isNoSelectable = true;
        };
        detallecatalogoNull.setId(0);
        detallecatalogoNull.setDescripcion(COMBONULO);

        paisNull = new Pais() {
            private static final long serialVersionUID = 1L;
            boolean isNoSelectable = true;
        };
        paisNull.setId(0);
        paisNull.setNacionalidad(COMBONULO);
        
        provinciaNull = new Provincia(){
            private static final long serialVersionUID = 1L;
            boolean isNoSelectable = true;
        };
        provinciaNull.setId(0);
        provinciaNull.setDescripcion(COMBONULO);
        
        cantonNull = new Canton(){
            private static final long serialVersionUID = 1L;
            boolean isNoSelectable = true;
        };
        cantonNull.setId(0);
        cantonNull.setDescripcion(COMBONULO);
        
        parroquiaNull = new Parroquia(){
            private static final long serialVersionUID = 1L;
            boolean isNoSelectable = true;
        };
        parroquiaNull.setId(0);
        parroquiaNull.setDescripcion(COMBONULO);
    }
    
    private void loadControles(){
        controlesValidar[0] = admisionFrame.getTxtIdentificacion();
        controlesValidar[1] = admisionFrame.getTxtPrimerNombre();
        controlesValidar[2] = admisionFrame.getTxtPrimerApellido();
        controlesValidar[3] = admisionFrame.getTxtTelefonoUno();
        controlesValidar[4] = admisionFrame.getTxtDireccion();
        controlesValidar[6] = admisionFrame.getDttFechaNacimiento();
        controlesValidar[7] = cmbNacionalidad;
        controlesValidar[8] = cmbIdenEtnica;
        controlesValidar[9] = cmbSexo;
        controlesValidar[10] = cmbOrientacionSexual;
        controlesValidar[11] = cmbIdentGenero;
        controlesValidar[12] = cmbTipoSeguro;
        controlesValidar[13] = cmbTipoBono;
        controlesValidar[14] = cmbProvincia;
        controlesValidar[15] = cmbCanton;
        controlesValidar[16] = cmbParroquia;
        controlesValidar[17] = cmbNacEtnica;
        controlesValidar[18] = cmbPueblos;
        controlesValidar[19] = cmbTipoIdentificacion;
        controlesValidar[20] = cmbTipoIdentificacionRepresentante;
        controlesValidar[21] = admisionFrame.getDttFechaAtencion();
    }
    
    private Object [] controlesLimpiar(){
    	return new Object[] {cmbTipoIdentificacion,txtIdentificacion,dttFechaNacimiento,txtPrimerNombre,txtSegundoNombre,
				             txtPrimerApellido,txtSegundoApellido,txtArchivo,cmbSexo,cmbOrientacionSexual,cmbIdentGenero,
				             cmbNacionalidad,cmbIdenEtnica,cmbNacEtnica,cmbPueblos,cmbTipoSeguro,cmbTipoBono,cmbProvincia,
				             cmbCanton,cmbParroquia,txtTelefonoUno,txtTelefonoDos,txtDireccion,
				             cmbTipoIdentificacionRepresentante,txtIdenRepresentante
				             }; 
    }
    private void validateNacionalidad(){
        if(admisionCmbNacionalidadModel.getSelectedItem() != null){
            if(admisionCmbNacionalidadModel.getSelectedItem().getNacionalidad().equals(PAIS)){
                controlesValidar[8] = cmbIdenEtnica;
                cmbIdenEtnica.setSelectedItem(detallecatalogoNull);
                UtilitarioForma.activarControles(admisionFrame, true, cmbIdenEtnica);
            }else{
                controlesValidar[8] = "";
                controlesValidar[17] = "";
                controlesValidar[18] = "";
                UtilitarioForma.activarControles(admisionFrame, false, cmbIdenEtnica,cmbNacEtnica,cmbPueblos);
                cmbIdenEtnica.setSelectedItem(detallecatalogoService.findOneByCatalogoIdNoAplica(ConstantesDetalleCatalogo.AUTOIDENTIFICACION_ETNICA));
                cmbNacEtnica.setSelectedItem(detallecatalogoNull);
                cmbPueblos.setSelectedItem(detallecatalogoNull);
            }
        }
    }
    
    public void cleanInput(){
        dttFechaNacimiento.setText(null);
        dttFechaNacimiento.clear();
        dttFechaAtencion.setText(null);
        dttFechaAtencion.clear();
        lblEdad.setText("");
        UtilitarioForma.limpiarCampos(admisionFrame, controlesLimpiar());
        setNullCombobox();
    }
    private void setNullCombobox(){
        cmbNacionalidad.setSelectedItem(paisNull);
        cmbTipoIdentificacion.setSelectedItem(detallecatalogoNull);
        cmbTipoIdentificacionRepresentante.setSelectedItem(detallecatalogoNull);
        cmbTipoSeguro.setSelectedItem(detallecatalogoNull);
        cmbTipoBono.setSelectedItem(detallecatalogoNull);
        cmbIdentGenero.setSelectedItem(detallecatalogoNull);
        cmbOrientacionSexual.setSelectedItem(detallecatalogoNull);
        cmbSexo.setSelectedItem(detallecatalogoNull);
        cmbIdenEtnica.setSelectedItem(detallecatalogoNull);
        cmbNacEtnica.setSelectedItem(detallecatalogoNull);
        cmbPueblos.setSelectedItem(detallecatalogoNull);
        cmbProvincia.setSelectedItem(provinciaNull);
        cmbCanton.setSelectedItem(cantonNull);
        cmbParroquia.setSelectedItem(parroquiaNull);
    }
    
    private void changeInformation(){
    	if(VariableSesion.getInstanciaVariablesSession().getPersona() != null){
    		VariableSesion.getInstanciaVariablesSession().setCambioInformacion(1);
    		VariableSesion.getInstanciaVariablesSession().setCambioInformacionDiagnostico(1);
    		VariableSesion.getInstanciaVariablesSession().setCambioInformacionVacunas(1);
    	}
    }
    private Boolean validateIdentificacionPacRep(){
    	Boolean validar = true;
    	if(txtIdentificacion.isEnabled() && txtIdenRepresentante.isEnabled() && 
			txtIdentificacion.getText().equals(txtIdenRepresentante.getText())){
			UtilitarioForma.muestraMensaje("Advertencia", admisionFrame, "ERROR", "El campo Identificación del paciente no debe ser igual al campo Identificación del Representante");
			validar = false;
    	}
    	return validar;
    }
    
    private void loadIdentidadGeneros(){
    	List<Detallecatalogo> identGeneros = detallecatalogoService.findAllIdentidadGenero();
    	if(admisionCmbSexoModel.getSelectedItem() != null){
    		if(admisionCmbSexoModel.getSelectedItem().getId().equals(HOMBRE)){
    			identGeneros.remove(detallecatalogoService.findOneByDetalleId(TRANSMASCULINO));
    		}else if(admisionCmbSexoModel.getSelectedItem().getId().equals(MUJER)){
    			identGeneros.remove(detallecatalogoService.findOneByDetalleId(TRANSFEMENINO));
    		}
    	}
        admisionCmbIdentGenero.clear();
        admisionCmbIdentGenero.addElements(identGeneros);
        cmbIdentGenero.setRenderer(new CmbDetalleCatalogoCellRenderer());
        cmbIdentGenero.setModel(admisionCmbIdentGenero);
        cmbIdentGenero.setSelectedItem(detallecatalogoNull);
    }
    
    private void validateFechaAtencion(){
    	int mes = dttFechaAtencion.getDate().getMonthValue();
    	int anio = dttFechaAtencion.getDate().getYear();
    	if(cierreatencionesService.findOneByUsuarioAnioMesCerrado(VariableSesion.getInstanciaVariablesSession().getUsuario().getId(), String.valueOf(anio), mes)){
    		dttFechaAtencion.clear();
    		JOptionPane.showMessageDialog(dttFechaAtencion, "La fecha ingresa no es permitida", "Error de fecha atención", JOptionPane.ERROR_MESSAGE);
    	}
    }

	public void setDatePickerVetoPolicy() {
		LocalDate today = LocalDate.now();
		DatePickerSettings dateSettings = dttFechaAtencion.getSettings();
		dateSettings.setVetoPolicy(new DateVetoPolicy() {
			
			@Override
			public boolean isDateAllowed(LocalDate date) {
				if(date.getYear() >= 2019) {
					if(date.isAfter(today) || date.isBefore(today.minusDays(90))){
						return false;
					}else if((VariableSesion.getInstanciaVariablesSession().getUsuario() != null)){
						return !cierreatencionesService.findOneByUsuarioAnioMesCerrado(VariableSesion.getInstanciaVariablesSession().getUsuario().getId(), String.valueOf(date.getYear()), date.getMonthValue());
					}
					return true;
				}else {
					return false;
				}
				
			}
		});
		
	}
    public void loadEntidad(){
        if(VariableSesion.getInstanciaVariablesSession().getEstablecimientoSalud() != null 
    		&&
    		VariableSesion.getInstanciaVariablesSession().getEstablecimientoSalud().getTipoestablecimientoId() != null
    		&&
    		VariableSesion.getInstanciaVariablesSession().getEstablecimientoSalud().getTipoestablecimientoId().getTipoestablecimientoId().getId().equals(56)){
        	btnCambioDpa.setVisible(true);
        }else{
        	btnCambioDpa.setVisible(false);
        }
    }
    private void changeDPA(){
    	JDialog dialog = new JDialog();
    	dialog.setSize(850, 150);
    	dialog.setLocationRelativeTo(null);
    	dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    	dialog.setVisible(true);
    	dialog.add(cambioDPAFrame);

    }
}
