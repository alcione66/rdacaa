package ec.gob.msp.rdacaa.reporte.model;

import java.util.Date;

import ec.gob.msp.rdacaa.business.entity.Atencionmedica;
import ec.gob.msp.rdacaa.business.entity.Intraextramural;
import ec.gob.msp.rdacaa.business.entity.Persona;
import lombok.Getter;
import lombok.Setter;

public class CaptacionTardiaReporte {
	
	@Getter
	@Setter
	private Atencionmedica atencionMedica;
	@Getter
	@Setter
	private Persona persona;
	@Getter
	@Setter
	private int vacunaIPV;
	@Getter
	@Setter
	private int vacunaFiebreAmarilla;
	@Getter
	@Setter
	private int vacunaSRP;
	@Getter
	@Setter
	private int vacunaDPT;
	@Getter
	@Setter
	private int vacunaDTPediatrica;
	@Getter
	@Setter
	private int vacunadTAdulto;
	@Getter
	@Setter
	private int vacunaSR;
	@Getter
	@Setter
	private int vacunaHBPediatrica;
	@Getter
	@Setter
	private int vacunaHBAdulto;
	@Getter
	@Setter
	private int vacunafIPV;
	@Getter
	@Setter
	private int vacunaSRPDos;
	@Getter
	@Setter
	private int vacunaOPV;
	@Getter
	@Setter
	private int vacunaDPTDos;
	@Getter
	@Setter
	private int vacunaDTPediatricaDos;
	@Getter
	@Setter
	private int vacunadTAdultoDos;
	@Getter
	@Setter
	private int vacunaSRDos;
	@Getter
	@Setter
	private int vacunaHBPediatricaDos;
	@Getter
	@Setter
	private int vacunaHBAdultoDos;
	@Getter
	@Setter
	private int vacunafIPVDos;
	@Getter
	@Setter
	private int vacunabOPV;
	@Getter
	@Setter
	private int vacunaOPVDos;
	@Getter
	@Setter
	private int vacunaDPTTres;
	@Getter
	@Setter
	private int vacunaDTPediatricaTres;
	@Getter
	@Setter
	private int vacunadTAdultoTres;
	@Getter
	@Setter
	private int vacunaHBPediatricaTres;
	@Getter
	@Setter
	private int vacunaHBAdultoTres;
	@Getter
	@Setter
	private int vacunabOPVDos;
	@Getter
	@Setter
	private int vacunaOPVTres;
	@Getter
	@Setter
	private int vacunaDPTCuatro;
	@Getter
	@Setter
	private int vacunaDTPediatricaCuatro;
	@Getter
	@Setter
	private int vacunadTAdultoCuatro;
	@Getter
	@Setter
	private int vacunabOPVTres;
	@Getter
	@Setter
	private int vacunaDTPediatricaCinco;
	@Getter
	@Setter
	private int vacunadTAdultoCinco;
	@Getter
	@Setter
	private int vacunadTAdultoSeis;
	@Getter
	@Setter
	private Intraextramural intraextramural;
	@Getter
	@Setter
	private Date fechaColocacionVacuna;

}
