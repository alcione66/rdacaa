/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.admision.model;

import ec.gob.msp.rdacaa.business.entity.Parroquia;
import ec.gob.msp.rdacaa.control.swing.model.CustomObjectToStringConverter;

/**
 *
 * @author dmurillo
 */
public class ParroquiaToStringConverter extends CustomObjectToStringConverter {

    @Override
    public String getPreferredStringForItem(Object o) {
        if (o == null) {
            return null;
        } else if (o instanceof Parroquia) {
            return ((Parroquia) o).getDescripcion();
        } else {
            return o.toString();
        }
    }

}
