package ec.gob.msp.rdacaa.reporte.utilitario;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.swing.JTable;

import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Element;
import com.itextpdf.text.Font;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfNumber;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfPage;
import com.itextpdf.text.pdf.PdfPageEventHelper;
import com.itextpdf.text.pdf.PdfWriter;

import ec.gob.msp.rdacaa.business.entity.Detallecatalogo;
import ec.gob.msp.rdacaa.business.entity.Entidad;
import ec.gob.msp.rdacaa.business.entity.Usuario;
import ec.gob.msp.rdacaa.utilitario.enumeracion.ComunEnum;
import ec.gob.msp.rdacaa.utilitario.fecha.FechasUtil;
import jxl.Workbook;
import jxl.format.Alignment;
import jxl.write.Label;
import jxl.write.WritableCellFormat;
import jxl.write.WritableFont;
import jxl.write.WritableImage;
import jxl.write.WritableSheet;
import jxl.write.WritableWorkbook;
import jxl.write.WriteException;
import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author dmurillo
 */
public class Exporter {
	@Getter
	@Setter
	private File file;
	@Getter
	@Setter
	private List<JTable> tabla;
	@Getter
	@Setter
	private List<String> nomFiles;
	@Getter
	@Setter
	private String elaborado;
	@Getter
	@Setter
	private Date fecha;
	@Getter
	@Setter
	private File logo;
	@Getter
	@Setter
	private Entidad entidad;
	@Getter
	@Setter
	private Usuario usuario ;
	@Getter
	@Setter
	private Detallecatalogo especialidad;
	@Getter
	@Setter
	private String establecimiento;
	@Getter
	@Setter
	private String tituloReporte;
	@Getter
	@Setter
	private String fechas;
	
	public class Rotate extends PdfPageEventHelper {
		 
        protected PdfNumber orientation = PdfPage.LANDSCAPE;
 
        public void setOrientation(PdfNumber orientation) {
            this.orientation = orientation;
        }
    }

	public boolean export() {

		try {
			DataOutputStream out = new DataOutputStream(new FileOutputStream(file));
			WritableWorkbook w = Workbook.createWorkbook(out);
			JTable table = tabla.get(0);
			WritableSheet s = w.createSheet(nomFiles.get(0), 0);
			int pie = 0;
			WritableImage writableImage = new WritableImage(0, 0, 3, 3, logo);
			
			Label newCellTitulo = new Label(7,1,String.valueOf("MINISTERIO DE SALUD PÚBLICA"));
			Label newCellCoordinacion = new Label(7,2,String.valueOf("COORDINACIÓN GENERAL DE PLANIFICACIÓN Y GESTIÓN ESTRATÉGICA"));
			Label newCellDireccion = new Label(7,3,String.valueOf("DIRECCIÓN NACIONAL DE ESTADÍSTICA Y ANÁLISIS DE INFORMACIÓN DE SALUD"));
			Label newCellTituloReporte = new Label(7,4,tituloReporte);
			Label newCellFechas = new Label(7,5,fechas);
			
			WritableFont headerFont = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
			WritableCellFormat headerFormat = new WritableCellFormat(headerFont);
			headerFormat.setAlignment(Alignment.CENTRE);
			newCellTitulo.setCellFormat(headerFormat);
			newCellCoordinacion.setCellFormat(headerFormat);
			newCellDireccion.setCellFormat(headerFormat);
			newCellTituloReporte.setCellFormat(headerFormat);
			newCellFechas.setCellFormat(headerFormat);
			s.addImage(writableImage);
			s.addCell(newCellTitulo);
			s.addCell(newCellCoordinacion);
			s.addCell(newCellDireccion);
			s.addCell(newCellTituloReporte);
			s.addCell(newCellFechas);
			
			// Para que salga el titulo de las columnas
			for (int i = 0; i < table.getColumnCount(); i++) {
				for (int j = 0; j < table.getRowCount(); j++) {
					Object titulo = table.getColumnName(i);
					s.addCell(new Label(i + 1, j + 7, String.valueOf(titulo)));
				}
			}
			for (int i = 0; i < table.getColumnCount(); i++) {
				for (int j = 0; j < table.getRowCount(); j++) {
					Object object = table.getValueAt(j, i);
					s.addCell(new Label(i + 1, j + 8, String.valueOf(object)));
					pie = j + 8;
				}
			}
			Label newTitFuente = new Label(1,pie + 4,String.valueOf("FUENTE:"));
			Label newTitAutor = new Label(1,pie + 5,String.valueOf("ELABORADOR POR:"));
			Label newTitFcElab = new Label(1,pie + 6,String.valueOf("FECHA DE ELABORACION:"));
			Label newTitEstab = new Label(1,pie + 7,String.valueOf("ESTABLECIMIENTO:"));
			
			
			WritableFont pieFont = new WritableFont(WritableFont.ARIAL, 10, WritableFont.BOLD);
			WritableCellFormat pieFormat = new WritableCellFormat(pieFont);
			pieFormat.setAlignment(Alignment.LEFT);
			
			newTitFuente.setCellFormat(pieFormat);
			newTitAutor.setCellFormat(pieFormat);
			newTitFcElab.setCellFormat(pieFormat);
			newTitEstab.setCellFormat(pieFormat);
			
			s.addCell(newTitFuente);
			s.addCell(new Label(2, pie + 4, String.valueOf("RDACAA 2,0")));
			s.addCell(newTitAutor);
			s.addCell(new Label(2, pie + 5, String.valueOf(elaborado)));
			s.addCell(newTitFcElab);
			s.addCell(new Label(2, pie + 6, FechasUtil.formateadorfechaAString(ComunEnum.PATRON_FECHA6.getDescripcion(), fecha)));
			s.addCell(newTitEstab);
			s.addCell(new Label(2, pie + 7, entidad.getId().toString()));
			s.addCell(new Label(3, pie + 7, entidad.getNombreoficial()));
			
			
			w.write();
			w.close();
			return true;
		}

		catch (IOException | WriteException e) {
			return false;
		}
	}
	
	public boolean exportPdf() {
		Boolean exp = false;
		final Font tituloReporteFont = new Font(Font.FontFamily.HELVETICA, 6, Font.BOLD);
		final Font tituloTabletFont = new Font(Font.FontFamily.HELVETICA, 4, Font.BOLD);
		final Font detalleFont = new Font(Font.FontFamily.HELVETICA, 3);
		Document document = new Document(PageSize.A4.rotate());
		try {
			PdfWriter.getInstance(document, new FileOutputStream(file));
			document.open();
			document.setMargins(0, 0, 5, 5);
			
			/**
			 * TITULO
			 */
			
			Image img = Image.getInstance(logo.getPath());
			img.scalePercent(50F);
			img.setAlignment(Image.LEFT);
			
			Paragraph titulologo = new Paragraph(); 
			Paragraph tituloMsp = new Paragraph("Ministerio de Salud Pública", tituloReporteFont);
			tituloMsp.setAlignment(Element.ALIGN_CENTER);
			Paragraph tituloCoordinacion = new Paragraph("Coordinación General de Planificación", tituloReporteFont);
			tituloCoordinacion.setAlignment(Element.ALIGN_CENTER);
			Paragraph tituloDireccion = new Paragraph("Dirección Nacional de Estadística y Análisis de Información de Salud", tituloReporteFont);
			tituloDireccion.setAlignment(Element.ALIGN_CENTER);
			Paragraph titulo = new Paragraph("REGISTRO DIARIO AUTOMATIZADO DE CONSULTAS Y ATENCIONES AMBULATORIAS (RDACAA)", tituloReporteFont);
			titulo.setAlignment(Element.ALIGN_CENTER);
			Paragraph espacio = new Paragraph(" ", tituloReporteFont);
			
			titulologo.add(img);
			document.add(titulologo);
			document.add(tituloMsp);
			document.add(tituloCoordinacion);
			document.add(tituloDireccion);
			document.add(titulo);
			document.add(espacio);
			
			/**
			 * CABECERA
			 * tabla.get(0).getColumnCount()
			 */
			tabla.get(0).removeColumn(tabla.get(0).getColumnModel().getColumn(0));
			PdfPTable table = new PdfPTable(38); 
			table.setWidthPercentage(100);
			PdfPCell columnHeader;
			
			columnHeader = new PdfPCell(new Phrase("BLOQUE A: Datos Generales del Establecimiento de Salud",tituloTabletFont));
			columnHeader.setColspan(11);
			table.addCell(columnHeader);
			
			columnHeader = new PdfPCell(new Phrase("BLOQUE B: Datos del Profesional",tituloTabletFont));
			columnHeader.setColspan(31);
			table.addCell(columnHeader);
            
			columnHeader = new PdfPCell(new Phrase("Fecha:",tituloTabletFont));
			columnHeader.setColspan(3);
			table.addCell(columnHeader);
			
			columnHeader = new PdfPCell(new Phrase(FechasUtil.formateadorfechaAString(ComunEnum.PATRON_FECHA6.getDescripcion(), new Date()),detalleFont));
			columnHeader.setColspan(8);
			table.addCell(columnHeader);
			
			columnHeader = new PdfPCell(new Phrase("Nombres y Apellidos:",tituloTabletFont));
			columnHeader.setColspan(2);
			table.addCell(columnHeader);
			columnHeader = new PdfPCell(new Phrase(elaborado,detalleFont));
			columnHeader.setColspan(5);
			table.addCell(columnHeader);
			columnHeader = new PdfPCell(new Phrase("Sexo:",tituloTabletFont));
			columnHeader.setColspan(2);
			table.addCell(columnHeader);
			columnHeader = new PdfPCell(new Phrase( usuario.getPersonaId().getCtsexoId() != null ? usuario.getPersonaId().getCtsexoId().getDescripcion() : "" ,detalleFont));
			columnHeader.setColspan(3);
			table.addCell(columnHeader);
			columnHeader = new PdfPCell(new Phrase("Fecha Nac:",tituloTabletFont));
			columnHeader.setColspan(2);
			table.addCell(columnHeader);
			columnHeader = new PdfPCell(new Phrase(FechasUtil.formateadorfechaAString(ComunEnum.PATRON_FECHA6.getDescripcion(), usuario.getPersonaId().getFechanacimiento()),detalleFont));
			columnHeader.setColspan(3);
			table.addCell(columnHeader);
			columnHeader = new PdfPCell(new Phrase("Formación Prof:",tituloTabletFont));
			columnHeader.setColspan(2);
			table.addCell(columnHeader);
			columnHeader = new PdfPCell(new Phrase("",detalleFont));
			columnHeader.setColspan(3);
			table.addCell(columnHeader);
			columnHeader = new PdfPCell(new Phrase("Especialidad:",tituloTabletFont));
			columnHeader.setColspan(2);
			table.addCell(columnHeader);
			columnHeader = new PdfPCell(new Phrase(especialidad.getDescripcion(),detalleFont));
			columnHeader.setColspan(4);
			table.addCell(columnHeader);
			
			columnHeader = new PdfPCell(new Phrase("Establecimiento de Salud:",tituloTabletFont));
			columnHeader.setColspan(2);
			table.addCell(columnHeader);
			columnHeader = new PdfPCell(new Phrase(entidad.getNombreoficial(),detalleFont));
			columnHeader.setColspan(3);
			table.addCell(columnHeader);
			columnHeader = new PdfPCell(new Phrase("Tipo:",tituloTabletFont));
			columnHeader.setColspan(2);
			table.addCell(columnHeader);
			columnHeader = new PdfPCell(new Phrase("",detalleFont));
			columnHeader.setColspan(4);
			table.addCell(columnHeader);
			
			columnHeader = new PdfPCell(new Phrase("Nacionalidad:",tituloTabletFont));
			columnHeader.setColspan(2);
			table.addCell(columnHeader);
			columnHeader = new PdfPCell(new Phrase(usuario.getPersonaId().getPaisId() != null ? usuario.getPersonaId().getPaisId().getDescripcion() : "",detalleFont));
			columnHeader.setColspan(5);
			table.addCell(columnHeader);
			columnHeader = new PdfPCell(new Phrase("Autoidentificación:",tituloTabletFont));
			columnHeader.setColspan(2);
			table.addCell(columnHeader);
			columnHeader = new PdfPCell(new Phrase("",detalleFont));
			columnHeader.setColspan(3);
			table.addCell(columnHeader);
			columnHeader = new PdfPCell(new Phrase("Cédula:",tituloTabletFont));
			columnHeader.setColspan(2);
			table.addCell(columnHeader);
			columnHeader = new PdfPCell(new Phrase(usuario.getPersonaId().getNumeroidentificacion(),detalleFont));
			columnHeader.setColspan(3);
			table.addCell(columnHeader);
			columnHeader = new PdfPCell(new Phrase("Código MSP:",tituloTabletFont));
			columnHeader.setColspan(2);
			table.addCell(columnHeader);
			columnHeader = new PdfPCell(new Phrase("",detalleFont));
			columnHeader.setColspan(3);
			table.addCell(columnHeader);
			columnHeader = new PdfPCell(new Phrase("Firma:",tituloTabletFont));
			columnHeader.setColspan(2);
			table.addCell(columnHeader);
			columnHeader = new PdfPCell(new Phrase("",detalleFont));
			columnHeader.setColspan(4);
			table.addCell(columnHeader);
			
			columnHeader = new PdfPCell(new Phrase("BLOQUE C: Datos del Paciente",tituloTabletFont));
			columnHeader.setColspan(11);
			table.addCell(columnHeader);
			
			columnHeader = new PdfPCell(new Phrase("BLOQUE D: Datos de Consulta/Atención",tituloTabletFont));
			columnHeader.setColspan(31);
			table.addCell(columnHeader);
			
			/**
			 * DETALLE
			 * tabla.get(0).getColumnCount()
			 */
			
            Paragraph subPara = new Paragraph("", tituloTabletFont);
     
			
			for (int column = 0; column < 38; column++) {  
					columnHeader = new PdfPCell(new Phrase(tabla.get(0).getColumnName(column),tituloTabletFont));
	                columnHeader.setHorizontalAlignment(Element.ALIGN_CENTER);
	                columnHeader.setVerticalAlignment(15);
	                columnHeader.setRotation(90);
	                table.addCell(columnHeader);
            }
            table.setHeaderRows(1);
            PdfPCell rowHeader;
            for (int row = 0; row < tabla.get(0).getRowCount(); row++) {                
                for (int column = 0; column < 38; column++) { 
            		rowHeader = new PdfPCell(new Phrase(tabla.get(0).getValueAt(row, column).toString(),detalleFont));
                    table.addCell(rowHeader);
                }
            }
            
            subPara.add(table);
            document.add(subPara);
			document.close();
            exp = true;
		}catch (DocumentException | IOException e) {
            exp = false;
        }
		return exp;
	}
}
