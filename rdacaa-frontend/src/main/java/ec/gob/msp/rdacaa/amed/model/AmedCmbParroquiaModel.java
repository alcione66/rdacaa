/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.amed.model;

import ec.gob.msp.rdacaa.business.entity.Parroquia;
import ec.gob.msp.rdacaa.control.swing.model.DefaultComboBoxModel;
import org.springframework.stereotype.Component;

/**
 *
 * @author dmurillo
 */
@Component
public class AmedCmbParroquiaModel extends DefaultComboBoxModel<Parroquia> {
    
    private static final long serialVersionUID = -1665669663319629669L;
    
}
