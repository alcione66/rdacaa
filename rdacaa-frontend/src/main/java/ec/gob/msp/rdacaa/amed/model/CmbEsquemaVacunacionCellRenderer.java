/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.amed.model;

import ec.gob.msp.rdacaa.business.entity.Esquemavacunacion;
import java.awt.Component;
import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;

/**
 *
 * @author eduardo
 */
public class CmbEsquemaVacunacionCellRenderer extends DefaultListCellRenderer{
    
    @Override
    public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
        if (value instanceof Esquemavacunacion) {
            Esquemavacunacion esqvac = (Esquemavacunacion) value;
            value = esqvac.getDosis();
        }
        
        return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
    }
}
