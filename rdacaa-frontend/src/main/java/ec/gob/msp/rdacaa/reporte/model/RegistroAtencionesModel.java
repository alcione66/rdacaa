package ec.gob.msp.rdacaa.reporte.model;

import java.util.List;

import ec.gob.msp.rdacaa.business.entity.Atenciongprioritario;
import ec.gob.msp.rdacaa.business.entity.Atencionmedica;
import ec.gob.msp.rdacaa.business.entity.Diagnosticoatencion;
import ec.gob.msp.rdacaa.business.entity.Entidad;
import ec.gob.msp.rdacaa.business.entity.Estrategiapaciente;
import ec.gob.msp.rdacaa.business.entity.Intraextramural;
import ec.gob.msp.rdacaa.business.entity.Persona;
import ec.gob.msp.rdacaa.business.entity.VariablesDatosAntropometricos;
import ec.gob.msp.rdacaa.business.entity.VariablesDatosObstetricos;
import ec.gob.msp.rdacaa.business.entity.VariablesExamenLaboratorio;
import ec.gob.msp.rdacaa.business.entity.VariablesGruposVulnerables;
import ec.gob.msp.rdacaa.business.entity.VariablesRefInt;
import ec.gob.msp.rdacaa.business.entity.VariablesVih;
import ec.gob.msp.rdacaa.business.entity.VariablesViolencia;
import lombok.Getter;
import lombok.Setter;

public class RegistroAtencionesModel {
	@Getter
	@Setter
	private Persona persona;
	@Getter
	@Setter
	private Atencionmedica atencionMedica;
	@Getter
	@Setter
	private List<Atenciongprioritario> gruposPrioritarios;
	@Getter
	@Setter
	private List<Diagnosticoatencion> diagnostico;
	@Getter
	@Setter
	private VariablesRefInt variablesRefInt;
	@Getter
	@Setter
	private VariablesDatosAntropometricos variablesDatosAntropometricos;
	@Getter
	@Setter
	private VariablesDatosObstetricos variablesDatosObstetricos;
	@Getter
	@Setter
	private VariablesViolencia variablesViolencia;
	@Getter
	@Setter
	private VariablesVih variablesVih;
	@Getter
	@Setter
	private VariablesGruposVulnerables variablesGruposVulnerables;
	@Getter
	@Setter
	private Intraextramural intraextramural;
	@Getter
	@Setter
	private Estrategiapaciente estrategiapaciente;
	@Getter
	@Setter
	private Entidad entidad;
	@Getter
	@Setter
	private VariablesExamenLaboratorio variablesExamenLaboratorio;
	
}
