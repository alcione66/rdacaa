package ec.gob.msp.rdacaa.admision.model;

import ec.gob.msp.rdacaa.business.entity.Parroquia;
import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;


public class CmbParroquiaCellRenderer extends DefaultListCellRenderer {

    private static final long serialVersionUID = -2754088130017285177L;

    @Override
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
			boolean cellHasFocus) {
		if (value instanceof Parroquia) {
			Parroquia parroquia = (Parroquia) value;
			value = parroquia.getDescripcion();
		}
		return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
	}
}