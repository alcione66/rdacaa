package ec.gob.msp.rdacaa.reporte.controller;

import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;

import com.github.lgooddatepicker.components.DatePicker;

import ec.gob.msp.rdacaa.business.entity.Atencionmedica;
import ec.gob.msp.rdacaa.business.entity.Registrovacunacion;
import ec.gob.msp.rdacaa.business.entity.Vacuna;
import ec.gob.msp.rdacaa.business.service.AtencionmedicaService;
import ec.gob.msp.rdacaa.business.service.RegistrovacunacionService;
import ec.gob.msp.rdacaa.business.service.VacunaService;
import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.reporte.forma.RegistroVacunasDosis;
import ec.gob.msp.rdacaa.reporte.model.VacunasDosisReporte;
import ec.gob.msp.rdacaa.reporte.model.VacunasDosisTablaModel;
import ec.gob.msp.rdacaa.reporte.model.VacunasModel;
import ec.gob.msp.rdacaa.reporte.utilitario.ReportesUtil;
import ec.gob.msp.rdacaa.utilitario.forma.FormatoTabla;
import ec.gob.msp.rdacaa.utilitario.forma.UtilitarioForma;

@Controller
@Lazy(value = false)
public class VacunasDosisController  {

	private static final Logger logger = LoggerFactory.getLogger(VacunasDosisController.class);
	private RegistroVacunasDosis vegistroVacunasDosisFrame;
	private AtencionmedicaService atencionmedicaService;
	private VacunaService vacunaService;
	private RegistrovacunacionService registrovacunacionService;
	private JButton btnBuscar;
	private JButton btnDescargar;
	private JTable tblReporte;
	private DatePicker dttDesde;
	private DatePicker dttHasta;
	private List<VacunasDosisReporte> controles;
	private VacunasDosisTablaModel modeloTabla;

	
	@Autowired
	public VacunasDosisController(RegistroVacunasDosis vegistroVacunasDosisFrame,
									 AtencionmedicaService atencionmedicaService,
									 VacunaService vacunaService,
									 RegistrovacunacionService registrovacunacionService) {
		this.vegistroVacunasDosisFrame = vegistroVacunasDosisFrame;
		this.atencionmedicaService = atencionmedicaService;
		this.vacunaService = vacunaService;
		this.registrovacunacionService = registrovacunacionService;
	}

	@PostConstruct
	private void init() {
		this.dttDesde = vegistroVacunasDosisFrame.getDttDesde();
		this.dttHasta = vegistroVacunasDosisFrame.getDttHasta();
		this.btnBuscar = vegistroVacunasDosisFrame.getBtnBuscar();
		this.btnDescargar = vegistroVacunasDosisFrame.getBtnDescargar();
		this.tblReporte = vegistroVacunasDosisFrame.getTblReporte();
		
		btnBuscar.addActionListener(e -> consultar());
		btnDescargar.addActionListener(e -> exportar());
		cargarTabla();
		vegistroVacunasDosisFrame.addInternalFrameListener( new  InternalFrameAdapter() {
			@Override
			public void internalFrameClosing(InternalFrameEvent e) {
				super.internalFrameClosing(e);
				cerrar();
			}
		});
	}

	private void consultar() {
		List<VacunasModel> infoVacunas;
		controles = new ArrayList<>();
		if (dttDesde.getDate() != null && dttHasta.getDate() != null) {
			List<Atencionmedica> atenciones = atencionmedicaService.findAllAtencionesByDates(VariableSesion.getInstanciaVariablesSession().getProfesional().getId(), Date.from(dttDesde.getDate().atStartOfDay(ZoneId.systemDefault()).toInstant()),
					  Date.from(dttHasta.getDate().atStartOfDay(ZoneId.systemDefault()).toInstant()),
						VariableSesion.getInstanciaVariablesSession().getCtEspecialidad().getId());
			if(!atenciones.isEmpty()){
				controles = new ArrayList<>();
				infoVacunas = new ArrayList<>();
				for (Atencionmedica atencion : atenciones) {
					VacunasModel vm = new VacunasModel();
					vm.setAtencionMedica(atencion);
					List<Registrovacunacion> rv = registrovacunacionService.findAllRegistroVacunaByAtencionMedica(atencion.getId());
					if(!rv.isEmpty()){
						vm.setRegistros(rv);
					}
					infoVacunas.add(vm);
				}
				
				int[][] valor = new int[20][6];
				
				for (VacunasModel vacunaModel : infoVacunas) {
					if(vacunaModel.getRegistros() != null){
						for (Registrovacunacion registroVacunacion : vacunaModel.getRegistros()) {
							for (int i = 0; i < valor.length; i++) {
								for (int j = 0; j < 6; j++) {
									if(registroVacunacion.getEsquemavacunacionId().getVacunaId().getId() == (i + 1) && Integer.valueOf(registroVacunacion.getEsquemavacunacionId().getDosis()) == (j + 1)){
										int cant = valor[i][j];
										cant++;
										valor[i][j] = cant;
									}
								}
								
							}
						}
					}
					
				}
				List<Vacuna> vacunas = vacunaService.findAllOrderedByDescripcion(); 
				for (Vacuna vacuna : vacunas) {
					VacunasDosisReporte vr = new VacunasDosisReporte();
					vr.setDescripcion(vacuna.getNombrevacuna());
					for (int i = 0; i < valor.length; i++) {
						if(vacuna.getId() == (i+1)){
							vr.setDosis1(valor[i][0]);
							vr.setDosis2(valor[i][1]);
							vr.setDosis3(valor[i][2]);
							vr.setDosis4(valor[i][3]);
							vr.setDosis5(valor[i][4]);
							vr.setDosis6(valor[i][5]);
							break;
						}
					}
					controles.add(vr);
				}
				cargarTabla();
			}else{
				UtilitarioForma.muestraMensaje("Consulta", vegistroVacunasDosisFrame, "INFO", "No existen resultados para los parámetros ingresados");
			}
		}
	}

	private void cargarTabla() {
		tblReporte.setDefaultRenderer(Object.class, new FormatoTabla());
		String[] columnNames = { "", 
								 "VACUNA",
								 "Dosis 1", 
								 "Dosis 2", 
								 "Dosis 3", 
								 "Dosis 4",
								 "Dosis 5",
								 "Dosis 6",
								 };

		modeloTabla = new VacunasDosisTablaModel(columnNames);
		controles = controles == null ? new ArrayList<>() : controles;
		modeloTabla.addFila(controles);
		tblReporte.setModel(modeloTabla);
		tblReporte.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		modeloTabla.ocultaColumnaCodigo(tblReporte);
		modeloTabla.definirAnchoColumnas(tblReporte, new int[] {400,400,400,400,400,400,400});

	}

	private void exportar() {
            String autor = VariableSesion.getInstanciaVariablesSession().getUsuario().getPersonaId().getPrimernombre() + " ";
         	   autor += VariableSesion.getInstanciaVariablesSession().getUsuario().getPersonaId().getPrimerapellido() + " ";
            StringBuilder fechas = new StringBuilder();
	    fechas.append("DESDE: ").append(dttDesde.getDateStringOrEmptyString()).append(" HASTA: ").append(dttHasta.getDateStringOrEmptyString());
            ReportesUtil.exportarXlsReportes(tblReporte, "Vacunas por Dosis", autor, "xls","Produccion por Dosis",fechas.toString());
	}
	
	private void cerrar(){
		
		dttDesde.setText("");
		dttHasta.setText("");
		if(modeloTabla != null){
			modeloTabla.clear();
		}
		
	}
}
