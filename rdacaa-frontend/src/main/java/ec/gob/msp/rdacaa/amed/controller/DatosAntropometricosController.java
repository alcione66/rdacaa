/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.amed.controller;

import java.awt.Color;
import java.awt.GridBagConstraints;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.swing.Box.Filler;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JRadioButton;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import ec.gob.msp.rdacaa.amed.forma.DatosAntropometricos;
import ec.gob.msp.rdacaa.business.entity.Medicionpaciente;
import ec.gob.msp.rdacaa.business.service.MedicionPacienteAlertasService;
import ec.gob.msp.rdacaa.business.service.MedicionpacienteService;
import ec.gob.msp.rdacaa.business.service.ScoreZAndCategoriaService;
import ec.gob.msp.rdacaa.business.service.ScoreZAndCategoriaService.PercentilDetalleNotFoundException;
import ec.gob.msp.rdacaa.business.service.ScoreZAndCategoriaService.PercentilNotFoundException;
import ec.gob.msp.rdacaa.business.service.SignosVitalesReglasValidacion;
import ec.gob.msp.rdacaa.business.service.SignosVitalesReglasValidacionNotFoundException;
import ec.gob.msp.rdacaa.business.service.SignosvitalesalertasService;
import ec.gob.msp.rdacaa.business.validation.HBRiesgoCorregidoValidator;
import ec.gob.msp.rdacaa.business.validation.HBRiesgoValidator;
import ec.gob.msp.rdacaa.business.validation.IndiceMasaCorporalValidator;
import ec.gob.msp.rdacaa.business.validation.PesoValidator;
import ec.gob.msp.rdacaa.business.validation.TallaValidator;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResultCatalog;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.business.validation.common.Validator;
import ec.gob.msp.rdacaa.control.swing.notifications.Notifications;
import ec.gob.msp.rdacaa.control.swing.personalizado.CajaTextoIess;
import ec.gob.msp.rdacaa.control.swing.personalizado.LabelIess;
import ec.gob.msp.rdacaa.control.swing.personalizado.TituloEtiquetaIess;
import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.utilitario.forma.UtilitarioForma;
import ec.gob.msp.rdacaa.utilitario.medico.UtilitarioMedico;

/**
 *
 * @author Eduardo Garcia
 * @author Saulo Velasco
 */
@Controller
public class DatosAntropometricosController {
	
	private final Logger logger = LoggerFactory.getLogger(DatosAntropometricosController.class);

	private static final int TALLA_MAXIMA = 300;

	private static final int TALLA_MINIMA = 38;

	private static final double PESO_MAXIMO = 500;

	private static final double PESO_MINIMO = 0.5d;

	private static final String NO_APLICA = "No aplica";

	private static final int TALLA_ACOSTADO = 1;

	private static final int TALLA_DE_PIE = 0;

	private static final int IS_EMBARAZADA_GRUPO_PRIORITARIO_ID = 634;

	private static final int EDAD_SEIS_MES_CERO_DIA = 600;
	
	private static final int EDAD_UN_ANIO_SEIS_MES_UN_DIA = 10601;
	private static final int EDAD_DOS_ANIO_CERO_MES_UN_DIA = 20001;
	private static final int EDAD_CINCO_ANIO_UN_MES_CERO_DIA = 50100;
	
	private static final int EDAD_DIEZ_Y_NUEVE_ANIO_CERO_MES_CERO_DIA = 190000;
	
	private static final int EDAD_CINCO_ANIO_CERO_MES_CERO_DIA = 50000;
	
	private static final int EDAD_DIEZ_ANIO_CERO_MES_CERO_DIA = 100000;
	
	private static final String LABEL_PESO_OBLIGATORIO = "<html><span style=\"color: #FF9933;\">*</span>Peso (kg)</html>";
	private static final String LABEL_PESO_NO_OBLIGATORIO = "Peso (kg)";
	private static final String LABEL_TALLA_OBLIGATORIO = "<html><span style=\"color: #FF9933;\">*</span>Talla (cm)</html>";
	private static final String LABEL_TALLA_NO_OBLIGATORIO = "Talla (cm)";
	private static final String LABEL_PERIMETRO_CEFALICO_OBLIGATORIO = "<html><span style=\"color: #FF9933;\">*</span>Perímetro Cefálico <br/>(cm)</html>";
	private static final String LABEL_PERIMETRO_CEFALICO_NO_OBLIGATORIO = "<html>Perímetro Cefálico <br/>(cm)</html>";

	protected static final double PERIMETRO_CEFALICO_MINIMO = 20.0;

	protected static final double PERIMETRO_CEFALICO_MAXIMO = 54.0;


	private DatosAntropometricos datosAntropometricosPanel;
	private MedicionpacienteService medicionpacienteService;
	private MedicionPacienteAlertasService medicionPacienteAlertasService;
	private ScoreZAndCategoriaService scoreZAndCategoriaService;
	private SignosvitalesalertasService signosvitalesalertasService;
	private PesoValidator pesoValidator;
	private TallaValidator tallaValidator;
	private IndiceMasaCorporalValidator imcValidator;
	private HBRiesgoValidator hbRiesgoValidator;
	private HBRiesgoCorregidoValidator hbRiesgoCorregidoValidator;
	private CajaTextoIess txtPesoPaciente;
	private CajaTextoIess txtTallaPaciente;
	private CajaTextoIess txtTallaCorregidaPaciente;
	private CajaTextoIess txtPerimetroCefalico;
	private CajaTextoIess txtIndiceMasaCorporal;
	private CajaTextoIess txtValorHBRiesgo;
	private CajaTextoIess txtValorHBRiesgoCorregido;
	private CajaTextoIess txtScoreZTallaParaEdad;
	private CajaTextoIess txtCategoriaTallaParaEdad;
	private CajaTextoIess txtScoreZPesoParaEdad;
	private CajaTextoIess txtCategoriaPesoParaEdad;
	private CajaTextoIess txtScoreZPesoParaLongitudTalla;
	private CajaTextoIess txtCategoriaPesoParaLongitudTalla;
	private CajaTextoIess txtScoreZIMCParaEdad;
	private CajaTextoIess txtCategoriaIMCParaEdad;
	private CajaTextoIess txtScoreZPerimetroCefalicoParaEdad;
	private CajaTextoIess txtCategoriaPerimetroCefalicoParaEdad;
	
	private JPanel panelScoreZConatiner;

	private JRadioButton rbAcostado;
	private JRadioButton rbDepie;

	private TituloEtiquetaIess lblPeso;
	private TituloEtiquetaIess lblTalla;
	private TituloEtiquetaIess lblTallaCorregida;
	private TituloEtiquetaIess lblPerimetroCefalico;
	private TituloEtiquetaIess lblScoreZPerimetroCefalico;
	
	private TituloEtiquetaIess lblValorHB;
	private TituloEtiquetaIess lblValorHBCorregido;
	private JPanel panelHBFactor;
	
	private LabelIess lblAltura;
	private LabelIess lblFactor;
	private JPanel panelNotificaciones;
	private JLabel lblDatosBioquimicos;
	
	private TituloEtiquetaIess lblTallaParaEdad;
	private TituloEtiquetaIess lblPesoParaEdad;
	private TituloEtiquetaIess lblPesoParaLongitudTalla;
	private TituloEtiquetaIess lblIMCParaEdad;

	List<ValidationResult> alertasHBRiesgo;

	private VariableSesion variableSesion;

	private boolean hbHasError = false;
	private boolean imcHasError = false;

	@Autowired
	public DatosAntropometricosController(DatosAntropometricos datosAntropometricosPanel,
			MedicionpacienteService medicionpacienteService,
			MedicionPacienteAlertasService medicionPacienteAlertasService,
			ScoreZAndCategoriaService scoreZAndCategoriaService,
			SignosvitalesalertasService signosvitalesalertasService, PesoValidator pesoValidator,
			TallaValidator tallaValidator, IndiceMasaCorporalValidator imcValidator,
			HBRiesgoValidator hbRiesgoValidator, HBRiesgoCorregidoValidator hbRiesgoCorregidoValidator) {
		this.datosAntropometricosPanel = datosAntropometricosPanel;
		this.medicionpacienteService = medicionpacienteService;
		this.medicionPacienteAlertasService = medicionPacienteAlertasService;
		this.scoreZAndCategoriaService = scoreZAndCategoriaService;
		this.signosvitalesalertasService = signosvitalesalertasService;
		this.pesoValidator = pesoValidator;
		this.tallaValidator = tallaValidator;
		this.imcValidator = imcValidator;
		this.hbRiesgoValidator = hbRiesgoValidator;
		this.hbRiesgoCorregidoValidator = hbRiesgoCorregidoValidator;
		this.variableSesion = VariableSesion.getInstanciaVariablesSession();
	}

	@PostConstruct
	private void init() {
		this.txtPesoPaciente = datosAntropometricosPanel.getTxtPesoPaciente();
		this.txtTallaPaciente = datosAntropometricosPanel.getTxtTallaPaciente();
		this.txtTallaCorregidaPaciente = datosAntropometricosPanel.getTxtTallaCorregidaPaciente();
		this.txtPerimetroCefalico = datosAntropometricosPanel.getTxtPerimetroCefalico();
		this.txtIndiceMasaCorporal = datosAntropometricosPanel.getTxtIndiceMasaCorporal();
		this.txtValorHBRiesgo = datosAntropometricosPanel.getTxtValorHBRiesgo();
		this.txtValorHBRiesgoCorregido = datosAntropometricosPanel.getTxtValorHBRiesgoCorregido();
		this.txtScoreZTallaParaEdad = datosAntropometricosPanel.getTxtScoreZTallaParaEdad();
		this.txtCategoriaTallaParaEdad = datosAntropometricosPanel.getTxtCategoriaTallaParaEdad();
		this.txtScoreZPesoParaEdad = datosAntropometricosPanel.getTxtScoreZPesoParaEdad();
		this.txtCategoriaPesoParaEdad = datosAntropometricosPanel.getTxtCategoriaPesoParaEdad();
		this.txtScoreZPesoParaLongitudTalla = datosAntropometricosPanel.getTxtPesoParaLongitudTalla();
		this.txtCategoriaPesoParaLongitudTalla = datosAntropometricosPanel.getTxtCategoriaPesoParaLongitudTalla();
		this.txtScoreZIMCParaEdad = datosAntropometricosPanel.getTxtIMCParaEdad();
		this.txtCategoriaIMCParaEdad = datosAntropometricosPanel.getTxtCategoriaIMCParaEdad();
		this.txtScoreZPerimetroCefalicoParaEdad = datosAntropometricosPanel.getTxtPerimetroCefalicoParaEdad();
		this.txtCategoriaPerimetroCefalicoParaEdad = datosAntropometricosPanel
				.getTxtCategoriaPerimetroCefalicoParaLongitudTalla();
		this.rbAcostado = datosAntropometricosPanel.getRbAcostado();
		this.rbDepie = datosAntropometricosPanel.getRbDepie();
		this.lblTallaCorregida = datosAntropometricosPanel.getLblTallaCorregida();
		this.lblAltura = datosAntropometricosPanel.getLblAltura();
		this.lblFactor = datosAntropometricosPanel.getLblFactor();
		this.panelNotificaciones = datosAntropometricosPanel.getPanelNotificaciones();
		this.panelNotificaciones.setLayout(new java.awt.GridBagLayout());
		
		this.panelScoreZConatiner = datosAntropometricosPanel.getPanelScoreZConatiner();
		
		this.lblPeso = datosAntropometricosPanel.getLblPeso();
		this.lblTalla = datosAntropometricosPanel.getLblTalla();
		this.lblPerimetroCefalico = datosAntropometricosPanel.getLblPerimetroCefalico() ;
		this.lblScoreZPerimetroCefalico = datosAntropometricosPanel.getLblScoreZPerimetroCefalico();

		this.lblValorHB = datosAntropometricosPanel.getLblValorHB();
		this.lblValorHBCorregido = datosAntropometricosPanel.getLblValorHBCorregido();
		this.panelHBFactor = datosAntropometricosPanel.getPanelHBFactor();
		this.lblDatosBioquimicos = datosAntropometricosPanel.getLblDatosBioquimicos();
	    
		this.lblTallaParaEdad = datosAntropometricosPanel.getLblTallaParaEdad();        
		this.lblPesoParaEdad = datosAntropometricosPanel.getLblPesoParaEdad();         
		this.lblPesoParaLongitudTalla = datosAntropometricosPanel.getLblPesoParaLongitudTalla();
		this.lblIMCParaEdad = datosAntropometricosPanel.getLblIMCParaEdad();          
		
		alertasHBRiesgo = new ArrayList<>();
		addKeyListeners();
		addWarningValidators();

		setGrayColorComponents();
	}

	private void setGrayColorComponents() {
		this.txtIndiceMasaCorporal.setBackground(Color.LIGHT_GRAY);
		this.txtValorHBRiesgoCorregido.setBackground(Color.LIGHT_GRAY);
		this.txtTallaCorregidaPaciente.setBackground(Color.LIGHT_GRAY);
		this.txtScoreZTallaParaEdad.setBackground(Color.LIGHT_GRAY);
		this.txtCategoriaTallaParaEdad.setBackground(Color.LIGHT_GRAY);
		this.txtScoreZPesoParaEdad.setBackground(Color.LIGHT_GRAY);
		this.txtCategoriaPesoParaEdad.setBackground(Color.LIGHT_GRAY);
		this.txtScoreZPesoParaLongitudTalla.setBackground(Color.LIGHT_GRAY);
		this.txtCategoriaPesoParaLongitudTalla.setBackground(Color.LIGHT_GRAY);
		this.txtScoreZIMCParaEdad.setBackground(Color.LIGHT_GRAY);
		this.txtCategoriaIMCParaEdad.setBackground(Color.LIGHT_GRAY);
		this.txtScoreZPerimetroCefalicoParaEdad.setBackground(Color.LIGHT_GRAY);
		this.txtCategoriaPerimetroCefalicoParaEdad.setBackground(Color.LIGHT_GRAY);
	}

	private void addWarningValidators() {
		this.txtPesoPaciente.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
				// No se necesita
			}

			@Override
			public void focusLost(FocusEvent e) {
				if (!UtilitarioForma.validarValorMinimoMaximo(e, PESO_MINIMO, PESO_MAXIMO)) {
					JOptionPane
							.showMessageDialog(
									null, "Valor incorrecto! El peso ingresado debe estar entre el rango de "
											+ PESO_MINIMO + " a " + PESO_MAXIMO + " kg",
									"Alerta!", JOptionPane.ERROR_MESSAGE);
				}
				addAlertasToPanel();
			}
		});
		this.txtTallaPaciente.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
				// No se necesita
			}

			@Override
			public void focusLost(FocusEvent e) {
				if (!UtilitarioForma.validarValorMinimoMaximo(e, TALLA_MINIMA, TALLA_MAXIMA)) {
					JOptionPane
							.showMessageDialog(
									null, "Valor incorrecto! La talla ingresada debe estar entre el rango de "
											+ TALLA_MINIMA + " a " + TALLA_MAXIMA + " cm",
									"Alerta!", JOptionPane.ERROR_MESSAGE);
				}
				actualizarTallaCorregida();
				addAlertasToPanel();
			}
		});
		this.txtPerimetroCefalico.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
				// No se necesita
			}

			@Override
			public void focusLost(FocusEvent e) {
				if (!UtilitarioForma.validarValorMinimoMaximo(e, PERIMETRO_CEFALICO_MINIMO, PERIMETRO_CEFALICO_MAXIMO)) {
					JOptionPane
							.showMessageDialog(
									null, "Valor incorrecto! El perímetro cefálico ingresado debe estar entre el rango de "
											+ PERIMETRO_CEFALICO_MINIMO + " a " + PERIMETRO_CEFALICO_MAXIMO + " cm",
									"Alerta!", JOptionPane.ERROR_MESSAGE);
					limpiarPerimetroCefalico();
				}
				addAlertasToPanel();
			}
		});
		this.txtValorHBRiesgo.addFocusListener(new FocusListener() {
			@Override
			public void focusGained(FocusEvent e) {
				// No se necesita
			}

			@Override
			public void focusLost(FocusEvent e) {
				addAlertasToPanel();
			}
		});
		this.rbDepie.addActionListener(l -> {
			actualizarTallaCorregida();
			addAlertasToPanel();
		});
		this.rbAcostado.addActionListener(l -> {
			actualizarTallaCorregida();
			addAlertasToPanel();
		});
	}

	private void limpiarPerimetroCefalico() {
		this.txtPerimetroCefalico.setText("");
	}
	
	private void actualizarTallaCorregida() {

		if (!this.txtTallaPaciente.getText().isEmpty() && this.txtTallaCorregidaPaciente.isVisible()) {
			this.txtTallaCorregidaPaciente.setText(calcularTallaCorregidaParaEdad().toString());
		}
	}

	private Double calcularTallaCorregidaParaEdad() {
		Double tallaCorregidaValor = Double.parseDouble(this.txtTallaPaciente.getText());
		Integer edadAniosMesDias = getEdadAnioMesDias();
		boolean isTallaDePie = rbDepie.isSelected();
		boolean isTallaAcostado = rbAcostado.isSelected();

		if (edadAniosMesDias >= EDAD_UN_ANIO_SEIS_MES_UN_DIA && edadAniosMesDias < EDAD_DOS_ANIO_CERO_MES_UN_DIA
				&& isTallaDePie) {
			tallaCorregidaValor = tallaCorregidaValor + 0.7d;
		} else if (edadAniosMesDias >= EDAD_DOS_ANIO_CERO_MES_UN_DIA
				&& edadAniosMesDias < EDAD_CINCO_ANIO_UN_MES_CERO_DIA && isTallaAcostado) {
			tallaCorregidaValor = tallaCorregidaValor - 0.7d;
		}
		return tallaCorregidaValor;
	}

	private Integer getEdadAnioMesDias() {
		return Integer.parseInt(concatenarEdad(variableSesion.getAniosPersona(), variableSesion.getMesesPersona(),
				variableSesion.getDiasPersona()));
	}

	private void addKeyListeners() {
		this.txtPesoPaciente.addKeyListener(new java.awt.event.KeyAdapter() {
			@Override
			public void keyTyped(java.awt.event.KeyEvent evt) {
				UtilitarioForma.soloNumerosDecimales(evt, 3);
			}
		});
		this.txtTallaPaciente.addKeyListener(new java.awt.event.KeyAdapter() {
			@Override
			public void keyTyped(java.awt.event.KeyEvent evt) {
				UtilitarioForma.soloNumerosDecimales(evt, 1);
			}
		});
		this.txtPerimetroCefalico.addKeyListener(new java.awt.event.KeyAdapter() {
			@Override
			public void keyTyped(java.awt.event.KeyEvent evt) {
				UtilitarioForma.soloNumerosDecimales(evt, 1);
				UtilitarioForma.validarLongitudCampo(evt, 4);
			}
		});
		this.txtValorHBRiesgo.addKeyListener(new java.awt.event.KeyAdapter() {
			@Override
			public void keyTyped(java.awt.event.KeyEvent evt) {
				UtilitarioForma.soloNumerosDecimales(evt, 1);

			}
		});
		this.txtValorHBRiesgoCorregido.addKeyListener(new java.awt.event.KeyAdapter() {
			@Override
			public void keyTyped(java.awt.event.KeyEvent evt) {
				UtilitarioForma.soloNumerosDecimales(evt, 1);
			}
		});
	}

	public void calcularIndiceMasaCorporal() {
		String imc = "";
		if (!txtPesoPaciente.getText().isEmpty() && !txtTallaPaciente.getText().isEmpty()) {
			Double imcDouble = UtilitarioMedico.calcularIndiceMasaCorporal(
					Double.parseDouble(txtPesoPaciente.getText()), calcularTallaCorregidaParaEdad() / 100);
			imc = imcDouble.toString();
		}
		this.txtIndiceMasaCorporal.setText(imc);
	}

	private void calcularValorHBRiesgoCorregido() {
		String valorHBRiesgoCorregido = "";
		String valorFactorCorrecionHB = "";
		if (!txtValorHBRiesgo.getText().isEmpty() && alertasHBRiesgo.isEmpty()) {
			Integer alturaEntidaNumber = variableSesion.getEstablecimientoSalud().getZ17s();
			if(alturaEntidaNumber == null ) {
				alturaEntidaNumber = 0;
			}
			Double[] valorRiesgoCorregidoFactor = UtilitarioMedico.calcularValorHBRiesgoCorregido(
					Double.parseDouble(txtValorHBRiesgo.getText()), alturaEntidaNumber);
			Double valorRiesgoCorregidoDouble = valorRiesgoCorregidoFactor[0];
			Double valorFactorDouble = valorRiesgoCorregidoFactor[1];
			valorHBRiesgoCorregido = valorRiesgoCorregidoDouble.toString();
			valorFactorCorrecionHB = "Factor: " + valorFactorDouble;
		}

		this.txtValorHBRiesgoCorregido.setText(valorHBRiesgoCorregido);
		this.lblFactor.setText(valorFactorCorrecionHB);
	}

	private void calcularScoreZYCategorias(boolean saveToVariableSesion) {
		if (variableSesion.getPersona() == null) {
			return;
		}
		Integer edadAnioMesDias = Integer.parseInt(concatenarEdad(variableSesion.getAniosPersona(),
				variableSesion.getMesesPersona(), variableSesion.getDiasPersona()));
		logger.debug("edadAnioMesDias {}", edadAnioMesDias);
		Integer edadEnMeses = variableSesion.getEdadEnMesesPersona();
		logger.debug("edadEnMeses {}",edadEnMeses);
		Integer edadEnDias = variableSesion.getEdadEnDiasPersona();
		logger.debug("edadEnDias {}",edadEnDias);
		logger.debug("sexo {}",getSexo());
		boolean isTallaDePie = rbDepie.isSelected();
		logger.debug("isTallaDePie {}",isTallaDePie);

		if (!txtPesoPaciente.getText().isEmpty() && !txtTallaPaciente.getText().isEmpty()) {
			try {
				Double scoreZTallaParaEdad = scoreZAndCategoriaService.calcularScoreZTallaParaEdad(
						calcularTallaCorregidaParaEdad(), edadAnioMesDias, edadEnMeses, edadEnDias, getSexo());
				this.txtScoreZTallaParaEdad.setText(scoreZTallaParaEdad.toString());

				SignosVitalesReglasValidacion categoriaTallaParaEdad = scoreZAndCategoriaService
						.calcularCategoriaTallaParaEdad(scoreZTallaParaEdad, edadAnioMesDias, getSexo());
				this.txtCategoriaTallaParaEdad.setText(categoriaTallaParaEdad.getAlerta());
				this.txtCategoriaTallaParaEdad
						.setBackground(Notifications.mapStringToColor(categoriaTallaParaEdad.getColor()));

				if (saveToVariableSesion) {
					variableSesion.setScoreZTallaParaEdad(
							medicionpacienteService.getMedicionScoreZTallaParaEdad(scoreZTallaParaEdad.toString()));
					variableSesion.setCategoriaTallaParaEdad(signosvitalesalertasService
							.findSignoVitalAlertaByID(categoriaTallaParaEdad.getIdSignoVitalAlerta()));
				}
			} catch (PercentilNotFoundException | PercentilDetalleNotFoundException
					| SignosVitalesReglasValidacionNotFoundException e) {
				this.txtScoreZTallaParaEdad.setText("");
				this.txtCategoriaTallaParaEdad.setText(NO_APLICA);
				this.txtCategoriaTallaParaEdad.setBackground(Color.LIGHT_GRAY);
				
				
			}

			try {
				Double scoreZPesoParaEdad = scoreZAndCategoriaService.calcularScoreZPesoParaEdad(
						Double.parseDouble(txtPesoPaciente.getText()), edadAnioMesDias, edadEnMeses, edadEnDias,
						getSexo());
				this.txtScoreZPesoParaEdad.setText(scoreZPesoParaEdad.toString());

				SignosVitalesReglasValidacion categoriaPesoParaEdad = scoreZAndCategoriaService
						.calcularCategoriaPesoParaEdad(scoreZPesoParaEdad, edadAnioMesDias, getSexo());
				this.txtCategoriaPesoParaEdad.setText(categoriaPesoParaEdad.getAlerta());
				this.txtCategoriaPesoParaEdad
						.setBackground(Notifications.mapStringToColor(categoriaPesoParaEdad.getColor()));

				if (saveToVariableSesion) {
					variableSesion.setScoreZPesoParaEdad(
							medicionpacienteService.getMedicionScoreZPesoParaEdad(scoreZPesoParaEdad.toString()));
					variableSesion.setCategoriaPesoParaEdad(signosvitalesalertasService
							.findSignoVitalAlertaByID(categoriaPesoParaEdad.getIdSignoVitalAlerta()));
				}
			} catch (PercentilNotFoundException | PercentilDetalleNotFoundException
					| SignosVitalesReglasValidacionNotFoundException e) {
				this.txtScoreZPesoParaEdad.setText("");
				this.txtCategoriaPesoParaEdad.setText(NO_APLICA);
				this.txtCategoriaPesoParaEdad.setBackground(Color.LIGHT_GRAY);
				
				
			}

			try {
				Double scoreZPesoParaLongitudTalla = scoreZAndCategoriaService.calcularScoreZPesoParaLongitudTalla(
						Double.parseDouble(txtPesoPaciente.getText()), calcularTallaCorregidaParaEdad(),
						edadAnioMesDias, edadEnMeses, edadEnDias, getSexo(), isTallaDePie);
				this.txtScoreZPesoParaLongitudTalla.setText(scoreZPesoParaLongitudTalla.toString());

				SignosVitalesReglasValidacion categoriaPesoParaLongitudTalla = scoreZAndCategoriaService
						.calcularCategoriaPesoParaLongitudTalla(scoreZPesoParaLongitudTalla, edadAnioMesDias,
								getSexo());
				this.txtCategoriaPesoParaLongitudTalla.setText(categoriaPesoParaLongitudTalla.getAlerta());
				this.txtCategoriaPesoParaLongitudTalla
						.setBackground(Notifications.mapStringToColor(categoriaPesoParaLongitudTalla.getColor()));

				if (saveToVariableSesion) {
					variableSesion.setScoreZPesoParaLongitudTalla(medicionpacienteService
							.getMedicionScoreZPesoParaLongitudTalla(scoreZPesoParaLongitudTalla.toString()));
					variableSesion.setCategoriaPesoParaLongitudTalla(signosvitalesalertasService
							.findSignoVitalAlertaByID(categoriaPesoParaLongitudTalla.getIdSignoVitalAlerta()));
				}
			} catch (PercentilNotFoundException | PercentilDetalleNotFoundException
					| SignosVitalesReglasValidacionNotFoundException e) {
				this.txtScoreZPesoParaLongitudTalla.setText("");
				this.txtCategoriaPesoParaLongitudTalla.setText(NO_APLICA);
				this.txtCategoriaPesoParaLongitudTalla.setBackground(Color.LIGHT_GRAY);
				
				
			}
		}

		if (!this.txtIndiceMasaCorporal.getText().isEmpty()) {
			try {
				Double scoreZIMCparaEdad = scoreZAndCategoriaService.calcularScoreZIMCparaEdad(
						Double.parseDouble(this.txtIndiceMasaCorporal.getText()), edadAnioMesDias, edadEnMeses,
						edadEnDias, getSexo());
				this.txtScoreZIMCParaEdad.setText(scoreZIMCparaEdad.toString());

				SignosVitalesReglasValidacion categoriaIMCparaEdad = scoreZAndCategoriaService
						.calcularCategoriaIMCparaEdad(scoreZIMCparaEdad, edadAnioMesDias, getSexo());
				this.txtCategoriaIMCParaEdad.setText(categoriaIMCparaEdad.getAlerta());
				this.txtCategoriaIMCParaEdad
						.setBackground(Notifications.mapStringToColor(categoriaIMCparaEdad.getColor()));

				if (saveToVariableSesion) {
					variableSesion.setScoreZIMCParaEdad(
							medicionpacienteService.getMedicionScoreZIMCParaEdad(scoreZIMCparaEdad.toString()));
					variableSesion.setCategoriaIMCParaEdad(signosvitalesalertasService
							.findSignoVitalAlertaByID(categoriaIMCparaEdad.getIdSignoVitalAlerta()));
				}
			} catch (PercentilNotFoundException | PercentilDetalleNotFoundException
					| SignosVitalesReglasValidacionNotFoundException e) {
				this.txtScoreZIMCParaEdad.setText("");
				this.txtCategoriaIMCParaEdad.setText(NO_APLICA);
				this.txtCategoriaIMCParaEdad.setBackground(Color.LIGHT_GRAY);
				
				
			}
		}

		if (!this.txtPerimetroCefalico.getText().isEmpty()) {
			try {
				Double scoreZPerimetroCefalicoParaEdad = scoreZAndCategoriaService
						.calcularScoreZPerimetroCefalicoParaEdad(
								Double.parseDouble(this.txtPerimetroCefalico.getText()), edadAnioMesDias, edadEnMeses,
								edadEnDias, getSexo());
				this.txtScoreZPerimetroCefalicoParaEdad.setText(scoreZPerimetroCefalicoParaEdad.toString());

				SignosVitalesReglasValidacion categoriaPerimetroCefalicoParaEdad = scoreZAndCategoriaService
						.calcularCategoriaPerimetroCefalicoParaEdad(scoreZPerimetroCefalicoParaEdad, edadAnioMesDias,
								getSexo());
				this.txtCategoriaPerimetroCefalicoParaEdad.setText(categoriaPerimetroCefalicoParaEdad.getAlerta());
				this.txtCategoriaPerimetroCefalicoParaEdad
						.setBackground(Notifications.mapStringToColor(categoriaPerimetroCefalicoParaEdad.getColor()));

				if (saveToVariableSesion) {
					variableSesion.setScoreZPerimetroCefalicoParaEdad(medicionpacienteService
							.getMedicionScoreZPerimetroCefalicoParaEdad(scoreZPerimetroCefalicoParaEdad.toString()));
					variableSesion.setCategoriaPerimetroCefalicoParaEdad(signosvitalesalertasService
							.findSignoVitalAlertaByID(categoriaPerimetroCefalicoParaEdad.getIdSignoVitalAlerta()));
				}
			} catch (PercentilNotFoundException | PercentilDetalleNotFoundException
					| SignosVitalesReglasValidacionNotFoundException e) {
				this.txtScoreZPerimetroCefalicoParaEdad.setText("");
				this.txtCategoriaPerimetroCefalicoParaEdad.setText(NO_APLICA);
				this.txtCategoriaPerimetroCefalicoParaEdad.setBackground(Color.LIGHT_GRAY);
				
				
			}
		}

	}

	private GridBagConstraints layoutConstraints(int gridx, int gridy, int gridheight, int weightx, int weighty,
			int fill, int anchor) {
		GridBagConstraints gridBagConstraints = new java.awt.GridBagConstraints();
		gridBagConstraints.gridx = gridx;
		gridBagConstraints.gridy = gridy;
		gridBagConstraints.gridheight = gridheight;
		gridBagConstraints.weightx = weightx;
		gridBagConstraints.weighty = weighty;
		gridBagConstraints.fill = fill;
		gridBagConstraints.anchor = anchor;
		return gridBagConstraints;
	}

	private void addRellenoVertical(int gridy, int weighty) {
		Filler rellenoVertical = new Filler(new java.awt.Dimension(0, 10), new java.awt.Dimension(0, 10),
				new java.awt.Dimension(32767, 10));
		panelNotificaciones.add(rellenoVertical, layoutConstraints(0, gridy, 1, 0, weighty,
				java.awt.GridBagConstraints.VERTICAL, java.awt.GridBagConstraints.CENTER));
	}

	public void addAlertasToPanel() {
		panelNotificaciones.removeAll();

		calcularIndiceMasaCorporal();
		JPanel panelPeso = null;
		if (!txtPesoPaciente.getText().isEmpty()) {
			panelPeso = alertasValidacion(runValidation(pesoValidator, txtPesoPaciente.getText()));
			if (panelPeso != null) {
				panelPeso.setVisible(true);
				panelNotificaciones.add(panelPeso, layoutConstraints(0, 0, 1, 1, 0,
						java.awt.GridBagConstraints.HORIZONTAL, java.awt.GridBagConstraints.NORTH));

				addRellenoVertical(1, 0);
			}
		}

		JPanel panelTalla = null;
		if (!txtTallaPaciente.getText().isEmpty()) {
			panelTalla = alertasValidacion(runValidation(tallaValidator, calcularTallaCorregidaParaEdad().toString()));
			if (panelTalla != null) {
				panelTalla.setVisible(true);
				panelNotificaciones.add(panelTalla, layoutConstraints(0, 2, 1, 1, 0,
						java.awt.GridBagConstraints.HORIZONTAL, java.awt.GridBagConstraints.NORTH));

				addRellenoVertical(3, 0);
			}
		}

		JPanel panelIMC = null;
		if (!txtPesoPaciente.getText().isEmpty() && !txtTallaPaciente.getText().isEmpty()) {
			panelIMC = alertasValidacion(runValidation(imcValidator, txtIndiceMasaCorporal.getText()));
			if (panelIMC != null) {
				panelIMC.setVisible(true);
				panelNotificaciones.add(panelIMC, layoutConstraints(0, 4, 1, 1, 0,
						java.awt.GridBagConstraints.HORIZONTAL, java.awt.GridBagConstraints.NORTH));

				addRellenoVertical(5, 0);
			}
		}

		JPanel panelValorHBRiesgo = null;
		if (!txtValorHBRiesgo.getText().isEmpty()) {
			alertasHBRiesgo = runValidation(hbRiesgoValidator, txtValorHBRiesgo.getText());
			panelValorHBRiesgo = alertasValidacion(alertasHBRiesgo);
			if (panelValorHBRiesgo != null) {
				panelValorHBRiesgo.setVisible(true);
				panelNotificaciones.add(panelValorHBRiesgo, layoutConstraints(0, 6, 1, 1, 0,
						java.awt.GridBagConstraints.HORIZONTAL, java.awt.GridBagConstraints.NORTH));

				addRellenoVertical(7, 0);
			}
		}

		calcularValorHBRiesgoCorregido();
		JPanel panelValorHBRiesgoCorregido = null;
		if (!txtValorHBRiesgo.getText().isEmpty() && panelValorHBRiesgo == null) {
			panelValorHBRiesgoCorregido = alertasValidacion(
					runValidation(hbRiesgoCorregidoValidator, txtValorHBRiesgoCorregido.getText()));
			if (panelValorHBRiesgoCorregido != null) {
				panelValorHBRiesgoCorregido.setVisible(true);
				panelNotificaciones.add(panelValorHBRiesgoCorregido, layoutConstraints(0, 8, 1, 1, 0,
						java.awt.GridBagConstraints.HORIZONTAL, java.awt.GridBagConstraints.NORTH));

				addRellenoVertical(9, 0);
			}
		}

		addRellenoVertical(10, 1);

		panelNotificaciones.revalidate();
		panelNotificaciones.repaint();

		calcularScoreZYCategorias(false);
	}

	private JPanel alertasValidacion(List<ValidationResult> alertas) {
		if (!alertas.isEmpty()) {
			return Notifications.getPanelMensajesTextArea(alertas);
		} else {
			return null;
		}
	}

	private List<ValidationResult> runValidation(Validator validator, String textoValor) {
		String[] valorEdadSexoIsEmbarazada = { textoValor,
				concatenarEdad(variableSesion.getAniosPersona(), variableSesion.getMesesPersona(),
						variableSesion.getDiasPersona()),
				getSexo(),
				variableSesion.getIsEmbarazada() != null ? variableSesion.getIsEmbarazada().toString() : "false" };
		String[] imcEdadTalla = { textoValor, concatenarEdad(variableSesion.getAniosPersona(),
				variableSesion.getMesesPersona(), variableSesion.getDiasPersona()), txtTallaPaciente.getText() };
		logger.debug("valorEdadSexoisEmbarazada valor {} años-meses-dias {} sexo {} isEmbarazada  {}" , 
				valorEdadSexoIsEmbarazada[0] , valorEdadSexoIsEmbarazada[1] , valorEdadSexoIsEmbarazada[2] , valorEdadSexoIsEmbarazada[3]);
		List<ValidationResult> alertas = null;
		if (validator instanceof IndiceMasaCorporalValidator) {
			alertas = validator.validate(imcEdadTalla);
			ValidationResult warningIMCMayor200Max = ValidationResultCatalog.IMC_MAYOR_200_ALERTA.getValidationError();
			Optional<ValidationResult> errorFound = alertas.stream().parallel().filter(alerta -> {
				return alerta.getCode().equals(warningIMCMayor200Max.getCode());
			}).findFirst();
			imcHasError = errorFound.isPresent();
			decorateErrorIMC();
		} else {
			alertas = validator.validate(valorEdadSexoIsEmbarazada);
		}

		if (validator instanceof HBRiesgoValidator) {
			ValidationResult warningHBRiensgoAlertaMin = ValidationResultCatalog.HB_RIESGO_ALERTA_MIN
					.getValidationError();
			ValidationResult warningHBRiensgoAlertaMax = ValidationResultCatalog.HB_RIESGO_ALERTA_MAX
					.getValidationError();
			Optional<ValidationResult> errorFound = alertas.stream().parallel().filter(alerta -> {
				return alerta.getCode().equals(warningHBRiensgoAlertaMin.getCode())
						|| alerta.getCode().equals(warningHBRiensgoAlertaMax.getCode());
			}).findFirst();
			hbHasError = errorFound.isPresent();
			decorateErrorValorHBRiesgo();
		}

		return alertas;
	}

	private String concatenarEdad(Integer edadAnios, Integer edadMeses, Integer edadDias) {
		return StringUtils.leftPad(edadAnios.toString(), 3, "0") + StringUtils.leftPad(edadMeses.toString(), 2, "0")
				+ StringUtils.leftPad(edadDias.toString(), 2, "0");
	}

	private String getSexo() {
		boolean isMujer = variableSesion.getIsMujer();
		boolean isHombre = variableSesion.getIsHombre();
		if (isMujer) {
			return "F";
		} else if (isHombre) {
			return "M";
		} else {
			return "I";
		}
	}

	public void getMedicionesAtropometricas() {
		if (!ValidationSupport.isNullOrEmptyString(txtPesoPaciente.getText())) {
			variableSesion.setMedicionPeso(medicionpacienteService.getMedicionPeso(txtPesoPaciente.getText()));
		}
		if (!ValidationSupport.isNullOrEmptyString(txtTallaPaciente.getText())) {
			Medicionpaciente medicionTalla = medicionpacienteService.getMedicionTalla(txtTallaPaciente.getText());
			if (rbDepie.isSelected()) {
				medicionTalla.setPosiciontalla(TALLA_DE_PIE);
			}
			if (rbAcostado.isSelected()) {
				medicionTalla.setPosiciontalla(TALLA_ACOSTADO);
			}
			variableSesion.setMedicionTalla(medicionTalla);
		}
		if (!ValidationSupport.isNullOrEmptyString(txtValorHBRiesgo.getText())) {
			variableSesion
					.setValorHBRiesgo(medicionpacienteService.getMedicionValorHBRiesgo(txtValorHBRiesgo.getText()));
		}
		if (!ValidationSupport.isNullOrEmptyString(txtValorHBRiesgoCorregido.getText())) {
			variableSesion.setValorHBRiesgoCorregido(
					medicionpacienteService.getMedicionValorHBRiesgoCorregido(txtValorHBRiesgoCorregido.getText()));
		}
		if (!ValidationSupport.isNullOrEmptyString(txtValorHBRiesgoCorregido.getText())) {
			Integer grupoPrioritarioEmbarazada = variableSesion.getIsEmbarazada() ? IS_EMBARAZADA_GRUPO_PRIORITARIO_ID
					: null;
			try {
				SignosVitalesReglasValidacion anemiaRA = medicionPacienteAlertasService
						.calculateIndicadorAnemiaHBRiesgoCorregido(txtValorHBRiesgoCorregido.getText(),
								getEdadAnioMesDias(), getSexo(), grupoPrioritarioEmbarazada);
				variableSesion.setIndicadorAnemiaHBRiesgoCorregido(
						signosvitalesalertasService.findSignoVitalAlertaByID(anemiaRA.getIdSignoVitalAlerta()));
			} catch (Exception e) {
				// Si no se encuentra no se guarda
			}
		}
		if (!ValidationSupport.isNullOrEmptyString(txtPerimetroCefalico.getText())) {
			variableSesion.setMedicionPerimetroCefalico(
					medicionpacienteService.getMedicionPerimetroCefalico(txtPerimetroCefalico.getText()));
		}
		if (!ValidationSupport.isNullOrEmptyString(txtTallaCorregidaPaciente.getText())
				&& txtTallaCorregidaPaciente.isVisible()) {
			variableSesion.setMedicionTallaCorregida(
					medicionpacienteService.getMedicionTallaCorregida(txtTallaCorregidaPaciente.getText()));
		}
		if (!ValidationSupport.isNullOrEmptyString(txtIndiceMasaCorporal.getText())) {
			variableSesion.setMedicionIMC(medicionpacienteService.getMedicionIMC(txtIndiceMasaCorporal.getText()));
		}
		if (!ValidationSupport.isNullOrEmptyString(txtIndiceMasaCorporal.getText())) {
			try {
				SignosVitalesReglasValidacion imcRA = medicionPacienteAlertasService
						.calculateClasificacionIMC(txtIndiceMasaCorporal.getText(), getEdadAnioMesDias(), getSexo());
				variableSesion.setClasificacionIMC(
						signosvitalesalertasService.findSignoVitalAlertaByID(imcRA.getIdSignoVitalAlerta()));
			} catch (Exception e) {
				// Si no se encuentra no se guarda
			}
		}
		calcularScoreZYCategorias(true);
	}

	public boolean validateForm() {
		Integer edadAniosMesDias = Integer.parseInt(concatenarEdad(variableSesion.getAniosPersona(),
				variableSesion.getMesesPersona(), variableSesion.getDiasPersona()));
		
		List<Object> controlesValidarDinamico = new ArrayList<>();
		
		if (edadAniosMesDias < EDAD_CINCO_ANIO_UN_MES_CERO_DIA || 
				(variableSesion.getIsEmbarazada() != null && variableSesion.getIsEmbarazada())) {
			controlesValidarDinamico.add(txtTallaPaciente);
		}
		
		if(edadAniosMesDias < EDAD_CINCO_ANIO_UN_MES_CERO_DIA || 
				(variableSesion.getIsEmbarazada() != null && variableSesion.getIsEmbarazada())) {
			controlesValidarDinamico.add(txtPesoPaciente);
		}
		
		if(edadAniosMesDias < EDAD_CINCO_ANIO_UN_MES_CERO_DIA) {
			controlesValidarDinamico.add(txtPerimetroCefalico);
		}
		
		Object[] controlesValidar = controlesValidarDinamico.toArray();

		decorateErrorIMC();
		decorateErrorValorHBRiesgo();

		boolean validarForma = UtilitarioForma.validarCamposRequeridos(controlesValidar);
		if (controlesValidar.length == 0 || validarForma) {
			getMedicionesAtropometricas();
		}

		return validarForma && !hbHasError && !imcHasError;
	}

	private void decorateErrorIMC() {
		if (imcHasError) {
			txtIndiceMasaCorporal.setearBordeCajaRequerida();
		} else {
			txtIndiceMasaCorporal.setearBordeCajaNormal();
		}
	}

	private void decorateErrorValorHBRiesgo() {
		if (hbHasError) {
			txtValorHBRiesgo.setearBordeCajaRequerida();
		} else {
			txtValorHBRiesgo.setearBordeCajaNormal();
		}
	}

	public void addAlturaEntidad() {
		Integer alturaEntidaNumber = variableSesion.getEstablecimientoSalud().getZ17s();
		String alturaEntidad = alturaEntidaNumber == null ? "0" : alturaEntidaNumber.toString();
		this.lblAltura.setText("Altura: " + alturaEntidad + "m");
	}

	public void cleanInput() {
		Object[] inputs = { txtPesoPaciente, txtTallaPaciente, txtTallaCorregidaPaciente, txtPerimetroCefalico,
				txtIndiceMasaCorporal, txtValorHBRiesgo, txtValorHBRiesgoCorregido, txtScoreZTallaParaEdad,
				txtCategoriaTallaParaEdad, txtScoreZPesoParaEdad, txtCategoriaPesoParaEdad,
				txtScoreZPesoParaLongitudTalla, txtCategoriaPesoParaLongitudTalla, txtScoreZIMCParaEdad,
				txtCategoriaIMCParaEdad, txtScoreZPerimetroCefalicoParaEdad, txtCategoriaPerimetroCefalicoParaEdad

		};
		UtilitarioForma.limpiarCampos(datosAntropometricosPanel, inputs);
		setGrayColorComponents();
	}

	public void refreshComponentsTallaCorregida() {
		Integer edadAniosMesDias = Integer.parseInt(concatenarEdad(variableSesion.getAniosPersona(),
				variableSesion.getMesesPersona(), variableSesion.getDiasPersona()));
			
		if (edadAniosMesDias < EDAD_CINCO_ANIO_UN_MES_CERO_DIA || 
				(variableSesion.getIsEmbarazada() != null && variableSesion.getIsEmbarazada())) {
			this.lblTalla.setText(LABEL_TALLA_OBLIGATORIO);
		}else {
			this.lblTalla.setText(LABEL_TALLA_NO_OBLIGATORIO);
		}
		
		if(edadAniosMesDias < EDAD_CINCO_ANIO_UN_MES_CERO_DIA || 
				(variableSesion.getIsEmbarazada() != null && variableSesion.getIsEmbarazada())) {
			this.lblPeso.setText(LABEL_PESO_OBLIGATORIO);
		}else {
			this.lblPeso.setText(LABEL_PESO_NO_OBLIGATORIO);
		}
		
		if(edadAniosMesDias < EDAD_CINCO_ANIO_UN_MES_CERO_DIA) {
			this.lblPerimetroCefalico.setText(LABEL_PERIMETRO_CEFALICO_OBLIGATORIO);
			lblPerimetroCefalico.setVisible(true);
			txtPerimetroCefalico.setVisible(true);
			
			lblScoreZPerimetroCefalico.setVisible(true);
			txtScoreZPerimetroCefalicoParaEdad.setVisible(true);
			txtCategoriaPerimetroCefalicoParaEdad.setVisible(true);
		}else {
			this.lblPerimetroCefalico.setText(LABEL_PERIMETRO_CEFALICO_NO_OBLIGATORIO);
			
			lblPerimetroCefalico.setVisible(false);
			txtPerimetroCefalico.setText("");
			txtPerimetroCefalico.setVisible(false);
			
			lblScoreZPerimetroCefalico.setVisible(false);
			txtScoreZPerimetroCefalicoParaEdad.setVisible(false);
			txtCategoriaPerimetroCefalicoParaEdad.setVisible(false);
		}

		if (edadAniosMesDias < EDAD_SEIS_MES_CERO_DIA) {
			lblDatosBioquimicos.setVisible(false);
			lblValorHB.setVisible(false);
			txtValorHBRiesgo.setVisible(false);
			lblValorHBCorregido.setVisible(false);
			txtValorHBRiesgoCorregido.setVisible(false);
			panelHBFactor.setVisible(false);
		} else {
			lblDatosBioquimicos.setVisible(true);
			lblValorHB.setVisible(true);
			txtValorHBRiesgo.setVisible(true);
			lblValorHBCorregido.setVisible(true);
			txtValorHBRiesgoCorregido.setVisible(true);
			panelHBFactor.setVisible(true);
		}
		
		if (edadAniosMesDias < EDAD_UN_ANIO_SEIS_MES_UN_DIA) {
			rbAcostado.setSelected(true);
			rbDepie.setVisible(false);
			lblTallaCorregida.setVisible(false);
			txtTallaCorregidaPaciente.setText("");
			txtTallaCorregidaPaciente.setVisible(false);
		} else if (edadAniosMesDias < EDAD_CINCO_ANIO_UN_MES_CERO_DIA) {
			rbDepie.setSelected(true);
			rbDepie.setVisible(true);
			rbAcostado.setVisible(true);
			lblTallaCorregida.setVisible(true);
			txtTallaCorregidaPaciente.setVisible(true);
		} else {
			rbDepie.setSelected(true);
			rbAcostado.setVisible(false);
			lblTallaCorregida.setVisible(false);
			txtTallaCorregidaPaciente.setText("");
			txtTallaCorregidaPaciente.setVisible(false);
		}
		
		if(edadAniosMesDias >= EDAD_CINCO_ANIO_UN_MES_CERO_DIA) {
			this.lblPesoParaLongitudTalla.setVisible(false);
			this.txtScoreZPesoParaLongitudTalla.setVisible(false);
			this.txtCategoriaPesoParaLongitudTalla.setVisible(false);
		} else {
			this.lblPesoParaLongitudTalla.setVisible(true);
			this.txtScoreZPesoParaLongitudTalla.setVisible(true);
			this.txtCategoriaPesoParaLongitudTalla.setVisible(true);
		}
		
		if(edadAniosMesDias > EDAD_DIEZ_ANIO_CERO_MES_CERO_DIA) {
			this.lblPesoParaEdad.setVisible(false);
			this.txtScoreZPesoParaEdad.setVisible(false);
			this.txtCategoriaPesoParaEdad.setVisible(false);
		} else {
			this.lblPesoParaEdad.setVisible(true);
			this.txtScoreZPesoParaEdad.setVisible(true);
			this.txtCategoriaPesoParaEdad.setVisible(true);
		}
		
		if(edadAniosMesDias > EDAD_DIEZ_Y_NUEVE_ANIO_CERO_MES_CERO_DIA) {
			this.panelScoreZConatiner.setVisible(false);
		} else {
			this.panelScoreZConatiner.setVisible(true);
		}
	}
}
