/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.principal.forma;

import java.awt.Image;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.beans.PropertyVetoException;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.swing.Icon;
import javax.swing.ImageIcon;
import javax.swing.JInternalFrame;
import javax.swing.JOptionPane;
import javax.swing.WindowConstants;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import ec.gob.msp.rdacaa.amed.controller.CargaPacientesController;
import ec.gob.msp.rdacaa.amed.controller.CierreMensualController;
import ec.gob.msp.rdacaa.amed.controller.ValidationController;
import ec.gob.msp.rdacaa.amed.forma.CierreMensual;
import ec.gob.msp.rdacaa.business.service.CierreatencionesService;
import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.reporte.controller.ReportesController;
import ec.gob.msp.rdacaa.reporte.forma.Reportes;
import ec.gob.msp.rdacaa.usuarios.controller.AdminUsuariosController;
import ec.gob.msp.rdacaa.usuarios.forma.AdminUsuarios;
import lombok.Getter;
import lombok.Setter;


@Component
public class EscritorioPrincipal extends javax.swing.JFrame {

    private static final long serialVersionUID = 2387189865060290322L;

    @Getter
    @Setter
    private VariableSesion vs;

    @Getter
    @Setter
    private ContainerWizard containerWizard;

    @Getter
    @Setter
    private AdminUsuarios adminUsuarios;

    @Getter
    @Setter
    private CierreMensual cierreMensual;
    
    @Getter
    @Setter
    private Reportes reportes;

    private final CierreMensualController cierreMensualController;

    @Autowired
    private Acceso acceso;

    private final AdminUsuariosController adminUsuariosController;
    
    @Getter
    private static boolean isEscritorioPrincipalInitialized;

    @Autowired
    private ValidationController validationController;
    
    @Autowired
    private CargaPacientesController cargaPacientesController;
    
    @Autowired
    private ReportesController reportesController;

    /**
     * Creates new form EscritorioPrincipal
     *
     * @param container
     * @param adminUsuarios
     * @param adminUsuariosController
     * @param cierreMensual
     * @param cierreMensualController
     * @param cierreatencionesService
     */

    @Autowired
    public EscritorioPrincipal(ContainerWizard container,
            AdminUsuarios adminUsuarios,
            AdminUsuariosController adminUsuariosController,
            CierreMensual cierreMensual,
            Reportes reportes,
            CierreMensualController cierreMensualController,
            CierreatencionesService cierreatencionesService,
            ValidationController validationController) {
        this.containerWizard = container;
        this.adminUsuarios = adminUsuarios;
        this.adminUsuariosController = adminUsuariosController;
        this.cierreMensual = cierreMensual;
        this.reportes = reportes;
        this.cierreMensualController = cierreMensualController;
        this.validationController = validationController;
        initComponents();
        this.setExtendedState(MAXIMIZED_BOTH);
        this.setIconImage(new ImageIcon(getClass().getResource("/principal/icono_principal.png")).getImage());
        this.setLocationRelativeTo(null);
        disableControls();
        
        ImageIcon logo = new ImageIcon(getClass().getResource("/principal/GEOSALUDBLANCO.png"));
        Icon icono = new ImageIcon(logo.getImage().getScaledInstance(1024, 500, Image.SCALE_DEFAULT));
        labelPrincipal.setIcon(icono);
        this.repaint();

        setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        
        vs = VariableSesion.getInstanciaVariablesSession();
        
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent we) {
                int eleccion = JOptionPane.showConfirmDialog(null, "¿Desea salir del Sistema?", "RDACAA 2.0", JOptionPane.YES_NO_OPTION);
                if (eleccion == 0) {
                	setVisible(false);
                    acceso.setVisible(true);
                    reportesController.cerrar();
                    if(vs != null) {
                    	vs.setUtilitario(null);
                    	vs.setInstanciaNull();
                    }
                    containerWizard.dispose();
                    EscritorioPrincipal.dsp_contenedorRDACAA.remove(containerWizard);
                }
            }
        });
        
        isEscritorioPrincipalInitialized = true;
    }
    
    public static void disableControlsCierre() {   
        btn_registroadmision.setEnabled(false);
    }
    
    public static void disableControls() {
        btn_gruposprioritarios.setEnabled(false);
        btn_datosantropometricos.setEnabled(false);
        btn_diagnosticos.setEnabled(false);
        btn_exameneslaboratorio.setEnabled(false);
        btn_infoobstetrica.setEnabled(false);
        btn_odontologia.setEnabled(false);
        btn_prescripcion.setEnabled(false);
        btn_procedimientos.setEnabled(false);
        btn_referencia.setEnabled(false);
        btn_sivan.setEnabled(false);
        btn_vacunacion.setEnabled(false);
        btn_vihsida.setEnabled(false);
        btn_gruposvulnerables.setEnabled(false);
        btn_datosviolencia.setEnabled(false);
    }
    
    public static void visibleControls() {
        btn_registroadmision.setVisible(false);
        btn_gruposprioritarios.setVisible(false);
        btn_datosantropometricos.setVisible(false);
        btn_diagnosticos.setVisible(false);
        btn_exameneslaboratorio.setVisible(false);
        btn_infoobstetrica.setVisible(false);
        btn_odontologia.setVisible(false);
        btn_prescripcion.setVisible(false);
        btn_procedimientos.setVisible(false);
        btn_referencia.setVisible(false);
        btn_sivan.setVisible(false);
        btn_vacunacion.setVisible(false);
        btn_vihsida.setVisible(false);
        btn_gruposvulnerables.setVisible(false);
        btn_datosviolencia.setVisible(false);
    }
    
    public static void disableControlsAttentionFlow(boolean status) {
    	btn_cierremes.setEnabled(status);
        //btn_profesionales.setEnabled(status);
        btn_reportes.setEnabled(status);
        btn_cerrarsesion.setEnabled(status);
        btn_carga_pacientes.setEnabled(status);
    }
    
    public final void loadingButtonsContainer() {
        containerWizard.loadWizardContainer();
    }
    
    public final void loadingButtonsInvited() {
    	containerWizard.renderButtonsByInvitado();
    }
    
    public final void loadingButtonsImportPacientes() {
    	containerWizard.renderButtonsByImportPacientes();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        labelPrincipal = new javax.swing.JLabel();
        PANEL_LATERAL = new javax.swing.JPanel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("RDACAA 2.0");

        dsp_contenedorRDACAA.setAutoscrolls(true);

        labelPrincipal.setAutoscrolls(true);

        dsp_contenedorRDACAA.setLayer(labelPrincipal, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout dsp_contenedorRDACAALayout = new javax.swing.GroupLayout(dsp_contenedorRDACAA);
        dsp_contenedorRDACAA.setLayout(dsp_contenedorRDACAALayout);
        dsp_contenedorRDACAALayout.setHorizontalGroup(
            dsp_contenedorRDACAALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, dsp_contenedorRDACAALayout.createSequentialGroup()
                .addComponent(labelPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, 682, Short.MAX_VALUE)
                .addContainerGap())
        );
        dsp_contenedorRDACAALayout.setVerticalGroup(
            dsp_contenedorRDACAALayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(labelPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        PANEL_LATERAL.setToolTipText("");

        JToolbarLateral.setBackground(new java.awt.Color(255, 255, 255));
        JToolbarLateral.setBorder(new javax.swing.border.LineBorder(new java.awt.Color(204, 204, 204), 1, true));
        JToolbarLateral.setFloatable(false);
        JToolbarLateral.setOrientation(javax.swing.SwingConstants.VERTICAL);
        JToolbarLateral.setRollover(true);
        JToolbarLateral.setToolTipText("Panel Lateral");
        JToolbarLateral.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        JToolbarLateral.setInheritsPopupMenu(true);

        btn_registroadmision.setBackground(new java.awt.Color(255, 255, 255));
        btn_registroadmision.setIcon(new javax.swing.ImageIcon(getClass().getResource("/principal/admision.jpg"))); // NOI18N
        btn_registroadmision.setText("Registro Paciente");
        btn_registroadmision.setToolTipText("Registro de admisión");
        btn_registroadmision.setBorder(null);
        btn_registroadmision.setFocusable(false);
        btn_registroadmision.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_registroadmision.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_registroadmision.setMaximumSize(new java.awt.Dimension(200, 30));
        btn_registroadmision.setMinimumSize(new java.awt.Dimension(200, 30));
        btn_registroadmision.setPreferredSize(new java.awt.Dimension(200, 30));
        btn_registroadmision.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_registroadmisionActionPerformed(evt);
            }
        });
        JToolbarLateral.add(btn_registroadmision);

        btn_gruposprioritarios.setBackground(new java.awt.Color(255, 255, 255));
        btn_gruposprioritarios.setIcon(new javax.swing.ImageIcon(getClass().getResource("/principal/grupoprioritario.jpg"))); // NOI18N
        btn_gruposprioritarios.setText("Grupos Prioritarios");
        btn_gruposprioritarios.setToolTipText("Registro de admisión");
        btn_gruposprioritarios.setFocusable(false);
        btn_gruposprioritarios.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_gruposprioritarios.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_gruposprioritarios.setMaximumSize(new java.awt.Dimension(200, 30));
        btn_gruposprioritarios.setMinimumSize(new java.awt.Dimension(200, 30));
        btn_gruposprioritarios.setPreferredSize(new java.awt.Dimension(200, 30));
        btn_gruposprioritarios.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_gruposprioritariosActionPerformed(evt);
            }
        });
        JToolbarLateral.add(btn_gruposprioritarios);

        btn_datosantropometricos.setBackground(new java.awt.Color(255, 255, 255));
        btn_datosantropometricos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/principal/antropometricos.jpg"))); // NOI18N
        btn_datosantropometricos.setText("D. Antropométricos");
        btn_datosantropometricos.setToolTipText("  D. Antropométricos");
        btn_datosantropometricos.setFocusable(false);
        btn_datosantropometricos.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_datosantropometricos.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_datosantropometricos.setMaximumSize(new java.awt.Dimension(200, 30));
        btn_datosantropometricos.setMinimumSize(new java.awt.Dimension(200, 30));
        btn_datosantropometricos.setPreferredSize(new java.awt.Dimension(200, 30));
        JToolbarLateral.add(btn_datosantropometricos);

        btn_infoobstetrica.setBackground(new java.awt.Color(255, 255, 255));
        btn_infoobstetrica.setIcon(new javax.swing.ImageIcon(getClass().getResource("/principal/infoobstetrica.jpg"))); // NOI18N
        btn_infoobstetrica.setText("Info. Obstétrica");
        btn_infoobstetrica.setToolTipText("   Info. Obstétrica");
        btn_infoobstetrica.setFocusable(false);
        btn_infoobstetrica.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_infoobstetrica.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_infoobstetrica.setMaximumSize(new java.awt.Dimension(200, 30));
        btn_infoobstetrica.setMinimumSize(new java.awt.Dimension(200, 30));
        btn_infoobstetrica.setPreferredSize(new java.awt.Dimension(200, 30));
        JToolbarLateral.add(btn_infoobstetrica);

        btn_vacunacion.setBackground(new java.awt.Color(255, 255, 255));
        btn_vacunacion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/principal/vacunacion.jpg"))); // NOI18N
        btn_vacunacion.setText("Vacunación     ");
        btn_vacunacion.setToolTipText("      Vacunación     ");
        btn_vacunacion.setFocusable(false);
        btn_vacunacion.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_vacunacion.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_vacunacion.setMaximumSize(new java.awt.Dimension(200, 30));
        btn_vacunacion.setMinimumSize(new java.awt.Dimension(200, 30));
        btn_vacunacion.setPreferredSize(new java.awt.Dimension(200, 30));
        JToolbarLateral.add(btn_vacunacion);

        btn_sivan.setBackground(new java.awt.Color(255, 255, 255));
        btn_sivan.setIcon(new javax.swing.ImageIcon(getClass().getResource("/principal/procedimientosactividades.jpg"))); // NOI18N
        btn_sivan.setText("Datos Nutricionales");
        btn_sivan.setToolTipText("Datos Nutricionales");
        btn_sivan.setFocusable(false);
        btn_sivan.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_sivan.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_sivan.setMaximumSize(new java.awt.Dimension(200, 30));
        btn_sivan.setMinimumSize(new java.awt.Dimension(200, 30));
        btn_sivan.setPreferredSize(new java.awt.Dimension(200, 30));
        JToolbarLateral.add(btn_sivan);

        btn_exameneslaboratorio.setBackground(new java.awt.Color(255, 255, 255));
        btn_exameneslaboratorio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/principal/laboratorio.jpg"))); // NOI18N
        btn_exameneslaboratorio.setText("Examenes Lab.");
        btn_exameneslaboratorio.setToolTipText("     Examenes Lab.");
        btn_exameneslaboratorio.setFocusable(false);
        btn_exameneslaboratorio.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_exameneslaboratorio.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_exameneslaboratorio.setMaximumSize(new java.awt.Dimension(200, 30));
        btn_exameneslaboratorio.setMinimumSize(new java.awt.Dimension(200, 30));
        btn_exameneslaboratorio.setPreferredSize(new java.awt.Dimension(200, 30));
        JToolbarLateral.add(btn_exameneslaboratorio);

        btn_diagnosticos.setBackground(new java.awt.Color(255, 255, 255));
        btn_diagnosticos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/principal/diagnosticos.jpg"))); // NOI18N
        btn_diagnosticos.setText("Diagnósticos");
        btn_diagnosticos.setToolTipText("     Diagnósticos   ");
        btn_diagnosticos.setFocusable(false);
        btn_diagnosticos.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_diagnosticos.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_diagnosticos.setMaximumSize(new java.awt.Dimension(200, 30));
        btn_diagnosticos.setMinimumSize(new java.awt.Dimension(200, 30));
        btn_diagnosticos.setPreferredSize(new java.awt.Dimension(200, 30));
        JToolbarLateral.add(btn_diagnosticos);

        btn_datosviolencia.setBackground(new java.awt.Color(255, 255, 255));
        btn_datosviolencia.setIcon(new javax.swing.ImageIcon(getClass().getResource("/principal/violencia.jpg"))); // NOI18N
        btn_datosviolencia.setText("Datos Violencia");
        btn_datosviolencia.setToolTipText("Datos Violencia");
        btn_datosviolencia.setFocusable(false);
        btn_datosviolencia.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_datosviolencia.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_datosviolencia.setMaximumSize(new java.awt.Dimension(200, 30));
        btn_datosviolencia.setMinimumSize(new java.awt.Dimension(200, 30));
        btn_datosviolencia.setPreferredSize(new java.awt.Dimension(200, 30));
        btn_datosviolencia.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_datosviolenciaActionPerformed(evt);
            }
        });
        JToolbarLateral.add(btn_datosviolencia);

        btn_vihsida.setBackground(new java.awt.Color(255, 255, 255));
        btn_vihsida.setIcon(new javax.swing.ImageIcon(getClass().getResource("/principal/vihsida.jpg"))); // NOI18N
        btn_vihsida.setText("VIH");
        btn_vihsida.setToolTipText("         VIH");
        btn_vihsida.setFocusable(false);
        btn_vihsida.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_vihsida.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_vihsida.setMaximumSize(new java.awt.Dimension(200, 30));
        btn_vihsida.setMinimumSize(new java.awt.Dimension(200, 30));
        btn_vihsida.setPreferredSize(new java.awt.Dimension(200, 30));
        JToolbarLateral.add(btn_vihsida);

        btn_procedimientos.setBackground(new java.awt.Color(255, 255, 255));
        btn_procedimientos.setIcon(new javax.swing.ImageIcon(getClass().getResource("/principal/procedimientosactividades.jpg"))); // NOI18N
        btn_procedimientos.setText("Procedimientos / Actdes");
        btn_procedimientos.setToolTipText("   Procdtos / Actdes");
        btn_procedimientos.setFocusable(false);
        btn_procedimientos.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_procedimientos.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_procedimientos.setMaximumSize(new java.awt.Dimension(200, 30));
        btn_procedimientos.setMinimumSize(new java.awt.Dimension(200, 30));
        btn_procedimientos.setPreferredSize(new java.awt.Dimension(200, 30));
        JToolbarLateral.add(btn_procedimientos);

        btn_odontologia.setBackground(new java.awt.Color(255, 255, 255));
        btn_odontologia.setIcon(new javax.swing.ImageIcon(getClass().getResource("/principal/odontologia.jpg"))); // NOI18N
        btn_odontologia.setText("Odontología    ");
        btn_odontologia.setToolTipText("Odontología");
        btn_odontologia.setFocusable(false);
        btn_odontologia.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_odontologia.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_odontologia.setMaximumSize(new java.awt.Dimension(200, 30));
        btn_odontologia.setMinimumSize(new java.awt.Dimension(200, 30));
        btn_odontologia.setPreferredSize(new java.awt.Dimension(200, 30));
        JToolbarLateral.add(btn_odontologia);

        btn_prescripcion.setBackground(new java.awt.Color(255, 255, 255));
        btn_prescripcion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/principal/prescripcion.jpg"))); // NOI18N
        btn_prescripcion.setText("Prescripción   ");
        btn_prescripcion.setToolTipText("       Prescripción   ");
        btn_prescripcion.setFocusable(false);
        btn_prescripcion.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_prescripcion.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_prescripcion.setMaximumSize(new java.awt.Dimension(200, 30));
        btn_prescripcion.setMinimumSize(new java.awt.Dimension(200, 30));
        btn_prescripcion.setPreferredSize(new java.awt.Dimension(200, 30));
        JToolbarLateral.add(btn_prescripcion);

        btn_gruposvulnerables.setBackground(new java.awt.Color(255, 255, 255));
        btn_gruposvulnerables.setIcon(new javax.swing.ImageIcon(getClass().getResource("/principal/gruposvulnerables.jpg"))); // NOI18N
        btn_gruposvulnerables.setText("Grupos Vulnerables");
        btn_gruposvulnerables.setToolTipText("Grupos Vulnerables");
        btn_gruposvulnerables.setFocusable(false);
        btn_gruposvulnerables.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_gruposvulnerables.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_gruposvulnerables.setMaximumSize(new java.awt.Dimension(200, 30));
        btn_gruposvulnerables.setMinimumSize(new java.awt.Dimension(200, 30));
        btn_gruposvulnerables.setPreferredSize(new java.awt.Dimension(200, 30));
        btn_gruposvulnerables.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_gruposvulnerablesActionPerformed(evt);
            }
        });
        JToolbarLateral.add(btn_gruposvulnerables);

        btn_referencia.setBackground(new java.awt.Color(255, 255, 255));
        btn_referencia.setIcon(new javax.swing.ImageIcon(getClass().getResource("/principal/referencia.jpg"))); // NOI18N
        btn_referencia.setText("Ref/CRef/Interconsulta");
        btn_referencia.setToolTipText("Ref/CRef/Interconsulta");
        btn_referencia.setFocusable(false);
        btn_referencia.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_referencia.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_referencia.setMaximumSize(new java.awt.Dimension(200, 30));
        btn_referencia.setMinimumSize(new java.awt.Dimension(200, 30));
        btn_referencia.setPreferredSize(new java.awt.Dimension(200, 30));
        JToolbarLateral.add(btn_referencia);

        btn_reportes.setBackground(new java.awt.Color(255, 255, 255));
        btn_reportes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/principal/reportes.jpg"))); // NOI18N
        btn_reportes.setText("Reportes");
        btn_reportes.setToolTipText("Reportes");
        btn_reportes.setFocusable(false);
        btn_reportes.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_reportes.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_reportes.setMaximumSize(new java.awt.Dimension(200, 30));
        btn_reportes.setMinimumSize(new java.awt.Dimension(200, 30));
        btn_reportes.setPreferredSize(new java.awt.Dimension(200, 30));
        btn_reportes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_reportesActionPerformed(evt);
            }
        });
        JToolbarLateral.add(btn_reportes);

        btn_cierremes.setBackground(new java.awt.Color(255, 255, 255));
        btn_cierremes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/principal/cierremes.jpg"))); // NOI18N
        btn_cierremes.setText("Cierre Mes");
        btn_cierremes.setToolTipText("Cierre Mes");
        btn_cierremes.setFocusable(false);
        btn_cierremes.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_cierremes.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_cierremes.setMaximumSize(new java.awt.Dimension(200, 30));
        btn_cierremes.setMinimumSize(new java.awt.Dimension(200, 30));
        btn_cierremes.setPreferredSize(new java.awt.Dimension(200, 30));
        btn_cierremes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cierremesActionPerformed(evt);
            }
        });
        JToolbarLateral.add(btn_cierremes);

        btn_carga_pacientes.setBackground(new java.awt.Color(255, 255, 255));
        btn_carga_pacientes.setIcon(new javax.swing.ImageIcon(getClass().getResource("/principal/profesionales.jpg"))); // NOI18N
        btn_carga_pacientes.setText("Cargar Pacientes");
        btn_carga_pacientes.setFocusable(false);
        btn_carga_pacientes.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_carga_pacientes.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_carga_pacientes.setMaximumSize(new java.awt.Dimension(200, 30));
        btn_carga_pacientes.setMinimumSize(new java.awt.Dimension(200, 30));
        btn_carga_pacientes.setPreferredSize(new java.awt.Dimension(200, 30));
        btn_carga_pacientes.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_carga_pacientesActionPerformed(evt);
            }
        });
        JToolbarLateral.add(btn_carga_pacientes);

        btn_validararchivo.setBackground(new java.awt.Color(255, 255, 255));
        btn_validararchivo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/principal/archivovalido.jpg"))); // NOI18N
        btn_validararchivo.setText("Validar Archivo");
        btn_validararchivo.setFocusable(false);
        btn_validararchivo.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_validararchivo.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_validararchivo.setMaximumSize(new java.awt.Dimension(200, 30));
        btn_validararchivo.setMinimumSize(new java.awt.Dimension(200, 30));
        btn_validararchivo.setPreferredSize(new java.awt.Dimension(200, 30));
        btn_validararchivo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_validararchivoActionPerformed(evt);
            }
        });
        JToolbarLateral.add(btn_validararchivo);

        btn_cerrarsesion.setBackground(new java.awt.Color(255, 255, 255));
        btn_cerrarsesion.setIcon(new javax.swing.ImageIcon(getClass().getResource("/principal/salir.jpg"))); // NOI18N
        btn_cerrarsesion.setText("Cerrar Sesión");
        btn_cerrarsesion.setToolTipText("Cerrar Sesión");
        btn_cerrarsesion.setFocusable(false);
        btn_cerrarsesion.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        btn_cerrarsesion.setHorizontalTextPosition(javax.swing.SwingConstants.RIGHT);
        btn_cerrarsesion.setMaximumSize(new java.awt.Dimension(200, 30));
        btn_cerrarsesion.setMinimumSize(new java.awt.Dimension(200, 30));
        btn_cerrarsesion.setPreferredSize(new java.awt.Dimension(200, 30));
        btn_cerrarsesion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btn_cerrarsesionActionPerformed(evt);
            }
        });
        JToolbarLateral.add(btn_cerrarsesion);

        javax.swing.GroupLayout PANEL_LATERALLayout = new javax.swing.GroupLayout(PANEL_LATERAL);
        PANEL_LATERAL.setLayout(PANEL_LATERALLayout);
        PANEL_LATERALLayout.setHorizontalGroup(
            PANEL_LATERALLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, PANEL_LATERALLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(JToolbarLateral, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(41, 41, 41))
        );
        PANEL_LATERALLayout.setVerticalGroup(
            PANEL_LATERALLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(PANEL_LATERALLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(JToolbarLateral, javax.swing.GroupLayout.DEFAULT_SIZE, 616, Short.MAX_VALUE)
                .addContainerGap())
        );

        MenuBarRDACAA.setBackground(new java.awt.Color(255, 255, 255));

        menu_inicio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/principal/home.png"))); // NOI18N
        menu_inicio.setText("Inicio");
        MenuBarRDACAA.add(menu_inicio);

        setJMenuBar(MenuBarRDACAA);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(PANEL_LATERAL, javax.swing.GroupLayout.PREFERRED_SIZE, 227, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(dsp_contenedorRDACAA))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(dsp_contenedorRDACAA)
            .addComponent(PANEL_LATERAL, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btn_registroadmisionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_registroadmisionActionPerformed
        if (isClosed(containerWizard)) {
            center(containerWizard);
            disableControlsAttentionFlow(false);
        }
    }//GEN-LAST:event_btn_registroadmisionActionPerformed

    private void btn_gruposprioritariosActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_gruposprioritariosActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_gruposprioritariosActionPerformed

    private void btn_cerrarsesionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cerrarsesionActionPerformed
        int eleccion = JOptionPane.showConfirmDialog(null, "¿Desea salir del Sistema?", "RDACAA 2.0", JOptionPane.YES_NO_OPTION);
        if (eleccion == 0) {
        	
            this.setVisible(false);
            acceso.setVisible(true);
            reportesController.cerrar();
            if(vs != null) {
            	vs.setUtilitario(null);
            	vs.setInstanciaNull();
            }
            
        }
    }//GEN-LAST:event_btn_cerrarsesionActionPerformed

    private void btn_reportesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_reportesActionPerformed
        if (isClosed(reportes)) {
            center(reportes);
        }
    }//GEN-LAST:event_btn_reportesActionPerformed

    private void btn_datosviolenciaActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_datosviolenciaActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_datosviolenciaActionPerformed

    private void btn_gruposvulnerablesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_gruposvulnerablesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btn_gruposvulnerablesActionPerformed

    private void btn_cierremesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_cierremesActionPerformed
        if (isClosed(cierreMensual)) {
            cierreMensualController.loadMeses();
            center(cierreMensual);
        }
    }//GEN-LAST:event_btn_cierremesActionPerformed

    private void btn_validararchivoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_validararchivoActionPerformed
    	 if (isClosed(validationController.getValidationRdacaa())) {
             center(validationController.getValidationRdacaa());
         }
    }//GEN-LAST:event_btn_validararchivoActionPerformed

    private void btn_carga_pacientesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btn_carga_pacientesActionPerformed
        if (isClosed(cargaPacientesController.getCargaPacientes())) {
             center(cargaPacientesController.getCargaPacientes());
         }
    }//GEN-LAST:event_btn_carga_pacientesActionPerformed

    /**
     * @param JInternalFrame
     */
    public void center(JInternalFrame JInternalFrame) {
        try {
            dsp_contenedorRDACAA.add(JInternalFrame);
            JInternalFrame.setVisible(true);
            JInternalFrame.show();
            JInternalFrame.setMaximum(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(EscritorioPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public boolean isClosed(Object obj) {
        JInternalFrame[] activos = dsp_contenedorRDACAA.getAllFrames();
        boolean cerrado = true;
        int i = 0;
        while (i < activos.length && cerrado) {
            if (activos[i] == obj) {
                cerrado = false;
            }
            i++;
        }
        return cerrado;
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static final javax.swing.JToolBar JToolbarLateral = new javax.swing.JToolBar();
    public static final javax.swing.JMenuBar MenuBarRDACAA = new javax.swing.JMenuBar();
    private javax.swing.JPanel PANEL_LATERAL;
    public static final javax.swing.JButton btn_carga_pacientes = new javax.swing.JButton();
    public static final javax.swing.JButton btn_cerrarsesion = new javax.swing.JButton();
    public static final javax.swing.JButton btn_cierremes = new javax.swing.JButton();
    public static final javax.swing.JButton btn_datosantropometricos = new javax.swing.JButton();
    public static final javax.swing.JButton btn_datosviolencia = new javax.swing.JButton();
    public static final javax.swing.JButton btn_diagnosticos = new javax.swing.JButton();
    public static final javax.swing.JButton btn_exameneslaboratorio = new javax.swing.JButton();
    public static final javax.swing.JButton btn_gruposprioritarios = new javax.swing.JButton();
    public static final javax.swing.JButton btn_gruposvulnerables = new javax.swing.JButton();
    public static final javax.swing.JButton btn_infoobstetrica = new javax.swing.JButton();
    public static final javax.swing.JButton btn_odontologia = new javax.swing.JButton();
    public static final javax.swing.JButton btn_prescripcion = new javax.swing.JButton();
    public static final javax.swing.JButton btn_procedimientos = new javax.swing.JButton();
    public static final javax.swing.JButton btn_referencia = new javax.swing.JButton();
    public static final javax.swing.JButton btn_registroadmision = new javax.swing.JButton();
    public static final javax.swing.JButton btn_reportes = new javax.swing.JButton();
    public static final javax.swing.JButton btn_sivan = new javax.swing.JButton();
    public static final javax.swing.JButton btn_vacunacion = new javax.swing.JButton();
    public static final javax.swing.JButton btn_validararchivo = new javax.swing.JButton();
    public static final javax.swing.JButton btn_vihsida = new javax.swing.JButton();
    public static final javax.swing.JDesktopPane dsp_contenedorRDACAA = new javax.swing.JDesktopPane();
    private javax.swing.JLabel labelPrincipal;
    public static final javax.swing.JMenu menu_inicio = new javax.swing.JMenu();
    // End of variables declaration//GEN-END:variables
}
