/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.principal.controller;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.annotation.PostConstruct;
import javax.swing.JButton;
import javax.swing.JTextField;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;

import ec.gob.msp.rdacaa.business.entity.Usuario;
import ec.gob.msp.rdacaa.business.service.UsuarioService;
import ec.gob.msp.rdacaa.business.validation.common.ValidationResult;
import ec.gob.msp.rdacaa.control.swing.notifications.Notifications;
import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.principal.forma.Acceso;
import ec.gob.msp.rdacaa.principal.forma.CambioContrasenia;
import ec.gob.msp.rdacaa.seguridades.util.DatosLlave;
import ec.gob.msp.rdacaa.seguridades.util.SeguridadUtil;
import ec.gob.msp.rdacaa.utilitario.forma.UtilitarioForma;

/**
 *
 * @author dmurillo
 */
@Controller
public class CambioContraseniaController {
    private CambioContrasenia cambioContraseniaFrame;
    private Acceso accesoFrame;
    private JTextField txtContraseniaAnterior;
    private JTextField txtContraseniaNueva;
    private JTextField txtContraseniaConfirmar;
    private JButton btnAceptar;
    private JButton btnCancelar;
    
    private UsuarioService usuarioService;
    @Autowired
	private Environment env;
    
    @Autowired
    public CambioContraseniaController(CambioContrasenia cambioContraseniaFrame,
                                       UsuarioService usuarioService,
                                       Acceso accesoFrame
                                      ){
        this.cambioContraseniaFrame = cambioContraseniaFrame;
        this.usuarioService = usuarioService;
        this.accesoFrame = accesoFrame;
    }
    
    @PostConstruct
    private void init() {
        this.txtContraseniaAnterior = cambioContraseniaFrame.getTxtContraseniaAnterior();
        this.txtContraseniaNueva = cambioContraseniaFrame.getTxtContraseniaNueva();
        this.txtContraseniaConfirmar = cambioContraseniaFrame.getTxtContraseniaConfirmar();
        this.btnAceptar = cambioContraseniaFrame.getBtnAceptar();
        this.btnCancelar = cambioContraseniaFrame.getBtnCancelar();
        
        txtContraseniaConfirmar.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyPressed(java.awt.event.KeyEvent evt) {
                if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
                    processAccessUsuario();
                }
            }
        });
        
        btnAceptar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                processAccessUsuario();
            }
        });
        
        btnCancelar.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cancel();
            }
        });
        limpiarFormulario();
    }
    
    public void limpiarFormulario() {
        if(VariableSesion.getInstanciaVariablesSession().getUsuario() != null){
            txtContraseniaAnterior.setText(VariableSesion.getInstanciaVariablesSession().getUsuario().getLogin());
        }
        UtilitarioForma.limpiarCampos(cambioContraseniaFrame, txtContraseniaNueva,txtContraseniaConfirmar);
    }
    
    private boolean validateCajas(){
        boolean validar = false;
        List<ValidationResult> listaErrores = new ArrayList<>();
        String n = txtContraseniaNueva.getText();
        String cn = txtContraseniaConfirmar.getText();
        String a = txtContraseniaAnterior.getText();
        if (n.length() < 8 || n.length() > 20) {
            ValidationResult validationError = new ValidationResult("", "La clave debe estar compuesta entre 8 a 20 dígitos");
            listaErrores.add(validationError);
        }
        Pattern p = Pattern.compile("[a-zA-Z][0-9]");
        Matcher m = p.matcher(n);
        if (!m.find()) {
            ValidationResult validationError = new ValidationResult("", "La clave debe estar compuesta incialmente con letras y números");
            listaErrores.add(validationError);
        }
        if (!n.equals(cn)) {
            ValidationResult validationError = new ValidationResult("", "La clave no coincide con el confirmar");
            listaErrores.add(validationError);
        }
        if (n.equals(a)) {
            ValidationResult validationError = new ValidationResult("", "La clave anterior no debe coincidir con la nueva clave");
            listaErrores.add(validationError);
        }
        if (!listaErrores.isEmpty()) {
            Notifications.showFormValidationAlert(listaErrores, "Validación Forma");
        }else{
            validar = true;
        }
        return validar;
    }
    private void processAccessUsuario(){
        if(validateCajas()){
            try {
                
            	Usuario usuario = VariableSesion.getInstanciaVariablesSession().getUsuario();
                
                DatosLlave keyUsuario = SeguridadUtil.crearLlaveUnicaUsuario(
                		usuario.getLogin(), 
                		txtContraseniaNueva.getText().trim());
                        
                usuario.setClave(SeguridadUtil.hashContrasena(txtContraseniaNueva.getText().trim()));
                usuario.setKey(keyUsuario.getKey());
                usuario.setSalt(keyUsuario.getSalt());
                usuario.setPublickey(SeguridadUtil.cifrarPublicoString(txtContraseniaNueva.getText().trim(), 
                		SeguridadUtil.leerLlavePublica(env.getProperty("publickeyAuth"))));
                usuario.setCambiarclave(0);
                usuario.setFechacreacion(new Date());
                usuarioService.update(usuario);
                UtilitarioForma.muestraMensaje("Cambio contraseña", cambioContraseniaFrame, "INFO", "Proceso realizado correctamente por favor vuelva a ingresar");
            } catch (GeneralSecurityException | IOException e) {
                UtilitarioForma.muestraMensaje("Cambio contraseña", cambioContraseniaFrame, "ERROR", "Existe un problema "+e.getMessage());
            }
            cancel();
            accesoFrame.setVisible(true);
        }   
    }
    private void cancel(){
        cambioContraseniaFrame.dispose();
    }
}
