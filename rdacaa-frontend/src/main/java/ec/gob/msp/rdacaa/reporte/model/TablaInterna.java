package ec.gob.msp.rdacaa.reporte.model;

import lombok.Getter;
import lombok.Setter;

public class TablaInterna {
	@Getter
	@Setter
	private String nombre;
	@Getter
	@Setter
	private Integer cantidad;
	@Getter
	@Setter
	private Double porcentaje;
	
	public TablaInterna() {
		super();
	}

	public TablaInterna(String nombre, Integer cantidad, Double porcentaje) {
		super();
		this.nombre = nombre;
		this.cantidad = cantidad;
		this.porcentaje = porcentaje;
	}
	
}
