/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.amed.model;

import ec.gob.msp.rdacaa.business.entity.Entidad;
import ec.gob.msp.rdacaa.control.swing.model.CustomObjectToStringConverter;

/**
 *
 * @author eduardo
 */
public class EntidadToStringConverter extends CustomObjectToStringConverter{
    
    @Override
    public String getPreferredStringForItem(Object o) {
        if (o == null) {
            return null;
        } else if (o instanceof Entidad) {
            return ((Entidad) o).getNombreoficial();
        } else {
            return o.toString();
        }
    }
}
