package ec.gob.msp.rdacaa.principal.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.principal.forma.MenuPrincipal;

@Controller
public class MenuPrincipalController {

private MenuPrincipal frame;
	
	/**
	 * Create the application.
	 */
	@Autowired
	public MenuPrincipalController(MenuPrincipal frame) {
		this.frame = frame;
	}

	/**
	 * Launch the application.
	 */
	public void prepareAndOpenFrame() {
		frame.setVisible(true);
	}

	public void setVs(VariableSesion vs) {
		frame.setVs(vs);
	}	
	
}
