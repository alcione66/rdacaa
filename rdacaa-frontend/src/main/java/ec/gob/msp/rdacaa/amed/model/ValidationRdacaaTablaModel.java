/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.amed.model;

import java.util.Arrays;

import ec.gob.msp.rdacaa.utilitario.forma.ModeloTablaGenerico;

/**
 *
 * @author msp
 */
public class ValidationRdacaaTablaModel extends ModeloTablaGenerico<ValidationRdacaaDTO> {

	private static final long serialVersionUID = 174954059632053069L;

	public ValidationRdacaaTablaModel(String[] columnas) {
		super(columnas);
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		int ascii = 0x2713;
		String checkChar = Character.toString((char) ascii);
		ValidationRdacaaDTO obj = getListaDatos().get(rowIndex);
		switch (columnIndex) {
		case 0:
			return obj;
		case 1:
			return obj.getNombreArchivo();
		case 2:
			return obj.getRutaArchivo();
		case 3:
			String tablasValidas = "";
			if (obj.getIsValidTables() == ValidationRdacaaDTO.NO_EXISTE_CAMPO_EN_BASE_O_ERROR) {
				tablasValidas = "X error verificando tablas 'atencion' y 'persona' ";
			} else if (obj.getIsValidTables() == ValidationRdacaaDTO.INVALIDO) {
				tablasValidas = "X tabla 'atencion' o 'persona' no encontrada";
			} else if (obj.getIsValidTables() == ValidationRdacaaDTO.VALIDO) {
				tablasValidas = checkChar;
			}
			return tablasValidas;
		case 4:
			return messageByMes(checkChar, obj);
		case 5:
			String uuids = "";
			if (obj.getHasUUIDsValidos() == ValidationRdacaaDTO.NO_EXISTE_CAMPO_EN_BASE_O_ERROR) {
				uuids = "X error validando UUIDs o error con el archivo de base de datos";
			} else if (obj.getHasUUIDsValidos() == ValidationRdacaaDTO.INVALIDO) {
				uuids = "X error validando UUIDs revisar archivos exportados con los UUIDs inválidos";
			} else if (obj.getHasUUIDsValidos() == ValidationRdacaaDTO.VALIDO) {
				uuids = checkChar;
			}
			return uuids;
		case 6:
			return messageUuidsAtencionXPersona(checkChar, obj);
		case 7:
			String establecimiento = "";
			if (obj.getIsSameEstablecimiento() == ValidationRdacaaDTO.NO_EXISTE_CAMPO_EN_BASE_O_ERROR) {
				establecimiento = "X no existe campo COD_EST_SALUD o error con el archivo de base de datos";
			} else if (obj.getIsSameEstablecimiento() == ValidationRdacaaDTO.INVALIDO) {
				establecimiento = "X establecimientos diferentes";
			} else if (obj.getIsSameEstablecimiento() == ValidationRdacaaDTO.VALIDO) {
				establecimiento = checkChar;
			}
			return establecimiento;
		case 8:
			return messageProfesional(checkChar, obj);
		case 9:
			return messageByEstadoValidacion(checkChar, obj);
		default:
			return "";
		}
	}

	private Object messageProfesional(String checkChar, ValidationRdacaaDTO obj) {
		String profesional = "";
		if (obj.getIsSameProfesional() == ValidationRdacaaDTO.NO_EXISTE_CAMPO_EN_BASE_O_ERROR) {
			profesional = "X no existe campo COD_PROFESIONAL o error con el archivo de base de datos";
		} else if (obj.getIsSameProfesional() == ValidationRdacaaDTO.INVALIDO) {
			profesional = "X mas de un profesional de la salud";
		} else if (obj.getIsSameProfesional() == ValidationRdacaaDTO.VALIDO) {
			profesional = checkChar;
		}
		return profesional;
	}

	private Object messageUuidsAtencionXPersona(String checkChar, ValidationRdacaaDTO obj) {
		String uuidsAtencionXPersona = "";
		if (findValidacionUUIDsAtencionXPersona(obj,
				ValidacionUUIDsAtencionXPersona.NO_EXISTE_CAMPO_EN_BASE_O_ERROR)) {
			uuidsAtencionXPersona = "X error validando las atenciones por persona o error con el archivo de base de datos";
		} else if (findValidacionUUIDsAtencionXPersona(obj, ValidacionUUIDsAtencionXPersona.UUIDS_PERSONA_HAS_NULL)
				|| findValidacionUUIDsAtencionXPersona(obj,
						ValidacionUUIDsAtencionXPersona.UUIDS_ATENCION_HAS_NULL)) {
			uuidsAtencionXPersona = "X la tabla 'atencion' o 'persona' poseen UUIDs nulos";
		} else if (findValidacionUUIDsAtencionXPersona(obj,
				ValidacionUUIDsAtencionXPersona.UUIDS_ATENCION_X_PERSONA_IS_INVALID)) {
			uuidsAtencionXPersona = "X la tabla 'atencion' tiene uuids que no corresponden a ninguno de la tabla 'persona'";
		}else if(findValidacionUUIDsAtencionXPersona(obj,
				ValidacionUUIDsAtencionXPersona.UUIDS_ATENCION_X_PERSONA_IS_VALID)) {
			uuidsAtencionXPersona = checkChar;
		}
		return uuidsAtencionXPersona;
	}

	private boolean findValidacionUUIDsAtencionXPersona(ValidationRdacaaDTO obj,
			ValidacionUUIDsAtencionXPersona findValue) {
		return Arrays.stream(obj.getHasUUIDsAtencionXPersona()).anyMatch(val -> val == findValue);
	}

	private Object messageByMes(String checkChar, ValidationRdacaaDTO obj) {
		String mes = "";
		if (obj.getIsValidMes() == ValidationRdacaaDTO.NO_EXISTE_CAMPO_EN_BASE_O_ERROR) {
			mes = "X no existe campo FEC_ATENCION o error con el archivo de base de datos";
		} else if (obj.getIsValidMes() == ValidationRdacaaDTO.INVALIDO) {
			mes = "X archivo no corresponde al mes indicado";
		} else if (obj.getIsValidMes() == ValidationRdacaaDTO.VALIDO) {
			mes = checkChar;
		}
		return mes;
	}

	private Object messageByEstadoValidacion(String checkChar, ValidationRdacaaDTO obj) {
		String variables = "";
		if (obj.getEstadoValidacion() != null) {
			if (obj.getEstadoValidacion().equals(ValidacionVariables.CARGA_INICIAL_NO_VALIDACION)) {
				variables = "";
			} else if (obj.getEstadoValidacion().equals(ValidacionVariables.ARCHIVO_NULO)) {
				variables = "X error generando el archivo";
			} else if (obj.getEstadoValidacion().equals(ValidacionVariables.ARCHIVO_CON_ERRORES)) {
				variables = "X archivo tiene errores de validación";
			} else if (obj.getEstadoValidacion().equals(ValidacionVariables.ARCHIVO_SIN_ERRORES)) {
				variables = checkChar;
			}
		}
		return variables;
	}

}
