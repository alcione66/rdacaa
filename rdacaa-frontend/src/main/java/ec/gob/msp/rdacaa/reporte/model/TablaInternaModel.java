package ec.gob.msp.rdacaa.reporte.model;

import ec.gob.msp.rdacaa.utilitario.forma.ModeloTablaGenerico;

public class TablaInternaModel extends ModeloTablaGenerico<TablaInterna>  {

	private static final long serialVersionUID = -2947315200909474650L;

	public TablaInternaModel(String[] columnas) {
		super(columnas);
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		TablaInterna obj = getListaDatos().get(rowIndex);
		switch (columnIndex) {
		case 0:
			return "";
		case 1:
			return obj.getNombre();
		case 2:
			return obj.getCantidad();
		case 3:
			return obj.getPorcentaje();
		default:
			return "";
		}
	}

}
