/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.amed.model;

import ec.gob.msp.rdacaa.business.entity.Cie;
import ec.gob.msp.rdacaa.control.swing.model.CustomObjectToStringConverter;

/**
 *
 * @author eduardo
 */
public class CieDescripcionToStringConverter extends CustomObjectToStringConverter {
    
    @Override
    public String getPreferredStringForItem(Object o) {
        if (o == null) {
            return null;
        } else if (o instanceof Cie) {
        	if((((Cie) o).getNombre() == null) || ((Cie) o).getCodigo() == null ){
        		return ((Cie) o).getCodigo();
        	}else{
        		return ((Cie) o).getCodigo()+" - "+((Cie) o).getNombre();
        	}
        } else {
            return o.toString();
        }
    }
}
