/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.amed.model;

import lombok.Getter;
import lombok.Setter;

/**
 *
 * @author msp
 */
@Getter
@Setter
public class ValidationRdacaaDTO {

	public static final int NO_INICIALIZADO = -2;
	public static final int NO_EXISTE_CAMPO_EN_BASE_O_ERROR = -1;
	public static final int INVALIDO = 0;
	public static final int VALIDO = 1;

	private String nombreArchivo;
	private String rutaArchivo;
	private int isValidTables;
	private int isValidMes;
	private int hasUUIDsValidos;
	private ValidacionUUIDsAtencionXPersona[] hasUUIDsAtencionXPersona;
	private int isSameEstablecimiento;
	private int isSameProfesional;
	private ValidacionVariables estadoValidacion;

	public ValidationRdacaaDTO() {
		nombreArchivo = "";
		rutaArchivo = "";
		isValidTables = NO_INICIALIZADO;
		isValidMes = NO_INICIALIZADO;
		hasUUIDsValidos = NO_INICIALIZADO;
		hasUUIDsAtencionXPersona = new ValidacionUUIDsAtencionXPersona[] {
				ValidacionUUIDsAtencionXPersona.NO_INICIALIZADO };
		isSameEstablecimiento = NO_INICIALIZADO;
		isSameProfesional = NO_INICIALIZADO;
		estadoValidacion = ValidacionVariables.CARGA_INICIAL_NO_VALIDACION;
	}

}
