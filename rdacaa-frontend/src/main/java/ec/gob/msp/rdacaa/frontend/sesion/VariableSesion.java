/*
 * Copyright 2018 MINISTERIO DE SALUD PUBLICA - ECUADOR
 * Todos los derechos reservados
 */
package ec.gob.msp.rdacaa.frontend.sesion;

import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import com.google.crypto.tink.KeysetHandle;

import ec.gob.msp.rdacaa.business.entity.Atenciongprioritario;
import ec.gob.msp.rdacaa.business.entity.Atenciongvulnerable;
import ec.gob.msp.rdacaa.business.entity.Atencionmedica;
import ec.gob.msp.rdacaa.business.entity.Atencionobstetrica;
import ec.gob.msp.rdacaa.business.entity.Atencionprescripcion;
import ec.gob.msp.rdacaa.business.entity.Atencionsivan;
import ec.gob.msp.rdacaa.business.entity.Atencionviolencia;
import ec.gob.msp.rdacaa.business.entity.Detallecatalogo;
import ec.gob.msp.rdacaa.business.entity.Diagnosticoatencion;
import ec.gob.msp.rdacaa.business.entity.Entidad;
import ec.gob.msp.rdacaa.business.entity.Estrategiapaciente;
import ec.gob.msp.rdacaa.business.entity.Examenlaboratorio;
import ec.gob.msp.rdacaa.business.entity.Interconsulta;
import ec.gob.msp.rdacaa.business.entity.Intraextramural;
import ec.gob.msp.rdacaa.business.entity.Medicionpaciente;
import ec.gob.msp.rdacaa.business.entity.Odontologia;
import ec.gob.msp.rdacaa.business.entity.Persona;
import ec.gob.msp.rdacaa.business.entity.Procedimientoatencion;
import ec.gob.msp.rdacaa.business.entity.Referenciaatencion;
import ec.gob.msp.rdacaa.business.entity.Registrovacunacion;
import ec.gob.msp.rdacaa.business.entity.Signosvitalesalertas;
import ec.gob.msp.rdacaa.business.entity.Usuario;
import ec.gob.msp.rdacaa.business.entity.Vihsida;
import lombok.Getter;
import lombok.Setter;

/**
 * <b> Incluir aqui la descripcion de la clase. </b>
 *
 * @author David Murillo
 *         <p>
 *         [$Author: David Murillo $, $Date: 11/06/2018]
 *         </p>
 */
public class VariableSesion {

	private static VariableSesion instanciaVariablesSession = new VariableSesion();

	@Getter
	@Setter
	private Persona persona;
	@Getter
	@Setter
	private Integer aniosPersona;
	@Getter
	@Setter
	private Integer mesesPersona;
	@Getter
	@Setter
	private Integer diasPersona;
	@Getter
	@Setter
	private LocalDate fechaNacimiento;
	@Getter
	@Setter
	private Integer edadEnAniosPersona;
	@Getter
	@Setter
	private Integer edadEnMesesPersona;
	@Getter
	@Setter
	private Integer edadEnDiasPersona;
	@Getter
	@Setter
	private Intraextramural intraExtraMural;
	@Getter
	@Setter
	private List<Atenciongprioritario> gruposPrioritarios;
	@Getter
	@Setter
	private Atencionmedica atencionMedica;
	@Getter
	@Setter
	private Persona profesional;
	@Getter
	@Setter
	private Usuario usuario;
	@Getter
	@Setter
	private Atencionobstetrica atencionobstetrica;
	@Getter
	@Setter
	private Vihsida vihsida;
	@Getter
	@Setter
	private Boolean isHombre;
	@Getter
	@Setter
	private Boolean isMujer;
	@Getter
	@Setter
	private Boolean isInterSexual;
	@Getter
	@Setter
	private Entidad establecimientoSalud;
	@Getter
	@Setter
	private Boolean isEmbarazada;
	@Getter
	@Setter
	private Boolean banderaCambioSexoGrupoP;
	@Getter
	@Setter
	private Detallecatalogo ctEspecialidad;
	@Getter
	@Setter
	private List<Diagnosticoatencion> diagnosticoatencion;	
	@Getter
	@Setter
	private Atencionsivan atencionsivan;
	@Getter
	@Setter
	private List<Registrovacunacion> registrovacunacion;
	@Getter
	@Setter
	private Examenlaboratorio examenlaboratorio;
	@Getter
	@Setter
	private List<Procedimientoatencion> procedimientoatencion;
	@Getter
	@Setter
	private Referenciaatencion referencia;
	@Getter
	@Setter
	private Interconsulta interconsulta;
	@Getter
	@Setter
	private Atencionprescripcion atencionprescripcion;
	@Getter
	@Setter
	private Double semanasGestacion;
	@Getter
	@Setter
	private List<String> codeFormsPanels;
	@Getter
	@Setter
	private Date fechaAtencion;
	@Getter
	@Setter
	private Odontologia odontologia;
	@Getter
	@Setter
	private Estrategiapaciente estrategiapaciente;
	@Getter
	@Setter
	private Atencionviolencia atencionviolencia;
    @Getter
	@Setter
    private List<Atenciongvulnerable> atenciongvulnerable;
	@Getter
	@Setter
	private Medicionpaciente medicionPeso;
	@Getter
	@Setter
	private Medicionpaciente medicionTalla;
	@Getter
	@Setter
	private Medicionpaciente medicionPerimetroCefalico;
	@Getter
	@Setter
	private Medicionpaciente medicionTallaCorregida;
	@Getter
	@Setter
	private Medicionpaciente tipoTomaTalla;
	@Getter
	@Setter
	private Medicionpaciente medicionIMC;
	@Getter
	@Setter
	private Signosvitalesalertas clasificacionIMC;
	@Getter
	@Setter
	private Medicionpaciente valorHBRiesgo;
	@Getter
	@Setter
	private Medicionpaciente valorHBRiesgoCorregido;
	@Getter
	@Setter
	private Signosvitalesalertas indicadorAnemiaHBRiesgoCorregido;
	@Getter
	@Setter
	private Medicionpaciente scoreZTallaParaEdad;
	@Getter
	@Setter
	private Signosvitalesalertas categoriaTallaParaEdad;
	@Getter
	@Setter
	private Medicionpaciente scoreZPesoParaEdad;
	@Getter
	@Setter
	private Signosvitalesalertas categoriaPesoParaEdad;
	@Getter
	@Setter
	private Medicionpaciente scoreZPesoParaLongitudTalla;
	@Getter
	@Setter
	private Signosvitalesalertas categoriaPesoParaLongitudTalla;
	@Getter
	@Setter
	private Medicionpaciente scoreZIMCParaEdad;
	@Getter
	@Setter
	private Signosvitalesalertas categoriaIMCParaEdad;
	@Getter
	@Setter
	private Medicionpaciente scoreZPerimetroCefalicoParaEdad;
	@Getter
	@Setter
	private Signosvitalesalertas categoriaPerimetroCefalicoParaEdad;
	@Getter
	@Setter
	private byte[] utilitario;
	@Getter
	@Setter
	private KeysetHandle keysetPublico;
	@Getter
	@Setter
	private int cambioInformacion;
	@Getter
	@Setter
	private int cambioInformacionDiagnostico;
	@Getter
	@Setter
	private int cambioInformacionVacunas;
        

	private VariableSesion() {
	}

	/**
	 * @return the instanciaParametros
	 */
	public static VariableSesion getInstanciaVariablesSession() {
		return instanciaVariablesSession;
	}

	public static void setInstanciaParametros(VariableSesion ainstanciaVariablesSession) {
		instanciaVariablesSession = ainstanciaVariablesSession;
	}

	public void setInstanciaNull() {
		persona = null;
		aniosPersona = 0;
		mesesPersona = 0;
		diasPersona = 0;
		intraExtraMural = null;
		gruposPrioritarios = null;
                atenciongvulnerable = null;
		atencionMedica = null;
		atencionobstetrica = null;
		vihsida = null;
		isHombre = null;
		isMujer = null;
		isInterSexual = null;
		isEmbarazada = null;
		banderaCambioSexoGrupoP = null;
		diagnosticoatencion = null;
		atencionsivan = null;
		registrovacunacion = null;
		examenlaboratorio = null;
		procedimientoatencion = null;
		referencia = null;
		interconsulta = null;
		atencionprescripcion = null;
		semanasGestacion = null;
		fechaAtencion = null;
		odontologia = null;
		estrategiapaciente = null;
		atencionviolencia = null;
		edadEnAniosPersona = null;
		edadEnMesesPersona = null;
		edadEnDiasPersona = null;
		fechaNacimiento = null;

		medicionPeso = null;
		medicionTalla = null;
		medicionPerimetroCefalico = null;
		medicionTallaCorregida = null;
		tipoTomaTalla = null;
		medicionIMC = null;
		clasificacionIMC = null;
		valorHBRiesgo = null;
		valorHBRiesgoCorregido = null;
		indicadorAnemiaHBRiesgoCorregido = null;
		scoreZTallaParaEdad = null;
		categoriaTallaParaEdad = null;
		scoreZPesoParaEdad = null;
		categoriaPesoParaEdad = null;
		scoreZPesoParaLongitudTalla = null;
		categoriaPesoParaLongitudTalla = null;
		scoreZIMCParaEdad = null;
		categoriaIMCParaEdad = null;
		scoreZPerimetroCefalicoParaEdad = null;
		categoriaPerimetroCefalicoParaEdad = null;
		cambioInformacion = 0;
		cambioInformacionDiagnostico = 0;
		cambioInformacionVacunas = 0;
		codeFormsPanels = null;
	}
}
