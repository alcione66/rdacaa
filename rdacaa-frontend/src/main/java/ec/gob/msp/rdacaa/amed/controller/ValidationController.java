/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.amed.controller;

import java.awt.BorderLayout;
import java.awt.Dialog;
import java.awt.Window;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Month;
import java.time.Year;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.annotation.PostConstruct;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JProgressBar;
import javax.swing.JTable;
import javax.swing.SwingUtilities;
import javax.swing.SwingWorker;
import javax.swing.filechooser.FileFilter;
import javax.swing.filechooser.FileNameExtensionFilter;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.google.common.collect.Lists;

import ec.gob.msp.rdacaa.amed.forma.ValidationRdacaa;
import ec.gob.msp.rdacaa.amed.model.ValidacionCmbAnioModel;
import ec.gob.msp.rdacaa.amed.model.ValidacionCmbMesModel;
import ec.gob.msp.rdacaa.amed.model.ValidacionUUIDsAtencionXPersona;
import ec.gob.msp.rdacaa.amed.model.ValidacionVariables;
import ec.gob.msp.rdacaa.amed.model.ValidationCmbMesCellRenderer;
import ec.gob.msp.rdacaa.amed.model.ValidationRdacaaDTO;
import ec.gob.msp.rdacaa.amed.model.ValidationRdacaaTablaModel;
import ec.gob.msp.rdacaa.business.validation.database.common.CipheringPublicKeyPlainDataFunction;
import ec.gob.msp.rdacaa.business.validation.database.common.MapperFunction;
import ec.gob.msp.rdacaa.business.validation.database.common.PreparingResultsFunction;
import ec.gob.msp.rdacaa.business.validation.database.common.ValidatorFunction;
import ec.gob.msp.rdacaa.control.swing.personalizado.ComboIess;
import ec.gob.msp.rdacaa.principal.forma.EscritorioPrincipal;
import ec.gob.msp.rdacaa.utilitario.exportdatabase.SQLiteUtil;
import ec.gob.msp.rdacaa.utilitario.forma.FormatoTabla;
import lombok.Getter;

/**
 *
 * @author msp
 */
@Controller
public class ValidationController {

	private static final Logger logger = LoggerFactory.getLogger(ValidationController.class);

	@Autowired
	private MapperFunction mapperFunction;

	@Autowired
	private ValidatorFunction validatorFunction;

	@Autowired
	private PreparingResultsFunction preparingResultsFunction;

	@Autowired
	private CipheringPublicKeyPlainDataFunction cipheringPublicKeyPlainDataFunction;

	@Getter
	private final ValidationRdacaa validationRdacaa;

	private JTable tablaArchivosAValidar;

	private Map<String, String> mapArchivosAValidar;

	private static final String UUIDS_PERSONA = "uuids-persona";
	private static final String UUIDS_ATENCION = "uuids-atencion";

	@Autowired
	public ValidationController(ValidationRdacaa validationRdacaa) {
		this.validationRdacaa = validationRdacaa;
	}

	@PostConstruct
	private void init() {
		mapArchivosAValidar = new HashMap<>();
		this.tablaArchivosAValidar = validationRdacaa.getTablaArchivosAValidar();
		ComboIess cmbAnios = validationRdacaa.getCmbAnios();
		ComboIess cmbMes = validationRdacaa.getCmbMes();

		JButton btnAgregar = validationRdacaa.getBtnAgregar();
		JButton btnLimpiar = validationRdacaa.getBtnLimpiar();
		JButton btnValidar = validationRdacaa.getBtnValidar();
		JButton btnRegresar = validationRdacaa.getBtnRegresar();

		initComboAnios(cmbAnios);
		initComboMes(cmbMes);
		initTablaArchivosAValidar();

		btnAgregar.addActionListener(l -> {
			agregarArchivosAValidar();
			cargarTablaArchivosAValidar();
		});
		btnLimpiar.addActionListener(l -> limpiarListaDeArchivos());
		btnValidar.addActionListener(l -> initValidationArchivosRdacaa());
		btnRegresar.addActionListener(l -> cerrarValidationPanel());

		cmbAnios.addActionListener(l -> {
			ValidationRdacaaTablaModel modeloTabla = (ValidationRdacaaTablaModel) tablaArchivosAValidar.getModel();
			modeloTabla.clear();
			cargarTablaArchivosAValidar();
		});
		cmbMes.addActionListener(l -> {
			ValidationRdacaaTablaModel modeloTabla = (ValidationRdacaaTablaModel) tablaArchivosAValidar.getModel();
			modeloTabla.clear();
			cargarTablaArchivosAValidar();
		});
	}

	private void initValidationArchivosRdacaa() {
		Window topFrame = (Window) SwingUtilities.getAncestorOfClass(Window.class,
				validationRdacaa);
		JDialog dlgProgress = new JDialog(topFrame, "Por favor espere...", Dialog.ModalityType.APPLICATION_MODAL);
		JLabel lblStatus = new JLabel("Validando archivos..."); 

		JProgressBar pbProgress = new JProgressBar(0, 100);
		pbProgress.setIndeterminate(true);

		dlgProgress.add(BorderLayout.NORTH, lblStatus);
		dlgProgress.add(BorderLayout.CENTER, pbProgress);
		dlgProgress.setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE); 
		dlgProgress.setSize(300, 90);
		dlgProgress.setLocationRelativeTo(null);

		SwingWorker<Void, Void> sw = new SwingWorker<Void, Void>() {
			@Override
			protected Void doInBackground() throws Exception {
				validarArchivosRdacaa();
				return null;
			}

			@Override
			protected void done() {
				dlgProgress.dispose();
			}
		};

		sw.execute(); 
		dlgProgress.setVisible(true); 
	}

	private void validarArchivosRdacaa() {
		ValidationRdacaaTablaModel modeloTabla = (ValidationRdacaaTablaModel) tablaArchivosAValidar.getModel();
		List<ValidationRdacaaDTO> listaArchivos = modeloTabla.getListaDatos();

		for (ValidationRdacaaDTO validationRdacaaDTO : listaArchivos) {
			boolean hasUUIDsAtencionXPersonaValid = Arrays.stream(validationRdacaaDTO.getHasUUIDsAtencionXPersona())
					.anyMatch(val -> val == ValidacionUUIDsAtencionXPersona.UUIDS_ATENCION_X_PERSONA_IS_VALID);
			if (validationRdacaaDTO.getIsValidTables() == ValidationRdacaaDTO.VALIDO
					&& validationRdacaaDTO.getIsValidMes() == ValidationRdacaaDTO.VALIDO
					&& validationRdacaaDTO.getHasUUIDsValidos() == ValidationRdacaaDTO.VALIDO
					&& hasUUIDsAtencionXPersonaValid
					&& validationRdacaaDTO.getIsSameEstablecimiento() == ValidationRdacaaDTO.VALIDO
					&& validationRdacaaDTO.getIsSameProfesional() == ValidationRdacaaDTO.VALIDO) {
				File validatedFile = validarArchivoIndividual(validationRdacaaDTO.getRutaArchivo());
				chooseEstadoValidacion(validationRdacaaDTO, validatedFile);
			}
		}
		modeloTabla.fireTableDataChanged();
	}

	private void chooseEstadoValidacion(ValidationRdacaaDTO validationRdacaaDTO, File validatedFile) {
		if (validatedFile != null) {
			try {
				boolean isValidatedFileWithoutErrors = SQLiteUtil
						.isValidatedFileWithoutErrors(validatedFile.getAbsolutePath());
				validationRdacaaDTO
						.setEstadoValidacion(isValidatedFileWithoutErrors ? ValidacionVariables.ARCHIVO_SIN_ERRORES
								: ValidacionVariables.ARCHIVO_CON_ERRORES);
			} catch (FileNotFoundException e) {
				logger.error("Error guardando detalle de excepción de conteo de errores {}", e);
			}
		} else {
			validationRdacaaDTO.setEstadoValidacion(ValidacionVariables.ARCHIVO_NULO);
		}
	}

	private File validarArchivoIndividual(String pathToFile) {
		File validatedFile;
		try {
			validatedFile = SQLiteUtil.exportAfterValidationMeshPlainFile(pathToFile,
					cipheringPublicKeyPlainDataFunction, mapperFunction, validatorFunction, preparingResultsFunction);
			return validatedFile;
		} catch (IOException e) {
			logger.error("Error guardando detalle de excepción de validación de variables {}", e);
		}
		return null;

	}

	private void cerrarValidationPanel() {
		validationRdacaa.dispose();
		EscritorioPrincipal.dsp_contenedorRDACAA.remove(validationRdacaa);
	}

	private void initTablaArchivosAValidar() {
		tablaArchivosAValidar.setDefaultRenderer(Object.class, new FormatoTabla());
		String[] columnNames = { "id", "Nombre", "Ruta", "Tablas Atención y Persona", "Validación mes", "UUIDs Válidos",
				"UUIDs Atención x Persona", "Establecimiento", "Profesional", "Validación variables" };

		ValidationRdacaaTablaModel modeloTabla = new ValidationRdacaaTablaModel(columnNames);
		tablaArchivosAValidar.setModel(modeloTabla);

		modeloTabla.ocultaColumnaCodigo(tablaArchivosAValidar);
	}

	@SuppressWarnings("unchecked")
	private void initComboAnios(ComboIess cmbAnios) {
		Year anioActual = Year.now();
		Year anioAnterior = anioActual.minusYears(1);
		List<Year> listaAnios = Lists.newArrayList(anioActual, anioAnterior);
		ValidacionCmbAnioModel validacionCmbAnioModel = new ValidacionCmbAnioModel();
		validacionCmbAnioModel.addElements(listaAnios);
		cmbAnios.setModel(validacionCmbAnioModel);
	}

	@SuppressWarnings("unchecked")
	private void initComboMes(ComboIess cmbMes) {
		Month[] meses = Month.values();
		List<Month> listaMeses = Arrays.asList(meses);
		ValidacionCmbMesModel validacionCmbMesModel = new ValidacionCmbMesModel();
		validacionCmbMesModel.addElements(listaMeses);
		cmbMes.setModel(validacionCmbMesModel);
		cmbMes.setRenderer(new ValidationCmbMesCellRenderer());
	}

	private void agregarArchivosAValidar() {
		FileFilter filter = new FileNameExtensionFilter(".rdacaa", "rdacaa");
		JFileChooser chooser = new JFileChooser();
		chooser.setMultiSelectionEnabled(true);
		chooser.setAcceptAllFileFilterUsed(false);
		chooser.setFileFilter(filter);
		int rVal = chooser.showOpenDialog(tablaArchivosAValidar);
		if (rVal == JFileChooser.APPROVE_OPTION) {
			File[] files = chooser.getSelectedFiles();
			for (int i = 0; i < files.length; i++) {
				File file = files[i];
				mapArchivosAValidar.put(file.getName(), file.getAbsolutePath());
			}
		}
	}

	private void cargarTablaArchivosAValidar() {
		ValidationRdacaaTablaModel modeloTabla = (ValidationRdacaaTablaModel) tablaArchivosAValidar.getModel();
		List<ValidationRdacaaDTO> listaDatos = new ArrayList<>();
		for (Map.Entry<String, String> entry : mapArchivosAValidar.entrySet()) {

			Optional<ValidationRdacaaDTO> findLoadedFiles = modeloTabla.getListaDatos().stream()
					.filter(dtoTemp -> dtoTemp.getRutaArchivo().equals(entry.getValue())).findFirst();

			if (!findLoadedFiles.isPresent()) {
				ValidationRdacaaDTO dto = new ValidationRdacaaDTO();
				dto.setNombreArchivo(entry.getKey());
				dto.setRutaArchivo(entry.getValue());
				dto.setIsValidTables(validarEstructuraArchivo(entry.getValue()));
				if (dto.getIsValidTables() == ValidationRdacaaDTO.VALIDO) {
					dto.setIsValidMes(validarAtencionesFechas(entry.getValue()));
					dto.setHasUUIDsValidos(validarUUIds(entry.getValue()));
					dto.setHasUUIDsAtencionXPersona(validarUUIDsAtencionXPersona(entry.getValue()));
					dto.setIsSameEstablecimiento(validarEstablecimiento(entry.getValue()));
					dto.setIsSameProfesional(validarProfesional(entry.getValue()));
					dto.setEstadoValidacion(ValidacionVariables.CARGA_INICIAL_NO_VALIDACION);
				}
				listaDatos.add(dto);
			}
		}
		modeloTabla.addFila(listaDatos);

		tablaArchivosAValidar.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);

		for (int i = 1; i < tablaArchivosAValidar.getColumnModel().getColumnCount(); i++) {
			tablaArchivosAValidar.getColumnModel().getColumn(i).setPreferredWidth(150);
		}

	}

	private ValidacionUUIDsAtencionXPersona[] validarUUIDsAtencionXPersona(String pathToFile) {
		Optional<int[]> optionalFileHasUUIDsAtencionXPersona = SQLiteUtil.fileHasUUIDsAtencionXPersona(pathToFile);

		if (!optionalFileHasUUIDsAtencionXPersona.isPresent()) {
			return new ValidacionUUIDsAtencionXPersona[] {
					ValidacionUUIDsAtencionXPersona.NO_EXISTE_CAMPO_EN_BASE_O_ERROR };
		} else {
			int[] hasUUIDsAtencionXPersonaArray = optionalFileHasUUIDsAtencionXPersona.get();

			int countUuidsPersonaNull = hasUUIDsAtencionXPersonaArray[0];
			int countUuidsAtencionNull = hasUUIDsAtencionXPersonaArray[1];
			int hasUUIDsAtencionXPersona = hasUUIDsAtencionXPersonaArray[2];

			ValidacionUUIDsAtencionXPersona[] validacionUUIDsAtencionXPersonaArray = new ValidacionUUIDsAtencionXPersona[3];
			validacionUUIDsAtencionXPersonaArray[0] = countUuidsPersonaNull > 0
					? ValidacionUUIDsAtencionXPersona.UUIDS_PERSONA_HAS_NULL
					: ValidacionUUIDsAtencionXPersona.UUIDS_PERSONA_OK;
			validacionUUIDsAtencionXPersonaArray[1] = countUuidsAtencionNull > 0
					? ValidacionUUIDsAtencionXPersona.UUIDS_ATENCION_HAS_NULL
					: ValidacionUUIDsAtencionXPersona.UUIDS_ATENCION_OK;
			validacionUUIDsAtencionXPersonaArray[2] = hasUUIDsAtencionXPersona == 1
					? ValidacionUUIDsAtencionXPersona.UUIDS_ATENCION_X_PERSONA_IS_VALID
					: ValidacionUUIDsAtencionXPersona.UUIDS_ATENCION_X_PERSONA_IS_INVALID;
			return validacionUUIDsAtencionXPersonaArray;
		}
	}

	private int validarUUIds(String pathToFile) {
		Optional<List<List<String[]>>> optionalResultadosUUID = SQLiteUtil.fileHasValidUUID(pathToFile);
		if (!optionalResultadosUUID.isPresent()) {
			return ValidationRdacaaDTO.NO_EXISTE_CAMPO_EN_BASE_O_ERROR;
		} else {
			List<List<String[]>> listasUUIDs = optionalResultadosUUID.get();
			List<String[]> listaInvalidUUIDsPersona = listasUUIDs.get(0);
			List<String[]> listaInvalidUUIDsAtencion = listasUUIDs.get(1);

			if (listaInvalidUUIDsPersona.isEmpty() && listaInvalidUUIDsAtencion.isEmpty()) {
				return ValidationRdacaaDTO.VALIDO;
			} else {
				if (!listaInvalidUUIDsPersona.isEmpty()) {
					exportUUIDsWithError(pathToFile, listaInvalidUUIDsPersona, UUIDS_PERSONA);
				}
				if (!listaInvalidUUIDsAtencion.isEmpty()) {
					exportUUIDsWithError(pathToFile, listaInvalidUUIDsAtencion, UUIDS_ATENCION);
				}
				return ValidationRdacaaDTO.INVALIDO;
			}
		}
	}

	private void exportUUIDsWithError(String pathToFile, List<String[]> listaInvalidUUIDs, String tipoLista) {
		String fileName = pathToFile + "-" + tipoLista + ".csv";

		try (FileWriter writer = new FileWriter(fileName);) {
			for (String[] str : listaInvalidUUIDs) {
				String uuid = str[0];
				Integer countUUID = Integer.valueOf(str[1]);
				writer.write(uuid + ";" + countUUID + ";");
				writer.write(System.getProperty("line.separator"));
			}
		} catch (Exception e) {
			logger.error("Error exportando lista de UUIDs tipo{}, {}", tipoLista, e);
		}
	}

	private int validarProfesional(String pathToFile) {
		return SQLiteUtil.isFileSameProfesional(pathToFile);
	}

	private int validarEstablecimiento(String pathToFile) {
		return SQLiteUtil.isFileSameEstablecimiento(pathToFile);
	}

	private void limpiarListaDeArchivos() {
		mapArchivosAValidar.clear();
		ValidationRdacaaTablaModel modeloTabla = (ValidationRdacaaTablaModel) tablaArchivosAValidar.getModel();
		modeloTabla.clear();
	}

	private int validarEstructuraArchivo(String pathToFile) {
		return SQLiteUtil.isFileStructureValid(pathToFile);
	}

	private int validarAtencionesFechas(String pathToFile) {
		try {
			java.sql.Date fechaInicio = new java.sql.Date(getFechaInicio().getTime());
			java.sql.Date fechaFin = new java.sql.Date(getFechaFin().getTime());
			logger.debug("fechaInicio {}", fechaInicio);
			logger.debug("fechaFin {}", fechaFin);
			return SQLiteUtil.isFileDatesValid(pathToFile, fechaInicio, fechaFin);
		} catch (ParseException e) {
			logger.error("Error convirtiendo fecha de String a Date {}", e);
			return ValidationRdacaaDTO.INVALIDO;
		}
	}

	private Date getFechaInicio() throws ParseException {
		ComboIess cmbAnios = validationRdacaa.getCmbAnios();
		ComboIess cmbMes = validationRdacaa.getCmbMes();
		String fechaAniosMes = cmbAnios.getSelectedItem().toString() + "-"
				+ StringUtils.leftPad(String.valueOf(((Month) cmbMes.getSelectedItem()).getValue()), 2, "0") + "-01";
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		return format.parse(fechaAniosMes);
	}

	private Date getFechaFin() throws ParseException {
		Calendar fechaFinCal = Calendar.getInstance();
		fechaFinCal.setTime(getFechaInicio());
		fechaFinCal.add(Calendar.MONTH, 1);
		fechaFinCal.add(Calendar.DAY_OF_YEAR, -1);
		return fechaFinCal.getTime();
	}
}
