/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.amed.controller;

import java.awt.event.ActionEvent;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.filechooser.FileNameExtensionFilter;
import javax.xml.bind.JAXBException;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.opencsv.CSVWriter;

import ec.gob.msp.rdacaa.amed.forma.CargaPacientes;
import ec.gob.msp.rdacaa.business.entity.Persona;
import ec.gob.msp.rdacaa.business.service.DetallecatalogoService;
import ec.gob.msp.rdacaa.business.service.PaisService;
import ec.gob.msp.rdacaa.business.service.ParroquiaService;
import ec.gob.msp.rdacaa.business.service.PersonaService;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.principal.forma.EscritorioPrincipal;
import ec.gob.msp.rdacaa.utilitario.fecha.FechasUtil;
import ec.gob.msp.rdacaa.utilitario.forma.UtilitarioForma;
import lombok.Getter;

/**
 *
 * @author eduardo
 */
@Controller
public class CargaPacientesController {
    
        private static final org.slf4j.Logger logger = LoggerFactory.getLogger(ValidationController.class);
        private static final String MENSAJE_ERROR = "ERROR";
        @Autowired
        private CargaPacientes cargaPacientesFrame;
        @Getter
	private final CargaPacientes cargaPacientes;
        private JFileChooser abrirArchivo;
        private JButton btnCargarPacientes;
        private Persona persona;
        private boolean cabecera;
        private boolean isArchivoValido;
        @Autowired
        private PersonaService personaService;
        @Autowired
        private ParroquiaService parroquiaService;
        @Autowired
        private DetallecatalogoService detallecatalogoService;
        @Autowired
        private PaisService paisService;
//        private List<String> listaErrores;
        private List< String > listaErrores;
        private String pathFile;
        private JLabel alertaCargaPacientes;
        private JLabel alertaCondicionesArchivo;
        
        @Autowired
	public CargaPacientesController(CargaPacientes cargaPacientes) {
                this.isArchivoValido = true;
		this.cargaPacientes = cargaPacientes;
        }
        @PostConstruct
	private void init() {
            this.alertaCondicionesArchivo = cargaPacientesFrame.getJLabel2();
            this.alertaCargaPacientes = cargaPacientesFrame.getJLabel1();
            this.btnCargarPacientes = cargaPacientesFrame.getBtnCargarPacientes();
            btnCargarPacientes.addActionListener((ActionEvent e) -> {
                try {
                    cargarPacientes();
                } catch (FileNotFoundException ex) {
                    Logger.getLogger(CargaPacientesController.class.getName()).log(Level.SEVERE, null, ex);
                } catch (IOException ex) {
                    Logger.getLogger(CargaPacientesController.class.getName()).log(Level.SEVERE, null, ex);
                }
        });
            JButton btnRegresar = cargaPacientesFrame.getBtnRegresar();
            btnRegresar.addActionListener(l -> cerrarValidationPanel());
            btnRegresar.addActionListener(l -> alertaCargaPacientes.setText(""));
            alertaCondicionesArchivo.setText(createAlertCargaArchivo());
        }
        
        private void cerrarValidationPanel() {
		cargaPacientesFrame.dispose();
		EscritorioPrincipal.dsp_contenedorRDACAA.remove(cargaPacientesFrame);
	}
        
        private void cargarPacientes() throws FileNotFoundException, IOException  {

            
        if (abrirArchivo == null){
            abrirArchivo = new JFileChooser();
            alertaCargaPacientes.setText("");
            FileNameExtensionFilter filtro = new FileNameExtensionFilter("Archivos .csv", "csv");
            abrirArchivo.setFileFilter(filtro);
            abrirArchivo.setFileSelectionMode(JFileChooser.FILES_ONLY);
            int resultado = abrirArchivo.showOpenDialog(cargaPacientesFrame);
            if (resultado == JFileChooser.APPROVE_OPTION) {
                
                File f = abrirArchivo.getSelectedFile();
                pathFile= abrirArchivo.getCurrentDirectory().getAbsolutePath();
                try {
                    if (processFile(f,pathFile)) {
                        abrirArchivo.cancelSelection();
                        abrirArchivo = null;
                        UtilitarioForma.muestraMensaje("", cargaPacientesFrame, "INFO", "Pacientes Cargados correctamente");
                        cabecera = true;
                    }else{
                        abrirArchivo = null;
                    }
                } catch (JAXBException ex) {
                    abrirArchivo = null;
                    UtilitarioForma.muestraMensaje("", cargaPacientesFrame, MENSAJE_ERROR, "existe un problema con la carga de pacientes " + ex.getMessage());
                }
            }else{
                abrirArchivo = null;
            } 
            
        }
    }
        

        private boolean processFile(File archivo, String pathFile) throws JAXBException, FileNotFoundException, IOException {
            int registro = 0;
            int numArchivo = 0;
            int numColumnas = 0;
            listaErrores = new ArrayList<>();
            boolean isFechaValida = false;
            cabecera = false;
            BufferedReader input = new BufferedReader(new FileReader(archivo));
            String linea;
            List< List<String> > filas = new ArrayList<>();
            while ((linea = input.readLine()) != null){
                char arregloCadena[] = linea.toCharArray();
                String filaString = new String(arregloCadena);
                List<String> contenidoFila = Arrays.asList(filaString.split(";"));
                
                        
                if (cabecera != false) {
                  if (!contenidoFila.isEmpty()){
                      System.out.println("Identificacion: "+contenidoFila.get(2));
                    if (personaService.findOneByPersonaNumIdentificacion(contenidoFila.get(2)) == null ) {
                        Integer edadAniosMesDias = 0;
                        Date fechaSistema = new Date();
                        String fechaSistemaFormt = "DD/MM/YYYY";
                        SimpleDateFormat objSDF = new SimpleDateFormat(fechaSistemaFormt);
                        Date fecNac = FechasUtil.formateadorStringAFecha("dd/mm/yyyy", contenidoFila.get(7));
                        Date fecSis = FechasUtil.formateadorStringAFecha("dd/mm/yyyy",objSDF.format(fechaSistema));
                        Integer anios = FechasUtil.getDiffDatesDesdeHasta(fecNac, fecSis, 0);
                        Integer meses = FechasUtil.getDiffDatesDesdeHasta(fecNac, fecSis, 1);
                        Integer dias = FechasUtil.getDiffDatesDesdeHasta(fecNac, fecSis, 2);
                        edadAniosMesDias= Integer.parseInt(concatenarEdad(anios,meses,dias));
                        System.out.println("edadAniosMesDias: "+edadAniosMesDias);
                        persona = new Persona();
                        persona.setUuidpersona(UUID.randomUUID().toString());
                        boolean isParroquiaNumericaValida = StringUtils.isNumeric(contenidoFila.get(0)) &&
//                                parroquiaService.findOneByParroquiaId(Integer.parseInt(contenidoFila.get(0))) != null;
                                parroquiaService.findOneByParroquiaByCode(contenidoFila.get(0)) != null;
                        if(isParroquiaNumericaValida){
                            persona.setParroquiaId(parroquiaService.findOneByParroquiaByCode(contenidoFila.get(0)));
                            numColumnas = numColumnas +1;
                        }else{
                            listaErrores.add("Registro: "+registro+" Parroquia Vacía o incorrecta " + contenidoFila.get(0));
                        } 
                        boolean isNumericoPaisValido = !ValidationSupport.isNullOrEmptyString(contenidoFila.get(1)) &&
//                                paisService.findOneByPaisId(Integer.parseInt(contenidoFila.get(1))) != null;
                                paisService.findOneByPaisCodigo(contenidoFila.get(1)) != null;
                        if (isNumericoPaisValido){
                            persona.setPaisId(paisService.findOneByPaisCodigo(contenidoFila.get(1)));
                            numColumnas = numColumnas +1;
                            }else{
                                listaErrores.add("Registro: "+registro+" Pais no existe " + contenidoFila.get(1));
                            }
                        boolean isTipoCedulaNumericaValida = contenidoFila.get(10).equals("6") &&
                                StringUtils.isNumeric(contenidoFila.get(2)) &&
                                !ValidationSupport.isNullOrEmptyString(contenidoFila.get(2))&&
                                contenidoFila.get(2).length() >= 10;
                        boolean isCedulaValida = ValidationSupport.isCedulaValida(contenidoFila.get(2));
                        if (isTipoCedulaNumericaValida){ 
                            numColumnas = numColumnas +1;
                            if (isCedulaValida){
                                persona.setNumeroidentificacion(contenidoFila.get(2));
                                persona.setCttipoidentificacionId(detallecatalogoService.findOneByDetalleId(Integer.parseInt(contenidoFila.get(10))));
                                numColumnas = numColumnas +1;
                            }else {
                                listaErrores.add("Registro: "+registro+" No cumple con criterios de identificación válida " + contenidoFila.get(2));
                            }
                        }else if (!ValidationSupport.isNullOrEmptyString(contenidoFila.get(10)) &&
                                detallecatalogoService.findOneByDetalleId(Integer.parseInt(contenidoFila.get(10))) != null&&
                                detallecatalogoService.isDetalleCatalogoValido(Integer.parseInt(contenidoFila.get(10)),3)){ 
                                persona.setNumeroidentificacion(contenidoFila.get(2));
                                persona.setCttipoidentificacionId(detallecatalogoService.findOneByDetalleId(Integer.parseInt(contenidoFila.get(8))));
                                numColumnas = numColumnas +1;
                        } else {
                                listaErrores.add("Registro: "+registro+" ERROR identificación, los datos son incorrectos (verifique que concuerde la identificación con el tipo de identificación o que el campo no se encuentre vacío)" + contenidoFila.get(2));
                            }
                        boolean isNombreValido = !contenidoFila.get(3).equals("");
                        if (isNombreValido){
                            persona.setPrimernombre(contenidoFila.get(3));                            
                            numColumnas = numColumnas +1;
                            }else{
                                listaErrores.add("Registro: "+registro+" Primer Nombre Vacío "+ contenidoFila.get(3));
                            }
                        boolean isSegundoNombreValido = !contenidoFila.get(4).equals("");
                        if(isSegundoNombreValido){
                            persona.setSegundonombre(contenidoFila.get(4));
                            numColumnas = numColumnas +1;
                        }else {
                            listaErrores.add("Registro: "+registro+" Segundo Nombre Vacío "+ contenidoFila.get(4));
                        }
                        boolean isApellidoValido = !contenidoFila.get(5).equals("");
                        if (isApellidoValido){
                            persona.setPrimerapellido(contenidoFila.get(5));
                            numColumnas = numColumnas +1;
                            }else{
                                listaErrores.add("Registro: "+registro+" Primer Apellido vacío" + contenidoFila.get(5));
                            }
                        boolean isSegudoApellidoValido = !contenidoFila.get(6).equals("");
                        if(isSegudoApellidoValido){
                            persona.setSegundoapellido(contenidoFila.get(6));
                            numColumnas = numColumnas + 1;
                        }else{
                            listaErrores.add("Registro: "+registro+" Segundo Apellido vacío" + contenidoFila.get(6));
                        }
                    try {
                        isFechaValida = FechasUtil.isFechaValida(contenidoFila.get(7));    
                        if (isFechaValida){
                            persona.setFechanacimiento(FechasUtil.formateadorStringAFecha("DD/MM/YYYY",contenidoFila.get(7)));
                            numColumnas = numColumnas +1;
                        }else{
                                listaErrores.add("Registro: "+registro+" Fecha inválida no cumple con el formato DD/MM/YYYY " + contenidoFila.get(7));
                            }
                    } catch (Exception e) {
                        listaErrores.add("Registro: "+registro+" Fecha invalida no cumple con el formato DD/MM/YYYY" + e);
                    }
                        
                        
                        boolean numeroArchivoValido = !ValidationSupport.isNullOrEmptyString(contenidoFila.get(8));
                        if (numeroArchivoValido){
                            persona.setNumeroarchivo(contenidoFila.get(8));
                            numColumnas = numColumnas +1;
                        }else{
                                listaErrores.add("Registro: "+registro+" número Archivo Vacío " + contenidoFila.get(8));
                            }
                        //
                        boolean isSexoNumericoValido = StringUtils.isNumeric(contenidoFila.get(9)) &&
                                detallecatalogoService.findOneByDetalleId(Integer.parseInt(contenidoFila.get(9))) != null;
                        if (isSexoNumericoValido){
                            persona.setCtsexoId(detallecatalogoService.findOneByDetalleId(Integer.parseInt(contenidoFila.get(9))));
                            numColumnas = numColumnas +1;
                        }else{
                                listaErrores.add("Registro: "+registro+" Código catalogo Sexo incorrecto " + contenidoFila.get(9));
                            }
                        boolean isTipoNumericoPersonaValido = StringUtils.isNumeric(contenidoFila.get(11)) &&
                                detallecatalogoService.findOneByDetalleId(Integer.parseInt(contenidoFila.get(11))) != null;
                        if (isTipoNumericoPersonaValido){
                            persona.setCttipopersonaId(detallecatalogoService.findOneByDetalleId(Integer.parseInt(contenidoFila.get(11))));
                            numColumnas = numColumnas +1;
                            }else {
                                listaErrores.add("Registro: "+registro+" Tipo de identificación no existe " + contenidoFila.get(11));
                            }
                        
                        boolean isOrientacionValido =StringUtils.isNumeric(contenidoFila.get(12)) &&
                                detallecatalogoService.findOneByDetalleId(Integer.parseInt(contenidoFila.get(12))) != null;
                        if (isOrientacionValido){
                            persona.setCtorientacionsexualId(detallecatalogoService.findOneByDetalleId(Integer.parseInt(contenidoFila.get(12))));
                            numColumnas = numColumnas +1;
                            }else {
                                listaErrores.add("Registro: "+registro+" Orientación Sexual no existe " + contenidoFila.get(12));
                            }
                        
                        boolean isEntidadGeneroValido =StringUtils.isNumeric(contenidoFila.get(13)) &&
                                detallecatalogoService.findOneByDetalleId(Integer.parseInt(contenidoFila.get(13))) != null;
                        if (isEntidadGeneroValido){
                            persona.setCtidentidadgeneroId(detallecatalogoService.findOneByDetalleId(Integer.parseInt(contenidoFila.get(13))));
                            numColumnas = numColumnas +1;
                            }else {
                                listaErrores.add("Registro: "+registro+" Identidád de genero no existe " + contenidoFila.get(13));
                            }
                        
                        boolean isAutoidentificacionValido =StringUtils.isNumeric(contenidoFila.get(14)) &&
                                detallecatalogoService.findOneByDetalleId(Integer.parseInt(contenidoFila.get(14))) != null;
                        if (isAutoidentificacionValido){
                            persona.setCtnacionalidadetnicaId(detallecatalogoService.findOneByDetalleId(Integer.parseInt(contenidoFila.get(14))));
                            numColumnas = numColumnas +1;
                            }else {
                                listaErrores.add("Registro: "+registro+" Autoidentificación no existe " + contenidoFila.get(14));
                            }
                        
                        boolean isNacionalidadPueblosValido =StringUtils.isNumeric(contenidoFila.get(15)) &&
                                detallecatalogoService.findOneByDetalleId(Integer.parseInt(contenidoFila.get(15))) != null;
                        if (isNacionalidadPueblosValido){
                            persona.setCtnacionalidadetnicaId(detallecatalogoService.findOneByDetalleId(Integer.parseInt(contenidoFila.get(15))));
                            numColumnas = numColumnas +1;
                            }else {
                                listaErrores.add("Registro: "+registro+" Nacionalidad Etnica no existe " + contenidoFila.get(15));
                            }
                        
                        boolean isTipoCedulaRepNumericaValida = contenidoFila.get(16).equals("6") &&
                                    StringUtils.isNumeric(contenidoFila.get(17)) &&
                                    !ValidationSupport.isNullOrEmptyString(contenidoFila.get(17))&&
                                    contenidoFila.get(17).length() >= 10;
                        boolean isCedulaRepValida = ValidationSupport.isCedulaValida(contenidoFila.get(17));
                        
                        if (edadAniosMesDias < 50000){  
                            System.out.println("edadAniosMesDias: "+edadAniosMesDias);
                            if (isTipoCedulaRepNumericaValida){ 
                                numColumnas = numColumnas +1;
                                if (isCedulaRepValida){
                                    persona.setNumeroidentificacion(contenidoFila.get(17));
                                    persona.setCttipoidentificacionId(detallecatalogoService.findOneByDetalleId(Integer.parseInt(contenidoFila.get(16))));
                                    numColumnas = numColumnas +1;
                                }else {
                                    listaErrores.add("Registro: "+registro+" No cumple con criterios de identificación Representante válida " + contenidoFila.get(17));
                                }
                            }else if (!ValidationSupport.isNullOrEmptyString(contenidoFila.get(16)) &&
                                    detallecatalogoService.findOneByDetalleId(Integer.parseInt(contenidoFila.get(16))) != null&&
                                    detallecatalogoService.isDetalleCatalogoValido(Integer.parseInt(contenidoFila.get(16)),3)){ 
                                    persona.setNumeroidentificacion(contenidoFila.get(17));
                                    persona.setCttipoidentificacionId(detallecatalogoService.findOneByDetalleId(Integer.parseInt(contenidoFila.get(16))));
                                    numColumnas = numColumnas +1;
                            } else {
                                    listaErrores.add("Registro: "+registro+" ERROR identificación Representante, los datos son incorrectos (verifique que concuerde la identificación con el tipo de identificación o que el campo no se encuentre vacío)" + contenidoFila.get(17));
                                }
                        
                        }
                        
                        boolean isDireccionValida = !ValidationSupport.isNullOrEmptyString(contenidoFila.get(18));
                        if (isDireccionValida){
                                persona.setDirecciondomicilio(contenidoFila.get(18));
                                numColumnas = numColumnas +1;
                        }else {
                                listaErrores.add("Registro: "+registro+" Dirección vacía o nula " + contenidoFila.get(18));
                            }
                        //VALIDA SI ES UN REGISTRO VALIDO Y COMPLETO
                        if (edadAniosMesDias < 50000){
                            if (isParroquiaNumericaValida &&
                                isDireccionValida &&
                                numeroArchivoValido &&
                                isFechaValida &&
                                isTipoCedulaNumericaValida &&
                                isCedulaValida &&
                                isNombreValido &&
                                isApellidoValido &&
                                isSexoNumericoValido &&
                                isTipoNumericoPersonaValido &&
                                isNumericoPaisValido &&
                                isSegundoNombreValido && 
                                isSegudoApellidoValido &&
                                isOrientacionValido &&
                                isEntidadGeneroValido &&
                                isAutoidentificacionValido &&
                                isTipoCedulaRepNumericaValida &&
                                isCedulaRepValida &&
                                numColumnas == 19){
                                persona = personaService.save(persona);
                                numColumnas = 0;
                                }
                        } else if (isParroquiaNumericaValida &&
                                isDireccionValida &&
                                numeroArchivoValido &&
                                isFechaValida &&
                                isTipoCedulaNumericaValida &&
                                isCedulaValida &&
                                isNombreValido &&
                                isApellidoValido &&
                                isSexoNumericoValido &&
                                isTipoNumericoPersonaValido &&
                                isNumericoPaisValido &&
                                isSegundoNombreValido && 
                                isSegudoApellidoValido &&
                                isOrientacionValido &&
                                isEntidadGeneroValido &&
                                isAutoidentificacionValido &&
                                numColumnas == 17){
                                persona = personaService.save(persona);
                                numColumnas = 0;
                                }
                        }else
                        {
                            listaErrores.add("Registro: "+registro+" Paciente ya se encuentra ingresado " + contenidoFila.get(2));
                        }
                    }else
                    {
                        listaErrores.add("Registro: "+registro+" Registro vacío");
                    }
                }
                filas.add(contenidoFila);
                registro =  registro +1;
                numColumnas = 0;
                cabecera = true;
            }
            if (!listaErrores.isEmpty()){
            isArchivoValido = false;
            exportRegistrosPacientesWithError(pathFile,listaErrores,"ErroresCargaPacientes"+numArchivo++);
            }else 
            input.close();
return true;
    }
        
    private void exportRegistrosPacientesWithError(String pathToFile, List<String> listaErroresPacientes, String tipoLista) throws IOException {
		String fileName = pathToFile + "/" + tipoLista + ".csv";
                
                String[] errores = new String[listaErroresPacientes.size()];
                int i = 0;
                for (String listaErroresPaciente : listaErroresPacientes) {
                    errores[i] = listaErroresPaciente;
                    i++;
                    }
                if (isArchivoValido == false){
                alertaCargaPacientes.setText(createAlertCargaPacientes(pathToFile));
                }else {
                alertaCargaPacientes.setText("Pacientes Cargados correctamente");
                }
                CSVWriter writer = new CSVWriter(new FileWriter(fileName));
                writer.writeNext(errores);
                writer.close(); 

	}
    
    public String createAlertCargaPacientes(String path) {
        return "<html>"
                + "<body>"
                + "&nbsp; <span style=\"color: #FF0000;\">¡Se procesaron solo algunos registros, por favor reviar el archivo con los errores</span> <br/>"
                + "&nbsp; &nbsp; <span style=\"color: #FF0000;\">   Ruta: "+path+"</span>"
                + "<body>"
                + "<html>";
    }
    
    public String createAlertCargaArchivo() {
        return "<html>"
                + "<body>"
                + "&nbsp; <span style=\"color: #428BCA;\">* Los valores deben estar separado por el caracter especial ';'</span> <br/>"
                + "&nbsp; <span style=\"color: #428BCA;\">* Debe ser un archivo con la extensión CSV </span> <br/>"
                + "&nbsp; <span style=\"color: #428BCA;\">* Todas las columnas son obligatorias</span> <br/>"
                + "&nbsp; <span style=\"color: #428BCA;\">* Para los valores de fechas debe tener el formato: DD/MM/YYYY</span> <br/>"
                + "&nbsp; <span style=\"color: #428BCA;\">* El número de identificación debe ser de 10 dígitos</span> <br/>"
                + "<body>"
                + "<html>";
    }
    
    private String concatenarEdad(Integer edadAnios, Integer edadMeses, Integer edadDias) {
		return StringUtils.leftPad(edadAnios.toString(), 3, "0") + StringUtils.leftPad(edadMeses.toString(), 2, "0")
				+ StringUtils.leftPad(edadDias.toString(), 2, "0");
	}
}
