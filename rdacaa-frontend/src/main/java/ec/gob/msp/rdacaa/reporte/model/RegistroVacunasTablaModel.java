package ec.gob.msp.rdacaa.reporte.model;

import ec.gob.msp.rdacaa.utilitario.forma.ModeloTablaGenerico;

public class RegistroVacunasTablaModel extends ModeloTablaGenerico<VacunasReporte> {

	public RegistroVacunasTablaModel(String[] columnas) {
		super(columnas);
	}

	private static final long serialVersionUID = 3711367031462145149L;

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		VacunasReporte obj = getListaDatos().get(rowIndex);
		switch (columnIndex) {
		case 0:
			return "";
		case 1:
			return obj.getDescripcion();
		case 2:
			return obj.getNum028();
		case 3:
			return obj.getNum111();
		case 4:
			return obj.getNum14();
		case 5:
			return obj.getNum59();	
		case 6:
			return obj.getNum1014();
		case 7:
			return obj.getNum1519();
		case 8:
			return obj.getNum2039();
		case 9:
			return obj.getNum4064();
		case 10:
			return obj.getNum65();
		default:
			return "";
		}
	}

}
