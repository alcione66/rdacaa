package ec.gob.msp.rdacaa.principal.controller;

import java.awt.Desktop;
import java.awt.EventQueue;
import java.awt.event.ActionEvent;
import java.awt.event.FocusEvent;
import java.awt.event.FocusListener;
import java.awt.event.KeyAdapter;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.swing.Box;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.xml.bind.JAXBException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.DataSourceBuilder;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Controller;

import ec.gob.msp.rdacaa.business.entity.Entidad;
import ec.gob.msp.rdacaa.business.entity.Entidadpersona;
import ec.gob.msp.rdacaa.business.entity.Parroquia;
import ec.gob.msp.rdacaa.business.entity.Persona;
import ec.gob.msp.rdacaa.business.entity.Usuario;
import ec.gob.msp.rdacaa.business.service.DetallecatalogoService;
import ec.gob.msp.rdacaa.business.service.EntidadService;
import ec.gob.msp.rdacaa.business.service.EntidadpersonaService;
import ec.gob.msp.rdacaa.business.service.PaisService;
import ec.gob.msp.rdacaa.business.service.ParroquiaService;
import ec.gob.msp.rdacaa.business.service.PersonaService;
import ec.gob.msp.rdacaa.business.service.TipoestablecimientoService;
import ec.gob.msp.rdacaa.business.service.UsuarioService;
import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.principal.forma.Acceso;
import ec.gob.msp.rdacaa.principal.forma.AcercaDeRdacaa;
import ec.gob.msp.rdacaa.principal.forma.CambioContrasenia;
import ec.gob.msp.rdacaa.principal.forma.EscritorioPrincipal;
import ec.gob.msp.rdacaa.principal.forma.RecuperarContrasenia;
import ec.gob.msp.rdacaa.principal.forma.TerminosDeUso;
import ec.gob.msp.rdacaa.seguridades.AccesoXmlService;
import ec.gob.msp.rdacaa.seguridades.AutenticacionService;
import ec.gob.msp.rdacaa.seguridades.util.SeguridadUtil;
import ec.gob.msp.rdacaa.seguridades.xml.ProfileUserPras;
import ec.gob.msp.rdacaa.system.database.OS;
import ec.gob.msp.rdacaa.system.database.OSInfo;
import ec.gob.msp.rdacaa.utilitario.enumeracion.ComunEnum;
import ec.gob.msp.rdacaa.utilitario.fecha.FechasUtil;
import ec.gob.msp.rdacaa.utilitario.file.Log;
import ec.gob.msp.rdacaa.utilitario.file.UtilitarioFile;
import ec.gob.msp.rdacaa.utilitario.forma.UtilitarioForma;

@Controller
@PropertySource(value = { "classpath:database-config-${rdacaa.profiles.active:default}.properties" })
public class AccesoPrincipalController {
    
	private static final String MENSAJE_ERROR = "ERROR";
    @Autowired
	private Environment env;
	@Autowired
    private Acceso loginFrame;
	@Autowired
    private CambioContrasenia cambioContraseniaFrame;
	@Autowired
    private CambioContraseniaController cambioContraseniaController;
	@Autowired
    private EscritorioPrincipalController escritorioPrincipalController;
	@Autowired
    private EscritorioPrincipal escritorioPrincipalFrame;
	@Autowired
	private TipoestablecimientoService tipoestablecimientoService;
        
    private RecuperarContrasenia recuperarContraseniaFrame;
    
    private JButton btnCargar;
    private JButton botonLogin;
    private JFileChooser abrirArchivo;
    private JMenuBar MenuBarRDACAA;
    private JButton btnTerminosDeUso;
    private JButton btnDownloadManual;
    private JButton btnImportPacientes;
    
    private JTextField txt_usuario;
    private JTextField txt_contrasena;
    private JLabel txt_establecimiento;
    private String establecimiento = "";
    
    private JCheckBox checbx_ingreso_prestador_externo;
    private JCheckBox checbx_acepta_terminos_de_uso;
    private JButton btnRecuperarContraseña;
    
    @Autowired
    private EntidadService entidadService;
    @Autowired
    private PersonaService personaService;
    @Autowired
    private ParroquiaService parroquiaService;
    @Autowired
    private DetallecatalogoService detallecatalogoService;
    @Autowired
    private PaisService paisService;
    @Autowired
    private UsuarioService usuarioService;
    @Autowired
    private EntidadpersonaService entidadpersonaService;
    @Autowired
    private AutenticacionService autenticacionService;
    @Autowired
    private TerminosDeUso terminosDeUso;
    @Autowired
    private TerminosDeUsoController terminosDeUsoController;
    @Autowired
    private RecuperarContraseniaController recuperarContraseniaController;
    
    private static final Integer PROFESIONAL_SALUD = 2480;
    
    private AcercaDeRdacaa acercaDeRdacaa;

    /**
     * Create the application.
     *
     */
    @Autowired
    public AccesoPrincipalController(RecuperarContrasenia recuperarContraseniaFrame,
            RecuperarContraseniaController recuperarContraseniaController) {
       //Los atributos de la clase son inyectados en tiempo de ejecucion
       this.recuperarContraseniaFrame = recuperarContraseniaFrame;
       this.cambioContraseniaController = cambioContraseniaController;
    }
    
    
    
    @PostConstruct
    private void init() throws GeneralSecurityException {
    	SeguridadUtil.init();
        this.botonLogin = loginFrame.getBtnAcceso();
        this.txt_usuario = loginFrame.getTxt_usuario();
        this.txt_contrasena = loginFrame.getTxt_contrasena();
        this.txt_establecimiento = loginFrame.getTxt_establecimiento();
        this.checbx_ingreso_prestador_externo = loginFrame.getChecbx_ingreso_prestador_externo();
        this.checbx_acepta_terminos_de_uso = loginFrame.getChecbx_acepta_terminos_de_uso();
        this.btnImportPacientes = loginFrame.getBtnImportPacientes();
        this.btnImportPacientes.addActionListener(e -> {
                    importPacientes();
        });
        this.botonLogin.addActionListener(e -> {
        	validarTerminosUsoSelected(); 
		});
        
        botonLogin.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                 if(e.getKeyCode()==KeyEvent.VK_ENTER) {
                      validarTerminosUsoSelected(); 
                 }
            } 
        });
        txt_contrasena.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                 if(e.getKeyCode()==KeyEvent.VK_ENTER) {
                     validarTerminosUsoSelected(); 
                 }
            } 
        });
        this.btnCargar = loginFrame.getBtnCargar();
        this.MenuBarRDACAA = EscritorioPrincipal.MenuBarRDACAA;
        EventQueue.invokeLater(() -> {
        	this.acercaDeRdacaa = new AcercaDeRdacaa(escritorioPrincipalFrame, true);
        });
        
        checbx_ingreso_prestador_externo.addActionListener((ActionEvent e) -> {
        	if(this.checbx_ingreso_prestador_externo.isSelected()) {
        		this.txt_usuario.setEnabled(false);
        		this.txt_usuario.setText("");
        		this.txt_contrasena.setEnabled(false);
        		this.txt_contrasena.setText("");
        	} else {
        		this.txt_usuario.setEnabled(true);
        		this.txt_contrasena.setEnabled(true);
        	}
        });

        
        btnCargar.addActionListener((ActionEvent e) -> {
            cargarCredenciales();
        });
        txt_usuario.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                UtilitarioForma.validarLongitudCampo(evt, 10);
            }
        });
        
        txt_usuario.addFocusListener(new FocusListener() {
            @Override
            public void focusGained(FocusEvent e) {
            	//No es usado
            }

            @Override
            public void focusLost(FocusEvent e) {
                
                EventQueue.invokeLater(new Runnable() {
                @Override
                public void run() {
                    if (!txt_usuario.getText().equals("")){
                        //cargaEntidadPersona(txt_usuario.getText()); 
                        try {
                        String establecimientoFirst = usuarioService.findFirstEntidadByUsuario(txt_usuario.getText()).getNombreoficial();
                          if (establecimientoFirst != null){
                              if (establecimientoFirst.length() > 25){
                                    txt_establecimiento.setText("Establecimiento :" + establecimientoFirst.substring(0, 24));
                                    System.out.println("Establecimeinto "+ establecimientoFirst.substring(0, 24));
                              }else{
                                  txt_establecimiento.setText("Establecimiento :" + establecimientoFirst);
                              }
                          }else {
                             System.out.println("No se encuentra entidad para el usuario "+ txt_usuario.getText());
                             txt_establecimiento.setText("Establecimiento : NINGUNO" );
                          }    
                        } catch (Exception e) {
                            System.out.println("No se encuentra entidad para el usuario "+ txt_usuario.getText());
                            txt_establecimiento.setText("");
                        }
                        
                        
                    } 
                }});
            }
        });
        
        this.btnTerminosDeUso = loginFrame.getBtnTerminos();
        this.btnTerminosDeUso.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                terminosDeUso.setVisible(true);
                terminosDeUsoController.cargaTexto();
            }

            @Override
            public void mousePressed(MouseEvent e) {
            	//no se usa
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            	//no se usa
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            	//no se usa
            }

            @Override
            public void mouseExited(MouseEvent e) {
            	//no se usa
            }
        });
        this.btnDownloadManual = loginFrame.getBtnDownloadManual();       
        this.btnDownloadManual.addMouseListener(new MouseListener() {
            
            @Override
            public void mouseClicked(MouseEvent e) {
                DataSourceBuilder<?> pathDocumentosBuilder = DataSourceBuilder.create();
		pathDocumentosBuilder.driverClassName(env.getProperty("rdacaa.datasource.driver-class-name"));
                Desktop desktop = Desktop.getDesktop();
                //File file1 = new File("/home/eduardo/Descargas/zt_git_cheat_sheet.pdf");
                File file1;
                File file2;
                try {
                if (OS.UNIX.equals(OSInfo.getOs())) {
                    //System.out.print("linux "+env.getProperty("rdacaa.manuales.url.unix"));
                    file1 = new File(env.getProperty("rdacaa.manuales.url.unix"));
                    desktop.open(file1);
                    file2 = new File(env.getProperty("rdacaa.instructivo.url.unix"));
                    desktop.open(file2);
                } else if (OS.WINDOWS.equals(OSInfo.getOs())) {
                    //System.out.print("windows"+env.getProperty("rdacaa.manuales.url.windows"));
                    file1 = new File(env.getProperty("rdacaa.manuales.url.windows"));
                    desktop.open(file1);
                    file2 = new File(env.getProperty("rdacaa.instructivo.url.windows"));
                    desktop.open(file2);
                }
                
                    
                } catch (IOException ex) {
                    Logger.getLogger(AccesoPrincipalController.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            @Override
            public void mousePressed(MouseEvent e) {
            	//no se usa
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            	//no se usa
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            	//no se usa
            }

            @Override
            public void mouseExited(MouseEvent e) {
            	//no se usa
            }
        });
        this.btnRecuperarContraseña = loginFrame.getBtnRecuperarContrasenia();
        btnRecuperarContraseña.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                recuperarContrasenia();
            }

            @Override
            public void mousePressed(MouseEvent e) {
            	//no se usa
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            	//no se usa
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            	//no se usa
            }

            @Override
            public void mouseExited(MouseEvent e) {
            	//no se usa
            }
        });
    }
    
//    public void cargaEntidadPersona(String usuario){
//      String establecimientoFirst = usuarioService.findFirstEntidadByUsuario(usuario).getNombreoficial();
//      System.out.println("1");
//      if (establecimientoFirst == null){
//         System.out.println("Usuario no se encuentra registrado");
//      }else {
//         txt_establecimiento.setText("Establecimiento :" + establecimientoFirst);
//      }
//    }
    public void validarTerminosUsoSelected(){
        try {
                        if (checbx_acepta_terminos_de_uso.isSelected()){
				validarCredenciales();
                            }else {
                            UtilitarioForma.muestraMensaje("Advertencia", loginFrame, "ADVERTENCIA", "Debe aceptar los Términos de Uso");
                           }
			} catch (GeneralSecurityException | IOException e1) {
				UtilitarioForma.muestraMensaje("Error", loginFrame, "ERROR", "Existe un problema "+e1.getMessage());
			}
    }
    public void validarCredenciales() throws GeneralSecurityException, IOException {
        VariableSesion variableSession = VariableSesion.getInstanciaVariablesSession();
        Usuario usuario;
        if(checbx_ingreso_prestador_externo.isSelected()) {
        	// Pintar cabecera
        	addCabeceraInfoPrestadorExterno();
        	// Llamada del container para actualizar los botones de accceso en base a una especialidad                                 
            escritorioPrincipalFrame.loadingButtonsInvited();
        } else {
            if (!txt_usuario.getText().equals("")) {
                usuario = autenticacionService.findUsuarioCambiaClave(txt_usuario.getText());
                if (usuario != null) {
                    variableSession.setUsuario(usuario);
                    VariableSesion.setInstanciaParametros(variableSession);
                    cambioContraseniaFrame.setVisible(true);
                    cambioContraseniaController.limpiarFormulario();
                    loginFrame.dispose();
                } else {
                    if (txt_usuario.getText().equals("") || txt_contrasena.getText().equals("")) {
                        UtilitarioForma.muestraMensaje("", loginFrame, MENSAJE_ERROR, "Estimado usuario los campos de inicio de sesión son obligatorios.");
                    } else {
                        List<Object> attributesUser = autenticacionService.getCredenciales(txt_usuario.getText().trim(), txt_contrasena.getText().trim());
                        if (!attributesUser.isEmpty()) {
                            variableSession.setUsuario(((Usuario) attributesUser.get(0)));
                            variableSession.setUtilitario(
                    		SeguridadUtil.descifrarConPasswordYSalt(
                            				txt_contrasena.getText(), 
                            				variableSession.getUsuario().getSalt(), 
                            				variableSession.getUsuario().getKey(), 
                            				variableSession.getUsuario().getLogin()));
                            variableSession.setProfesional(((Usuario) attributesUser.get(0)).getPersonaId());
                            variableSession.setEstablecimientoSalud(((Usuario) attributesUser.get(0)).getEntidadId()); 
                            
                            variableSession.setKeysetPublico(SeguridadUtil.leerLlavePublica(env.getProperty("publickey")));
                            if (!attributesUser.get(1).toString().equals("[]")) {
                                List<String> optionList = new ArrayList<>();                            
                                ArrayList<Entidadpersona> entpers = (ArrayList<Entidadpersona>) attributesUser.get(1);
                                entpers.forEach(entidadpersona -> {
                                	optionList.add(entidadpersona.getCtespecialidadId().getDescripcion());
                                });
                                if (!optionList.isEmpty()) {
                                    if(optionList.size() == 1) {
                                        entpers.forEach(entidadpersona -> {
                                            if (entidadpersona.getCtespecialidadId().getDescripcion().equals(optionList.get(0))) {
                                                variableSession.setCtEspecialidad(entidadpersona.getCtespecialidadId());
                                            }
                                        });
                                        addCabeceraInfoUsuario(variableSession);
                                    } else {
                                        Object[] optionsInputDialog = optionList.toArray();
                                        String code = (String) JOptionPane.showInputDialog(
                                            null,
                                            "Por favor seleccione una Especialidad Médica",
                                            "Especialidades Médicas",
                                            JOptionPane.QUESTION_MESSAGE,
                                            null,
                                            optionsInputDialog,
                                            null
                                        );
                                        if ((code != null) && (code.length() > 0)) {
                                            entpers.forEach(entidadpersona -> {
                                                if (entidadpersona.getCtespecialidadId().getDescripcion().equals(code)) {
                                                    variableSession.setCtEspecialidad(entidadpersona.getCtespecialidadId());
                                                }
                                            });
                                            addCabeceraInfoUsuario(variableSession);
                                        }       
                                    }
                                    //Llamada del container para actualizar los botones de accceso en base a una especialidad                                 
                                    escritorioPrincipalFrame.loadingButtonsContainer();
                                    // Clean Inputs
                                    txt_usuario.setText("");
                                    txt_contrasena.setText("");
                                }
                            } else {
                                UtilitarioForma.muestraMensaje("", loginFrame, MENSAJE_ERROR, "Estimado usuario su perfil profesional no posee ninguna especialidad, por favor verificar.");
                            }
                        } else {
                            UtilitarioForma.muestraMensaje("", loginFrame, MENSAJE_ERROR, "No se ha encontrado resultados, por favor verifique sus crendenciales. Puede ingresar como usuario invitado");
                        }                    
                    }
                }
            }
        }
    }
    private void importPacientes() {
    	escritorioPrincipalFrame.loadingButtonsImportPacientes();
    	escritorioPrincipalController.prepareAndOpenFrame();
    	loginFrame.dispose();
    }
    
    public void addCabeceraInfoUsuario(VariableSesion variableSession) {
        escritorioPrincipalController.setVs(variableSession);
        escritorioPrincipalController.prepareAndOpenFrame();
//        escritorioPrincipalController.validateCierre();
        String nombrecompleto = variableSession.getProfesional().getPrimernombre() + " "
                + variableSession.getProfesional().getSegundonombre() + " "
                + variableSession.getProfesional().getPrimerapellido() + " "
                + variableSession.getProfesional().getSegundoapellido();
        JMenu infoUsuario = new JMenu("Usuario: " + nombrecompleto + " ");
        JMenu infoEspecialidad = new JMenu("Especialidad: " + variableSession.getCtEspecialidad().getDescripcion() + " ");
        JMenu infoEstablecimiento = new JMenu("Establecimiento: " + variableSession.getEstablecimientoSalud().getNombreoficial() + " ");
        StringBuilder dpa = new StringBuilder();
        if(variableSession.getEstablecimientoSalud().getUbicacionParroquiaId() != null){
        	dpa.append(variableSession.getEstablecimientoSalud().getUbicacionParroquiaId().getCantonId().getProvinciaId().getDescripcion());
        	dpa.append(" - ");
        	dpa.append(variableSession.getEstablecimientoSalud().getUbicacionParroquiaId().getCantonId().getDescripcion());
        	dpa.append(" - ");
        	dpa.append(variableSession.getEstablecimientoSalud().getUbicacionParroquiaId().getDescripcion());
        }else{
        	Parroquia parroquia = parroquiaService.findOneByParroquiaId(variableSession.getEstablecimientoSalud().getParroquiaId());
        	dpa.append(parroquia.getCantonId().getProvinciaId().getDescripcion());
        	dpa.append(" - ");
        	dpa.append(parroquia.getCantonId().getDescripcion());
        	dpa.append(" - ");
        	dpa.append(parroquia.getDescripcion());
        }
        
        MenuBarRDACAA.removeAll();
        MenuBarRDACAA.add(EscritorioPrincipal.menu_inicio);
        
        JMenu infoDPA = new JMenu("DPA: " + dpa + " ");
        JMenu infoAyuda = new JMenu("");
        addCabeceraInfoListeners(infoAyuda);
        MenuBarRDACAA.add(Box.createHorizontalGlue());
        MenuBarRDACAA.add(infoUsuario);
        MenuBarRDACAA.add(infoEspecialidad);
        MenuBarRDACAA.add(infoEstablecimiento);
        MenuBarRDACAA.add(infoDPA);
        MenuBarRDACAA.add(infoAyuda).setIcon(new javax.swing.ImageIcon(getClass().getResource("/principal/acerca.jpg")));
        escritorioPrincipalFrame.pack();
        VariableSesion.setInstanciaParametros(variableSession);
        loginFrame.dispose();
    }
    
    private void addCabeceraInfoPrestadorExterno() {
        escritorioPrincipalController.prepareAndOpenFrame();
        MenuBarRDACAA.removeAll();
        MenuBarRDACAA.add(EscritorioPrincipal.menu_inicio);
//        escritorioPrincipalController.validateCierre();
        String nombrecompleto = "Invitado - Prestador Externo";
        JMenu infoUsuario = new JMenu("Usuario: " + nombrecompleto + " ");
        JMenu infoAyuda = new JMenu("");
        addCabeceraInfoListeners(infoAyuda);
        MenuBarRDACAA.add(Box.createHorizontalGlue());
        MenuBarRDACAA.add(infoUsuario);
        MenuBarRDACAA.add(infoAyuda).setIcon(new javax.swing.ImageIcon(getClass().getResource("/principal/acerca.jpg")));
        escritorioPrincipalFrame.pack();
        loginFrame.dispose();
    }
    
    private void addCabeceraInfoListeners(JMenu infoAyuda) {
    	infoAyuda.addMouseListener(new MouseAdapter(){
            @Override
            public void mouseClicked(MouseEvent e) {
                acercaDeRdacaa.setLocationRelativeTo(escritorioPrincipalFrame);
                acercaDeRdacaa.setVisible(true);
            }
    	});
    }

    /**
     * Launch the application.
     */
    public void prepareAndOpenFrame() {
        loginFrame.setVisible(true);
        txt_establecimiento.setText(establecimiento);
        
    }
    
    private void cargarCredenciales() {
        if (abrirArchivo == null) {
            abrirArchivo = new JFileChooser();
            abrirArchivo.setFileSelectionMode(JFileChooser.FILES_ONLY);
            int seleccion = abrirArchivo.showOpenDialog(loginFrame);
            if (seleccion == JFileChooser.APPROVE_OPTION) {
                File f = abrirArchivo.getSelectedFile();
                try {
                    if (processFile(f)) {
                        abrirArchivo.cancelSelection();
                        abrirArchivo = null;
                        UtilitarioForma.muestraMensaje("", loginFrame, "INFO", "Credenciales cargadas correctamente");
                        if (establecimiento != null && establecimiento.length() > 25 ){
                        txt_establecimiento.setText("Establecimiento :"+ establecimiento.substring(0, 24));
                        }else {
                            txt_establecimiento.setText("Establecimiento :"+ establecimiento);
                                    }
                    }else{
                        abrirArchivo = null;
                    }
                } catch (JAXBException ex) {
                    abrirArchivo = null;
                    UtilitarioForma.muestraMensaje("", loginFrame, MENSAJE_ERROR, "Existe un problema con la carga de credenciales " + ex.getMessage());
                }
            }else{
                abrirArchivo = null;
            }
        }
    }
    
    private boolean processFile(File archivo) throws JAXBException {
        boolean validar = false;
        if (UtilitarioFile.isXML(archivo)) {
            ProfileUserPras profileUserPras = AccesoXmlService.processAcceso(archivo);
            if (profileUserPras != null) {
                Entidad entidad = entidadService.findOneByEntidadId(profileUserPras.getInfoEntidad().getId());
                /**
                 * se carga la clase entidad
                 */
                if (entidad == null) {
                    entidad = new Entidad();
                    entidad.setId(profileUserPras.getInfoEntidad().getId());
                    entidad.setCodigo(profileUserPras.getInfoEntidad().getCodigo());
                    entidad.setRuc(profileUserPras.getInfoEntidad().getRuc());
                    entidad.setEntidadId(entidadService.findOneByEntidadId(profileUserPras.getInfoEntidad().getEntidadId()));
                    entidad.setNombreoficial(profileUserPras.getInfoEntidad().getNombreOficial());
                    entidad.setNombrecomercial(profileUserPras.getInfoEntidad().getNombreComercial());
                    entidad.setDireccion(profileUserPras.getInfoEntidad().getDireccion());
                    entidad.setDireccionreferencia(profileUserPras.getInfoEntidad().getDireccionreferencia());
                    entidad.setNumestablecimiento(profileUserPras.getInfoEntidad().getNumestablecimiento());
                    entidad.setTelefono(profileUserPras.getInfoEntidad().getTelefono());
                    entidad.setEmail(profileUserPras.getInfoEntidad().getEmail());
                    entidad.setParroquiaId(profileUserPras.getInfoEntidad().getParroquiaId());
                    entidad.setZ17s(profileUserPras.getInfoEntidad().getZ17S());
                    
                    if(profileUserPras.getInfoEntidad().getTipoestablecimientoId() > 0){
                    	entidad.setTipoestablecimientoId(tipoestablecimientoService.findOneByTipoEstablecimientoId(profileUserPras.getInfoEntidad().getTipoestablecimientoId()));
                    }
                    entidad = entidadService.save(entidad);
                }
                establecimiento = profileUserPras.getInfoEntidad().getNombreOficial();
                /**
                 * se carga la clase persona
                 */
                if (!profileUserPras.getPerfilPersona().getPersona().isEmpty()) {
                    try {
                	for (ProfileUserPras.PerfilPersona.Persona personaItem : profileUserPras.getPerfilPersona().getPersona()) {
                            Persona persona = personaService.findOneByProfesionalNumIdentificacion(personaItem.getNumeroIdentificacion());
                            if (persona == null) {
                                persona = new Persona();
                                if (personaItem.getId() != 0) {
                                    persona.setIdpras((int) personaItem.getId());
                                }
                                if (!personaItem.getParroquia().equals("") && personaItem.getParroquia() != null) {
                                    persona.setParroquiaId(parroquiaService.findOneByParroquiaId(Integer.parseInt(personaItem.getParroquia())));
                                }
                                if (!personaItem.getNumeroIdentificacion().equals("") && personaItem.getNumeroIdentificacion() != null) {
                                    persona.setNumerohistoriaclinica(personaItem.getNumeroIdentificacion());
                                    persona.setNumeroidentificacion(personaItem.getNumeroIdentificacion());
                                }
                                if (!personaItem.getPrimerNombre().equals("") && personaItem.getPrimerNombre() != null) {
                                	persona.setPrimernombre(personaItem.getPrimerNombre());
                                }
                                if (!personaItem.getSegundoNombre().equals("") && personaItem.getSegundoNombre() != null) {
                                	persona.setSegundonombre(personaItem.getSegundoNombre());
                                }
                                if (!personaItem.getApellidoPaterno().equals("") && personaItem.getApellidoPaterno() != null) {
                                	persona.setPrimerapellido(personaItem.getApellidoPaterno());
                                }
                                if (!personaItem.getApellidoMaterno().equals("") && personaItem.getApellidoMaterno() != null) {
                                	persona.setSegundoapellido(personaItem.getApellidoMaterno());
                                }
                                if (!personaItem.getFechaNacimiento().equals("") && personaItem.getFechaNacimiento() != null) {
                                	persona.setFechanacimiento(FechasUtil.formateadorStringAFecha(ComunEnum.PATRON_FECHA6.getDescripcion(), personaItem.getFechaNacimiento()));
                                }
                                persona.setNumeroarchivo("0");
                                persona.setEstado(0);
                                if(personaItem.getCatalogoSexo() != 0){
                                	persona.setCtsexoId(detallecatalogoService.findOneByDetalleId(personaItem.getCatalogoSexo()));
                                }
                                if(personaItem.getCatalogoTipoIdentificacion() != 0){
                                	persona.setCttipoidentificacionId(detallecatalogoService.findOneByDetalleId(personaItem.getCatalogoTipoIdentificacion()));
                                }
                                persona.setCttipopersonaId(detallecatalogoService.findOneByDetalleId(PROFESIONAL_SALUD));
                                if(personaItem.getPais() != 0 && personaItem.getPais() > 0){
                                	persona.setPaisId(paisService.findOneByPaisId(personaItem.getPais()));
                                }
                                persona = personaService.save(persona);
                                /**
                                 * se carga la clase usuario
                                 */
                                if (usuarioService.findOneByLogin(personaItem.getUsuario()) == null ) {
                                    Usuario usuario = new Usuario();
                                    usuario.setIdpras(personaItem.getUsuarioId());
                                    usuario.setPersonaId(persona);
                                    usuario.setEntidadId(entidad);
                                    usuario.setCtestadoId(detallecatalogoService.findOneByDetalleId(personaItem.getEstadoUsuario()));
                                    usuario.setLogin(personaItem.getUsuario());
                                    usuario.setCambiarclave(1);
                                    usuario.setFechacreacion(FechasUtil.formateadorStringAFecha(ComunEnum.PATRON_FECHA6.getDescripcion(), personaItem.getFechaCreacionUsuario()));
                                    usuarioService.save(usuario);  
                                }                               
                            } 
                                 /**
                                     * se carga la clase especialidad
                                     */                            
                            if (usuarioService.findOneByLogin(personaItem.getUsuario()) != null  && !personaItem.getEspecialidad().isEmpty()){
                                    saveEspecialidadUsuario(persona, entidad, personaItem);
                            }
                            
                            
                        }
                        } catch (Exception e) {
                                Log.error("AccesoPrincipalController", "Error ", e);
                        }
                }
            }
            validar = true;
        } else {
            UtilitarioForma.muestraMensaje("", loginFrame, MENSAJE_ERROR, "El archivo seleccionado no es xml por favor verifique ");
        }
        return validar;
    }
    
    private void saveEspecialidadUsuario(Persona persona, Entidad entidad, ProfileUserPras.PerfilPersona.Persona personaItem) {

        List<Entidadpersona> listaEspecialidadesEntidad = entidadpersonaService.findAllByPersonaId(persona.getId(),entidad.getId());
        
        if (!personaItem.getEspecialidad().isEmpty()) {
            for (ProfileUserPras.PerfilPersona.Persona.Especialidad especialidadItem : personaItem.getEspecialidad()) {
                Entidadpersona entidadpersonaBuscada = entidadpersonaService.findOneByEspecialidadId(persona.getId(),
                        entidad.getId(),
                        especialidadItem.getEspecialidad());
                if ( entidadpersonaBuscada == null) {
                    Entidadpersona entidadpersona = new Entidadpersona();
                    entidadpersona.setEntidadId(entidad);
                    entidadpersona.setPersonaId(persona);
                    entidadpersona.setCtespecialidadId(detallecatalogoService.findOneByDetalleId(especialidadItem.getEspecialidad()));
                    entidadpersona.setEstado(1);
                    entidadpersonaService.save(entidadpersona);
                }else{
                    listaEspecialidadesEntidad.remove(entidadpersonaBuscada);
                }
            }
            
            listaEspecialidadesEntidad.forEach(ep -> {
                ep.setEstado(0);
                entidadpersonaService.save(ep);
            });
        }

    }
    public void recuperarContrasenia(){
    
        JDialog dialog = new JDialog();
    	dialog.setSize(580, 250);
    	dialog.setLocationRelativeTo(null);
    	dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
    	dialog.setVisible(true);
    	dialog.add(recuperarContraseniaFrame);

    }
}
