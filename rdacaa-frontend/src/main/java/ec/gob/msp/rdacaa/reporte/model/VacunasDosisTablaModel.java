package ec.gob.msp.rdacaa.reporte.model;

import ec.gob.msp.rdacaa.utilitario.forma.ModeloTablaGenerico;

public class VacunasDosisTablaModel extends ModeloTablaGenerico<VacunasDosisReporte> {

	public VacunasDosisTablaModel(String[] columnas) {
		super(columnas);
	}

	private static final long serialVersionUID = 3711367031462145149L;

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		VacunasDosisReporte obj = getListaDatos().get(rowIndex);
		switch (columnIndex) {
		case 0:
			return "";
		case 1:
			return obj.getDescripcion();
		case 2:
			return obj.getDosis1();
		case 3:
			return obj.getDosis2();
		case 4:
			return obj.getDosis3();
		case 5:
			return obj.getDosis4();	
		case 6:
			return obj.getDosis5();
		case 7:
			return obj.getDosis6();
		default:
			return "";
		}
	}

}
