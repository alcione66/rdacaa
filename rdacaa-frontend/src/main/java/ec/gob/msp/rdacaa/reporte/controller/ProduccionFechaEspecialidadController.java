package ec.gob.msp.rdacaa.reporte.controller;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;

import com.github.lgooddatepicker.components.DatePicker;

import ec.gob.msp.rdacaa.business.entity.ReporteProduccion;
import ec.gob.msp.rdacaa.business.service.ReporteProduccionService;
import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.reporte.forma.ProduccionFechaEspecialidad;
import ec.gob.msp.rdacaa.reporte.model.ProduccionFechaEspecialidadTablaModel;
import ec.gob.msp.rdacaa.reporte.utilitario.ReportesUtil;
import ec.gob.msp.rdacaa.utilitario.forma.FormatoTabla;
import ec.gob.msp.rdacaa.utilitario.forma.UtilitarioForma;

@Controller
@Lazy(value = false)
public class ProduccionFechaEspecialidadController implements Serializable {

	private static final long serialVersionUID = -2134844824421741615L;
	private ProduccionFechaEspecialidad produccionFechaEspecialidadFrame;
	private JButton btnBuscar;
	private JButton btnDescargar;
	private JTable tblReporte;
	private DatePicker dttDesde;
	private DatePicker dttHasta;
	private List<ReporteProduccion> controles;
	private ReporteProduccionService reporteProduccionService;
	private ProduccionFechaEspecialidadTablaModel modeloTabla;

	@Autowired
	public ProduccionFechaEspecialidadController(ProduccionFechaEspecialidad produccionFechaEspecialidadFrame,
			ReporteProduccionService reporteProduccionService) {
		this.produccionFechaEspecialidadFrame = produccionFechaEspecialidadFrame;
		this.reporteProduccionService = reporteProduccionService;
	}

	@PostConstruct
	private void init() {
		this.dttDesde = produccionFechaEspecialidadFrame.getDttDesde();
		this.dttHasta = produccionFechaEspecialidadFrame.getDttHasta();
		this.btnBuscar = produccionFechaEspecialidadFrame.getBtnBuscar();
		this.btnDescargar = produccionFechaEspecialidadFrame.getBtnDescargar();
		this.tblReporte = produccionFechaEspecialidadFrame.getTblReporte();

		btnBuscar.addActionListener(e -> consultar());
		btnDescargar.addActionListener(e -> exportar());

		cargarTabla();
		produccionFechaEspecialidadFrame.addInternalFrameListener( new  InternalFrameAdapter() {
			@Override
			public void internalFrameClosing(InternalFrameEvent e) {
				super.internalFrameClosing(e);
				cerrar();
			}
		});
	}

	private void consultar() {
		if (dttDesde.getDate() != null && dttHasta.getDate() != null) {
			Object[] datos = reporteProduccionService.findOneReporteProduccionByDates(
					dttDesde.getDateStringOrEmptyString(), dttHasta.getDateStringOrEmptyString(), VariableSesion.getInstanciaVariablesSession().getCtEspecialidad().getId());
			controles = new ArrayList<>();
			if (datos != null && datos.length > 0 ) {
				for (int i = 0; i < datos.length; i++) {
					Object[] fila = (Object[]) datos[i];
					ReporteProduccion rp = new ReporteProduccion();
					if (fila[0] != null) {
						rp.setCodigoestablecimiento(fila[0].toString());
					} else {
						rp.setCodigoestablecimiento("N/A");
					}
					if (fila[1] != null) {
						rp.setNombreestablecimiento(fila[1].toString());
					}
					if (fila[2] != null) {
						rp.setNombreprofesional(fila[2].toString());
					}
					if (fila[3] != null) {
						rp.setEspecialidaprofesional(fila[3].toString());
					}
					if (fila[4] != null) {
						rp.setPrevencionprimera(fila[4].toString());
					} else {
						rp.setPrevencionprimera("0");
					}
					if (fila[5] != null) {
						rp.setPrevencionsubsecuente(fila[5].toString());
					} else {
						rp.setPrevencionsubsecuente("0");
					}
					if (fila[6] != null) {
						rp.setMorbilidadprimera((String) fila[6]);
					} else {
						rp.setMorbilidadprimera("0");
					}
					if (fila[7] != null) {
						rp.setMorbilidadsubsecuente((String) fila[7]);
					} else {
						rp.setMorbilidadsubsecuente("0");
					}
					if (fila[8] != null) {
						rp.setVacunas(fila[8].toString());
					} else {
						rp.setVacunas("0");
					}
					if (fila[9] != null) {
						rp.setId(Integer.valueOf(fila[9].toString()));
					} else {
						rp.setId(0);
					}
					controles.add(rp);
				}
				
			}else{
				UtilitarioForma.muestraMensaje("Consulta", produccionFechaEspecialidadFrame, "INFO", "No existen resultados para los parámetros ingresados");
			}
			cargarTabla();
		}
	}

	private void cargarTabla() {

		
		tblReporte.setDefaultRenderer(Object.class, new FormatoTabla());
		String[] columnNames = { "", "COD_ESTABLECIMIENTO", "NOM_ESTABLECIMIENTO", "NOM_PROFESIONAL",
				"ESPECIALIDAD_PROFESIONAL", "PREVENTIVA_PRIMERA", "PREVENTIVA_SEGUNDA", "MORBILIDAD_PRIMERA",
				"MORBILIDAD_SEGUNDA", "ATENCION_VACUNAS","TOTAL_ATENCIONES" };

		modeloTabla = new ProduccionFechaEspecialidadTablaModel(columnNames);
		controles = controles == null ? new ArrayList<>() : controles;
		modeloTabla.addFila(controles);
		tblReporte.setModel(modeloTabla);
		tblReporte.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		modeloTabla.ocultaColumnaCodigo(tblReporte);
		modeloTabla.definirAnchoColumnas(tblReporte, new int[] { 200, 400, 400, 200, 200, 200, 200, 200, 200, 200 });

	}

	private void exportar() {
            
            String autor = VariableSesion.getInstanciaVariablesSession().getUsuario().getPersonaId().getPrimernombre() + " ";
         	   autor += VariableSesion.getInstanciaVariablesSession().getUsuario().getPersonaId().getPrimerapellido() + " ";
            StringBuilder fechas = new StringBuilder();
                fechas.append("DESDE: ").append(dttDesde.getDateStringOrEmptyString()).append(" HASTA: ").append(dttHasta.getDateStringOrEmptyString());
            ReportesUtil.exportarXlsReportes(tblReporte, "Reporte Produccion", autor, "xls","Reporte de producción del profesional de la salud",fechas.toString());
	}
private void cerrar(){
		
		dttDesde.setText("");
		dttHasta.setText("");
		if(modeloTabla != null){
			modeloTabla.clear();
		}
		
	}
}
