/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.principal.wizard;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.github.cjwizard.WizardSettings;

import ec.gob.msp.rdacaa.admision.controller.AdmisionController;
import ec.gob.msp.rdacaa.admision.forma.Admision;
import ec.gob.msp.rdacaa.principal.controller.CommonPanelController;

/**
 *
 * @author miguel.faubla
 */
@Component
public class PanelAdmision extends com.github.cjwizard.WizardPage {


	private static final long serialVersionUID = -4703993024736485982L;
	private final Admision admision;
    private final AdmisionController admisionController;
    private final CommonPanelController commonPanelController;

    @Autowired
    public PanelAdmision(Admision admision, AdmisionController admisionController, CommonPanelController commonPanelController) {
        super("Registro de paciente", "Admisión");
        this.admision = admision;
        this.admisionController = admisionController;
        this.commonPanelController = commonPanelController;
    }

    @PostConstruct
    private void init() {
        admision.setVisible(true);
        add(admision);
    }

    @Override
    public boolean onNext(WizardSettings settings) {
        return admisionController.validateForm();
    }

    @Override
    public void updateSettings(WizardSettings settings) {
        commonPanelController.refreshPanels();
    }

}
