/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.amed.controller;

import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;

import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import ec.gob.msp.rdacaa.admision.model.CmbDetalleCatalogoCellRenderer;
import ec.gob.msp.rdacaa.admision.model.DetalleCatalogoToStringConverter;
import ec.gob.msp.rdacaa.amed.forma.DiagnosticoCIE;
import ec.gob.msp.rdacaa.amed.model.AmedCmbCodigoCIEModel;
import ec.gob.msp.rdacaa.amed.model.AmedCmbCondicionDiagnosticoModel;
import ec.gob.msp.rdacaa.amed.model.AmedCmbDescripcionCIEModel;
import ec.gob.msp.rdacaa.amed.model.AmedCmbTipoAtencionModel;
import ec.gob.msp.rdacaa.amed.model.AmedCmbTipoDiagnosticoModel;
import ec.gob.msp.rdacaa.amed.model.CieDescripcionToStringConverter;
import ec.gob.msp.rdacaa.amed.model.CmbCieDescripcionCellRenderer;
import ec.gob.msp.rdacaa.amed.model.DiagnosticoAtencionTablaModel;
import ec.gob.msp.rdacaa.business.entity.Cie;
import ec.gob.msp.rdacaa.business.entity.Detallecatalogo;
import ec.gob.msp.rdacaa.business.entity.Diagnosticoatencion;
import ec.gob.msp.rdacaa.business.service.CieService;
import ec.gob.msp.rdacaa.business.service.CievigilanciaService;
import ec.gob.msp.rdacaa.business.service.CiexespecialidadService;
import ec.gob.msp.rdacaa.business.service.DetallecatalogoService;
import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.utilitario.enumeracion.ComunEnum;
import ec.gob.msp.rdacaa.utilitario.forma.ButtonColumn;
import ec.gob.msp.rdacaa.utilitario.forma.FormatoTabla;
import ec.gob.msp.rdacaa.utilitario.forma.UtilitarioForma;
import ec.gob.msp.rdacaa.utilitario.listas.ListasUtil;

/**
 *
 * @author eduardo
 */
@Controller
public class DiagnosticoCIEController {

	private final DiagnosticoCIE diagnosticoCIEFrame;
	private final DetallecatalogoService detallecatalogoService;
	private final CieService cieService;
	private final CiexespecialidadService ciexespecialidadService;
	private final CievigilanciaService cievigilanciaService;
	private final AmedCmbCodigoCIEModel amedCmbCodigoCIEModel;
	private final AmedCmbDescripcionCIEModel amedCmbDescripcionCIEModel;
	private final AmedCmbTipoAtencionModel amedCmbTipoAtencionModel;
	private final AmedCmbTipoDiagnosticoModel amedCmbTipoDiagnosticoModel;
	private final AmedCmbCondicionDiagnosticoModel amedCmbCondicionDiagnosticoModel;
	private DiagnosticoAtencionTablaModel modeloTabla;
	private final VariableSesion variableSesion = VariableSesion.getInstanciaVariablesSession();
	private JComboBox<Cie> cmbDescripcionCie;
	private JComboBox<Detallecatalogo> cmbTipoDiagnostico;
	private JComboBox<Detallecatalogo> cmbTipoAtencion;
	private JComboBox<Detallecatalogo> cmbCondicionDiagnostico;
	private JTable tblDiagnosticos;
	private final List<Diagnosticoatencion> listaDiagnostico = new ArrayList<>();
	private JButton btnAgregar;
	private final Object[] controlesValidar = new Object[3];
	private Detallecatalogo detallecatalogoNull;
	private Cie cieNull;
	private JTextField txtCertificado;
	private static final String CIE_CERTIFICADO = "Z027";
	private JLabel alerta_diagnostico;

	@Autowired
	public DiagnosticoCIEController(DiagnosticoCIE diagnosticoCIEFrame, DetallecatalogoService detallecatalogoService,
			CieService cieService, CiexespecialidadService ciexespecialidadService,
			CievigilanciaService cievigilanciaService, AmedCmbCodigoCIEModel amedCmbCodigoCIEModel,
			AmedCmbDescripcionCIEModel amedCmbDescripcionCIEModel) {
		this.diagnosticoCIEFrame = diagnosticoCIEFrame;
		this.cieService = cieService;
		this.ciexespecialidadService = ciexespecialidadService;
		this.cievigilanciaService = cievigilanciaService;
		this.detallecatalogoService = detallecatalogoService;
		this.amedCmbCodigoCIEModel = amedCmbCodigoCIEModel;
		this.amedCmbDescripcionCIEModel = amedCmbDescripcionCIEModel;
		this.amedCmbTipoAtencionModel = new AmedCmbTipoAtencionModel();
		this.amedCmbTipoDiagnosticoModel = new AmedCmbTipoDiagnosticoModel();
		this.amedCmbCondicionDiagnosticoModel = new AmedCmbCondicionDiagnosticoModel();
	}

	@PostConstruct
	private void init() {
		loadDefaultCombos();
		this.btnAgregar = diagnosticoCIEFrame.getBtnAgregar();
		this.cmbCondicionDiagnostico = diagnosticoCIEFrame.getCmbCondicionDiagnostico();
		this.cmbDescripcionCie = diagnosticoCIEFrame.getCmbDescripcionCie();
		this.cmbTipoDiagnostico = diagnosticoCIEFrame.getCmbTipoDiagnostico();
		this.cmbTipoAtencion = diagnosticoCIEFrame.getCmbTipoAtencion();
		this.tblDiagnosticos = diagnosticoCIEFrame.getTblDiagnosticos();
		this.txtCertificado = diagnosticoCIEFrame.getCajaTextoIess1();
		AutoCompleteDecorator.decorate(cmbDescripcionCie, new CieDescripcionToStringConverter());
		AutoCompleteDecorator.decorate(cmbCondicionDiagnostico, new DetalleCatalogoToStringConverter());
		AutoCompleteDecorator.decorate(cmbTipoDiagnostico, new DetalleCatalogoToStringConverter());
		AutoCompleteDecorator.decorate(cmbTipoAtencion, new DetalleCatalogoToStringConverter());
		this.alerta_diagnostico = diagnosticoCIEFrame.getGetLblAlerta();
		UtilitarioForma.habilitarControles(false, txtCertificado);
		cmbTipoDiagnostico.setEnabled(false);

		controlesValidar[0] = cmbTipoDiagnostico;
		controlesValidar[1] = cmbTipoAtencion;

		loadDescripcionCie();
		loadCondicionDiagnostico();
		loadTipoAtencion();
		loadTipoDiagnostico();
		cargarTabla();
		this.cmbDescripcionCie.addActionListener(e -> habilitarCampoCErtificado());

		this.cmbDescripcionCie.addActionListener((ActionEvent e) -> {
			if (amedCmbDescripcionCIEModel.getSelectedItem() != null
					&& amedCmbDescripcionCIEModel.getSelectedItem().getId() != 0) {
				enableTipoDiagnostico(amedCmbDescripcionCIEModel.getSelectedItem().getCodigo());
			}
		});

		this.cmbTipoDiagnostico.addActionListener((ActionEvent e) -> {
			if (amedCmbTipoDiagnosticoModel.getSelectedItem() != null) {
				if (amedCmbTipoDiagnosticoModel.getSelectedItem().getId() == 228) { // Morbilidad
					cmbCondicionDiagnostico.setEnabled(true);
					controlesValidar[2] = cmbCondicionDiagnostico;
				} else { // Prevencion
					cmbCondicionDiagnostico.setEnabled(false);
					cmbCondicionDiagnostico.setSelectedItem(detallecatalogoNull);
					controlesValidar[2] = new Object();
				}
			}
		});

		btnAgregar.addMouseListener(new MouseListener() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (amedCmbDescripcionCIEModel.getSelectedItem().getCodigo().contains(CIE_CERTIFICADO)) {
					Object[] inputs = { cmbDescripcionCie, cmbTipoAtencion, txtCertificado };
					if (UtilitarioForma.validarCamposRequeridos(inputs)) {
						getCIE();
					}
				} else {
					if (validateCamposRequeridos()) {
						getCIE();
					}
				}
			}

			@Override
			public void mousePressed(MouseEvent e) {
				// No se usa
			}

			@Override
			public void mouseReleased(MouseEvent e) {
				// No se usa
			}

			@Override
			public void mouseEntered(MouseEvent e) {
				// No se usa
			}

			@Override
			public void mouseExited(MouseEvent e) {
				// No se usa
			}
		});
		txtCertificado.addKeyListener(new java.awt.event.KeyAdapter() {
			@Override
			public void keyTyped(java.awt.event.KeyEvent evt) {
				UtilitarioForma.soloNumeros(evt);
				UtilitarioForma.validarLongitudCampo(evt, 10);
			}
		});
		cmbDescripcionCie.addActionListener(e -> createAlertVacuna());

	}

	public void enableTipoDiagnostico(String codigoCie) {
		String caracter = codigoCie.trim().toUpperCase().substring(0, 1);
		if (caracter.equals("Z")) {
			cmbTipoDiagnostico.setSelectedItem(detallecatalogoService.findOneByDetalleId(227)); // Prevencion
			cmbCondicionDiagnostico.setEnabled(false);
		} else {
			cmbTipoDiagnostico.setSelectedItem(detallecatalogoService.findOneByDetalleId(228)); // Morbilidad
			cmbCondicionDiagnostico.setEnabled(true);
		}
	}

	public void loadDescripcionCie() {
		if(variableSesion.getPersona() != null) {
			cleanTableByChangueSexId();
		}

		List<Cie> listDiagnosticoDescripcionCie = null;
		if (VariableSesion.getInstanciaVariablesSession().getPersona() != null
				&& amedCmbDescripcionCIEModel.getSize() == 0) {
			listDiagnosticoDescripcionCie = cieService.findAllCie10ConsolidadorByEdadAndIdSexoAndIdEspecialidad(
					VariableSesion.getInstanciaVariablesSession().getDiasPersona(),
					VariableSesion.getInstanciaVariablesSession().getMesesPersona(),
					VariableSesion.getInstanciaVariablesSession().getAniosPersona(),
					VariableSesion.getInstanciaVariablesSession().getPersona().getCtsexoId().getId(),
					VariableSesion.getInstanciaVariablesSession().getCtEspecialidad().getId());
			if (listDiagnosticoDescripcionCie.isEmpty()) {
				listDiagnosticoDescripcionCie = cieService.
						findAllCie10ConsolidadorByEdadAndIdSexoAndNoEspecialidad(VariableSesion.getInstanciaVariablesSession().getDiasPersona(),
								VariableSesion.getInstanciaVariablesSession().getMesesPersona(),
								VariableSesion.getInstanciaVariablesSession().getAniosPersona(),
								VariableSesion.getInstanciaVariablesSession().getPersona().getCtsexoId().getId());
			}
		}
		if (listDiagnosticoDescripcionCie != null && !listDiagnosticoDescripcionCie.isEmpty()) {
			ListasUtil.ordenarLista(listDiagnosticoDescripcionCie, "codigo", true);
			amedCmbDescripcionCIEModel.clear();
			amedCmbDescripcionCIEModel.addElements(listDiagnosticoDescripcionCie);
			cmbDescripcionCie.setRenderer(new CmbCieDescripcionCellRenderer());
			cmbDescripcionCie.setModel(amedCmbDescripcionCIEModel);
		}
		cmbDescripcionCie.setSelectedItem(cieNull);
	}
	
	public void cleanTableByChangueSexId() {
		// Limpiar Lista cuando cambien de sexo
		if(variableSesion.getPersona() != null ) {
			if(variableSesion.getCambioInformacionDiagnostico() != 0) {
					listaDiagnostico.clear();
					amedCmbDescripcionCIEModel.clear();
					cargarTabla();
					VariableSesion.getInstanciaVariablesSession().setCambioInformacionDiagnostico(0);
					variableSesion.setDiagnosticoatencion(null);
			}
		}
	}

	private void loadCondicionDiagnostico() {
		List<Detallecatalogo> listaCondicionDiagnostico = detallecatalogoService.findAllCondicionDiagnostico();
		amedCmbCondicionDiagnosticoModel.clear();
		amedCmbCondicionDiagnosticoModel.addElements(listaCondicionDiagnostico);
		cmbCondicionDiagnostico.setRenderer(new CmbDetalleCatalogoCellRenderer());
		cmbCondicionDiagnostico.setModel(amedCmbCondicionDiagnosticoModel);
		cmbCondicionDiagnostico.setSelectedItem(detallecatalogoNull);
	}

	private void loadTipoAtencion() {
		List<Detallecatalogo> listaTipoAtencion = detallecatalogoService.findAllTipoAtencion();
		amedCmbTipoAtencionModel.clear();
		amedCmbTipoAtencionModel.addElements(listaTipoAtencion);
		cmbTipoAtencion.setRenderer(new CmbDetalleCatalogoCellRenderer());
		cmbTipoAtencion.setModel(amedCmbTipoAtencionModel);
		cmbTipoAtencion.setSelectedItem(detallecatalogoNull);
	}

	private void loadTipoDiagnostico() {
		List<Detallecatalogo> listaTipoDiagnostico = detallecatalogoService.findAllTipoDiagnostico();
		amedCmbTipoDiagnosticoModel.clear();
		amedCmbTipoDiagnosticoModel.addElements(listaTipoDiagnostico);
		cmbTipoDiagnostico.setRenderer(new CmbDetalleCatalogoCellRenderer());
		cmbTipoDiagnostico.setModel(amedCmbTipoDiagnosticoModel);
		cmbTipoDiagnostico.setSelectedItem(detallecatalogoNull);
	}

	private void cargarTabla() {
		tblDiagnosticos.setDefaultRenderer(Object.class, new FormatoTabla());
		String[] columns = { "Id", "Cie", "Descripción", "Tipo Diagnóstico", "Tipo Atención", "Condición Diagnóstico",
				"Certificado", "Eliminar" };
		modeloTabla = new DiagnosticoAtencionTablaModel(columns);
		modeloTabla.addFila(listaDiagnostico != null && listaDiagnostico.isEmpty() ? new ArrayList() : listaDiagnostico);
		tblDiagnosticos.setModel(modeloTabla);
		if (listaDiagnostico != null && !listaDiagnostico.isEmpty()) {
			loadButtonDelete(tblDiagnosticos, 7);
		}
		modeloTabla.ocultaColumnaCodigo(tblDiagnosticos);
		modeloTabla.definirAnchoColumnas(tblDiagnosticos, new int[] { 10, 100, 30, 28, 30, 20, 25 });
	}

	public boolean validateForm() {
		boolean validarForma = false;
		if (!VariableSesion.getInstanciaVariablesSession().getCtEspecialidad().getId()
				.equals(Integer.parseInt(ComunEnum.IDENTIFICADOR_ENFERMERIA.getDescripcion()))
				&& !VariableSesion.getInstanciaVariablesSession().getCtEspecialidad().getId()
						.equals(Integer.parseInt(ComunEnum.IDENTIFICADOR_ENFERMERIA_RURAL.getDescripcion()))
				&& !VariableSesion.getInstanciaVariablesSession().getCtEspecialidad().getId()
						.equals(Integer.parseInt(ComunEnum.IDENTIFICADOR_AUXILIAR_ENFERMERIA.getDescripcion()))) {
			if (!listaDiagnostico.isEmpty() && listaDiagnostico.size() > 0) {
				validarForma = true;
			} else {
				UtilitarioForma.muestraMensaje("", diagnosticoCIEFrame, "ERROR",
						"Estimado usuario debe al menos agregar un (CIE10) a la lista de diagnósticos.");
			}
		} else {
			validarForma = true;
		}

		return validarForma;
	}

	public boolean validateCamposRequeridos() {
		boolean validarForma = UtilitarioForma.validarCamposRequeridos(controlesValidar);

		return validarForma;
	}

	public void validateCieVigilancia() {
		if (amedCmbDescripcionCIEModel.getSelectedItem() != null
				&& amedCmbCondicionDiagnosticoModel.getSelectedItem() != null
				&& amedCmbTipoAtencionModel.getSelectedItem() != null) {
			Boolean cievigilancia = cievigilanciaService.findOneAlertaCieVigilancia(
					amedCmbDescripcionCIEModel.getSelectedItem().getCodigo(),
					amedCmbTipoAtencionModel.getSelectedItem().getId(),
					amedCmbCondicionDiagnosticoModel.getSelectedItem().getId());
			if (cievigilancia == true) {
				UtilitarioForma.muestraMensaje("¡ADVERTENCIA VIGILANCIA!", diagnosticoCIEFrame, "ATENCION",
						"A seleccionado un diagnóstico de epidemiología, por favor notificar inmediatamente");
			}
		}
	}

	private void getCIE() {
		Diagnosticoatencion diagnosticoatencion = new Diagnosticoatencion();
		diagnosticoatencion.setCieId(amedCmbDescripcionCIEModel.getSelectedItem());
		if (amedCmbCondicionDiagnosticoModel.getSelectedItem().getId() != 0) {
			diagnosticoatencion.setCtcondiciondiagnositcoId(amedCmbCondicionDiagnosticoModel.getSelectedItem());
		}
		diagnosticoatencion.setCtmorbilidaddiagnosticoId(amedCmbTipoDiagnosticoModel.getSelectedItem());
		diagnosticoatencion.setCtprevenciondiagnosticoId(amedCmbTipoAtencionModel.getSelectedItem());
		diagnosticoatencion.setEstado(1);
		diagnosticoatencion.setObservacion(0);
		if (txtCertificado.getText().equals("")) {
			diagnosticoatencion.setNumerocertificado("");
		} else {
			diagnosticoatencion.setNumerocertificado(txtCertificado.getText());
		}

		if (listaDiagnostico.isEmpty()) {
			listaDiagnostico.add(diagnosticoatencion);
		} else {
			if (listaDiagnostico.size() == 5) {
				UtilitarioForma.muestraMensaje("", diagnosticoCIEFrame, "ERROR",
						"Estimado usuario le recordamos que puede ingresar un máximo de 5 códigos, por favor verificar.");
			} else {
				Optional<Diagnosticoatencion> diagnostico = listaDiagnostico.stream()
						.filter(diag -> diag.getCieId().getCodigo().equals(diagnosticoatencion.getCieId().getCodigo()))
						.findFirst();
				if (diagnostico.isPresent()) {
					UtilitarioForma.muestraMensaje("", diagnosticoCIEFrame, "ERROR",
							"Estimado usuario el código CIE ya ha sido agregado, por favor verificar.");
				} else {
					listaDiagnostico.add(diagnosticoatencion);
				}
			}
		}

		variableSesion.setDiagnosticoatencion(listaDiagnostico);
		validateCieVigilancia();
		cleanInput(null);
		cargarTabla();
	}

	public void loadDefaultCombos() {
		detallecatalogoNull = new Detallecatalogo() {
			private static final long serialVersionUID = 1L;
			boolean isNoSelectable = true;
		};
		detallecatalogoNull.setId(0);
		detallecatalogoNull.setDescripcion("Seleccione");

		cieNull = new Cie() {
			private static final long serialVersionUID = 1L;
			boolean isNoSelectable = true;
		};
		cieNull.setId(0);
		cieNull.setCodigo("Seleccione");
	}

	private void loadButtonDelete(JTable table, int column) {
		try {
			ButtonColumn buttonColumn = new ButtonColumn(table, createActionExport(), column);
			Image img = ImageIO.read(getClass().getResource("/principal/grupoprioritario.jpg"));
			buttonColumn.getTableCellRendererComponent(table, new ImageIcon(img), false, true, column, column);
		} catch (IOException ex) {
			Logger.getLogger(DiagnosticoCIEController.class.getName()).log(Level.SEVERE, null, ex);
		}
	}

	private Action createActionExport() {
		return new AbstractAction() {
			@Override
			public void actionPerformed(ActionEvent e) {
				listaDiagnostico.remove(tblDiagnosticos.getSelectedRow());
				cargarTabla();
			}
		};
	}

	private void habilitarCampoCErtificado() {
		if (amedCmbDescripcionCIEModel.getSelectedItem() != null) {
			if (amedCmbDescripcionCIEModel.getSelectedItem().getCodigo().contains(CIE_CERTIFICADO)) {
				UtilitarioForma.habilitarControles(true, txtCertificado);

			} else {
				UtilitarioForma.habilitarControles(false, txtCertificado);
			}
		}
	}

	public void cleanInput(String panel) {
		if (panel != null && panel.equals("commons")) {
			listaDiagnostico.clear();
			cargarTabla();
		}
		Object[] inputs = { cmbDescripcionCie, cmbTipoAtencion, cmbTipoDiagnostico, cmbCondicionDiagnostico };
		UtilitarioForma.limpiarCampos(diagnosticoCIEFrame, inputs);
		cmbDescripcionCie.setSelectedItem(cieNull);
		cmbCondicionDiagnostico.setSelectedItem(detallecatalogoNull);
		cmbTipoAtencion.setSelectedItem(detallecatalogoNull);
		preloadCmbTipoAtencion();
		cmbTipoDiagnostico.setSelectedItem(detallecatalogoNull);
		txtCertificado.setText("");
	}

	public void createAlertVacuna() {

		alerta_diagnostico.setVisible(true);
		alerta_diagnostico.setBorder(BorderFactory.createLineBorder(Color.RED, 1));
		alerta_diagnostico.setText(
				"<html><body><span style=\"color: #FF0000;\">¡Estimado usuario favor verificar que el Diagnóstico seleccionado sea el correcto!</span></body></html>");

	}
	
	public void preloadCmbTipoAtencion() {
		if (VariableSesion.getInstanciaVariablesSession().getCtEspecialidad().getId()
				.equals(Integer.parseInt(ComunEnum.IDENTIFICADOR_ENFERMERIA.getDescripcion()))
				|| VariableSesion.getInstanciaVariablesSession().getCtEspecialidad().getId()
						.equals(Integer.parseInt(ComunEnum.IDENTIFICADOR_ENFERMERIA_RURAL.getDescripcion()))
				|| VariableSesion.getInstanciaVariablesSession().getCtEspecialidad().getId()
						.equals(Integer.parseInt(ComunEnum.IDENTIFICADOR_AUXILIAR_ENFERMERIA.getDescripcion()))) {
			cmbTipoAtencion.setSelectedItem(detallecatalogoService.findTipoAtencionSubsecuenteDiagnostico());
			cmbTipoAtencion.setEnabled(false);
		}else {
			cmbTipoAtencion.setEnabled(true);
		}
	}
}
