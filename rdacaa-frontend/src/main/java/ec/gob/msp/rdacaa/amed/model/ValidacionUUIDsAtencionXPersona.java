package ec.gob.msp.rdacaa.amed.model;

public enum ValidacionUUIDsAtencionXPersona {

	NO_INICIALIZADO, NO_EXISTE_CAMPO_EN_BASE_O_ERROR, UUIDS_PERSONA_HAS_NULL, UUIDS_PERSONA_OK, UUIDS_ATENCION_HAS_NULL,
	UUIDS_ATENCION_OK, UUIDS_ATENCION_X_PERSONA_IS_VALID, UUIDS_ATENCION_X_PERSONA_IS_INVALID
}
