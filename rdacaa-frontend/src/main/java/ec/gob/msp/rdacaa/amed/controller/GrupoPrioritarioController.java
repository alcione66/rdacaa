/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.amed.controller;



import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.time.ZoneId;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JList;
import javax.swing.JOptionPane;

import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import ec.gob.msp.rdacaa.admision.model.AdmisionDetalleCatalogoModel;
import ec.gob.msp.rdacaa.admision.model.CmbDetalleCatalogoCellRenderer;
import ec.gob.msp.rdacaa.admision.model.DetalleCatalogoToStringConverter;
import ec.gob.msp.rdacaa.amed.forma.GrupoPrioritario;
import ec.gob.msp.rdacaa.amed.model.AmedCmbLugarAtencionModel;
import ec.gob.msp.rdacaa.amed.model.AmedGrupoPrioritarioModel;
import ec.gob.msp.rdacaa.business.entity.Atenciongprioritario;
import ec.gob.msp.rdacaa.business.entity.Detallecatalogo;
import ec.gob.msp.rdacaa.business.entity.Estrategiapaciente;
import ec.gob.msp.rdacaa.business.entity.Intraextramural;
import ec.gob.msp.rdacaa.business.service.DetallecatalogoService;
import ec.gob.msp.rdacaa.business.service.GprioritariovalidacionService;
import ec.gob.msp.rdacaa.business.service.LugaratencionvalidacionService;
import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.utilitario.enumeracion.ComunEnum;
import ec.gob.msp.rdacaa.utilitario.forma.UtilitarioForma;

/**
 *
 * @author eduardo
 */
@Controller
public class GrupoPrioritarioController {

    private GrupoPrioritario grupoPrioritarioFrame;

    private DetallecatalogoService detallecatalogoService;
    private GprioritariovalidacionService gprioritariovalidacionService; 
    private LugaratencionvalidacionService lugaratencionvalidacionService;
    private AmedGrupoPrioritarioModel amedCmbGrupoPrioritarioModel;
    private AmedGrupoPrioritarioModel amedCmbGrupoPrioritarioSelectedModel;
    private AmedCmbLugarAtencionModel amedCmbLugarAtencionModel;
    private AdmisionDetalleCatalogoModel amedCmbEstrategiaModel;

    private JList<Detallecatalogo> lstGrupoPrioritarioLoad;
    private JList<Detallecatalogo> lstGrupoPrioritarioSelected;
    private JComboBox<Detallecatalogo> cmbLugarAtencion;
    private JComboBox<Detallecatalogo> cmbEstrategia;
    private JButton btnAgregarGPSelected;
    private JButton btnRegresarGPSelected;
    private VariableSesion variableSesion;
    
    private static final String ESTABLECIMIENTO = "Establecimiento";
    private static final Integer INTRAMURAL = 2589;
    private static final Integer EXTRAMURAL = 2590;
    private static final String NOAPLICA = "No aplica";
    private static final Integer CODIGONOAPLICA = 633;
    private Detallecatalogo detallecatalogoNull;
    private Detallecatalogo dcNinguno;
    

    @Autowired
    public GrupoPrioritarioController(GrupoPrioritario grupoPrioritarioFrame,
            DetallecatalogoService detallecatalogoService,
            GprioritariovalidacionService gprioritariovalidacionService,
            LugaratencionvalidacionService lugaratencionvalidacionService
            ) {
        this.grupoPrioritarioFrame = grupoPrioritarioFrame;
        this.amedCmbGrupoPrioritarioModel = new AmedGrupoPrioritarioModel();
        this.detallecatalogoService = detallecatalogoService;
        this.amedCmbGrupoPrioritarioSelectedModel = new AmedGrupoPrioritarioModel();
        this.amedCmbLugarAtencionModel = new AmedCmbLugarAtencionModel();
        this.gprioritariovalidacionService = gprioritariovalidacionService;
        this.lugaratencionvalidacionService = lugaratencionvalidacionService;
        this.amedCmbEstrategiaModel = new AdmisionDetalleCatalogoModel();
    }

    @PostConstruct
    private void init() {
        this.lstGrupoPrioritarioLoad = grupoPrioritarioFrame.getLstGrupoPrioritarioLoad();
        this.btnAgregarGPSelected = grupoPrioritarioFrame.getBtnAgregarGPSelected();
        this.btnRegresarGPSelected = grupoPrioritarioFrame.getBtnRegresarGPSelected();
        this.lstGrupoPrioritarioSelected = grupoPrioritarioFrame.getLstGrupoPrioritarioSelected();
        this.cmbLugarAtencion = grupoPrioritarioFrame.getCmbLugarAtencion();
        this.cmbEstrategia = grupoPrioritarioFrame.getCmbEstrategia();
        loadDefaultCombos();
        AutoCompleteDecorator.decorate(cmbLugarAtencion, new DetalleCatalogoToStringConverter());
        AutoCompleteDecorator.decorate(cmbEstrategia, new DetalleCatalogoToStringConverter());
        loadCatalogoGrupoPrioritario();
        loadLugarAtencion();
        loadEstrategiaMSP();
        btnAgregarGPSelected.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                grupoPrioritarioSelected();
                
            }

            @Override
            public void mousePressed(MouseEvent e) {
            	//no se usa
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            	//no se usa
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            	//no se usa
            }

            @Override
            public void mouseExited(MouseEvent e) {
            	//no se usa
            }
        });
        btnRegresarGPSelected.addMouseListener(new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                grupoPrioritarioSelectedRemove();
            }

            @Override
            public void mousePressed(MouseEvent e) {
            	//no se usa
            }

            @Override
            public void mouseReleased(MouseEvent e) {
            	//no se usa
            }

            @Override
            public void mouseEntered(MouseEvent e) {
            	//no se usa
            }

            @Override
            public void mouseExited(MouseEvent e) {
            	//no se usa
            }
        });
        cmbLugarAtencion.addActionListener(dt -> validateTipoAtencion());
    }

    public void loadCatalogoGrupoPrioritario() {
        List<Detallecatalogo> listaGruposPrioritarios = null;
        variableSesion = VariableSesion.getInstanciaVariablesSession();
        if(variableSesion.getPersona() != null ){
        	if(variableSesion.getGruposPrioritarios() == null){
        		listaGruposPrioritarios = gprioritariovalidacionService.findGruposPrioritariosFiltrados(variableSesion.getAniosPersona(), variableSesion.getMesesPersona(), variableSesion.getDiasPersona(), variableSesion.getPersona().getCtsexoId().getId());
        		VariableSesion.getInstanciaVariablesSession().setCambioInformacion(0);
        	}else{
        		if(variableSesion.getCambioInformacion() != 0){
            		listaGruposPrioritarios = gprioritariovalidacionService.findGruposPrioritariosFiltrados(variableSesion.getAniosPersona(), variableSesion.getMesesPersona(), variableSesion.getDiasPersona(), variableSesion.getPersona().getCtsexoId().getId());
            		amedCmbGrupoPrioritarioSelectedModel.removeAllElements();
            		variableSesion.setGruposPrioritarios(null);
            		VariableSesion.getInstanciaVariablesSession().setCambioInformacion(0);
            		lstGrupoPrioritarioLoad.setEnabled(true);
            	}else{
            		listaGruposPrioritarios = gprioritariovalidacionService.findGruposPrioritariosFiltrados(variableSesion.getAniosPersona(), variableSesion.getMesesPersona(), variableSesion.getDiasPersona(), variableSesion.getPersona().getCtsexoId().getId());
            		for (Atenciongprioritario atenciongprioritario : variableSesion.getGruposPrioritarios()) {
            			listaGruposPrioritarios.remove(atenciongprioritario.getCtgprioritarioId());
					}
            	}
        	}
        	amedCmbGrupoPrioritarioModel.clear();
            amedCmbGrupoPrioritarioModel.addElements(listaGruposPrioritarios);
            lstGrupoPrioritarioLoad.setCellRenderer(new CmbDetalleCatalogoCellRenderer());
            lstGrupoPrioritarioLoad.setModel(amedCmbGrupoPrioritarioModel);
            lstGrupoPrioritarioSelected.setCellRenderer(new CmbDetalleCatalogoCellRenderer());
        }
    }

    private void grupoPrioritarioSelected() {
    	List<Detallecatalogo> listaGruposPrioritariosSelected = new ArrayList<>();
    	if (!lstGrupoPrioritarioLoad.isSelectionEmpty()){
    		if (amedCmbGrupoPrioritarioSelectedModel.size() < 5 || (amedCmbGrupoPrioritarioModel.getElementAt(lstGrupoPrioritarioLoad.getSelectedIndex()).getDescripcion().equals(NOAPLICA)) ){
    			if(amedCmbGrupoPrioritarioModel.getElementAt(lstGrupoPrioritarioLoad.getSelectedIndex()).getDescripcion().equals(NOAPLICA)){
                    amedCmbGrupoPrioritarioSelectedModel.removeAllElements();
                    listaGruposPrioritariosSelected.add(detallecatalogoService.findOneByDetalleId(CODIGONOAPLICA));
                    amedCmbGrupoPrioritarioSelectedModel.addElements(listaGruposPrioritariosSelected);
                    lstGrupoPrioritarioSelected.setModel(amedCmbGrupoPrioritarioSelectedModel);
                    loadCatalogoGrupoPrioritario();
                    lstGrupoPrioritarioLoad.setEnabled(false);
                }else{
                    listaGruposPrioritariosSelected.add(amedCmbGrupoPrioritarioModel.getElementAt(lstGrupoPrioritarioLoad.getSelectedIndex()));
                    amedCmbGrupoPrioritarioSelectedModel.addElements(listaGruposPrioritariosSelected);
                    lstGrupoPrioritarioSelected.setModel(amedCmbGrupoPrioritarioSelectedModel);
                    amedCmbGrupoPrioritarioModel.removeElement(amedCmbGrupoPrioritarioModel.getElementAt(lstGrupoPrioritarioLoad.getSelectedIndex()));
                    lstGrupoPrioritarioLoad.setModel(amedCmbGrupoPrioritarioModel);
                }
    		}else{
    			JOptionPane.showMessageDialog(null, "A superado el límite máximo ");
    		}
    	}else {
            JOptionPane.showMessageDialog(null, "No seleccionó un elemento o la lista está vacía");
         }
    }
    
    private void grupoPrioritarioSelectedRemove() {
        List<Detallecatalogo> listaGruposPrioritariosSelectedRemove = new ArrayList<>();
        if (!lstGrupoPrioritarioSelected.isSelectionEmpty()) {
            if(amedCmbGrupoPrioritarioSelectedModel.getElementAt(lstGrupoPrioritarioSelected.getSelectedIndex()).getDescripcion().equals(NOAPLICA)){
                amedCmbGrupoPrioritarioSelectedModel.removeAllElements();
                loadCatalogoGrupoPrioritario();
                lstGrupoPrioritarioLoad.setEnabled(true);
            }else{
                listaGruposPrioritariosSelectedRemove.add(amedCmbGrupoPrioritarioSelectedModel.getElementAt(lstGrupoPrioritarioSelected.getSelectedIndex()));
                amedCmbGrupoPrioritarioModel.addElements(listaGruposPrioritariosSelectedRemove);
                lstGrupoPrioritarioLoad.setModel(amedCmbGrupoPrioritarioModel);
                amedCmbGrupoPrioritarioSelectedModel.removeElement(amedCmbGrupoPrioritarioSelectedModel.getElementAt(lstGrupoPrioritarioSelected.getSelectedIndex()));
                lstGrupoPrioritarioSelected.setModel(amedCmbGrupoPrioritarioSelectedModel);
            }
        } else {
           JOptionPane.showMessageDialog(null, "No seleccionó un elemento o la lista está vacía");
        }
    }
    
    public void loadLugarAtencion(){
    	List<Detallecatalogo> lugarAtencion = null;
    	
    	if(cmbLugarAtencion.getSelectedItem() == null || cmbLugarAtencion.getSelectedItem().equals(detallecatalogoNull) 
    			|| variableSesion.getCambioInformacion() != 0){
    		if(VariableSesion.getInstanciaVariablesSession().getPersona() != null){
        		lugarAtencion = lugaratencionvalidacionService.findLugarAtencionFiltrados(VariableSesion.getInstanciaVariablesSession().getAniosPersona(), VariableSesion.getInstanciaVariablesSession().getMesesPersona(), VariableSesion.getInstanciaVariablesSession().getDiasPersona(), VariableSesion.getInstanciaVariablesSession().getPersona().getCtsexoId().getId());
        	}else{
        		lugarAtencion = detallecatalogoService.findAllLugarAtencion();
        	}
        	amedCmbLugarAtencionModel.clear();
            amedCmbLugarAtencionModel.addElements(lugarAtencion);
            cmbLugarAtencion.setRenderer(new CmbDetalleCatalogoCellRenderer());
            cmbLugarAtencion.setModel(amedCmbLugarAtencionModel);
            cmbLugarAtencion.setSelectedItem(detallecatalogoNull);
        }
    }
    
    private void loadEstrategiaMSP(){
        List<Detallecatalogo> estrategias = detallecatalogoService.findAllEstrategiasMSP();
        loadCombosNinguno();
        estrategias.add(dcNinguno);
        amedCmbEstrategiaModel.clear();
        amedCmbEstrategiaModel.addElements(estrategias);
        cmbEstrategia.setRenderer(new CmbDetalleCatalogoCellRenderer());
        cmbEstrategia.setModel(amedCmbEstrategiaModel);
        cmbEstrategia.setSelectedItem(detallecatalogoNull);
    }
      
    private void validateTipoAtencion(){
        if(amedCmbLugarAtencionModel.getSelectedItem() != null) {
            String tipoAtencion = (amedCmbLugarAtencionModel.getSelectedItem().getDescripcion().equals(ESTABLECIMIENTO)) ? "ATENCIÓN: INTRAMURAL" : "ATENCIÓN: EXTRAMURAL";
            grupoPrioritarioFrame.getLblIntraExtraMural().setText(tipoAtencion);
        }
    }
    
    public void getGruposPrioritarios(){
        
        List<Atenciongprioritario> listaGruposPrioritariosSelected = new ArrayList<>();
        Atenciongprioritario atenciongprioritario; 
        if(!amedCmbGrupoPrioritarioSelectedModel.isEmpty()){
            for (int i = 0; i < amedCmbGrupoPrioritarioSelectedModel.getSize(); i++) {
                atenciongprioritario = new Atenciongprioritario();
                atenciongprioritario.setCtgprioritarioId(amedCmbGrupoPrioritarioSelectedModel.get(i));
                listaGruposPrioritariosSelected.add(atenciongprioritario);
            }
        }
        variableSesion.setIsEmbarazada(false);
        if(!listaGruposPrioritariosSelected.isEmpty()) {
            listaGruposPrioritariosSelected.forEach(item -> {
                if(item.getCtgprioritarioId().getId() == Integer.parseInt(ComunEnum.CATALOGO_ID_EMBARAZADA.getDescripcion())) {
                    VariableSesion.getInstanciaVariablesSession().setIsEmbarazada(true);
                }
            });
        }
        VariableSesion.getInstanciaVariablesSession().setGruposPrioritarios(listaGruposPrioritariosSelected);
        
        if(amedCmbLugarAtencionModel.getSelectedItem() != null) {
            Intraextramural intraextramural = new Intraextramural();
            Detallecatalogo ct_tipoatencion = (amedCmbLugarAtencionModel.getSelectedItem().getDescripcion().equals(ESTABLECIMIENTO)) ? detallecatalogoService.findOneByDetalleId(INTRAMURAL) : detallecatalogoService.findOneByDetalleId(EXTRAMURAL);
            intraextramural.setCttipoatencionId(ct_tipoatencion);
            intraextramural.setCtlugaratencionId(amedCmbLugarAtencionModel.getSelectedItem());
            VariableSesion.getInstanciaVariablesSession().setIntraExtraMural(intraextramural);
        }
		VariableSesion.getInstanciaVariablesSession().setEdadEnAniosPersona((int) ChronoUnit.YEARS
				.between(VariableSesion.getInstanciaVariablesSession().getFechaNacimiento(), VariableSesion.getInstanciaVariablesSession().getFechaAtencion().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()));
		VariableSesion.getInstanciaVariablesSession().setEdadEnMesesPersona((int) ChronoUnit.MONTHS
				.between(VariableSesion.getInstanciaVariablesSession().getFechaNacimiento(), VariableSesion.getInstanciaVariablesSession().getFechaAtencion().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()));
		VariableSesion.getInstanciaVariablesSession().setEdadEnDiasPersona((int) ChronoUnit.DAYS
				.between(VariableSesion.getInstanciaVariablesSession().getFechaNacimiento(), VariableSesion.getInstanciaVariablesSession().getFechaAtencion().toInstant().atZone(ZoneId.systemDefault()).toLocalDate()));
		if(amedCmbEstrategiaModel.getSelectedItem() != null && amedCmbEstrategiaModel.getSelectedItem().getId() != 0){    
            Estrategiapaciente estrategiapaciente = new Estrategiapaciente();
            estrategiapaciente.setCtestrategiaId(amedCmbEstrategiaModel.getSelectedItem());
            estrategiapaciente.setFecharegistro(VariableSesion.getInstanciaVariablesSession().getFechaAtencion());
            VariableSesion.getInstanciaVariablesSession().setEstrategiapaciente(estrategiapaciente);
        }
    }
    
    public boolean validateForm() {
  
        boolean validarForma = false;
        if(amedCmbEstrategiaModel != null && amedCmbEstrategiaModel.getSelectedItem().getDescripcion().equals("Ninguno")){
        	validarForma = UtilitarioForma.validarCamposRequeridos(cmbLugarAtencion,lstGrupoPrioritarioSelected);
        }else{
        	validarForma = UtilitarioForma.validarCamposRequeridos(cmbLugarAtencion,lstGrupoPrioritarioSelected,cmbEstrategia);
        }
        		
        if(validarForma){
            getGruposPrioritarios();
        }
       return validarForma;
    }
    
    public void removeItemsJListGrupoPrioritario() {
        amedCmbGrupoPrioritarioSelectedModel.removeAllElements();
        VariableSesion.getInstanciaVariablesSession().setBanderaCambioSexoGrupoP(false);
    }
    
    public void loadDefaultCombos(){
        detallecatalogoNull = new Detallecatalogo(){
            private static final long serialVersionUID = 1L;
            boolean isNoSelectable = true;
        };
        detallecatalogoNull.setId(0);
        detallecatalogoNull.setDescripcion("Seleccione");
    }
    
    public void loadCombosNinguno(){
        dcNinguno = new Detallecatalogo(){
            private static final long serialVersionUID = 1L;
            boolean isNoSelectable = true;
        };
        dcNinguno.setId(0);
        dcNinguno.setDescripcion("Ninguno");
    }

    public void cleanInput() {
        Object[] inputs = { cmbLugarAtencion, lstGrupoPrioritarioSelected };
        UtilitarioForma.limpiarCampos(grupoPrioritarioFrame, inputs);
        loadCatalogoGrupoPrioritario();
        cmbLugarAtencion.setSelectedItem(detallecatalogoNull);
        cmbEstrategia.setSelectedItem(detallecatalogoNull);
        lstGrupoPrioritarioLoad.setEnabled(true);
    }
}
