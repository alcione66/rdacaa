/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.amed.model;

import ec.gob.msp.rdacaa.business.entity.Cie;
import ec.gob.msp.rdacaa.control.swing.model.DefaultComboBoxModel;
import org.springframework.stereotype.Component;

/**
 *
 * @author eduardo
 */
@Component
public class AmedCmbDescripcionCIEModel extends DefaultComboBoxModel<Cie>{
    
    private static final long serialVersionUID = -8610567823784470682L;
    
}
