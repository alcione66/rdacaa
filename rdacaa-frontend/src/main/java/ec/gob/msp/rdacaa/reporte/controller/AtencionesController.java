package ec.gob.msp.rdacaa.reporte.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;

import com.github.lgooddatepicker.components.DatePicker;

import ec.gob.msp.rdacaa.business.entity.Atenciongprioritario;
import ec.gob.msp.rdacaa.business.entity.Atencionmedica;
import ec.gob.msp.rdacaa.business.entity.Diagnosticoatencion;
import ec.gob.msp.rdacaa.business.entity.Entidad;
import ec.gob.msp.rdacaa.business.entity.Estrategiapaciente;
import ec.gob.msp.rdacaa.business.entity.Intraextramural;
import ec.gob.msp.rdacaa.business.entity.VariablesDatosAntropometricos;
import ec.gob.msp.rdacaa.business.entity.VariablesDatosObstetricos;
import ec.gob.msp.rdacaa.business.entity.VariablesExamenLaboratorio;
import ec.gob.msp.rdacaa.business.entity.VariablesGruposVulnerables;
import ec.gob.msp.rdacaa.business.entity.VariablesRefInt;
import ec.gob.msp.rdacaa.business.entity.VariablesVih;
import ec.gob.msp.rdacaa.business.entity.VariablesViolencia;
import ec.gob.msp.rdacaa.business.service.AtenciongprioritarioService;
import ec.gob.msp.rdacaa.business.service.AtencionmedicaService;
import ec.gob.msp.rdacaa.business.service.CievigilanciaService;
import ec.gob.msp.rdacaa.business.service.DiagnosticoatencionService;
import ec.gob.msp.rdacaa.business.service.EntidadService;
import ec.gob.msp.rdacaa.business.service.EstrategiapacienteService;
import ec.gob.msp.rdacaa.business.service.IntraextramuralService;
import ec.gob.msp.rdacaa.business.service.PersonaService;
import ec.gob.msp.rdacaa.business.service.VariablesDatosAntropometricosService;
import ec.gob.msp.rdacaa.business.service.VariablesDatosObstetricosService;
import ec.gob.msp.rdacaa.business.service.VariablesExamenLaboratorioService;
import ec.gob.msp.rdacaa.business.service.VariablesGruposVulnerablesService;
import ec.gob.msp.rdacaa.business.service.VariablesRefIntService;
import ec.gob.msp.rdacaa.business.service.VariablesVihService;
import ec.gob.msp.rdacaa.business.service.VariablesViolenciaService;
import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.reporte.forma.Atenciones;
import ec.gob.msp.rdacaa.reporte.model.AtencionesTablaModel;
import ec.gob.msp.rdacaa.reporte.model.RegistroAtencionesModel;
import ec.gob.msp.rdacaa.reporte.utilitario.ReportesUtil;
import ec.gob.msp.rdacaa.seguridades.util.SeguridadUtil;
import ec.gob.msp.rdacaa.utilitario.forma.FormatoTabla;
import javax.swing.JComboBox;

@Controller
@Lazy(value = false)
public class AtencionesController  {

	private static final Logger logger = LoggerFactory.getLogger(AtencionesController.class);
	private static final String CODIGOCIE = "CODIGO CIE 10";
	private static final String PREVENCION = "PREVENCION";
	private static final String MORBILIDAD = "MORBILIDAD";
	private static final String DIAGNOSTICO = "CONDICION DE DIAGNOSTICO";
	private Atenciones atencionesFrame;
	private AtencionmedicaService atencionmedicaService;
	private PersonaService personaService;
	private AtenciongprioritarioService atenciongprioritarioService;
	private DiagnosticoatencionService diagnosticoatencionService;
	private VariablesRefIntService variablesRefIntService;
	private VariablesDatosAntropometricosService variablesDatosAntropometricosService;
	private VariablesDatosObstetricosService variablesDatosObstetricosService;
	private VariablesViolenciaService variablesViolenciaService;
	private VariablesVihService variablesVihService;
	private VariablesGruposVulnerablesService variablesGruposVulnerablesService;
	private CievigilanciaService cievigilanciaService;
	private IntraextramuralService intraextramuralService;
	private EstrategiapacienteService estrategiapacienteService;
	private EntidadService entidadService;
	private VariablesExamenLaboratorioService variablesExamenLaboratorioService;
	private JButton btnBuscar;
	private JButton btnDescargar;
	private JButton btnImprimir;
	private JTable tblReporte;
	private DatePicker dttDesde;
	private DatePicker dttHasta;
	private List<RegistroAtencionesModel> controles;
	private AtencionesTablaModel modeloTabla;
	private JComboBox<String> cmbNotificacion;

	@Autowired
	public AtencionesController(Atenciones atencionesFrame, AtencionmedicaService atencionmedicaService,
			PersonaService personaService, AtenciongprioritarioService atenciongprioritarioService,
			DiagnosticoatencionService diagnosticoatencionService, VariablesRefIntService variablesRefIntService,
			VariablesDatosAntropometricosService variablesDatosAntropometricosService,
			VariablesDatosObstetricosService variablesDatosObstetricosService,
			VariablesViolenciaService variablesViolenciaService, VariablesVihService variablesVihService,
			VariablesGruposVulnerablesService variablesGruposVulnerablesService,
			CievigilanciaService cievigilanciaService,
			IntraextramuralService intraextramuralService,
			EstrategiapacienteService estrategiapacienteService,
			EntidadService entidadService,
			VariablesExamenLaboratorioService variablesExamenLaboratorioService
			) {
		this.atencionesFrame = atencionesFrame;
		this.atencionmedicaService = atencionmedicaService;
		this.personaService = personaService;
		this.atenciongprioritarioService = atenciongprioritarioService;
		this.diagnosticoatencionService = diagnosticoatencionService;
		this.variablesRefIntService = variablesRefIntService;
		this.variablesDatosAntropometricosService = variablesDatosAntropometricosService;
		this.variablesDatosObstetricosService = variablesDatosObstetricosService;
		this.variablesViolenciaService = variablesViolenciaService;
		this.variablesVihService = variablesVihService;
		this.variablesGruposVulnerablesService = variablesGruposVulnerablesService;
		this.cievigilanciaService = cievigilanciaService;
		this.intraextramuralService = intraextramuralService;
		this.estrategiapacienteService = estrategiapacienteService;
		this.entidadService = entidadService;
		this.variablesExamenLaboratorioService = variablesExamenLaboratorioService;
	}

	@PostConstruct
	private void init() {
		this.dttDesde = atencionesFrame.getDttDesde();
		this.dttHasta = atencionesFrame.getDttHasta();
		this.btnBuscar = atencionesFrame.getBtnBuscar();
		this.btnDescargar = atencionesFrame.getBtnDescargar();
		this.btnImprimir = atencionesFrame.getBtnImprimir();
		this.tblReporte = atencionesFrame.getTblReporte();
		this.cmbNotificacion = atencionesFrame.getCmbNotificacion();

		btnBuscar.addActionListener(e -> consultar());
		btnDescargar.addActionListener(e -> exportarXLS());
		btnImprimir.addActionListener(e -> exportarPDF());
		cargarTabla();
		atencionesFrame.addInternalFrameListener(new InternalFrameAdapter() {
			@Override
			public void internalFrameClosing(InternalFrameEvent e) {
				super.internalFrameClosing(e);
				cerrar();
			}
		});
	}

	private void consultar() {
		if (dttDesde.getDate() != null && dttHasta.getDate() != null) {
			ejecutarConsulta();
			cargarTabla();
		}
	}

	private void cargarTabla() {

		tblReporte.setDefaultRenderer(Object.class, new FormatoTabla());
		String [] columnNames = {"", "No", "ID REGISTRO", "NOMBRE Y APELLIDOS", "CEDULA", "SEXO", "ORIENTACION SEXUAL",
			"IDENTIDAD GENERO", "FECHA NACIMIENTO", "EDAD AÑOS", "EDAD MESES", "EDAD DÍAS" , "NACIONALIDAD", "APORTA",
			"CEDULA REPRESENTANTE",	"AUTOIDENTIDAD PACIENTE",	"PUEBLOS PACIENTE",	"PROVINCIA PACIENTE",	"CANTON PACIENTE",	
			"PARROQUIA PACIENTE",	"DIRECCION PACIENTE",	"TELEFONO CONVENCIONAL PACIENTE",	"TELEFONO FAMILIAR PACIENTE",	
			"TIPO DE BONO PACIENTE","GP 1", "GP 2", "GP 3","GP 4", "GP 5", CODIGOCIE, PREVENCION, MORBILIDAD, DIAGNOSTICO, CODIGOCIE, PREVENCION, MORBILIDAD,
			DIAGNOSTICO, CODIGOCIE, PREVENCION, MORBILIDAD, DIAGNOSTICO, CODIGOCIE, PREVENCION, MORBILIDAD,
			DIAGNOSTICO, CODIGOCIE, PREVENCION, MORBILIDAD, DIAGNOSTICO, "REFERENCIA - CONTRAREFERENCIA",
			"INTERCONSULTA SOLICITADA/RECIBIDA", "TALLA", "TALLA CORREGIDA", "PESO", "PERIMETRO CEFALICO", "IMC",
			"IMC CLASIFICACION", "TIPO TOMA TALLA", "PUNTAJE Z TALLA EDAD", "PUNTAJE Z PESO EDAD",
			"PUNTAJE Z PESO TALLA", "PUNTAJE Z IMC EDAD", "PUNTAJE Z PERIMETRO CEFALICO EDAD",
			"CATEGORIA TALLA EDAD", "CATEGORIA PESO EDAD", "CATEGORIA PESO TALLA", "CATEGORIA IMC EDAD",
			"CATEGORIA PERIMETRO CEFALICO EDAD", "HB", "HB CORREGIDO", "ANEMIA", "RIESGO OBSTETRICO", "PLAN PARTO",
			"PLAN TRANSPORTE", "EMBARAZO PLANIFICADO", "METODO DETERMINAR SEMANAS GESTACION", "FUM",
			"SEMANAS GESTACION","NO TREPONEMICA","TREPONEMICA","TRATAMIENTO","TTO PAREJA","BACTERIURIA",
			"FORMULARIO", "IDENTIFICA AGRESOR", "LESSIONES OCURRIDAS", "PARENTESCO",
			"MOTIVO PRUEBA", "PRIMERA PRUEBA", "PRIMERA PRUEBA REACTIVA", "SEGUNDA PRUEBA",
			"SEGUNDA PRUEBA REACTIVA", "VIA TRANSMISION", "CARGA VIRAL", "CD4", "GV1", "GV2", "GV3", "GV4", "GV5",
			"FECHA ATENCION","ESTRATEGIA","LUGAR ATENCION","COD ESTABLECIMIENTO","NOM ESTABLECIMIENTO","TIPO ESTABLECIMIENTO",
            "INSTITUCION DEL SISTEMA","NOMBRE PROFESIONAL","APELLIDO PROFESIONAL","NACIONALIDAD PROFESIONAL","SEXO PROFESIONAL",
			"AUTOIDENTIDAD PROFESIONAL","FECHA NACIMIENTO PROFESIONAL","ESPECIALIDAD PROFESIONAL"
		};

		modeloTabla = new AtencionesTablaModel(columnNames);
		controles = controles == null ? new ArrayList<>() : controles;
		modeloTabla.addFila(controles);
		tblReporte.setModel(modeloTabla);
		tblReporte.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		modeloTabla.ocultaColumnaCodigo(tblReporte);
		modeloTabla.definirAnchoColumnas(tblReporte, definirTamano(columnNames.length));

	}

	private void exportarXLS() {
		exportFile("xls");
	}
	
	private void exportarPDF() {
		exportFile("pdf");
	}

	private void exportFile(String dataType) {
		String autor = VariableSesion.getInstanciaVariablesSession().getUsuario().getPersonaId().getPrimernombre()
				+ " ";
		autor += VariableSesion.getInstanciaVariablesSession().getUsuario().getPersonaId().getPrimerapellido() + " ";
		
		StringBuilder fechas = new StringBuilder();
		fechas.append("DESDE: ").append(dttDesde.getDateStringOrEmptyString()).append(" HASTA: ")
				.append(dttHasta.getDateStringOrEmptyString());

		String tituloReporte = cmbNotificacion.getSelectedItem().toString();
		if (cmbNotificacion.getSelectedItem().equals("Todo")) {
			tituloReporte = "Reporte de Atenciones General";
		} else {
			tituloReporte = "Reporte de Atenciones de " + tituloReporte;
		}

		ReportesUtil.exportarXlsReportes(tblReporte, "Atenciones", autor, dataType, tituloReporte, fechas.toString());
	}



	private void ejecutarConsulta() {
		RegistroAtencionesModel registroAtencionesModel;
		controles = new ArrayList<>();
		List<Atencionmedica> atenciones = atencionmedicaService.findAllAtencionesByDates(
				VariableSesion.getInstanciaVariablesSession().getProfesional().getId(),
				Date.from(dttDesde.getDate().atStartOfDay(ZoneId.systemDefault()).toInstant()),
				Date.from(dttHasta.getDate().atStartOfDay(ZoneId.systemDefault()).toInstant()),
				VariableSesion.getInstanciaVariablesSession().getCtEspecialidad().getId()
				);
		if (atenciones != null && !atenciones.isEmpty()) {
			if (cmbNotificacion.getSelectedItem().equals("Notificación Obligatoria")) {
				List<Atencionmedica> atencionNot = new ArrayList<>();
				for (Atencionmedica atencion : atenciones) {
					List<Diagnosticoatencion> da = diagnosticoatencionService
							.findAllDiagnosticoByAtencionMedica(atencion.getId());
					if (!da.isEmpty()) {
						int notifica = 0;
						for (Diagnosticoatencion diagnosticoatencionItem : da) {
							if (diagnosticoatencionItem.getCtcondiciondiagnositcoId() != null && cievigilanciaService
									.findOneAlertaCieVigilancia(diagnosticoatencionItem.getCieId().getCodigo(),
											diagnosticoatencionItem.getCtprevenciondiagnosticoId().getId(),
											diagnosticoatencionItem.getCtcondiciondiagnositcoId().getId())) {
								notifica++;
							}
						}
						if(notifica > 0){
							atencionNot.add(atencion);
						}
					}
				}
				atenciones = atencionNot;
			}
			controles = new ArrayList<>();
			for (Atencionmedica atencion : atenciones) {
				registroAtencionesModel = new RegistroAtencionesModel();
				registroAtencionesModel.setAtencionMedica(atencion);
				String co = null;

				try {
					co = SeguridadUtil.descifrarString(atencion.getKsCident(),
							VariableSesion.getInstanciaVariablesSession().getUtilitario(),
							VariableSesion.getInstanciaVariablesSession().getUsuario().getLogin());
					registroAtencionesModel.setPersona(personaService.findOneByPersonaUUID(co));
				} catch (GeneralSecurityException e) {
					logger.error("Error GeneralSecurityException {} ", e.getMessage());
				} catch (IOException e) {
					logger.error("Error IOException {} ", e.getMessage());
				}

				List<Atenciongprioritario> gp = atenciongprioritarioService
						.findAllGrupoPrioritariosByAtencionMedica(atencion.getId());
				if (!gp.isEmpty()) {
					registroAtencionesModel.setGruposPrioritarios(gp);
				}
				List<Diagnosticoatencion> da = diagnosticoatencionService
						.findAllDiagnosticoByAtencionMedica(atencion.getId());
				if (!da.isEmpty()) {
					registroAtencionesModel.setDiagnostico(da);
				}
				VariablesRefInt vr = variablesRefIntService.findOneByAtencionId(atencion.getId());
				if (vr != null) {
					registroAtencionesModel.setVariablesRefInt(vr);
				}
				VariablesDatosAntropometricos vda = variablesDatosAntropometricosService
						.findOneByAtencionId(atencion.getId());
				if (vda != null) {
					registroAtencionesModel.setVariablesDatosAntropometricos(vda);
				}
				VariablesDatosObstetricos vdo = variablesDatosObstetricosService.findOneByAtencionId(atencion.getId());
				if (vdo != null) {
					registroAtencionesModel.setVariablesDatosObstetricos(vdo);
				}
				VariablesViolencia vv = variablesViolenciaService.findOneByAtencionId(atencion.getId());
				if (vv != null) {
					registroAtencionesModel.setVariablesViolencia(vv);
				}
				VariablesVih vVhi = variablesVihService.findOneByAtencionId(atencion.getId());
				if (vVhi != null) {
					registroAtencionesModel.setVariablesVih(vVhi);
				}
				VariablesGruposVulnerables vgv = variablesGruposVulnerablesService
						.findOneByAtencionId(atencion.getId());
				if (vgv != null) {
					registroAtencionesModel.setVariablesGruposVulnerables(vgv);
				}
				Intraextramural iv = intraextramuralService.findIntraextramuralByAtencionId(atencion.getId());
				if (iv != null) {
					registroAtencionesModel.setIntraextramural(iv);
				}
				Estrategiapaciente ev = estrategiapacienteService.findByAtencionId(atencion.getId());
				if (ev != null) {
					registroAtencionesModel.setEstrategiapaciente(ev);
				}
				Entidad entidad = entidadService.findOneByEntidadId(atencion.getEntidadId());
				if (entidad != null) {
					registroAtencionesModel.setEntidad(entidad);
				}
				VariablesExamenLaboratorio exl = variablesExamenLaboratorioService.findOneByAtencionId(atencion.getId());
				if (exl != null) {
					registroAtencionesModel.setVariablesExamenLaboratorio(exl);
				}
				controles.add(registroAtencionesModel);
			}
		}else{
			controles.clear();
		}
	}

	private void cerrar() {

		dttDesde.setText("");
		dttHasta.setText("");
		if (modeloTabla != null) {
			modeloTabla.clear();
		}

	}

	private int[] definirTamano(int tam) {
		int[] medida = new int[tam];
		for (int i = 0; i < medida.length; i++) {
			medida[i] = 150;
		}
		return medida;
	}
}
