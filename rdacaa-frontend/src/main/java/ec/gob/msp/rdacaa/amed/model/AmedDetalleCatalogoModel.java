/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.amed.model;

import ec.gob.msp.rdacaa.business.entity.Detallecatalogo;
import ec.gob.msp.rdacaa.control.swing.model.DefaultComboBoxModel;
import org.springframework.stereotype.Component;

/**
 *
 * @author eduardo
 */
@Component
public class AmedDetalleCatalogoModel extends DefaultComboBoxModel<Detallecatalogo>{
    
    private static final long serialVersionUID = 7842788967125952613L;
    
}
