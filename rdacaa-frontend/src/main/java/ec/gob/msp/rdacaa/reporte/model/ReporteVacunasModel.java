package ec.gob.msp.rdacaa.reporte.model;

import ec.gob.msp.rdacaa.business.entity.Atencionmedica;
import ec.gob.msp.rdacaa.business.entity.Entidad;
import ec.gob.msp.rdacaa.business.entity.Intraextramural;
import ec.gob.msp.rdacaa.business.entity.Persona;
import ec.gob.msp.rdacaa.business.entity.VariablesGruposPrioritarios;
import ec.gob.msp.rdacaa.business.entity.VariablesVacunas;
import lombok.Getter;
import lombok.Setter;

public class ReporteVacunasModel {
	
	@Getter
	@Setter
	private Atencionmedica atencionMedica;
	@Getter
	@Setter
	private Persona persona;
	@Getter
	@Setter
	private Persona profesional;
	@Getter
	@Setter
	private Entidad entidad;
	@Getter
	@Setter
	private VariablesVacunas variablesVacunas;
	@Getter
	@Setter
	private VariablesGruposPrioritarios variablesGruposPrioritarios;
	@Getter
	@Setter
	private Intraextramural intraextramural;

	
}
	
	
