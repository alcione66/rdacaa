/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.amed.controller;


import java.awt.Color;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.annotation.PostConstruct;
import javax.imageio.ImageIO;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.BorderFactory;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JLabel;
import javax.swing.JTable;
import javax.swing.JTextField;

import org.jdesktop.swingx.autocomplete.AutoCompleteDecorator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.github.lgooddatepicker.components.DatePicker;
import com.github.lgooddatepicker.components.DatePickerSettings;
import com.github.lgooddatepicker.optionalusertools.DateVetoPolicy;

import ec.gob.msp.rdacaa.admision.model.CmbDetalleCatalogoCellRenderer;
import ec.gob.msp.rdacaa.admision.model.DetalleCatalogoToStringConverter;
import ec.gob.msp.rdacaa.amed.forma.Vacunas;
import ec.gob.msp.rdacaa.amed.model.AmedCmbDosisVacunasModel;
import ec.gob.msp.rdacaa.amed.model.AmedCmbGriesgoxVacunaModel;
import ec.gob.msp.rdacaa.amed.model.AmedCmbVacunasModel;
import ec.gob.msp.rdacaa.amed.model.CmbEsquemaVacunacionCellRenderer;
import ec.gob.msp.rdacaa.amed.model.CmbVacunaCellRenderer;
import ec.gob.msp.rdacaa.amed.model.EsquemaVacunacionToStringConverter;
import ec.gob.msp.rdacaa.amed.model.VacunaToStringConverter;
import ec.gob.msp.rdacaa.amed.model.VacunasTablaModel;
import ec.gob.msp.rdacaa.business.entity.Detallecatalogo;
import ec.gob.msp.rdacaa.business.entity.Esquemavacunacion;
import ec.gob.msp.rdacaa.business.entity.Registrovacunacion;
import ec.gob.msp.rdacaa.business.entity.Vacuna;
import ec.gob.msp.rdacaa.business.service.DetallecatalogoService;
import ec.gob.msp.rdacaa.business.service.EsquemaVacunacionService;
import ec.gob.msp.rdacaa.business.service.GriesgoxvacunaService;
import ec.gob.msp.rdacaa.business.service.VacunaService;
import ec.gob.msp.rdacaa.business.service.VacunaXEsquema;
import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.utilitario.fecha.FechasUtil;
import ec.gob.msp.rdacaa.utilitario.forma.ButtonColumn;
import ec.gob.msp.rdacaa.utilitario.forma.FormatoTabla;
import ec.gob.msp.rdacaa.utilitario.forma.UtilitarioForma;

/**
 *
 * @author eduardo
 */
@Controller
public class VacunasController {

    private final Vacunas vacunasFrame;
    private final VacunaService vacunaService;
    private final EsquemaVacunacionService esquemaVacunacionService;
    private final GriesgoxvacunaService griesgoxvacunaService;
    private DetallecatalogoService detallecatalogoService;
    private final AmedCmbVacunasModel amedCmbVacunasModel;
    private final AmedCmbDosisVacunasModel amedCmbDosisVacunasModel;
    private final VariableSesion variableSesion = VariableSesion.getInstanciaVariablesSession();
    private JComboBox<VacunaXEsquema> cmbVacunas;
    private JComboBox<Esquemavacunacion> cmbDosis;
    private JButton btnAgregar;
    private VacunasTablaModel modeloTabla;
    private JTable tblVacunas;
    private final List<Registrovacunacion> listRegistrovacunacion = new ArrayList<>();
    private final Object[] controles = new Object[4];
    private JTextField txtLote;
    private Vacuna vacunaNull;
    private Esquemavacunacion esquemavacunacionNull;
    private DatePicker dttFechaRegistro;
    private VacunaXEsquema vacunaXEsquemaNull;
    private JCheckBox chk_habilitarfechaaplicacion;
    private JLabel alerta_vacuna;
    private final AmedCmbGriesgoxVacunaModel amedCmbGriesgoxVacunaModel;
    private JComboBox<Detallecatalogo> cmbGrupoRiesgoXVacuna;
    private Detallecatalogo detallecatalogoNull;


    @Autowired
    public VacunasController(
        Vacunas vacunasFrame,
        VacunaService vacunaService,
        GriesgoxvacunaService griesgoxvacunaService,
        EsquemaVacunacionService esquemaVacunacionService,
        DetallecatalogoService detallecatalogoService
    ) 
    {
        this.vacunasFrame = vacunasFrame;
        this.vacunaService = vacunaService;
        this.griesgoxvacunaService = griesgoxvacunaService;
        this.esquemaVacunacionService = esquemaVacunacionService;
        this.detallecatalogoService = detallecatalogoService;
        this.amedCmbVacunasModel = new AmedCmbVacunasModel();
        this.amedCmbDosisVacunasModel = new AmedCmbDosisVacunasModel();
        this.amedCmbGriesgoxVacunaModel = new AmedCmbGriesgoxVacunaModel();
    }
    
    @PostConstruct
    private void init() {
        loadDefaultCombos();
        this.cmbVacunas = vacunasFrame.getCmbVacunas(); 
        this.cmbDosis = vacunasFrame.getCmbDosis();
        this.btnAgregar = vacunasFrame.getBtnAgregar();
        this.tblVacunas = vacunasFrame.getTblVacunas();
        this.txtLote = vacunasFrame.getTxtLote();
        this.dttFechaRegistro = vacunasFrame.getDttFechaRegistro();
        this.dttFechaRegistro.setEnabled(false);
        this.chk_habilitarfechaaplicacion = vacunasFrame.getChk_habilitarfechaaplicacion();
        this.alerta_vacuna = vacunasFrame.getAlerta_vacuna();
        cmbVacunas.setRenderer(new CmbVacunaCellRenderer());
        AutoCompleteDecorator.decorate(cmbVacunas, new VacunaToStringConverter());
        AutoCompleteDecorator.decorate(cmbDosis, new EsquemaVacunacionToStringConverter());
        loadVacunas();
        cmbVacunas.addActionListener(e -> loadEsquemaDosis());
        this.cmbGrupoRiesgoXVacuna = vacunasFrame.getCmbGrupoRiesgoXVacuna();
        this.cmbGrupoRiesgoXVacuna.setRenderer(new CmbDetalleCatalogoCellRenderer());
        AutoCompleteDecorator.decorate(cmbGrupoRiesgoXVacuna, new DetalleCatalogoToStringConverter());
        
        
        controles[0] = vacunasFrame.getCmbVacunas();
        controles[1] = vacunasFrame.getCmbDosis();
        controles[2] = vacunasFrame.getDttFechaRegistro();
        controles[3] = null;
       
        cargarTabla();
        
        btnAgregar.addMouseListener (new MouseListener() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if(validateForm()) {
                    Esquemavacunacion esquemavacunacion = esquemaVacunacionService.verifyApplicationVacunaByEdadAndEsquemaId(variableSesion.getDiasPersona(), variableSesion.getMesesPersona(), variableSesion.getAniosPersona(), amedCmbDosisVacunasModel.getSelectedItem().getId());
                    if(esquemavacunacion != null) {
                        if (UtilitarioForma.validarCamposRequeridos(controles)) {
                            getVacunas();
                        }
                    } else {
                        UtilitarioForma.muestraMensaje("", vacunasFrame, "ERROR", "Estimado usuario la vacuna no se puede aplicar, por favor verificar.");
                    }
                }
            }
            @Override
            public void mousePressed(MouseEvent e) {
            }
            @Override
            public void mouseReleased(MouseEvent e) {
            }
            @Override
            public void mouseEntered(MouseEvent e) {
            }
            @Override
            public void mouseExited(MouseEvent e) {
            }
        });
        
        cmbVacunas.addItemListener((ItemEvent e) -> {
            if(e.getStateChange() == ItemEvent.SELECTED) {
                loadGrupoRiesgo(((VacunaXEsquema)e.getItem()).getIdVacuna());
            }
        });
        
        dttFechaRegistro.addDateChangeListener(dtt -> validFechaAplicacionVacuna());
        
        txtLote.addKeyListener(new java.awt.event.KeyAdapter() {
            @Override
            public void keyTyped(java.awt.event.KeyEvent evt) {
                UtilitarioForma.validarLongitudCampo(evt, 20);
                UtilitarioForma.textoSinCaracteresEspeciales(evt);
            }
        });
        
        chk_habilitarfechaaplicacion.addItemListener((ItemEvent e) -> {
            if(e.getStateChange() == ItemEvent.SELECTED) {
                this.dttFechaRegistro.setEnabled(true);
            } else {
                this.dttFechaRegistro.setEnabled(false);
                this.dttFechaRegistro.setDate(variableSesion.getFechaAtencion().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
            }
        });
        
    }

        
    public void loadVacunas() {
        List<VacunaXEsquema> listaVacunas = new ArrayList<>();
        List<VacunaXEsquema> listVacunaByEdad = new ArrayList<>();
        if(amedCmbVacunasModel.getSelectedItem() != null) {
            if(amedCmbVacunasModel.getSelectedItem().equals(vacunaXEsquemaNull)) {
                if(variableSesion.getPersona() != null) {
                    listVacunaByEdad = esquemaVacunacionService.findBySearchEdad(variableSesion.getDiasPersona(), variableSesion.getMesesPersona(), variableSesion.getAniosPersona());
                    listVacunaByEdad.forEach((VacunaXEsquema vacxesq) -> {
                        int hombre = variableSesion.getIsHombre() ? 1 : 0;
                        int mujer = variableSesion.getIsMujer() ? 1 : 0;
                        int intersexual = variableSesion.getIsInterSexual() ? 1 : 0;
                        if(hombre == 1) {
                            if(vacxesq.getVacuna().getHombre().equals(hombre)) {
                                listaVacunas.add(vacxesq);
                            }
                        }
                        if(mujer == 1) {
                            if(vacxesq.getVacuna().getMujer().equals(mujer)) {
                                listaVacunas.add(vacxesq);
                            }
                        }
                        if(intersexual == 1) {
                            if(vacxesq.getVacuna().getIntersexual().equals(intersexual)) {
                                listaVacunas.add(vacxesq);
                            }
                        }
                    });
                }
                
            }
        }
          
        amedCmbVacunasModel.clear();
        amedCmbVacunasModel.addElements(listaVacunas);

        cmbVacunas.setModel(amedCmbVacunasModel);
        cmbVacunas.setSelectedItem(vacunaXEsquemaNull); 
        
        if(variableSesion.getCambioInformacionVacunas() != 0 ) {
        	variableSesion.setCambioInformacionVacunas(0);
        	if(variableSesion.getRegistrovacunacion() != null) {
        		variableSesion.getRegistrovacunacion().clear();
            	modeloTabla.clear();
        	}
        }
        
    }
    
    private void loadEsquemaDosis() {  
        List<Esquemavacunacion> listaEsquemaDosis = new ArrayList<>();
        if(amedCmbVacunasModel.getSelectedItem() != null && !amedCmbVacunasModel.getSelectedItem().equals(vacunaXEsquemaNull)) {
            if(variableSesion.getPersona() != null) {
                alerta_vacuna.setVisible(true);
                alerta_vacuna.setText(createAlertVacuna());
                alerta_vacuna.setBorder(BorderFactory.createLineBorder(Color.RED, 1));
                listaEsquemaDosis = esquemaVacunacionService.findBySearchVacunaAndEdad(variableSesion.getDiasPersona(), variableSesion.getMesesPersona(), variableSesion.getAniosPersona(), amedCmbVacunasModel.getSelectedItem().getVacuna(), amedCmbVacunasModel.getSelectedItem().getEsquema().getId());
            }
        } else {
            alerta_vacuna.setVisible(false);
        }
        
        amedCmbDosisVacunasModel.clear();
        amedCmbDosisVacunasModel.addElements(listaEsquemaDosis);
        cmbDosis.setRenderer(new CmbEsquemaVacunacionCellRenderer());
        cmbDosis.setModel(amedCmbDosisVacunasModel);
        cmbDosis.setSelectedItem(esquemavacunacionNull);
        if(variableSesion.getFechaAtencion() != null) {
        	dttFechaRegistro.setDate(variableSesion.getFechaAtencion().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
        }
    }
    
    private void loadGrupoRiesgo(Integer vacunaId) {   
        
    	if(variableSesion.getPersona() != null) {
            List<Detallecatalogo> grupoRiesgoXVacuna = 
            		griesgoxvacunaService.findAllGriesgoxVacuna(vacunaId, 
            				variableSesion.getIsHombre(), variableSesion.getIsMujer(), variableSesion.getIsInterSexual() );
            amedCmbGriesgoxVacunaModel.clear();
            amedCmbGriesgoxVacunaModel.addElements(grupoRiesgoXVacuna);
            cmbGrupoRiesgoXVacuna.setModel(amedCmbGriesgoxVacunaModel);
            cmbGrupoRiesgoXVacuna.setSelectedItem(detallecatalogoNull);
            if (grupoRiesgoXVacuna.isEmpty()){
                cmbGrupoRiesgoXVacuna.setEnabled(false);    
                controles[3] = null;
            }else {
                cmbGrupoRiesgoXVacuna.setEnabled(true);    
                controles[3] = vacunasFrame.getCmbGrupoRiesgoXVacuna();
            }
    	}
    }
    
    public String createAlertVacuna() {
        return "<html>"
                + "<body>"
                + "&nbsp; <span style=\"color: #FF0000;\">¡Estimado usuario favor verificar que la vacuna seleccionada es la que debe recibir el paciente!</span>"//+ amedCmbVacunasModel.getSelectedItem().getVacuna().getNombrevacuna()
                + "<body>"
                + "<html>";
    }
    
    public void validFechaAplicacionVacuna() {
        if(variableSesion.getPersona() != null) {
            if(!dttFechaRegistro.getText().equals("")) {
                Integer anios = FechasUtil.getDiffDatesDesdeHasta(Date.from(variableSesion.getFechaNacimiento().atStartOfDay(ZoneId.systemDefault()).toInstant()),Date.from(dttFechaRegistro.getDate().atStartOfDay(ZoneId.systemDefault()).toInstant()),0);
                Integer mes = FechasUtil.getDiffDatesDesdeHasta(Date.from(variableSesion.getFechaNacimiento().atStartOfDay(ZoneId.systemDefault()).toInstant()),Date.from(dttFechaRegistro.getDate().atStartOfDay(ZoneId.systemDefault()).toInstant()),1);
                Integer dia = FechasUtil.getDiffDatesDesdeHasta(Date.from(variableSesion.getFechaNacimiento().atStartOfDay(ZoneId.systemDefault()).toInstant()),Date.from(dttFechaRegistro.getDate().atStartOfDay(ZoneId.systemDefault()).toInstant()),2);
                if(anios < 0 || mes < 0 || dia < 0) {
                    UtilitarioForma.muestraMensaje("", vacunasFrame, "ERROR", "Estimado usuario la fecha de aplicación de la vacuna es menor a la edad actual del paciente.");
                    dttFechaRegistro.setText("");
                }
            }
        }
    }
    
    public void setDatePickerVetoPolicyVacuna() {
    	LocalDate today = LocalDate.now();
    	if(variableSesion.getPersona() != null) {
    		LocalDate fechaAtencion = variableSesion.getFechaAtencion().toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			DatePickerSettings dateSettings = dttFechaRegistro.getSettings();
			dateSettings.setVetoPolicy(new DateVetoPolicy() {
				@Override
				public boolean isDateAllowed(LocalDate date) {
					if(date.getYear() >= 2019) {
						if(date.isAfter(today) || date.isBefore(today.minusDays(90)) || date.isAfter(fechaAtencion)){
							return false; 
						} 
						return true;
					}else {
						return false;
					}
					
				}
			});
    	}
	}
    
    private void cargarTabla() {
        tblVacunas.setDefaultRenderer(Object.class, new FormatoTabla());
        String[] columns = {"Id", "Vacuna", "Dosis", "Lote", "Grupo Riesgo", "Fecha" , "Eliminar"};
        modeloTabla = new VacunasTablaModel(columns);
        modeloTabla.addFila(listRegistrovacunacion.isEmpty() ? new ArrayList() : listRegistrovacunacion);
        tblVacunas.setModel(modeloTabla);
        if(!listRegistrovacunacion.isEmpty()) {
            loadButtonDelete(tblVacunas, 6);
        }
        modeloTabla.ocultaColumnaCodigo(tblVacunas);
        modeloTabla.definirAnchoColumnas(tblVacunas ,new int[] { 30,30,30,90,60,60 });   
    }
    
    public void getVacunas() {
        Registrovacunacion registrovacunacion = new Registrovacunacion();
        registrovacunacion.setEsquemavacunacionId(amedCmbDosisVacunasModel.getSelectedItem());
        registrovacunacion.setValor(0);
        registrovacunacion.setFecharegistro(Date.from(dttFechaRegistro.getDate().atStartOfDay(ZoneId.systemDefault()).toInstant()));
        if (amedCmbGriesgoxVacunaModel.getSize() != 0){
        registrovacunacion.setCtgruporiesgoId(amedCmbGriesgoxVacunaModel.getSelectedItem()); //825)
        }else{
             registrovacunacion.setCtgruporiesgoId(null);
        }
            
        if (txtLote.getText().equals("")) {
            registrovacunacion.setLote("");
        } else {
            registrovacunacion.setLote(txtLote.getText());
        }
        if(listRegistrovacunacion.isEmpty()) {
            listRegistrovacunacion.add(registrovacunacion);
        } else {
        	if (listRegistrovacunacion.size() == 5) {
				UtilitarioForma.muestraMensaje("", vacunasFrame, "ERROR",
						"Estimado usuario le recordamos que puede ingresar un máximo de 5 vacunas, por favor verificar.");
			} else {
	            Optional<Registrovacunacion> vacuna = listRegistrovacunacion.stream().filter(vac -> vac.getEsquemavacunacionId().getVacunaId().getId().equals(registrovacunacion.getEsquemavacunacionId().getVacunaId().getId())).findFirst();
	            Optional<Registrovacunacion> vacunaDosis = listRegistrovacunacion.stream().filter(vac -> vac.getEsquemavacunacionId().getDosis().equals(registrovacunacion.getEsquemavacunacionId().getDosis())).findFirst();
	            if(vacuna.isPresent() && vacunaDosis.isPresent()) {
	                UtilitarioForma.muestraMensaje("", vacunasFrame, "ERROR", "Estimado usuario la vacuna y la dosis ya han sido agregadas, por favor verificar.");
	            } else {
	                listRegistrovacunacion.add(registrovacunacion);
	            }
			}
        }
        variableSesion.setRegistrovacunacion(listRegistrovacunacion);
        cleanInput(null);
        cargarTabla();
    }
    
    public boolean validateForm(){
        boolean validarForma = UtilitarioForma.validarCamposRequeridos(controles);      
        return validarForma;
    }
    
    public void loadDefaultCombos(){
        vacunaNull = new Vacuna(){
            private static final long serialVersionUID = 1L;
            boolean isNoSelectable = true;
        };
        
        esquemavacunacionNull = new Esquemavacunacion() {
            private static final long serialVersionUID = 1L;
            boolean isNoSelectable = true;
        };
        
        vacunaXEsquemaNull = new VacunaXEsquema() {
        	 private static final long serialVersionUID = 1L;
             boolean isNoSelectable = true;
        };
        
        detallecatalogoNull = new  Detallecatalogo(){
            private static final long serialVersionUID = 1L;
            boolean isNoSelectable = true;
        };
        
        vacunaNull.setId(0);
        vacunaNull.setNombrevacuna("Seleccione");
        
        esquemavacunacionNull.setId(0);
        esquemavacunacionNull.setDosis("Seleccione");
        
        vacunaXEsquemaNull.setIdVacuna(0);
        vacunaXEsquemaNull.setNombreVacunaXNombreEsquema("Seleccione");
        
        detallecatalogoNull.setId(0);
        detallecatalogoNull.setDescripcion("Seleccione");
    }
    
    private void loadButtonDelete(JTable table, int column) {
        try {
            ButtonColumn buttonColumn = new ButtonColumn(table, createActionExport(), column);
            Image img = ImageIO.read(getClass().getResource("/principal/grupoprioritario.jpg"));
            buttonColumn.getTableCellRendererComponent(table, new ImageIcon(img), false,true, column, column);
        } catch (IOException ex) {
            Logger.getLogger(DiagnosticoCIEController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private Action createActionExport() {
        return new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                listRegistrovacunacion.remove(tblVacunas.getSelectedRow());
                cargarTabla();
            }
        };
    }

    public void cleanInput(String panel) {
        if(panel != null && panel.equals("commons")) {
            listRegistrovacunacion.clear();
            cargarTabla();
        }
        
        cmbVacunas.setSelectedItem(vacunaXEsquemaNull);
        cmbDosis.setSelectedItem(esquemavacunacionNull);
        cmbGrupoRiesgoXVacuna.setSelectedItem(detallecatalogoNull);
        dttFechaRegistro.setText("");
        dttFechaRegistro.setEnabled(false);
        if(variableSesion.getPersona() != null && variableSesion.getFechaAtencion() != null) {
        	dttFechaRegistro.setDate(variableSesion.getFechaAtencion().toInstant().atZone(ZoneId.systemDefault()).toLocalDate());
        }
        chk_habilitarfechaaplicacion.setSelected(false);
        txtLote.setText("");
    }
}
