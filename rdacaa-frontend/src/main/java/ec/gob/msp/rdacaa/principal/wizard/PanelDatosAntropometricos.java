/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.principal.wizard;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.github.cjwizard.WizardSettings;

import ec.gob.msp.rdacaa.amed.controller.DatosAntropometricosController;
import ec.gob.msp.rdacaa.amed.forma.DatosAntropometricos;
import ec.gob.msp.rdacaa.principal.controller.CommonPanelController;

/**
 *
 * @author miguel.faubla
 */
@Component
public class PanelDatosAntropometricos extends com.github.cjwizard.WizardPage {
    
    private final DatosAntropometricos datosAntropometricos;
    private final DatosAntropometricosController datosAntropometricosController;
    private final CommonPanelController commonPanelController;

    @Autowired
    public PanelDatosAntropometricos(DatosAntropometricos datosAntropometricos, DatosAntropometricosController datosAntropometricosController, CommonPanelController commonPanelController) {
        super("Registro de Datos Antropométricos y Bioquímicos", "Atención Médica");
        this.datosAntropometricos = datosAntropometricos;
        this.datosAntropometricosController = datosAntropometricosController;
        this.commonPanelController = commonPanelController;
    }
    
    @PostConstruct
    private void init() {
        datosAntropometricos.setVisible(true);
        add(datosAntropometricos);
    }
    
    @Override
    public boolean onNext(WizardSettings settings) {
        return datosAntropometricosController.validateForm();
    }
    
    @Override
    public void updateSettings(WizardSettings settings) {
         commonPanelController.refreshPanels();
    }
    
}
