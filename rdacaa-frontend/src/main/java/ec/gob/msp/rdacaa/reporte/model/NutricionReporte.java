package ec.gob.msp.rdacaa.reporte.model;


import ec.gob.msp.rdacaa.business.entity.Atencionmedica;
import ec.gob.msp.rdacaa.business.entity.Entidad;
import ec.gob.msp.rdacaa.business.entity.Intraextramural;
import ec.gob.msp.rdacaa.business.entity.Persona;
import ec.gob.msp.rdacaa.business.entity.VariablesDatosAntropometricos;
import ec.gob.msp.rdacaa.business.entity.VariablesPrescripcion;
import ec.gob.msp.rdacaa.business.entity.VariablesSivanCuestionario;
import lombok.Getter;
import lombok.Setter;

public class NutricionReporte {

	@Getter
	@Setter
	private Entidad entidad;
	@Getter
	@Setter
	private Persona profesional;
	@Getter
	@Setter
	private Atencionmedica atencionmedica; 
	@Getter
	@Setter
	private Persona paciente;
	@Getter
	@Setter
	private VariablesDatosAntropometricos da;
	@Getter
	@Setter
	private VariablesSivanCuestionario sc;
	@Getter
	@Setter
	private VariablesPrescripcion vp;
        @Getter
	@Setter
	private Intraextramural lugarAtencion;
	
}
