/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.amed.controller;

import java.awt.event.ActionEvent;
import java.io.File;
import java.io.IOException;
import java.nio.file.FileSystems;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Month;
import java.time.Year;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.stream.IntStream;

import javax.annotation.PostConstruct;
import javax.swing.AbstractAction;
import javax.swing.Action;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JTable;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.google.common.collect.Lists;
import com.google.common.io.Files;

import ec.gob.msp.rdacaa.amed.forma.CierreMensual;
import ec.gob.msp.rdacaa.amed.model.CierreAtencionTablaModel;
import ec.gob.msp.rdacaa.amed.model.ValidacionCmbAnioModel;
import ec.gob.msp.rdacaa.amed.model.ValidacionCmbMesModel;
import ec.gob.msp.rdacaa.amed.model.ValidationCmbMesCellRenderer;
import ec.gob.msp.rdacaa.business.entity.Cierreatenciones;
import ec.gob.msp.rdacaa.business.service.CierreatencionesService;
import ec.gob.msp.rdacaa.control.swing.personalizado.ComboIess;
import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.principal.forma.EscritorioPrincipal;
import ec.gob.msp.rdacaa.utilitario.exportdatabase.ExportSQLiteDatabase;
import ec.gob.msp.rdacaa.utilitario.forma.ButtonColumn;
import ec.gob.msp.rdacaa.utilitario.forma.FormatoTabla;
import ec.gob.msp.rdacaa.utilitario.forma.UtilitarioForma;

/**
 *
 * @author dmurillo
 */
@Controller
public class CierreMensualController {

	private static final Logger logger = LoggerFactory.getLogger(CierreMensualController.class);

	private CierreMensual cierreMensualFrame;
	private CierreatencionesService cierreatencionesService;
	private ExportSQLiteDatabase exportSQLiteDatabase;
	private ComboIess cmbAnios;
    private ComboIess cmbMes;
	private JTable tblMeses;
	private JButton btnCierre;
	private JButton btnCancelar;

	private List<Cierreatenciones> controles;

	@Autowired
	public CierreMensualController(CierreMensual cierreMensualFrame, CierreatencionesService cierreatencionesService,
			ExportSQLiteDatabase exportSQLiteDatabase) {
		this.cierreMensualFrame = cierreMensualFrame;
		this.cierreatencionesService = cierreatencionesService;
		this.exportSQLiteDatabase = exportSQLiteDatabase;
	}

	@PostConstruct
	private void init() {
		this.tblMeses = cierreMensualFrame.getTblMeses();
		this.btnCierre = cierreMensualFrame.getBtnCierre();
		this.btnCancelar = cierreMensualFrame.getBtnCancelar();
        this.cmbAnios = cierreMensualFrame.getCmbAnios();
        this.cmbMes = cierreMensualFrame.getCmbMes();

		loadMeses();
		cargarTabla();
                initComboAnios(cmbAnios);
		initComboMes(cmbMes);
                
		btnCierre.addActionListener(l -> processCierre());
		btnCancelar.addActionListener(l -> cancelCierre());

	}

	public void loadMeses() {
		if (VariableSesion.getInstanciaVariablesSession().getUsuario() != null) {
			controles = cierreatencionesService
					.findAllByUsuarioId(VariableSesion.getInstanciaVariablesSession().getUsuario().getId());
			cargarTabla();
		}
	}

	private void cargarTabla() {

		tblMeses.setDefaultRenderer(Object.class, new FormatoTabla());
		String[] columnNames = { "id", "Anio", "Mes", "Estado", "Exportar" };

		CierreAtencionTablaModel modeloTabla = new CierreAtencionTablaModel(columnNames);
		controles = controles == null ? new ArrayList<>() : controles;
		modeloTabla.addFila(controles);

		tblMeses.setModel(modeloTabla);

		IntStream.range(0, controles.size()).forEach(idx -> {
			Cierreatenciones cierre = controles.get(idx);
			if (cierre.getEstadocierre().equals(0)) {
				cargarBotonesExport(tblMeses, 4);
			}

		});
		modeloTabla.ocultaColumnaCodigo(tblMeses);

	}

	private void cargarBotonesExport(JTable table, int column) {
		new ButtonColumn(table, createActionExport(), column);
	}

	private Action createActionExport() {
		return new AbstractAction() {
			private static final long serialVersionUID = 3331165511639553287L;

			public void actionPerformed(ActionEvent e) {
				JTable table = (JTable) e.getSource();
				int modelRow = Integer.parseInt((e.getActionCommand()));
				if (table.getModel().getValueAt(modelRow, 3).equals("CERRADO")) {

					Cierreatenciones cierre = (Cierreatenciones) table.getModel().getValueAt(modelRow, 0);
					String anio = cierre.getAnio();
					String mes = cierre.getMes().toString();
					JFileChooser c = new JFileChooser();
					int rVal = c.showSaveDialog(tblMeses);
					export(c, rVal, anio, mes);
				}
			}

			private void export(JFileChooser c, int rVal, String anio, String mes) {
				String filename;
				String dir;
				if (rVal == JFileChooser.APPROVE_OPTION) {
					filename = c.getSelectedFile().getName();
					if (!Files.getFileExtension(filename).equalsIgnoreCase("rdacaa")) {
						filename = Files.getNameWithoutExtension(filename)+".rdacaa";
					}
					dir = c.getCurrentDirectory().toString();
					String fileExportString = dir + FileSystems.getDefault().getSeparator() + filename;
					File fileExport = new File(fileExportString);
					boolean confirmExport = true;
					if (fileExport.exists()) {
						int dialogResult = JOptionPane.showConfirmDialog(tblMeses, "¿Desea sobreescribir el archivo?",
								"Advertencia", JOptionPane.YES_NO_OPTION);
						if (dialogResult == JOptionPane.YES_OPTION) {
							confirmExport = true;
						}else {
							confirmExport = false;
						}
					} 
										
					if(confirmExport){
						processExport(fileExportString, VariableSesion.getInstanciaVariablesSession().getProfesional().getIdpras().toString(),
								anio, mes);
					}
				}
			}
		};
	}

	private void processExport(String pathSalida, String idProfesional, String anio, String mes) {
		try {
			exportSQLiteDatabase.processExport(pathSalida, idProfesional, anio, StringUtils.leftPad(mes, 2, "0") , anio, StringUtils.leftPad(mes, 2, "0"));
		} catch (IOException e) {
			String msg = "Error exportando archivo";
			JOptionPane.showMessageDialog(tblMeses, msg, "Error de exportación", JOptionPane.ERROR_MESSAGE);
			logger.error(msg, e);
		}
	}

	private boolean isCierreValido() {
		Date fechaFinMesCombo = getFechaFinCombo();
		Date fechaFinMesFromToday = getFechaFinMesFromToday();
		return fechaFinMesCombo != null && fechaFinMesFromToday != null  
				&& ( fechaFinMesCombo.before(fechaFinMesFromToday) 
				|| fechaFinMesCombo.equals(fechaFinMesFromToday));
	}
	
	private void processCierre() {
		if(isCierreValido()) {
			if (UtilitarioForma.confirmacion(cierreMensualFrame, "Desea cerrar de mes..?") &&
	        		UtilitarioForma.confirmacion(cierreMensualFrame, "Confirmar el cierre..?")) {
	        	if(cierreatencionesService.findOneByUsuarioAnioMesCerrado(VariableSesion.getInstanciaVariablesSession().getUsuario().getId(), cmbAnios.getSelectedItem().toString() , cmbMes.getSelectedIndex() + 1)){
	        		JOptionPane.showMessageDialog(tblMeses, "El registro ingresado ya se encuentra cerrado por favor verifique", "Error Cierre", JOptionPane.ERROR_MESSAGE);
	        	}else{
	        		Cierreatenciones ca = cierreatencionesService.findOneByUsuarioAnioMesActivo(VariableSesion.getInstanciaVariablesSession().getUsuario().getId(), cmbAnios.getSelectedItem().toString() , cmbMes.getSelectedIndex() + 1);
	        			if(ca == null){
	            			ca = new Cierreatenciones();
	    	        		ca.setAnio(cmbAnios.getSelectedItem().toString());
	    	        		ca.setEstadocierre(0);
	    	        		ca.setMes(cmbMes.getSelectedIndex() + 1);
	    	        		ca.setUsuarioId(VariableSesion.getInstanciaVariablesSession().getUsuario());
	            		}else{
	            			ca.setEstadocierre(0);
	            		}
	        			cierreatencionesService.saveAndFlush(ca);
	        	}
	            loadMeses();
	        }
		}else {
        	String msg = "No se puede cerrar un mes posterior al actual";
			JOptionPane.showMessageDialog(tblMeses, msg, "Mensaje informativo", JOptionPane.INFORMATION_MESSAGE);
        }
  	}

	private void cancelCierre() {
            cierreMensualFrame.dispose();
            EscritorioPrincipal.dsp_contenedorRDACAA.remove(cierreMensualFrame);
	}
        
        @SuppressWarnings("unchecked")
	private void initComboAnios(ComboIess cmbAnios) {
		Year anioActual = Year.now();
		Year anioAnterior = anioActual.minusYears(1);
		List<Year> listaAnios = Lists.newArrayList(anioActual, anioAnterior);
		ValidacionCmbAnioModel validacionCmbAnioModel = new ValidacionCmbAnioModel();
		validacionCmbAnioModel.addElements(listaAnios);
		cmbAnios.setModel(validacionCmbAnioModel);
	}

	@SuppressWarnings("unchecked")
	private void initComboMes(ComboIess cmbMes) {
		Month[] meses = Month.values();
		List<Month> listaMeses = Arrays.asList(meses);
		ValidacionCmbMesModel validacionCmbMesModel = new ValidacionCmbMesModel();
		validacionCmbMesModel.addElements(listaMeses);
		cmbMes.setModel(validacionCmbMesModel);
		cmbMes.setRenderer(new ValidationCmbMesCellRenderer());
	}
	
	private Date getFechaInicioCombo() {
		String fechaAniosMes = cmbAnios.getSelectedItem().toString() + "-"
				+ StringUtils.leftPad(String.valueOf(((Month) cmbMes.getSelectedItem()).getValue()), 2, "0") + "-01";
		DateFormat format = new SimpleDateFormat("yyyy-MM-dd");
		try {
			return format.parse(fechaAniosMes);
		} catch (ParseException e) {
			return null;
		}
	}
	
	private Date getFechaFinCombo() {
		Calendar fechaFinCal = Calendar.getInstance();
		fechaFinCal.setTime(getFechaInicioCombo());
		fechaFinCal.add(Calendar.MONTH, 1);
		fechaFinCal.add(Calendar.DAY_OF_YEAR, -1);
		return fechaFinCal.getTime();
	}
	
	private Date getFechaFinMesFromToday() {
		DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");

		Calendar cal = Calendar.getInstance();
		String fechaAniosMes = cal.get(Calendar.YEAR) + "-"
				+ StringUtils.leftPad(String.valueOf(cal.get(Calendar.MONTH)+1), 2, "0") + "-01";
		Date finMes;
		try {
			finMes = formatter.parse(fechaAniosMes);
		} catch (ParseException e) {
			return null;
		}

		cal.setTime(finMes);
		cal.add(Calendar.MONTH, 1);
		cal.add(Calendar.DAY_OF_YEAR, -1);
		
		return cal.getTime();
	}
}
