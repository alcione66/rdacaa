package ec.gob.msp.rdacaa.reporte.model;

import ec.gob.msp.rdacaa.utilitario.enumeracion.ComunEnum;
import ec.gob.msp.rdacaa.utilitario.fecha.FechasUtil;
import ec.gob.msp.rdacaa.utilitario.forma.ModeloTablaGenerico;

public class CaptacionVacunasModel extends ModeloTablaGenerico<CaptacionTardiaReporte> {
	
	private static final long serialVersionUID = -4673313770337919501L;
	private static final String ESPACIO = " ";

	public CaptacionVacunasModel(String[] columnas) {
		super(columnas);
	}

	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		CaptacionTardiaReporte obj = getListaDatos().get(rowIndex);
		
		switch (columnIndex) {
		case 0:
			return "";
		case 1:
			return rowIndex + 1;
		case 2:
			StringBuilder nombre = new StringBuilder();
			nombre.append(obj.getPersona().getPrimerapellido());
			nombre.append(ESPACIO);
			nombre.append(obj.getPersona().getSegundoapellido() == null ? "" :  obj.getPersona().getSegundoapellido());
			nombre.append(ESPACIO);
			nombre.append(obj.getPersona().getPrimernombre());
			nombre.append(ESPACIO);
			nombre.append(obj.getPersona().getSegundonombre() == null ? "" : obj.getPersona().getSegundonombre());
			return nombre;
		case 3:
			return obj.getPersona().getNumerohistoriaclinica();
		case 4:
			StringBuilder edadAnios = new StringBuilder();
			edadAnios.append(FechasUtil.getDiffDatesDesdeHasta(obj.getPersona().getFechanacimiento(), obj.getAtencionMedica().getFechaatencion(),0));
			return edadAnios;
		case 5:
			StringBuilder edadMeses = new StringBuilder();
			edadMeses.append(FechasUtil.getDiffDatesDesdeHasta(obj.getPersona().getFechanacimiento(), obj.getAtencionMedica().getFechaatencion(),1));
			return edadMeses;
		case 6:
			StringBuilder edadDias = new StringBuilder();
			edadDias.append(FechasUtil.getDiffDatesDesdeHasta(obj.getPersona().getFechanacimiento(), obj.getAtencionMedica().getFechaatencion(),2));
			return edadDias;
		case 7:
			return obj.getPersona().getCtsexoId().getDescripcion();
		case 8:
			return "SI";
		case 9:
			return obj.getPersona().getParroquiaId().getCantonId().getProvinciaId().getDescripcion();
		case 10:
			return obj.getPersona().getParroquiaId().getCantonId().getDescripcion();
		case 11:
			return obj.getPersona().getParroquiaId().getDescripcion();
		case 12:
			return obj.getPersona().getPaisId().getDescripcion();
		case 13:
			return obj.getPersona().getCtetniaId().getDescripcion();
		case 14:
			return obj.getIntraextramural().getCtlugaratencionId().getDescripcion();
		case 15:
			return FechasUtil.formateadorfechaAString(ComunEnum.PATRON_FECHA6.getDescripcion(), obj.getFechaColocacionVacuna());
		case 16:
			return obj.getVacunaIPV();
		case 17:
			return obj.getVacunaFiebreAmarilla();
		case 18:
			return obj.getVacunaSRP();
		case 19:
			return obj.getVacunaDPT();
		case 20:
			return obj.getVacunaDTPediatrica();
		case 21:
			return obj.getVacunadTAdulto();
		case 22:
			return obj.getVacunaSR();
		case 23:
			return obj.getVacunaHBPediatrica();
		case 24:
			return obj.getVacunaHBAdulto();
		case 25:
			return obj.getVacunafIPV();
		case 26:
			return obj.getVacunaSRPDos();
		case 27:
			return obj.getVacunaOPV();
		case 28:
			return obj.getVacunaDPTDos();
		case 29:
			return obj.getVacunaDTPediatricaDos();
		case 30:
			return obj.getVacunadTAdultoDos();
		case 31:
			return obj.getVacunaSRDos();
		case 32:
			return obj.getVacunaHBPediatricaDos();
		case 33:
			return obj.getVacunaHBAdultoDos();
		case 34:
			return obj.getVacunafIPVDos();
		case 35:
			return obj.getVacunabOPV();
		case 36:
			return obj.getVacunaOPVDos();
		case 37:
			return obj.getVacunaDPTTres();
		case 38:
			return obj.getVacunaDTPediatricaTres();
		case 39:
			return obj.getVacunadTAdultoTres();
		case 40:
			return obj.getVacunaHBPediatricaTres();
		case 41:
			return obj.getVacunaHBAdultoTres();
		case 42:
			return obj.getVacunabOPVDos();
		case 43:
			return obj.getVacunaOPVTres();
		case 44:
			return obj.getVacunaDPTCuatro();
		case 45:
			return obj.getVacunaDTPediatricaCuatro();
		case 46:
			return obj.getVacunadTAdultoCuatro();
		case 47:
			return obj.getVacunabOPVTres();
		case 48:
			return obj.getVacunaDTPediatricaCinco();
		case 49:
			return obj.getVacunadTAdultoCinco();
		case 50:
			return obj.getVacunadTAdultoSeis();
		default:
			return "";
		}
	}
}
