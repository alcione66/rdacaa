package ec.gob.msp.rdacaa.reporte.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;

import com.github.lgooddatepicker.components.DatePicker;

import ec.gob.msp.rdacaa.business.entity.Atencionmedica;
import ec.gob.msp.rdacaa.business.entity.Registrovacunacion;
import ec.gob.msp.rdacaa.business.entity.Vacuna;
import ec.gob.msp.rdacaa.business.service.AtencionmedicaService;
import ec.gob.msp.rdacaa.business.service.PersonaService;
import ec.gob.msp.rdacaa.business.service.RegistrovacunacionService;
import ec.gob.msp.rdacaa.business.service.VacunaService;
import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.reporte.forma.RegistroVacunas;
import ec.gob.msp.rdacaa.reporte.model.RegistroVacunasTablaModel;
import ec.gob.msp.rdacaa.reporte.model.VacunasModel;
import ec.gob.msp.rdacaa.reporte.model.VacunasReporte;
import ec.gob.msp.rdacaa.reporte.utilitario.ReportesUtil;
import ec.gob.msp.rdacaa.seguridades.util.SeguridadUtil;
import ec.gob.msp.rdacaa.utilitario.fecha.FechasUtil;
import ec.gob.msp.rdacaa.utilitario.forma.FormatoTabla;
import ec.gob.msp.rdacaa.utilitario.forma.UtilitarioForma;

@Controller
@Lazy(value = false)
public class RegistroVacunasController  {

	private static final Logger logger = LoggerFactory.getLogger(RegistroVacunasController.class);
	private RegistroVacunas registroVacunasFrame;
	private AtencionmedicaService atencionmedicaService;
	private PersonaService personaService;
	private VacunaService vacunaService;
	private RegistrovacunacionService registrovacunacionService;
	private JButton btnBuscar;
	private JButton btnDescargar;
	private JTable tblReporte;
	private DatePicker dttDesde;
	private DatePicker dttHasta;
	private List<VacunasReporte> controles;
	private RegistroVacunasTablaModel modeloTabla;

	
	@Autowired
	public RegistroVacunasController(RegistroVacunas registroVacunasFrame,
									 AtencionmedicaService atencionmedicaService,
									 PersonaService personaService,
									 VacunaService vacunaService,
									 RegistrovacunacionService registrovacunacionService) {
		this.registroVacunasFrame = registroVacunasFrame;
		this.atencionmedicaService = atencionmedicaService;
		this.personaService = personaService;
		this.vacunaService = vacunaService;
		this.registrovacunacionService = registrovacunacionService;
	}

	@PostConstruct
	private void init() {
		this.dttDesde = registroVacunasFrame.getDttDesde();
		this.dttHasta = registroVacunasFrame.getDttHasta();
		this.btnBuscar = registroVacunasFrame.getBtnBuscar();
		this.btnDescargar = registroVacunasFrame.getBtnDescargar();
		this.tblReporte = registroVacunasFrame.getTblReporte();
		
		btnBuscar.addActionListener(e -> consultar());
		btnDescargar.addActionListener(e -> exportar());
		cargarTabla();
		registroVacunasFrame.addInternalFrameListener( new  InternalFrameAdapter() {
			@Override
			public void internalFrameClosing(InternalFrameEvent e) {
				super.internalFrameClosing(e);
				cerrar();
			}
		});
	}

	private void consultar() {
		List<VacunasModel> infoVacunas;
		controles = new ArrayList<>();
		if (dttDesde.getDate() != null && dttHasta.getDate() != null) {
			List<Atencionmedica> atenciones = atencionmedicaService.findAllAtencionesByDates(VariableSesion.getInstanciaVariablesSession().getProfesional().getId(), Date.from(dttDesde.getDate().atStartOfDay(ZoneId.systemDefault()).toInstant()),
					  Date.from(dttHasta.getDate().atStartOfDay(ZoneId.systemDefault()).toInstant()),
						VariableSesion.getInstanciaVariablesSession().getCtEspecialidad().getId());
			if(!atenciones.isEmpty()){
				controles = new ArrayList<>();
				infoVacunas = new ArrayList<>();
				for (Atencionmedica atencion : atenciones) {
					VacunasModel vm = new VacunasModel();
					vm.setAtencionMedica(atencion);
					String co = null;
					try {
						co = SeguridadUtil.descifrarString(atencion.getKsCident(), VariableSesion.getInstanciaVariablesSession().getUtilitario(), VariableSesion.getInstanciaVariablesSession().getUsuario().getLogin()) ;
						vm.setPersona(personaService.findOneByPersonaUUID(co));
					} catch (GeneralSecurityException e) {
						logger.error("Error GeneralSecurityException {} ",e.getMessage());
					} catch (IOException e) {
						logger.error("Error IOException {} ",e.getMessage());
					}
					List<Registrovacunacion> rv = registrovacunacionService.findAllRegistroVacunaByAtencionMedica(atencion.getId());
					if(!rv.isEmpty()){
						vm.setRegistros(rv);
					}
					infoVacunas.add(vm);
				}
				int anios = 0;
				int meses = 0;
				int dias = 0;
				int edad = 0;
				int grupo = 0;
				
				int[][] valor = new int[20][9];
				
				for (VacunasModel vacunaModel : infoVacunas) {
					anios = FechasUtil.getDiffDatesDesdeHasta(vacunaModel.getPersona().getFechanacimiento(), vacunaModel.getAtencionMedica().getFechaatencion(),0);
					meses = FechasUtil.getDiffDatesDesdeHasta(vacunaModel.getPersona().getFechanacimiento(), vacunaModel.getAtencionMedica().getFechaatencion(),1);
					dias = FechasUtil.getDiffDatesDesdeHasta(vacunaModel.getPersona().getFechanacimiento(), vacunaModel.getAtencionMedica().getFechaatencion(),2);
					edad = concatenarEdad(anios,meses,dias);
					grupo = buscarGrupo(edad);
					if(vacunaModel.getRegistros() != null){
						for (Registrovacunacion registroVacunacion : vacunaModel.getRegistros()) {
							for (int i = 0; i < valor.length; i++) {
								for (int j = 0; j < 9; j++) {
									if(registroVacunacion.getEsquemavacunacionId().getVacunaId().getId() == (i + 1) && grupo == (j + 1)){
										int cant = valor[i][j];
										cant++;
										valor[i][j] = cant;
									}
								}
								
							}
						}
					}
					
				}
				List<Vacuna> vacunas = vacunaService.findAllOrderedByDescripcion(); 
				for (Vacuna vacuna : vacunas) {
					VacunasReporte vr = new VacunasReporte();
					vr.setDescripcion(vacuna.getNombrevacuna());
					for (int i = 0; i < valor.length; i++) {
						if(vacuna.getId() == (i+1)){
							vr.setNum028(valor[i][0]);
							vr.setNum111(valor[i][1]);
							vr.setNum14(valor[i][2]);
							vr.setNum59(valor[i][3]);
							vr.setNum1014(valor[i][4]);
							vr.setNum1519(valor[i][5]);
							vr.setNum2039(valor[i][6]);
							vr.setNum4064(valor[i][7]);
							vr.setNum65(valor[i][8]);
							break;
						}
					}
					controles.add(vr);
				}
				cargarTabla();
			}else{
				UtilitarioForma.muestraMensaje("Consulta", registroVacunasFrame, "INFO", "No existen resultados para los parámetros ingresados");
			}
		}
	}

	private void cargarTabla() {
		tblReporte.setDefaultRenderer(Object.class, new FormatoTabla());
		String[] columnNames = { "", 
								 "VACUNA",
								 "0 a 28 DIAS", 
								 "1 MES A 11 MESES", 
								 "1 AÑO A 4 AÑOS", 
								 "5 AÑOS A 9 AÑOS",
								 "10 A 14 AÑOS",
								 "15 A 19 AÑOS",
								 "20 A 39 AÑOS",
								 "40 A 64 AÑOS",
								 "> 65 AÑOS"
								 };

		modeloTabla = new RegistroVacunasTablaModel(columnNames);
		controles = controles == null ? new ArrayList<>() : controles;
		modeloTabla.addFila(controles);
		tblReporte.setModel(modeloTabla);
		tblReporte.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		modeloTabla.ocultaColumnaCodigo(tblReporte);
		modeloTabla.definirAnchoColumnas(tblReporte, new int[] {200,200,200,200,200,200,200,200,200,200});

	}

	private void exportar() {
            String autor = VariableSesion.getInstanciaVariablesSession().getUsuario().getPersonaId().getPrimernombre() + " ";
         	   autor += VariableSesion.getInstanciaVariablesSession().getUsuario().getPersonaId().getPrimerapellido() + " ";
            StringBuilder fechas = new StringBuilder();
	    fechas.append("DESDE: ").append(dttDesde.getDateStringOrEmptyString()).append(" HASTA: ").append(dttHasta.getDateStringOrEmptyString());
            ReportesUtil.exportarXlsReportes(tblReporte, "Registro Vacunas", autor, "xls","Registro de Vacunas",fechas.toString());
	}
	private  Integer concatenarEdad(Integer edadAnios, Integer edadMeses, Integer edadDias) {
    	String edadString = StringUtils.leftPad(edadAnios.toString(), 3, "0") + StringUtils.leftPad(edadMeses.toString(), 2, "0") + StringUtils.leftPad(edadDias.toString(), 2, "0");
    	return Integer.parseInt(edadString);
	} 
	
	private int buscarGrupo(Integer edad){
		int grupo = 0;
		
		if(edad >= 650000){
			grupo = 9;
		}else if(edad >= 400000 && edad < 650000 ){
			grupo = 8;
		}else if(edad >= 200000 && edad < 400000 ){
			grupo = 7;
		}else if(edad >= 150000 && edad < 200000 ){
			grupo = 6;
		}else if(edad >= 100000 && edad < 150000 ){
			grupo = 5;
		}else if(edad >= 50000 && edad < 100000 ){
			grupo = 4;
		}else if(edad >= 10000 && edad < 50000 ){
			grupo = 3;
		}else if(edad > 28 && edad < 10000 ){
			grupo = 2;
		}else if(edad <= 28 ){
			grupo = 1;
		}
		
		return grupo;
	}
	private void cerrar(){
		
		dttDesde.setText("");
		dttHasta.setText("");
		if(modeloTabla != null){
			modeloTabla.clear();
		}
		
	}
}
