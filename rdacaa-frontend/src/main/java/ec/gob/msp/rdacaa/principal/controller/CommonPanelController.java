/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.principal.controller;


import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import com.github.cjwizard.WizardPage;

import ec.gob.msp.rdacaa.admision.controller.AdmisionController;
import ec.gob.msp.rdacaa.amed.controller.DatosAntropometricosController;
import ec.gob.msp.rdacaa.amed.controller.DatosSIVANController;
import ec.gob.msp.rdacaa.amed.controller.DiagnosticoCIEController;
import ec.gob.msp.rdacaa.amed.controller.GrupoPrioritarioController;
import ec.gob.msp.rdacaa.amed.controller.GrupoVulnerableController;
import ec.gob.msp.rdacaa.amed.controller.InformacionObstetricaController;
import ec.gob.msp.rdacaa.amed.controller.OdontologiaController;
import ec.gob.msp.rdacaa.amed.controller.OrdenMedicaController;
import ec.gob.msp.rdacaa.amed.controller.PrescripcionController;
import ec.gob.msp.rdacaa.amed.controller.ProcedimientosController;
import ec.gob.msp.rdacaa.amed.controller.ReferenciaController;
import ec.gob.msp.rdacaa.amed.controller.VIHController;
import ec.gob.msp.rdacaa.amed.controller.VacunasController;
import ec.gob.msp.rdacaa.amed.controller.ViolenciaController;
import ec.gob.msp.rdacaa.amed.forma.DatosAntropometricos;
import ec.gob.msp.rdacaa.amed.forma.DatosSIVAN;
import ec.gob.msp.rdacaa.amed.forma.DiagnosticoCIE;
import ec.gob.msp.rdacaa.amed.forma.GrupoPrioritario;
import ec.gob.msp.rdacaa.amed.forma.GrupoVulnerable;
import ec.gob.msp.rdacaa.amed.forma.InfoObstetrica;
import ec.gob.msp.rdacaa.amed.forma.OdontologiaPanel;
import ec.gob.msp.rdacaa.amed.forma.OrdenMedica;
import ec.gob.msp.rdacaa.amed.forma.Prescripcion;
import ec.gob.msp.rdacaa.amed.forma.Procedimientos;
import ec.gob.msp.rdacaa.amed.forma.Referencia;
import ec.gob.msp.rdacaa.amed.forma.VIH;
import ec.gob.msp.rdacaa.amed.forma.Vacunas;
import ec.gob.msp.rdacaa.amed.forma.ViolenciaPanel;
import ec.gob.msp.rdacaa.business.validation.common.ValidationSupport;
import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.principal.wizard.PanelAdmision;
import ec.gob.msp.rdacaa.principal.wizard.PanelVacunas;
import ec.gob.msp.rdacaa.utilitario.forma.UtilitarioForma;

/**
 *
 * @author Saulo Velasco
 */
@Controller
public class CommonPanelController {

	private final VariableSesion variableSesion = VariableSesion.getInstanciaVariablesSession();
	private static final int CODIGOESTABLECIMIENTO = 712;

    @Autowired
    private AdmisionController admisionController;
    @Autowired
    private GrupoVulnerableController grupoVulnerableController;
    @Autowired
    private GrupoPrioritarioController grupoPrioritarioController;
    @Autowired
    private DatosAntropometricosController datosAntropometricosController;
    @Autowired
    private InformacionObstetricaController informacionObstetricaController;
    @Autowired
    private VIHController vihController;
    @Autowired
    private VacunasController vacunasController;
    @Autowired
    private DatosSIVANController datosSIVANController;
    @Autowired
    private OrdenMedicaController examenesLaboratorio;
    @Autowired 
    private DiagnosticoCIEController diagnosticoCIEController;
    @Autowired
    private ProcedimientosController procedimientosController;
    @Autowired 
    private OdontologiaController odontologiaController;
    @Autowired
    private PrescripcionController prescripcionController;
    @Autowired
    private ReferenciaController referenciaController;
    @Autowired
    private DatosAntropometricos datosAntropometricos;
    @Autowired
    private GrupoPrioritario grupoPrioritario;
    @Autowired
    private InfoObstetrica infoObstetrica;
    @Autowired
    private VIH Vih;
    @Autowired
    private Vacunas vacunas;
    @Autowired
    private DiagnosticoCIE diagnosticoCIE;
    @Autowired
    private OrdenMedica ordenMedica;
    @Autowired
    private Prescripcion prescripcion;
    @Autowired
    private Procedimientos procedimientos;
    @Autowired
    private OdontologiaPanel odontologia;
    @Autowired
    private DatosSIVAN datosSIVAN;
    @Autowired
    private Referencia referencia;
    @Autowired
    private ViolenciaPanel violenciaPanel;
    @Autowired
    private GrupoVulnerable grupoVulnerable;
    @Autowired
    private ViolenciaController violenciaController;
    

    public void refreshPanels() {
        refreshGrupoPrioritarioPanel();
        refreshDatosAntropometricos();
        refreshDatosObstetricos();
        refreshVIH();
        refreshVacunas();
        refreshDiagnosticoCIE();
        refreshOrdenMedica();
        refreshPrescripcion();
        refreshProcedimientos();
        refreshOdontologia();
        refreshDatosSIVAN();
        refreshReferencia();
        refreshViolenciaPanel();
        refreshVulnerablesPanel();
    }

    private void refreshGrupoPrioritarioPanel() {
        grupoPrioritario.getLblPaciente().setText(getCabeceraPacienteTexto());
        grupoPrioritarioController.loadLugarAtencion();
        grupoPrioritarioController.loadCatalogoGrupoPrioritario();
        if(variableSesion.getBanderaCambioSexoGrupoP() != null && variableSesion.getBanderaCambioSexoGrupoP()) {
            grupoPrioritarioController.removeItemsJListGrupoPrioritario();
        }
    }

    private void refreshDatosAntropometricos() {
        datosAntropometricos.getLblPaciente().setText(getCabeceraPacienteTexto());
        datosAntropometricosController.addAlturaEntidad();
        datosAntropometricosController.addAlertasToPanel();
        datosAntropometricosController.refreshComponentsTallaCorregida();
    }
    
    private void refreshDatosObstetricos() {
        infoObstetrica.getLblPaciente().setText(getCabeceraPacienteTexto());
        informacionObstetricaController.loadCatRiesgoObstetrico();
        informacionObstetricaController.updateDatePickerSettingsFUM();
        informacionObstetricaController.setterSemanasGestacion();
    }
    
    private void refreshVIH() {
        Vih.getLblPaciente().setText(getCabeceraPacienteTexto());
        vihController.validateForm();
    }

    private void refreshVacunas(){
        vacunas.getLblPaciente().setText(getCabeceraPacienteTexto());
        vacunasController.loadVacunas();
    }
    
    private void refreshDiagnosticoCIE(){
        diagnosticoCIE.getLblPaciente().setText(getCabeceraPacienteTexto());
        diagnosticoCIEController.loadDescripcionCie();
        diagnosticoCIEController.preloadCmbTipoAtencion();
    }
    
    private void refreshOrdenMedica(){
        ordenMedica.getLblPaciente().setText(getCabeceraPacienteTexto());
        ordenMedica.getPanelPruebasSifilis().setVisible(variableSesion.getSemanasGestacion() != null && variableSesion.getSemanasGestacion() != 20);
    }
    
    private void refreshPrescripcion(){
        prescripcion.getLblPaciente().setText(getCabeceraPacienteTexto());
        Object[] controles_6_24_meses = { prescripcion.getRbhierromineralSI(), prescripcion.getRbhierromineralNO() };
        Object[] controles_6_59_meses = { prescripcion.getRbvitaminacapsulaSI(), prescripcion.getRbvitaminacapsulaNO() };
        Object[] controles_24_59_meses = { prescripcion.getRbhierrojarabeSI(), prescripcion.getRbhierrojarabeNO() };
        Object[] controles_mujer_gestante = { prescripcion.getRbhierroacidofolicoSI(), prescripcion.getRbhierroacidofolicoNO() };   
        UtilitarioForma.habilitarControles(false, controles_6_24_meses);
        UtilitarioForma.habilitarControles(false, controles_6_59_meses);
        UtilitarioForma.habilitarControles(false, controles_24_59_meses);
        UtilitarioForma.habilitarControles(false, controles_mujer_gestante);
        
        Integer edadAniosMesDias = Integer.parseInt(concatenarEdad(variableSesion.getAniosPersona(), variableSesion.getMesesPersona(), variableSesion.getDiasPersona()));
        
        if(variableSesion.getAniosPersona() != null && edadAniosMesDias < 50000) {
            if (edadAniosMesDias >= 600 && edadAniosMesDias < 20000){
                UtilitarioForma.habilitarControles(true, controles_6_24_meses);
            }
            if (edadAniosMesDias >= 600 && edadAniosMesDias < 50000){
                UtilitarioForma.habilitarControles(true, controles_6_59_meses);
            }
            if (edadAniosMesDias >= 20000 && edadAniosMesDias < 50000){
                UtilitarioForma.habilitarControles(true, controles_24_59_meses);
            }
        } else {
            if((variableSesion.getIsMujer() != null && variableSesion.getIsMujer()) && (variableSesion.getIsEmbarazada() != null && variableSesion.getIsEmbarazada())) {
                if(variableSesion.getAniosPersona() != null && variableSesion.getAniosPersona() > 8) {
                    UtilitarioForma.habilitarControles(true, controles_mujer_gestante);
                }
            }
        }
    }
    
    private String concatenarEdad(Integer edadAnios, Integer edadMeses, Integer edadDias) {
        return StringUtils.leftPad(edadAnios.toString(), 3, "0") + StringUtils.leftPad(edadMeses.toString(), 2, "0")
                + StringUtils.leftPad(edadDias.toString(), 2, "0");
    }
    
    private void refreshProcedimientos(){
        procedimientos.getLblPaciente().setText(getCabeceraPacienteTexto());
    }
    
    private void refreshOdontologia(){
        odontologia.getLblPaciente().setText(getCabeceraPacienteTexto());
        Integer edadAniosMesDias = Integer.parseInt(concatenarEdad(variableSesion.getAniosPersona(), variableSesion.getMesesPersona(), variableSesion.getDiasPersona()));
        if(edadAniosMesDias >= 120000) {
            Object[] controlesCPO = { odontologia.getCtxCariados(),odontologia.getCtxPerdidos(),odontologia.getCtxObturados() };
            odontologia.getCtxExtraidos().setText("0");
            UtilitarioForma.habilitarControles(false, odontologia.getCtxExtraidos());
            UtilitarioForma.habilitarControles(true, controlesCPO);
        } else if(edadAniosMesDias > 600 && edadAniosMesDias < 120000){
            Object[] controlesCEO = { odontologia.getCtxCariados(), odontologia.getCtxExtraidos(),odontologia.getCtxObturados() };
            odontologia.getCtxPerdidos().setText("0");
            UtilitarioForma.habilitarControles(false, odontologia.getCtxPerdidos());
            UtilitarioForma.habilitarControles(true, controlesCEO);
        }
    }
    
    private void refreshDatosSIVAN(){
        datosSIVAN.getLblPaciente().setText(getCabeceraPacienteTexto());
        Object[] habilitarControles = {
            datosSIVAN.getRbrecibiolechematSI(),
            datosSIVAN.getRbrecibiolechematNO(),
            datosSIVAN.getRbconsumioalimentosolidSI(),
            datosSIVAN.getRbconsumioalimentosolidNO(),
            datosSIVAN.getRbperiodolactanciaSI(),
            datosSIVAN.getRbperiodolactanciaNO()
        };
        Object[] controlesAlimentoSolido = { datosSIVAN.getRbconsumioalimentosolidSI(), datosSIVAN.getRbconsumioalimentosolidNO() };
        Object[] controlesLecheMaterna = { datosSIVAN.getRbrecibiolechematSI(), datosSIVAN.getRbrecibiolechematNO() };
        Object[] controlesPeriodoLactancia = { datosSIVAN.getRbperiodolactanciaSI(), datosSIVAN.getRbperiodolactanciaNO() };
        
        UtilitarioForma.habilitarControles(false, habilitarControles);
        
        Integer edadAniosMesDias = Integer.parseInt(concatenarEdad(variableSesion.getAniosPersona(), variableSesion.getMesesPersona(), variableSesion.getDiasPersona()));
        
        // 0 a 5 meses -- Leche materna
        if(edadAniosMesDias < 600) {
        	UtilitarioForma.habilitarControles(true, controlesLecheMaterna);
        	//Disable - Clean
        	UtilitarioForma.habilitarControles(false, controlesAlimentoSolido);
        	UtilitarioForma.habilitarControles(false, controlesPeriodoLactancia);
        	datosSIVAN.getBtnGroupConsumioAlimentoSolid().clearSelection();
        	datosSIVAN.getBtnGroupPeriodoLactancia().clearSelection();
        }
        // 6 a 8 meses -- Elementos solidos
        if(edadAniosMesDias >= 600 && edadAniosMesDias < 900) {
        	UtilitarioForma.habilitarControles(true, controlesAlimentoSolido);
        	//Disable - Clean
            UtilitarioForma.habilitarControles(false, controlesLecheMaterna);
            UtilitarioForma.habilitarControles(false, controlesPeriodoLactancia);
            datosSIVAN.getBtnGroupRecibioLecheMat().clearSelection();
            datosSIVAN.getBtnGroupPeriodoLactancia().clearSelection();
        }
        // Mujer y Embarazada 8 años y 56 años
        if (variableSesion.getIsMujer() != null && edadAniosMesDias >= 80000 && edadAniosMesDias < 560000) {
            if (variableSesion.getIsMujer()){
                UtilitarioForma.habilitarControles(true, controlesPeriodoLactancia);
                UtilitarioForma.habilitarControles(false, controlesAlimentoSolido);
            	UtilitarioForma.habilitarControles(false, controlesLecheMaterna);
            	datosSIVAN.getBtnGroupConsumioAlimentoSolid().clearSelection();
            	datosSIVAN.getBtnGroupRecibioLecheMat().clearSelection();
            } else {
                UtilitarioForma.habilitarControles(false, controlesPeriodoLactancia);
            }
        }
    }
    
    private void refreshReferencia(){
        referencia.getLblPaciente().setText(getCabeceraPacienteTexto());
    }
    
    private String getCabeceraPacienteTexto() {
        String espacios = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
        String sexo = ((variableSesion.getIsHombre() != null && variableSesion.getIsHombre())) ? espacios + " SEXO: Hombre" : ((variableSesion.getIsMujer() != null && variableSesion.getIsMujer())) ? espacios + " SEXO: Mujer" : ((variableSesion.getIsInterSexual() != null && variableSesion.getIsInterSexual())) ? espacios + " SEXO: Intersexual" : " ";
        String tipoAtencion = "";
        if(variableSesion.getIntraExtraMural() != null) {
           tipoAtencion = (variableSesion.getIntraExtraMural().getCtlugaratencionId().getId() == CODIGOESTABLECIMIENTO) ? "ATENCIÓN: INTRAMURAL" : "ATENCIÓN: EXTRAMURAL";
        }
        String historiaclinica = "";
        String paciente = "";
        String edad = "";
        if(variableSesion.getPersona() != null) {
            historiaclinica = "HCUE: " + variableSesion.getPersona().getNumerohistoriaclinica();
            paciente = espacios + " PACIENTE: " + variableSesion.getPersona().getPrimernombre();
            if(!ValidationSupport.isNullOrEmptyString(variableSesion.getPersona().getSegundonombre())){
                paciente += " "+variableSesion.getPersona().getSegundonombre();
            }
            if(!ValidationSupport.isNullOrEmptyString(variableSesion.getPersona().getPrimerapellido())){
                paciente += " "+variableSesion.getPersona().getPrimerapellido();
            }
            if(!ValidationSupport.isNullOrEmptyString(variableSesion.getPersona().getSegundoapellido())){
                paciente += " "+variableSesion.getPersona().getSegundoapellido();
            }
            edad = espacios + " EDAD: " + variableSesion.getAniosPersona() + " años "
                    + variableSesion.getMesesPersona() + " meses " + variableSesion.getDiasPersona() + " días";
        }
        String semanasGestacion = "";
        if(variableSesion.getSemanasGestacion() != null){
            semanasGestacion = "SEMANAS DE GESTACIÓN: " + variableSesion.getSemanasGestacion();
        }
          
        return "<html>"
                + "<body>"
                + "&nbsp;"
                + historiaclinica 
                + paciente 
                + "&nbsp; <span style=\"color: #0635A9;\"> "+ edad +" </span>"  
                + sexo 
                +""
                + "<br> <br> &nbsp;"+ tipoAtencion +""
                + "&nbsp;&nbsp;&nbsp;&nbsp;"+ semanasGestacion +""
                + "</body>"
                + "</html>";
    }
    
    private void refreshViolenciaPanel(){
        violenciaPanel.getLblPaciente().setText(getCabeceraPacienteTexto());
    }
    

    private void refreshVulnerablesPanel(){
        grupoVulnerable.getLblPaciente().setText(getCabeceraPacienteTexto());
        grupoVulnerableController.loadGruposVulnerables();
    }

    public void cleanInputsAllPanels() {
        admisionController.cleanInput();
        grupoPrioritarioController.cleanInput();
        datosAntropometricosController.cleanInput();
        informacionObstetricaController.cleanInput();
        vihController.cleanInput();
        vacunasController.cleanInput("commons");
        datosSIVANController.cleanInput();
        examenesLaboratorio.cleanInput();
        diagnosticoCIEController.cleanInput("commons");
        procedimientosController.cleanInput("commons");
        odontologiaController.cleanInput();
        prescripcionController.cleanInput();
        referenciaController.cleanInput();
        violenciaController.cleanInput();
        grupoVulnerableController.cleanInput("commons");
        
    }
    
    public void initDatesVetoPolicy(WizardPage newPage){
    	if(newPage instanceof PanelAdmision){
    		admisionController.setDatePickerVetoPolicy();
    	}
    	if(newPage instanceof PanelVacunas){
    		vacunasController.setDatePickerVetoPolicyVacuna();
    	}
    }
    
    public void initCambioDPA(WizardPage newPage){
    	if(newPage instanceof PanelAdmision){
    		admisionController.loadEntidad();
    	}
    }
}
