/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.amed.model;

import ec.gob.msp.rdacaa.business.service.VacunaXEsquema;
import ec.gob.msp.rdacaa.control.swing.model.CustomObjectToStringConverter;

/**
 *
 * @author dmurillo
 */
public class VacunaToStringConverter extends CustomObjectToStringConverter {

    @Override
    public String getPreferredStringForItem(Object o) {
        if (o == null) {
            return null;
        } else if (o instanceof VacunaXEsquema) {
            return ((VacunaXEsquema) o).getNombreVacunaXNombreEsquema();
        } else {
            return o.toString();
        }
    }    

}
