/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ec.gob.msp.rdacaa.principal.wizard;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.github.cjwizard.WizardSettings;

import ec.gob.msp.rdacaa.amed.controller.ProcedimientosController;
import ec.gob.msp.rdacaa.amed.forma.Procedimientos;
import ec.gob.msp.rdacaa.principal.controller.CommonPanelController;

/**
 *
 * @author eduardo
 */
@Component
public class PanelProcedimientos extends com.github.cjwizard.WizardPage{
    
    private Procedimientos procedimientos;
    private ProcedimientosController procedimientosController;
    private CommonPanelController commonPanelController;
    
    @Autowired
    public PanelProcedimientos(Procedimientos procedimientos,ProcedimientosController procedimientosController,CommonPanelController commonPanelController) {
        super("Registro de Procedimientos y Actividades", "Atencion Médica");
        this.procedimientos = procedimientos;
        this.procedimientosController = procedimientosController;
        this.commonPanelController = commonPanelController;
    }
    
    @PostConstruct
    private void init() {
        procedimientos.setVisible(true);
        add(procedimientos);
    }

    @Override
    public void updateSettings(WizardSettings settings) {
        commonPanelController.refreshPanels();
    }
}
