package ec.gob.msp.rdacaa.reporte.controller;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.time.ZoneId;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.swing.JButton;
import javax.swing.JTable;
import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;
import org.springframework.stereotype.Controller;

import com.github.lgooddatepicker.components.DatePicker;

import ec.gob.msp.rdacaa.business.entity.Atencionmedica;
import ec.gob.msp.rdacaa.business.entity.Entidad;
import ec.gob.msp.rdacaa.business.entity.Intraextramural;
import ec.gob.msp.rdacaa.business.entity.VariablesGruposPrioritarios;
import ec.gob.msp.rdacaa.business.entity.VariablesVacunas;
import ec.gob.msp.rdacaa.business.service.AtencionmedicaService;
import ec.gob.msp.rdacaa.business.service.EntidadService;
import ec.gob.msp.rdacaa.business.service.IntraextramuralService;
import ec.gob.msp.rdacaa.business.service.PersonaService;
import ec.gob.msp.rdacaa.business.service.VariablesGruposPrioritariosService;
import ec.gob.msp.rdacaa.business.service.VariablesVacunasService;
import ec.gob.msp.rdacaa.frontend.sesion.VariableSesion;
import ec.gob.msp.rdacaa.reporte.forma.ReporteVacunas;
import ec.gob.msp.rdacaa.reporte.model.ReporteVacunasModel;
import ec.gob.msp.rdacaa.reporte.model.ReporteVacunasTablaModel;
import ec.gob.msp.rdacaa.reporte.utilitario.ReportesUtil;
import ec.gob.msp.rdacaa.seguridades.util.SeguridadUtil;
import ec.gob.msp.rdacaa.utilitario.forma.FormatoTabla;
import ec.gob.msp.rdacaa.utilitario.forma.UtilitarioForma;

@Controller
@Lazy(value = false)
public class ReporteVacunasController  {

	private static final Logger logger = LoggerFactory.getLogger(ReporteVacunasController.class);
	private ReporteVacunas reporteVacunasFrame;
	private AtencionmedicaService atencionmedicaService;
	private PersonaService personaService;
	private EntidadService entidadService;
	private VariablesVacunasService variablesVacunasService;
	private IntraextramuralService intraextramuralService;
	private VariablesGruposPrioritariosService variablesGruposPrioritariosService;
	private JButton btnBuscar;
	private JButton btnDescargar;
	private JTable tblReporte;
	private DatePicker dttDesde;
	private DatePicker dttHasta;
	private List<ReporteVacunasModel> controles;
	private ReporteVacunasTablaModel modeloTabla;

	
	@Autowired
	public ReporteVacunasController(ReporteVacunas reporteVacunasFrame,
									 AtencionmedicaService atencionmedicaService,
									 PersonaService personaService,
									 EntidadService entidadService,
									 IntraextramuralService intraextramuralService,
									 VariablesVacunasService variablesVacunasService,
									 VariablesGruposPrioritariosService variablesGruposPrioritariosService
									) {
		this.reporteVacunasFrame = reporteVacunasFrame;
		this.atencionmedicaService = atencionmedicaService;
		this.personaService = personaService;
		this.entidadService = entidadService;
		this.variablesVacunasService = variablesVacunasService;
		this.variablesGruposPrioritariosService = variablesGruposPrioritariosService;
		this.intraextramuralService = intraextramuralService;
	}

	@PostConstruct
	private void init() {
		this.dttDesde = reporteVacunasFrame.getDttDesde();
		this.dttHasta = reporteVacunasFrame.getDttHasta();
		this.btnBuscar = reporteVacunasFrame.getBtnBuscar();
		this.btnDescargar = reporteVacunasFrame.getBtnDescargar();
		this.tblReporte = reporteVacunasFrame.getTblReporte();
		
		btnBuscar.addActionListener(e -> consultar());
		btnDescargar.addActionListener(e -> exportar());
		cargarTabla();
		reporteVacunasFrame.addInternalFrameListener( new  InternalFrameAdapter() {
			@Override
			public void internalFrameClosing(InternalFrameEvent e) {
				super.internalFrameClosing(e);
				cerrar();
			}
		});
	}

	private void consultar() {
		controles = new ArrayList<>();
		if (dttDesde.getDate() != null && dttHasta.getDate() != null) {
			List<Atencionmedica> atenciones = atencionmedicaService.findAllAtencionesByDates(VariableSesion.getInstanciaVariablesSession().getProfesional().getId(), Date.from(dttDesde.getDate().atStartOfDay(ZoneId.systemDefault()).toInstant()),
					  Date.from(dttHasta.getDate().atStartOfDay(ZoneId.systemDefault()).toInstant()),
						VariableSesion.getInstanciaVariablesSession().getCtEspecialidad().getId());
			if(!atenciones.isEmpty()){
				for (Atencionmedica atencion : atenciones) {
					ReporteVacunasModel rvm = new ReporteVacunasModel();
					rvm.setAtencionMedica(atencion);
					String co = null;
					try {
						co = SeguridadUtil.descifrarString(atencion.getKsCident(), VariableSesion.getInstanciaVariablesSession().getUtilitario(), VariableSesion.getInstanciaVariablesSession().getUsuario().getLogin()) ;
						rvm.setPersona(personaService.findOneByPersonaUUID(co));
						rvm.setProfesional(personaService.findOneByPersonaId(atencion.getUsuariocreacionId().getId()));
					} catch (GeneralSecurityException e) {
						logger.error("Error GeneralSecurityException {} ",e.getMessage());
					} catch (IOException e) {
						logger.error("Error IOException {} ",e.getMessage());
					}
					Entidad en = entidadService.findOneByEntidadId(atencion.getEntidadId());
					if(en != null){
						rvm.setEntidad(en);
					}
					VariablesVacunas vv = variablesVacunasService.findOneByAtencionId(atencion.getId());
					if(vv != null){
						rvm.setVariablesVacunas(vv);
					}
					VariablesGruposPrioritarios vgp = variablesGruposPrioritariosService.findOneByAtencionId(atencion.getId());
					if(vgp != null){
						rvm.setVariablesGruposPrioritarios(vgp);
					}
					Intraextramural iv = intraextramuralService.findIntraextramuralByAtencionId(atencion.getId());
					if (iv != null) {
						rvm.setIntraextramural(iv);
					}
					controles.add(rvm);
				}
				cargarTabla();
			}else{
				UtilitarioForma.muestraMensaje("Consulta", reporteVacunasFrame, "INFO", "No existen resultados para los parámetros ingresados");
			}
		}
	}

	private void cargarTabla() {
		tblReporte.setDefaultRenderer(Object.class, new FormatoTabla());
		String[] columnNames = { "", 
								 "No", 
								 "ID REGISTRO",
								 "LUGAR DE ATENCION",
								 "CODIGO ESTABLECIMIENTO",
								 "NOMBRE ESTABLECIMIENTO", 
								 "CEDULA PROFESIONAL",
								 "NOMBRE Y APELLIDOS PROFESIONAL",
								 "SEXO PROFESIONAL",
								 "FECHA NACIMIENTO PROFESIONAL",
								 "PROVINCIA PROFESIONAL", 
								 "CANTON PROFESIONAL", 
								 "PARROQUIA PROFESIONAL",
								 "ESPECIALIDAD",
								 "NOMBRE Y APELLIDOS PACIENTE", 
								 "CEDULA PACIENTE", 
								 "SEXO PACIENTE", 
								 "ORIENTACION SEXUAL PACIENTE",
								 "IDENTIDAD GENERO PACIENTE", 
								 "FECHA NACIMIENTO PACIENTE", 
								 "EDAD AÑOS PACIENTE", 
								 "EDAD MESES PACIENTE", 
								 "EDAD DÍAS PACIENTE" , 
								 "NACIONALIDAD PACIENTE", 
								 "SEGURO PACIENTE", 
								 "GP 1", 
								 "GP 2", 
								 "GP 3",
								 "GP 4", 
								 "GP 5",
								 "VACUNA 1",
								 "GR 1",
								 "DOSIS 1",
								 "LOTE 1",
								 "ESQUEMA 1",
								 "FECHA COLOCACION 1",
								 "VACUNA 2",
								 "GR 2",
								 "DOSIS 2",
								 "LOTE 2",
								 "ESQUEMA 2",
								 "FECHA COLOCACION 2",
								 "VACUNA 3",
								 "GR 3",
								 "DOSIS 3",
								 "LOTE 3",
								 "ESQUEMA 3",
								 "FECHA COLOCACION 3",
								 "VACUNA 4",
								 "GR 4",
								 "DOSIS 4",
								 "LOTE 4",
								 "ESQUEMA 4",
								 "FECHA COLOCACION 4",
								 "VACUNA 5",
								 "GR 5",
								 "DOSIS 5",
								 "LOTE 5",
								 "ESQUEMA 5",
								 "FECHA COLOCACION 5"
								 };

		modeloTabla = new ReporteVacunasTablaModel(columnNames);
		controles = controles == null ? new ArrayList<>() : controles;
		modeloTabla.addFila(controles);
		tblReporte.setModel(modeloTabla);
		tblReporte.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		modeloTabla.ocultaColumnaCodigo(tblReporte);
		modeloTabla.definirAnchoColumnas(tblReporte, definirTamano(columnNames.length));

	}

	private void exportar() {
            String autor = VariableSesion.getInstanciaVariablesSession().getUsuario().getPersonaId().getPrimernombre() + " ";
         	   autor += VariableSesion.getInstanciaVariablesSession().getUsuario().getPersonaId().getPrimerapellido() + " ";
            StringBuilder fechas = new StringBuilder();
	    fechas.append("DESDE: ").append(dttDesde.getDateStringOrEmptyString()).append(" HASTA: ").append(dttHasta.getDateStringOrEmptyString());
            ReportesUtil.exportarXlsReportes(tblReporte, "Registro Vacunas", autor, "xls","Registro de Vacunas",fechas.toString());
	}
	
	private void cerrar(){
		
		dttDesde.setText("");
		dttHasta.setText("");
		if(modeloTabla != null){
			modeloTabla.clear();
		}
		
	}
	
	private int[] definirTamano(int tam) {
		int[] medida = new int[tam];
		for (int i = 0; i < medida.length; i++) {
			medida[i] = 300;
		}
		return medida;
	}
}
