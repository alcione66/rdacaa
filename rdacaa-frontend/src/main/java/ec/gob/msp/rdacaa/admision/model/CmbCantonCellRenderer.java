package ec.gob.msp.rdacaa.admision.model;

import ec.gob.msp.rdacaa.business.entity.Canton;
import java.awt.Component;

import javax.swing.DefaultListCellRenderer;
import javax.swing.JList;


public class CmbCantonCellRenderer extends DefaultListCellRenderer {

    private static final long serialVersionUID = 31992535773831999L;

    @Override
	public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected,
			boolean cellHasFocus) {
		if (value instanceof Canton) {
			Canton canton = (Canton) value;
			value = canton.getDescripcion();
		}
		return super.getListCellRendererComponent(list, value, index, isSelected, cellHasFocus);
	}
}