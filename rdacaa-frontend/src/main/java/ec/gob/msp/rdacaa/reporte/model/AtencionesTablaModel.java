package ec.gob.msp.rdacaa.reporte.model;

import ec.gob.msp.rdacaa.utilitario.enumeracion.ComunEnum;
import ec.gob.msp.rdacaa.utilitario.fecha.FechasUtil;
import ec.gob.msp.rdacaa.utilitario.forma.ModeloTablaGenerico;

public class AtencionesTablaModel extends ModeloTablaGenerico<RegistroAtencionesModel> {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3449942638951131528L;
	private static final String ESPACIO = " ";
	private static final String NOAPLICA = "NO APLICA";
	private static final int prevencion = 227;
	private static final int morbilidad = 228;
	private StringBuilder dato = new StringBuilder(); 
	private StringBuilder tipo;

	public AtencionesTablaModel(String[] columnas) {
		super(columnas);
	}


	@Override
	public Object getValueAt(int rowIndex, int columnIndex) {
		RegistroAtencionesModel obj = getListaDatos().get(rowIndex);
		String gpCuatro = "";
		String gpTres = "";
		String gpDos = "";
		String gpUno = "";
		String gpCero = "";
		
		String dCuatro = "";
		String dTres = "";
		String dDos = "";
		String dUno = "";
		String dCero = "";
		
		/**
		 * GRUPOS PRIORITARIOS
		 */
		if(obj.getGruposPrioritarios() != null){
			switch (obj.getGruposPrioritarios().size()) {
			case 5:
				gpCuatro = obj.getGruposPrioritarios().get(4).getCtgprioritarioId().getDescripcion();
				gpTres = obj.getGruposPrioritarios().get(3).getCtgprioritarioId().getDescripcion();
				gpDos = obj.getGruposPrioritarios().get(2).getCtgprioritarioId().getDescripcion();
				gpUno = obj.getGruposPrioritarios().get(1).getCtgprioritarioId().getDescripcion();
				gpCero = obj.getGruposPrioritarios().get(0).getCtgprioritarioId().getDescripcion();
				break;
			case 4:
				gpCuatro = NOAPLICA;
				gpTres = obj.getGruposPrioritarios().get(3).getCtgprioritarioId().getDescripcion();
				gpDos = obj.getGruposPrioritarios().get(2).getCtgprioritarioId().getDescripcion();
				gpUno = obj.getGruposPrioritarios().get(1).getCtgprioritarioId().getDescripcion();
				gpCero = obj.getGruposPrioritarios().get(0).getCtgprioritarioId().getDescripcion();
				break;
			case 3:
				gpCuatro = NOAPLICA;
				gpTres = NOAPLICA;
				gpDos = obj.getGruposPrioritarios().get(2).getCtgprioritarioId().getDescripcion();
				gpUno = obj.getGruposPrioritarios().get(1).getCtgprioritarioId().getDescripcion();
				gpCero = obj.getGruposPrioritarios().get(0).getCtgprioritarioId().getDescripcion();
				break;
			case 2:
				gpCuatro = NOAPLICA;
				gpTres = NOAPLICA;
				gpDos = NOAPLICA;
				gpUno = obj.getGruposPrioritarios().get(1).getCtgprioritarioId().getDescripcion();
				gpCero = obj.getGruposPrioritarios().get(0).getCtgprioritarioId().getDescripcion();
				break;
			case 1:
				gpCuatro = NOAPLICA;
				gpTres = NOAPLICA;
				gpDos = NOAPLICA;
				gpUno = NOAPLICA;
				gpCero = obj.getGruposPrioritarios().get(0).getCtgprioritarioId().getDescripcion();
				break;
			default:
				break;
			}
			
		}else{
			gpCuatro = NOAPLICA;
			gpTres = NOAPLICA;
			gpDos = NOAPLICA;
			gpUno = NOAPLICA;
			gpCero = NOAPLICA;
		}
		/**
		 * DIAGNOSTICOS
		 */
		if(obj.getDiagnostico() != null){
			switch (obj.getDiagnostico().size()) {
			case 5:
				dCuatro = obj.getDiagnostico().get(4).getCieId().getCodigo()+" "+obj.getDiagnostico().get(4).getCieId().getNombre();
				dTres = obj.getDiagnostico().get(3).getCieId().getCodigo()+" "+obj.getDiagnostico().get(3).getCieId().getNombre();
				dDos = obj.getDiagnostico().get(2).getCieId().getCodigo()+" "+obj.getDiagnostico().get(2).getCieId().getNombre();
				dUno = obj.getDiagnostico().get(1).getCieId().getCodigo()+" "+obj.getDiagnostico().get(1).getCieId().getNombre();
				dCero = obj.getDiagnostico().get(0).getCieId().getCodigo()+" "+obj.getDiagnostico().get(0).getCieId().getNombre();
				break;
			case 4:
				dCuatro = NOAPLICA;
				dTres = obj.getDiagnostico().get(3).getCieId().getCodigo()+" "+obj.getDiagnostico().get(3).getCieId().getNombre();
				dDos = obj.getDiagnostico().get(2).getCieId().getCodigo()+" "+obj.getDiagnostico().get(2).getCieId().getNombre();
				dUno = obj.getDiagnostico().get(1).getCieId().getCodigo()+" "+obj.getDiagnostico().get(1).getCieId().getNombre();
				dCero = obj.getDiagnostico().get(0).getCieId().getCodigo()+" "+obj.getDiagnostico().get(0).getCieId().getNombre();
				break;
			case 3:
				dCuatro = NOAPLICA;
				dTres = NOAPLICA;
				dDos = obj.getDiagnostico().get(2).getCieId().getCodigo()+" "+obj.getDiagnostico().get(2).getCieId().getNombre();
				dUno = obj.getDiagnostico().get(1).getCieId().getCodigo()+" "+obj.getDiagnostico().get(1).getCieId().getNombre();
				dCero = obj.getDiagnostico().get(0).getCieId().getCodigo()+" "+obj.getDiagnostico().get(0).getCieId().getNombre();
				break;
			case 2:
				dCuatro = NOAPLICA;
				dTres = NOAPLICA;
				dDos = NOAPLICA;
				dUno = obj.getDiagnostico().get(1).getCieId().getCodigo()+" "+obj.getDiagnostico().get(1).getCieId().getNombre();
				dCero = obj.getDiagnostico().get(0).getCieId().getCodigo()+" "+obj.getDiagnostico().get(0).getCieId().getNombre();
				break;
			case 1:
				dCuatro = NOAPLICA;
				dTres = NOAPLICA;
				dDos = NOAPLICA;
				dUno = NOAPLICA;
				dCero = obj.getDiagnostico().get(0).getCieId().getCodigo()+" "+obj.getDiagnostico().get(0).getCieId().getNombre();
				break;
			default:
				break;
			}
		}else{
			dCuatro = NOAPLICA;
			dTres = NOAPLICA;
			dDos = NOAPLICA;
			dUno = NOAPLICA;
			dCero = NOAPLICA;
		}
		
		switch (columnIndex) {
		case 0:
			return "";
		case 1:
			return rowIndex + 1;
		case 2:
			return obj.getAtencionMedica().getId();
		case 3:
			StringBuilder nombre = new StringBuilder();
			nombre.append(obj.getPersona().getPrimernombre());
			nombre.append(ESPACIO);
			nombre.append(obj.getPersona().getSegundonombre() == null ? "" :  obj.getPersona().getSegundonombre());
			nombre.append(ESPACIO);
			nombre.append(obj.getPersona().getPrimerapellido());
			nombre.append(ESPACIO);
			nombre.append(obj.getPersona().getSegundoapellido() == null ? "" : obj.getPersona().getSegundoapellido());
			return nombre;
		case 4:
			return obj.getPersona().getNumerohistoriaclinica();
		case 5:
			return obj.getPersona().getCtsexoId().getDescripcion();
		case 6:
			return obj.getPersona().getCtorientacionsexualId().getDescripcion();
		case 7:
			if(obj.getPersona().getCtidentidadgeneroId() == null){
				return NOAPLICA;
			}
			return obj.getPersona().getCtidentidadgeneroId().getDescripcion();
		case 8:
			return FechasUtil.formateadorfechaAString(ComunEnum.PATRON_FECHA6.getDescripcion(), obj.getPersona().getFechanacimiento());
		case 9:
			StringBuilder edadAnios = new StringBuilder();
			edadAnios.append(FechasUtil.getDiffDatesDesdeHasta(obj.getPersona().getFechanacimiento(), obj.getAtencionMedica().getFechaatencion(),0));
			return edadAnios;
		case 10:
			StringBuilder edadMeses = new StringBuilder();
			edadMeses.append(FechasUtil.getDiffDatesDesdeHasta(obj.getPersona().getFechanacimiento(), obj.getAtencionMedica().getFechaatencion(),1));
			return edadMeses;
		case 11:
			StringBuilder edadDias = new StringBuilder();
			edadDias.append(FechasUtil.getDiffDatesDesdeHasta(obj.getPersona().getFechanacimiento(), obj.getAtencionMedica().getFechaatencion(),2));
			return edadDias;
		case 12:
			return obj.getPersona().getPaisId().getCodigo();
		case 13:
			return obj.getPersona().getCttiposegurosaludId().getDescripcion();
		case 14:
			return obj.getAtencionMedica().getUsuariocreacionId().getNumeroidentificacion();
		case 15:
			if(obj.getPersona().getCtnacionalidadetnicaId() != null){
				return	obj.getPersona().getCtnacionalidadetnicaId().getDescripcion();
			}
			return NOAPLICA;
		case 16:
			if(obj.getPersona().getCtpuebloId() != null){
				return	obj.getPersona().getCtpuebloId().getDescripcion();
			}
			return NOAPLICA;
		case 17:
			return obj.getPersona().getParroquiaId().getCantonId().getProvinciaId().getDescripcion();
		case 18:
			return obj.getPersona().getParroquiaId().getCantonId().getDescripcion();
		case 19:
			return obj.getPersona().getParroquiaId().getDescripcion();
		case 20:
			return obj.getPersona().getDirecciondomicilio();
		case 21:
			return obj.getPersona().getTelefonopaciente();
		case 22:
			if(obj.getPersona().getTelefonofamiliar() != null){
				return obj.getPersona().getTelefonofamiliar();
			}
			return NOAPLICA;
		case 23:
			return obj.getPersona().getCttipobonoId().getDescripcion();
		case 24:
			return gpCero;
		case 25:
			return gpUno;
		case 26:
			return gpDos;
		case 27:
			return gpTres;
		case 28:
			return gpCuatro;
		case 29:
			return dCero;
		case 30:
			if(dCero.equals(NOAPLICA)){
				return 0;
			}else if(obj.getDiagnostico().get(0).getCtmorbilidaddiagnosticoId() != null && 
					obj.getDiagnostico().get(0).getCtmorbilidaddiagnosticoId().getId() == prevencion){
				tipo = new StringBuilder();
				tipo.append(obj.getDiagnostico().get(0).getCtprevenciondiagnosticoId().getId());
				tipo.append(ESPACIO);
				tipo.append(obj.getDiagnostico().get(0).getCtprevenciondiagnosticoId().getDescripcion());
				return tipo;
			}
			return 0;
		case 31:
			if(dCero.equals(NOAPLICA)){
				return 0;
			}else if(obj.getDiagnostico().get(0).getCtmorbilidaddiagnosticoId() != null 
					&& obj.getDiagnostico().get(0).getCtmorbilidaddiagnosticoId().getId() == morbilidad){
				tipo = new StringBuilder();
				tipo.append(obj.getDiagnostico().get(0).getCtprevenciondiagnosticoId().getId());
				tipo.append(ESPACIO);
				tipo.append(obj.getDiagnostico().get(0).getCtprevenciondiagnosticoId().getDescripcion());
				return tipo;
			}
			return 0;
		case 32:
			if(dCero.equals(NOAPLICA)){
				return 0;
			}else if(obj.getDiagnostico().get(0).getCtcondiciondiagnositcoId() != null){
				return obj.getDiagnostico().get(0).getCtcondiciondiagnositcoId().getId();
			}
			return 0;
		case 33:
			return dUno;
		case 34:
			if(dUno.equals(NOAPLICA)){
				return 0;
			}else if(obj.getDiagnostico().get(1).getCtprevenciondiagnosticoId() != null && 
					obj.getDiagnostico().get(1).getCtmorbilidaddiagnosticoId().getId() == prevencion){
				tipo = new StringBuilder();
				tipo.append(obj.getDiagnostico().get(1).getCtprevenciondiagnosticoId().getId());
				tipo.append(ESPACIO);
				tipo.append(obj.getDiagnostico().get(1).getCtprevenciondiagnosticoId().getDescripcion());
				return tipo;
			}
			return 0;
		case 35:
			if(dUno.equals(NOAPLICA)){
				return 0;
			}else if(obj.getDiagnostico().get(1).getCtmorbilidaddiagnosticoId() != null && 
					obj.getDiagnostico().get(1).getCtmorbilidaddiagnosticoId().getId() == morbilidad){
				tipo = new StringBuilder();
				tipo.append(obj.getDiagnostico().get(1).getCtprevenciondiagnosticoId().getId());
				tipo.append(ESPACIO);
				tipo.append(obj.getDiagnostico().get(1).getCtprevenciondiagnosticoId().getDescripcion());
				return tipo;
			}
			return 0;
		case 36:
			if(dUno.equals(NOAPLICA)){
				return 0;
			}else if(obj.getDiagnostico().get(1).getCtcondiciondiagnositcoId() != null){
				return obj.getDiagnostico().get(1).getCtcondiciondiagnositcoId().getId();
			}
			return 0;
		case 37:
			return dDos;
		case 38:
			if(dDos.equals(NOAPLICA)){
				return 0;
			}else if(obj.getDiagnostico().get(2).getCtprevenciondiagnosticoId() != null && 
					obj.getDiagnostico().get(2).getCtmorbilidaddiagnosticoId().getId() == prevencion){
				tipo = new StringBuilder();
				tipo.append(obj.getDiagnostico().get(2).getCtprevenciondiagnosticoId().getId());
				tipo.append(ESPACIO);
				tipo.append(obj.getDiagnostico().get(2).getCtprevenciondiagnosticoId().getDescripcion());
				return tipo;
			}
			return 0;
		case 39:
			if(dDos.equals(NOAPLICA)){
				return 0;
			}else if(obj.getDiagnostico().get(2).getCtmorbilidaddiagnosticoId() != null && 
					obj.getDiagnostico().get(2).getCtmorbilidaddiagnosticoId().getId() == morbilidad){
				tipo = new StringBuilder();
				tipo.append(obj.getDiagnostico().get(2).getCtprevenciondiagnosticoId().getId());
				tipo.append(ESPACIO);
				tipo.append(obj.getDiagnostico().get(2).getCtprevenciondiagnosticoId().getDescripcion());
				return tipo;
			}
			return 0;
		case 40:
			if(dDos.equals(NOAPLICA)){
				return 0;
			}else if(obj.getDiagnostico().get(2).getCtcondiciondiagnositcoId() != null){
				return obj.getDiagnostico().get(2).getCtcondiciondiagnositcoId().getId();
			}
			return 0;
		case 41:
			return dTres;
		case 42:
			if(dTres.equals(NOAPLICA)){
				return 0;
			}else if(obj.getDiagnostico().get(3).getCtprevenciondiagnosticoId() != null && 
					obj.getDiagnostico().get(3).getCtmorbilidaddiagnosticoId().getId() == prevencion){
				tipo = new StringBuilder();
				tipo.append(obj.getDiagnostico().get(3).getCtprevenciondiagnosticoId().getId());
				tipo.append(ESPACIO);
				tipo.append(obj.getDiagnostico().get(3).getCtprevenciondiagnosticoId().getDescripcion());
				return tipo;
			}
			return 0;
		case 43:
			if(dTres.equals(NOAPLICA)){
				return 0;
			}else if(obj.getDiagnostico().get(3).getCtmorbilidaddiagnosticoId() != null &&
					obj.getDiagnostico().get(3).getCtmorbilidaddiagnosticoId().getId() == morbilidad){
				tipo = new StringBuilder();
				tipo.append(obj.getDiagnostico().get(3).getCtprevenciondiagnosticoId().getId());
				tipo.append(ESPACIO);
				tipo.append(obj.getDiagnostico().get(3).getCtprevenciondiagnosticoId().getDescripcion());
				return tipo;
			}
			return 0;
		case 44:
			if(dTres.equals(NOAPLICA)){
				return 0;
			}else if(obj.getDiagnostico().get(3).getCtcondiciondiagnositcoId() != null){
				return obj.getDiagnostico().get(3).getCtcondiciondiagnositcoId().getId();
			}
			return 0;
		case 45:
			return dCuatro;
		case 46:
			if(dCuatro.equals(NOAPLICA)){
				return 0;
			}else if(obj.getDiagnostico().get(4).getCtprevenciondiagnosticoId() != null &&
					obj.getDiagnostico().get(4).getCtmorbilidaddiagnosticoId().getId() == prevencion){
				tipo = new StringBuilder();
				tipo.append(obj.getDiagnostico().get(4).getCtprevenciondiagnosticoId().getId());
				tipo.append(ESPACIO);
				tipo.append(obj.getDiagnostico().get(4).getCtprevenciondiagnosticoId().getDescripcion());
				return tipo;
			}
			return 0;
		case 47:
			if(dCuatro.equals(NOAPLICA)){
				return 0;
			}else if(obj.getDiagnostico().get(4).getCtmorbilidaddiagnosticoId() != null && 
					obj.getDiagnostico().get(4).getCtmorbilidaddiagnosticoId().getId() == morbilidad){
				tipo = new StringBuilder();
				tipo.append(obj.getDiagnostico().get(4).getCtprevenciondiagnosticoId().getId());
				tipo.append(ESPACIO);
				tipo.append(obj.getDiagnostico().get(4).getCtprevenciondiagnosticoId().getDescripcion());
				return tipo;
			}
			return 0;
		case 48:
			if(dCuatro.equals(NOAPLICA)){
				return 0;
			}else if(obj.getDiagnostico().get(4).getCtcondiciondiagnositcoId() != null){
				return obj.getDiagnostico().get(4).getCtcondiciondiagnositcoId().getId();
			}
			return 0;
		case 49:
			if(obj.getVariablesRefInt().getRefConSubsistemaDesc() != null){
				tipo = new StringBuilder();
				tipo.append(obj.getVariablesRefInt().getRefConSubsistemaCod());
				tipo.append(ESPACIO);
				tipo.append(obj.getVariablesRefInt().getRefConSubsistemaDesc());
				return tipo;
			}else{
				return NOAPLICA;
			}
		case 50:
			if(obj.getVariablesRefInt().getRefConInterconsultaDesc() != null){
				tipo = new StringBuilder();
				tipo.append(obj.getVariablesRefInt().getRefConInterconsultaCod());
				tipo.append(ESPACIO);
				tipo.append(obj.getVariablesRefInt().getRefConInterconsultaDesc());
				return tipo;
			}else{
				return NOAPLICA;
			}
		case 51:
			return obj.getVariablesDatosAntropometricos().getDatAntTalla() == null ? "" : obj.getVariablesDatosAntropometricos().getDatAntTalla();
		case 52:
			return obj.getVariablesDatosAntropometricos().getDatAntTallaCorregida() == null ? "" : obj.getVariablesDatosAntropometricos().getDatAntTallaCorregida();
		case 53:
			return obj.getVariablesDatosAntropometricos().getDatAntPeso() == null ? "" : obj.getVariablesDatosAntropometricos().getDatAntPeso();
		case 54:
			return obj.getVariablesDatosAntropometricos().getDatAntPerCefalico() == null ? "" : obj.getVariablesDatosAntropometricos().getDatAntPerCefalico();
		case 55:
			return obj.getVariablesDatosAntropometricos().getDatAntImc() == null ? "" : obj.getVariablesDatosAntropometricos().getDatAntImc();
		case 56:
			dato = new StringBuilder();
			dato.append(obj.getVariablesDatosAntropometricos().getDatAntImcClasificacion() == null ? "" :  obj.getVariablesDatosAntropometricos().getDatAntImcClasificacion());
			dato.append(ESPACIO);
			dato.append(obj.getVariablesDatosAntropometricos().getDatAntImcClasificacionDesc() == null ? "" :  obj.getVariablesDatosAntropometricos().getDatAntImcClasificacionDesc());
			return dato;
		case 57:
			dato = new StringBuilder();
			dato.append(obj.getVariablesDatosAntropometricos().getCodDatAntTipTomaTalla() == null ? "" :  obj.getVariablesDatosAntropometricos().getCodDatAntTipTomaTalla());
			dato.append(ESPACIO);
			dato.append(obj.getVariablesDatosAntropometricos().getCodDatAntTipTomaTallaDesc() == null ? "" :  obj.getVariablesDatosAntropometricos().getCodDatAntTipTomaTallaDesc());
			return dato;
		case 58:
			return obj.getVariablesDatosAntropometricos().getDatAntPuntajeZTe() == null ? "" : obj.getVariablesDatosAntropometricos().getDatAntPuntajeZTe();
		case 59:
			return obj.getVariablesDatosAntropometricos().getDatAntPuntajeZPe() == null ? "" : obj.getVariablesDatosAntropometricos().getDatAntPuntajeZPe();
		case 60:
			return obj.getVariablesDatosAntropometricos().getDatAntPuntajeZPt() == null ? "" : obj.getVariablesDatosAntropometricos().getDatAntPuntajeZPt();
		case 61:
			return obj.getVariablesDatosAntropometricos().getDatAntPuntajeZImcE() == null ? "" : obj.getVariablesDatosAntropometricos().getDatAntPuntajeZImcE();
		case 62:
			return obj.getVariablesDatosAntropometricos().getDatAntPuntajeZPcE() == null ? "" : obj.getVariablesDatosAntropometricos().getDatAntPuntajeZPcE();
		case 63:
			dato = new StringBuilder();
			dato.append(obj.getVariablesDatosAntropometricos().getDatAntCategoriaTe() == null ? "" :  obj.getVariablesDatosAntropometricos().getDatAntCategoriaTe());
			dato.append(ESPACIO);
			dato.append(obj.getVariablesDatosAntropometricos().getDatAntCategoriaTeDesc() == null ? "" :  obj.getVariablesDatosAntropometricos().getDatAntCategoriaTeDesc());
			return dato;
		case 64:
			dato = new StringBuilder();
			dato.append(obj.getVariablesDatosAntropometricos().getDatAntCategoriaPe() == null ? "" :  obj.getVariablesDatosAntropometricos().getDatAntCategoriaPe());
			dato.append(ESPACIO);
			dato.append(obj.getVariablesDatosAntropometricos().getDatAntCategoriaPeDesc() == null ? "" :  obj.getVariablesDatosAntropometricos().getDatAntCategoriaPeDesc());
			return dato;
		case 65:
			dato = new StringBuilder();
			dato.append(obj.getVariablesDatosAntropometricos().getDatAntCategoriaPt() == null ? "" :  obj.getVariablesDatosAntropometricos().getDatAntCategoriaPt());
			dato.append(ESPACIO);
			dato.append(obj.getVariablesDatosAntropometricos().getDatAntCategoriaPtDesc() == null ? "" :  obj.getVariablesDatosAntropometricos().getDatAntCategoriaPtDesc());
			return dato;
		case 66:
			dato = new StringBuilder();
			dato.append(obj.getVariablesDatosAntropometricos().getDatAntCategoriaImcE() == null ? "" :  obj.getVariablesDatosAntropometricos().getDatAntCategoriaImcE());
			dato.append(ESPACIO);
			dato.append(obj.getVariablesDatosAntropometricos().getDatAntCategoriaImcEDesc() == null ? "" :  obj.getVariablesDatosAntropometricos().getDatAntCategoriaImcEDesc());
			return dato;
		case 67:
			dato = new StringBuilder();
			dato.append(obj.getVariablesDatosAntropometricos().getDatAntCategoriaPcE() == null ? "" :  obj.getVariablesDatosAntropometricos().getDatAntCategoriaPcE());
			dato.append(ESPACIO);
			dato.append(obj.getVariablesDatosAntropometricos().getDatAntCategoriaPcEDesc() == null ? "" :  obj.getVariablesDatosAntropometricos().getDatAntCategoriaPcEDesc());
			return dato;
		case 68:
			return obj.getVariablesDatosAntropometricos().getDatAntHb() == null ? "" : obj.getVariablesDatosAntropometricos().getDatAntHb();
		case 69:
			return obj.getVariablesDatosAntropometricos().getDatAntHbCorregido() == null ? "" : obj.getVariablesDatosAntropometricos().getDatAntHbCorregido();
		case 70:
			dato = new StringBuilder();
			dato.append(obj.getVariablesDatosAntropometricos().getCodIndAnemia() == null ? "" :  obj.getVariablesDatosAntropometricos().getCodIndAnemia());
			dato.append(ESPACIO);
			dato.append(obj.getVariablesDatosAntropometricos().getCodIndAnemiaDesc() == null ? "" :  obj.getVariablesDatosAntropometricos().getCodIndAnemiaDesc());
			return dato;
		case 71:
			dato = new StringBuilder();
			dato.append(obj.getVariablesDatosObstetricos().getCodObsRiesObstetrico() == null ? "" :  obj.getVariablesDatosObstetricos().getCodObsRiesObstetrico());
			dato.append(ESPACIO);
			dato.append(obj.getVariablesDatosObstetricos().getCodObsRiesObstetricoDesc() == null ? "" :  obj.getVariablesDatosObstetricos().getCodObsRiesObstetricoDesc());
			return dato;
		case 72:
			dato = new StringBuilder();
			dato.append(obj.getVariablesDatosObstetricos().getCodObsPlaParto() == null ? "" :  obj.getVariablesDatosObstetricos().getCodObsPlaParto());
			dato.append(ESPACIO);
			dato.append(obj.getVariablesDatosObstetricos().getCodObsPlaPartoDesc() == null ? "" :  obj.getVariablesDatosObstetricos().getCodObsPlaPartoDesc());
			return dato;
		case 73:
			dato = new StringBuilder();
			dato.append(obj.getVariablesDatosObstetricos().getCodObsPlaTransporte() == null ? "" :  obj.getVariablesDatosObstetricos().getCodObsPlaTransporte());
			dato.append(ESPACIO);
			dato.append(obj.getVariablesDatosObstetricos().getCodObsPlaTransporteDesc() == null ? "" :  obj.getVariablesDatosObstetricos().getCodObsPlaTransporteDesc());
			return dato;
		case 74:
			dato = new StringBuilder();
			dato.append(obj.getVariablesDatosObstetricos().getCodObsEmbPlanificado() == null ? "" :  obj.getVariablesDatosObstetricos().getCodObsEmbPlanificado());
			dato.append(ESPACIO);
			dato.append(obj.getVariablesDatosObstetricos().getCodObsEmbPlanificadoDesc() == null ? "" :  obj.getVariablesDatosObstetricos().getCodObsEmbPlanificadoDesc());
			return dato;
		case 75:
			dato = new StringBuilder();
			dato.append(obj.getVariablesDatosObstetricos().getCodObsMetDetSemanasGestacion() == null ? "" :  obj.getVariablesDatosObstetricos().getCodObsMetDetSemanasGestacion());
			dato.append(ESPACIO);
			dato.append(obj.getVariablesDatosObstetricos().getCodObsMetDetSemanasGestacionDesc() == null ? "" :  obj.getVariablesDatosObstetricos().getCodObsMetDetSemanasGestacionDesc());
			return dato;
		case 76:
			return obj.getVariablesDatosObstetricos().getCodObsFum() == null ? "" : obj.getVariablesDatosObstetricos().getCodObsFum();
		case 77:
			return obj.getVariablesDatosObstetricos().getObsSemGestacion() == null ? "" : obj.getVariablesDatosObstetricos().getObsSemGestacion();
		case 78:
			dato = new StringBuilder();
			dato.append(obj.getVariablesExamenLaboratorio().getCodExaLabSifilisNoTreponemica() == null ? "" :  obj.getVariablesExamenLaboratorio().getCodExaLabSifilisNoTreponemica());
			dato.append(ESPACIO);
			dato.append(obj.getVariablesExamenLaboratorio().getCodExaLabSifilisNoTreponemicaDesc() == null ? "" :  obj.getVariablesExamenLaboratorio().getCodExaLabSifilisNoTreponemicaDesc());
			return dato;
		case 79:
			dato = new StringBuilder();
			dato.append(obj.getVariablesExamenLaboratorio().getCodExaLabSifilisTreponemica() == null ? "" :  obj.getVariablesExamenLaboratorio().getCodExaLabSifilisTreponemica());
			dato.append(ESPACIO);
			dato.append(obj.getVariablesExamenLaboratorio().getCodExaLabSifilisTreponemicaDesc() == null ? "" :  obj.getVariablesExamenLaboratorio().getCodExaLabSifilisTreponemicaDesc());
			return dato;
		case 80:
			dato = new StringBuilder();
			dato.append(obj.getVariablesExamenLaboratorio().getCodExaLabSifilisTratamiento() == null ? "" :  obj.getVariablesExamenLaboratorio().getCodExaLabSifilisTratamiento());
			dato.append(ESPACIO);
			dato.append(obj.getVariablesExamenLaboratorio().getCodExaLabSifilisTratamientoDesc() == null ? "" :  obj.getVariablesExamenLaboratorio().getCodExaLabSifilisTratamientoDesc());
			return dato;
		case 81:
			dato = new StringBuilder();
			dato.append(obj.getVariablesExamenLaboratorio().getCodExaLabSifilisTratamientoPareja() == null ? "" :  obj.getVariablesExamenLaboratorio().getCodExaLabSifilisTratamientoPareja());
			dato.append(ESPACIO);
			dato.append(obj.getVariablesExamenLaboratorio().getCodExaLabSifilisTratamientoParejaDesc() == null ? "" :  obj.getVariablesExamenLaboratorio().getCodExaLabSifilisTratamientoParejaDesc());
			return dato;
		case 82:
			dato = new StringBuilder();
			dato.append(obj.getVariablesExamenLaboratorio().getCodResBacteriuria() == null ? "" :  obj.getVariablesExamenLaboratorio().getCodResBacteriuria());
			dato.append(ESPACIO);
			dato.append(obj.getVariablesExamenLaboratorio().getCodResBacteriuriaDesc() == null ? "" :  obj.getVariablesExamenLaboratorio().getCodResBacteriuriaDesc());
			return dato;	
		case 83:
			return obj.getVariablesViolencia().getViolNumSerie() == null ? "" : obj.getVariablesViolencia().getViolNumSerie();
		case 84:
			dato = new StringBuilder();
			dato.append(obj.getVariablesViolencia().getCodViolIdeAgresor() == null ? "" :  obj.getVariablesViolencia().getCodViolIdeAgresor());
			dato.append(ESPACIO);
			dato.append(obj.getVariablesViolencia().getCodViolIdeAgresorDesc() == null ? "" :  obj.getVariablesViolencia().getCodViolIdeAgresorDesc());
			return dato;
		case 85:
			dato = new StringBuilder();
			dato.append(obj.getVariablesViolencia().getCodViolLesiones() == null ? "" :  obj.getVariablesViolencia().getCodViolLesiones());
			dato.append(ESPACIO);
			dato.append(obj.getVariablesViolencia().getCodViolLesionesDesc() == null ? "" :  obj.getVariablesViolencia().getCodViolLesionesDesc());
			return dato;
		case 86:
			dato = new StringBuilder();
			dato.append(obj.getVariablesViolencia().getCodViolParentesco() == null ? "" :  obj.getVariablesViolencia().getCodViolParentesco());
			dato.append(ESPACIO);
			dato.append(obj.getVariablesViolencia().getCodViolParentescoDesc() == null ? "" :  obj.getVariablesViolencia().getCodViolParentescoDesc());
			return dato;
		case 87:
			dato = new StringBuilder();
			dato.append(obj.getVariablesVih().getCodVihMotPrueba() == null ? "" :  obj.getVariablesVih().getCodVihMotPrueba());
			dato.append(ESPACIO);
			dato.append(obj.getVariablesVih().getCodVihMotPruebaDesc() == null ? "" :  obj.getVariablesVih().getCodVihMotPruebaDesc());
			return dato;
		case 88:
			dato = new StringBuilder();
			dato.append(obj.getVariablesVih().getCodVihPriPrueba() == null ? "" :  obj.getVariablesVih().getCodVihPriPrueba());
			dato.append(ESPACIO);
			dato.append(obj.getVariablesVih().getCodVihPriPruebaDesc() == null ? "" :  obj.getVariablesVih().getCodVihPriPruebaDesc());
			return dato;
		case 89:
			dato = new StringBuilder();
			dato.append(obj.getVariablesVih().getCodVihPriPruebaReactiva() == null ? "" :  obj.getVariablesVih().getCodVihPriPruebaReactiva());
			dato.append(ESPACIO);
			dato.append(obj.getVariablesVih().getCodVihPriPruebaReactivaDesc() == null ? "" :  obj.getVariablesVih().getCodVihPriPruebaReactivaDesc());
			return dato;
		case 90:
			dato = new StringBuilder();
			dato.append(obj.getVariablesVih().getCodVihSegPrueba() == null ? "" :  obj.getVariablesVih().getCodVihSegPrueba());
			dato.append(ESPACIO);
			dato.append(obj.getVariablesVih().getCodVihSegPruebaDesc() == null ? "" :  obj.getVariablesVih().getCodVihSegPruebaDesc());
			return dato;
		case 91:
			dato = new StringBuilder();
			dato.append(obj.getVariablesVih().getCodVihSegPruebaReactiva() == null ? "" :  obj.getVariablesVih().getCodVihSegPruebaReactiva());
			dato.append(ESPACIO);
			dato.append(obj.getVariablesVih().getCodVihSegPruebaReactivaDesc() == null ? "" :  obj.getVariablesVih().getCodVihSegPruebaReactivaDesc());
			return dato;
		case 92:
			dato = new StringBuilder();
			dato.append(obj.getVariablesVih().getCodVihViaTransmision() == null ? "" :  obj.getVariablesVih().getCodVihViaTransmision());
			dato.append(ESPACIO);
			dato.append(obj.getVariablesVih().getCodVihViaTransmisionDesc() == null ? "" :  obj.getVariablesVih().getCodVihViaTransmisionDesc());
			return dato;
		case 93:
			return obj.getVariablesVih().getVihCargaViral() == null ? "" :  obj.getVariablesVih().getVihCargaViral();
		case 94:
			return obj.getVariablesVih().getVihCd4() == null ? "" :  obj.getVariablesVih().getVihCd4();
		case 95:
			dato = new StringBuilder();
			dato.append(obj.getVariablesGruposVulnerables().getCodGrpVulnerable1() == null ? "" :  obj.getVariablesGruposVulnerables().getCodGrpVulnerable1());
			dato.append(ESPACIO);
			dato.append(obj.getVariablesGruposVulnerables().getCodGrpVulnerableDesc1() == null ? "" :  obj.getVariablesGruposVulnerables().getCodGrpVulnerableDesc1());
			return dato;
		case 96:
			dato = new StringBuilder();
			dato.append(obj.getVariablesGruposVulnerables().getCodGrpVulnerable2() == null ? "" :  obj.getVariablesGruposVulnerables().getCodGrpVulnerable2());
			dato.append(ESPACIO);
			dato.append(obj.getVariablesGruposVulnerables().getCodGrpVulnerableDesc2() == null ? "" :  obj.getVariablesGruposVulnerables().getCodGrpVulnerableDesc2());
			return dato;
		case 97:
			dato = new StringBuilder();
			dato.append(obj.getVariablesGruposVulnerables().getCodGrpVulnerable3() == null ? "" :  obj.getVariablesGruposVulnerables().getCodGrpVulnerable3());
			dato.append(ESPACIO);
			dato.append(obj.getVariablesGruposVulnerables().getCodGrpVulnerableDesc3() == null ? "" :  obj.getVariablesGruposVulnerables().getCodGrpVulnerableDesc3());
			return dato;
		case 98:
			dato = new StringBuilder();
			dato.append(obj.getVariablesGruposVulnerables().getCodGrpVulnerable4() == null ? "" :  obj.getVariablesGruposVulnerables().getCodGrpVulnerable4());
			dato.append(ESPACIO);
			dato.append(obj.getVariablesGruposVulnerables().getCodGrpVulnerableDesc4() == null ? "" :  obj.getVariablesGruposVulnerables().getCodGrpVulnerableDesc4());
			return dato;
		case 99:
			dato = new StringBuilder();
			dato.append(obj.getVariablesGruposVulnerables().getCodGrpVulnerable5() == null ? "" :  obj.getVariablesGruposVulnerables().getCodGrpVulnerable5());
			dato.append(ESPACIO);
			dato.append(obj.getVariablesGruposVulnerables().getCodGrpVulnerableDesc5() == null ? "" :  obj.getVariablesGruposVulnerables().getCodGrpVulnerableDesc5());
			return dato;
		case 100:
			return FechasUtil.formateadorfechaAString(ComunEnum.PATRON_FECHA6.getDescripcion(), obj.getAtencionMedica().getFechaatencion());
		case 101:
			if(obj.getEstrategiapaciente() != null){
				return obj.getEstrategiapaciente().getCtestrategiaId().getDescripcion();
			}
			return NOAPLICA;
		case 102:
			if(obj.getIntraextramural() != null){
				return obj.getIntraextramural().getCtlugaratencionId().getDescripcion();
			}
			return NOAPLICA;
		case 103:
			return obj.getEntidad().getId();
		case 104:
			return obj.getEntidad().getNombreoficial();
		case 105:
			if(obj.getEntidad().getTipoestablecimientoId() != null){
				return	obj.getEntidad().getTipoestablecimientoId().getDescripcion();
			}
			return NOAPLICA;
		case 106:
			return "";
		case 107:
			nombre = new StringBuilder();
			nombre.append(obj.getAtencionMedica().getUsuariocreacionId().getPrimernombre());
			nombre.append(ESPACIO);
			nombre.append(obj.getAtencionMedica().getUsuariocreacionId().getSegundonombre() == null ? "" :  obj.getAtencionMedica().getUsuariocreacionId().getSegundonombre());
			return nombre;
		case 108:
			nombre = new StringBuilder();
			nombre.append(obj.getAtencionMedica().getUsuariocreacionId().getPrimerapellido());
			nombre.append(ESPACIO);
			nombre.append(obj.getAtencionMedica().getUsuariocreacionId().getSegundoapellido() == null ? "" : obj.getAtencionMedica().getUsuariocreacionId().getSegundoapellido());
			return nombre;
		case 109:
			if(obj.getAtencionMedica().getUsuariocreacionId().getPaisId() != null){
				return obj.getAtencionMedica().getUsuariocreacionId().getPaisId().getCodigo();
			}
			return NOAPLICA;
		case 110:
			if(obj.getAtencionMedica().getUsuariocreacionId().getCtsexoId() != null){
				return obj.getAtencionMedica().getUsuariocreacionId().getCtsexoId().getDescripcion();
			}
			return NOAPLICA;
		case 111:
			if(obj.getAtencionMedica().getUsuariocreacionId().getCtnacionalidadetnicaId() != null){
				return obj.getAtencionMedica().getUsuariocreacionId().getCtnacionalidadetnicaId().getDescripcion();
			}
			return NOAPLICA;
		case 112:
			return FechasUtil.formateadorfechaAString(ComunEnum.PATRON_FECHA6.getDescripcion(), obj.getAtencionMedica().getUsuariocreacionId().getFechanacimiento());
		case 113:
			return obj.getAtencionMedica().getCtespecialidadmedicaId().getDescripcion();
		default:
			return "";
		}
	}

}
